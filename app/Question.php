<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class Question extends Model 
{
	
	protected $fillable = [
        'category','add_by_user','question','next_open_qsn','point','negative_point','question_type','attachment','status'
    ];
		
	public function tests()
    {
        return $this->belongsToMany('App\Test');
    }
	public function answers()
    {
        return $this->hasMany('App\Answer','question_id','id');
    }
	public function sub_questions()
    {
        return $this->hasMany('App\SubQuestion','question_id','id');
    }
	public function sub_answers()
    {
        return $this->hasMany('App\SubAnswer','question_id','id');
    }
	public function test_answer()
    {
        return $this->hasMany('App\TestAnswer','question_id','id');
    }
}
