<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class AnswerComment extends Model 
{
    protected $table = "answer_comments";
    protected $fillable = [
        'user_id', 'answer_id', 'parent_id', 'comment', 'like_count'
    ];
	
	public  function parentComment(){
        return $this->belongsTo('App\AnswerComment', 'parent_id');
    }
	public function replyComments()
    {
        return $this->hasMany('App\AnswerComment','parent_id','id')->orderBy('id','DESC');
    }
	public  function comuser(){
        return $this->belongsTo('App\User', 'user_id','id');
    }
}
