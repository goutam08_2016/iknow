<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class FavoriteCategory extends Model 
{
	//
	 protected $table = "favorite_categories";
	 protected $fillable = [
        'user_id','cat_id'
    ];
	
	public function category()
    {
        return $this->hasOne('App\Category','id','cat_id');
    }
	
}
