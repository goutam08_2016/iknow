<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class Answer extends Model 
{
	
	protected $fillable = [
        'user_id','question_id','content',
    ];
		
	public function question()
    {
        return $this->belongsTo('App\Question');
	}
	public function answerUser()
    {
        return $this->hasOne('App\User','id','user_id');
	}
	public function comments()
    {
        return $this->hasMany('App\AnswerComment','answer_id','id')->where('parent_id',0)->orderBy('id','DESC');
    }
	public function all_comments()
    {
        return $this->hasMany('App\AnswerComment','answer_id','id');
    }
	
	public function upvotes()
    {

        return $this->hasMany('App\UpvoteDownvote','answer_id','id')->where('status','Y');

    }
	public function downvotes()
    {

        return $this->hasMany('App\UpvoteDownvote','answer_id','id')->where('status','N');

    }
}
