<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class Invoice extends Model 
{
	protected $fillable = [
        'to_email','from_email','package_id','user_id','price','vat','amount','description','note','tc','by_admin',
    ];
	
	
}
