<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class Category extends Model 
{	protected $table = "categories";
	 protected $fillable = [
        'title','parent_id','icon'
    ];
		
		public function blogs()    {        return $this->hasMany('App\Blog','cat_id','id')->orderBy('id','desc');    }
}
