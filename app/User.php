<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name','nickname','nationality', 'details', 'user_name','email','recovery_email','user_type','profile_image','company_name','company_number','vat_number', 'password', 'confirmation_code', 'utc_timezone', 'address', 'postal_code', 'lat', 'lng', 'status','in_step','gender','dob','current_address','age', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	public function work_experiences()
    {
        return $this->hasMany('App\WorkExperience','user_id','id')->orderBy('id','desc');
    }
	
	public function educations()
    {
        return $this->hasMany('App\Education','user_id','id')->orderBy('id','desc');
    }
	public function tropics()
    {
        return $this->hasMany('App\Tropic','user_id','id');
    }
	
	
	public function groups()
    {
        return $this->hasMany('App\Group','user_id','id')->orderBy('id','desc');
    }
	public function groupMembers()
    {
        return $this->belongsToMany('App\Group');
    }
	public function categories()
    {
        return $this->hasMany('App\Category','user_id','id')->orderBy('id','desc');
    }	
	public function packages()
    {
        return $this->belongsToMany('App\Package');
    }
	public function current_package()
    {
        return $this->belongsToMany('App\Package')->withPivot('created_at')->where('paid','Y')->where('valid','Y')->orderBy('id','desc');
    }	
	public function questions()
    {
        return $this->hasMany('App\Question','add_by_user','id')->orderBy('id','desc');
    }	
	public function test_evaluations()
    {
        return $this->hasMany('App\TestEvaluation','user_id','id');
    }	
	public function user_Addresses()
    {
        return $this->hasMany('App\UserAddres','user_id','id');
    }
	public function user_favorites()
    {
        return $this->hasMany('App\FavoriteTropic','user_id','id');
    }
	public function contact_ifno()
    {
        return $this->hasMany('App\ContactIfno','user_id','id')->orderBy('id','DESC');
    }
	public function filterset_order()
    {
        return $this->hasOne('App\FilterOrderSet','user_id','id');
    }	
	
	public function user_follows()
    {
        return $this->hasMany('App\Follow','followed_by','id')->orderBy('id','DESC');
    }
	public function user_followers()
    {
        return $this->hasMany('App\Follow','followed_to','id')->orderBy('id','DESC');
    }
	public function touser_forwards()
    {
        return $this->hasMany('App\QuestionForward','to_user','id')->orderBy('id','DESC');
    }
	
	public function user_friend_requests()
    {
        return $this->hasMany('App\Friend','to_user_id','id')->orderBy('id','DESC');
    }
	public function friend_requests()
    {
        return $this->hasMany('App\Friend','to_user_id','id')->where('status','N')->orderBy('id','DESC');
    }
	public function user_friends()
    {
        return $this->hasMany('App\Friend','to_user_id','id')->where('status','Y')->orderBy('id','DESC');
    }
	
	public function question_follows()
    {
        return $this->hasMany('App\QuestionFollow','followed_by','id')->orderBy('id','DESC');
    }
	
	public function fav_categories()
    {
        return $this->hasMany('App\FavoriteCategory','user_id','id');
    }
	public function blogs()
    {
        return $this->hasMany('App\Blog','user_id','id')->orderBy('id','desc');
    }
	public function pass_qsns()
    {
        return $this->hasMany('App\PassQuestion','user_id','id');
    }
	
	public function fav_friends()
    {
        return $this->hasMany('App\FavFriend','user_id','id');
    }
}
