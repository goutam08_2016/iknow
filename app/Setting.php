<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'admin_name',
        'admin_email',
        'site_title',
        'contact_email',
        'contact_name',
        'contact_phone',
        'site_logo',		
        'seo_title',
        'seo_keywords',
        'seo_description',
        'contact_address',
        'lat',
        'lng',
    ];
}
