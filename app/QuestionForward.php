<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class QuestionForward extends Model 
{
    protected $table = "question_forwards";
    protected $fillable = [
        'question_id', 'from_user', 'to_user', 'type', 'note',
    ];
	
	public function question()
    {
        return $this->hasOne('App\AskQuestion','id','question_id');
    }
	public function from_usr()
    {
        return $this->hasOne('App\User','id','from_user');
    }
}
