<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class Blog extends Model 
{	
	protected $fillable = [
        'title','blog_image','user_id','cat_id','description','status'
    ];	
	public function blog_user()	{		return $this->hasOne('App\User','id','user_id');	}
	public function blog_category()	{		return $this->hasOne('App\Category','id','cat_id');		}
	public function blog_comments()	{		return $this->hasMany('App\BlogComment','blog_id','id');	}
	public function blog_views()	{		return $this->hasMany('App\BlogView','blog_id','id');	}	
}
