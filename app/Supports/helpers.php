<?php

use Illuminate\Support\Str;
//use App\AskQuestion;
//use DB;
use App\User;




if (! function_exists('admin_url')) {

    /**

     * Generate a url for the application.

     * @param string $path

     * @return \Illuminate\Contracts\Routing\UrlGenerator|string

     */

    function admin_url($path = null)

    {

        $admin_path = ADMIN_PREFIX . '/';

        if (is_null($path)) {

            return url($admin_path);

        }

        return url($admin_path . $path);

    }



}



if (! function_exists('random_number')) {

    function random_number($length) {

        $result = '';



        for($i = 0; $i < $length; $i++) {

            $result .= mt_rand(0, 9);

        }



        return $result;

    }

}



if (! function_exists('words')) {

    /**

     * Limit the number of words in a string.

     *

     * @param  string  $value

     * @param  int     $words

     * @param  string  $end

     * @return string

     */

    function words($value, $words = 100, $end = '...')

    {

        return Str::words($value, $words, $end);

    }

}



if (! function_exists('generate_id_slug')) {

    function generate_id_slug($value)

    {

        return random_number(8) . $value . random_number(10);

    }

}



if (! function_exists('get_id_from_slug')) {

    function get_id_from_slug($value)

    {

        return substr(substr($value,0, -10), 8);

    }

}

if (! function_exists('youtubeconverter')) {

	function youtubeconverter($string) {

	  $string = preg_replace('~

		  # Match non-linked youtube URL in the wild. (Rev:20111012)

		  https?://         # Required scheme. Either http or https.

		  (?:[0-9A-Z-]+\.)? # Optional subdomain.

		  (?:               # Group host alternatives.

			youtu\.be/      # Either youtu.be,

		  | youtube\.com    # or youtube.com followed by

			\S*             # Allow anything up to VIDEO_ID,

			[^\w\-\s]       # but char before ID is non-ID char.

		  )                 # End host alternatives.

		  ([\w\-]{11})      # $1: VIDEO_ID is exactly 11 chars.

		  (?=[^\w\-]|$)     # Assert next char is non-ID or EOS.

		  (?!               # Assert URL is not pre-linked.

			[?=&+%\w]*      # Allow URL (query) remainder.

			(?:             # Group pre-linked alternatives.

			  [\'"][^<>]*>  # Either inside a start tag,

			| </a>          # or inside <a> element text contents.

			)               # End recognized pre-linked alts.

		  )                 # End negative lookahead assertion.

		  [?=&+%\w-]*        # Consume any URL (query) remainder.

		  ~ix', '<iframe width="645" height="363" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>', $string);

	  return $string;

	}
}

if (! function_exists('get_user_name')) {
    function get_user_name($user_id=0)
    {
        $user_id=(int)$user_id;
        if($user_id!=0 && $user_id!='')
        {
            $user_data=User::find($user_id);
            if(count($user_data)>0)
            {
                echo $user_data->nickname;
            }
            else{
                echo 'Not Available';
            }
        }
        else{
             echo 'Not Available';
        }
    }

}

if (! function_exists('time_elapsed_string')) {
	function time_elapsed_string($ptime = 0)
	{
		$etime = time() - $ptime;
		if ($etime < 1)
		{
			return '0 seconds';
		}
		$a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
					30 * 24 * 60 * 60       =>  'month',
					24 * 60 * 60            =>  'day',
					60 * 60                 =>  'hour',
					60                      =>  'minute',
					1                       =>  'second'
					);
		foreach ($a as $secs => $str)
		{
			$d = $etime / $secs;
			if ($d >= 1)
			{
				$r = round($d);
				return $r . ' ' . $str . ($r > 1 ? 's' : '') ;
			}
		}
	}
}

if (! function_exists('reply_comment')) {
	function reply_comment($commts = null,$answer = null,$comnt_data='')
	{		
		if(count($commts)>0)
		{
			foreach($commts as $comntnext)
			{
				$comnt_data.='<li>
					<figure><img src="'.asset("assets/frontend").'/images/userthumb.png" alt=""></figure>
					<div class="blogcommnt">
						<h4 class="blgTtl"><a href="">'.$comntnext->comuser->nickname.'</a></h4>
						<div class="blogTxts"><p>'.nl2br($comntnext->comment).'</p>';
						if(strlen($comntnext->comment)>1123)
								$comnt_data.='<span class="mr">...more</span>';
						$comnt_data.='</div>
						<div class="blogmeta">
							<span><font>'.$comntnext->like_count.'</font> <a class="cmtLike" href="javascript:;" cmid="'.$comntnext->id.'"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <span>Like</span></a></span>
							<span>'.count($comntnext->replyComments).' <a href="javascript:void(0);" class="replayForm"><i class="fa fa-reply" aria-hidden="true"></i> <span>Reply</span></a></span>
							<span>'.time_elapsed_string(strtotime($comntnext->created_at)).'</span>
							<!--<a href="" class="replaycmt"><i class="fa fa-reply-all" aria-hidden="true"></i> <span>Replay</span></a>-->
						</div>
						<div class="smallCommnt smallreplay clear">
							<figure><img src="'.asset("assets/frontend").'/images/userthumb.png" alt=""></figure>
							<div class="blogcommnt">
								<form class="commentfrm" method="POST">
									<textarea name="comment" placeholder="Write a comment..."></textarea>
									<input name="answer_id" type="hidden" value="'.$answer->id.'"/>
									<input name="parent_id" type="hidden" value="'.$comntnext->id.'"/>
									<button type="submit" value="submit"><i class="fa fa-reply-all" aria-hidden="true"></i><span>Replay</span></button>
								</form>
							</div>
						</div>
						<div class="commntList"><ul>';
						
						if(count($comntnext->replyComments)>0)
						{		
							$comnt_data=reply_comment($comntnext->replyComments,$answer,$comnt_data);
						}
						
						$comnt_data.='</ul></div>
					</div>
				</li>';
			}
		}
		return $comnt_data;
	}
}

if (! function_exists('blog_reply_comment')) {
	function blog_reply_comment($commts = null,$blog = null,$comnt_data='')
	{		
		if(count($commts)>0)
		{
			foreach($commts as $comntnext)
			{
				$comnt_data.='<li>
					<figure><img src="'.asset("assets/frontend").'/images/userthumb.png" alt=""></figure>
					<div class="blogcommnt">
						<h4><a href="">'.$comntnext->comment_user->nickname.'</a></h4>
						<p>'.$comntnext->comment.'</p>
						<div class="blogmeta">
							<span>0 <a href=""><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <span>Like</span></a></span>
							<span>'.count($comntnext->replyComments).' <a href="javascript:void(0);" class="replayForm"><i class="fa fa-reply" aria-hidden="true"></i> <span>Reply</span></a></span>
							<span>'.time_elapsed_string(strtotime($comntnext->created_at)).'</span>
							<!--<a href="" class="replaycmt"><i class="fa fa-reply-all" aria-hidden="true"></i> <span>Replay</span></a>-->
						</div>
						<div class="smallCommnt smallreplay clear">
							<figure><img src="'.asset("assets/frontend").'/images/userthumb.png" alt=""></figure>
							<div class="blogcommnt">
								<form class="commentfrm" method="POST">
									<textarea name="comment" placeholder="Write a comment..."></textarea>
									<input name="blog_id" type="hidden" value="'.$blog->id.'"/>
									<input name="parent_id" type="hidden" value="'.$comntnext->id.'"/>
									<button type="submit" value="submit"><i class="fa fa-reply-all" aria-hidden="true"></i><span>Replay</span></button>
								</form>
							</div>
						</div>
						<div class="commntList"><ul>';
						
						if(count($comntnext->replyComments)>0)
						{		
							$comnt_data=blog_reply_comment($comntnext->replyComments,$blog,$comnt_data);
						}
						
						$comnt_data.='</ul></div>
					</div>
				</li>';
			}
		}
		return $comnt_data;
	}
}

if (! function_exists('sent_question_not_replied')) {
	function sent_question_not_replied($user_id = 0)
	{
		
		$query = \DB::select( \DB::raw("SELECT COUNT(*) as QC FROM ask_questions WHERE id NOT IN (SELECT question_id FROM answers WHERE question_id IN (SELECT id FROM ask_questions WHERE user_id =$user_id ) GROUP BY question_id ) AND user_id =$user_id ") );
		return isset($query[0]) ?$query[0]->QC:'';
	}
}

if (! function_exists('received_question_not_replied')) {
	function received_question_not_replied($user_id = 0)
	{
		
		$query = \DB::select( \DB::raw("SELECT COUNT(question_id) as QC FROM question_forwards WHERE question_id NOT IN (SELECT question_id FROM answers WHERE question_id IN (SELECT question_id FROM question_forwards WHERE to_user =$user_id ) GROUP BY question_id ) AND to_user =$user_id ") );
		return isset($query[0]) ?$query[0]->QC:'';
	}
}
if (! function_exists('last_received_msg')) {
	function last_received_msg($to_user_id = 0,$from_user_id = 0)
	{
		//echo $to_user_id,$from_user_id;
		$query = \DB::select( \DB::raw("SELECT * FROM `chat_messages` WHERE (to_user_id=$to_user_id AND from_user_id=$from_user_id) OR (to_user_id=$from_user_id AND from_user_id=$to_user_id) AND type='T' ORDER BY id DESC LIMIT 1") );
		//dd($query);
		return isset($query[0]) ?substr(strip_tags($query[0]->message), 0 , 25) : '';
	}
}



