<?php
namespace App\Helpers;

use App\Language;
use App\Setting;
use App\Sitetext;
use App\SitetextTranslation;
class CustomHelper
{
    static function siteStaticText($statictextId = 0)
    {
        $text = '';
        if($statictextId != 0)
        {
            $sitetext = Sitetext::translatedIn(app()->getLocale())->where([['id', '=', $statictextId]])->first();
            if(count($sitetext) == 0)
            {
                $fallback_language = Language::where('fallback_locale', 'Y')->first();
                $sitetext = SitetextTranslation::where([['sitetext_id', '=', $statictextId],['locale', '=', $fallback_language->locale]])->first();
            }
            $text = $sitetext->name;
        }
        return $text;
    }
    static function encrypt($string, $key) 
    {
        $result = '';
        for($i=0; $i<strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key))-1, 1);
            $char = chr(ord($char)+ord($keychar));
            $result.=$char;
        }
        return base64_encode($result);
    }
    static function decrypt($string, $key) 
    {
        $result = '';
        $string = base64_decode($string);
        for($i=0; $i<strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key))-1, 1);
            $char = chr(ord($char)-ord($keychar));
            $result.=$char;
        }
        return $result;
    }
}
