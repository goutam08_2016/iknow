<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class FilterOrderSet extends Model 
{
    protected $table = "filter_order_sets";
	public $timestamps = false;
	
    protected $fillable = [
        'user_id', 'filter_set'
    ];
	
	
}
