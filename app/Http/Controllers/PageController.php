<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller {

    // Add methods to add, edit, delete and show pages

    // create method to create new pages
    // submit the form to this method
   /*  public function create()
    {
        $inputs = Input::all();
		//dd($inputs);
        $page = Page::create($inputs);
    } */

    // Show a page by slug
    public function show($slug = 'home')
    {
        //$page = page::whereSlug($slug)->first();
		$page = \App\PageTranslation::where('locale',app()->getLocale())->where('slug',$slug)->first();
		//dd($page);
		if(empty($page))
		{
			return redirect()->to('');
		}
        return view('page.index')->with('page', $page);
    }
}