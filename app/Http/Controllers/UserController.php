<?php

namespace App\Http\Controllers;

use App\User;
use App\Country;
use App\WorkExperience;
use App\Education;
use App\Tropic;
use App\Company;
use App\Job;
use App\University;
use App\SchoolMajor;
use App\AllTropic;
use App\UserAddres;
use App\AskQuestion;
use App\FilterOrderSet;
use App\Answer;
use App\QuestionForward;
use App\Follow;
use App\AnswerComment;
use App\UpvoteDownvote;
use App\Friend;
use App\Chatmessage;
use App\QuestionFollow;
use App\TropicQuestion;
use App\Category;
use App\BlogView;
use App\Blog;
use App\PassQuestion;
use App\QuestionView;
use Image;
use DB;
use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use Omnipay\Omnipay;

use Session;


class UserController extends Controller
{
	private $advance_search_data; 
	public $chat_messsage_data = array();
    /**
     * UserController constructor.
     */
    public function __construct()
    {
		$user = Auth::user();
        $unread_msg = [];$chat_users = [];

        if(Auth::check())
        {
			$chat_users = Friend::where(['to_user_id' => $user->id, 'status'=>'Y'])->orWhere(['from_user_id' => $user->id, 'status'=>'Y'])->orderBy('created_at', 'desc')->get();
			
            $unread_msg = Chatmessage::where([
                                    ['to_user_id', '=', $user->id],
                                    ['to_user_view_status', '=', 'N']
                                ])
                                ->orderBy('created_at', 'desc')
                                ->get();
        }
		//dd($chat_users->toArray());
        $this->chat_messsage_data = [
            'chat_users' => $chat_users,
            'unread_msg' => $unread_msg
        ];
    }

    /**
     * Activate user account.
     *
     * @param $confirmation_code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate($confirmation_code,$utype)
    {
        if($confirmation_code)
        {
            $user = User::where(['confirmation_code' => $confirmation_code])->first();
            if($user !== null)
            {
                $user->confirmation_code = '';
                $user->status = 'Y';
                $user->save();
                Auth::login($user);
				if($utype==2)
					return redirect()->to('/pricing')->with('activation', 'Your account is now activated!');
				else
					return redirect()->to('/')->with('activation', 'Your account is now activated!');
            }
        }
        return redirect()->to('/')->with('activation', 'Sorry! Unable to process your request.');
    }

    public function account($id=0)
    {
		if($id>0)
		{
			$user = User::find($id);
		}
		else	$user = Auth::user();
		
		$userfollows = array();
		if(!empty(Auth::user()->user_follows))
		{
			foreach(Auth::user()->user_follows as $val)
			{
				$userfollows[] = $val->followed_to;
			}
		}
		$companies = Company::all();
		$jobs = Job::all();
		$universities = University::all();
		$school_majors = SchoolMajor::all();
		$countries = Country::all();
		$all_tropics = AllTropic::all();
		$degrees = \App\Degree::all();
		$frequest = array();
		if(Auth::check())
		{			
			$frequest = \App\Friend::where([
					'from_user_id'=>Auth::user()->id,
					'to_user_id'=>$user->id
					])->first();
		}
        return view('user.profile',compact('user','frequest','countries','jobs','companies','universities','school_majors','all_tropics','degrees','userfollows'))->with('chat_messsage_data',$this->chat_messsage_data);
    }
		
    /**
     * Update details of the user.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateAccount(Request $request)
    {
        $this->validate($request,[
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'user_name' => 'required|max:255|unique:users,user_name,'.Auth::user()->id,
            'email' => 'required|max:255|unique:users,email,'.Auth::user()->id,
            'address' => 'required|max:255',
            'utc_timezone' => 'required',
        ]);
        $input = $request->all();
		//dd($input);
        /* if(isset($input['email'])) {
            unset($input['email']);
        } */
		$time = time();
		
		if($request->hasFile('profile_image')){
            $old_image = 'assets/upload/profile_image/'.Auth::user()->profile_image;
            \File::delete($old_image);
			
            $path   = public_path().'/assets/upload/profile_image/';
            $image  = $request->file('profile_image');
            $save_name = $time.str_random(10).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->save($path . $save_name, 100);
            $input['profile_image'] = $save_name;
        }
		
		if($input['password']!="")
		{
			$this->validate($request, [
                'old_password' => 'required',
                'password' => 'required|confirmed|min:6',
            ]);			
			Auth::user()->update(['password' => bcrypt($request->input('password'))]);
		}
		unset($input['password']);
		
        Auth::user()->fill($input)->save();
        return redirect(url('user/my-details'))
                ->with('success', 'Account details successfully updated.');
    }

    public function changePassword(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('user.change-password');
        }
        elseif ($request->isMethod('post')) {
            $this->validate($request, [
                'old_password' => 'required|old_password',
                'password' => 'required|confirmed|min:6',
            ]);
            Auth::user()->update(['password' => bcrypt($request->input('password'))]);
            return redirect(url('user/account'))
                ->with('success', 'Password successfully updated.');
        }
        return redirect(url('user/account'));
    }		
   
	public function signupStep($step=0)
	{
		if ($step==0) {
			$step=1;
		}
		$companies = Company::all();
		$jobs = Job::all();
		$universities = University::all();
		$school_majors = SchoolMajor::all();
		$countries = Country::all();
		$all_tropics = AllTropic::all();
		$degrees = \App\Degree::all();
		//$user_address = UserAddres::all();
		$user = Auth::user();
		$user_address = $user->user_Addresses;
        return view('user.signup.signup-step'.$step,compact('user','step','countries','companies','jobs','universities','school_majors','all_tropics','user_address','degrees'))->with('chat_messsage_data',$this->chat_messsage_data);
	}
   
	public function signupStepProcess(Request $request)
	{
		$inp_data = $request->all();
		/* dd($inp_data); */
		$user = Auth::user();
		$in_step = $inp_data['in_step'];
		switch($in_step)
		{
			case 1:
			$in_step=$in_step+1;			
			//$user->user_name = $inp_data['user_name'];
			$user->nickname = isset($inp_data['user_name'])? $inp_data['user_name'] :'';
			$name = explode(' ',$inp_data['user_name']);
			$user->first_name = $name[0];
			$user->last_name = isset($name[0])? str_replace($name[0],'',$inp_data['user_name']) :'';
			$user->gender = $inp_data['gender'];
			$user->nationality = $inp_data['nationality'];
			$user->in_step = $in_step;
			
			$inp_data['day'] = ($inp_data['day']!='')?$inp_data['day']:'1';
			$inp_data['month'] = ($inp_data['month']!='')?$inp_data['month']:'1';
			$user->dob = $inp_data['day'].'-'.$inp_data['month'].'-'.$inp_data['year'];
				$today = time();
				$dobtime = strtotime($user->dob);
				$age = round((int)($today - $dobtime)/(86400*365));
			$user->age = $age;
			break;
			
			case 2:
			$in_step=$in_step+1;
			$user->in_step = $in_step;
			break;
			
			case 3:
			$in_step=$in_step+1;
			$user->in_step = $in_step;
			break;
			
			case 4:
			$in_step=$in_step+1;
			$user->in_step = $in_step;
			break;
			
			case 5:
			//$user->in_step = $in_step;
			$in_step=$in_step+1;
			$user->status = "Y";
			break;
			
		}		
		$user->save();
		if($in_step==6)
		return redirect(url('user/account/'));
		else
		return redirect(url('user/signup-step/'.$in_step));
	}
	
	public function addAddress(Request $request)
	{
		$inp_data = $request->all();
		//print_r($inp_data);
		//$fld = $inp_data['fname'];
		//$user->$fld = $inp_data['address'];
		$user = Auth::user();
		$insdata = [
			'user_id' => $user->id,
			'address' => $inp_data['address'],
			'type' => $inp_data['addType'],
		];
		if($inp_data['addType']==1)
		{
			$curr_addr = UserAddres::where(['user_id'=>$user->id,'type'=>$inp_data['addType']])->first();
			if(empty($curr_addr))
			{
				$insdata['is_default'] = 'Y';
			}
		}
		$userAddres = UserAddres::create($insdata);
		return $userAddres;
	}
	
    public function delAddress(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);exit;
		$user = Auth::user();
		if(!empty($input_data))
		{
			$cond=array();
			$cond['id']	= $input_data['id'];
			$cond['user_id']= $user->id;	
			$qdata = UserAddres::where($cond)->delete();	
			if($qdata==1)
			{
					return UserAddres::where(array('is_default'=>'Y','user_id'=>$user->id))->first();
			}
			else	return 0;
		}
	}
	
	public function addWorkExpProcess(Request $request)
	{
		$inp_data = $request->all();
		//print_r($inp_data);exit;
		$user = Auth::user();
		$inp_data['user_id'] = $user->id;
		if(isset($inp_data['wrkexp_id']) && $inp_data['wrkexp_id']!='')
		{
			$wrkexp_id = $inp_data['wrkexp_id']; unset($inp_data['wrkexp_id']);
			$workExperience = WorkExperience::where('id',$wrkexp_id)->update($inp_data);
		}else{
			unset($inp_data['wrkexp_id']);
			$workExperience = WorkExperience::create($inp_data);
		}		
		
		return $workExperience; 
	}
	
    public function delWorkExpProcess(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);exit;
		if(!empty($input_data))
		{
			$cond=array();
			$cond['id']	= $input_data['id'];
			$cond['user_id']= Auth::user()->id;	
			$qdata = WorkExperience::where($cond)->delete();	
			return $qdata;
		}
	}
	public function addEducationProcess(Request $request)
	{
		$inp_data = $request->all();
		//print_r($inp_data);
		$user = Auth::user();
		$inp_data['user_id'] = $user->id;
		
		if(isset($inp_data['edu_id']) && $inp_data['edu_id']!='')
		{
			$edu_id = $inp_data['edu_id']; unset($inp_data['edu_id']);
			$education = Education::where('id',$edu_id)->update($inp_data);
		}else{
			unset($inp_data['edu_id']);
			$education = Education::create($inp_data);
		}
		
		return $education; 
	}
	
    public function delEducationProcess(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);exit;
		if(!empty($input_data))
		{
			$cond=array();
			$cond['id']	= $input_data['id'];
			$cond['user_id']= Auth::user()->id;	
			$qdata = Education::where($cond)->delete();	
			return $qdata;
		}
	}
	
	public function addTropicProcess(Request $request)
	{
		$inp_data = $request->all();
		//print_r($inp_data);exit;
		$user = Auth::user();
		if(!empty($inp_data['ids']))
		{
			Tropic::where('user_id',$user->id)->where('type',$inp_data['type'])->delete();
			$ids = explode(',',trim($inp_data['ids']));
			if(!empty($ids))
			{
				foreach($ids as $id)
				{
					if(!empty($id))
					{
						$tropic = AllTropic::find($id);
						$ins_data['user_id'] = $user->id;
						$ins_data['tropic_id'] = $id;
						$ins_data['group_id'] = $tropic->group_id;
						$ins_data['type'] = $inp_data['type'];
						$user_tropic = Tropic::create($ins_data);						
					}					
				}
			}		
		}
		return $user->tropics; 
	}
	
    public function delTropicProcess(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);exit;
		if(!empty($input_data))
		{
			$cond=array();
			$cond['id']	= $input_data['id'];
			$cond['user_id']= Auth::user()->id;	
			$tpdata=Tropic::where($cond)->first();
			$qdata = Tropic::where($cond)->delete();	
			if($qdata==1)
			{				
				return $tpdata->tropic_id;
			}else{
				return 0;
			}
			
		}
	}
	
    public function userUpdateProcess(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);exit;
		$user_data =array();
		if(!empty($input_data))
		{
			if(isset($input_data['fullname']) && $input_data['fullname']!='')
			{
				$fullname = explode(' ',$input_data['fullname']);
				$user_data = [
					'first_name' => isset($fullname[0])?$fullname[0]:'',
					'last_name' => isset($fullname[0])? str_replace($fullname[0].' ','',$input_data['fullname']) :'',
					];
			}
			
			if(isset($input_data['nickname']) && $input_data['nickname']!='')
			{
				$user_data = [
					//'nickname' => isset($input_data['nickname'])? str_replace(' ','',$input_data['nickname']) :'',
					'nickname' => isset($input_data['nickname'])? $input_data['nickname'] :'',
				];
			}
			if(isset($input_data['gender']) && $input_data['gender']!='')
			{
				$user_data = [
					'gender' => $input_data['gender'],
				];
			}
			if(isset($input_data['year']) )
			{
				$doob  = ($input_data['day']!='')?$input_data['day'].'-':'1'.'-';
				$doob .= ($input_data['month']!='')?$input_data['month'].'-':'1'.'-';
				$doob .= $input_data['year'];
					$today = time();
					$dobtime = strtotime($doob);
					$age = round((int)($today - $dobtime)/(86400*365));
					
				$user_data = [
					'dob' => $doob,
					'age' => $age,
				];
			}
			
			if(isset($input_data['recovery_email']) && $input_data['recovery_email']!='')
			{
				$user_data = [
					'recovery_email' => $input_data['recovery_email'],
				];
			}
			if(isset($input_data['password']) && $input_data['password']!="")
			{
					
				\Validator::extend('pwdvalidation', function($field, $value, $parameters)
				{
					return \Hash::check($value, Auth::user()->password);
				});

				$messages = array('pwdvalidation' => 'The Old Password is Incorrect');
				$this->validate($request, [
					'old_password' => 'required|pwdvalidation',
					'password' => 'required|confirmed|min:6',
				]);	
				
				$user_data = [
					'password' => bcrypt($input_data['password']),
				];				
			}
			
			if(isset($input_data['details']) && $input_data['details']!="")
			{
				$user_data = [
					'details' => $input_data['details'],
				];
			}
			$usrdata = Auth::user()->fill($user_data)->save();	
			$usr = Auth::user();
			$today = time();
			//$dobtime = strtotime($usr->dob);
			//$age = round((int)($today - $dobtime)/(86400*365));
			//$usr->age = $age;
			if(isset($input_data['password']) && $input_data['password']!="")
			{
				$usr->change_pass=1;
			}
			return $usr;
		}
	}
	public function getUserAutocomplete(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);
		$searchdata = User::where('nickname', 'like', $input_data['term'].'%')->limit(20)->get();
		$retun_arr = array();
		if(!empty($searchdata))
		{
			foreach($searchdata as $val)
			{
				$retun_arr['label'][] = $val->nickname;
				$retun_arr['value'][] = $val->id;
			}
		}
		return json_encode($retun_arr);
	}
	public function getCompanyAutocomplete(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data['term']);exit;
		$searchdata = Company::where('title', 'like', '%'.$input_data['term'].'%')->limit(20)->get();
		//dd($searchdata->toArray());
		$retun_arr = array();
		if(!empty($searchdata))
		{
			foreach($searchdata as $val)
			{
				$retun_arr[] = $val->title;
			}
		}
		return json_encode($retun_arr);
	}
	public function getJobAutocomplete(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);
		$searchdata = Job::where('title', 'like', '%'.$input_data['term'].'%')->limit(20)->get();
		$retun_arr = array();
		if(!empty($searchdata))
		{
			foreach($searchdata as $val)
			{
				$retun_arr[] = $val->title;
			}
		}
		return json_encode($retun_arr);
	}
	public function getUniversityAutocomplete(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);
		$searchdata = University::where('title', 'like', '%'.$input_data['term'].'%')->limit(20)->get();
		$retun_arr = array();
		if(!empty($searchdata))
		{
			foreach($searchdata as $val)
			{
				$retun_arr[] = $val->title;
			}
		}
		return json_encode($retun_arr);
	}
	public function getSchoolMajorAutocomplete(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);
		$searchdata = SchoolMajor::where('title', 'like', '%'.$input_data['term'].'%')->limit(20)->get();
		$retun_arr = array();
		if(!empty($searchdata))
		{
			foreach($searchdata as $val)
			{
				$retun_arr[] = $val->title;
			}
		}
		return json_encode($retun_arr);
	}
	
    public function setting()
    {
		$setting = app('settings');
		$current_url= \Request::path();
		$user = Auth::user();
		$all_tropics = AllTropic::all();
        return view('user.setting',compact('user','setting','current_url','all_tropics'))->with('chat_messsage_data',$this->chat_messsage_data);
    }
    public function privacy()
    {
		$setting = app('settings');
		$current_url= \Request::path();
		$user = Auth::user();
		$all_tropics = AllTropic::all();
		$prevdata = \App\PrivacySetting::where('user_id',$user->id)->first();
        return view('user.privacy',compact('user','setting','current_url','prevdata','all_tropics'))->with('chat_messsage_data',$this->chat_messsage_data);
    }
    public function notification()
    {
		$setting = app('settings');
		$current_url= \Request::path();
		$user = Auth::user();
		$all_tropics = AllTropic::all();
		$prevdata = \App\NotificationSetting::where('user_id',$user->id)->first();
        return view('user.notification',compact('user','setting','current_url','prevdata','all_tropics'))->with('chat_messsage_data',$this->chat_messsage_data);
    }
	
	public function updatePrivacySettings(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);
		
		$user = Auth::user();
		$retun_arr = array();
		if(!empty($input_data))
		{
			$ins_data['user_id'] = $user->id;
			$prevdata = \App\PrivacySetting::where('user_id',$user->id)->first();
			$ins_data = [
			'online' => isset($input_data['online'])?$input_data['online']:'N',
			'other_view_account' => isset($input_data['other_view_account'])?$input_data['other_view_account']:'N',
			'want_receive_message' => isset($input_data['want_receive_message'])?$input_data['want_receive_message']:'A',
			'decline_request' => isset($input_data['decline_request'])?$input_data['decline_request']:'N',
			'request_intelval' => isset($input_data['request_intelval'])?$input_data['request_intelval']:'D',
			'max_request' => isset($input_data['max_request'])?$input_data['max_request']:0,
			'allow_comments' => isset($input_data['allow_comments'])?$input_data['allow_comments']:'N',
			'deactivate_account' => isset($input_data['deactivate_account'])?$input_data['deactivate_account']:'N',
			];
			
			if(!empty($prevdata))
			{
				if(isset($ins_data['deactivate_account']))
				{
					$deactivate=($prevdata->deactivate_account=='N')?'Y':'N';
					$ins_data['deactivate_account'] = $deactivate;					
				}
				$updata = \App\PrivacySetting::where('user_id',$user->id)->update($ins_data);
				if($updata == 1)	
					$retun_arr = \App\PrivacySetting::where('user_id',$user->id)->first();
				
			}else{
				$retun_arr = \App\PrivacySetting::create($ins_data);				
			}
			
		}
		return json_encode($retun_arr);
	}
	
	public function updateNotificationSettings(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);
		
		$user = Auth::user();
		$retun_arr = array();
		if(!empty($input_data))
		{
			$prevdata = \App\NotificationSetting::where('user_id',$user->id)->first();
			$ins_data = [
			'user_id' => $user->id,
			'answer_question' => isset($input_data['answer_question'])?$input_data['answer_question']:'N',
			'upvote' => isset($input_data['upvote'])?$input_data['upvote']:'N',
			'new_follower' => isset($input_data['new_follower'])?$input_data['new_follower']:'N',
			'new_nessage' => isset($input_data['new_nessage'])?$input_data['new_nessage']:'N',
			'tag_me' => isset($input_data['tag_me'])?$input_data['tag_me']:'N',
			'comment_my' => isset($input_data['comment_my'])?$input_data['comment_my']:'N',			
			];
			
			if(!empty($prevdata))
			{
				
				$updata = \App\NotificationSetting::where('user_id',$user->id)->update($ins_data);
				if($updata == 1)	
					$retun_arr = \App\NotificationSetting::where('user_id',$user->id)->first();
				
			}else{
				$retun_arr = \App\NotificationSetting::create($ins_data);				
			}
			
		}
		return json_encode($retun_arr);
	}
	
	public function setDefaultAddress(Request $request)
    {
		$input_data = $request->all();
		$user = Auth::user();
		if(isset($input_data['address_id']) && $input_data['address_id']!='')
		{
			UserAddres::where('user_id',$user->id)->update(array('is_default'=>'N'));
			$udata = UserAddres::where('id',$input_data['address_id'])->update(array('is_default'=>'Y'));	
			return UserAddres::where(array('is_default'=>'Y','user_id'=>$user->id))->first();
		}
		else	return 0;
	}	
	
	public function addFavorite(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);
		$user = Auth::user();
		$fav = \App\FavoriteTropic::where(['tropic_id'=>$input_data['tropic_id'],'user_id'=>$user->id])->first();
		//dd($fav);
		if(count($fav)>0)
		{
			$fav->delete();
		}else{
			$fav = \App\FavoriteTropic::create(['tropic_id'=>$input_data['tropic_id'],'user_id'=>$user->id]);
		}
		return $fav;
	}
		
	public function searchUsers(Request $request)
    {
		$input_data = $request->all();
		$list_type=$input_data['list_type'];
		unset($input_data['list_type']);
		$auth_user=Auth::User();
		$user_filter = array();
		$users_data = array();
		
		if(Auth::check() && Auth::user()->filterset_order)	$user_filter = (array) json_decode(Auth::user()->filterset_order->filter_set);
		/* print_r($input_data);exit; */
		$users_arr=array();$uaddr=array();$uaddr2=array();$uaddr3=array();$jobs=array();$education=array();$major=array();$tropics=array();$nicknames=array();$departments=array();$usr_gendr=array();$users_age=array();
		
		//\DB::connection()->enableQueryLog();
		if((isset($input_data['loction']) && $input_data['loction']!='' && in_array('locationlive',$user_filter)) || (isset($input_data['loction']) && $input_data['loction']!='' && empty($user_filter))) 
		{
			$udata = UserAddres::select('user_id')->where('address', 'like', '%'.$input_data['loction'].'%')->where('type',1)->groupBy('user_id')->get();
			$udata = json_decode(json_encode($udata), true);
			$uaddr = array_column($udata,'user_id');		
		}
		
		if((isset($input_data['user_live']) && !empty($input_data['user_live']) && in_array('locationlive',$user_filter)) || (isset($input_data['user_live']) && !empty($input_data['user_live']) && empty($user_filter))) 
		{
			$udata = UserAddres::select('user_id')
					->where(function ($query) use ($input_data) {
						
						foreach($input_data['user_live'] as $val)
						{
							$query->orWhere('address', 'like', '%'.$val.'%');
						}
					})
					->groupBy('user_id')->get();
			$udata = json_decode(json_encode($udata), true);
			$uaddr = array_column($udata,'user_id');		
		}
		
		
		if((isset($input_data['location_visit']) && $input_data['location_visit']!='' && in_array('locationvisit',$user_filter)) || (isset($input_data['location_visit']) && $input_data['location_visit']!='' && empty($user_filter))) 
		{
			$udata = UserAddres::select('user_id')->where('address', 'like', '%'.$input_data['location_visit'].'%')->where('type',2)->groupBy('user_id')->get();
			$udata = json_decode(json_encode($udata), true);
			$uaddr3 = array_column($udata,'user_id');		
		}
		
		if((isset($input_data['joblocation']) && $input_data['joblocation']!='' && in_array('workplace',$user_filter)) || (isset($input_data['joblocation']) && $input_data['joblocation']!='' && empty($user_filter))) 
		{
			$udata = WorkExperience::select('user_id')->where('company', 'like', '%'.$input_data['joblocation'].'%')->groupBy('user_id')->get();
			$udata = json_decode(json_encode($udata), true);
			$uaddr2 = array_column($udata,'user_id');		
		} /* */
		if((isset($input_data['workplace']) && !empty($input_data['workplace']) && in_array('workplace',$user_filter)) || (isset($input_data['workplace']) && !empty($input_data['workplace']) && empty($user_filter))) 
		{
			$udata = WorkExperience::select('user_id')
					->where(function ($query) use ($input_data) {
						
						foreach($input_data['workplace'] as $val)
						{
							$query->orWhere('company', 'like', '%'.$val.'%');
						}
					})
					->groupBy('user_id')->get();
			$udata = json_decode(json_encode($udata), true);
			$uaddr2 = array_column($udata,'user_id');		
		}
		
		if((isset($input_data['job']) && $input_data['job']!='' && in_array('profesion',$user_filter)) || (isset($input_data['job']) && $input_data['job']!='' && empty($user_filter))) 
		{
			$jobs = WorkExperience::select('user_id')->where('title', 'like', '%'.$input_data['job'].'%')->groupBy('user_id')->get();
			$jobs = json_decode(json_encode($jobs), true);
			$jobs = array_column($jobs,'user_id');		
		}
		
		if((isset($input_data['department']) && $input_data['department']!='' && in_array('depatment',$user_filter)) || (isset($input_data['department']) && $input_data['department']!='' && empty($user_filter))) 
		{
			$departments = WorkExperience::select('user_id')->where('department', 'like', '%'.$input_data['department'].'%')->groupBy('user_id')->get();
			$departments = json_decode(json_encode($departments), true);
			$departments = array_column($departments,'user_id');		
		}
		
		if((isset($input_data['education']) && $input_data['education']!='' && in_array('eductn',$user_filter)) || (isset($input_data['education']) && $input_data['education']!='' && empty($user_filter))) 
		{
			$education = Education::select('user_id')->where('institution', 'like', '%'.$input_data['education'].'%')->groupBy('user_id')->get();
			$education = json_decode(json_encode($education), true);
			$education = array_column($education,'user_id');		
		}
		
		if((isset($input_data['major']) && $input_data['major']!='' && in_array('majr',$user_filter)) || (isset($input_data['major']) && $input_data['major']!='' && empty($user_filter))) 
		{
			$major = Education::select('user_id')->where('major', 'like', '%'.$input_data['major'].'%')->groupBy('user_id')->get();
			$major = json_decode(json_encode($major), true);
			$major = array_column($major,'user_id');		
		}
		
		if((isset($input_data['tropicid']) && $input_data['tropicid']!='' && in_array('tpic',$user_filter)) || (isset($input_data['tropicid']) && $input_data['tropicid']!='' && empty($user_filter))) 
		{
			$tropics = Tropic::select('user_id')->where('tropic_id', $input_data['tropicid'])->groupBy('user_id')->get();
			$tropics = json_decode(json_encode($tropics), true);
			$tropics = array_column($tropics,'user_id');		
		}
			
		if(isset($input_data['nickname']) && $input_data['nickname']!='')
		{
			
			$nicknames = User::where('nickname','like','%'.trim($input_data['nickname']).'%')->get();
			$nicknames = json_decode(json_encode($nicknames), true);
			$nicknames = array_column($nicknames,'id');	
		}
		
		if((isset($input_data['gender']) && $input_data['gender']!='' && in_array('gendr',$user_filter)) || (isset($input_data['gender']) && $input_data['gender']!='' && empty($user_filter)))
		{
			/* if(!empty($users_data))
				$users_data = $users_data->where('gender',$input_data['gender']);
			else	$users_data =  */
			$usr_gendr = User::select('id')->where('gender',$input_data['gender'])->get();
			$usr_gendr = json_decode(json_encode($usr_gendr), true);
			$usr_gendr = array_column($usr_gendr,'id');	
		}	
		
		$fage=18;$tage=100;
		if((isset($input_data['age_from']) && $input_data['age_from']!='' && in_array('agee',$user_filter)) || (isset($input_data['age_from']) && $input_data['age_from']!='' && empty($user_filter)))
		{
			$fage=$input_data['age_from'];
			
		}
		if((isset($input_data['age_to']) && $input_data['age_to']!='' && in_array('agee',$user_filter)) || (isset($input_data['age_to']) && $input_data['age_to']!='' && empty($user_filter)))
		{
			$tage=$input_data['age_to'];
		}
		//if((isset($input_data['age_from']) && $input_data['age_from']!='') || (isset($input_data['age_to']) && $input_data['age_to']!=''))
		if((isset($input_data['age_from']) && $input_data['age_from']!='') || (isset($input_data['age_to']) && $input_data['age_to']!=''))
		{
			//if(!empty($users_data))	$users_data = $users_data->where([['age','>=',$fage]])->where([['age','<=',$tage]]);
			$users_age = User::select('id')->where([['age','>=',$fage]])->where([['age','<=',$tage]])->get();	
			$users_age = json_decode(json_encode($users_age), true);
			$users_age = array_column($users_age,'id');				
		}
		//print_r($users_age);exit;
		$filtr = Array ( 'prof' => $jobs,'name' => $nicknames, 'locl' => $uaddr, 'locv' => $uaddr3, 'dpt' => $departments, 'tpic' => $tropics, 'workpl' => $uaddr2, 'eduction' => $education, 'mjor' => $major, 'gnder' => $usr_gendr, 'ag' => $users_age );
		//print_r($user_filter);exit;
		//array_merge//array_intersect
		/* if(!empty($user_filter))
		{
			$flarr=array();
			foreach($user_filter as $key=>$vl)
			{
				if(isset($filtr[$key]))
				{
					foreach($filtr[$key] as $avl)
					{
						$flarr[]=$avl;
					}
				}
			}
			foreach($filtr as $key=>$vl)
			{
				if(!array_key_exists($key,$user_filter))
				{
					foreach($vl as $avl)
					{
						$flarr[]=$avl;
					}					
				}
			}
			
			$users_arr = array_unique($flarr);			
			
		}else $users_arr = array_intersect($uaddr,$uaddr2,$uaddr3,$jobs,$education,$major,$tropics,$departments);*/	
		
		if(!empty($filtr))
		{
			$flag=0;
			foreach($input_data as $arr)
			{
				if(!empty($arr) )	$flag++;
			}
			if($input_data['age_from']!='' && $input_data['age_to']!='' && $flag==2)	$flag = 1;
			$u_arr = array_merge($nicknames,$uaddr,$uaddr2,$uaddr3,$jobs,$education,$major,$tropics,$departments,$usr_gendr,$users_age);
			
			if($flag == 1)
			{
				$users_arr = $u_arr;
			}else{
				/* foreach(array_count_values($u_arr) as $val => $c)
				{
					if($c > 1) $users_arr[] = $val;	
				} */
				$nwArr=array(); $chck=0;
				foreach($filtr as $val)
				{
					if(!empty($val))
					{
						$nwArr[] = $val;
						$chck++;
					}
				}
				if( $chck > 0) $users_arr = call_user_func_array('array_intersect',$nwArr);
			}
			   		
		}	
		/* echo $flag;
		print_r($users_arr);exit; */
		if(!empty($users_arr)){
			/* if(!empty($users_data))
			{
				 $users_data = $users_data->whereIn('id',$users_arr);
			}else{} */
				$users_data = User::whereIn('id',$users_arr);
						
		}		
		
		
		if($auth_user && !empty($users_data))	$users_data = $users_data->where('id','<>',$auth_user->id)->where('status','Y');
		
		//if(empty($users_arr))	$users_data = User::where('status','Y');
		/* if(!empty($users_arr)){
			$ordusr=implode(',',$users_arr);
			$users_data->orderBy(DB::raw('FIELD(id, '.$ordusr.')'));
		} */
		

		/* print_r($users_data->toSql());	exit;  */
		if(!empty($users_data))	$users_data = $users_data->get();
		
		/* $query = \DB::getQueryLog();
		
		dd($query); */
		/*print_r($users_data->toArray()); */
		$ajax_html = view('include.peoplebox',compact('users_data'))->render();
		return $return_data = ['has_error' => 0, 'list_type' => $list_type,'match_count' => count($users_data), 'ajax_html' => $ajax_html];
		
	}
	
	public function showAdvanceSearch()
	{
		return  view('include.advance_search')->render();
	}
	public function searchPeople()
	{
		$all_tropics = AllTropic::all();
		return  view('search-people',compact('all_tropics'))->with('chat_messsage_data',$this->chat_messsage_data);
	}
		
	public function postQuestion(Request $request)
	{
		$input_data = $request->all();
		//print_r($input_data);exit;
		unset($input_data['frm_type']);
		$note = isset($input_data['note'])?$input_data['note']:''; unset($input_data['note']);
		$users_to = isset($input_data['users_to'])?$input_data['users_to']:''; unset($input_data['users_to']);
		$time = time(); $post_tags = array();
		if(isset($input_data['tags']) && !empty($input_data['tags']))
		{
			$post_tags = $input_data['tags'];
			unset($input_data['tags']);
		}
		$auth_user = Auth::User();
		
		$input_data['user_id'] = $auth_user->id;
		if(isset($input_data['_token']))	unset($input_data['_token']);
		
		//echo "<pre>";print_r($input_data);exit;	
			$is_forword = $input_data['is_forword'];
			unset($input_data['is_forword']);
			if($request->hasFile('question_attached')){
				/* $old_image = 'assets/upload/question_attached/'.$question->attached;
				\File::delete($old_image); */
				
				$path   = public_path().'/assets/upload/question_attached/';
				$image  = $request->file('question_attached');
				$save_name = $time.str_random(10).'.'.$image->getClientOriginalExtension();
				//Image::make($image->getRealPath())->save($path . $save_name, 100);
				
				$image->move($path, $save_name);
				
				$input_data['question_attached'] = $save_name;
			}else{
				unset($input_data['question_attached']);
			}
			if(!empty($input_data['qsn_id']))
			{
				$qid = $input_data['qsn_id'];	
					unset($input_data['qsn_id']);
				$question = AskQuestion::where('id',$qid)->update($input_data); 
			}
			else{
				unset($input_data['qsn_id']);
				$question = AskQuestion::create($input_data);
				$qid = $question->id;
			}
			
			if(isset($question) && !empty($post_tags))
			{
				/* echo $qid;
				dd($post_tags); */
				foreach($post_tags as $val)
				{
					$preven = TropicQuestion::where(['tropic_id'=>$val,'question_id'=>$qid])->first();
					if(empty($preven))
					{						
						$put_data['tropic_id']	=	$val;
						$put_data['question_id']=	$qid;
						$put_data['user_id']	=	Auth::user()->id;
						TropicQuestion::create($put_data);
					}
					$trpic = AllTropic::find($val);
					if(!empty($trpic->favorite_tropic))
					{
						foreach($trpic->favorite_tropic as $tval)
						{
							$ins_data['question_id'] = $qid;
							$ins_data['from_user'] = Auth::user()->id;						
							$ins_data['to_user'] = $tval->user_id;
							$ins_data['type'] = 'T';	
							$qfwrd = QuestionForward::where($ins_data)->delete();
							
							$quefwrd = QuestionForward::create($ins_data);
							
						}
					}
				}
			}
			//echo "<pre>";print_r($question);exit;
						/* 
			if(!empty($input_data['to']))
			{
				$to_arr = explode(',',$input_data['to']); */
			if(!empty($users_to))
			{
				$to_arr = explode(',',$users_to);
				if(!empty($to_arr))
				{
					foreach($to_arr as $val)
					{	
						if($val!='')
						{
							$to_usr = User::select('id')->where('email',$val)->orWhere('nickname',$val)->first();
							if(!empty($to_usr))
							{	
							$ins_data['question_id'] = $qid;
							$ins_data['from_user'] = Auth::user()->id;						
							$ins_data['to_user'] = $to_usr->id;
							$ins_data['type'] = (isset($is_forword) && $is_forword==1)?'F':'Q';	
							$qfwrd = QuestionForward::where($ins_data)->delete();
							$ins_data['note'] = $note;
							$question = QuestionForward::create($ins_data);
							}
						}
					}			
				}else{
					$to_usr = User::select('id')->where('email',$users_to)->orWhere('user_name',$users_to)->first();
						if(!empty($to_usr))
						{	
							$ins_data['question_id'] = $qid;
							$ins_data['from_user'] = Auth::user()->id;						
							$ins_data['to_user'] = $to_usr->id;
							$ins_data['type'] = (isset($is_forword) && $is_forword==1)?'F':'Q';		
							$qfwrd = QuestionForward::where($ins_data)->delete();
							$ins_data['note'] = $note;
							$question = QuestionForward::create($ins_data);
						}
				}
			}
			$msg = (isset($is_forword) && $is_forword==1)?'Question successfully forwarded.':'Question post successfull.';		
			/* return redirect()->back()->with('askqsn_success', $msg); */
			return  $msg;
		
		
		
	}
	
	public function addContactinfo(Request $request)
	{
		$input_data = $request->all();
		//print_r($input_data);
		$user = Auth::user();
		$return_data = 0;
		if(isset($input_data['info_id']) && $input_data['info_id']!='')
		{
			$info_id = $input_data['info_id']; unset($input_data['info_id']);
			$res = \App\ContactIfno::where('id',$info_id)->update($input_data); 
		}
		else{
			$input_data['user_id'] = $user->id;
			$res = \App\ContactIfno::create($input_data);
			
		}
		$return_data = view('include.user_contact_info',compact('user'))->render();
		return $return_data;
	}
	
	public function deleteContactinfo(Request $request)
	{
		$input_data = $request->all();
		//print_r($input_data);
		$user = Auth::user();
		if(isset($input_data['info_id']) && $input_data['info_id']!='')
		{
			$res= \App\ContactIfno::where('id',$input_data['info_id'])->delete();
		}
		return $res;
	}
	
	public function setFilter(Request $request)
	{
		$input_data = $request->all();
		//print_r($input_data);
		unset($input_data['styp']);
		$user = Auth::user();
		$prev_data = FilterOrderSet::where('user_id',$user->id)->first();
		if(!empty($prev_data))
		{
			$prev_data->filter_set= json_encode($input_data);
			$prev_data->save();
		}else{
			$setdata = FilterOrderSet::create(['user_id'=> $user->id,'filter_set'=>json_encode($input_data)]);			
		}
		return 1;
	}
	public function question_home(Request $request)
	{
		$user_data = Auth::user(); $userfollows = array();
		$pass_arr = array();
		$confirm_pass_arr = array();
		if(!empty($user_data->pass_qsns))
		{
			foreach($user_data->pass_qsns as $val)
			{
				$pass_arr[] = $val->question_id;
				
				if($val->status == 'Y')	$confirm_pass_arr[] = $val->question_id;
				
			}
		}
		$q_data=AskQuestion::where('id','<>',0)->orderBy('id','desc')->get();
		//dd($q_data->toArray());
		$quest_data=array();
		/* if(!empty($q_data))
		{
			foreach($q_data as $one_quest)
			{
				if($one_quest->to!='')
				{
					if(in_array($user_data->id,explode(',',$one_quest->to)))
					{
						$quest_data[]=$one_quest;
					}
				}
			}
		} */
		
		$userfollows = array();
		if(!empty($user_data->question_follows))
		{
			foreach($user_data->question_follows as $val)
			{
				$userfollows[] = $val->question_id;
			}
		}
		$forwards_qsn = array();
		if(!empty($user_data->touser_forwards))
		{
			foreach($user_data->touser_forwards as $frwd)
			{
				$forwards_qsn[] = $frwd->question_id;
			}
		}
		
		/* $q2_data=AskQuestion::whereIn('id',$userfollows)->orWhereIn('id',$forwards_qsn)->orderBy('id','desc')->get();
		if(!empty($q2_data))
		{
			foreach($q2_data as $one_quest)
			{
				$quest_data[]=$one_quest;
			}
		} */
		/* $collection = AskQuestion::where('to', 'like', '%,' . $user_data->id . ',%')->orWhere('to', 'like', '%' . $user_data->id . ',%')->get();
		
		$other_collection = AskQuestion::WhereIn('id',$userfollows)->get();
		$other_collection2 = AskQuestion::WhereIn('id',$forwards_qsn)->orderBy('id','desc')->get();
		
		$quest_data= $collection->merge($other_collection);
		$quest_data= $quest_data->merge($other_collection2);*/
		//dd($quest_data); 
		return view('dashboard.dashboard_home',compact('user_data','pass_arr','confirm_pass_arr','quest_data','userfollows','forwards_qsn'))->with('chat_messsage_data',$this->chat_messsage_data);
	}
	public function user_sent_question(Request $request)
	{
		$user_data = Auth::user();

		$quest_data=AskQuestion::where(['user_id'=>$user_data->id])->orderBy('id','desc')->get();
		//echo json_encode($quest_data);exit;
		return view('user.sent_question',compact('user_data','quest_data'))->with('chat_messsage_data',$this->chat_messsage_data);
	}
	public function user_received_question(request $request)
	{
		$user_data=Auth::user();
		$quest_data=AskQuestion::where('id','<>',0)->orderBy('id','desc');
		//dd($user_data->pass_qsns->toArray());
		$pass_arr = array();
		$confirm_pass_arr = array();
		if(!empty($user_data->pass_qsns))
		{
			foreach($user_data->pass_qsns as $val)
			{
				$pass_arr[] = $val->question_id;
				
				if($val->status == 'Y')	$confirm_pass_arr[] = $val->question_id;
				
			}
		}
		$forwards_qsn = array();
		if(!empty($user_data->touser_forwards))
		{
			foreach($user_data->touser_forwards as $frwd)
			{
				$forwards_qsn[] = $frwd->question_id;
			}
		}
		
		
		$quest_data = $quest_data->get();
		/* 
		$userfollows = array();
		if(!empty($user_data->user_follows))
		{
			foreach($user_data->user_follows as $val)
			{
				$userfollows[] = $val->followed_to;
			}
		} */
		$userfollows = array();
		if(!empty($user_data->question_follows))
		{
			foreach($user_data->question_follows as $val)
			{
				$userfollows[] = $val->question_id;
			}
		}
		
		/* if(!empty($user_data->user_follows))	$userfollows = array_column((array)$user_data->user_follows,'followed_to'); */
		//dd($userfollows);
		$selected_quest=array();
		foreach($quest_data as $one_quest)
		{
			if($one_quest->to!='')
			{
				if(in_array($user_data->id,explode(',',$one_quest->to)))
				{
					$selected_quest[]=$one_quest;
				}
			}
		}
		foreach($quest_data as $one_quest)
		{
			if($one_quest->to!='')
			{
				if(in_array($one_quest->id,$forwards_qsn))
				{
					$selected_quest[]=$one_quest;
				}
			}
		}

		return view('user.received_question',compact('user_data','pass_arr','confirm_pass_arr','selected_quest','userfollows','forwards_qsn'))->with('chat_messsage_data',$this->chat_messsage_data);
	}
	
	public function questionDetails($id=0)
	{
		$user=Auth::user();
		$question = AskQuestion::where('id',$id)
					->with('answers.upvotes')
					->first();
		//dd($question);
		$ip = $_SERVER["REMOTE_ADDR"];
		$qsnView = \App\QuestionView::where(['ip'=>$ip,'question_id'=>$id])->first(); 
		if(empty($qsnView))
		{
			QuestionView::create(['ip'=>$ip,'question_id'=>$id]);
		}
		return view('user.question_details',compact('question','user'))->with('chat_messsage_data',$this->chat_messsage_data);
	}
	
	public function answerProcess(request $request)
	{
		$input_data = $request->all();
		//dd($input_data);
		if(!empty($input_data['content']))	
		{		
			$user=Auth::user();	
			if(!empty($input_data['ans_id']))	
			{
				$answer=Answer::find($input_data['ans_id']);
				$answer->content = $input_data['content'];
				$answer->save();
			}else	
			{				
				$input_data['user_id'] = $user->id;
				$answer = Answer::create($input_data);
				
				$question = AskQuestion::find($input_data['question_id']);
				if(!empty($question))
				{
					$ins_data['question_id'] = $question->id;
					$ins_data['from_user'] =  $user->id;					
					$ins_data['to_user'] = $question->user_id;
					$ins_data['type'] = 'A';						
					$question_a = QuestionForward::create($ins_data);					
				}
			}
		}
		//dd($answer);
		return redirect()->back()->with('message', 'Answer added successfull!');
	}
	
	public function getQuestionDetails(request $request)
	{
		$user=Auth::user();
		$input_data = $request->all();
		$question = AskQuestion::find($input_data['qid']);
		//dd($question->toArray());
		$response = array();
		$response['question'] = $question;$response['tos'] = '';$response['tagslist']='';
		$toUsers = User::select('email')->whereIn('id',explode(',',$question->to))->get();
		if(!empty($toUsers))
		{
			foreach($toUsers as $val){
				$response['tos'] .= $val->email.',';
			}
		}
		if(isset($question->question_tropics))
		{
			foreach($question->question_tropics as $trpc)
			{
				$response['tagslist'] .= '<span>'.$trpc->tropic->title.'</span>,';
			}
		}
		//print_r($response);exit;
		
		return json_encode($response);
	}
	public function deleteQuestion(request $request)
	{		
		$input_data = $request->all();
		//print_r($input_data);exit;
		$question = AskQuestion::find($input_data['qid']);
		$res = $question->delete();
		return json_encode($res);
	}
	public function followUnfollow(request $request)
	{		
		$input_data = $request->all();
		//print_r($input_data);exit;
		$has_error = 1;
		/* $question = AskQuestion::find($input_data['qid']);
		$res = $question->delete(); */
		$user = Auth::User();
		if(isset($input_data['followto']) && $input_data['followto']!='' && $user)
		{
			$follow = Follow::where(['followed_to'=>$input_data['followto'],'followed_by'=>$user->id])->first();
			if(!empty($follow))
			{
				$follow->delete();
				$has_error = 2;				
			}else{
				$follow = new Follow;
				$follow->followed_to = $input_data['followto'];
				$follow->followed_by = $user->id;
				/* print_r($block);exit; */
				$follow->save();
				$has_error = 0;
			}
		}
		
		return $has_error;
	}
	public function qsnFollowUnfollow(request $request)
	{		
		$input_data = $request->all();
		//print_r($input_data);exit;
		$has_error = 1;
		/* $res = $question->delete(); */
		$user = Auth::User();
		if(isset($input_data['qsnfollow']) && $input_data['qsnfollow']!='' && $user)
		{
			$question = AskQuestion::find($input_data['qsnfollow']);
			$follow = QuestionFollow::where(['question_id'=>$input_data['qsnfollow'],'followed_by'=>$user->id])->first();
			$frwd_question = QuestionForward::where(['question_id'=>$input_data['qsnfollow'],'to_user'=>$user->id,'type'=>'FO'])->first();
			if(!empty($follow) )
			{
				$follow->delete();
				if(!empty($frwd_question))
					$frwd_question->delete();
				$has_error = 2;				
			}else{
				$follow = new QuestionFollow;
				$follow->question_id = $input_data['qsnfollow'];
				$follow->followed_by = $user->id;
				/* print_r($block);exit; */
				$follow->save();
				
				$ins_data['question_id'] = $question->id;
				$ins_data['from_user'] =  $question->user_id;					
				$ins_data['to_user'] = $user->id;
				$ins_data['type'] = 'FO';						
				$question_f = QuestionForward::create($ins_data);
				
				$has_error = 0;
			}
		}
		
		return $has_error;
	}
	
	public function commentProcess(request $request)
	{
		$input_data = $request->all();
		/* dd($input_data); */
		$has_error = 1; $res = array();
		$user=Auth::user();
		if($user && $input_data['comment']!='')
		{			
			$input_data['user_id'] = $user->id;
			$comment = AnswerComment::create($input_data);
			if(!empty($comment))
			{
				$res['created_at'] = time_elapsed_string(strtotime($comment->created_at));
				$res['nickname'] = $comment->comuser->nickname;
			}
			$res['comment'] = $comment;
			$has_error = 0;	
		}
		$res['has_error'] = $has_error;
		//dd($answer);
		return json_encode($res);
	}
	
	public function upvoteDownvoteProcess(request $request)
	{
		$input_data = $request->all();
		$user=Auth::user(); $res = array();
		/* dd($input_data); */
		if($input_data['ans_id'] && $user)
		{
			$answer = Answer::find($input_data['ans_id']);
			if($user->id != $answer->user_id)
			{
				
				$vote = UpvoteDownvote::where(['answer_id'=>$input_data['ans_id'],'user_id'=>$user->id])->first();
				if(!empty($vote))
				{
					$vote->status = $input_data['status'];
					$vote->save();
				}else{					
					$ins_data['answer_id'] = $input_data['ans_id'];
					$ins_data['user_id'] = $user->id;
					$ins_data['status'] = $input_data['status'];			
					$vote = UpvoteDownvote::create($ins_data);
				}
			}
			$res['upvotes'] = count($answer->upvotes);
			$res['downvotes'] = count($answer->downvotes);
		}
		return json_encode($res);
	}
	
	public function showFriendRequest()
	{
		$user=Auth::user();
		
		return view('user.friend_request',compact('user'))->with('chat_messsage_data',$this->chat_messsage_data);
	}
	
	public function sendFriendRequest(request $request)
	{
		$user=Auth::user();
		$input_data = $request->all();
		//print_r($input_data);
		$send_req = 0;
		if(!empty($input_data))
		{
			foreach($input_data as $touser)
			{				
				$ins_data['from_user_id'] = $user->id;
				$ins_data['to_user_id'] = $touser;	
				$friend = Friend::where($ins_data)->first();
				//dd($friend);
				if(!empty($friend))
				{	
					$friend->delete();
					$send_req = 2;	
				}	
				else
				{					
					if($user->id != $touser)
					{
						$friend = Friend::create($ins_data);
						$send_req = 1;						
					}
				}	
			}		
		}		
		return $send_req;
	}
	
	public function acceptFriendRequest(request $request)
	{
		$user=Auth::user();
		$input_data = $request->all();
		$has_error = 1;
		if(isset($input_data['frnd_id']) && $input_data['frnd_id']!="")
		{				
			$ins_data['from_user_id'] = $input_data['frnd_id'];
			$ins_data['to_user_id'] = $user->id;	
			$ins_data['status'] = 'N';	
			$friend = Friend::where($ins_data)->first();
			if(!empty($friend))
			{					
				$friend->status = "Y";
				$friend->save();
				$has_error = 0;
			}		
		}		
		return $has_error;
	}
	public function deleteFriendRequest(request $request)
	{
		$user=Auth::user();
		$input_data = $request->all();
		$has_error = 1;
		if(isset($input_data['frnd_id']) && $input_data['frnd_id']!="")
		{				
			$ins_data['from_user_id'] = $input_data['frnd_id'];
			$ins_data['to_user_id'] = $user->id;
			$friend = Friend::where($ins_data)->first();
			if(!empty($friend))
			{					
				$friend->delete();
				$has_error = 0;
			}		
		}		
		return $has_error;
	}
	
	public function showFriends()
	{
		$user=Auth::user();
		$frnd_ids = array();
		$fav_frnds = array();
		$friends = Friend::where('status','Y')
					->where(function($query)use($user)
					{
						$query->where('from_user_id',$user->id)
							  ->orWhere('to_user_id',$user->id);
					})
					->get();
		
		if(!empty($friends))
		{
			foreach($friends as $val)
			{
				
				if($val->from_user_id != $user->id)	$frnd_ids[]=$val->from_user_id;
				if($val->to_user_id != $user->id)		$frnd_ids[]=$val->to_user_id; /* */
			}
		}
		if(!empty($user->fav_friends))
		{
			foreach($user->fav_friends as $fav)
			{
				$fav_frnds[] = $fav->friend_id;
			}
		}
		$user_friends = User::where('status','Y')->whereIn('id',$frnd_ids)->get();
		return view('user.friends',compact('user','user_friends','fav_frnds'))->with('chat_messsage_data',$this->chat_messsage_data);
	}
	public function showFollowers()
	{
		$user=Auth::user();
		
		return view('user.followers',compact('user'))->with('chat_messsage_data',$this->chat_messsage_data);
	}
	
	public function blogList($cat=0)
	{
		$user=Auth::user();
		$catagory = Category::find($cat); 
		return view('read.blog-list',compact('user','catagory'))->with('chat_messsage_data',$this->chat_messsage_data);
	}
	
	public function adminBlogList($cat=0)
	{
		$user=Auth::user();
		$blogs = Blog::where(['user_id'=>0])->orWhere(['cat_id'=>0])->get(); 
		return view('read.admin-blog-list',compact('user','blogs'))->with('chat_messsage_data',$this->chat_messsage_data);
	}
	
	public function blogDetails($id=0)
	{
		$user=Auth::user();
		$ip = $_SERVER["REMOTE_ADDR"];
		$blogView = \App\BlogView::where(['ip'=>$ip,'blog_id'=>$id])->first(); 
		if(empty($blogView))
		{
			BlogView::create(['ip'=>$ip,'blog_id'=>$id]);
		}
		$blog = Blog::find($id); 
		return view('read.blog-details',compact('user','blog'))->with('chat_messsage_data',$this->chat_messsage_data);
	}
	
	public function blogCmmentProcess(request $request)
	{
		$input_data = $request->all();
		/* dd($input_data); */
		$has_error = 1; $res = array();
		$user=Auth::user();
		if($user && $input_data['comment']!='')
		{			
			$input_data['user_id'] = $user->id;
			$comment = \App\BlogComment::create($input_data);
			if(!empty($comment))
			{
				$res['created_at'] = time_elapsed_string(strtotime($comment->created_at));
				$res['nickname'] = $comment->comment_user->nickname;
				$res['replyCount'] = count($comment->replyComments);
			}
			$res['comment'] = $comment;
			$has_error = 0;	
		}
		$res['has_error'] = $has_error;
		//dd($answer);
		return json_encode($res);
	}
	
	public function myBlogs()
	{
		$user=Auth::user();
		$catagories = Category::orderBy('title','ASC')->get(); 
		return view('read.add-blog',compact('user','catagories'))->with('chat_messsage_data',$this->chat_messsage_data);
	}
	
	public function blogDeleteProcess(request $request)
	{
		$inp_data=$request->all();
		$blog =  Blog::find($inp_data['blog_id']);
		if(!empty($blog))
		{			
			$old_image = 'assets/upload/blog_image/'.$blog->blog_image;
			if(file_exists($old_image)){
				\File::delete($old_image);
			}
			if($blog->delete())
			{
				return 1;
			}
			else	return 0;
		}else	return 0;
	}
	public function getBlog(request $request)
	{
		$inp_data=$request->all();
		$blog =  Blog::find($inp_data['blog_id']);
		if(!empty($blog))
		{		
			return $blog;
		}else	return 0;
	}
	public function blogPost(request $request)
	{
		$this->validate($request,[
            'title' => 'required|max:255',
            //'cat_id' => 'required',
            'description' => 'required',
			
        ]);
		$user=Auth::user();
		$time = time();
		$inp_data=$request->all();
		if($inp_data['blog_id']!=0)
		{
			$blog = Blog::find($inp_data['blog_id']);
			unset($inp_data['blog_id']);
		}
		
		$inp_data['user_id'] = $user->id;
		if($request->hasFile('blog_image'))
		{
			if(isset($blog))
			{
				$old_image = 'assets/upload/blog_image/'.$blog->blog_image;
				\File::delete($old_image);				
			}

            $path   = public_path().'/assets/upload/blog_image/';
            $image  = $request->file('blog_image');
            $save_name = $time.str_random(10).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->save($path . $save_name, 100);
            $inp_data['blog_image'] = $save_name;
        }
		if(isset($blog))
		{
			$blog->fill($inp_data)->save();
		}
		else	Blog::create($inp_data);
		
		return redirect()->back()->with('success','Blog post successfull!!');
	}
	public function blogCatagories()
	{
		$user=Auth::user(); $fav_arr = array();
		$catagories = Category::orderBy('title','ASC')->get(); 
		//dd($user->fav_categories->toArray());
		if(count($user->fav_categories)>0)
		{
			foreach($user->fav_categories as $val)
			{
				$fav_arr[]= $val->cat_id;
			}
		}
				
		//dd($fav_arr);
		//dd($catagories->toArray());
		return view('read.catagories',compact('user','catagories','fav_arr'))->with('chat_messsage_data',$this->chat_messsage_data);
	}
	
	public function addFavCat(request $request)
	{
		$input_data = $request->all();
		//print_r($input_data);
		$user = Auth::user(); $res_arr=array();
		$fav = \App\FavoriteCategory::where(['cat_id'=>$input_data['cat_id'],'user_id'=>$user->id])->first();
		//dd($fav);
		if(count($fav)>0)
		{
			$fav->delete();
			$res_arr['msg']=0;
		}else{
			$fav = \App\FavoriteCategory::create(['cat_id'=>$input_data['cat_id'],'user_id'=>$user->id]);
			if(!empty($fav))
			{				
				$cat= Category::find($fav->cat_id);
				$res_arr['msg']=1;
				$res_arr['favHtml']='<li class="fav" id="cat_'.$cat->id.'">
						<div class="catThumb">
							<figure><a href="'.url('user/blog-list/'.$cat->id).'"><img src="'.asset('assets/upload/category_icon/'.$cat->icon).'" alt=""></a></figure>
							<a href="javascript:void(0);" class="str addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="'.$cat->id.'"/></a>
						</div>
						<span><a href="'.url('user/blog-list/'.$cat->id).'">'.$cat->title.'</a></span>
					</li>';
			}
			
		}
		return json_encode($res_arr);
	}
	
    public function sendRemindMessage(Request $request)
    {
        $setting = app('settings');
        $input_data = $request->all();
		//dd($input_data);
        $user_det = Auth::user();
		if($user_det && $input_data['qsn_id']!='')
		{			
			
			$questn = AskQuestion::where('id',$input_data['qsn_id'])->first();
			if(!empty($questn->all_forwards))
			{
				foreach($questn->all_forwards as $val)
				{
					$to_user_det = User::where('id',$val->to_user)->first();
				
					$message = 'Hi, I really need to get helps on this question <a href="'.url("user/question-details/".$questn->id).'">'.$questn->title.'</a>. <br/> Could you help me now?';
					$chat_msg = new Chatmessage;
					$chat_msg->from_user_id = $val->from_user;
					$chat_msg->to_user_id = $val->to_user;
					//$chat_msg->from_user_country_id = $user_det->country_id;
					$chat_msg->to_user_country_id = $to_user_det->country_id;
					//$chat_msg->from_user_type = $user_det->user_type;
					$chat_msg->to_user_type = $to_user_det->user_type;
					$chat_msg->message = $message;
					$chat_msg->from_user_view_status = 'S';
					$chat_msg->to_user_view_status = 'N';
					$chat_msg->type = $input_data['type'];
					$chat_msg->save();
				}
				
				$return_data = ['has_error' => 0];
			}			

           /*  $chat_msg_arr[count($chat_msg_arr)] = $chat_msg;
            $this->chat_messsage_data['chat_msg_arr'] = $chat_msg_arr;
            $this->chat_messsage_data['to_user_id'] = $input_data['to_user_id'];
            $this->chat_messsage_data['from_user_id'] = $input_data['from_user_id'];
            $ajax_msg_html = view('chat.ajax_chat_messages')->with('chat_messsage_data',$this->chat_messsage_data)->render(); */
		}
			
        return response()->json($return_data);
    }
	
	
    public function passQuestion(Request $request)
    {
        $input_data = $request->all();
		//dd($input_data);
		$has_error=1;
        $user_det = Auth::user();
		if($user_det && $input_data['question_id']!='')
		{	
			$input_data['user_id'] = $user_det->id;
			$old_data = PassQuestion::where(['question_id'=>$input_data['question_id'],'user_id'=>$input_data['user_id']])->first();
			if(empty($old_data))
			{				
				$pass= PassQuestion::create($input_data);
				if($pass)	$has_error=0;
			}
				
		}
		return response()->json(['has_error'=>$has_error]);
	}
	
	
    public function confirmPassQuestion()
    {
		PassQuestion::where('created_at','<=',Carbon::now()->subDays(7))->update(['status' => 'Y']);
	}
		
	public function commentLikeProcess(request $request)
	{		
		$input_data = $request->all();
		//print_r($input_data);exit;
		$has_error = 1;
		
		$user = Auth::User();
		$res = 0;
		if(isset($input_data['cmnt_id']) && $input_data['cmnt_id']!='' && $user)
		{
			$answerComment = AnswerComment::find($input_data['cmnt_id']);
			if(!empty($answerComment))
			{
				if($answerComment->user_id != $user->id)
				{
					$answerComment->like_count = $answerComment->like_count+1;
					$answerComment->save();					
				}
				$res = $answerComment->like_count;				
			}
		}
		
		return $res;
	}
	
	public function addFavFriend(request $request)
	{
		$input_data = $request->all();
		//print_r($input_data);
		$user = Auth::user(); $res_arr=array();
		$fav = \App\FavFriend::where(['friend_id'=>$input_data['friend_id'],'user_id'=>$user->id])->first();
		//dd($fav);
		if(count($fav)>0)
		{
			$fav->delete();
			$res_arr['msg']=0;
		}else{
			$fav = \App\FavFriend::create(['friend_id'=>$input_data['friend_id'],'user_id'=>$user->id]);
			if(!empty($fav))
			{				
				$cat= User::find($fav->friend_id);
				$res_arr['msg']=1;
				/* $res_arr['favHtml']='<li class="fav" id="cat_'.$cat->id.'">
						<div class="catThumb">
							<figure><a href="'.url('user/blog-list/'.$cat->id).'"><img src="'.asset('assets/upload/category_icon/'.$cat->icon).'" alt=""></a></figure>
							<a href="javascript:void(0);" class="str addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="'.$cat->id.'"/></a>
						</div>
						<span><a href="'.url('user/blog-list/'.$cat->id).'">'.$cat->title.'</a></span>
					</li>'; */
			}
			
		}
		return json_encode($res_arr);
	}
	public function searchFriends(request $request)
	{
		$input_data = $request->all();
		$user = Auth::user(); $res_arr=array();
		if(isset($input_data['keywrd']))
		{
			$frnd_ids = array();
			$fav_frnds = array();
			$friends = Friend::where('status','Y')
						->where(function($query)use($user)
						{
							$query->where('from_user_id',$user->id)
								  ->orWhere('to_user_id',$user->id);
						})
						->get();
			
			if(!empty($friends))
			{
				foreach($friends as $val)
				{
					
					if($val->from_user_id != $user->id)	$frnd_ids[]=$val->from_user_id;
					if($val->to_user_id != $user->id)		$frnd_ids[]=$val->to_user_id;
				}
			}
			if(!empty($user->fav_friends))
			{
				foreach($user->fav_friends as $fav)
				{
					$fav_frnds[] = $fav->friend_id;
				}
			}		
			//dd($fav_frnds);
			if($input_data['tab']==2)
			{
				if(!empty($input_data['keywrd']))
				{
					$user_friends = User::where('nickname', 'like', '%'.$input_data['keywrd'].'%')->where('status','Y')->whereIn('id',$fav_frnds)->get();
				}else{
					$user_friends = User::where('status','Y')->whereIn('id',$fav_frnds)->get();
				}
			}else{
				if(!empty($input_data['keywrd']))
				{
					
				$user_friends = User::where('nickname', 'like', '%'.$input_data['keywrd'].'%')->where('status','Y')->whereIn('id',$frnd_ids)->get();
				}else{
					$user_friends = User::where('status','Y')->whereIn('id',$frnd_ids)->get();
				}
			}
			
			
			//dd($user_friends->toArray());
			if(count($user_friends)>0)
			{
				
				$res_arr['tabNo']=$input_data['tab'];
				$res_arr['msg']=1;
				$res_arr['frndHtml']='';
				foreach($user_friends as $frndreq)
				{
					$res_arr['frndHtml'].='<div class="peopleBox">
							<div class="listWrap">
								<div class="topCon clear">
									<div class="conThumb">
										<figure>
											<a href="'.url('profile/'.$frndreq->id).'" target="_blank" class="btnText"><img src="'.asset("assets/frontend/images/profile.jpg").'" width="100" height="100" alt=""></a>
										</figure>
										<span class="ageCnt"><i class="fa '.($frndreq->gender=='M'?'fa-mars':'fa-venus').'" aria-hidden="true"></i>'.$frndreq->age.' yrs</span>
									</div>
								</div>
								<div class="peopleDesc">
									<div class="vMiddle">
										<div class="MiddleText">
											<a href="javascript:;" class="CStar '.(in_array($frndreq->id,$fav_frnds)?'adStar':'').'" fid="'.$frndreq->id.'"  id="cat_'.$frndreq->id.'">
												<i class="fa fa-star" aria-hidden="true"></i>
											</a>
											<h4><a href="'.url('profile/'.$frndreq->id).'" target="_blank">'.$frndreq->nickname.'</a><span class="avlTxt online"><samp>available</samp></span></h4>
											<p>';
											$we = $frndreq->work_experiences()->orderBy('id','desc')->first();
											if(!empty($we)){
											$res_arr['frndHtml'].=$we->title.' - '.$we->company;
											}
											$res_arr['frndHtml'].='</p><p>';
											$edu = $frndreq->educations()->orderBy('id','desc')->first();
											
											if(!empty($edu)){
											$res_arr['frndHtml'].=$edu->institution.' - '.$edu->major;
											}
											$res_arr['frndHtml'].='</p><p>';
											$live = $frndreq->user_Addresses()->where('is_default','Y')->first();
											if(!empty($live)){
												$res_arr['frndHtml'].='Live In - '.$live->address;
											}
											$res_arr['frndHtml'].='</p>
										</div>
									</div>
									<div class="chatRight">
										<a href=""><i class="fa fa-commenting-o" aria-hidden="true"></i> Message</a>
										<a href=""><i class="fa fa-phone" aria-hidden="true"></i> Call</a>
									</div>
								</div>
								<div class="shortDesc">
									<div class="descText">
										<div class="descTop clear">
											<figure><img src="'.asset('assets/frontend') .'/images/profile.jpg" alt=""></figure>
											<h4><a href="">'.$frndreq->nickname.'</a><span><i class="fa fa-mars" aria-hidden="true"></i>'.$frndreq->age.' yrs</span></h4>
											<p>';
											if(!empty($we)){
											$res_arr['frndHtml'].=$we->title.' - '.$we->company;
											}
											$res_arr['frndHtml'].='</p><p>';
											if(!empty($edu)){
											$res_arr['frndHtml'].=$edu->institution.' - '.$edu->major;
											}
											$res_arr['frndHtml'].='</p><p>';
											if(!empty($live)){
												$res_arr['frndHtml'].='Live In - '.$live->address;
											}
											$res_arr['frndHtml'].='</p>
										</div>
										<div class="deskBtm clear">
											<a href=""><i class="fa fa-commenting-o" aria-hidden="true"></i> Message</a><a href=""><i class="fa fa-phone" aria-hidden="true"></i> Call</a>
										</div>
									</div>
								</div>
							</div>
						</div>';
				}
				
			}	
		}
		return json_encode($res_arr);
	}
	
	public function uploadDp(request $request)
	{
		$input_data = $request->all();
		//print_r($input_data);exit;
		$return_data = "";
		$user = Auth::user(); 
		if($request->hasFile('profile_image')){
			$old_image = 'assets/upload/profile_image/'.$user->profile_image;
			\File::delete($old_image); /* */
			$time = time();
			$path   = public_path().'/assets/upload/profile_image/';
			$image  = $request->file('profile_image');
			$save_name = $time.str_random(10).'.'.$image->getClientOriginalExtension();
			//Image::make($image->getRealPath())->save($path . $save_name, 100);
			
			$image->move($path, $save_name);
			
			$user->profile_image = $save_name;
			$user->save();
			$return_data = asset('assets/upload/profile_image/'.$save_name);
		}else{
			unset($input_data['profile_image']);
		}
		echo $return_data;
	}
	
}
