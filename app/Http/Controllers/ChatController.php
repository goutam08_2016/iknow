<?php
namespace App\Http\Controllers;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use View;
use App\Userlogin;
use App\Friend;
use App\Chatmessage;
use App\Language;
use App\Staticmessage;
use App\Membership;
use App\UserCredit;
use App\Sitefeature;
use App\MembershipFeature;
use App\UserMembership;
use App\Notification;

use App\Appearance;
use App\Bestfeature;
use App\Bodyart;
use App\Bodytype;
use App\City;
use App\Country;
use App\Drinkinghabit;
use App\Eyecolor;
use App\Favoritetoy;
use App\Favouriteposition;
use App\Haircolor;
use App\Sexualdrive;
use App\Sexualorientation;
use App\Smokinghabit;
use App\Starsign;
use App\Personality;
use App\Relationshipstatus;
use App\Ethnicity;
use App\Religion;
use App\Whatturn;
use App\UserToInterest;
use DB;

class ChatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * execute sql query.
     \DB::connection()->enableQueryLog();
    // run query here
    $query = \DB::getQueryLog();
    $lastQuery = end($query);
     */
    public $more_search_data = array();
    public $chat_messsage_data = array();
    public function __construct()
    {
        $this->languages = app('languages');
        $this->fallback_language = $this->languages->where('fallback_locale', 'Y')->first();
		/* 
        $personalities = Personality::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $relationshipstatuses = Relationshipstatus::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $ethnicities = Ethnicity::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $religions = Religion::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $haircolors = Haircolor::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $appearances = Appearance::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $eyecolors = Eyecolor::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $bodytypes = Bodytype::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $starsigns = Starsign::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $smokinghabits = Smokinghabit::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $drinkinghabits = Drinkinghabit::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $bestfeatures = Bestfeature::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $bodyarts = Bodyart::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $whatturns = Whatturn::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $sexualorientations = Sexualorientation::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $favouritepositions = Favouriteposition::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $favoritetoys = Favoritetoy::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();
        $sexualdrives = Sexualdrive::translatedIn(app()->getLocale())->orderBy('id', 'desc')->get();

        $this->more_search_data=[
            'personalities'=>$personalities,
            'relationshipstatuses'=> $relationshipstatuses,
            'ethnicities' => $ethnicities,
            'religions' => $religions,
            'haircolors'=>$haircolors,
            'appearances'=>$appearances,
            'eyecolors'=>$eyecolors,
            'bodytypes'=>$bodytypes,
            'starsigns'=>$starsigns,
            'smokinghabits'=>$smokinghabits,
            'drinkinghabits'=>$drinkinghabits,
            'bestfeatures'=>$bestfeatures,
            'bodyarts'=>$bodyarts,
            'whatturns'=>$whatturns,
            'sexualorientations'=>$sexualorientations,
            'favouritepositions'=>$favouritepositions,
            'favoritetoys'=>$favoritetoys,
            'sexualdrives'=>$sexualdrives
        ]; */
    }
	
    private function getChatUserList()
    {
        $chat_users = [];
		$loguser = Auth::User();
        if(Auth::check())
        {
            $chat_users = Friend::where(['to_user_id' => Auth::user()->id, 'status'=>'Y'])->orWhere(['from_user_id' => Auth::user()->id, 'status'=>'Y'])->orderBy('created_at', 'desc')->get();
			
        }
		/* if(count($chat_users) > 0)
		{
			for($i = 0;$i < count($chat_users); $i++)
			{
				$chat_users[$i]->toUserMessage = Chatmessage::where([
												['from_user_id','=',$chat_users[$i]->to_user_id],
												['to_user_id','=',$chat_users[$i]->from_user_id]
												])->orderBy('created_at', 'desc')->first();
			}
		} */

        $unread_msg = [];
        if(Auth::check())
        {
            $unread_msg = Chatmessage::where([
                                    ['to_user_id', '=', Auth::user()->id],
                                    ['to_user_view_status', '=', 'N']
                                ])
                                ->orderBy('created_at', 'desc')
                                ->get();
        }
		//dd($chat_users->toArray());
        $this->chat_messsage_data = [
            'chat_users' => $chat_users,
            'unread_msg' => $unread_msg
        ];
        //$this->getAllUnreadNotification();
        if(session('touser_id') == '')
        {
            if(count($chat_users) > 0)
            {                
				if($chat_users[0]->from_user_id == $loguser->id ){				
					session()->set('touser_id',$chat_users[0]->to_user_id);
				}	
				else if($chat_users[0]->to_user_id == $loguser->id ){	
					session()->set('touser_id',$chat_users[0]->from_user_id);
				}
            }
        } /* */
    }

    public function messages($uid=0)
    {
       /* session()->forget('touser_id'); */
        $this->getChatUserList();
		
        $message_expired = 0;
        $user_det = User::where('id',Auth::user()->id)->first();
        /* $count_send_msg = Chatmessage::where([
                                        ['from_user_id','=', Auth::user()->id],
                                        ['is_flirt','=', 0]
                                    ])->get()->count(); */

        
        /* if($user_det->canSendMessage() == 0)
        {
            if($user_det->message_delete_status == 'Y')
            {
                if(time() > $user_det->message_delete_time)
                {
                    $message_expired = 1;
                }
                $message_limit = 1;
            }
            else
            {
                $message_limit = 0;
            }
        }
        else
        {
            $message_limit = 0;
        } */
		$message_limit = 10;
        $data = [
            'message_expired' => $message_expired,            
            'message_limit' => $message_limit,                 
            'chat_user' => $uid            
        ];

        return view('chat.chat_messages',$data)->with('chat_messsage_data',$this->chat_messsage_data);
    }
	
    private function getAllUnreadNotification()
    {
        $unread_msg = [];

        if(Auth::check())
        {
            $type_arr = [];
            if(Auth::user()->message_notification=='N')
            {
                array_push($type_arr, 1);
            }
            if(Auth::user()->flirt_notification=='N')
            {
                array_push($type_arr, 2);
            }
            if(Auth::user()->profile_view_notification=='N')
            {
                array_push($type_arr, 3);
            }
            if(Auth::user()->favourite_notification=='N')
            {
                array_push($type_arr, 4);
            }


            $unread_msg = Notification::where([
                                    ['to_user_id', '=', Auth::user()->id],
                                    ['view_status', '=', 'N']
                                ]);
            if(!empty($type_arr))
            {
                $unread_msg = $unread_msg->whereNotIn('type', $type_arr);
            }
            $unread_msg = $unread_msg->orderBy('created_at', 'desc')
                                ->get();
        }
        $notification_arr = [];
        $notification_arr['msg'] = 0;
        $notification_arr['flirt'] = 0;
        $notification_arr['profile_view'] = 0;
        $notification_arr['favourite'] = 0;
        if(count($unread_msg) > 0)
        {
            foreach($unread_msg as $notf)
            {
                switch ($notf->type) 
                {
                    case 1:
                        $notification_arr['msg']++;
                        break;
                    case 2:
                        $notification_arr['flirt']++;
                        break;
                    case 3:
                        $notification_arr['profile_view']++;
                        break;
                    case 4:
                        $notification_arr['favourite']++;
                        break;
                    default:
                        # code...
                        break;
                }
            }
        }
        
        $this->chat_messsage_data['unread_notification_msg'] = $unread_msg;
        $this->chat_messsage_data['notification_arr'] = $notification_arr;
        
    }

    public function addToChat(Request $request)
    {
        $user_id = $request->input('user_id');
        $auth_user = Auth::user()->id;
        $chat_users = Friend::where([
                                    ['from_user_id', '=', $auth_user],
                                    ['to_user_id','=', $user_id]
                                ])->orWhere([
                                    ['from_user_id', '=', $user_id],
                                    ['to_user_id','=', $auth_user]
                                ])->get();
        if($chat_users->isEmpty())
        {
            $chat_user1 = new Friend;
            $chat_user1->from_user_id = $auth_user;
            $chat_user1->to_user_id = $user_id;
            $chat_user1->send_status = 1;
            $chat_user1->save();

            $chat_user2 = new Friend;
            $chat_user2->from_user_id = $user_id;
            $chat_user2->to_user_id = $auth_user;
            $chat_user2->send_status = 0;
            $chat_user2->save();

            $to_user_det = User::find($user_id);
            $is_online = (count($to_user_det->userLogin) > 0)?$to_user_det->userLogin->login_status:0;
            $data = ['to_user_det' => $to_user_det,'is_online' => $is_online];
            $li_html = view('chat.chat_ritgh_panel_single_list',$data)->render();
            $return_data = ['has_error' => 0, 'to_user_det' => $to_user_det,'is_online' => $is_online, 'li_html' => $li_html];
        }
        else
        {
            $return_data = ['has_error' => 1];
        }
        return response()->json($return_data);
    }

    public function startChat(Request $request)
    {
        $user_id = $request->input('user_id');
        $request->session()->set('touser_id',$user_id);
        echo $user_id;
    }
    public function startChatMessage(Request $request)
    {
        $to_user_id = $request->input('toUserID');
        $from_user_id = Auth::user()->id;
        $request->session()->set('touser_id',$to_user_id);
        $all_msg_list = Chatmessage::where([
                                    ['from_user_id', '=', $from_user_id],
                                    ['to_user_id','=', $to_user_id]
                                ])->orWhere([
                                    ['from_user_id', '=', $to_user_id],
                                    ['to_user_id','=', $from_user_id]
                                ])->orderBy('created_at', 'asc')
                                ->get();
		Chatmessage::where([
                      ['from_user_id', '=', $to_user_id],
                      ['to_user_id','=', $from_user_id],
					  ['to_user_view_status', '=', 'N']
                    ])->update(['to_user_view_status' => 'S']);
        $this->chat_messsage_data['all_msg_list'] = $all_msg_list;
        $this->chat_messsage_data['to_user_id'] = $to_user_id;
        $this->chat_messsage_data['from_user_id'] = $from_user_id;
        $all_msg_list_html = view('chat.chat_touser_messages')->with('chat_messsage_data',$this->chat_messsage_data)->render();
        $return_data = ['has_error' => 0, 'all_msg_list_html' => $all_msg_list_html];
        return response()->json($return_data);
    }
    public function sendMessage(Request $request)
    {
        $setting = app('settings');
        $input_data = $request->all();
        $user_det = User::where('id',$input_data['from_user_id'])->first();
        $to_user_det = User::where('id',$input_data['to_user_id'])->first();
        $count_send_msg = Chatmessage::where([
                                        ['from_user_id','=', $input_data['from_user_id']],
                                        ['is_flirt','=', 0]
                                    ])->get()->count();
        /*$membership_message_features = [];
        if($user_det->user_type == 1)
        {
            $membership_message_features = MembershipFeature::where([
                                                            ['site_feature_id', '=', 1],
                                                            ['membership_id', '=', $user_det->user_membership_active->membership_id]
                                                        ])->first();
        }*/
        
        /*if($user_det->user_type == 1 && count($membership_message_features) > 0 && $membership_message_features->is_unlimited == 'N' && $membership_message_features->site_feature_value <= $count_send_msg)
        {
            
            $message_delete_time = strtotime('+'.$setting->message_warning_time.' days');
            User::where('id',$input_data['from_user_id'])->update(['message_delete_status' => 'Y', 'message_delete_time' => $message_delete_time]);
            
            $return_data = ['message_delete_time' => $message_delete_time, 'has_error' => 1];
            
        }*/
        /* if($user_det->canSendMessage() == 0)
        {
            $message_delete_time = strtotime('+'.$setting->message_warning_time.' days');
            User::where('id',$input_data['from_user_id'])->update(['message_delete_status' => 'Y', 'message_delete_time' => $message_delete_time]);
            
            $return_data = ['message_delete_time' => $message_delete_time, 'has_error' => 1];
        }
        else
        { */
            $chat_msg = new Chatmessage;
            $chat_msg->from_user_id = $input_data['from_user_id'];
            $chat_msg->to_user_id = $input_data['to_user_id'];
            $chat_msg->from_user_country_id = $user_det->country_id;
            $chat_msg->to_user_country_id = $to_user_det->country_id;
            $chat_msg->from_user_type = $user_det->user_type;
            $chat_msg->to_user_type = $to_user_det->user_type;
            $chat_msg->message = $input_data['message'];
            $chat_msg->from_user_view_status = 'S';
            $chat_msg->to_user_view_status = 'N';
            $chat_msg->type = $input_data['type'];
            $chat_msg->save();

            $from_user_id = session('touser_id');
            $to_user_id = Auth::user()->id;
            $chat_msg_arr = Chatmessage::where([
                          ['to_user_id','=', $to_user_id],
                          ['from_user_id','=', $from_user_id],
                          ['to_user_view_status', '=', 'N']
                        ])->get();
            if(count($chat_msg_arr) > 0)
            {
                foreach($chat_msg_arr as $unrd_msg)
                {
                    $unrd_msg->to_user_view_status = 'S';
                    $unrd_msg->save();
                }
            }
			
            $chat_msg_arr[count($chat_msg_arr)] = $chat_msg;
            $this->chat_messsage_data['chat_msg_arr'] = $chat_msg_arr;
            $this->chat_messsage_data['to_user_id'] = $input_data['to_user_id'];
            $this->chat_messsage_data['from_user_id'] = $input_data['from_user_id'];
            $ajax_msg_html = view('chat.ajax_chat_messages')->with('chat_messsage_data',$this->chat_messsage_data)->render();
            $return_data = ['has_error' => 0, 'ajax_msg_html' => $ajax_msg_html];
            /* $this->curlCallFunction(); */
        //}
        return response()->json($return_data);
    }
    public function uploadChatfile(Request $request)
    {
        $input_data = $request->all();

        $user_det = User::where('id',$input_data['from_user_id'])->first();
        $to_user_det = User::where('id',$input_data['to_user_id'])->first();
        $count_send_msg = Chatmessage::where([
                                        ['from_user_id','=', $input_data['from_user_id']],
                                        ['is_flirt','=', 0]
                                    ])->get()->count();
        /*$membership_message_features = [];
        if($user_det->user_type == 1)
        {
            $membership_message_features = MembershipFeature::where([
                                                            ['site_feature_id', '=', 1],
                                                            ['membership_id', '=', $user_det->user_membership_active->membership_id]
                                                        ])->first();
        }*/

        /*if($user_det->user_type == 1 && count($membership_message_features) > 0 && $membership_message_features->is_unlimited == 'N' && $membership_message_features->site_feature_value <= $count_send_msg)
        {
            $return_data = ['has_error' => 1];
        }*/
        /* if($user_det->canSendMessage() == 0)
        {
            $return_data = ['has_error' => 1];
        }
        else
        { */
            $time = time();
            if($request->file('attachFile'))
            {
                $attachFile_name = $request->file('attachFile')->getClientOriginalName();
                $file_ext = $request->file('attachFile')->getClientOriginalExtension();
                $attachFile = $time.'_'.strtolower(str_replace(' ','-',$attachFile_name));
                $request->file('attachFile')->move(public_path() . '/assets/upload/chat_files/', $attachFile);
                $chat_msg = new Chatmessage;
                $chat_msg->from_user_id = $input_data['from_user_id'];
                $chat_msg->to_user_id = $input_data['to_user_id'];
                $chat_msg->from_user_country_id = $user_det->country_id;
                $chat_msg->to_user_country_id = $to_user_det->country_id;
                $chat_msg->from_user_type = $user_det->user_type;
                $chat_msg->to_user_type = $to_user_det->user_type;
                $chat_msg->message = $attachFile_name;
                $chat_msg->from_user_view_status = 'S';
                $chat_msg->to_user_view_status = 'N';
                $chat_msg->type = 'F';
                $chat_msg->file_name = $attachFile;
                $chat_msg->file_ext = $file_ext;
                $chat_msg->save();

                $from_user_id = session('touser_id');
                $to_user_id = Auth::user()->id;
                $chat_msg_arr = Chatmessage::where([
                              ['to_user_id','=', $to_user_id],
                              ['from_user_id','=', $from_user_id],
                              ['to_user_view_status', '=', 'N']
                            ])->get();
                if(count($chat_msg_arr) > 0)
                {
                    foreach($chat_msg_arr as $unrd_msg)
                    {
                        $unrd_msg->to_user_view_status = 'S';
                        $unrd_msg->save();
                    }
                }

                $chat_msg_arr[count($chat_msg_arr)] = $chat_msg;

                $this->chat_messsage_data['chat_msg_arr'] = $chat_msg_arr;
                $this->chat_messsage_data['to_user_id'] = $input_data['to_user_id'];
                $this->chat_messsage_data['from_user_id'] = $input_data['from_user_id'];
                $ajax_msg_html = view('chat.ajax_chat_messages')->with('chat_messsage_data',$this->chat_messsage_data)->render();
                $return_data = ['has_error' => 0, 'ajax_msg_html' => $ajax_msg_html];
                /* $this->curlCallFunction(); */
            }
            else
            {
                $return_data = ['has_error' => 2];
            }
        /* } */
        return response()->json($return_data);
    }
	public function loadUnreadMessages(Request $request)
	{
		$return_data = [];
		$return_data['has_error'] = 0;
		$from_user_id = session('touser_id');
		$to_user_id = Auth::user()->id;
		$all_unread_msg = Chatmessage::where([
                      ['to_user_id','=', $to_user_id],
					  ['to_user_view_status', '=', 'N']
                    ])->get();
		
		$chat_msg_arr = [];
		$from_user_msg_count = [];
		if(count($all_unread_msg) > 0)
		{
			foreach($all_unread_msg as $key => $unrd_msg)
			{
				if(!isset($from_user_msg_count[$unrd_msg->from_user_id]))
				{
					$from_user_msg_count[$unrd_msg->from_user_id] = 0;
				}
				if($unrd_msg->from_user_id == $from_user_id)
				{
					$chat_msg_arr[] = $unrd_msg;
					$unrd_msg->to_user_view_status = 'S';
					$unrd_msg->save();
				}
				else
				{
					$from_user_msg_count[$unrd_msg->from_user_id] += 1;
				}
			}
			$this->chat_messsage_data['chat_msg_arr'] = $chat_msg_arr;
            $this->chat_messsage_data['to_user_id'] = $from_user_id;
            $this->chat_messsage_data['from_user_id'] = $to_user_id;
            $ajax_msg_html = view('chat.ajax_chat_messages')->with('chat_messsage_data',$this->chat_messsage_data)->render();
			$return_data['from_user_msg_count'] = $from_user_msg_count;
			$return_data['total_unread_msg_count'] = count($all_unread_msg);
            $return_data['ajax_msg_html'] = $ajax_msg_html;
		}
		else
		{
			$return_data['has_error'] = 1;
		}
        /* Notification::where([
                                ['to_user_id', '=', Auth::user()->id],
                                ['view_status', '=', 'N'],
                                ['type', '=', 1]
                            ])->update(['view_status' => 'S']); */
		return response()->json($return_data);
	}
	public function updateLoginStatus()
	{
		$user = Auth::user();
		$user_login = $user->userLogin;
		if($user_login->login_status == 1)
		{
			$user_login->lastlogintime = time();
			$user_login->save();
		}
	}

    public function listFlirtMessage(Request $request)
    {
        $return_data = [];
        $local = app()->getLocale();
        $input_data = $request->all();
        $local_lang = $input_data['local_lang'];
        if($local_lang && in_array($local_lang, config('translatable.locales')))
        {
            app()->setLocale($local_lang);
        }
        $staticmessages = Staticmessage::translatedIn(app()->getLocale())->orderBy('id', 'asc')->get();
        $flirt_msg_html = view('chat.flirt_messages_list')->with('staticmessages',$staticmessages)->render();

        app()->setLocale($local);

        $return_data['has_error'] = 0;
        $return_data['flirt_msg_html'] = $flirt_msg_html;
        return response()->json($return_data);
    }
    public function sendFlirtMessage(Request $request)
    {
        $return_data = [];
        $return_data['has_error'] = 0;
        $input_data = $request->all();

        $user_det = User::where('id',$input_data['from_user_id'])->first();
        $to_user_det = User::where('id',$input_data['to_user_id'])->first();
        $count_send_msg = Chatmessage::where([
                                        ['from_user_id','=', $input_data['from_user_id']],
                                        ['is_flirt','=', 1]
                                    ])->groupBy('to_user_id')->get()->count();
        /*$membership_message_features = [];
        if($user_det->user_type == 1)
        {
            $membership_message_features = MembershipFeature::where([
                                                            ['site_feature_id', '=', 2],
                                                            ['membership_id', '=', $user_det->user_membership_active->membership_id]
                                                        ])->first();
        }*/

        /*if($user_det->user_type == 1 && count($membership_message_features) > 0 && $membership_message_features->is_unlimited == 'N' && $membership_message_features->site_feature_value <= $count_send_msg)
        {
            $return_data['has_error'] = 1;
        }*/
        if($user_det->canSendFlirt() == 0)
        {
           $return_data['has_error'] = 1;
        }
        else
        {
            $from_user_id = $input_data['from_user_id'];
            $to_user_id = $input_data['to_user_id'];
            $chat_users = Friend::where([
                                        ['from_user_id', '=', $from_user_id],
                                        ['to_user_id','=', $to_user_id]
                                    ])->orWhere([
                                        ['from_user_id', '=', $to_user_id],
                                        ['to_user_id','=', $from_user_id]
                                    ])->get();
            if($chat_users->isEmpty())
            {
                $chat_user1 = new Friend;
                $chat_user1->from_user_id = $from_user_id;
                $chat_user1->to_user_id = $to_user_id;
                $chat_user1->send_status = 1;
                $chat_user1->save();

                $chat_user2 = new Friend;
                $chat_user2->from_user_id = $to_user_id;
                $chat_user2->to_user_id = $from_user_id;
                $chat_user2->send_status = 0;
                $chat_user2->save();
            }

            $chat_msg = new Chatmessage;
            $chat_msg->from_user_id = $from_user_id;
            $chat_msg->to_user_id = $to_user_id;
            $chat_msg->from_user_country_id = $user_det->country_id;
            $chat_msg->to_user_country_id = $to_user_det->country_id;
            $chat_msg->from_user_type = $user_det->user_type;
            $chat_msg->to_user_type = $to_user_det->user_type;
            $chat_msg->message = $input_data['message'];
            $chat_msg->from_user_view_status = 'S';
            $chat_msg->to_user_view_status = 'N';
            $chat_msg->type = 'T';
            $chat_msg->is_flirt = 1;
            $chat_msg->save();

            $new_notify = new Notification;
            $new_notify->notify_type_id = $chat_msg->id;
            $new_notify->from_user_id = $input_data['from_user_id'];
            $new_notify->to_user_id = $input_data['to_user_id'];
            $new_notify->type = 2;
            $new_notify->view_status = 'N';
            $new_notify->audio_status = 'N';
            $new_notify->admin_view = 'N';
            $new_notify->save();

            /****credit deduction 18-01-2017***/
            if($user_det->user_type == 1)
            {
                $credits_per_flirt = app()->settings->credits_per_flirt;
                $user_credit_balance = $user_det->credit_balance - $credits_per_flirt;
                $time = time();
                $user_credit = new UserCredit;
                $user_credit->user_id = $user_det->id;
                $user_credit->action_id = $chat_msg->id;
                $user_credit->action_type = 3;
                $user_credit->date_time = $time;
                $user_credit->transaction_type = 'D';
                $user_credit->credit_amount = $credits_per_flirt;
                $user_credit->user_balance = $user_credit_balance;
                $user_credit->payment_status = 'Y';
                $user_credit->save();

                $user_det->credit_balance = $user_credit_balance;
                $user_det->save();
            }
            /****credit deduction END 18-01-2017***/

            $return_data['msg_id'] =  $chat_msg->id;
            $this->curlCallFunction();
        }
        return response()->json($return_data);
    }

    public function updateNotification()
    {
        //$this->getChatUserList();
        $return_data = array();
        $this->getAllUnreadNotification();
        $return_data['has_error'] = 0;
        $notification_html = view('chat.notification_list')->with('chat_messsage_data',$this->chat_messsage_data)->render();
        $return_data['notification_html'] = $notification_html;
        $return_data['chat_messsage_data'] = $this->chat_messsage_data;
        $return_data['has_audio'] = 0;
        $audio_count = Notification::where([
                                        ['to_user_id', '=', Auth::user()->id],
                                        ['audio_status', '=', 'N']
                                    ])->get()->count();
        if($audio_count > 0)
        {
            $return_data['has_audio'] = 1;
        }
        
        Notification::where('to_user_id', Auth::user()->id)
                        ->update(['audio_status'=>'Y']);
        $return_data['notification_arr'] = $this->chat_messsage_data['notification_arr'];
        return response()->json($return_data);
    }
    public function viewNotification(Request $request)
    {
        $return_data['has_error'] = 0;
        Notification::where('to_user_id', Auth::user()->id)
                        ->update(['view_status'=>'S']);
        return response()->json($return_data);
    }

    /*********Chat popup related methods******/

    public function addToChatPop(Request $request)
    {
        $user_id = $request->input('user_id');
        $auth_user = Auth::user()->id;
        $chat_users = Friend::where([
                                    ['from_user_id', '=', $auth_user],
                                    ['to_user_id','=', $user_id]
                                ])->orWhere([
                                    ['from_user_id', '=', $user_id],
                                    ['to_user_id','=', $auth_user]
                                ])->get();
        if($chat_users->isEmpty())
        {
            $chat_user1 = new Friend;
            $chat_user1->from_user_id = $auth_user;
            $chat_user1->to_user_id = $user_id;
            $chat_user1->send_status = 1;
            $chat_user1->save();

            $chat_user2 = new Friend;
            $chat_user2->from_user_id = $user_id;
            $chat_user2->to_user_id = $auth_user;
            $chat_user2->send_status = 0;
            $chat_user2->save();

            $to_user_det = User::find($user_id);
            $is_online = (count($to_user_det->userLogin) > 0)?$to_user_det->userLogin->login_status:0;
            $data = ['to_user_det' => $to_user_det,'is_online' => $is_online];
            $li_html = view('chat.chat_ritgh_panel_single_list',$data)->render();
            $return_data = ['has_error' => 0, 'to_user_det' => $to_user_det,'is_online' => $is_online, 'li_html' => $li_html];
        }
        else
        {
            $return_data = ['has_error' => 1];
        }
        return response()->json($return_data);
    }
    
    public function startChatPop(Request $request)
    {
        $user_id = $request->input('user_id');
        $to_user_id = $user_id;
        $from_user_id = Auth::user()->id;

        $setting = app('settings');
        $input_data = $request->all();
        $user_det = Auth::user();
        $count_send_msg = Chatmessage::where([
                                        ['from_user_id','=', $from_user_id],
                                        /* ['is_flirt','=', 0] */
                                    ])->get()->count();
        /*$membership_message_features = [];
        if($user_det->user_type == 1)
        {
            $membership_message_features = MembershipFeature::where([
                                                            ['site_feature_id', '=', 1],
                                                            ['membership_id', '=', $user_det->user_membership_active->membership_id]
                                                        ])->first();
        }*/

        $to_user_details_pop = User::find($to_user_id);
        $all_msg_list = Chatmessage::where([
                                    ['from_user_id', '=', $from_user_id],
                                    ['to_user_id','=', $to_user_id]
                                ])->orWhere([
                                    ['from_user_id', '=', $to_user_id],
                                    ['to_user_id','=', $from_user_id]
                                ])->orderBy('created_at', 'asc')
                                ->get();
        Chatmessage::where([
                      ['from_user_id', '=', $to_user_id],
                      ['to_user_id','=', $from_user_id],
                      ['to_user_view_status', '=', 'N']
                    ])->update(['to_user_view_status' => 'S']);
        $this->chat_messsage_data['all_msg_list_pop'] = $all_msg_list;
        $this->chat_messsage_data['to_user_id_pop'] = $to_user_id;
        $this->chat_messsage_data['from_user_id_pop'] = $from_user_id;
        $this->chat_messsage_data['to_user_details_pop'] = $to_user_details_pop;
        $this->chat_messsage_data['can_msg_pop'] = 1;
		
        /*$this->chat_messsage_data['can_msg_pop'] = $user_det->canSendMessage();

        if($user_det->user_type == 1 && count($membership_message_features) > 0 && $membership_message_features->is_unlimited == 'N' && $membership_message_features->site_feature_value <= $count_send_msg)
        {
            $this->chat_messsage_data['can_msg_pop'] = 0;
        }*/

        $all_msg_list_html = view('chat.chat_pop.chatpop_clone')->with('chat_messsage_data',$this->chat_messsage_data)->render();
        $return_data = ['has_error' => 0, 'all_msg_list_html' => $all_msg_list_html, 'to_user_id' => $to_user_id];
        return response()->json($return_data);
    }

    public function sendMessagePop(Request $request)
    {
        $setting = app('settings');
        $input_data = $request->all();
        $user_det = User::where('id',$input_data['from_user_id'])->first();
        $to_user_det = User::where('id',$input_data['to_user_id'])->first();
        $count_send_msg = Chatmessage::where([
                                        ['from_user_id','=', $input_data['from_user_id']],
                                        ['is_flirt','=', 0]
                                    ])->get()->count();
        /*$membership_message_features = [];
        if($user_det->user_type == 1)
        {
            $membership_message_features = MembershipFeature::where([
                                                            ['site_feature_id', '=', 1],
                                                            ['membership_id', '=', $user_det->user_membership_active->membership_id]
                                                        ])->first();
        }*/
        
        /*if($user_det->user_type == 1 && count($membership_message_features) > 0 && $membership_message_features->is_unlimited == 'N' && $membership_message_features->site_feature_value <= $count_send_msg)
        {
            
            $message_delete_time = strtotime('+'.$setting->message_warning_time.' days');
            User::where('id',$input_data['from_user_id'])->update(['message_delete_status' => 'Y', 'message_delete_time' => $message_delete_time]);
            
            $return_data = ['message_delete_time' => $message_delete_time, 'has_error' => 1];
            
        }
        if($user_det->canSendMessage() == 0)
        {
            
            $message_delete_time = strtotime('+'.$setting->message_warning_time.' days');
            User::where('id',$input_data['from_user_id'])->update(['message_delete_status' => 'Y', 'message_delete_time' => $message_delete_time]);
            
            $return_data = ['message_delete_time' => $message_delete_time, 'has_error' => 1];
            
        }
        else
        {*/
            $chat_msg = new Chatmessage;
            $chat_msg->from_user_id = $input_data['from_user_id'];
            $chat_msg->to_user_id = $input_data['to_user_id'];
            $chat_msg->from_user_country_id = $user_det->country_id;
            $chat_msg->to_user_country_id = $to_user_det->country_id;
            $chat_msg->from_user_type = $user_det->user_type;
            $chat_msg->to_user_type = $to_user_det->user_type;
            $chat_msg->message = $input_data['message'];
            $chat_msg->from_user_view_status = 'S';
            $chat_msg->to_user_view_status = 'N';
            $chat_msg->type = $input_data['type'];
            $chat_msg->save();

            /* $new_notify = new Notification;
            $new_notify->notify_type_id = $chat_msg->id;
            $new_notify->from_user_id = $input_data['from_user_id'];
            $new_notify->to_user_id = $input_data['to_user_id'];
            $new_notify->type = 1;
            $new_notify->view_status = 'N';
            $new_notify->audio_status = 'N';
            $new_notify->admin_view = 'N';
            $new_notify->save(); */

            $from_user_id = session('touser_id');
            $to_user_id = Auth::user()->id;
            $chat_msg_arr = Chatmessage::where([
                          ['to_user_id','=', $to_user_id],
                          ['from_user_id','=', $from_user_id],
                          ['to_user_view_status', '=', 'N']
                        ])->get();
            if(count($chat_msg_arr) > 0)
            {
                foreach($chat_msg_arr as $unrd_msg)
                {
                    $unrd_msg->to_user_view_status = 'S';
                    $unrd_msg->save();
                }
            }

            $chat_msg_arr[count($chat_msg_arr)] = $chat_msg;
            $this->chat_messsage_data['chat_msg_arr_pop'] = $chat_msg_arr;
            $this->chat_messsage_data['to_user_id_pop'] = $input_data['to_user_id'];
            $this->chat_messsage_data['from_user_id_pop'] = $input_data['from_user_id'];
            $ajax_msg_html = view('chat.chat_pop.ajax_chatpop_messages')->with('chat_messsage_data',$this->chat_messsage_data)->render();
            $return_data = ['has_error' => 0, 'ajax_msg_html' => $ajax_msg_html];
            //$this->curlCallFunction();
        //}
        return response()->json($return_data);
    }
    public function uploadChatfilePop(Request $request)
    {
        $input_data = $request->all();

        $user_det = User::where('id',$input_data['from_user_id'])->first();
        $to_user_det = User::where('id',$input_data['to_user_id'])->first();
        $count_send_msg = Chatmessage::where([
                                        ['from_user_id','=', $input_data['from_user_id']],
                                       /*  ['is_flirt','=', 0] */
                                    ])->get()->count();
        /*$membership_message_features = [];
        if($user_det->user_type == 1)
        {
            $membership_message_features = MembershipFeature::where([
                                                            ['site_feature_id', '=', 1],
                                                            ['membership_id', '=', $user_det->user_membership_active->membership_id]
                                                        ])->first();
        }*/

        /*if($user_det->user_type == 1 && count($membership_message_features) > 0 && $membership_message_features->is_unlimited == 'N' && $membership_message_features->site_feature_value <= $count_send_msg)
        {
            $return_data = ['has_error' => 1,'to_user_id_pop' => $input_data['to_user_id']];
        }
        if($user_det->canSendMessage() == 0)
        {
            $return_data = ['has_error' => 1,'to_user_id_pop' => $input_data['to_user_id']];
        }
        else
        {*/
            $time = time();
            if($request->file('attachFile'))
            {
                $attachFile_name = $request->file('attachFile')->getClientOriginalName();
                $file_ext = $request->file('attachFile')->getClientOriginalExtension();
                $attachFile = $time.'_'.strtolower(str_replace(' ','-',$attachFile_name));
                $request->file('attachFile')->move(public_path() . '/assets/upload/chat_files/', $attachFile);
                $chat_msg = new Chatmessage;
                $chat_msg->from_user_id = $input_data['from_user_id'];
                $chat_msg->to_user_id = $input_data['to_user_id'];
                $chat_msg->from_user_country_id = $user_det->country_id;
                $chat_msg->to_user_country_id = $to_user_det->country_id;
                $chat_msg->from_user_type = $user_det->user_type;
                $chat_msg->to_user_type = $to_user_det->user_type;
                $chat_msg->message = $attachFile_name;
                $chat_msg->from_user_view_status = 'S';
                $chat_msg->to_user_view_status = 'N';
                $chat_msg->type = 'F';
                $chat_msg->file_name = $attachFile;
                $chat_msg->file_ext = $file_ext;
                $chat_msg->save();

                /* $new_notify = new Notification;
                $new_notify->notify_type_id = $chat_msg->id;
                $new_notify->from_user_id = $input_data['from_user_id'];
                $new_notify->to_user_id = $input_data['to_user_id'];
                $new_notify->type = 1;
                $new_notify->view_status = 'N';
                $new_notify->audio_status = 'N';
                $new_notify->admin_view = 'N';
                $new_notify->save(); */

                $from_user_id = session('touser_id');
                $to_user_id = Auth::user()->id;
                $chat_msg_arr = Chatmessage::where([
                              ['to_user_id','=', $to_user_id],
                              ['from_user_id','=', $from_user_id],
                              ['to_user_view_status', '=', 'N']
                            ])->get();
                if(count($chat_msg_arr) > 0)
                {
                    foreach($chat_msg_arr as $unrd_msg)
                    {
                        $unrd_msg->to_user_view_status = 'S';
                        $unrd_msg->save();
                    }
                }

                $chat_msg_arr[count($chat_msg_arr)] = $chat_msg;

                $this->chat_messsage_data['chat_msg_arr_pop'] = $chat_msg_arr;
                $this->chat_messsage_data['to_user_id_pop'] = $input_data['to_user_id'];
                $this->chat_messsage_data['from_user_id_pop'] = $input_data['from_user_id'];
                $ajax_msg_html = view('chat.chat_pop.ajax_chatpop_messages')->with('chat_messsage_data',$this->chat_messsage_data)->render();
                $return_data = ['has_error' => 0, 'ajax_msg_html' => $ajax_msg_html, 'to_user_id_pop' => $input_data['to_user_id']];
                //$this->curlCallFunction();
            }
            else
            {
                $return_data = ['has_error' => 2, 'to_user_id_pop' => $input_data['to_user_id']];
            }
        //}
        return response()->json($return_data);
    }
    public function loadUnreadMessagesPop(Request $request)
    {
        $return_data = [];
        $return_data['has_error'] = 0;
        $to_user_id = Auth::user()->id;

        $user_det = Auth::user();
        $count_send_msg = Chatmessage::where([
                                        ['from_user_id','=', Auth::user()->id],
                                       /*  ['is_flirt','=', 0] */
                                    ])->get()->count();
        /*$membership_message_features = [];
        if($user_det->user_type == 1)
        {
            $membership_message_features = MembershipFeature::where([
                                                            ['site_feature_id', '=', 1],
                                                            ['membership_id', '=', $user_det->user_membership_active->membership_id]
                                                        ])->first();
        }*/

        /*if($user_det->user_type == 1 && count($membership_message_features) > 0 && $membership_message_features->is_unlimited == 'N' && $membership_message_features->site_feature_value <= $count_send_msg)
        {
            $return_data['has_error'] = 1;
        }
        if($user_det->canSendMessage() == 0)
        {
            $return_data['has_error'] = 1;
        }
        else
        {*/
            $all_unread_msg = Chatmessage::where([
                      ['to_user_id','=', $to_user_id],
                      ['to_user_view_status', '=', 'N']
                    ])->get();
        
            $chat_msg_arr = [];
            if(count($all_unread_msg) > 0)
            {
                foreach($all_unread_msg as $key => $unrd_msg)
                {
                    if(!isset($chat_msg_arr[$unrd_msg->from_user_id]))
                    {
                        $chat_msg_arr[$unrd_msg->from_user_id] = [];
                    }
                    $chat_msg_arr[$unrd_msg->from_user_id][] = $unrd_msg;
                    $unrd_msg->to_user_view_status = 'S';
                    $unrd_msg->save();
                }
                $return_data['from_user_msg'] = [];
                foreach($chat_msg_arr as $key => $msg_arr)
                {
                    $this->chat_messsage_data['chat_msg_arr_pop'] = $msg_arr;
                    $this->chat_messsage_data['to_user_id_pop'] = $key;
                    $this->chat_messsage_data['from_user_id_pop'] = $to_user_id;
                    $ajax_msg_html = view('chat.chat_pop.ajax_chatpop_messages')->with('chat_messsage_data',$this->chat_messsage_data)->render();
                    $return_data['from_user_msg'][$key]['ajax_msg_html'] = $ajax_msg_html;
                }
            }
            else
            {
                $return_data['has_error'] = 1;
            }
            /* Notification::where([
                                ['to_user_id', '=', Auth::user()->id],
                                ['view_status', '=', 'N'],
                                ['type', '=', 1]
                            ])->update(['view_status' => 'S']); */
        //}
        return response()->json($return_data);
    }

    private function curlCallFunction()
    {
        $url = env('CHAT_CURL', 'http://kikkmode.com/frontend/web/cronjob/add-unreplied-messages');
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_VERBOSE, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
        //curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
        $curlData = curl_exec($curl);
        if(curl_errno($curl))
        {
            return curl_error($curl);
        }
        else
        {
            return $curlData;
        }
        curl_close($curl);
    }
}
