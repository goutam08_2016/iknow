<?php

namespace App\Http\Controllers\Auth;

use URL;
use App\User;
use App\EmailTemplate;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            //'first_name' => 'required|max:255',
            //'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
			//'user_name' => 'required|max:255|unique:users',
            //'address' => 'required',
            //'utc_timezone' => 'required',
            'password' => 'required|min:6|confirmed',
            //'g-recaptcha-response' => 'required',
            'terms_of_services' => 'required',
            //'address' => 'required|max:255',
            //'user_type' => 'required|in:1,2'
        ],[
            //'user_type.required' => 'Register as normal user or client',
            //'user_type.in' => 'Register as normal user or client'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        /* return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]); */
    }
		
	public function register(Request $request)
    {
		$data=$request->all();
       // dd($data);
		//
		
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
		//dd($data);
		$confirmation_code = str_random(30);
        $user_data = [
            //'first_name' => $data['first_name'],
            //'last_name' => $data['last_name'],
            //'user_name' => $data['user_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            //'user_type' => $data['user_type'],
            /* 'city' => $data['city'],
            'province' => $data['province'],*/
            //'address' => $data['address'], 
            //'utc_timezone' => $data['utc_timezone'], 
            'status' => 'E',
            'in_step' => 1,
            //'confirmation_code' => $confirmation_code
        ];
		/* if($data['user_type']==2)
		{
			$user_data['business_name']= $data['business_name'];
		} */
		//dd($user_data);
        // Add optional address inputs
        if(!empty($data['phone']))
            $user_data['phone'] = $data['phone'];
        if(!empty($data['website']))
            $user_data['website'] = $data['website'];
        if(!empty($data['fax']))
            $user_data['fax'] = $data['fax'];
        if(!empty($data['postal_code']))
            $user_data['postal_code'] = $data['postal_code'];
        if(!empty($data['lat']))
            $user_data['lat'] = $data['lat'];
        if(!empty($data['lng']))
            $user_data['lng'] = $data['lng'];

        $user = User::create($user_data);
		Auth::login($user);		
       /*  if($user !== null) {	
			session(['register_success' => 'Your registration successful. Please check your email to active your account.']);
            $setting = app('settings');
            $mail_data = [
                'confirmation_code' => $confirmation_code,
                'user' => $user,
                'setting' => $setting,
                'utype' => $data['utype']
            ];
			$email_template = EmailTemplate::find(1);
			$mailcontent=htmlspecialchars_decode($email_template->description);
			$mailcontent=str_replace('{USER_NAME}',$user->first_name,$mailcontent);
			$mailcontent=str_replace('{SITE_URL}',URL::to('/'),$mailcontent);
			$mailcontent=str_replace('{SITE_TITLE}',$setting->site_title,$mailcontent);
			$mailcontent=str_replace('{CONTACT_MAIL}',$setting->contact_email,$mailcontent);
			$mailcontent=str_replace('{ACTIVATION_LINK}',URL::to('/register/verify/' . $confirmation_code.'/'.$data['utype']),$mailcontent);
				
            //Mail::send('auth.emails.verify', $mail_data, function($message) use($user) {
            Mail::raw($mailcontent,$mail_data, function($message) use($user) {
                $setting = app('settings');
                $subject = $setting->site_title . 'Verify your email address';

                $message->from($setting->contact_email, $setting->contact_name);
                $message->to($user->email, $user->first_name.' '. $user->last_name);
				
                $message->replyTo($setting->contact_email, $setting->contact_name);
                $message->subject($subject);
            });
			
			
		} */
		//return $user;
		return redirect(url('user/signup-step/1'));
	}
	
	public function showRegistrationForm($type=0)
    {		
		$timezones= \App\Timezone::all();

		return view('register', compact('timezones','type'));
	}
		
	public function postLogin(Request $request)
    {
        $inputdata = $request->all();
		//dd($inputdata);
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);
        $user_det = User::where('email',$credentials['email'])->first();
        if(count($user_det) > 0)
        {
            //unset();$credentials['username']
            $credentials['email'] = $request->input('email');
        }

        if (Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) 
		{
            if(Auth::user()->status == 'Y')
            {
              
                $this->handleUserWasAuthenticated($request, $throttles);
				$this->redirectTo='/user/question-home';
				
            }
            else
            {
				$in_step = Auth::user()->in_step;
				return redirect(url('user/signup-step/'.$in_step));
                /* auth::logout();
               return redirect($this->redirectPath())
					->withInput($request->only($this->loginUsername(), 'remember'))
					->withErrors([
					$this->loginUsername() => 'Your account is not activated.'
					])->with('inactive','inactive'); */
            }
        }
		
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }
		//dd($this->redirectPath());
		return redirect($this->redirectPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
        //return response()->json($response);

    }
	
	public function socialLogin(Request $request)
    {
		$data = $request->all();
		//print_r($data);		exit;
		$has_error = 1;			
		$return_data = array();
			/* && $data['social_type']=='vk' */
		if(!empty($data['social_type']) )
		{ 
			if(!empty($data['name']))
					$uname = $data['name'] ;
			if(!empty($data['screen_name']))
					$uname =  $data['screen_name'];
			$name_arr = explode(' ',$uname);
			if(!isset($data['first_name']))
					$data['first_name'] = $name_arr[0];
				
			if(!empty($data['email']))
					$email = $data['email'];
			else	$email = strtolower($data['first_name'])."@demo.com";
			
			//$user = User::where('user_name',$uname)->orWhere('email',$email)->first();
			$user = User::where('email',$email)->first();
			
			
			$password = 'ABC'.rand(100,9999);
			if(empty($user))
			{
				$user_data = [
					'first_name' => $data['first_name'],
					'last_name' => $data['last_name'],
					'nickname' => $uname,
					'email' => $email,
					'password' => bcrypt($password),
					'user_type' => 1,
					'status' => 'Y',
					'in_step' => 5,
				];
				
				if(!empty($data['mobile_phone']))
					$user_data['phone'] = $data['mobile_phone'];			

				$user = User::create($user_data);
				
			}
			//dd($user->toArray());
		
			Auth::login($user);			
			return $user;
		}		
	}
		
	
}
