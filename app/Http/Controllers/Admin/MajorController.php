<?php
namespace App\Http\Controllers\Admin;


use App\SchoolMajor;

use App\Language;

use Illuminate\Http\Request;



use App\Http\Requests;

use App\Http\Controllers\Controller;



class MajorController extends Controller
{

    protected $languages;

    protected $fallback_language;

    /**

     * CmsPageController constructor.

     */

    public function __construct()
    {

        $this->languages = app('languages');

        $this->fallback_language = $this->languages->where('fallback_locale', 'Y')->first();

    }



    /**

     * Display a listing of the resource.

     *

     * @param string $locale

     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View

     */

    public function index($locale = '')

    {

        /* if($locale && in_array($locale, config('translatable.locales'))) {

            app()->setLocale($locale);

        } */

        $current_language = Language::current();


        $per_page = config('constants.ADMIN_PER_PAGE');
        $majors = SchoolMajor::orderBy('id', 'desc')->paginate($per_page);

        //$cms = Page::translatedIn(app()->getLocale())->orderBy('id', 'desc')->paginate($per_page);

        return view('admin.majors.list', compact('majors','current_language'));

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
		
        return view('admin.email_templates.add');

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {		
        $this->validate($request, [

                'email_title' => 'required|max:255',

                'description' => 'required',

            ]

        );

        EmailTemplate::create($request->all());

        return redirect()->back()->with('success', 'Page successfully added.');

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $email_templates = EmailTemplate::find($id);

		//dd($packages->toArray());

        return view('admin.email_templates.edit', compact('email_templates'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        $this->validate($request, [

                'email_title' => 'required|max:255',

                'description' => 'required',

            ]

        );
		$inp_data=$request->all();
		$data['email_title']= $inp_data['email_title'];
		$data['description']= $inp_data['description'];
		$data['locale']= $this->fallback_language->locale;
		
        $emailTemplate = EmailTemplate::find($id);
		//unset($data['_method']);unset($data['_token']);
		//dd($data);
        $emailTemplate->fill($data)->save();

        return redirect()->back()->with('success', 'Update was successfully done.');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)
    {
        SchoolMajor::destroy($id);

        return redirect()->back()->with('success', 'School Major successfully removed!');

    }

    public function deleteAll(Request $request)
    {
		$input_data = $request->all();
		if(isset($input_data['data_ids']) && $input_data['data_ids']!='')
		{		
			$data_ids=explode(',',$input_data['data_ids']);
			//dd($data_ids);			
			$d= SchoolMajor::whereIn('id',$data_ids)->delete();
			return 1;
			//return redirect()->back()->with('success', 'Records successfully removed!');
		}
		else return 0;
    }
}

