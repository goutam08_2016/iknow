<?php



namespace App\Http\Controllers\Admin;



use App\Package;

use App\Language;

use Illuminate\Http\Request;



use App\Http\Requests;

use App\Http\Controllers\Controller;



class PackageController extends Controller

{

    protected $languages;

    protected $fallback_language;



    /**

     * CmsPageController constructor.

     */

    public function __construct()

    {

        $this->languages = app('languages');

        $this->fallback_language = $this->languages->where('fallback_locale', 'Y')->first();

    }



    /**

     * Display a listing of the resource.

     *

     * @param string $locale

     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View

     */

    public function index($locale = '')

    {

        /* if($locale && in_array($locale, config('translatable.locales'))) {

            app()->setLocale($locale);

        } */

        $current_language = Language::current();


        $per_page = config('constants.ADMIN_PER_PAGE');
        $packages = Package::orderBy('id', 'desc')->paginate($per_page);

        //$cms = Page::translatedIn(app()->getLocale())->orderBy('id', 'desc')->paginate($per_page);

        return view('admin.packages.list', compact('packages','current_language'));

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
		
        return view('admin.packages.add');

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {		
        $this->validate($request, [

                'plan' => 'required|max:255',

                'duration' => 'required|numeric',

                'price' => 'required|numeric',

                'details' => 'required',

            ]

        );

        Package::create($request->all());

        return redirect()->back()->with('success', 'Page successfully added.');

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $packages = Package::find($id);

		//dd($packages->toArray());

        return view('admin.packages.edit', compact('packages'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        $this->validate($request, [

                'plan' => 'required|max:255',

                'duration' => 'required|numeric',

                'price' => 'required|numeric',

                'details' => 'required',

            ]

        );
		$inp_data=$request->all();
		$data['plan']= $inp_data['plan'];
		$data['duration']= $inp_data['duration'];
		$data['price']= $inp_data['price'];
		$data['details']= $inp_data['details'];
		
        $package = Package::find($id);
		//unset($data['_method']);unset($data['_token']);
		//dd($data);
        $package->fill($data)->save();

        return redirect()->back()->with('success', 'Update was successfully done.');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        Package::destroy($id);

        return redirect()->back()->with('success', 'Page successfully removed!');



    }



    public function deleteTranslation($locale = '', $id)

    {

        if($locale && in_array($locale, config('translatable.locales'))) {

            if($locale == '' || !in_array($locale, config('translatable.locales'))) {

                return redirect()->back()->with('error', 'Sorry! Unable to process your request.');

            }

            app()->setLocale($locale);

            $cms = Page::find($id)->getTranslation($locale);

            $cms->delete();

            $current_language = Language::current();

            return redirect()->back()->with('success', 'Page for ' . $current_language->name . ' successfully removed!');

        }

        return redirect()->back()->with('error', 'Sorry! Unable to process your request.');

    }

}

