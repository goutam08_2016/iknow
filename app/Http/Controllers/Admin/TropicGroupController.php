<?php



namespace App\Http\Controllers\Admin;



use App\AllTropic;
use App\TropicGroup;

use App\Language;

use Illuminate\Http\Request;



use App\Http\Requests;

use App\Http\Controllers\Controller;



class TropicGroupController extends Controller

{

    protected $languages;

    protected $fallback_language;



    /**

     * CmsPageController constructor.

     */

    public function __construct()

    {

        $this->languages = app('languages');

        $this->fallback_language = $this->languages->where('fallback_locale', 'Y')->first();

    }



    /**

     * Display a listing of the resource.

     *

     * @param string $locale

     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View

     */

    public function index($locale = '')

    {

        /* if($locale && in_array($locale, config('translatable.locales'))) {

            app()->setLocale($locale);

        } */

        $current_language = Language::current();


        $per_page = config('constants.ADMIN_PER_PAGE');
        $groups = TropicGroup::orderBy('id', 'desc')->paginate($per_page);

        //$cms = Page::translatedIn(app()->getLocale())->orderBy('id', 'desc')->paginate($per_page);

        return view('admin.groups.list', compact('groups','current_language'));

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
		
        return view('admin.groups.add');

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {		
        $this->validate($request, [

                'name' => 'required|max:255',
            ]
        );
		$inp_data=$request->all();		
		
        TropicGroup::create($inp_data);

        return redirect()->back()->with('success', 'Group successfully added.');

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $group = TropicGroup::find($id);

		//dd($packages->toArray());

        return view('admin.groups.edit', compact('group'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)
    {

        $this->validate($request, [

                'name' => 'required|max:255',
            ]

        );
		$inp_data=$request->all();
		$data['name']= $inp_data['name'];
		
        $group = TropicGroup::find($id);
		//unset($data['_method']);unset($data['_token']);
		//dd($data);
        $group->fill($data)->save();

        return redirect()->back()->with('success', 'Update was successfully done.');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)
    {

        TropicGroup::destroy($id);
		AllTropic::where('group_id',$id)->update(array('group_id'=>0));
		\App\Tropic::where('group_id',$id)->update(array('group_id'=>0));
        return redirect()->back()->with('success', 'Tropic group successfully removed!');

    }



    /* public function deleteTranslation($locale = '', $id)

    {

        if($locale && in_array($locale, config('translatable.locales'))) {

            if($locale == '' || !in_array($locale, config('translatable.locales'))) {

                return redirect()->back()->with('error', 'Sorry! Unable to process your request.');

            }

            app()->setLocale($locale);

            $cms = Page::find($id)->getTranslation($locale);

            $cms->delete();

            $current_language = Language::current();

            return redirect()->back()->with('success', 'Page for ' . $current_language->name . ' successfully removed!');

        }

        return redirect()->back()->with('error', 'Sorry! Unable to process your request.');

    } */

}

