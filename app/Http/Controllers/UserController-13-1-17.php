<?php

namespace App\Http\Controllers;

use App\User;
use App\Country;
use App\WorkExperience;
use App\Education;
use App\Tropic;
use App\Company;
use App\Job;
use App\University;
use App\SchoolMajor;
use App\AllTropic;
use App\UserAddres;
use App\AskQuestion;
use App\FilterOrderSet;
use Image;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use Omnipay\Omnipay;

use Session;


class UserController extends Controller
{
	private $advance_search_data; 
    /**
     * UserController constructor.
     */
    public function __construct()
    {
		
    }

    /**
     * Activate user account.
     *
     * @param $confirmation_code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate($confirmation_code,$utype)
    {
        if($confirmation_code)
        {
            $user = User::where(['confirmation_code' => $confirmation_code])->first();
            if($user !== null)
            {
                $user->confirmation_code = '';
                $user->status = 'Y';
                $user->save();
                Auth::login($user);
				if($utype==2)
					return redirect()->to('/pricing')->with('activation', 'Your account is now activated!');
				else
					return redirect()->to('/')->with('activation', 'Your account is now activated!');
            }
        }
        return redirect()->to('/')->with('activation', 'Sorry! Unable to process your request.');
    }

    public function account($id=0)
    {
		if($id>0)
		{
			$user = User::find($id);
		}
		else	$user = Auth::user();
		$companies = Company::all();
		$jobs = Job::all();
		$universities = University::all();
		$school_majors = SchoolMajor::all();
		$countries = Country::all();
		$all_tropics = AllTropic::all();
		$degrees = \App\Degree::all();
        return view('user.profile',compact('user','countries','jobs','companies','universities','school_majors','all_tropics','degrees'));
    }
		
    /**
     * Update details of the user.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateAccount(Request $request)
    {
        $this->validate($request,[
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'user_name' => 'required|max:255|unique:users,user_name,'.Auth::user()->id,
            'email' => 'required|max:255|unique:users,email,'.Auth::user()->id,
            'address' => 'required|max:255',
            'utc_timezone' => 'required',
        ]);
        $input = $request->all();
		//dd($input);
        /* if(isset($input['email'])) {
            unset($input['email']);
        } */
		$time = time();
		
		if($request->hasFile('profile_image')){
            $old_image = 'assets/upload/profile_image/'.Auth::user()->profile_image;
            \File::delete($old_image);
			
            $path   = public_path().'/assets/upload/profile_image/';
            $image  = $request->file('profile_image');
            $save_name = $time.str_random(10).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->save($path . $save_name, 100);
            $input['profile_image'] = $save_name;
        }
		
		if($input['password']!="")
		{
			$this->validate($request, [
                'old_password' => 'required',
                'password' => 'required|confirmed|min:6',
            ]);			
			Auth::user()->update(['password' => bcrypt($request->input('password'))]);
		}
		unset($input['password']);
		
        Auth::user()->fill($input)->save();
        return redirect(url('user/my-details'))
                ->with('success', 'Account details successfully updated.');
    }

    public function changePassword(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('user.change-password');
        }
        elseif ($request->isMethod('post')) {
            $this->validate($request, [
                'old_password' => 'required|old_password',
                'password' => 'required|confirmed|min:6',
            ]);
            Auth::user()->update(['password' => bcrypt($request->input('password'))]);
            return redirect(url('user/account'))
                ->with('success', 'Password successfully updated.');
        }
        return redirect(url('user/account'));
    }		
   
	public function signupStep($step=0)
	{
		if ($step==0) {
			$step=1;
		}
		$companies = Company::all();
		$jobs = Job::all();
		$universities = University::all();
		$school_majors = SchoolMajor::all();
		$countries = Country::all();
		$all_tropics = AllTropic::all();
		$degrees = \App\Degree::all();
		//$user_address = UserAddres::all();
		$user = Auth::user();
		$user_address = $user->user_Addresses;
        return view('user.signup.signup-step'.$step,compact('user','step','countries','companies','jobs','universities','school_majors','all_tropics','user_address','degrees'));
	}
   
	public function signupStepProcess(Request $request)
	{
		$inp_data = $request->all();
		//dd($inp_data);
		$user = Auth::user();
		$in_step = $inp_data['in_step'];
		switch($in_step)
		{
			case 1:
			$in_step=$in_step+1;			
			//$user->user_name = $inp_data['user_name'];
			$name = explode(' ',$inp_data['user_name']);
			$user->first_name = $name[0];
			$user->last_name = isset($name[0])? str_replace($name[0],'',$inp_data['user_name']) :'';
			$user->nickname = isset($input_data['user_name'])? $input_data['user_name'] :'';
			$user->gender = $inp_data['gender'];
			$user->in_step = $in_step;
			
			$inp_data['day'] = ($inp_data['day']!='')?$inp_data['day']:'1';
			$inp_data['month'] = ($inp_data['month']!='')?$inp_data['month']:'1';
			$user->dob = $inp_data['day'].'-'.$inp_data['month'].'-'.$inp_data['year'];
				$today = time();
				$dobtime = strtotime($user->dob);
				$age = round((int)($today - $dobtime)/(86400*365));
			$user->age = $age;
			break;
			
			case 2:
			$in_step=$in_step+1;
			$user->in_step = $in_step;
			break;
			
			case 3:
			$in_step=$in_step+1;
			$user->in_step = $in_step;
			break;
			
			case 4:
			$in_step=$in_step+1;
			$user->in_step = $in_step;
			break;
			
			case 5:
			//$user->in_step = $in_step;
			$in_step=$in_step+1;
			$user->status = "Y";
			break;
			
		}		
		$user->save();
		if($in_step==6)
		return redirect(url('user/account/'));
		else
		return redirect(url('user/signup-step/'.$in_step));
	}
	
	public function addAddress(Request $request)
	{
		$inp_data = $request->all();
		//print_r($inp_data);
		//$fld = $inp_data['fname'];
		//$user->$fld = $inp_data['address'];
		$user = Auth::user();
		$insdata = [
			'user_id' => $user->id,
			'address' => $inp_data['address'],
			'type' => $inp_data['addType'],
		];
		if($inp_data['addType']==1)
		{
			$curr_addr = UserAddres::where(['user_id'=>$user->id,'type'=>$inp_data['addType']])->first();
			if(empty($curr_addr))
			{
				$insdata['is_default'] = 'Y';
			}
		}
		$userAddres = UserAddres::create($insdata);
		return $userAddres;
	}
	
    public function delAddress(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);exit;
		$user = Auth::user();
		if(!empty($input_data))
		{
			$cond=array();
			$cond['id']	= $input_data['id'];
			$cond['user_id']= $user->id;	
			$qdata = UserAddres::where($cond)->delete();	
			if($qdata==1)
			{
					return UserAddres::where(array('is_default'=>'Y','user_id'=>$user->id))->first();
			}
			else	return 0;
		}
	}
	
	public function addWorkExpProcess(Request $request)
	{
		$inp_data = $request->all();
		//print_r($inp_data);exit;
		$user = Auth::user();
		$inp_data['user_id'] = $user->id;
		if(isset($inp_data['wrkexp_id']) && $inp_data['wrkexp_id']!='')
		{
			$wrkexp_id = $inp_data['wrkexp_id']; unset($inp_data['wrkexp_id']);
			$workExperience = WorkExperience::where('id',$wrkexp_id)->update($inp_data);
		}else{
			unset($inp_data['wrkexp_id']);
			$workExperience = WorkExperience::create($inp_data);
		}		
		
		return $workExperience; 
	}
	
    public function delWorkExpProcess(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);exit;
		if(!empty($input_data))
		{
			$cond=array();
			$cond['id']	= $input_data['id'];
			$cond['user_id']= Auth::user()->id;	
			$qdata = WorkExperience::where($cond)->delete();	
			return $qdata;
		}
	}
	public function addEducationProcess(Request $request)
	{
		$inp_data = $request->all();
		//print_r($inp_data);
		$user = Auth::user();
		$inp_data['user_id'] = $user->id;
		
		if(isset($inp_data['edu_id']) && $inp_data['edu_id']!='')
		{
			$edu_id = $inp_data['edu_id']; unset($inp_data['edu_id']);
			$education = Education::where('id',$edu_id)->update($inp_data);
		}else{
			unset($inp_data['edu_id']);
			$education = Education::create($inp_data);
		}
		
		return $education; 
	}
	
    public function delEducationProcess(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);exit;
		if(!empty($input_data))
		{
			$cond=array();
			$cond['id']	= $input_data['id'];
			$cond['user_id']= Auth::user()->id;	
			$qdata = Education::where($cond)->delete();	
			return $qdata;
		}
	}
	
	public function addTropicProcess(Request $request)
	{
		$inp_data = $request->all();
		//print_r($inp_data);exit;
		$user = Auth::user();
		if(!empty($inp_data['ids']))
		{
			Tropic::where('user_id',$user->id)->where('type',$inp_data['type'])->delete();
			$ids = explode(',',trim($inp_data['ids']));
			if(!empty($ids))
			{
				foreach($ids as $id)
				{
					if(!empty($id))
					{
						$tropic = AllTropic::find($id);
						$ins_data['user_id'] = $user->id;
						$ins_data['tropic_id'] = $id;
						$ins_data['group_id'] = $tropic->group_id;
						$ins_data['type'] = $inp_data['type'];
						$user_tropic = Tropic::create($ins_data);						
					}					
				}
			}		
		}
		return $user->tropics; 
	}
	
    public function delTropicProcess(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);exit;
		if(!empty($input_data))
		{
			$cond=array();
			$cond['id']	= $input_data['id'];
			$cond['user_id']= Auth::user()->id;	
			$qdata = Tropic::where($cond)->delete();	
			return $qdata;
		}
	}
	
    public function userUpdateProcess(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);exit;
		$user_data =array();
		if(!empty($input_data))
		{
			if(isset($input_data['fullname']) && $input_data['fullname']!='')
			{
				$fullname = explode(' ',$input_data['fullname']);
				$user_data = [
					'first_name' => isset($fullname[0])?$fullname[0]:'',
					'last_name' => isset($fullname[0])? str_replace($fullname[0].' ','',$input_data['fullname']) :'',
					];
			}
			
			if(isset($input_data['nickname']) && $input_data['nickname']!='')
			{
				$user_data = [
					//'nickname' => isset($input_data['nickname'])? str_replace(' ','',$input_data['nickname']) :'',
					'nickname' => isset($input_data['nickname'])? $input_data['nickname'] :'',
				];
			}
			if(isset($input_data['gender']) && $input_data['gender']!='')
			{
				$user_data = [
					'gender' => $input_data['gender'],
				];
			}
			if(isset($input_data['year']) )
			{
				$doob  = ($input_data['day']!='')?$input_data['day'].'-':'1'.'-';
				$doob .= ($input_data['month']!='')?$input_data['month'].'-':'1'.'-';
				$doob .= $input_data['year'];
					$today = time();
					$dobtime = strtotime($doob);
					$age = round((int)($today - $dobtime)/(86400*365));
					
				$user_data = [
					'dob' => $doob,
					'age' => $age,
				];
			}
			
			if(isset($input_data['recovery_email']) && $input_data['recovery_email']!='')
			{
				$user_data = [
					'recovery_email' => $input_data['recovery_email'],
				];
			}
			if(isset($input_data['password']) && $input_data['password']!="")
			{
					
				\Validator::extend('pwdvalidation', function($field, $value, $parameters)
				{
					return \Hash::check($value, Auth::user()->password);
				});

				$messages = array('pwdvalidation' => 'The Old Password is Incorrect');
				$this->validate($request, [
					'old_password' => 'required|pwdvalidation',
					'password' => 'required|confirmed|min:6',
				]);	
				
				$user_data = [
					'password' => bcrypt($input_data['password']),
				];				
			}
			
			$usrdata = Auth::user()->fill($user_data)->save();	
			$usr = Auth::user();
			$today = time();
			//$dobtime = strtotime($usr->dob);
			//$age = round((int)($today - $dobtime)/(86400*365));
			//$usr->age = $age;
			if(isset($input_data['password']) && $input_data['password']!="")
			{
				$usr->change_pass=1;
			}
			return $usr;
		}
	}
	public function getCompanyAutocomplete(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);
		$searchdata = Company::where('title', 'like', '%'.$input_data['term'].'%')->get();
		$retun_arr = array();
		if(!empty($searchdata))
		{
			foreach($searchdata as $val)
			{
				$retun_arr[] = $val->title;
			}
		}
		return json_encode($retun_arr);
	}
	public function getJobAutocomplete(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);
		$searchdata = Job::where('title', 'like', '%'.$input_data['term'].'%')->get();
		$retun_arr = array();
		if(!empty($searchdata))
		{
			foreach($searchdata as $val)
			{
				$retun_arr[] = $val->title;
			}
		}
		return json_encode($retun_arr);
	}
	public function getUniversityAutocomplete(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);
		$searchdata = University::where('title', 'like', '%'.$input_data['term'].'%')->get();
		$retun_arr = array();
		if(!empty($searchdata))
		{
			foreach($searchdata as $val)
			{
				$retun_arr[] = $val->title;
			}
		}
		return json_encode($retun_arr);
	}
	public function getSchoolMajorAutocomplete(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);
		$searchdata = SchoolMajor::where('title', 'like', '%'.$input_data['term'].'%')->get();
		$retun_arr = array();
		if(!empty($searchdata))
		{
			foreach($searchdata as $val)
			{
				$retun_arr[] = $val->title;
			}
		}
		return json_encode($retun_arr);
	}
	
    public function setting()
    {
		$setting = app('settings');
		$current_url= \Request::path();
		$user = Auth::user();
		$all_tropics = AllTropic::all();
        return view('user.setting',compact('user','setting','current_url','all_tropics'));
    }
    public function privacy()
    {
		$setting = app('settings');
		$current_url= \Request::path();
		$user = Auth::user();
		$all_tropics = AllTropic::all();
		$prevdata = \App\PrivacySetting::where('user_id',$user->id)->first();
        return view('user.privacy',compact('user','setting','current_url','prevdata','all_tropics'));
    }
    public function notification()
    {
		$setting = app('settings');
		$current_url= \Request::path();
		$user = Auth::user();
		$all_tropics = AllTropic::all();
		$prevdata = \App\NotificationSetting::where('user_id',$user->id)->first();
        return view('user.notification',compact('user','setting','current_url','prevdata','all_tropics'));
    }
	
	public function updatePrivacySettings(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);
		
		$user = Auth::user();
		$retun_arr = array();
		if(!empty($input_data))
		{
			$ins_data['user_id'] = $user->id;
			$prevdata = \App\PrivacySetting::where('user_id',$user->id)->first();
			$ins_data = [
			'online' => isset($input_data['online'])?$input_data['online']:'N',
			'other_view_account' => isset($input_data['other_view_account'])?$input_data['other_view_account']:'N',
			'want_receive_message' => isset($input_data['want_receive_message'])?$input_data['want_receive_message']:'A',
			'decline_request' => isset($input_data['decline_request'])?$input_data['decline_request']:'N',
			'request_intelval' => isset($input_data['request_intelval'])?$input_data['request_intelval']:'D',
			'max_request' => isset($input_data['max_request'])?$input_data['max_request']:0,
			'allow_comments' => isset($input_data['allow_comments'])?$input_data['allow_comments']:'N',
			'deactivate_account' => isset($input_data['deactivate_account'])?$input_data['deactivate_account']:'N',
			];
			
			if(!empty($prevdata))
			{
				if(isset($ins_data['deactivate_account']))
				{
					$deactivate=($prevdata->deactivate_account=='N')?'Y':'N';
					$ins_data['deactivate_account'] = $deactivate;					
				}
				$updata = \App\PrivacySetting::where('user_id',$user->id)->update($ins_data);
				if($updata == 1)	
					$retun_arr = \App\PrivacySetting::where('user_id',$user->id)->first();
				
			}else{
				$retun_arr = \App\PrivacySetting::create($ins_data);				
			}
			
		}
		return json_encode($retun_arr);
	}
	
	public function updateNotificationSettings(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);
		
		$user = Auth::user();
		$retun_arr = array();
		if(!empty($input_data))
		{
			$prevdata = \App\NotificationSetting::where('user_id',$user->id)->first();
			$ins_data = [
			'user_id' => $user->id,
			'answer_question' => isset($input_data['answer_question'])?$input_data['answer_question']:'N',
			'upvote' => isset($input_data['upvote'])?$input_data['upvote']:'N',
			'new_follower' => isset($input_data['new_follower'])?$input_data['new_follower']:'N',
			'new_nessage' => isset($input_data['new_nessage'])?$input_data['new_nessage']:'N',
			'tag_me' => isset($input_data['tag_me'])?$input_data['tag_me']:'N',
			'comment_my' => isset($input_data['comment_my'])?$input_data['comment_my']:'N',			
			];
			
			if(!empty($prevdata))
			{
				
				$updata = \App\NotificationSetting::where('user_id',$user->id)->update($ins_data);
				if($updata == 1)	
					$retun_arr = \App\NotificationSetting::where('user_id',$user->id)->first();
				
			}else{
				$retun_arr = \App\NotificationSetting::create($ins_data);				
			}
			
		}
		return json_encode($retun_arr);
	}
	
	public function setDefaultAddress(Request $request)
    {
		$input_data = $request->all();
		$user = Auth::user();
		if(isset($input_data['address_id']) && $input_data['address_id']!='')
		{
			UserAddres::where('user_id',$user->id)->update(array('is_default'=>'N'));
			$udata = UserAddres::where('id',$input_data['address_id'])->update(array('is_default'=>'Y'));	
			return UserAddres::where(array('is_default'=>'Y','user_id'=>$user->id))->first();
		}
		else	return 0;
	}	
	
	public function addFavorite(Request $request)
    {
		$input_data = $request->all();
		//print_r($input_data);
		$user = Auth::user();
		$fav = \App\FavoriteTropic::where(['tropic_id'=>$input_data['tropic_id'],'user_id'=>$user->id])->first();
		//dd($fav);
		if(count($fav)>0)
		{
			$fav->delete();
		}else{
			$fav = \App\FavoriteTropic::create(['tropic_id'=>$input_data['tropic_id'],'user_id'=>$user->id]);
		}
		return $fav;
	}
		
	public function searchUsers(Request $request)
    {
		$input_data = $request->all();
		$user_filter = array();
		if(Auth::check())	$user_filter = (array) json_decode(Auth::user()->filterset_order->filter_set);
		/* print_r($user_filter);exit; */
		$users_arr=array();$uaddr=array();$uaddr2=array();$uaddr3=array();$jobs=array();$education=array();$major=array();$tropics=array();$departments=array();
		if((isset($input_data['loction']) && $input_data['loction']!='' && in_array('locationlive',$user_filter)) || (isset($input_data['loction']) && $input_data['loction']!='' && empty($user_filter))) 
		{
			$udata = UserAddres::select('user_id')->where('address', 'like', '%'.$input_data['loction'].'%')->where('type',1)->groupBy('user_id')->get();
			$udata = json_decode(json_encode($udata), true);
			$uaddr = array_column($udata,'user_id');		
		}
		
		if((isset($input_data['location_visit']) && $input_data['location_visit']!='' && in_array('locationvisit',$user_filter)) || (isset($input_data['location_visit']) && $input_data['location_visit']!='' && empty($user_filter))) 
		{
			$udata = UserAddres::select('user_id')->where('address', 'like', '%'.$input_data['location_visit'].'%')->where('type',2)->groupBy('user_id')->get();
			$udata = json_decode(json_encode($udata), true);
			$uaddr3 = array_column($udata,'user_id');		
		}
		
		if((isset($input_data['joblocation']) && $input_data['joblocation']!='' && in_array('workplace',$user_filter)) || (isset($input_data['joblocation']) && $input_data['joblocation']!='' && empty($user_filter))) 
		{
			$udata = WorkExperience::select('user_id')->where('company', 'like', '%'.$input_data['joblocation'].'%')->groupBy('user_id')->get();
			$udata = json_decode(json_encode($udata), true);
			$uaddr2 = array_column($udata,'user_id');		
		}
		
		if((isset($input_data['job']) && $input_data['job']!='' && in_array('profesion',$user_filter)) || (isset($input_data['job']) && $input_data['job']!='' && empty($user_filter))) 
		{
			$jobs = WorkExperience::select('user_id')->where('title', 'like', '%'.$input_data['job'].'%')->groupBy('user_id')->get();
			$jobs = json_decode(json_encode($jobs), true);
			$jobs = array_column($jobs,'user_id');		
		}
		
		if((isset($input_data['department']) && $input_data['department']!='' && in_array('depatment',$user_filter)) || (isset($input_data['department']) && $input_data['department']!='' && empty($user_filter))) 
		{
			$departments = WorkExperience::select('user_id')->where('department', 'like', '%'.$input_data['department'].'%')->groupBy('user_id')->get();
			$departments = json_decode(json_encode($departments), true);
			$departments = array_column($departments,'user_id');		
		}
		
		if((isset($input_data['education']) && $input_data['education']!='' && in_array('eductn',$user_filter)) || (isset($input_data['education']) && $input_data['education']!='' && empty($user_filter))) 
		{
			$education = Education::select('user_id')->where('institution', 'like', '%'.$input_data['education'].'%')->groupBy('user_id')->get();
			$education = json_decode(json_encode($education), true);
			$education = array_column($education,'user_id');		
		}
		
		if((isset($input_data['major']) && $input_data['major']!='' && in_array('majr',$user_filter)) || (isset($input_data['major']) && $input_data['major']!='' && empty($user_filter))) 
		{
			$major = Education::select('user_id')->where('major', 'like', '%'.$input_data['major'].'%')->groupBy('user_id')->get();
			$major = json_decode(json_encode($major), true);
			$major = array_column($major,'user_id');		
		}
		
		if((isset($input_data['tropicid']) && $input_data['tropicid']!='' && in_array('tpic',$user_filter)) || (isset($input_data['tropicid']) && $input_data['tropicid']!='' && empty($user_filter))) 
		{
			$tropics = Tropic::select('user_id')->where('tropic_id', $input_data['tropicid'])->groupBy('user_id')->get();
			$tropics = json_decode(json_encode($tropics), true);
			$tropics = array_column($tropics,'user_id');		
		}
		$filtr = Array ( 'prof' => $jobs, 'locl' => $uaddr, 'locv' => $uaddr3, 'dpt' => $departments, 'tpic' => $tropics, 'workpl' => $uaddr2, 'eduction' => $education, 'mjor' => $major );
		$users_arr = array_merge($uaddr,$uaddr2,$uaddr3,$jobs,$education,$major,$tropics,$departments);
		//
		if(!empty($user_filter))
		{
			$flarr=array();
			foreach($user_filter as $key=>$vl)
			{
				if(isset($filtr[$key]))
				{
					foreach($filtr[$key] as $avl)
					{
						$flarr[]=$avl;
					}
				}
			}
			foreach($filtr as $key=>$vl)
			{
				if(!array_key_exists($key,$user_filter))
				{
					foreach($vl as $avl)
					{
						$flarr[]=$avl;
					}					
				}
			}
			
			$users_arr = array_unique($flarr);			
			
		}
		/* print_r($flarr);exit;*///array_intersect
		$fage=18; $tage=100;
		
		if(!empty($users_arr)){
			$users_data = User::whereIn('id',$users_arr);
		}
				
		if((isset($input_data['age_from']) && $input_data['age_from']!='' && in_array('agee',$user_filter)) || (isset($input_data['age_from']) && $input_data['age_from']!='' && empty($user_filter)))
		{
			$fage=$input_data['age_from'];
			
		}
		if((isset($input_data['age_to']) && $input_data['age_to']!='' && in_array('agee',$user_filter)) || (isset($input_data['age_to']) && $input_data['age_to']!='' && empty($user_filter)))
		{
			$tage=$input_data['age_to'];
		}
		$users_data = $users_data->where([['age','>=',$fage]])->where([['age','<=',$tage]]);
		
		if((isset($input_data['gender']) && $input_data['gender']!='' && in_array('gendr',$user_filter)) || (isset($input_data['gender']) && $input_data['gender']!='' && empty($user_filter)))
		{
			$users_data = $users_data->where('gender',$input_data['gender']);
		}
		$users_data = $users_data->get();
		/* print_r($users_data->toArray());exit; */
		$ajax_html = view('include.peoplebox',compact('users_data'))->render();
		return $return_data = ['has_error' => 0, 'list_type' => $input_data['list_type'],'match_count' => count($users_data), 'ajax_html' => $ajax_html];
		
	}
	
	public function showAdvanceSearch()
	{
		return  view('include.advance_search')->render();
	}
	public function searchPeople()
	{
		$all_tropics = AllTropic::all();
		return  view('search-people',compact('all_tropics'));
	}
	
	public function postQuestion(Request $request)
	{
		$input_data = $request->all();
		$time = time();
		$input_data['user_id'] = Auth::user()->id;
		//
		if($request->hasFile('question_attached')){
            /* $old_image = 'assets/upload/question_attached/'.$question->attached;
            \File::delete($old_image); */
			
            $path   = public_path().'/assets/upload/question_attached/';
            $image  = $request->file('question_attached');
            $save_name = $time.str_random(10).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->save($path . $save_name, 100);
            $input_data['question_attached'] = $save_name;
        }
		$question = AskQuestion::create($input_data);
		//echo "<pre>";print_r($question);exit;
		return redirect()->back()->with('askqsn_success', 'Question post successfull.');
	}
	
	public function addContactinfo(Request $request)
	{
		$input_data = $request->all();
		//print_r($input_data);
		$user = Auth::user();
		$return_data = 0;
		if(isset($input_data['info_id']) && $input_data['info_id']!='')
		{
			$info_id = $input_data['info_id']; unset($input_data['info_id']);
			$res = \App\ContactIfno::where('id',$info_id)->update($input_data); 
		}
		else{
			$input_data['user_id'] = $user->id;
			$res = \App\ContactIfno::create($input_data);
			
		}
		$return_data = view('include.user_contact_info',compact('user'))->render();
		return $return_data;
	}
	
	public function deleteContactinfo(Request $request)
	{
		$input_data = $request->all();
		//print_r($input_data);
		$user = Auth::user();
		if(isset($input_data['info_id']) && $input_data['info_id']!='')
		{
			$res= \App\ContactIfno::where('id',$input_data['info_id'])->delete();
		}
		return $res;
	}
	
	public function setFilter(Request $request)
	{
		$input_data = $request->all();
		//print_r($input_data);
		$user = Auth::user();
		$prev_data = FilterOrderSet::where('user_id',$user->id)->first();
		if(!empty($prev_data))
		{
			$prev_data->filter_set= json_encode($input_data);
			$prev_data->save();
		}else{
			$setdata = FilterOrderSet::create(['user_id'=> $user->id,'filter_set'=>json_encode($input_data)]);			
		}
		return 1;
	}

	
}
