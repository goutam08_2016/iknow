<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;



use App\Http\Requests;

use App\Package;

use App\AllTropic;

use App\Friend;
use App\Chatmessage;
use App\ContactMessages;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;



class HomeController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */
	public $chat_messsage_data = array();
	public $alpha_set = [
		['start'=>'0','end'=>'3'],
		['start'=>'4','end'=>'7'],
		['start'=>'8','end'=>'11'],
		['start'=>'12','end'=>'15'],
		['start'=>'16','end'=>'19'],
		['start'=>'20','end'=>'23'],
		['start'=>'24','end'=>'25']
	];
	public $alphabets = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
	
    public function __construct()
    {
		$user = Auth::user();
        $unread_msg = [];$chat_users = [];

        if(Auth::check())
        {
			$chat_users = Friend::where(['to_user_id' => $user->id, 'status'=>'Y'])->orWhere(['from_user_id' => $user->id, 'status'=>'Y'])->orderBy('created_at', 'desc')->get();
			
            $unread_msg = Chatmessage::where([
                                    ['to_user_id', '=', $user->id],
                                    ['to_user_view_status', '=', 'N']
                                ])
                                ->orderBy('created_at', 'desc')
                                ->get();
        }
		//dd($chat_users->toArray());
        $this->chat_messsage_data = [
            'chat_users' => $chat_users,
            'unread_msg' => $unread_msg
        ];
    }
	
    public function index()
    {
		$data = [];
        $user = Auth::user();
        if (Auth::check()) {
            if ($user->status == 'E') {
                return redirect(url('/user/signup-step/'.$user->in_step));
            }else{
                return redirect(url('/user/account'));
            }
        }
        return view('index', $data);

    }

    public function registerAs()

    {	

		$data = [];

        return view('register-as', $data);

    }
	
	public function tropicPage($id=0)
    {
		$tropic = array(); $userfollows = array();
		$tropic = AllTropic::find($id);
		$user = Auth::User();
		if(empty($tropic))
		{			
			return redirect(url('topics'));
			
		}
		if($user)
		{
			if(!empty($user->question_follows))
			{
				foreach($user->question_follows as $val)
				{
					$userfollows[] = $val->question_id;
				}
			}
		}
		//$p_qs = \App\AskQuestion::where()
		$pinned_questions = \DB::table('ask_questions')
			->join('tropic_questions', 'tropic_questions.question_id', '=', 'ask_questions.id')
			->where('tropic_questions.tropic_id', '=', $tropic->id )
			->where('ask_questions.is_pinned', '=', 1 )
			->orderBy('ask_questions.pinned_time','DESC')
			->limit(4 )
			->get();
		//dd($pinned_questions);
		$data = [
			'user' => $user,
			'pinned_questions' => $pinned_questions,
			'tropic' => $tropic,
			'userfollows' => $userfollows,
			'chat_messsage_data' => $this->chat_messsage_data
		];
		return view('topic-page', $data);
	}
	
    public function topics()
    {
		if(Input::get('trpc'))
		{
			$trpc = Input::get('trpc');
			
			$all_data = \DB::select( \DB::raw("SELECT SUBSTR(title, 1, 1) AS first_alpha FROM all_tropics WHERE `title` LIKE '%".$trpc."%' GROUP BY first_alpha ") );
		}			
		else{			
			$all_data = \DB::select( \DB::raw("SELECT SUBSTR(title, 1, 1) AS first_alpha FROM all_tropics WHERE 1 GROUP BY first_alpha ") );
		}
		
		$noof_pages = ceil(count($all_data)/4);
		//echo "<pre>";print_r($all_data);exit;
		$page=1;
		if(Input::get('page'))
		{
			$page = Input::get('page');
		}
		$page_no=$page-1;
		$i = 0;
		$page_alpha_arr = array();
		$user_favs = array();
		if(Auth::check())
		{
			$user_favorites = Auth::user()->user_favorites;
			//dd($user_favorites->toArray());
			if(count($user_favorites)>0)
			{
				foreach($user_favorites as $val)
				{
					$user_favs[]=$val->tropic_id;
				}
			}
		}
		$topics = AllTropic::where('id', '<>', 0);
		$fav_topics = AllTropic::whereIn('id',$user_favs);
		if(Input::get('alph'))
		{
			$alph = Input::get('alph');
			$topics = $topics->where('title', 'like', $alph.'%');
			$fav_topics = $fav_topics->where('title', 'like', $alph.'%');
		}
		if(Input::get('trpc'))
		{
			$trpc = Input::get('trpc');
			$topics = $topics->where('title', 'like', '%'.$trpc.'%');
			$fav_topics = $fav_topics->where('title', 'like', '%'.$trpc.'%');
		}	/*  */	
		$topics = $topics->orderBy('title')->get();
		$fav_topics = $fav_topics->orderBy('title')->get();
		
		$start_ky= $page_no*4;
		$end_ky= ($page*4)-1;
		$fav_alphabets = array();
		if(count($fav_topics)>0)
		{
			foreach($fav_topics as $fa)
			{
				if(!in_array($fa->title[0],$fav_alphabets))	$fav_alphabets[] = $fa->title[0];
			}
		}
		$all_tropics = AllTropic::all();
		//dd($fav_alphabets);
		$data = [
			'topics' => $topics,
			'all_data' => $all_data,
			'alphabets' => $this->alphabets,
			'noof_pages' => $noof_pages,
			'start_ky' => $start_ky,
			'end_ky' => $end_ky,
			'user_favs' => $user_favs,
			'fav_topics' => $fav_topics,
			'fav_alphabets' => $fav_alphabets,
			'all_tropics' => $all_tropics,
			'chat_messsage_data'=>$this->chat_messsage_data,
		];

        return view('topics', $data);

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        //

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        //

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        //

    }

	

	public function accountUpgrade(Request $request)

    {

		$packages= Package::all();

		$current_plan = array();

		if(Auth::check())

		{

			$user = Auth::user();			

			if(!empty($user->current_package[0])){

				$current_plan = $user->current_package[0];

			}

			//dd($current_plan->plan);

		}

		//dd($user->toArray());

		

		$slug="pricing";

		$cmsdata = \App\PageTranslation::where('locale',app()->getLocale())->where('slug',$slug)->first();

        return view('user.upgrade-account',compact('current_plan','packages','cmsdata'));

    }

	public function contactUs()

    {

		$setting = app('settings');

		$slug="contact-us";

		$cmsdata = \App\PageTranslation::where('locale',app()->getLocale())->where('slug',$slug)->first();

		//dd($cmsdata->toArray());

        return view('contact-us',compact('setting','cmsdata'))->with('chat_messsage_data',$this->chat_messsage_data);

    }

	

	public function contactProcess(Request $request)

    {

		$input_data = $request->all();

		$this->validate($request, [

           'name' => 'required',

           'email' => 'required|email',

		  // 'email_confirmation' => 'required|same:email',

           'comments' => 'required',

        ]);

		//dd($input_data);

		$mailcontent = $input_data['comments'];

		$to=app('settings')->admin_email;

		

		$headers ="From: ".$input_data['name']."<".$input_data['email'].">\n";

		$headers .= "MIME-Version: 1.0\n"; 

		$headers .= "Content-type: text/html; charset=UTF-8\n"; 

		$subject = "Contact Mail Notification";

		$message ="<html><head></head><body>"."<style type=\"text/css\">

		<!--

		.style4 {font-size: x-small}

		-->

		</style>

		".$mailcontent."

		</body></html>"; 

		@mail($to,$subject, $message,$headers);

		

		$contactMessage =new ContactMessages;

		$contactMessage->sender_name = $input_data['name'];

		$contactMessage->sender_email = $input_data['email'];

		//$contactMessage->phone = $input_data['phone'];

		$contactMessage->comments = $input_data['comments'];

		$contactMessage->save(); /* */		

		return redirect(url('contact-us'))->with('success', 'Thank you for contacting us. We will get back to you within 1 business day.');

	}

	

	public function getPackage(Request $request)

    {

		$input_data = $request->all();

		//print_r($input_data);

		$package = Package::find($input_data['id']);

		if(!empty($package))

				echo json_encode($package);

		else	echo "error";

	}

	

}

