<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here we register all routes for Admin..
|
*/
/**
 * Define uri prefix for admin control pages..
 */
define('ADMIN_PREFIX', 'admin');

Route::group(['prefix' => ADMIN_PREFIX], function() {

	// Login Routes...
    Route::get('login','AdminAuth\AuthController@showLoginForm');
    Route::post('login','AdminAuth\AuthController@login');
    Route::get('logout','AdminAuth\AuthController@logout');
    // All authenticated routes..
    Route::group(['middleware' => 'admin'], function() {
        Route::get('', 'Admin\AdminController@index');
        Route::get('/dashboard', 'Admin\AdminController@index');
        Route::get('settings', 'Admin\AdminController@settings');
        Route::post('settings', 'Admin\AdminController@updateSettings');

        Route::resource('user', 'Admin\UserController', ['only' => [
            'index', 'edit', 'update', 'destroy'
        ]]);
		/* Route::resource('package', 'Admin\PackageController', ['only' => [
            'index', 'create', 'store', 'show','edit', 'update', 'destroy'
        ]]); */		
		Route::resource('degree', 'Admin\DegreeController', ['only' => [
            'index', 'create', 'store', 'show','edit', 'update', 'destroy'
        ]]);
		Route::resource('group', 'Admin\TropicGroupController', ['only' => [
            'index', 'create', 'store', 'show','edit', 'update', 'destroy'
        ]]);
		Route::resource('tropic', 'Admin\TropicController', ['only' => [
            'index', 'create', 'store', 'show','edit', 'update', 'destroy',
        ]]);
		Route::resource('email-template', 'Admin\EmailTemplateController', ['only' => [
            'index', 'create', 'store', 'show','edit', 'update', 'destroy'
        ]]);	
		Route::resource('categories', 'Admin\CategoryController', ['only' => [
            'index', 'create', 'store', 'show','edit', 'update', 'destroy'
        ]]);		
        Route::get('blog/admin-blog', 'Admin\BlogController@adminBlogList');
		Route::resource('blog', 'Admin\BlogController', ['only' => [
            'index', 'create', 'store', 'show','edit', 'update', 'destroy'
        ]]);		
        Route::resource('adsences', 'Admin\AdsenceController');
        Route::resource('phone_type', 'Admin\PhoneTypeController');
        Route::get('{locale}/phone_type', 'Admin\PhoneTypeController@index');
        Route::delete('{locale}/phone_type/{id}', 'Admin\PhoneTypeController@deleteTranslation');		
		Route::resource('cms-page', 'Admin\CmsPageController');
        Route::get('{locale}/cms-page', 'Admin\CmsPageController@index');
        Route::delete('{locale}/cms-page/{id}', 'Admin\CmsPageController@deleteTranslation');
		Route::get('user/{id}/invoices', 'Admin\UserController@invoices');
		Route::get('user/{user_id}/invoice-details/{id}', 'Admin\UserController@showInvoice');
		Route::get('user/{user_id}/add-invoice', 'Admin\UserController@addInvoice');
		Route::post('user/add-invoice', 'Admin\UserController@addInvoiceProcess');
		Route::get('topic/add-csv', 'Admin\TropicController@addCsv');
		Route::post('topic/add-csv', 'Admin\TropicController@uploadCsv');
		Route::get('csv/company-csv', 'Admin\CsvController@manageCsv');
		Route::post('csv/company-csv', 'Admin\CsvController@uploadCsv');
		Route::get('csv/job-csv', 'Admin\CsvController@manageCsv');
		Route::post('csv/job-csv', 'Admin\CsvController@uploadCsv');
		Route::get('csv/school-csv', 'Admin\CsvController@manageCsv');
		Route::post('csv/school-csv', 'Admin\CsvController@uploadCsv');
		Route::get('csv/major-csv', 'Admin\CsvController@manageCsv');
		Route::post('csv/major-csv', 'Admin\CsvController@uploadCsv');
		Route::post('user/block/', 'Admin\UserController@block');
		
		Route::post('blog/block', 'Admin\BlogController@block');
		
		Route::post('question/block', 'Admin\QuestionController@block');
		
		Route::resource('company', 'Admin\CompanyController', ['only' => [
            'index', 'destroy'
        ]]);
		Route::resource('job', 'Admin\JobController', ['only' => [
            'index', 'destroy'
        ]]);
		Route::resource('school', 'Admin\SchoolController', ['only' => [
            'index', 'destroy'
        ]]);
		Route::resource('major', 'Admin\MajorController', ['only' => [
            'index', 'destroy'
        ]]);
		Route::resource('question', 'Admin\QuestionController', ['only' => [
            'index', 'destroy',
        ]]);
		Route::post('question/pinned', 'Admin\QuestionController@pinnedProccess');
		Route::post('topic-delete-all', 'Admin\TropicController@deleteAll');
		Route::post('company-delete-all', 'Admin\CompanyController@deleteAll');
		Route::post('jobs-delete-all', 'Admin\JobController@deleteAll');
		Route::post('school-delete-all', 'Admin\SchoolController@deleteAll');
		Route::post('major-delete-all', 'Admin\MajorController@deleteAll');
		Route::post('question-delete-all', 'Admin\QuestionController@deleteAll');
		Route::post('blog-delete-all', 'Admin\BlogController@deleteAll');
		Route::post('degree-delete-all', 'Admin\DegreeController@deleteAll');
		Route::post('user-delete-all', 'Admin\UserController@deleteAll');
    });

});
