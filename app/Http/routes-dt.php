<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('access-denied', 'HomeController@accessDenied');
Route::group(['middlewareGroups' => 'web'], function() {
    Route::group(['middleware' => 'validcountry'], function() {

    // Route::auth();
    Route::post('login', 'Auth\AuthController@postLogin');
    Route::get('register', 'HomeController@getRegister');
    Route::get('register-success/{token}', 'HomeController@getRegisterSuccess');
    Route::post('resend-confirmation-email', 'HomeController@resendConfirmationEmail');
    Route::post('register', 'Auth\AuthController@postRegister');
    Route::get('register/verify/{code}', 'UserController@activate');
    Route::post('get_city', 'HomeController@getCity');
    Route::get('logout', 'Auth\AuthController@logout');

    Route::get('/', 'HomeController@index');

    Route::get('step2', function() {
       return view('ad.postAdStep2');
    });

    Route::get('user/public-profile/{user_id_key}', 'UserController@publicProfile');
    Route::get('user/profile-details/{user_id}', 'UserController@profileDetails');
    Route::post('user/get-online-profile-list', 'UserController@getOnlineProfileList');
    Route::get('user/unsubscribe-daily-emails/{user_id}', 'UserController@unsubscribeDailyEmails');
    Route::get('daily-email-unsubscribe-success', 'UserController@unsubscribeDailyEmailSuccess');
    // All authenticated routes...
    Route::group(['middleware' => 'auth'], function() {
        // Post Ad and Edit Ad routes...

        Route::group(['prefix' => 'user'], function() {
            /*Route::get('account', 'UserController@account');
            Route::post('update-account', 'UserController@updateAccount');*/
            /*Route::get('addlangtext', 'UserController@addlangtext');*/
            Route::match(['get', 'post'], 'change-password', 'UserController@changePassword');
            Route::resource('edit-profile', 'UserController@editProfiledetails');
            Route::match(['get', 'post'], '{locale}/edit-profile', 'UserController@editProfiledetails');
            Route::post('edit-profile-process', 'UserController@editProfileProcess');
            Route::post('upload-profile-picture/{is_profile?}', 'UserController@uploadProfilePicture');
            Route::post('profile-image-upload-with-crop/{is_profile?}', 'UserController@profileImageUploadWithCrop');
            Route::post('delete-profile-image', 'UserController@deleteProfileImage');
            Route::get('photos/{user_id}', 'UserController@photos');
            Route::get('make-current-profile-pic/{photo_id}', 'UserController@makeCurrentProfilePic');
            Route::get('delete-profile-pic/{photo_id}', 'UserController@deleteProfilePic');
            /*Route::get('profile-details/{user_id}', 'UserController@profileDetails');*/
            Route::get('my-profile', 'UserController@myProfileDetails');
            Route::get('my-favourites', 'UserController@myFavourites');
            Route::get('{locale}/search', 'UserController@seachProfile');
            Route::post('{locale}/get-search', 'UserController@getSeachProfile');
            Route::post('{locale}/get-search-profile-list', 'UserController@getSeachProfileList');
            Route::get('flirt', 'UserController@flirt');
            Route::get('viewer-list', 'UserController@viewerList');
            Route::get('{locale}/membership', 'UserController@membership');
            Route::post('{locale}/membershipUpgradeProcess', 'UserController@membershipUpgradeProcess');
            Route::get('membership-payment/{membership_id}', 'UserController@membershipPayment');
            Route::post('membershipPaymentProcess', 'UserController@membershipPaymentProcess');
            Route::get('membership-payment-success', 'UserController@membershipPaymentSuccess');

            Route::get('{locale}/credit', 'UserController@credit');
            Route::post('{locale}/creditBuyProcess', 'UserController@creditBuyProcess');
            Route::get('credit-payment/{credit_id}', 'UserController@creditPayment');
            Route::post('creditPaymentProcess', 'UserController@creditPaymentProcess');
            Route::get('credit-payment-success', 'UserController@creditPaymentSuccess');

            Route::get('online-users', 'UserController@onlineUsers');
            Route::match(['get', 'post'], 'notification-settings', 'UserController@notificationSettings');
            /*Route::post('get-online-profile-list', 'UserController@getOnlineProfileList');*/
            Route::get('settings', 'UserController@settings');
            Route::post('updateSettings', 'UserController@updateSettings');


        });
        //9/8/2016
        Route::group(['prefix' => 'ajax'], function() {
            Route::post('make_favourite', 'AjaxController@make_favourite');
            Route::post('remove_favourite', 'AjaxController@remove_favourite');
            Route::get('unseenViewerCount', 'AjaxController@unseenViewerCount');
            Route::post('flirtUserDetails', 'AjaxController@flirtUserDetails');
            Route::post('deleteJunkMessage', 'AjaxController@deleteJunkMessage');
          });

        /**Chat/message related routes**/
        Route::group(['prefix' => 'chat'], function() {
            Route::post('addToChat', 'ChatController@addToChat');
            Route::get('messages', 'ChatController@messages');
            Route::post('startChat', 'ChatController@startChat');
            Route::post('startChatMessage', 'ChatController@startChatMessage');
            Route::post('sendMessage', 'ChatController@sendMessage');
            Route::post('uploadChatfile', 'ChatController@uploadChatfile');
            Route::post('loadUnreadMessages', 'ChatController@loadUnreadMessages');
            Route::get('updateLoginStatus', 'ChatController@updateLoginStatus');
            Route::post('listFlirtMessage', 'ChatController@listFlirtMessage');
            Route::post('sendFlirtMessage', 'ChatController@sendFlirtMessage');
            Route::get('updateNotification', 'ChatController@updateNotification');
            Route::post('viewNotification', 'ChatController@viewNotification');
          });

        Route::group(['prefix' => 'chat-pop'], function() {
            Route::post('addToChatPop', 'ChatController@addToChatPop');
            Route::post('startChatPop', 'ChatController@startChatPop');
            Route::post('sendMessagePop', 'ChatController@sendMessagePop');
            Route::post('uploadChatfilePop', 'ChatController@uploadChatfilePop');
            Route::post('loadUnreadMessagesPop', 'ChatController@loadUnreadMessagesPop');
          });

    });
    /*---------------------- Forgot Password Section -----------------------*/
    Route::get('forget-password','ForgotPasswordController@forgetPassword');
    Route::post('forget-password','ForgotPasswordController@forgotPasswordProcess');
    Route::get('reset-password/{token}','ForgotPasswordController@resetPassword');
    Route::get('reset-password','ForgotPasswordController@resetPassword');
    Route::post('reset-password','ForgotPasswordController@resetPassProcess');

    /*****************Cron job related routing****************/

    Route::group(['prefix' => 'cronjob'], function() {
            Route::get('loginStatusCheckUpdate', 'CronController@loginStatusCheckUpdate');
            Route::get('sendBotMessages', 'CronController@sendBotMessages');
            Route::get('checkExpireMembership', 'CronController@checkExpireMembership');
            Route::get('dailyEmails', 'CronController@dailyEmails');
            Route::get('sendBotMessagesNewMember', 'CronController@sendBotMessagesNewMember');
            Route::get('setModelOnline', 'CronController@setModelOnline');
            Route::get('setInterest', 'CronController@setInterest');
            Route::get('deleteUnwantedModels', 'CronController@deleteUnwantedModels');
            Route::get('deleteUnwantedMessages', 'CronController@deleteUnwantedMessages');
          });

    /*****************CMS Page routes************************/
        Route::get('about', 'HomeController@about');
        Route::get('affilite', 'HomeController@affilite');
        Route::get('faq', 'HomeController@faq');
        Route::get('privacy-cookies', 'HomeController@privacyCookies');
        Route::get('terms-conditions', 'HomeController@termsConditions');
        Route::get('content-removal', 'HomeController@contentRemoval');
        Route::get('disclaimer', 'HomeController@disclaimer');
        Route::get('privacy-policy', 'HomeController@privacyPolicy');
        Route::get('contact-us', 'HomeController@contactUs');
        Route::post('contact-us', 'HomeController@postContactUs');
        Route::get('security', 'HomeController@security');
        Route::get('blog', 'HomeController@blog');
        Route::get('toys', 'HomeController@toys');
    });
});