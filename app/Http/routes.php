<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();
Route::get('topics', 'HomeController@topics');
Route::get('topic/{id}', 'HomeController@tropicPage');
Route::get('register-as', 'HomeController@registerAs');
Route::get('register/{type}', 'Auth\AuthController@showRegistrationForm');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('register/verify/{code}/{utype}', 'UserController@activate');
Route::get('pricing', 'HomeController@accountUpgrade');
Route::post('pricing', 'HomeController@accountUpgrade');
Route::get('contact-us', 'HomeController@contactUs');
Route::post('contact-us', 'HomeController@contactProcess');
Route::post('get-package', 'HomeController@getPackage');
Route::post('social-login', 'Auth\AuthController@socialLogin');

/*---------------------- Forgot Password Section -----------------------*/
Route::get('forget-password','ForgotPasswordController@forgetPassword');
Route::post('forget-password','ForgotPasswordController@forgotPasswordProcess');
Route::get('reset-password/{token}','ForgotPasswordController@resetPassword');
Route::get('reset-password','ForgotPasswordController@resetPassword');
Route::post('reset-password','ForgotPasswordController@resetPassProcess');
/*---------------------- Forgot Password Section -----------------------*/

Route::get('profile/{id}', 'UserController@account');
Route::get('confirm-pass-question', 'UserController@confirmPassQuestion');

Route::group(['middleware' => 'auth'], function() {
	Route::group(['prefix' => 'user'], function() {
		Route::get('signup-step/{step}', 'UserController@signupStep');
		Route::post('signup-step/{step}', 'UserController@signupStepProcess');
		Route::get('question-home','UserController@question_home');
		Route::get('sent-question','UserController@user_sent_question');
		Route::get('received-question','UserController@user_received_question');
		Route::get('account', 'UserController@account');
		Route::post('add-address/', 'UserController@addAddress');
		Route::post('add-workexp/', 'UserController@addWorkExpProcess');
		Route::post('add-education/', 'UserController@addEducationProcess');
		Route::post('add-tropic/', 'UserController@addTropicProcess');
		Route::post('del-workexp', 'UserController@delWorkExpProcess');
 		Route::post('del-education', 'UserController@delEducationProcess');
  		Route::post('del-tropic', 'UserController@delTropicProcess');
  		Route::post('del-address', 'UserController@delAddress');
		Route::post('update-userdata', 'UserController@userUpdateProcess');
		Route::get('setting', 'UserController@setting');
		Route::get('privacy', 'UserController@privacy');
		Route::get('notification', 'UserController@notification');
		Route::post('update-privacy-settings', 'UserController@updatePrivacySettings');
		Route::post('update-notify-settings', 'UserController@updateNotificationSettings');
		Route::post('setdeflt-address', 'UserController@setDefaultAddress');
		Route::get('question-details/{id}','UserController@questionDetails');
		Route::post('answer-process', 'UserController@answerProcess');
		
		/* Route::get('pay-notify/{id}/{user_id}', 'UserController@paymentNotify');
		Route::post('pay-notify', 'UserController@paymentPostNotify');
		Route::get('upgrade', 'UserController@accountUpgrade');
		Route::post('upgrade', 'UserController@accountUpgradeProcess');
		Route::get('my-details', 'UserController@myDetails');
		Route::post('update-account', 'UserController@updateAccount'); */
		Route::post('add-favorite', 'UserController@addFavorite');
		Route::get('show-advance-search', 'UserController@showAdvanceSearch');
		Route::post('post-question', 'UserController@postQuestion');
		Route::post('add-contactinfo', 'UserController@addContactinfo');
		Route::post('del-contactinfo', 'UserController@deleteContactinfo');
		Route::get('show-askqsn', 'UserController@showAskqsn');
		Route::post('set-filter', 'UserController@setFilter');
		Route::post('get-question', 'UserController@getQuestionDetails');
		Route::post('del-question', 'UserController@deleteQuestion');
		Route::post('follow-unfollow', 'UserController@followUnfollow');
		Route::post('qsn-follow-unfollow', 'UserController@qsnFollowUnfollow');
		Route::post('comment-process', 'UserController@commentProcess');
		Route::post('like-comment', 'UserController@commentLikeProcess');
		Route::post('upvote-downvote', 'UserController@upvoteDownvoteProcess');
		Route::get('friend-request', 'UserController@showFriendRequest');
		Route::post('send-friend-request', 'UserController@sendFriendRequest');
		Route::post('accept-friend-request', 'UserController@acceptFriendRequest');
		Route::post('del-friend-request', 'UserController@deleteFriendRequest');
		Route::get('friends', 'UserController@showFriends');
		Route::get('followers', 'UserController@showFollowers');
		//Route::get('read', 'UserController@readList');
		Route::get('blog-catagories', 'UserController@blogCatagories');
		Route::post('add-fav-cat', 'UserController@addFavCat');
		Route::post('add-fav-friend', 'UserController@addFavFriend');
		Route::get('my-blogs', 'UserController@myBlogs');
		Route::post('blog-post', 'UserController@blogPost');
		Route::get('blog-list/{cat}', 'UserController@blogList');
		Route::get('blog-details/{id}', 'UserController@blogDetails');
		Route::post('blog-comment-process', 'UserController@blogCmmentProcess');
		Route::post('delete-blog', 'UserController@blogDeleteProcess');
		Route::post('get-blog', 'UserController@getBlog');
		Route::get('admin-blogs', 'UserController@adminBlogList');
		Route::get('get-user-autocomplete', 'UserController@getUserAutocomplete');
		Route::post('send-remind-msg', 'UserController@sendRemindMessage');
		Route::post('pass-question', 'UserController@passQuestion');
		Route::post('search-friends', 'UserController@searchFriends');
		Route::post('upload-dp', 'UserController@uploadDp');
	});
	 /**Chat/message related routes**/
	Route::group(['prefix' => 'chat'], function() {
		Route::post('addToChat', 'ChatController@addToChat');
		Route::get('messages', 'ChatController@messages');
		Route::get('messages/{id}', 'ChatController@messages');
		Route::post('startChat', 'ChatController@startChat');
		Route::post('startChatMessage', 'ChatController@startChatMessage');
		Route::post('sendMessage', 'ChatController@sendMessage');
		Route::post('uploadChatfile', 'ChatController@uploadChatfile');
		Route::post('loadUnreadMessages', 'ChatController@loadUnreadMessages');
		Route::get('updateLoginStatus', 'ChatController@updateLoginStatus');
		Route::post('listFlirtMessage', 'ChatController@listFlirtMessage');
		Route::post('sendFlirtMessage', 'ChatController@sendFlirtMessage');
		Route::get('updateNotification', 'ChatController@updateNotification');
		Route::post('viewNotification', 'ChatController@viewNotification');
	  });

	Route::group(['prefix' => 'chat-pop'], function() {
		Route::post('addToChatPop', 'ChatController@addToChatPop');
		Route::post('startChatPop', 'ChatController@startChatPop');
		Route::post('sendMessagePop', 'ChatController@sendMessagePop');
		Route::post('uploadChatfilePop', 'ChatController@uploadChatfilePop');
		Route::post('loadUnreadMessagesPop', 'ChatController@loadUnreadMessagesPop');
	  });
});
		Route::get('user/search-people', 'UserController@searchPeople');
		Route::post('user/search-users', 'UserController@searchUsers');
		Route::get('user/company-autocomplete', 'UserController@getCompanyAutocomplete');
		Route::get('user/job-autocomplete', 'UserController@getJobAutocomplete');
		Route::get('user/university-autocomplete', 'UserController@getUniversityAutocomplete');
		Route::get('user/major-autocomplete', 'UserController@getSchoolMajorAutocomplete');
		Route::get('clear-cache',function(){
			Cache::flush();
		});
Route::group(['middleware' => 'locale'], function() {
	Route::get('sl/{locale}', function ($locale) {
	});
	Route::get('/', 'HomeController@index');
});