@extends('layouts.app')



@section('pageTitle', 'Log in | ')



@section('customStyle')



@endsection



@section('content')

	<section class="innerpages">

    	<div class="wrapper clear">

        	<div class="middle-border-box registerform clear">				

				<div class="register-left loginPage" >

					<h2>Log In</h2>

					

					@if($errors->any())

						<div id="login_warnings" class="warnings" style="">

							<div id="login_error_message" class="error_message">

								@foreach ($errors->all() as $error)

									<p>{!! $error !!}</p>

								@endforeach

							</div>

						</div>

					@elseif(session('register_success'))

						<div class="success_message">

							<p>{!! session('register_success') !!}</p>

						</div>

						<?php session()->forget('register_success');?>

					@endif

					<form method="post" id="loginForm" action="{{ url('/login') }}" data-parsley-validate="">



						{{ csrf_field() }}

						<div class="formrow clear">

							<div class="formcol">

								<label class="">

									<span class="formTtl">Email :</span>

									<div class="formFld">

										<input type="email" name="email" data-parsley-trigger="change" required placeholder=""/>

									</div>

								</label>

							</div>

							<div class="formcol">

								<label class="">

									<span class="formTtl">Password :</span>

									<div class="formFld">

										<input type="password" name="password" required placeholder=""/>

									</div>

								</label>

							</div>

						</div>

						

						<div class="submit-row">

                        	<input type="submit" class="colorbtn bluebtn left" value="Log In"/>

                        </div>

						<div class="formrow terms">

                        	<label>

                            <span class="icon"></span><a href="{{ url('/password/reset') }}">Forgot your password?</a> </label>

                        </div>

					</form>

					

				</div>

				<div class="register-right">

                	<div class="others-btn">

                    	<a href="javascript:void(0)" class="fb-btn" onclick="myFacebookLogin();"><i class="fa fa-facebook" aria-hidden="true"></i>Sign Up with Facebook</a>

                        <a href="javascript:void(0)" class="google-btn " onclick="gplusLogin()"><i class="fa fa-google-plus" aria-hidden="true"></i>Sign Up with Google</a>
						
						<a href="javascript:void(0)" class="fb-btn" id="login_button" onclick="VK.Auth.login(authInfo);">Sign Up with Vk.com</a>
                    </div>

                    <h3>Account type: Business</h3>

                    <ul>

                    	<li>Free 30 day trial</li>

                        <li>No credit card required</li>

                        <li>Register in under 1 minute</li>

                    </ul>

                </div>

			</div>

		</div>

	</section>

@endsection

@section('customScript')

@endsection