<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <title>Activate your account on {{ $setting->site_title }}</title>
</head>
<body>
<header style=" margin: 0px auto; width: 600px;">{{ $setting->site_title }}</header>
<section style=" margin: 0px auto; width: 600px; border: 1px solid #EBEBEB; padding: 10px;">
    <div style="font-family:Verdana,Arial;font-weight:normal;font-size:16px; font-weight: bold;line-height:18px;margin-bottom:10px;margin-top:20px">Hi {{ $user->first_name }},</div>
    <div style="font-family:Verdana,Arial;font-weight:normal;font-size:12px;line-height:18px;margin-bottom:10px;margin-top:10px">Thanks for creating an account on {{ $setting->site_title }}.
        Please follow the link below to verify your email address: </div>

    <div style="font-family:Verdana,Arial;font-weight:normal;text-align:center;font-size:16px;line-height:18px;margin-bottom:30px;margin-top:30px"><a href="{{ url('register/verify/' . $confirmation_code.'/'.$utype) }}" style="text-decoration: none; padding: 10px 40px; background: #3696C2; color: #fff;">ACTIVATE ACCOUNT</a></div>

    <div style="font-family:Verdana,Arial;font-weight:normal;font-size:12px;margin-bottom:10px;margin-top:10px">If you have any questions, please feel free to contact us at {{ $setting->contact_email }}.</div>
</section>
<footer style="font-family:Verdana,Arial;font-weight:normal;text-align:center;font-size:12px;line-height:18px;margin-bottom:75px;margin-top:30px">Thank you, {{ $setting->site_title }}</footer>
</body>
</html>