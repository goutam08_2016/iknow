@extends('layouts.basic')

@section('pageTitle', 'Welcome to ')

@section('customStyle')

@endsection

@section('content')
	<div class="logincontent">
		<p class="logintag">Get the right answer from the right person in the right time</p>		
		@if($errors->any())

			<div id="login_warnings" class="warnings" style="">

				<div id="login_error_message" class="error_message">

					@foreach ($errors->all() as $error)
					
						<p>{!! $error !!}</p>

					@endforeach

				</div>

			</div>

		@elseif(session('register_success'))

			<div class="success_message">

				<p>{!! session('register_success') !!}</p>

			</div>

			<?php session()->forget('register_success');?>
		@endif

		<form method="post" id="loginForm" action="{{ url('/login') }}" data-parsley-validate="">
			{{csrf_field()}}
			<div class="formrow sociallogin clear">
				<a href="javascript:void(0);" class="fblogin" onclick="myFacebookLogin();"><span><img src="{!! asset('assets/frontend/images/fb.png') !!}" alt=""></span>Login with Facebook</a>
				<a href="javascript:void(0);" class="glogin" onclick="gplusLogin();"><span><img src="{!! asset('assets/frontend/images/google.png') !!}" alt=""></span>Login with Gmail</a>
			</div>
			<div class="formrow alteroption">
				<span class="alter">or</span>
			</div>
			<div class="formrow borderbox">
				<input type="email" name="email" data-parsley-trigger="change" required  Placeholder="Email">
			</div>
			<div class="formrow borderbox">
				<input type="password" name="password" Placeholder="Password" required />
			</div>
			<div class="formrow remember clear">
				<div class="rememberbox">
					<label>
						<input type="checkbox" name="rem">
						<span class="checkbox"></span>
						<span class="label">Remember me</span>
					</label>
				</div>
				<a href="#" class="forgotpass">Forgot  your  password?</a>
			</div>
			<div class="formrow loginbutton clear">
				<input type="submit" value="Login  as member" class="memberlogin">
				<input type="submit" value="Login  as guest" class="guestlogin">
			</div>
			<span class="notmember">Not a member? <a href="{{url('register')}}">Sign up now</a></span>
		</form>
	</div>
    
@endsection

@section('customScript')

@endsection