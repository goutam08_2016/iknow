<?php
//use App\Helpers\CustomHelper;
//dd($chat_messsage_data['chat_users']->toArray());
$loguser = Auth::User();
?>
<div id="chtMmbrlst" class="chatMmbrList mCustomScrollbar">
@if(count($chat_messsage_data['chat_users']) > 0)
    <ul>
    @foreach($chat_messsage_data['chat_users'] as $cht_usr)
		
        @if($cht_usr->from_user_id == $loguser->id )
			
            <li {!! session('touser_id') == $cht_usr->to_user->id?'class="selectli"':'' !!} id="user_list_{!! $cht_usr->to_user->id !!}">
                <a href="javascript:void(0);" onclick="startChatMessage('{!! $cht_usr->to_user->id !!}');">
                    <span class="user">
                        @if($cht_usr->to_user->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$cht_usr->to_user->profile_image))
                            <img src="{!! asset('assets/upload/profile_pictures/'.$cht_usr->to_user->profile_image) !!}" alt="{!! $cht_usr->to_user->first_name !!} {!! $cht_usr->to_user->last_name !!}"/>
                        @else
                            <img src="{!! asset('assets/frontend/images/user.jpg') !!}" alt="{!! $cht_usr->to_user->first_name !!} {!! $cht_usr->to_user->last_name !!}"/>
                        @endif
                    </span>
                    <span class="nameTm">
                        <span class="name">
                            <span class="{!! (count($cht_usr->to_user->userLogin) > 0 && $cht_usr->to_user->userLogin->login_status == 1)?'online':'offline' !!}"></span>
                            {!! $cht_usr->to_user->first_name !!} {!! $cht_usr->to_user->last_name !!}
    						<span class="count" style="display:none;"></span>
                        </span>
						<span class="msgUTxt">
							<i class="fa fa-envelope-o"></i> {!!last_received_msg($loguser->id,$cht_usr->to_user_id)!!}
						</span>
                        @if(count($cht_usr->toUserMessage))
                        <p>{!! $cht_usr->toUserMessage->message !!}</p>
                        @endif
                    </span>
                    @if($cht_usr)
                        <span class="lastOnline">{!! date('D,M-d',strtotime($cht_usr->created_at)) !!}</span>
                    @endif
                </a>
            </li>
        @elseif($cht_usr->to_user_id == $loguser->id )
			
            <li {!! session('touser_id') == $cht_usr->form_user->id?'class="selectli"':'' !!} id="user_list_{!! $cht_usr->form_user->id !!}">
                <a href="javascript:void(0);" onclick="startChatMessage('{!! $cht_usr->form_user->id !!}');">
                    <span class="user">
                        @if($cht_usr->form_user->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$cht_usr->form_user->profile_image))
                            <img src="{!! asset('assets/upload/profile_pictures/'.$cht_usr->form_user->profile_image) !!}" alt="{!! $cht_usr->form_user->first_name !!}  {!! $cht_usr->form_user->last_name !!}"/>
                        @else
                            <img src="{!! asset('assets/frontend/images/user.jpg') !!}" alt="{!! $cht_usr->form_user->first_name !!} {!! $cht_usr->form_user->last_name !!}"/>
                        @endif
                    </span>
                    <span class="nameTm">
                        <span class="name">
                            <span class="{!! (count($cht_usr->form_user->userLogin) > 0 && $cht_usr->form_user->userLogin->login_status == 1)?'online':'offline' !!}"></span>
                            {!! $cht_usr->form_user->first_name !!} {!! $cht_usr->form_user->last_name !!}
    						<span class="count" style="display:none;"></span>
                        </span>
						<span class="msgUTxt">
							<i class="fa fa-envelope-o"></i> {!!last_received_msg($loguser->id,$cht_usr->from_user_id)!!}
						</span>
                        @if(count($cht_usr->toUserMessage))
                        <p>{!! $cht_usr->toUserMessage->message !!}</p>
                        @endif
                    </span>
                    @if($cht_usr)
                        <span class="lastOnline">{!! date('D,M-d',strtotime($cht_usr->created_at)) !!}</span>
                    @endif
                </a>
            </li>
         @endif
    @endforeach
    </ul>
@else
<ul><li> cc msg </li></ul>
@endif      
</div>