<?php
//use App\Helpers\CustomHelper;
?>
<div class="popupchatbox" id="chatbox_pop_{!! $chat_messsage_data['to_user_id_pop'] !!}">
	<div class="onlinechat">
		<span class="title">
			<span class="uname"><span class="status{!! (count($chat_messsage_data['to_user_details_pop']->userLogin) > 0 && $chat_messsage_data['to_user_details_pop']->userLogin->login_status == 1)?' online':' offline' !!}"></span>{!! $chat_messsage_data['to_user_details_pop']->first_name !!} {!! $chat_messsage_data['to_user_details_pop']->last_name !!}</span>
			<span class="onchatopncls"><i class="fa fa-minus"></i></span>
			<span class="onchatclose" data-user-id="{!! $chat_messsage_data['to_user_id_pop'] !!}"><i class="fa fa-close"></i></span>
		</span>
		<div class="msglist">
			<div class="chatallmsg" id="showbox_{!! $chat_messsage_data['to_user_id_pop'] !!}">
				@if(count($chat_messsage_data['all_msg_list_pop']) > 0)
					@foreach($chat_messsage_data['all_msg_list_pop'] as $msg)
						@if($msg->from_user_id == $chat_messsage_data['from_user_id_pop'])
							<div class="list myreply">
								@if($msg->fromUser->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$msg->fromUser->profile_image))
									<a target="_blank" href="{!! url('profile/'.$msg->fromUser->id) !!}" class="photo">
										<img src="{!! asset('assets/upload/profile_pictures/'.$msg->fromUser->profile_image) !!}" alt="{!! $msg->fromUser->first_name !!} {!! $msg->fromUser->last_name !!}"/>
									</a>
			                    @else
			                    	<a target="_blank" href="{!! url('profile/'.$msg->fromUser->id) !!}" class="photo">
			                        	<img src="{!! asset('assets/common/images/default_avatar.jpg') !!}" alt="{!! $msg->fromUser->first_name !!} {!! $msg->fromUser->last_name !!}"/>
			                        </a>
			                    @endif
								<span class="comment">
									<span class="type">
										@if($msg->type == 'T')
											<p>{!! $msg->message !!}</p>
										@else
											@if(in_array($msg->file_ext,array('jpg','JPG','JPEG','png','PNG','bmp','BMP','gif','GIF')))
												<p><a target="_blank" href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" class="chat_pic_msg" ><img style="width:80px;" src="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}"/></a></p>
											@else
												<p><a target="_blank" href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" download >{!! $msg->message !!}</a></p>
											@endif
										@endif
									</span>
								</span>
								<small class="chattime">{!! date('D,M-d h:i:s A',strtotime($msg->created_at)) !!}</small>
							</div>
						@else
							<div class="list">
								@if($msg->fromUser->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$msg->fromUser->profile_image))
									<a target="_blank" href="{!! url('profile/'.$msg->fromUser->id) !!}" class="photo">
										<img src="{!! asset('assets/upload/profile_pictures/'.$msg->fromUser->profile_image) !!}" alt="{!! $msg->fromUser->first_name !!} {!! $msg->fromUser->last_name !!}"/>
									</a>
			                    @else
			                    	<a target="_blank" href="{!! url('profile/'.$msg->fromUser->id) !!}" class="photo">
			                        	<img src="{!! asset('assets/common/images/default_avatar.jpg') !!}" alt="{!! $msg->fromUser->first_name !!} {!! $msg->fromUser->last_name !!}"/>
			                        </a>
			                    @endif
								<span class="comment">
									<span class="type">
										@if($msg->type == 'T')
											<p>{!! $msg->message !!}</p>
										@else
											@if(in_array($msg->file_ext,array('jpg','JPG','JPEG','png','PNG','bmp','BMP','gif','GIF')))
												<p><a href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" class="chat_pic_msg" ><img style="width:80px;" src="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}"/></a></p>
											@else
												<p><a href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" download >{!! $msg->message !!}</a></p>
											@endif
										@endif
									</span>
								</span>
								<small class="chattime">{!! date('D,M-d h:i:s A',strtotime($msg->created_at)) !!}</small>
							</div>
						@endif
					@endforeach
				@endif
			</div>
		</div>
		<div class="onlinechattype" id="onlinechattype_{!! $chat_messsage_data['to_user_id_pop'] !!}">
		@if($chat_messsage_data['can_msg_pop'] == 1)
			<textarea placeholder="write message" name="message" class="message"></textarea>
			<form name="upload_chat_form" id="upload_chat_form_{!! $chat_messsage_data['to_user_id_pop'] !!}" class="upload_chat_form" action="{!! url('chat-pop/uploadChatfilePop') !!}" method="post" enctype="multipart/form-data">
				<label class="chatAttach">
					<input name="attachFile" type="file" class="attachFile" />
					<img src="{!! asset('assets/frontend/chat/img/attachment.jpg') !!}" alt=""/>
				</label>
				<input type="hidden" name="from_user_id" class="from_user_id" value="{!! $chat_messsage_data['from_user_id_pop'] !!}" />
				<input type="hidden" name="to_user_id" class="to_user_id" value="{!! $chat_messsage_data['to_user_id_pop'] !!}" />
			</form>
		@else
			<div id="warningMessageBox" class="chatSbmtformsg" style="min-height:0px;">
				<div class="buttonset align-center">
					<a class="button" href="{!! url('user/'.app()->getLocale().'/credit') !!}">Buy Credits to Send Message </a>
				</div>
			</div>
		@endif
		</div>
	</div>
</div>