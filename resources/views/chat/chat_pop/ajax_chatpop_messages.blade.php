@if(count($chat_messsage_data['chat_msg_arr_pop']) > 0)
	@foreach($chat_messsage_data['chat_msg_arr_pop'] as $msg)
		@if($msg->from_user_id == $chat_messsage_data['from_user_id_pop'])
			<div class="list myreply">
				@if($msg->fromUser->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$msg->fromUser->profile_image))
					<a target="_blank" href="{!! url('profile/'.$msg->fromUser->id) !!}" class="photo">
						<img src="{!! asset('assets/upload/profile_pictures/'.$msg->fromUser->profile_image) !!}" alt="{!! $msg->fromUser->first_name !!} {!! $msg->fromUser->last_name !!}"/>
					</a>
                @else
                	<a target="_blank" href="{!! url('profile/'.$msg->fromUser->id) !!}" class="photo">
                    	<img src="{!! asset('assets/common/images/default_avatar.jpg') !!}" alt="{!! $msg->fromUser->first_name !!} {!! $msg->fromUser->last_name !!}"/>
                    </a>
                @endif
				<span class="comment">
					<span class="type">
						@if($msg->type == 'T')
							<p>{!! $msg->message !!}</p>
						@else
							@if(in_array($msg->file_ext,array('jpg','JPG','JPEG','png','PNG','bmp','BMP','gif','GIF')))
								<p><a href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" class="chat_pic_msg" ><img style="width:80px;" src="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}"/></a></p>
							@else
								<p><a href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" download >{!! $msg->message !!}</a></p>
							@endif
						@endif
					</span>
				</span>
				<small class="chattime">{!! date('D,M-d h:i:s A',strtotime($msg->created_at)) !!}</small>
			</div>
		@else
			<div class="list">
				@if($msg->fromUser->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$msg->fromUser->profile_image))
					<a target="_blank" href="{!! url('profile/'.$msg->fromUser->id) !!}" class="photo">
						<img src="{!! asset('assets/upload/profile_pictures/'.$msg->fromUser->profile_image) !!}" alt="{!! $msg->fromUser->first_name !!} {!! $msg->fromUser->last_name !!}"/>
					</a>
                @else
                	<a target="_blank" href="{!! url('profile/'.$msg->fromUser->id) !!}" class="photo">
                    	<img src="{!! asset('assets/common/images/default_avatar.jpg') !!}" alt="{!! $msg->fromUser->first_name !!} {!! $msg->fromUser->last_name !!}"/>
                    </a>
                @endif
				<span class="comment">
					<span class="type">
						@if($msg->type == 'T')
							<p>{!! $msg->message !!}</p>
						@else
							@if(in_array($msg->file_ext,array('jpg','JPG','JPEG','png','PNG','bmp','BMP','gif','GIF')))
								<p><a href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" class="chat_pic_msg" ><img style="width:80px;" src="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}"/></a></p>
							@else
								<p><a href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" download >{!! $msg->message !!}</a></p>
							@endif
						@endif
					</span>
				</span>
				<small class="chattime">{!! date('D,M-d h:i:s A',strtotime($msg->created_at)) !!}</small>
			</div>
		@endif
	@endforeach
@endif
			