<?php
$loguser = Auth::User();
if($loguser):
?>
<div id="tab1" class="allUserTabtab_view online_userchat_right_panel">
	<span class="ttl">All Users</span>
    <ul class="allFriendsList">
    @if(count($chat_messsage_data['chat_users']) > 0)
        @foreach($chat_messsage_data['chat_users'] as $cht_usr)
            @if($cht_usr->from_user_id == $loguser->id )				
				<li>
					<a href="javascript:void(0);" onclick="startChatPop('{!! $cht_usr->to_user->id !!}', 0);">
						<span class="image">
							@if($cht_usr->to_user->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$cht_usr->to_user->profile_image))
								<img src="{!! asset('assets/upload/profile_pictures/'.$cht_usr->to_user->profile_image) !!}" alt="{!! $cht_usr->to_user->first_name !!} {!! $cht_usr->to_user->last_name !!}"/>
							@else
								<img src="{!! asset('assets/common/images/default_avatar.jpg') !!}" alt="{!! $cht_usr->to_user->first_name !!} {!! $cht_usr->to_user->last_name !!}"/>
							@endif
						</span>
						<span class="name">{!! $cht_usr->to_user->first_name !!} {!! $cht_usr->to_user->last_name !!}</span>
						<span class="status online"></span>
					</a>
				</li>
            @elseif($cht_usr->to_user_id == $loguser->id )				
				<li>
					<a href="javascript:void(0);" onclick="startChatPop('{!! $cht_usr->form_user->id !!}', 0);">
						<span class="image">
							@if($cht_usr->form_user->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$cht_usr->form_user->profile_image))
								<img src="{!! asset('assets/upload/profile_pictures/'.$cht_usr->form_user->profile_image) !!}" alt="{!! $cht_usr->form_user->first_name !!} {!! $cht_usr->form_user->last_name !!}"/>
							@else
								<img src="{!! asset('assets/common/images/default_avatar.jpg') !!}" alt="{!! $cht_usr->form_user->first_name !!} {!! $cht_usr->form_user->last_name !!}"/>
							@endif
						</span>
						<span class="name">{!! $cht_usr->form_user->first_name !!} {!! $cht_usr->form_user->last_name !!}</span>
						<span class="status online"></span>
					</a>
				</li>
					
            @endif
        @endforeach
    @endif
    </ul>
</div>
<?php
endif;
?>