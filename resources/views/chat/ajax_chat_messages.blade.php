<?php
//use App\Helpers\CustomHelper;
?>
@if(isset($chat_messsage_data['chat_msg_arr']) && count($chat_messsage_data['chat_msg_arr']))
    @foreach($chat_messsage_data['chat_msg_arr'] as $msg)
        @if($msg->from_user_id == $chat_messsage_data['from_user_id'])
            <li class="list meChat">
                <span class="name" style="display:block;"><a href="{!! url('profile/'.$msg->fromUser->id) !!}" target="_blank">{!! $msg->fromUser->first_name !!} {!! $msg->fromUser->last_name !!}</a></span>
               <span class="time">{!! time_elapsed_string(strtotime($msg->created_at)) !!} ago</span>
                @if($msg->type == 'T')
					<p>{!! $msg->message !!}</p>
				@else
					@if(in_array($msg->file_ext,array('jpg','JPG','JPEG','png','PNG','bmp','BMP','gif','GIF')))
						<p><a href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" class="chat_pic_msg" ><img style="width:120px;" src="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}"/></a></p>
					@else
						<p><a href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" download >{!! $msg->message !!}</a></p>
					@endif
				@endif                                                
            </li>
        @else
            <li class="list userChat">
               <span class="name" style="display:block;"><a href="{!! url('profile/'.$msg->fromUser->id) !!}" target="_blank">{!! $msg->fromUser->first_name !!} {!! $msg->fromUser->last_name !!}</a></span>
                <span class="time">{!! time_elapsed_string(strtotime($msg->created_at)) !!} ago</span>
                @if($msg->type == 'T')
					<p>{!! $msg->message !!}</p>
				@else
					@if(in_array($msg->file_ext,array('jpg','JPG','JPEG','png','PNG','bmp','BMP','gif','GIF')))
						<p><a href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" class="chat_pic_msg" ><img style="width:120px;" src="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}"/></a></p>
					@else
						<p><a href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" download >{!! $msg->message !!}</a></p>
					@endif
				@endif                                               
            </li>
        @endif
    @endforeach
</ul>
@endif    