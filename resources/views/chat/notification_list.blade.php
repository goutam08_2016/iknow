<?php
use App\Helpers\CustomHelper;
?>
<a href="javascript:void(0);" id="notyfybtn"><i class="fa fa-bell"></i>
@if(count($chat_messsage_data['unread_notification_msg']) > 0)
	<span class="count" id="ntification_count">{!! count($chat_messsage_data['unread_notification_msg']) !!}</span>
@endif
</a>
@if(count($chat_messsage_data['unread_notification_msg']) > 0)
<div class="notifybox">
    <ul>
    @foreach($chat_messsage_data['unread_notification_msg'] as $notfcnt)
    	<?php
            $senderUser = '<span class="usrname">
                <a href="'.url('user/profile-details/'.$notfcnt->fromUser->id).'">';
            if(count($notfcnt->fromUser->userLogin) && $notfcnt->fromUser->userLogin->login_status == 1){
                $senderUser .= '<span class="status online"></span>';
            } else {
                $senderUser .= '<span class="status"></span>';
            }
            if($notfcnt->fromUser->first_name == '' && $notfcnt->fromUser->last_name == '') {
                $senderUser .= $notfcnt->fromUser->username;
            } else {
                $senderUser .= $notfcnt->fromUser->username;
            }
            $senderUser .= '</a></span>'
        ?>

        <li>
    		@if($notfcnt->type == 1)
                <span class="notytext">{!! CustomHelper::siteStaticText(114) !!}</span>
                {!! $senderUser !!}
                <a href="javascript:void(0);" title="View Details" class="viewnotify" onclick="startChat('{!! $notfcnt->fromUser->id !!}');"><i class="fa fa-eye" aria-hidden="true"></i></a>
            @elseif($notfcnt->type == 2)
                <span class="notytext">{!! CustomHelper::siteStaticText(115) !!}</span>
                {!! $senderUser !!}
                <a href="javascript:void(0);" title="View Details" class="viewnotify" onclick="startChat('{!! $notfcnt->fromUser->id !!}');"><i class="fa fa-eye" aria-hidden="true"></i></a>
            @elseif($notfcnt->type == 3)
                {!! $senderUser !!}
                <span class="notytext"> {!! CustomHelper::siteStaticText(116) !!}</span>
                <a href="{!! url('user/viewer-list') !!}" title="View Details" class="viewnotify"><i class="fa fa-eye" aria-hidden="true"></i></a>
            @elseif($notfcnt->type == 4)
                {!! $senderUser !!}
                <span class="notytext"> {!! CustomHelper::siteStaticText(117) !!}</span>
                <a href="{!! url('user/profile-details/'.$notfcnt->fromUser->id) !!}" title="View Details" class="viewnotify"><i class="fa fa-eye" aria-hidden="true"></i></a>    
            @endif
        	
            
        </li>
    @endforeach
    </ul>
</div>
@else
<div class="notifybox">
    <ul>
        <li><span class="notytext">{!! CustomHelper::siteStaticText(262) !!}</span></li>
    </ul>
</div>
@endif
