@extends('layouts.app')

@section('pageTitle', 'Welcome to ')

@section('customStyle')

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

@endsection

@section('content')

	<!--body open-->
	@if(Auth::check())
    <section class="mainbody clear">
    	<!--left pan open-->
		@include('include.left_pan')
    	
		@endif
        <!--left pan close-->
        
               

        <!--middle open-->
        <div class="{{Auth::check()?'middlecol':'pageMiddle'}}"><!--  -->
        	<div class="topicSearch clear">
            	<h2>Topic</h2>
                <div class="rightSrc">
                	<form name="srch" method="get" action="">
                    	<input type="text" name="trpc" placeholder="Find Topic" value="{{isset($_GET['trpc'])?$_GET['trpc']:''}}">
						
                    	<input type="hidden" name="page" value="{{isset($_GET['page'])?$_GET['page']:1}}">
                    	<input type="hidden" name="tb" value="{{isset($_GET['tb'])?$_GET['tb']:''}}">
						
                        <button type="submit" value="Search"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
        	<div class="topictab">
                <div class="filterBtn tabmenu">
                    <ul>
                        <li class="{{(isset($_GET['tb']) && $_GET['tb']=='al') || !isset($_GET['tb'])?'active':''}}"><a href="{{url('topics')}}?tb=al" id="tab1">All topics</a></li>
                        <li class="{{isset($_GET['tb']) && $_GET['tb']=='fv'?'active':''}}"><a href="{{url('topics')}}?tb=fv" id="tab2">Favorite topics</a></li>
                    </ul>
                </div>
				
                <div class="tabContWrap">
                    <div class="topicWrap " style="display:{{(isset($_GET['tb']) && $_GET['tb']=='al') || !isset($_GET['tb'])?'block':'none'}}">
                    	<div class="allTopic">
                        	<div class="wrapnumber">
                                <ul class="listName caSlide">
                                	<li class="item"><a href="{{url('topics?tb=al')}}">All</a></li>
									@foreach($alphabets as $val)
                                    <li class="item"><a href="{{url('topics?tb=al&alph='.$val)}}">{{$val}}</a></li>
									@endforeach
                                </ul>
                            </div>
                            <div class="allList">
							@if(isset($_GET['alph']))
								<div class="topicList">
									<h3>{{$_GET['alph']}}</h3>
									<ul>
										@foreach($topics as $val)
										@if($val->title[0]==$_GET['alph'])
										<li class="{{in_array($val->id,$user_favs)?'fav':''}}"><a href="{{url('topic/'.$val->id)}}">{{$val->title}}<span>{{$val->tropic_questions->count()}}</span></a>
											@if(Auth::check())
											<a href="javascript:void(0);" class="addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="{{$val->id}}"/></a>
											@endif
										</li>
										@endif
										@endforeach
									</ul>
								</div>
							@else	
								@if(!empty($all_data))
									@foreach($all_data as $itr=>$aplha)
									@if($itr >= $start_ky && $itr <= $end_ky)
										<div class="topicList">
											<h3>{{$aplha->first_alpha}}</h3>
											<ul>
												@foreach($topics as $val)
												@if($val->title[0]==$aplha->first_alpha)
												<li class="{{in_array($val->id,$user_favs)?'fav':''}}">
													<a href="{{url('topic/'.$val->id)}}">{{$val->title}}<span>{{$val->tropic_questions->count()}}</span></a>
													@if(Auth::check())
													<a href="javascript:void(0);" class="addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="{{$val->id}}"/></a>
													@endif
												</li>
												@endif
												@endforeach
											</ul>
										</div>
									@endif
									@endforeach
								@endif
							@endif
								
                            </div>
                        </div>
                        <div class="pagination">
                            <ul>
                                <!--<li class="prev"><a href="">Prev</a></li>
                                <li><span>...</span></li>
                                <li class="next"><a href="">Next</a></li>
                                <li class="current"><span>1</span></li>-->
								@if(!empty($noof_pages) && !isset($_GET['alph']))
								@for ( $i=1; $i <= $noof_pages; $i++ )
									@if(isset($_GET['page']) && $_GET['page']==$i )
									<li class="current"><span>{{$i}}</span></li>
									@else
									<li > <a href="{{url('topics?tb=al&page='.$i)}}">{{$i}}</a></li>
									@endif
								@endfor
								@endif
                            </ul>
                        </div>
                    </div>
                    <div class="topicWrap " style="display:{{isset($_GET['tb']) && $_GET['tb']=='fv'?'block':'none'}}">
                    	<div class="allTopic">
                        	<div class="wrapnumber">
                                <ul class="listName caSlide">
                                	<li class="item"><a href="{{url('topics?tb=fv')}}">All</a></li>
                                    @foreach($alphabets as $val)
                                    <li class="item"><a href="{{url('topics?tb=fv&alph='.$val)}}">{{$val}}</a></li>
									@endforeach
                                </ul>
                            </div>
							@if(Auth::check())
                            <div class="allList">
								@if(isset($_GET['alph']))
								<div class="topicList">
									<h3>{{$_GET['alph']}}</h3>
									<ul>
										@foreach($fav_topics as $val)
										@if($val->title[0]==$_GET['alph'])
										<li class="fav">
											<a href="{{url('topic/'.$val->id)}}">{{$val->title}}<span>{{$val->tropic_questions->count()}}</span></a>
											@if(Auth::check())
											<a href="javascript:void(0);" class="addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="{{$val->id}}"/></a>
											@endif
										</li>
										@endif
										@endforeach
									</ul>
								</div>
								@elseif(isset($_GET['trpc']) && !empty($fav_alphabets))
									@foreach($fav_alphabets as $itr=>$aplha)
									<div class="topicList">
										<h3>{{$aplha}}</h3>
										<ul>
											@foreach($fav_topics as $val)
											@if($val->title[0]==$aplha)
											<li class="fav">
												<a href="{{url('topic/'.$val->id)}}">{{$val->title}}<span>{{$val->tropic_questions->count()}}</span></a>
												@if(Auth::check())
												<a href="javascript:void(0);" class="addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="{{$val->id}}"/></a>
												@endif
											</li>
											@endif
											@endforeach
										</ul>
									</div>
									@endforeach
								@else	
									@if(!empty($fav_alphabets))
										@foreach($fav_alphabets as $itr=>$aplha)
											<div class="topicList">
												<h3>{{$aplha}}</h3>
												<ul>
													@foreach($fav_topics as $val)
													@if($val->title[0]==$aplha && in_array($val->id,$user_favs))
													<li class="fav">
														<a href="{{url('topic/'.$val->id)}}">{{$val->title}}<span>{{$val->tropic_questions->count()}}</span></a>
														@if(Auth::check())
														<a href="javascript:void(0);" class="addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="{{$val->id}}"/></a>
														@endif
													</li>
													@endif
													@endforeach
												</ul>
											</div>
										@endforeach
									@endif
								@endif								
                            </div>
							@else
								<div class="allList loginReq">Please login first. <a href="{{url('')}}">Click here.</a></div>
							@endif
                        </div>
							
                        <div class="pagination">
                            {{-- <ul>
							 @if(!empty($noof_pages) && !isset($_GET['alph']))
								@for ( $i=1; $i <= $noof_pages; $i++ )
									@if(isset($_GET['page']) && $_GET['page']==$i )
									<li class="current"><span>{{$i}}</span></li>
									@else
									<li > <a href="{{url('topics?page='.$i)}}">{{$i}}</a></li>
									@endif
								@endfor
								@endif
							
                            </ul>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--middle close-->
        @if(Auth::check())
        <!--right pan open-->
		@include('include.right_pan')
		<!--right pan close-->
	</section>
		@endif
    <!--body close-->

@endsection



@section('customScript')

<script>
 
$( function() {
	
});

$(document).on('click',".addF", function(e){
	e.preventDefault();
	var currentobj = $(this);
	var addFid = $(this).find('input').val();
	//alert(addFid);
	$.ajax({
		type:"post",
		url: "{!! url('user/add-favorite') !!}" ,
		headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
		data: {'tropic_id':addFid, },			
		//dataType: "json",			
		success:function(res) {
			console.log(res);
			var parentli = currentobj.parent('li');
			if(parentli.hasClass('fav')){
				parentli.removeClass('fav');
			}else	parentli.addClass('fav');
		}
	}); 
});

</script>

@endsection