@extends('layouts.app')

@section('pageTitle', 'friend request')

@section('content')
<section class="mainbody clear">
@include('include.left_pan')

<!--middle open-->
<div class="middlecol">
	<div class="blogCate">
		<div class="blogTab">
			<div class="filterBtn tabmenu">
				<ul>
					<li><a href="javascript:void(0);" id="tab1">All</a></li>
					<li><a href="javascript:void(0);" id="tab2">Favorite</a></li>
				</ul>
			</div>
			<div class="tabContWrap">
				<div class="BlogCatList tabCont">
					<ul id="catlst">
					@if(count($catagories)>0)
						@foreach($catagories as $cat)
						<li class="{{in_array($cat->id,$fav_arr)?'fav':''}}" id="cat_{{$cat->id}}">
							<div class="catThumb">
								<figure><a href="{{url('user/blog-list/'.$cat->id)}}"><img src="{!! asset('assets/upload/category_icon/'.$cat->icon) !!}" alt=""></a></figure>
								<a href="javascript:void(0);" class="str addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="{{$cat->id}}"/></a>
							</div>
							<span><a href="{{url('user/blog-list/'.$cat->id)}}">{{$cat->title}}</a></span>
						</li>
						@endforeach
					@endif						
					</ul>
				</div>
				<div class="BlogCatList tabCont">
					<ul id="favLst">
					@if(count($user->fav_categories)>0)
						@foreach($user->fav_categories as $val)
						<?php $cat=$val->category;?>
						<li class="fav" id="cat_{{$cat->id}}">
							<div class="catThumb">
								<figure><a href="{{url('user/blog-list/'.$cat->id)}}"><img src="{!! asset('assets/upload/category_icon/'.$cat->icon) !!}" alt=""></a></figure>
								<a href="javascript:void(0);" class="str addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="{{$cat->id}}"/></a>
							</div>
							<span><a href="{{url('user/blog-list/'.$cat->id)}}">{{$cat->title}}</a></span>
						</li>
						@endforeach
					@endif						
					</ul>
				</div>
			</div>
		</div>
	</div>        
</div>
<!--middle close-->


@include('include.right_pan')
</section>
@endsection

@section('customScript')
<script>
$(document).on('click',".addF", function(e){
	e.preventDefault();
	var currentobj = $(this);
	var addFid = $(this).find('input').val();
	//alert(addFid);
	$.ajax({
		type:"post",
		url: "{!! url('user/add-fav-cat') !!}" ,
		headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
		data: {'cat_id':addFid, },			
		dataType: "json",			
		success:function(res) {
			//console.log(res);
			var parentli = currentobj.parent().parent('li');
			
			if(res.msg==0){
				//parentli.removeClass('fav');
				$("#favLst").find('li#cat_'+addFid).remove();
				$("#catlst").find('li#cat_'+addFid).removeClass('fav');
				
			}else if(res.msg==1){				
				$("#favLst").append(res.favHtml);
				$("#catlst").find('li#cat_'+addFid).addClass('fav');
			}
		}
	}); 
});
</script>
@endsection