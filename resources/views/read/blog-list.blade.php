@extends('layouts.app')

@section('pageTitle', 'friend request')

@section('content')
<section class="mainbody clear">
@include('include.left_pan')

<!--middle open-->
<div class="middlecol demoBlog">
	<!--<div  class="blogMenu">		
		<div class="slideBlogMenu">
			<a class="btn prev">Previous</a>
			<ul id="owl-demo" class="owl-carousel owl-theme">
				<li class="item"><a href="#">America</a></li>
				<li class="item"><a href="#">africa</a></li>
				<li class="item"><a href="#">australia</a></li>
				<li class="item"><a href="#">asia</a></li>
				<li class="item"><a href="javascript:void(0);" class="blogSubBtn">europe</a></li>
			</ul>
			<a class="btn next">Next</a>
		</div>
		
		
		<div class="moreSub clear">
			
			<ul>
				<li><a href="#">England</a></li>
				<li><a href="#">France</a></li>
				<li><a href="#">Germany</a></li>
				<li><a href="#">Sweden</a></li>
				<li><a href="#">Portugal</a></li>
				<li><a href="#">Spain</a></li>
				<li><a href="#">italy</a></li>
			</ul>
		</div>
	</div>-->
	<div class="breadcrumb">
		<ul>
			<li><a href="{{url('user/blog-catagories')}}">home</a></li>
			<li class="active"><a href="javascript:;">{{$catagory->title}}</a></li>
		</ul>
	</div><!---->
	<div class="demoList">
		@if(!empty($catagory->blogs))
			@foreach($catagory->blogs as $blog)
			<div class="bloglist clear">
				<figure>
				@php ($bimg = $blog->blog_image!="" ? $blog->blog_image :'blog.jpg')
					<a href="{{url('user/blog-details/'.$blog->id)}}"><img src="{!! asset('assets/upload/blog_image/'.$bimg) !!}" alt=""></a>
				
				</figure>
				<div class="blogText">
					<h3><a href="{{url('user/blog-details/'.$blog->id)}}">{{$blog->title}}</a></h3>
					<div class="postMeta">
						 <ul>
							<li><a href="{{url('user/blog-details/'.$blog->id)}}"><i class="fa fa-user"></i> {{!empty($blog->blog_user)?$blog->blog_user->first_name.' '.$blog->blog_user->last_name:''}}</a></li>
							<li><i class="fa fa-calendar"></i> {{date('d/m/Y',strtotime($blog->created_at))}}</li>
							<li><i class="fa fa-tags"></i> {{!empty($blog->blog_category)?$blog->blog_category->title:''}}</li>
							<li><a href="{{url('user/blog-details/'.$blog->id)}}"><i class="fa fa-comments-o"></i> {!!count($blog->blog_comments)!!} comments</a></li>
							<li><i class="fa fa-eye"></i> {!!count($blog->blog_views)!!} views</li>
						</ul>
					</div>
					<div class="blogDesc">
						<article>{!!($blog->description)!!} </article>
						
						<a href="{{url('user/blog-details/'.$blog->id)}}" class="bluebtn">View Details</a>
					</div>
				</div>
			</div>
			@endforeach
		@endif
	</div>
</div>
<!--middle close-->


@include('include.right_pan')
</section>
@endsection
@section('customScript')

<script>
	$(window).load(function(){
		$("body").on("click",".addBlogBtn", function(){
			if(!$(".addNewBlog").is(":visible")){
				$(".addNewBlog").slideDown(400);	
			}
		});	
		$("body").on("click",".addBlogCancel", function(){
			$(".addNewBlog").slideUp(400);
		});	
	});
</script>
@endsection