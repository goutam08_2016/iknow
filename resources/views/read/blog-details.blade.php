@extends('layouts.app')

@section('pageTitle', 'friend request')

@section('content')
<section class="mainbody clear">
@include('include.left_pan')

<!--middle open-->
	<div class="middlecol demoBlog">
		<!--<div  class="blogMenu">
			
			<div class="slideBlogMenu">
				<a class="btn prev">Previous</a>
				<ul id="owl-demo" class="owl-carousel owl-theme">
					<li class="item"><a href="#">America</a></li>
					<li class="item"><a href="#">africa</a></li>
					<li class="item"><a href="#">australia</a></li>
					<li class="item"><a href="#">asia</a></li>
					<li class="item"><a href="javascript:void(0);" class="blogSubBtn">europe</a></li>
				</ul>
				<a class="btn next">Next</a>
			</div>
			
			
			<div class="moreSub clear">
				<ul>
					<li><a href="#">England</a></li>
					<li><a href="#">France</a></li>
					<li><a href="#">Germany</a></li>
					<li><a href="#">Sweden</a></li>
					<li><a href="#">Portugal</a></li>
					<li><a href="#">Spain</a></li>
					<li><a href="#">italy</a></li>
				</ul>
			</div>
		</div>-->
		<div class="breadcrumb">
			<ul>
				@if(!empty($blog->blog_category))
					<li><a href="{{url('user/blog-catagories')}}">home</a></li>
					<li><a href="{{url('user/blog-list/'.$blog->cat_id)}}">{{$blog->blog_category->title}}</a></li>
				@else
					<li><a href="{{url('user/admin-blogs')}}">home</a></li>
				@endif
				<li class="active"><a href="javascript:;">{{$blog->title}}</a></li>
			</ul>
		</div>
		@if(!empty($blog))
		<div class="blogLeft detailsLeft csblog">
			<div class="blogRow">
				<h2>{{$blog->title}}</h2>
				<div class="postMeta">
					 <ul>
						<li><a href=""><i class="fa fa-user" aria-hidden="true"></i> {{!empty($blog->blog_user)?$blog->blog_user->first_name.' '.$blog->blog_user->last_name:''}}</a></li>
						<li><i class="fa fa-calendar" aria-hidden="true"></i> {{date('d/m/Y',strtotime($blog->created_at))}}</li>
						<li><i class="fa fa-tags" aria-hidden="true"></i> {{!empty($blog->blog_category)?$blog->blog_category->title:''}}</li>
						<li><a href=""><i class="fa fa-comments-o" aria-hidden="true"></i> {!!count($blog->blog_comments)!!} comments</a></li>
						<li><i class="fa fa-eye" aria-hidden="true"></i> {!!count($blog->blog_views)!!} views</li>
					</ul>
				</div>
				<div class="sharelist">
					<!-- Go to www.addthis.com/dashboard to customize your tools --> 
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58e351206f5cc494"></script> 
					<!-- Go to www.addthis.com/dashboard to customize your tools --> 
					<div class="addthis_inline_share_toolbox"></div>
				</div>
				<figure>
				@php ($bimg = $blog->blog_image!="" ? $blog->blog_image :'blog.jpg')
					<!--<a href="{{url('user/blog-details/'.$blog->id)}}"><img src="{!! asset('assets/upload/blog_image/'.$bimg) !!}" alt=""></a>-->
				
				</figure>
				<div class="blogDesc">
					<div class="blgdesc">
						{!!($blog->description)!!}
					</div>
				</div>
			</div>
			<div class="blogRow blogComments">
				<h2>Comments <span>({!!count($blog->blog_comments)!!})</span></h2>
				<div class="fbComment">
					<div class="smallCommnt clear">
						<figure><img src="{!!asset('assets/frontend')!!}/images/userthumb.png" alt=""></figure>
						<div class="blogcommnt">
							<form class="commentfrm" method="POST">
								<textarea name="comment" placeholder="Write a comment..."></textarea>
								<input name="blog_id" type="hidden" value="{{$blog->id}}"/>
								<input name="parent_id" type="hidden" value="0"/>
								<button type="submit" value="submit"><i class="fa fa-reply-all" aria-hidden="true"></i><span>Reply</span></button>
							</form>
						</div>
					</div>
					<div class="commntList parentComment">	
						<ul>
						@if(count($blog->blog_comments)>0)
						{!!blog_reply_comment($blog->blog_comments,$blog,'') !!}
						@endif
						</ul>
					</div>					
					<span class="moreText">
						<a href="javascript:void(0);" class="moreComments">More Comments</a>
						<a href="javascript:void(0);" class="lessComments">Less Comments</a>
					</span>
				</div>
			</div>
		</div>
		@endif
	</div>

<!--middle close-->


@include('include.right_pan')
</section>
@endsection
@section('customScript')

<script>
	$(window).load(function(){
		$("body").on("click",".addBlogBtn", function(){
			if(!$(".addNewBlog").is(":visible")){
				$(".addNewBlog").slideDown(400);	
			}
		});	
		$("body").on("click",".addBlogCancel", function(){
			$(".addNewBlog").slideUp(400);
		});	
	});
	
	$('body').on("click",".replayForm",function() {
		if(!$(this).parent('span').parent('.blogmeta').next('.smallreplay').is(":visible")){
			$('.smallreplay').slideUp(200);
			$(this).parent('span').parent('.blogmeta').next('.smallreplay').slideDown(200);
		}else{
			$(this).parent('span').parent('.blogmeta').next('.smallreplay').slideUp(200);
		}
	});
	$('body').on("click",".closeBox",function() {
		$(this).parent('.smallreplay ').slideUp(200);
	});
	$(document).on( "submit","form.commentfrm", function( event ) {
	
		event.preventDefault();
		
		var formdata =  $( this ).serializeArray();
				
		var fobj = $(this);

		$.ajax({
			type:"post",
			url: "{!! url('user/blog-comment-process') !!}" ,
			headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
			data: formdata,			
			dataType: "json",			
			success:function(res) {
				//console.log(res);
				if(res.comment)
				{
					var chtml = '<li>'
								+'<figure><img src="{!!asset('assets/frontend')!!}/images/userthumb.png" alt=""></figure>'
								+'<div class="blogcommnt">'
									+'<h4><a href="">'+res.nickname+'</a></h4>'
									+'<p>'+res.comment.comment+'</p>'
									+'<div class="blogmeta">'
										+'<span>0 <a href=""><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <span>Like</span></a></span>'
										+'<span>'+res.replyCount+' <a href="javascript:void(0);" class="replayForm"><i class="fa fa-reply" aria-hidden="true"></i> <span>Reply</span></a></span>'
										+'<span>'+res.created_at+'</span>'
									+'</div>'
									+'<div class="smallCommnt smallreplay clear">'
										+'<figure><img src="{!!asset('assets/frontend')!!}/images/userthumb.png" alt=""></figure>'
										+'<div class="blogcommnt">'
											+'<form class="commentfrm" method="POST">'
												+'<textarea name="comment" placeholder="Write a comment..."></textarea>'
												+'<input name="blog_id" type="hidden" value="'+res.comment.blog_id+'"/>'
												+'<input name="parent_id" type="hidden" value="'+res.comment.id+'"/>'
												+'<button type="submit" value="submit"><i class="fa fa-reply-all" aria-hidden="true"></i><span>Reply</span></button>'
											+'</form>'
										+'</div>'
									+'</div><div class="commntList" style="display:block;"><ul></ul></div>'
								+'</div>'
							+'</li>';
					fobj.parent().parent().next('.commntList').children('ul').prepend(chtml);		
					fobj.find("textarea").val("");
				}
			}
		});
		
	});
	
</script>
@endsection