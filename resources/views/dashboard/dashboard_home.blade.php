@extends('layouts.app')



@section('pageTitle', 'Welcome to ')

@section('content')
<section class="mainbody clear">
@include('include.left_pan')
<?php
$action=['Q' => 'Asked a Question' ,'A' => 'Replied to question' ,'F' => 'Forward a Question','FO' => 'Asked a Question','T' => 'Asked a Question'];

?>
<!--middle open-->
        <div class="middlecol listQuestion homeQues">
        	<div class="topfilter clear">
            	<h2>Hi <span>{{$user_data->first_name.' '.$user_data->last_name}}</span></h2>
            	<!--<div class="hadLeft">
                    <h2>Hi <span>Samir Maity</span></h2>
                </div>
                <div class="rightView">
                	<h2>Answer View</h2>
                    <ul>
                    	<li><span>46</span><samp>last 30 days</samp></li>
                        <li><span>82</span><samp>all time</samp></li>
                    </ul>
                </div>-->
            </div>
            
            <div class="topfilter homeComm">
            	<h2>Our Community</h2>
                <ul>
                	<li>
                    	<div class="wrapCont">
                            <figure><img src="{!! asset('assets/frontend') !!}/images/z1.png" alt=""></figure>
                            <div class="cText">
                                <span>{!!\App\User::all()->count()!!}</span>
                                <p>users</p>
                            </div>
                        </div>
                    </li>
                    <!--<li>
                    	<div class="wrapCont">
                            <figure><img src="{!! asset('assets/frontend') !!}/images/z2.png" alt=""></figure>
                            <div class="cText">
                                <span>88</span>
                                <p>Speakers</p>
                            </div>
                        </div>
                    </li>-->
                    <li>
                    	<div class="wrapCont">
                            <figure><img src="{!! asset('assets/frontend') !!}/images/z3.png" alt=""></figure>
                            <div class="cText">
                                <span>{!!\App\UserAddres::all()->groupBy('address')->count()!!}</span>
                                <p>Countries</p>
                            </div>
                        </div>
                    </li>
                    <!--<li>
                    	<div class="wrapCont">
                            <figure><img src="{!! asset('assets/frontend') !!}/images/google_translate.png" alt=""></figure>
                            <div class="cText">
                                <span>12</span>
                                <p>languages</p>
                            </div>
                        </div>
                    </li>-->
                    <li>
                    	<div class="wrapCont">
                            <figure><img src="{!! asset('assets/frontend') !!}/images/z4.png" alt=""></figure>
                            <div class="cText">
                                <span>{!!\App\WorkExperience::all()->groupBy('company')->count()!!}</span>
                                <p>professions</p>
                            </div>
                        </div>
                    </li>
                    <li>
                    	<div class="wrapCont">
                            <figure><img src="{!! asset('assets/frontend') !!}/images/z6.png" alt=""></figure>
                            <div class="cText">
                                <span>
								<?php 
								$currentDate = \Carbon\Carbon::now();
								/*$agoDate = $currentDate->subDays($currentDate->dayOfWeek)->subWeek();*/
								$week_answer= \App\Answer::where('created_at', '>=', $currentDate->subWeek())->get();
								if(!empty($week_answer)){
									$cno = ceil(count($week_answer)/7);
								}else	$cno = 0;
								echo $cno;
								?>
								</span>
                                <p>Questions answered each day</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            
            <div class="contentmiddle qsPage">
			@if(!empty($user_data->touser_forwards))				
				@foreach($user_data->touser_forwards as $frwd_qsn)
				<?php $one_quest = $frwd_qsn->question;
				$qsn_id = $one_quest->id; ?>
				@if((!in_array($one_quest->id,$confirm_pass_arr)))
            	<div class="middlerow clear" <?php echo (in_array($one_quest->id,$pass_arr))?'style="background-color:#ececec;"':'';?>>
                	<div class="middleleft">
                    	<div class="usertop">
							<?php $profImag = asset('assets/frontend/images/profile.jpg');
							if($frwd_qsn->from_usr->profile_image !='' && file_exists('assets/upload/profile_image/'.$frwd_qsn->from_usr->profile_image)==1)
							{
								$profImag =asset('assets/upload/profile_image/'.$frwd_qsn->from_usr->profile_image);
							}									
							?>
                        	<div class="hovernxt"><figure><img src="{!!$profImag!!}" width="47" height="47" alt=""></figure>
                            <!--
                            <div class="hoverSpace">
                                <div class="hoverBox clear">
                                    <div class="leftPop">
                                        <figure><img src="{!!asset('assets/frontend')!!}/images/profile.jpg" alt=""></figure>
                                        <h2>Chris Lockwood</h2>
                                        <a href="#" class="bluebtn">message</a>
                                    </div>
                                    <div class="rightPop">
                                        <ul>
                                            <li><i class="fa fa-plus" aria-hidden="true"></i> Registered : 16-09-2016</li>
                                            <li><i class="fa fa-heart" aria-hidden="true"></i> Age : 26</li>
                                            <li><i class="fa fa-transgender" aria-hidden="true"></i> Sex : Male</li>
                                            <li><i class="fa fa-map-marker" aria-hidden="true"></i> Country : India</li>
                                            <li><i class="fa fa-building-o" aria-hidden="true"></i> Vietin Bank</li>
                                            <li><i class="fa fa-wrench" aria-hidden="true"></i> Web Developer</li>
                                        </ul>
                                    </div>
                                </div>
                            </div> -->
                            <!-- -->
                            </div>
                            <span><a href="{{url('profile/'.$frwd_qsn->from_user)}}">{{get_user_name($frwd_qsn->from_user)}} </a></span>
							<samp>{!!$action[$frwd_qsn->type]!!}</samp>
                            <div class="blockArrow">
                                <ul>
                                    <li>
										<a href="javascript:void(0);" class="qsnfollowbtn {{in_array($one_quest->id,$userfollows)?'factive':''}}">
											<img src="{!!asset('assets/frontend')!!}/images/a3.png" alt=""/>
											<span style="{{in_array($one_quest->id,$userfollows)?'color:#3498db':''}}">{{in_array($one_quest->id,$userfollows)?'Unfollow':'follow'}}</span>
										</a>
										<input type="hidden" value="{{$one_quest->id}}"/>
									</li>
                                    <li><a href="#"><img src="{!!asset('assets/frontend')!!}/images/a1.png" alt=""><span>save link</span></a></li>
                                    <li><a href="#"><img src="{!!asset('assets/frontend')!!}/images/a2.png" alt=""><span>report</span></a></li>
                                </ul>
                            </div>
                            
                        </div>
                        <h3 class="descqs"><a href="{{url('user/question-details/'.$one_quest->id)}}"><i class="fa fa-file-text-o" aria-hidden="true"></i>{{$one_quest->title}}</a></h3>
                        <p class="des-text">{!!nl2br($one_quest->content)!!}</p>
                        <div class="mobileCount">
                        	<div class="topsmall">
                                <span>{{time_elapsed_string(strtotime($frwd_qsn->created_at))}}</span>
                                <span>About 
								@if(!empty($one_quest->question_tropics))
									@foreach($one_quest->question_tropics as $val)
										<a href="{{url('topic/'.$val->tropic->id)}}" class="qpop">{{$val->tropic->title}}</a> , 
									@endforeach
								@endif
								</span>
                            </div>
                            <div class="noDesc">
                                <div class="retail">
                                    <span class="noLext">To</span>
                                    <ul>
                                        <li>Chris Lockwood - Lorem</li>
                                        <li>Chris Lockwood - Lorem</li>
                                        <li>Chris Lockwood - Lorem</li>
                                        <li>Chris Lockwood - Lorem</li>
                                        <li>Chris Lockwood - Lorem</li>
                                        <li>Chris Lockwood - Lorem</li>
                                        <li>Chris Lockwood - Lorem</li>
                                        <li>Chris Lockwood - Lorem</li>
                                        <li>Chris Lockwood - Lorem</li>
                                        <li>Chris Lockwood - Lorem</li>
                                        <li>Chris Lockwood - Lorem</li>
                                    </ul>
                                    <a href="javascript:void(0);" class="moreTo">View All</a>
                                </div>
                            </div>
                        	<ul>
                            	<li><i class="fa fa-eye" aria-hidden="true"></i> <span>{!!count($one_quest->qsn_views)!!}</span> views</li>
								<li><i class="fa fa-comments-o" aria-hidden="true"></i> <span>{!!count($one_quest->answers)!!}</span> answers</li>
								<li>
									<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <span>
									<?php $cnt=0;
										if(!empty($one_quest->answers))
										{
											foreach($one_quest->answers as $val)
											{
												$cnt+= count($val->upvotes);
											}
										}
										echo $cnt;
										?>
									</span> votes
								</li>
                            </ul>
                        </div>
                        <a class="readans" href="javascript:void(0);">Read {{count($one_quest->answers)}} answers</a>
                        <div class="viewqsbox">
						@if(!empty($one_quest->answers))						
							@foreach($one_quest->answers as $answer)
                            <div class="qsbox clear">
								<?php $profImag = asset('assets/frontend/images/profile.jpg');
								if($answer->answerUser->profile_image !='' && file_exists('assets/upload/profile_image/'.$answer->answerUser->profile_image)==1)
								{
									$profImag =asset('assets/upload/profile_image/'.$answer->answerUser->profile_image);
								}									
								?>
                            	<figure><img src="{!!$profImag!!}}" alt=""></figure>
                                <h4><a href="">{{$answer->answerUser->nickname}}</a></h4>
                                <p>{!!substr($answer->content,0,200)!!} <a href="#">View More</a></p>
                                <span class="pstTime">{{time_elapsed_string(strtotime($answer->created_at))}}</span>
                            </div>
							@endforeach
						@endif
                        </div>
                        <ul class="question-tags">
                        	<li><a href="{{url('user/question-details/'.$one_quest->id)}}" class="bluebg">Answer</a></li>
                            <li><a href="#" class="pinkbg">Pass</a></li>
                            <!--<li><a href="#forwordPop" class="yellowbg shortqspopup">Forward</a></li>-->
                            <li><a href="#askpopup" class="yellowbg shortqspopup frwdbtn">Forward<input type="hidden" value="{{$one_quest->id}}"/></a></li>
                        </ul>
                    </div>
                </div>
				
				@if(in_array($qsn_id,$userfollows) && !empty($one_quest->answers)	)
					@foreach($one_quest->answers as $answer)
						<div class="middlerow clear">
							<div class="middleleft">
								<div class="usertop">
									<?php $profImag = asset('assets/frontend/images/profile.jpg');
									if($answer->answerUser->profile_image !='' && file_exists('assets/upload/profile_image/'.$answer->answerUser->profile_image)==1)
									{
										$profImag =asset('assets/upload/profile_image/'.$answer->answerUser->profile_image);
									}									
									?>
									<div class="hovernxt"><figure><img src="{!!$profImag!!}" width="47" height="47" alt=""></figure>
									<!--
									<div class="hoverSpace">
										<div class="hoverBox clear">
											<div class="leftPop">
												<figure><img src="{!!asset('assets/frontend')!!}/images/profile.jpg" alt=""></figure>
												<h2>Chris Lockwood</h2>
												<a href="#" class="bluebtn">message</a>
											</div>
											<div class="rightPop">
												<ul>
													<li><i class="fa fa-plus" aria-hidden="true"></i> Registered : 16-09-2016</li>
													<li><i class="fa fa-heart" aria-hidden="true"></i> Age : 26</li>
													<li><i class="fa fa-transgender" aria-hidden="true"></i> Sex : Male</li>
													<li><i class="fa fa-map-marker" aria-hidden="true"></i> Country : India</li>
													<li><i class="fa fa-building-o" aria-hidden="true"></i> Vietin Bank</li>
													<li><i class="fa fa-wrench" aria-hidden="true"></i> Web Developer</li>
												</ul>
											</div>
										</div>
									</div> -->
									<!-- -->
									</div>
									<span>
									<?php //$frward = $user_data->touser_forwards()->where('question_id',$one_quest->id)->first();
									
									?>
									{{get_user_name($answer->user_id)}} </span>
									<samp>Replied to Question</samp>
									<div class="blockArrow">
										<ul>
											<li>
												<a href="javascript:void(0);" class="qsnfollowbtn {{in_array($one_quest->id,$userfollows)?'factive':''}}">
													<img src="{!!asset('assets/frontend')!!}/images/a3.png" alt=""/>
													<span style="{{in_array($one_quest->id,$userfollows)?'color:#3498db':''}}">{{in_array($one_quest->id,$userfollows)?'Unfollow':'follow'}}</span>
												</a>
												<input type="hidden" value="{{$one_quest->id}}"/>
											</li>
											<li><a href="#"><img src="{!!asset('assets/frontend')!!}/images/a1.png" alt=""><span>save link</span></a></li>
											<li><a href="#"><img src="{!!asset('assets/frontend')!!}/images/a2.png" alt=""><span>report</span></a></li>
										</ul>
									</div>
									
								</div>
								<h3 class="descqs"><a href="{{url('user/question-details/'.$one_quest->id)}}"><i class="fa fa-file-text-o" aria-hidden="true"></i>{{$one_quest->title}}</a></h3>
								<p class="des-text">{!!nl2br($one_quest->content)!!}</p>
								<div class="mobileCount">
									<div class="topsmall">
										<span>{{time_elapsed_string(strtotime($one_quest->created_at))}}</span>
										<span>About
										@if(!empty($one_quest->question_tropics))
											@foreach($one_quest->question_tropics as $val)
												<a href="{{url('topic/'.$val->tropic->id)}}" class="qpop">{{$val->tropic->title}}</a> , 
											@endforeach
										@endif
										</span>
									</div>
									<div class="noDesc">
										<div class="retail">
											<span class="noLext">To</span>
											<ul>
												<li>Chris Lockwood - Lorem</li>
												<li>Chris Lockwood - Lorem</li>
												<li>Chris Lockwood - Lorem</li>
												<li>Chris Lockwood - Lorem</li>
												<li>Chris Lockwood - Lorem</li>
												<li>Chris Lockwood - Lorem</li>
												<li>Chris Lockwood - Lorem</li>
												<li>Chris Lockwood - Lorem</li>
												<li>Chris Lockwood - Lorem</li>
												<li>Chris Lockwood - Lorem</li>
												<li>Chris Lockwood - Lorem</li>
											</ul>
											<a href="javascript:void(0);" class="moreTo">View All</a>
										</div>
									</div>
									<ul>
										<li><i class="fa fa-eye" aria-hidden="true"></i> <span>{!!count($one_quest->qsn_views)!!}</span> views</li>
										<li><i class="fa fa-comments-o" aria-hidden="true"></i> <span>{!!count($one_quest->answers)!!}</span> answers</li>
										<li>
											<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <span>
											<?php $cnt=0;
												if(!empty($one_quest->answers))
												{
													foreach($one_quest->answers as $val)
													{
														$cnt+= count($val->upvotes);
													}
												}
												echo $cnt;
												?>
											</span> votes
										</li>
									</ul>
								</div>
								<a class="readans" href="javascript:void(0);">Read {{count($one_quest->answers)}} answers</a>
								<div class="viewqsbox">
								@if(!empty($one_quest->answers))						
									@foreach($one_quest->answers as $answer)
									<div class="qsbox clear">
										<?php $profImag = asset('assets/frontend/images/profile.jpg');
										if($answer->answerUser->profile_image !='' && file_exists('assets/upload/profile_image/'.$answer->answerUser->profile_image)==1)
										{
											$profImag =asset('assets/upload/profile_image/'.$answer->answerUser->profile_image);
										}									
										?>
										<figure><img src="{!!$profImag!!}" alt=""></figure>
										<h4><a href="">{{$answer->answerUser->nickname}}</a></h4>
										<p>{!!substr($answer->content,0,200)!!} <a href="#">View More</a></p>
										<span class="pstTime">{{time_elapsed_string(strtotime($answer->created_at))}}</span>
									</div>
									@endforeach
								@endif
								</div>
								<ul class="question-tags">
									<li><a href="{{url('user/question-details/'.$one_quest->id)}}" class="bluebg">Answer</a></li>
									<li><a href="#" class="pinkbg">Pass</a></li>
									<!--<li><a href="#forwordPop" class="yellowbg shortqspopup">Forward</a></li>-->
									<li><a href="javascript:void(0);" class="yellowbg shortqspopup frwdbtn">Forward<input type="hidden" value="{{$one_quest->id}}"/></a></li>
								</ul>
							</div>
						</div>
					@endforeach
				@endif
				
				@endif
				@endforeach				
			@endif
			</div>
        </div>
        <!--middle close-->

@include('include.right_pan')
</section>
@endsection
@section('customScript')
<script>

$('.frwdbtn').click(function(){
	
	$('#qsnFrm').find("input[type=text], select, textarea").val("");
	
	$('#sec1,#sec2,#sec3,#sec4,#sec5,#sec6,#sec7').find("input[type=text], select, textarea").prop('disabled', true);			
	$('#sec1,#sec2,#sec3,#sec4,#sec5,#sec6,#sec7').css('display','none');
	var qobj = $(this);
	var qid = qobj.children('input').val();
	//alert(qid);
	$.ajax({
		type:"post",
		url: "{!! url('user/get-question') !!}" ,
		headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
		data: {'qid':qid},			
		dataType: "json",			
		success:function(res) {
			//console.log(res);
			if(res)
			{			
				
				$('#inpids').val(res.question.to);		
				$('#emails').val('');
				$('#inpids').val('');				
				$('#qsnFrm').find('select[name="post_type"]').val(res.question.post_type);
				$('#qsnFrm').find('input[name="tags"]').val(res.question.tags);
				$('#qsnFrm').find('input[name="title"]').val(res.question.title);
				$('#qsnFrm').find('textarea[name="content"]').val(res.question.content);
				$('#qsnFrm').find('input[name="qsn_id"]').val(res.question.id);
				$('#qsnFrm').find('input[name="is_forword"]').val(1);
				$('#qsnFrm').find('input[type="submit"]').val('Forward');
				$('#sec8').show();
				$('#sec8').find("textarea").prop('disabled', false);
				$('#asktlt').text('Forward ');
				//$('#sec4').find('.label').text('Note:');
				//$('#sec1,#sec5,#sec6,#sec7').remove();	
				$('#qsn_id').val(res.question.id);
			} 
			
		}
	});
});

$('.frwdbtn').fancybox({
	padding:0,
	afterShow: function(){
		$(".viewansbox").mCustomScrollbar();
	},
	afterClose: function() {
		$('#sec8').find("textarea").prop('disabled', true);
		$('#sec8').hide();
		$('#qsnFrm').find('input[type="submit"]').val('Submit Question');
		$('#asktlt').text('Ask a Question');
	}
});
$('a.qsnfollowbtn').click(function(){
	var fobj = $(this);
	var qsnfollow = fobj.next('input').val();
	/* alert(qsnfollow); */
	$.ajax({
		type:"post",
		url: "{{url('user/qsn-follow-unfollow')}}" ,
		headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
		data: {'qsnfollow':qsnfollow},			
		dataType: "json",			
		success:function(res) {
			/* console.log(res); */
			if(res==0){
				fobj.children('span').text('Unfollow').css('color','#3498db');
				fobj.addClass('factive');
			}
			else if(res==2){
				fobj.children('span').text('Follow').css('color','#6c6c6c');
				fobj.removeClass('factive');
			}
		}
	});	
});
</script>
@endsection