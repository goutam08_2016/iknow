@extends('layouts.app')

@section('pageTitle', 'Welcome to ')

@section('customStyle')

@endsection

@section('content')

    <!--main content open-->
    <section class="upadted">
    	<div class="wrapper">
        	<h2>Update Details</h2>
                <div class="updated-box">
					@if ($errors->any())
						<div class="warnings">
							<div class="error_message">
								@foreach ($errors->all() as $error)
									<p>{!! $error !!}</p>
								@endforeach
							</div>
						</div>
					@elseif(session('success'))
						<div class="success_message">
							<p>{!! session('success') !!}</p>
						</div>
						<?php session()->forget('success');?>
					@endif
                	<form name="update" method="post" action="{{url('user/update-account')}}" novalidate enctype="multipart/form-data">
						{{ csrf_field() }}
                        <div class="updated-cont">
							
                            <div class="formrow clear">
                            	<div class="formcol">
                                	<label>
                                    	<span class="label">Profile Image *</span>
                                        <div class="textfld">
                                        	<input type="file" name="profile_image" />
                                        </div>
                                    </label>
                                </div>
                            	<div class="formcol">
                                	<label>
										@if(!empty($user->profile_image))
										<img src="{!!asset('assets/upload/profile_image/'.$user->profile_image)!!}"/>
										@endif
                                    </label>
                                </div>
                            </div>
                        	<div class="formrow clear">
                            	<div class="formcol">
                                	<label>
                                    	<span class="label">First name *</span>
                                        <div class="textfld">
                                        	<input type="text" name="first_name" value="{{$user->first_name}}"/>
                                        </div>
                                    </label>
                                </div>
                                <div class="formcol">
                                	<label>
                                    	<span class="label">Last name *</span>
                                        <div class="textfld">
                                        	<input type="text" name="last_name" value="{{$user->last_name}}">
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="formrow clear">
                            	<div class="formcol">
                                	<label>
                                    	<span class="label">Username *</span>
                                        <div class="textfld">
                                        	<input type="text" name="user_name" value="{{$user->user_name}}">
                                        </div>
                                    </label>
                                </div>
                                <div class="formcol">
                                	<label>
                                    	<span class="label">Email address *</span>
                                        <div class="textfld">
                                        	<input type="email" name="email" value="{{$user->email}}" />
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="formrow clear">
                            	<div class="formcol">
                                	<label>
                                    	<span class="label">Address *</span>
                                        <div class="textfld">
                                        	<input type="text" id="address" name="address" value="{{$user->address}}" />
											<input type="hidden" name="postal_code" id="postal_code" value="{{$user->postal_code}}" />
											<input type="hidden" name="lat" id="lat" value="{{$user->lat}}" />
											<input type="hidden" name="lng" id="lng" value="{{$user->lng}}" />
                                        </div>
                                    </label>
                                </div>
                                <div class="formcol">
                                	<label>
                                    	<span class="label">Select your Time-Zone *</span>
                                        <div class="textfld">
											<select name="utc_timezone">
											<option value=""> Please select </option>
											@if(!empty($timezones))
												@foreach($timezones as $val)
													<option value="{!!$val->id!!}" {{ $user->utc_timezone == $val->id ? 'selected' : '' }} > ( GMT {{ $val->UTC_offset }} ) {{ $val->TimeZone }} </option>
												@endforeach
											@endif                               	
											</select>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="formrow  clear">
                            	<div class="formcol">
                                	<label>
                                    	<span class="label">Display Name *</span>
                                        <div class="textfld">
                                        	<input type="text" readonly value="{{$user->user_name}}">
                                        </div>
                                    </label>
                                </div>
                                <div class="formcol">
                                	<span class="smalltxt">Tour users see this when logged in</span>
                                </div>
                            </div>
                            <div class="formrow last clear">                            	
								<div class="formcol">
                                	<label>
                                    	<span class="label">Company Name</span>
                                        <div class="textfld">
                                        	<input type="text" name="company_name" value="{{$user->company_name}}" />
                                        </div>
                                    </label>
                                </div>
                                <div class="formcol">
                                	<label>
                                    	<span class="label">Company Number</span>
                                        <div class="textfld">
                                        	<input type="text" name="company_number" value="{{$user->company_number}}" />
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="formrow last clear">                            	
								<div class="formcol">
                                	<label>
                                    	<span class="label">Vat Number</span>
                                        <div class="textfld">
                                        	<input type="text" name="vat_number" value="{{$user->vat_number}}" />
                                        </div>
                                    </label>
                                </div>
                                <div class="formcol">
                                	
                                </div>
                            </div>
                        </div>
                        <div class="updated-cont">
                        	<a href="javascript:void(0);" class="changepass">Change login password</a>
                        	<div class="changepassword">
                            	<p class="notetxt">Leave these 'New password' fields blank if you 'Do not' want to change your password.</p>
                                <div class="formrow clear">
                                    <div class="formcol">
                                        <label>
                                            <span class="label">New password *</span>
                                            <div class="textfld">
                                                <input type="password" name="password" value="" >
                                            </div>
                                        </label>
                                    </div>
                                    <div class="formcol">
                                        <span class="smalltxt">Min 6 characters</span>
                                    </div>
                                </div>
                                <div class="formrow clear">
                                    <div class="formcol">
                                        <label>
                                            <span class="label">Retype New password</span>
                                            <div class="textfld">
                                                <input type="password" name="password_confirmation" value=""/>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="formcol">
                                        <label>
                                            <span class="label">Current login password *</span>
                                            <div class="textfld">
                                                <input type="password" name="old_password" value=""/>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <p class="requiredtxt">Always required when updating any details.</p>
                                <div class="submit-row">
                                	<input type="reset" class="colorbtn" value="Cancel">
                                    <input type="submit" class="colorbtn bluebtn" value="Update">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!--main content close-->
    

@endsection

@section('customScript')

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ config('constants.GM_API_KEY') }}&libraries=places"></script>
<script type="text/javascript">

function initialize() {
	var input = document.getElementById('address');
	
	var options = {
		//componentRestrictions: {country: 'CA'},		
		//bounds: defaultBounds,
		//types: ['establishment']
	};
	var autocomplete = new google.maps.places.Autocomplete(input,options);
	
	google.maps.event.addListener(autocomplete, 'place_changed', function() {
		var place = autocomplete.getPlace();
		if (!place.geometry) {
			return;
		}

		if(!place.address_components) {
			return;
		}
		var postal_code;
		place.address_components.forEach(function (item, index) {
			if(item.types[0] === "postal_code")
				postal_code =item.long_name;
		});
		if(postal_code)
			$("#postal_code").val(postal_code);

		if (place.geometry.location) {
			var lat = place.geometry.location.lat();
			var lng = place.geometry.location.lng();
			$("#lat").val(lat);
			$("#lng").val(lng);
		}
	});
	// Stop form submission when enter key pressed.
	google.maps.event.addDomListener(input, 'keydown', function(e) {
		if (e.keyCode == 13) {
			e.preventDefault();
		}
	});
	
}
 google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endsection