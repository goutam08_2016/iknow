@extends('layouts.app')

@section('pageTitle', 'Welcome to ')
@section('seo_sec')
@if(!empty($cmsdata))
<title> {{$cmsdata->seo_title}}</title>
<meta name="description" content="{{$cmsdata->seo_description}}">
<meta name="keywords" content="{{$cmsdata->seo_keywords}}">
@endif
@endsection
@section('customStyle')

@endsection

@section('content')
	<?php $packduration = array('1'=>'monthly','3'=>'quarterly','6'=>'half-yearly','12'=>'yearly')?>
    <!--main content open-->
    <section class="about-top pricingpage">
    	<div class="wrapper clear">
        	<h2>Pricing table</h2>
			<div class="error_message">
				<?php
				if(!empty($current_plan))
				{
					$startd = strtotime($current_plan->pivot->created_at);
					$exp = ($current_plan->duration * 30*86400);
					$expd = (int)$startd + $exp;
				}
				?>
				{{!empty($current_plan)? 'Package valid from '.date('d M,Y',$startd).' date to '.date('d M,Y', $expd ).' date and after '.date('d M,Y', $expd ).' date you can choose other package': '' }}
			</div>
			@if(session('upgrade_success'))
				<div class="success_message">
					<p>{!! session('upgrade_success') !!}</p>
				</div>
				<?php session()->forget('upgrade_success'); ?>
			@endif
			<div class="pricetable">			
				@if(!empty($packages))
					@foreach($packages as $ky=>$val)
						<div class="pricebox {{(!empty($current_plan) && $current_plan->id==$val->id)?'slct':''}}">
							<span class="pricehead">{{$val->plan}}</span>
							<span class="pricevalue">{{$val->price==0 ?'Free Package':'$'.$val->price}} / {{!empty($packduration[$val->duration])?$packduration[$val->duration]:''}}</span>
							<span class="pricelink">
							@if($val->price==0)							
								<a href="javascript:void(0)" class="colorbtn free">Free</a>
							@else
								@if(Auth::check()==false)
									<a href="{{url('login')}}" class="colorbtn free">Upgrade</a>
								@else
								<form method="POST" action="{{url('user/upgrade')}}">
									<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
									<input type="submit" class="colorbtn" value="{{(!empty($current_plan) && $current_plan->id==$val->id)?'Current Plan':'Upgrade'}}" {{!empty($current_plan)? 'disabled': '' }} />
									<input type="hidden" name="package_id" value="{{$val->id}}" />
								</form>
								@endif
							@endif
							</span>
							<ul class="pricedeta">
								<li>Lorem Ipsum is simply dummy text</li>
								<li>Lorem Ipsum is simply dummy text</li>
								<li>Lorem Ipsum is simply dummy text</li>
								<li>Lorem Ipsum is simply dummy text</li>
								<li>Lorem Ipsum is simply dummy text</li>
							</ul>
						</div>					
					@endforeach
				@endif  
			</div>
        </div>
    </section>
    <!--main content close-->    
        
@endsection

@section('customScript')

<script>
$(document).ready(function() {
    
});
</script>
@endsection