@extends('layouts.app')

@section('pageTitle', 'Welcome to ')

@section('customStyle')

@endsection

@section('content')
	<div class="innerPage">
		<div class="wrapper">
			<div class="logRegContainer">
				<h2>Account Details</h2>
				<div class="logRegForm">
                    @if($errors->any())
                        <div id="login_warnings" class="warnings" style="">
                            <div id="login_error_message" class="error_message">
                                @foreach ($errors->all() as $error)
                                    <p>{!! $error !!}</p>
                                @endforeach
                            </div>
                        </div>
                    @elseif(session('success'))
                        <div class="success_message">
                            <p>{!! session('success') !!}</p>
                        </div>
                    @endif
					<form method="post" action="{{ url('user/change-password') }}" data-parsley-validate="">
						{{ csrf_field() }}
                        <label class="formRow">
                            <span class="formTtl">Old Password :</span>
                            <div class="formFld">
                                <input type="password" name="old_password">
                            </div>
                        </label>
                        <label class="formRow">
                            <span class="formTtl">New Password :</span>
                            <div class="formFld">
                                <input type="password" name="password">
                            </div>
                        </label>
                        <label class="formRow">
                            <span class="formTtl">Retype New Password :</span>
                            <div class="formFld">
                                <input type="password" name="password_confirmation">
                            </div>
                        </label>
						<div class="formRow">
							<input type="submit" class="button full" value="Change Password"/>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('customScript')

@endsection