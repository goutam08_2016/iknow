@extends('layouts.app')



@section('pageTitle', 'Welcome to ')



@section('customStyle')

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

@endsection



@section('content')

	<!--body open-->
    <section class="mainbody clear">
    	<!--left pan open-->
    	@include('include.left_pan')
        <!--left pan close-->
        
        <!--middle open-->
        <div class="middlecol">
            <div class="pTab">
            	@include('include.setting_tabs')
				<div class="tabContWrap settingcont">
					<form id="notify" method="post" action="">
						<div class="settingbox">
							<h3>Request Answer</h3>
							<div class="settingrow">
								<label>
									<input type="checkbox" name="answer_question" value="Y" onChange="$('#notify').submit();" {{(!empty($prevdata) && $prevdata->answer_question=='Y') || empty($prevdata)?'checked':''}} >
									<span class="chkicon"></span>
									<span class="chklabel">Question people want you to answer</span>
								</label>
								<p class="smallNot">We'll notify you when another person requests your answer to a question.</p>
							</div>
						</div>
						<div class="settingbox">
							<h3>Upvotes and new followers</h3>
							<div class="settingrow">
								<label>
									<input type="checkbox" value="Y" name="upvote" onChange="$('#notify').submit();" {{(!empty($prevdata) && $prevdata->upvote=='Y') || empty($prevdata)?'checked':''}} >
									<span class="chkicon"></span>
									<span class="chklabel">Upvotes</span>
								</label>
								<p class="smallNot">We'll notify you when another person upvotes a question or comment you wrote.</p>
							</div>
							<div class="settingrow">
								<label>
									<input type="checkbox" name="new_follower" value="Y" onChange="$('#notify').submit();" {{(!empty($prevdata) && $prevdata->new_follower=='Y') || empty($prevdata)?'checked':''}} >
									<span class="chkicon"></span>
									<span class="chklabel">New Followers</span>
								</label>
								<p class="smallNot">We'll notify you when a new person starts following you</p>
							</div>
						</div>
						<div class="settingbox">
							<h3>Messages, Mentions, and Comments</h3>
							<div class="settingrow">
								<label>
									<input type="checkbox" name="new_nessage" value="Y" onChange="$('#notify').submit();" {{(!empty($prevdata) && $prevdata->new_nessage=='Y') || empty($prevdata)?'checked':''}} >
									<span class="chkicon"></span>
									<span class="chklabel">New Messages</span>
								</label>
								<p class="smallNot">We'll notify you when someone send you a new message on <strong>iKnow</strong>.</p>
							</div>
							<div class="settingrow">
								<label>
									<input type="checkbox" name="tag_me" value="Y" onChange="$('#notify').submit();" {{(!empty($prevdata) && $prevdata->tag_me=='Y') || empty($prevdata)?'checked':''}} >
									<span class="chkicon"></span>
									<span class="chklabel">Mentions</span>
								</label>
								<p class="smallNot">We'll notify you when someone mentions (tag) you in a answer, comment, or post</p>
							</div>
							<div class="settingrow">
								<label>
									<input type="checkbox" name="comment_my" value="Y" onChange="$('#notify').submit();" {{(!empty($prevdata) && $prevdata->comment_my=='Y') || empty($prevdata)?'checked':''}} >
									<span class="chkicon"></span>
									<span class="chklabel">Comments</span>
								</label>
								<p class="smallNot">We'll notify you when someone comments on your answers, quiestions, and posts.</p>
							</div>
						</div>
					</form>
                </div>
				
            </div>
        </div>
        <!--middle close-->
        
        <!--right pan open-->
        @include('include.right_pan')
        <!--right pan close-->
    </section>
    <!--body close-->

@endsection



@section('customScript')

<script>
$( "form#notify" ).on( "submit", function( event ) {
  event.preventDefault();
  var formdata =  $( this ).serialize();
  //console.log( $( this ).serialize() );
  updateNotifySettings(formdata);
});

function updateNotifySettings(formdata)
{
	$.ajax({
		type:"post",
		url: "{!! url('user/update-notify-settings') !!}" ,
		headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
		data: formdata,			
		//dataType: "json",			
		error:function(erres) {	
			console.log(erres.responseText); 
			var reponse = JSON.parse(erres.responseText);
			
		},
		success:function(res) {
			//console.log(res); 
			
		}

	});
}
</script>

@endsection