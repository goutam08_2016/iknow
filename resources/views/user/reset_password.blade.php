<?php
//use App\Helpers\CustomHelper;
?>
@extends('layouts.basic')

@section('pageTitle', 'Hotcupido.com | Reset Password')

@section('customStyle')

@endsection

@section('content')

<div class="middleadjstcont"> 
<div class="popupDiv frgtpassform">
  <div class="popupDivHeader"> 
    <span class="popupTtl">Reset Password</span>
    <div class="clear"></div>
  </div>
  <div class="popupDivBody">
  @if($errors->any())
  <div class="warnings">
      <strong>
          @foreach($errors->all() as $error)
          <p>{{ $error }}</p>
          @endforeach
      </strong>
  </div>
  @elseif(session('reset_pass_success'))
  <div class="success">
      <strong>{{ session('reset_pass_success') }}</strong>
  </div>
  @endif
  @if(session('error_msg'))
      <div class="warnings">
          {!! session('error_msg') !!}
      </div>
  @endif

    {!! csrf_field() !!}
    <form method="post" action="{{url('reset-password')}}" name="reset_pass" data-parsley-validate>
    {!! csrf_field() !!}
    <input type="hidden" name="token" value="{{ $token }}">
      <div class="popupCont inline">
        <div class="logForm">
          <div class="formRow">
              <span class="formTtl">Password <font>*</font></span>
              <span class="formFld">
                  <input type="password" name="password">
              </span>
          </div>
          <div class="formRow">
              <span class="formTtl">Confirm Password <font>*</font></span>
              <span class="formFld">
                  <input type="password" name="password_confirmation">
              </span>
          </div>          
        </div>
        <div class="clear"></div>
      </div>
      <div class="popupFooter">
        <button type="submit" class="full">Reset Password</button>
      </div>
    </form>
  </div>
</div>
</div>

@endsection

@section('customScript')

@endsection
