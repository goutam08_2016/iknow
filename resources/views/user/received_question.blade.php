@extends('layouts.app')



@section('pageTitle', 'Welcome to ')

@section('content')
<section class="mainbody clear">
@include('include.left_pan')
<div class="middlecol listQuestion singleQs">
        	
	<div class="topfilter clear">
		<div class="filter-left">
			<span>Question Type</span>
			<select>
				<option>All</option>
				<option>Quick Chat</option>
				<option>Detailed Discussion</option>
			</select>
		</div>
		<div class="filter-right">
			<span>Status</span>
			<select>
				<option>Unanswered</option>
				<option>Answered</option>
			</select>
		</div>
	</div>
	<div class="pTab nextF">
		<div class="filterBtn tabmenu">
			<ul>
				<li><a href="{{url('user/sent-question')}}">Sent Question</a></li>
				<li class="active"><a href="javascript:void(0);">Received Question</a></li>
			</ul>
		</div>
	</div>
	<?php
	$recv_quesn=$user_data->touser_forwards()->where(function($q) {
												  $q->where('type', 'F')
													->orWhere('type', 'Q');
											  })->get();
	?>
	@if(count($recv_quesn)>0)
		@foreach($recv_quesn as $frwd_qsn)
		<?php  $one_quest = $frwd_qsn->question;?>
		@if((!in_array($one_quest->id,$confirm_pass_arr)))
		<div class="middlerow clear" <?php echo (in_array($one_quest->id,$pass_arr))?'style="background-color:#ececec;"':'';?>>
			<div class="middleleft">
				<div class="usertop nwUsertop">
					<div class="hovernxt">
						<?php $profImag = asset('assets/frontend/images/profile.jpg');
						if($frwd_qsn->from_usr->profile_image !='' && file_exists('assets/upload/profile_image/'.$frwd_qsn->from_usr->profile_image)==1)
						{
							$profImag =asset('assets/upload/profile_image/'.$frwd_qsn->from_usr->profile_image);
						}									
						?>
					<figure><img src="{!!$profImag!!}" width="47" height="47" alt=""></figure>
					<!--
					<div class="hoverSpace">
						<div class="hoverBox clear">
							<div class="leftPop">
								<figure><img src="{!!asset('assets/frontend')!!}/images/profile.jpg" alt=""></figure>
								<h2>Chris Lockwood</h2>
								<a href="#" class="bluebtn">message</a>
							</div>
							<div class="rightPop">
								<ul>
									<li><i class="fa fa-plus" aria-hidden="true"></i> Registered : 16-09-2016</li>
									<li><i class="fa fa-heart" aria-hidden="true"></i> Age : 26</li>
									<li><i class="fa fa-transgender" aria-hidden="true"></i> Sex : Male</li>
									<li><i class="fa fa-map-marker" aria-hidden="true"></i> Country : India</li>
									<li><i class="fa fa-building-o" aria-hidden="true"></i> Vietin Bank</li>
									<li><i class="fa fa-wrench" aria-hidden="true"></i> Web Developer</li>
								</ul>
							</div>
						</div>
					</div> -->
					<!-- -->
					</div>
					<div class="recUsrInfo">
						<span><a href="{{url('profile/'.$frwd_qsn->from_user)}}">{{get_user_name($frwd_qsn->from_user)}} </a></span>
						<samp>{{$frwd_qsn->type == 'F'?'forward a Question':'asked a Question'}}</samp>
					</div>
					<div class="spacer"></div>
					<div class="blockArrow">
						<ul>
							<li>
								<a href="javascript:void(0);" class="qsnfollowbtn {{in_array($one_quest->id,$userfollows)?'factive':''}}">
									<img src="{!!asset('assets/frontend')!!}/images/a3.png" alt=""/>
									<span style="{{in_array($one_quest->id,$userfollows)?'color:#3498db':''}}">{{in_array($one_quest->id,$userfollows)?'Unfollow':'follow'}}</span>
								</a>
								<input type="hidden" value="{{$one_quest->id}}"/>
							</li>
							<li><a href="#"><img src="{!!asset('assets/frontend')!!}/images/a1.png" alt=""><span>save link</span></a></li>
							<li><a href="#"><img src="{!!asset('assets/frontend')!!}/images/a2.png" alt=""><span>report</span></a></li>
						</ul>
						
					</div>
					
				</div>
                @if($frwd_qsn->note!='')
				<div class="note">{{$frwd_qsn->note}} </div>
                @endif			
                <h3 class="descqs"><a href="{{url('user/question-details/'.$one_quest->id)}}"><i class="fa fa-file-text-o" aria-hidden="true"></i>{{$one_quest->title}}</a></h3>
				<div class="ansbody "><p class="des-text">{!!nl2br($one_quest->content)!!}</p></div>
				<div style="float:right;">
					@if(strlen($one_quest->content)>500)
						<a href="javascript:void(0);" class="ansFull" style="float:right;"></a>
					@endif
				</div>
				<div class="mobileCount">
					<div class="topsmall">
						<span>{{time_elapsed_string(strtotime($frwd_qsn->created_at))}}</span>
						<span>About 
						@if(!empty($one_quest->question_tropics))
							@foreach($one_quest->question_tropics as $val)
								<a href="{{url('topic/'.$val->tropic->id)}}" class="qpop">{{$val->tropic->title}}</a> , 
							@endforeach
						@endif
						</span>
					</div>
					<div class="noDesc">
						<div class="retail">
							<span class="noLext">To</span>
							<ul>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
							</ul>
							<a href="javascript:void(0);" class="moreTo">View All</a>
						</div>
					</div>
					<ul>
						<li><i class="fa fa-eye" aria-hidden="true"></i> <span>{!!count($frwd_qsn->question->qsn_views)!!}</span> views</li>
						<li><i class="fa fa-comments-o" aria-hidden="true"></i> <span>{!!count($frwd_qsn->question->answers)!!}</span> answers</li>
						<li><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <span>
						<?php $cnt=0;
							if(!empty($frwd_qsn->question->answers))
							{
								foreach($frwd_qsn->question->answers as $val)
								{
									$cnt+= count($val->upvotes);
								}
							}
							echo $cnt;
							?>
						</span> votes</li>
					</ul>
				</div>
				<a class="readans" href="javascript:void(0);">Read {{count($one_quest->answers)}} answers</a>
				<div class="viewqsbox">
				@if(!empty($one_quest->answers))						
					@foreach($one_quest->answers as $answer)
					<div class="qsbox clear">
						<figure><img src="{!!asset('assets')!!}{{$answer->answerUser->profile_image!=''?'/upload/profile_image/'.$answer->answerUser->profile_image:'/frontend/images/userthumb.png'}}" alt=""></figure>
						<h4><a href="">{{$answer->answerUser->nickname}}</a></h4>
						<div class="ansbody nwd"><p>{!!$answer->content !!} </p></div>
						<div class="pstTime">
							@if(strlen($answer->content)>240)
							<a href="javascript:void(0);" class="ansFull" style="float:right;"></a>
							@endif
							{{time_elapsed_string(strtotime($answer->created_at))}}
						</div>
					</div>
					@endforeach
				@endif
				</div>
				<ul class="question-tags">
					<li><a href="{{url('user/question-details/'.$one_quest->id)}}" class="bluebg">Answer</a></li>
					<li><a href="javascript:;" class="pinkbg passBtn" qid="{{$one_quest->id}}" >Pass</a></li>
					<!--<li><a href="#forwordPop" class="yellowbg shortqspopup">Forward</a></li>-->
					<li><a href="#askpopup" class="yellowbg shortqspopup frwdbtn">Forward<input type="hidden" value="{{$one_quest->id}}"/></a></li>
				</ul>
			</div>
		</div>
		@endif
	@endforeach
	@endif
	</div>


@include('include.right_pan')
</section>
@endsection
@section('customScript')
<script>
$('.ansFull').click(function(){
		$(this).parent().prev('.ansbody').toggleClass('allShow');
		$(this).toggleClass('active');
	});
$('.frwdbtn').click(function(){
	
	$('#qsnFrm').find("input[type=text], select, textarea").val("");
	$(".emsg").remove();
	$('#sec1,#sec2,#sec3,#sec4,#sec5,#sec6,#sec7').find("input[type=text], select, textarea").prop('disabled', true);			
	$('#sec1,#sec2,#sec3,#sec4,#sec5,#sec6,#sec7').css('display','none');
	var qobj = $(this);
	var qid = qobj.children('input').val();
	//alert(qid);
	$.ajax({
		type:"post",
		url: "{!! url('user/get-question') !!}" ,
		headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
		data: {'qid':qid},			
		dataType: "json",			
		success:function(res) {
			//console.log(res);
			if(res)
			{			
				
				$('#inpids').val(res.question.to);		
				$('#emails').val('');
				$('#inpids').val('');				
				$('#qsnFrm').find('select[name="post_type"]').val(res.question.post_type);
				$('#qsnFrm').find('input[name="tags"]').val(res.question.tags);
				$('#qsnFrm').find('input[name="title"]').val(res.question.title);
				$('#qsnFrm').find('textarea[name="content"]').val(res.question.content);
				$('#qsnFrm').find('input[name="qsn_id"]').val(res.question.id);
				$('#qsnFrm').find('input[name="is_forword"]').val(1);
				$('#qsnFrm').find('input[type="submit"]').val('Forward');
				$('#sec8').show();
				$('#sec8').find("textarea").prop('disabled', false);
				$('#asktlt').text('Forward ');
				//$('#sec1,#sec5,#sec6,#sec7').remove();	
				$('#qsn_id').val(res.question.id);
			} 
			
		}
	});
});
$('.frwdbtn').fancybox({
	padding:0,
	afterShow: function(){
		$(".viewansbox").mCustomScrollbar();
	},
	afterClose: function() {
		$('#sec8').find("textarea").prop('disabled', true);
		$('#sec8').hide();
		$('#qsnFrm').find('input[type="submit"]').val('Submit Question');
		$('#asktlt').text('Ask a Question');
	}
});
$('a.qsnfollowbtn').click(function(){
	var fobj = $(this);
	var qsnfollow = fobj.next('input').val();
	/* alert(qsnfollow); */
	$.ajax({
		type:"post",
		url: "{{url('user/qsn-follow-unfollow')}}" ,
		headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
		data: {'qsnfollow':qsnfollow},			
		dataType: "json",			
		success:function(res) {
			/* console.log(res); */
			if(res==0){
				fobj.children('span').text('Unfollow').css('color','#3498db');
				fobj.addClass('factive');
			}
			else if(res==2){
				fobj.children('span').text('Follow').css('color','#6c6c6c');
				fobj.removeClass('factive');
			}
		}
	});	
});

</script>
<style>

.blockArrow ul li a.factive img{
    -moz-filter: grayscale(0%);
    -webkit-filter: grayscale(0%);
    -ms-filter: grayscale(0%);
    filter: grayscale(0%);
	}
</style>
@endsection