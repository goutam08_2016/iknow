@extends('layouts.app')

@section('pageTitle', 'Welcome to ')

@section('customStyle')

@endsection

@section('content')

    <!--body open-->
    <section class="pagecontent">
       <div class="container">
       		<div class="signupcont">
            	<h2>Sign Up</h2>							
				
            	<div class="signupwrap">					
					<div class="signupStepsCont">
						<div class="signupSteps">
							<span class="st green"><i class="fa fa-check"></i></span>
							<span class="st st2 green"><i class="fa fa-check"></i></span>
							<span class="st st3 green"><i class="fa fa-check"></i></span>
							<span class="st st4 green"><i class="fa fa-check"></i></span>
							<span class="st st5">5</span>
							<div class="stepProgress">
								<div class="current" style="width:75%;"></div>
							</div>
						</div>
					</div>
                    <div class="signupbox steplast clear">
                    	<div class="signupleft">
                        	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book <strong><span>i</span>Know</strong>.</p>
                            <p> It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book <strong><span>i</span>Know</strong>.</p>
                            <div class="buttonbox clear">
								<form name="signup" method="post" action="">   
								{{csrf_field()}}
									<a href="{{url('user/signup-step/4')}}" class="prevstep assbtn">prev</a>
									 <input type="submit" value="Finish" class="nextstep bluebtn">			
								 <input type="hidden" name="in_step" value="{{$step}}" />							
								</form>
							</div>
                        </div>
                        <div class="signupthumb clear">
                        	<img src="{!! asset('assets/frontend') !!}/images/signup-charecter.png" width="146" height="459" alt="">
                        </div>
                    </div>
                </div>
                
            </div>
       </div>
    </section>
    <!--body close-->

@endsection

@section('customScript')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKqvPeyXND6or2BXGG1O-qp-bC_lIz7Uk&libraries=places"></script>
<script>
$(document).ready(function() {
    $('.closebox').click(function() {
		$(this).parent('.addbox').parent('li').remove();
	});
});

</script>

@endsection