@extends('layouts.app')



@section('pageTitle', 'friend request')

@section('content')
<section class="mainbody clear">
@include('include.left_pan')


<!--middle open-->
<div class="middlecol">
	<div class="contactNext">
		<div class="contactHead">
			<div class="titlebox clear">
				<h2><i class="fa fa-user" aria-hidden="true"></i> Contact</h2>
				<a href="{{url('user/friend-request')}}">Friend Request<span>{!!count($user->user_friend_requests)!!}</span></a>
			</div>
			<div class="smallHead clear">
				<ul class="quickLink">
					<li class="active"><a href="{{url('user/friends')}}">Friends</a></li>
					<li><a href="{{url('user/followers')}}">Followers</a></li>
					<!--<li><a href="#">Friend Request<span>24</span></a></li>-->
				</ul>
				<div class="frndSrch">
					<a href="{{url('user/search-people')}}" class="srchTool">Advanced Search</a>
					<div class="adnanceSrc">
						<form id="frndsrch" name="srch" method="get" action="">
							<input type="text" name="keywrd" placeholder="Search Friends">
							<input type="hidden" id="tab_slc" name="tab" value="1"/>
							<button type="submit" value="search"><i class="fa fa-search" aria-hidden="true"></i></button>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		<!--<div class="topPeople clear">
				<div class="filLeft clear">
					<div class="filCol">
						<span class="label">Location</span>
						<div class="selectbox">
							<input type="text" value="">
						</div>
					</div>
					<div class="filCol">
						<span class="label">Profession</span>
						<div class="selectbox">
							<input type="text" value="">
						</div>
					</div>
				</div>
				<!--<a href="" class="recLink bluebtn">Recommendation</a>-->
			<!--</div>-->
		
		<div class="conTab">
			
			<div class="closebar tabmenu">
				<ul>
					<li><a href="javascript:void(0);" id="tab1">All Friends</a></li>
					<li><a href="javascript:void(0);" id="tab2">Closed Friends</a></li>
				</ul>
			</div>
			
			<div class="contList tabContWrap">
				
				<div class="allpeople tabCont">
					<div class="wrapnumber">
						<ul class="listName caSlide">
							<!--<li class="item"><a href="">All</a></li>
							<li class="item"><a href="">A</a></li>
							<li class="item"><a href="">b</a></li>
							<li class="item"><a href="">c</a></li>
							<li class="item"><a href="">d</a></li>
							<li class="item"><a href="">e</a></li>
							<li class="item"><a href="">f</a></li>
							<li class="item"><a href="">g</a></li>
							<li class="item"><a href="">h</a></li>
							<li class="item"><a href="">i</a></li>
							<li class="item"><a href="">j</a></li>
							<li class="item"><a href="">k</a></li>
							<li class="item"><a href="">l</a></li>
							<li class="item"><a href="">m</a></li>
							<li class="item"><a href="">n</a></li>
							<li class="item"><a href="">o</a></li>
							<li class="item"><a href="">p</a></li>
							<li class="item"><a href="">q</a></li>
							<li class="item"><a href="">r</a></li>
							<li class="item"><a href="">s</a></li>
							<li class="item"><a href="">t</a></li>
							<li class="item"><a href="">u</a></li>
							<li class="item"><a href="">v</a></li>
							<li class="item"><a href="">w</a></li>
							<li class="item"><a href="">x</a></li>
							<li class="item"><a href="">y</a></li>
							<li class="item"><a href="">z</a></li>-->
						</ul>
					</div>
					<div class="newAll srcCont clear" id="catlst">					
					@if(count($user_friends)>0)
						@foreach($user_friends as $frndreq)
						<div class="peopleBox">
							<div class="listWrap">
								<div class="topCon clear">
									<div class="conThumb">
										<figure>
											<?php $profImag = asset('assets/frontend/images/profile.jpg');
											if($frndreq->profile_image !='' && file_exists('assets/upload/profile_image/'.$frndreq->profile_image)==1)
											{
												$profImag =asset('assets/upload/profile_image/'.$frndreq->profile_image);
											}									
											?>
											<a href="{{url('profile/'.$frndreq->id)}}" target="_blank" class="btnText"><img src="{!! $profImag !!}" width="100" height="100" alt=""></a>
										</figure>
										<span class="ageCnt"><i class="fa {{$frndreq->gender=='M'?'fa-mars':'fa-venus'}}" aria-hidden="true"></i>{{$frndreq->age}} yrs</span>
									</div>
								</div>
								<div class="peopleDesc">
									<div class="vMiddle">
										<div class="MiddleText">
											<?php
											//$fid = ($frndreq->from_user_id == Auth::User()->id)? $frndreq->to_user_id:$frndreq->from_user_id;
											$fid = $frndreq->id;
											?>
											<a href="javascript:;" class="CStar {{in_array($fid,$fav_frnds)?'adStar':''}}" fid="{{ $fid}}"  id="cat_{{$fid}}">
												<i class="fa fa-star" aria-hidden="true"></i>
											</a>
											<h4><a href="{{url('profile/'.$frndreq->id)}}" target="_blank">{{$frndreq->nickname}}</a><span class="avlTxt online"><samp>available</samp></span></h4>
											
											<p><?php $we = $frndreq->work_experiences()->orderBy('id','desc')->first();?>
											@if(!empty($we))
											{{$we->title}} - {{$we->company}}
											@endif
											</p>
											<p><?php $edu = $frndreq->educations()->orderBy('id','desc')->first();?>
											@if(!empty($edu))
											{{$edu->institution}} - {{$edu->major}}
											@endif
											</p>
											<p><?php $live = $frndreq->user_Addresses()->where('is_default','Y')->first();?>
											@if(!empty($live))
												Live In - {{$live->address}}
											@endif
											</p>
										</div>
									</div>
									<div class="chatRight">
										<a href=""><i class="fa fa-commenting-o" aria-hidden="true"></i> Message</a>
										<a href=""><i class="fa fa-phone" aria-hidden="true"></i> Call</a>
									</div>
								</div>
								<div class="shortDesc">
									<div class="descText">
										<div class="descTop clear">
											<figure><img src="{!! $profImag !!}" alt=""></figure>
											<h4><a href="">{{$frndreq->nickname}}</a><span><i class="fa fa-mars" aria-hidden="true"></i>{{$frndreq->age}} yrs</span></h4>
											<p>
											@if(!empty($we))
											{{$we->title}} - {{$we->company}}
											@endif
											</p>
											<p>
											@if(!empty($edu))
											{{$edu->institution}} - {{$edu->major}}
											@endif
											</p>
											<p>
											@if(!empty($live))
												Live In - {{$live->address}}
											@endif
											</p>
										</div>
										<div class="deskBtm clear">
											<a href=""><i class="fa fa-commenting-o" aria-hidden="true"></i> Message</a><a href=""><i class="fa fa-phone" aria-hidden="true"></i> Call</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach						
					@else
						<h3>No Request Found..</h3>
					@endif
					</div>
					<!--<div class="pagination">
						<ul>
							<li class="prev"><a href="">Prev</a></li>
							<li class="current"><span>1</span></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
							<li><span>...</span></li>
							<li><a href="">6</a></li>
							<li class="next"><a href="">Next</a></li>
						</ul>
					</div>-->
				</div>
				<div class="allpeople tabCont">
					<div class="wrapnumber">
						<ul class="listName caSlide">
							<!--<li class="item"><a href="">All</a></li>
							<li class="item"><a href="">A</a></li>
							<li class="item"><a href="">b</a></li>
							<li class="item"><a href="">c</a></li>
							<li class="item"><a href="">d</a></li>
							<li class="item"><a href="">e</a></li>
							<li class="item"><a href="">f</a></li>
							<li class="item"><a href="">g</a></li>
							<li class="item"><a href="">h</a></li>
							<li class="item"><a href="">i</a></li>
							<li class="item"><a href="">j</a></li>
							<li class="item"><a href="">k</a></li>
							<li class="item"><a href="">l</a></li>
							<li class="item"><a href="">m</a></li>
							<li class="item"><a href="">n</a></li>
							<li class="item"><a href="">o</a></li>
							<li class="item"><a href="">p</a></li>
							<li class="item"><a href="">q</a></li>
							<li class="item"><a href="">r</a></li>
							<li class="item"><a href="">s</a></li>
							<li class="item"><a href="">t</a></li>
							<li class="item"><a href="">u</a></li>
							<li class="item"><a href="">v</a></li>
							<li class="item"><a href="">w</a></li>
							<li class="item"><a href="">x</a></li>
							<li class="item"><a href="">y</a></li>
							<li class="item"><a href="">z</a></li>-->
						</ul>
					</div>
					<div class="newAll srcCont clear"  id="favLst">	
					
					@if(count($user->fav_friends)>0)
						@foreach($user->fav_friends as $frnd)
						@php ($frndreq = $frnd->friend)
						<div class="peopleBox">
							<div class="listWrap">
								<div class="topCon clear">
									<div class="conThumb">
										<?php $profImag = asset('assets/frontend/images/profile.jpg');
										if($frndreq->profile_image !='' && file_exists('assets/upload/profile_image/'.$frndreq->profile_image)==1)
										{
											$profImag =asset('assets/upload/profile_image/'.$frndreq->profile_image);
										}									
										?>
										<figure>
											<a href="{{url('profile/'.$frndreq->id)}}" target="_blank" class="btnText"><img src="{!! $profImag !!}" width="100" height="100" alt=""></a>
										</figure>
										<span class="ageCnt"><i class="fa {{$frndreq->gender=='M'?'fa-mars':'fa-venus'}}" aria-hidden="true"></i>{{$frndreq->age}} yrs</span>
									</div>
								</div>
								<div class="peopleDesc">
									<div class="vMiddle">
										<div class="MiddleText">
											<?php
											$fid = $frndreq->id;
											?>
											<a href="javascript:;" class="CStar adStar" fid="{{ $fid}}"  id="cat_{{$fid}}">
												<i class="fa fa-star" aria-hidden="true"></i>
											</a>
											<h4><a href="{{url('profile/'.$frndreq->id)}}" target="_blank">{{$frndreq->nickname}}</a><span class="avlTxt online"><samp>available</samp></span></h4>
											
											<p><?php $we = $frndreq->work_experiences()->orderBy('id','desc')->first();?>
											@if(!empty($we))
											{{$we->title}} - {{$we->company}}
											@endif
											</p>
											<p><?php $edu = $frndreq->educations()->orderBy('id','desc')->first();?>
											@if(!empty($edu))
											{{$edu->institution}} - {{$edu->major}}
											@endif
											</p>
											<p><?php $live = $frndreq->user_Addresses()->where('is_default','Y')->first();?>
											@if(!empty($live))
												Live In - {{$live->address}}
											@endif
											</p>
										</div>
									</div>
									<div class="chatRight">
										<a href=""><i class="fa fa-commenting-o" aria-hidden="true"></i> Message</a>
										<a href=""><i class="fa fa-phone" aria-hidden="true"></i> Call</a>
									</div>
								</div>
								<div class="shortDesc">
									<div class="descText">
										<div class="descTop clear">
											<?php $profImag = asset('assets/frontend/images/profile.jpg');
											if($frndreq->profile_image !='' && file_exists('assets/upload/profile_image/'.$frndreq->profile_image)==1)
											{
												$profImag =asset('assets/upload/profile_image/'.$frndreq->profile_image);
											}									
											?>
											<figure><img src="{!! $profImag !!}" alt=""></figure>
											<h4><a href="">{{$frndreq->nickname}}</a><span><i class="fa fa-mars" aria-hidden="true"></i>{{$frndreq->age}} yrs</span></h4>
											<p>
											@if(!empty($we))
											{{$we->title}} - {{$we->company}}
											@endif
											</p>
											<p>
											@if(!empty($edu))
											{{$edu->institution}} - {{$edu->major}}
											@endif
											</p>
											<p>
											@if(!empty($live))
												Live In - {{$live->address}}
											@endif
											</p>
										</div>
										<div class="deskBtm clear">
											<a href=""><i class="fa fa-commenting-o" aria-hidden="true"></i> Message</a><a href=""><i class="fa fa-phone" aria-hidden="true"></i> Call</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach						
					@else
						<h3>No Record Found..</h3>
					@endif
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<!--middle close-->

@include('include.right_pan')
</section>
@endsection
@section('customScript')
<script>

$("form#frndsrch").on('submit', function(e){
	var frmobj = $(this);
	event.preventDefault();
	var formdata =  $( this ).serialize();
	/* console.log(formdata); */
	$.ajax({
		type:"post",
		url: "{!! url('user/search-friends') !!}" ,
		headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
		data: formdata,			
		dataType: "json",			
		success:function(res) {
			//console.log(res);
			if(res.msg==1)
			{
				if(res.tabNo==2)
				{
					$('#favLst').html(res.frndHtml);
				}else{
					$('#catlst').html(res.frndHtml);
				}
			}
		}
	});
});
$(document).on('click',".CStar", function(e){
	e.preventDefault();
	var currentobj = $(this);
	var addFid = $(this).attr('fid');
	//alert(addFid);
	$.ajax({
		type:"post",
		url: "{!! url('user/add-fav-friend') !!}" ,
		headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
		data: {'friend_id':addFid, },			
		dataType: "json",			
		success:function(res) {
			//console.log(res);
			var parentli = currentobj.parent().parent('li');
			
			if(res.msg==0){
				//parentli.removeClass('fav');
				$("#favLst").find('li#cat_'+addFid).remove();
				//$("#catlst").find('li#cat_'+addFid).removeClass('fav');
				currentobj.removeClass('adStar');
				
			}else if(res.msg==1){				
				$("#favLst").append(res.favHtml);
				currentobj.addClass('adStar');
				//$("#catlst").find('li#cat_'+addFid).addClass('fav');
			}
		}
	}); 
});

$(document).on( "click",".acceptfrnd", function( event ) {

  //event.preventDefault();
  var obj =  $( this );
  var frnd_id = obj.parent().children('input').val();
  //alert(frnd_id);
	 $('.sndmsg').html('');
	$.ajax({
		type:"post",
		url: "{!! url('user/accept-friend-request') !!}" ,
		headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
		data: {'frnd_id':frnd_id},			
		//dataType: "json",			
		success:function(res) {
			/* console.log(res); */
			if(res==0){
				obj.find('span').text('Accepted');
				obj.css('background-color', '#f39c11');
			}
		}
	});/* */
});
$(document).on( "click",".delfrnd", function( event ) {

  //event.preventDefault();
  var obj =  $( this );
  var frnd_id = obj.parent().children('input').val();
  //alert(frnd_id);
    if (confirm("Are you delete?") == true) {        
		$.ajax({
			type:"post",
			url: "{!! url('user/del-friend-request') !!}" ,
			headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
			data: {'frnd_id':frnd_id},			
			//dataType: "json",			
			success:function(res) {
				/* console.log(res); */
				if(res==0){
					obj.parent().parent().parent().parent('.peopleBox').remove();
				}
			}
		});		
    }
	
});

$('.tabCont').hide(0);
$('.tabCont:first-child').show(0);
$('.tabmenu ul li:first-child').addClass('active');
$('#tab1').click(function() {
	$('#tab_slc').val(1);
	$('.tabCont').hide(0);
	$('.tabCont:first-child').show(0);
	$('.tabmenu ul').find('li').removeClass('active');
	$(this).parent('li').addClass('active');
});
$('#tab2').click(function() {	
	$('#tab_slc').val(2);
	$('.tabCont').hide(0);
	$('.tabCont:nth-child(2)').show(0);
	$('.tabmenu ul').find('li').removeClass('active');
	$(this).parent('li').addClass('active');
});
$('#tab3').click(function() {
	$('.tabCont').hide(0);
	$('.tabCont:nth-child(3)').show(0);
	$('.tabmenu ul').find('li').removeClass('active');
	$(this).parent('li').addClass('active');
});

</script>
@endsection