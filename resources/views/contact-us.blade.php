@extends('layouts.app')

@section('pageTitle', 'Welcome to ')

@section('seo_sec')
@if(!empty($cmsdata))
<title> {{$cmsdata->seo_title}}</title>
<meta name="description" content="{{$cmsdata->seo_description}}">
<meta name="keywords" content="{{$cmsdata->seo_keywords}}">
@endif
@endsection
@section('customStyle')

@endsection

@section('content')


    <!--top section open-->
    <section class="test-top">
    	<div class="wrapper clear">
        	<h2>Contact</h2>
        </div>
    </section>
    <!--top section close-->
    
    <!--main content open-->
    <section class="testpage blogpage">
    	<div class="wrapper clear">
            <div class="contactMap" >
                <div id="map_cnv" style="height:300px;"></div>
            </div>
            <div class="contactBottom clear">
                <div class="conLeft">
                    <div class="conWrap">
                        <h2>Contact Us</h2>
                        <div class="conForm">
                            <p class="formTag">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravida odio, sit amet suscipit risus ultrices eu. Fusce viverra neque at purus laoreet consequat.</p>
							@if ($errors->any())
								<div class="warnings">
									<div class="error_message">
										@foreach ($errors->all() as $error)
											<p>{!! $error !!}</p>
										@endforeach
									</div>
								</div>
							@elseif(session('success'))
								<div class="success_message">
									<p>{!! session('success') !!}</p>
								</div>
								<?php session()->forget('success');?>
							@endif
                            <form name="confrm" method="post" action="{{url('contact-us')}}">
							   {{ csrf_field() }}
                                <div class="formLine">
                                    <label>
                                        <span class="label">Your name</span>
                                        <div class="textfld">
                                            <input type="text" name="name" value="" />
                                        </div>
                                    </label>
                                </div>
                                <div class="formLine">
                                    <label>
                                        <span class="label">Your email</span>
                                        <div class="textfld">
                                            <input type="email" name="email" value="" />
                                        </div>
                                    </label>
                                </div>
                                <div class="formLine">
                                    <label>
                                        <span class="label">Your Message</span>
                                        <div class="textfld">
                                            <textarea name="comments"></textarea>
                                        </div>
                                    </label>
                                </div>
                                <div class="textfld submitBtn"><input type="submit" value="Send" class="colorbtn"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="conRight">
                    <div class="conWrap">
                        <h2>Contact Us</h2>
                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravida odio, sit amet suscipit risus ultrices eu. Fusce viverra neque at purus laoreet consequat. Vivamus vulputate posuere nisl quis consequat. Donec congue commodo mi, sed commodo velit fringilla ac. </p>
                        <ul> -->
                            <li>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>Address :
                                <p>{{$setting->contact_address}}</p>
                            </li>
                            <li>
                                <i class="fa fa-phone" aria-hidden="true"></i>Phone Number :
                                <p>{{$setting->contact_phone}}</p>
                            </li>
                            <li>
                                <i class="fa fa-envelope" aria-hidden="true"></i>Email Id :
                                <p>{{$setting->contact_email}}</p>
                            </li>
                            <!-- <li>
                                <i class="fa fa-share-square-o" aria-hidden="true"></i>Social Links :
                                <p>
                                    <a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                                    <a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                                    <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                                    <a href=""><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
                                    <a href=""><i class="fa fa-rss-square" aria-hidden="true"></i></a>
                                </p>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--main content close-->
    

@endsection

@section('customScript')
<script type="text/javascript">
function initMap() {
	var map = new google.maps.Map(document.getElementById('map_cnv'), {
	  center: { lat: {{$setting->lat}}, lng: {{$setting->lng}} },
	  zoom: 8
	});
	var myLatlng = {lat: {{$setting->lat}}, lng: {{$setting->lng}} };
	var marker = new google.maps.Marker({
	  position: myLatlng,
	  map: map,
	});
}
</script>	
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6g0i6uEsWrZ3mRuSddpe_-pObwo_BGbU&libraries=places&callback=initMap"></script>
@endsection