@extends('layouts.app')

@section('pageTitle', 'Welcome to ')

@section('customStyle')

@endsection

@section('content')
       

	<!--about top open-->
    <section class="about-top">
    	<div class="wrapper">
        	<h2> {{ $page->title }}</h2>
            {!! $page->page_content !!}
        </div>
    </section>
    <!--about top close-->
    @if($page->slug=='about-us')
    <!--blue section open-->
    <section class="ourteam">
    	<div class="wrapper">
        	<h2>Out Team</h2>
            <ul>
            	<li>
                	<figure><img src="{!! asset('assets/frontend/images/demo.jpg') !!}" width="250" height="250" alt=""></figure>
                    <h3>John Smith</h3>
                    <span class="team-post">Director</span>
                    <div class="social-link"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></div>
                </li>
                <li>
                	<figure><img src="{!! asset('assets/frontend/images/demo.jpg') !!}" width="250" height="250" alt=""></figure>
                    <h3>John Smith</h3>
                    <span class="team-post">Director</span>
                    <div class="social-link"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></div>
                </li>
                <li>
                	<figure><img src="{!! asset('assets/frontend/images/demo.jpg') !!}" width="250" height="250" alt=""></figure>
                    <h3>John Smith</h3>
                    <span class="team-post">Director</span>
                    <div class="social-link"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></div>
                </li>
            </ul>
        </div>
    </section>
    <!--blue section close-->
    @endif
	@if($page->slug=='stories')
     <!--story bottom open-->
    <section class="story-bottom">
    	<div class="wrapper">
        	<iframe width="700" height="350" src="https://www.youtube.com/embed/9gTw2EDkaDQ" frameborder="0" allowfullscreen=""></iframe>
        </div>
    </section>
    <!--story bottom close top open-->
    @endif
    <!--track result open-->
    <!--track result close-->
    
    <!--testimonial open-->
    <section class="testimonial">
    	<div class="wrapper">
        	<h2>Testimonial</h2>
        	<div class="testimonialslider">
            	<div class="testislide">
                	<figure><img src="{!! asset('assets/frontend/images/testi-thumb.png') !!}" width="136" height="136" alt=""></figure>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                    <h4>- Director, John Smith</h4>
                    <a href="#" class="colorbtn bluebtn">View all ></a>
                </div>
                <div class="testislide">
                	<figure><img src="{!! asset('assets/frontend/images/testi-thumb.png') !!}" width="136" height="136" alt=""></figure>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                    <h4>- Director, John Smith</h4>
                    <a href="#" class="colorbtn bluebtn">View all ></a>
                </div>
            </div>
        </div>
    </section>
    <!--testimonial close-->
    
		 
@endsection

@section('customScript')

@endsection

