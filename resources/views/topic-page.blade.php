@extends('layouts.app')

@section('pageTitle', 'Welcome to ')

@section('customStyle')

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

@endsection

@section('content')

	<!--body open-->
    <section class="mainbody clear">
    	<!--left pan open-->
		@include('include.left_pan')    	
        <!--left pan close-->
		<?php
		$favrit = (isset($user) && !empty($user))? $user->user_favorites()->where('tropic_id',$tropic->id)->first() : [];
		?>
        <!--middle open-->
         <div class="middlecol listQuestion singleQs">
            <div class="topicNext clear">
				<?php
				if($tropic->image!='')
						$timg = asset('assets/upload/topic/'.$tropic->image);
				else	$timg = asset('assets/frontend').'/images/no-image.jpg';
				?>
            	<figure><img src="{!! $timg !!}" alt=""></figure>
                <div class="nextTop">
                    <h2>{{$tropic->title}}</h2><a href="javascript:void(0);" class="smF addF"><i class="fa fa-star" aria-hidden="true" style="color:{{!empty($favrit)?'#f3c411':'#ccc'}}"></i><input type="hidden" value="{{$tropic->id}}"/><span>Favourite</span></a>
                    <span class="postNo"><samp>{!!$tropic->tropic_questions->count()!!}</samp> posts</span>
                </div>
            </div>
            <div class="contentmiddle qsPage nextqs">
            	<div class="middlerow qsTopic clear">
                	<div class="nextBg clear">
					@if(!empty($pinned_questions))
					@foreach($pinned_questions as $tq)		
						<div class="middleleft topNo">
							<h3 class="descqs"><a href="{{url('user/question-details/'.$tq->question_id)}}">{{$tq->title}}</a></h3>
							<!--<div class="mobileCount">
								<ul>
									<li><i class="fa fa-eye" aria-hidden="true"></i> <span>9</span> views</li>
									<li><i class="fa fa-comments-o" aria-hidden="true"></i> <span>9</span> answers</li>
									<li><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <span>9</span> votes</li>
								</ul>
							</div>-->
						</div>
					@endforeach
					@endif	
                    </div>
                    <a href="javascript:void(0);" id="newThrd" class="bluebtn newThr"><i class="fa fa-pencil-square-o"></i>Create new thread</a>
                </div>
                <div class="topicListBot">
				<?php $forwards_qsn = array();?>
				@if(count($tropic->tropic_questions)>0)
					@foreach($tropic->tropic_questions as $tqsn)
					<?php $one_quest = $tqsn->question;?>
					<div class="middlerow clear">
						<div class="middleleft">
							<div class="usertop">
								<div class="hovernxt">
									<figure><img src="{!!asset('assets')!!}{{$one_quest->questionUser->profile_image!=''?'/upload/profile_image/'.$one_quest->questionUser->profile_image:'/frontend/images/userthumb.png'}}" width="47" height="47" alt=""></figure>
								</div>
								<span>
								<?php $usrid = (isset($one_quest->question_forward) && in_array($one_quest->id,$forwards_qsn))?$one_quest->question_forward->from_user:$one_quest->user_id;?>
								<a href="{{url('profile/'.$usrid)}}">{{get_user_name($usrid)}} </a>
								</span>
								<samp>{{in_array($one_quest->id,$forwards_qsn)?'forward a Question':'asked a Question'}}</samp>
								<div class="blockArrow">
									<ul>
										<li>
											<a href="javascript:void(0);" class="qsnfollowbtn {{in_array($one_quest->id,$userfollows)?'factive':''}}">
												<img src="{!!asset('assets/frontend')!!}/images/a3.png" alt=""/>
												<span style="{{in_array($one_quest->id,$userfollows)?'color:#3498db':''}}">{{in_array($one_quest->id,$userfollows)?'Unfollow':'follow'}}</span>
											</a>
											<input type="hidden" value="{{$one_quest->id}}"/>
										</li>
										<li><a href="#"><img src="{!!asset('assets/frontend')!!}/images/a1.png" alt=""><span>save link</span></a></li>
										<li><a href="#"><img src="{!!asset('assets/frontend')!!}/images/a2.png" alt=""><span>report</span></a></li>
									</ul>
								</div>
								
							</div>
							<h3 class="descqs"><a href="{{url('user/question-details/'.$one_quest->id)}}"><i class="fa fa-file-text-o" aria-hidden="true"></i>{{$one_quest->title}}</a></h3>
							<p class="des-text">{!!nl2br(substr($one_quest->content,0,650))!!}</p>
							<div class="mobileCount">
								<div class="topsmall">
									<span>{{time_elapsed_string(strtotime($one_quest->created_at))}}</span>
									
									<span>About &nbsp;
									
										@if(!empty($one_quest->question_tropics))
											@foreach($one_quest->question_tropics as $val)
												<a href="{{url('topic/'.$val->tropic->id)}}" class="qpop">{{$val->tropic->title}}</a> , 
											@endforeach
										@endif
									</span>
									
								</div>
								<!--<div class="noDesc">
									<div class="retail">
										<span class="noLext">To</span>
										<ul>
										@if(!empty($one_quest->question_tropics))
											@foreach($one_quest->question_tropics as $val)
												<li><a href="{{url('topic/'.$val->tropic->id)}}" class="qpop">{{$val->tropic->title}}</a></li>
											@endforeach
										@endif
										</ul>
										<a href="javascript:void(0);" class="moreTo">View All</a>
									</div>
								</div>-->
								<ul>
									<li><i class="fa fa-eye" aria-hidden="true"></i> <span>{!!count($one_quest->qsn_views)!!}</span> views</li>
									<li><i class="fa fa-comments-o" aria-hidden="true"></i> <span>{!!count($one_quest->answers)!!}</span> answers</li>
									<li><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <span>
									<?php $cnt=0;
									if(!empty($one_quest->answers))
									{
										foreach($one_quest->answers as $val)
										{
											$cnt+= count($val->upvotes);
										}
									}
									echo $cnt;
									?>
									</span> votes</li>
								</ul>
							</div>
							<a class="readans" href="javascript:void(0);">Read {{count($one_quest->answers)}} answers</a>
							<div class="viewqsbox">
							@if(!empty($one_quest->answers))						
								@foreach($one_quest->answers as $answer)
								<div class="qsbox clear">
									<figure><img src="{!!asset('assets')!!}{{$answer->answerUser->profile_image!=''?'/upload/profile_image/'.$answer->answerUser->profile_image:'/frontend/images/userthumb.png'}}" alt=""></figure>
									<h4><a href="">{{$answer->answerUser->nickname}}</a></h4>
									<p>{!!nl2br(substr($answer->content,0,200))!!} <a href="#">View More</a></p>
									<span class="pstTime">{{time_elapsed_string(strtotime($answer->created_at))}}</span>
								</div>
								@endforeach
							@endif
							</div>
							<ul class="question-tags">
								<li><a href="{{url('user/question-details/'.$one_quest->id)}}" class="bluebg">Answer</a></li>
								<li><a href="{{url('user/search-people')}}" class="pinkbg">Request</a></li>
							</ul>
						</div>
					</div>
					@endforeach
				@endif	
                </div>
            </div>
        </div>   
       
		<!--middle close-->
        @if(Auth::check())
        <!--right pan open-->
		@endif
		@include('include.right_pan')
		<!--right pan close-->
	</section>
    <!--body close-->

@endsection



@section('customScript')

<script>
 
$( function() {
	
});
function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}
$(document).on('click',".addF", function(e){
	e.preventDefault();
	var currentobj = $(this);
	var addFid = $(this).find('input').val();
	//alert(addFid);
	$.ajax({
		type:"post",
		url: "{!! url('user/add-favorite') !!}" ,
		headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
		data: {'tropic_id':addFid, },			
		//dataType: "json",			
		success:function(res) {
			//console.log(res);
			var icolor = rgb2hex(currentobj.children('i').css('color'));
			//alert(icolor);
			if(icolor=="#f3c411"){
				currentobj.children('i').css('color','#ccc');
			}else	
				currentobj.children('i').css('color','#f3c411');
		}
	}); 
});

$('a.qsnfollowbtn').click(function(){
	var fobj = $(this);
	var qsnfollow = fobj.next('input').val();
	/* alert(qsnfollow); */
	$.ajax({
		type:"post",
		url: "{{url('user/qsn-follow-unfollow')}}" ,
		headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
		data: {'qsnfollow':qsnfollow},			
		dataType: "json",			
		success:function(res) {
			/* console.log(res); */
			if(res==0){
				fobj.children('span').text('Unfollow').css('color','#3498db');
				fobj.addClass('factive');
			}
			else if(res==2){
				fobj.children('span').text('Follow').css('color','#6c6c6c');
				fobj.removeClass('factive');
			}
		}
	});	
});

$('#newThrd').click(function(){
		$("#askbtn").fancybox().trigger('click');
});

</script>

@endsection