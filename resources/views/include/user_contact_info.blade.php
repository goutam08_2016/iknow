@if(count($user->contact_ifno)>0)
	@foreach($user->contact_ifno as $val)
	<form class="contactinfofrm">
		<div class="linkAddress viwSmlAdd">
			@if(Auth::check() && $user->id == Auth::user()->id) 
			<a href="javascript:void(0)" class="cnEdt rnvinfo"><img src="{!! asset('assets/frontend') !!}/images/cancel.png" alt=""></a>
			<a href="javascript:void(0)" class="edtEml"><img src="{!! asset('assets/frontend') !!}/images/edit.png" alt=""></a>
			@endif
			<ul>
				<li>
					@if($val->info1)
					<div class="textE clear"><img src="{!! asset('assets/frontend') !!}/images/gmail.png" alt=""> {{$val->info1}}</div>
					@endif
					<div class="contactEdit clear">
						<img src="{!! asset('assets/frontend') !!}/images/gmail.png" alt=""><strong>*</strong><input name="info1" type="email" value="{{$val->info1}}"/>
					</div>
				</li>
				<li>
					@if($val->info2)
					<div class="textE clear"><img src="{!! asset('assets/frontend') !!}/images/message.png" alt=""> {{$val->info2}}</div>
					@endif
					<div class="contactEdit clear">
						<img src="{!! asset('assets/frontend') !!}/images/message.png" alt=""><input name="info2" type="text" value="{{$val->info2}}"/>
					</div>
				</li>
				<li>
					@if($val->info3)
					<div class="textE clear"><img src="{!! asset('assets/frontend') !!}/images/fb1.png" alt=""> {{$val->info3}}</div>
					@endif
					<div class="contactEdit clear">
						<img src="{!! asset('assets/frontend') !!}/images/fb1.png" alt=""><input name="info3" type="text" value="{{$val->info3}}"/>
					</div>
				</li>
				<li>
					@if($val->info4)
					<div class="textE clear"><img src="{!! asset('assets/frontend') !!}/images/skype.png" alt="">{{$val->info4}}</div>
					@endif
					<div class="contactEdit clear">
						<img src="{!! asset('assets/frontend') !!}/images/skype.png" alt=""><input name="info4" type="text" value="{{$val->info4}}">
					</div>
				</li>
			</ul>
			<div class="textfld btnRow">
				<a href="javascript:void(0)" class="cnclBtn">Cancel</a>
				<input type="submit" value="Save" class="bluebtn">
			</div>									
				<input type="hidden" name="info_id" value="{{$val->id}}"/>
		</div>
	</form>
	@endforeach
@endif