@if(count($users_data)>0)
	@foreach($users_data as $user)
	<div class="peopleBox">
		<div class="listWrap">
			<div class="topCon clear">
				<div class="conThumb">
					<?php $profImag = asset('assets/frontend/images/profile.jpg');
					if($user->profile_image !='' && file_exists('assets/upload/profile_image/'.$user->profile_image)==1)
					{
						$profImag =asset('assets/upload/profile_image/'.$user->profile_image);
					}									
					?>
					<figure>
						<a href="javascript:void(0)" class="btnText"><img src="{!! $profImag !!}" width="100" height="100" alt=""></a>
					</figure>
					<span class="ageCnt"><i class="fa {{$user->gender=='M'?'fa-mars':'fa-venus'}}" aria-hidden="true"></i>{{$user->age}} yrs</span>
				</div>
			</div>
				
			<div class="peopleDesc">
				<div class="chatRight">
					<label>
						<input type="checkbox" name="{{$user->email}}" value="{{$user->id}}" />
						
						<span class="checkbox"><img src="{!! asset('assets/frontend') !!}/images/clr.png" alt=""></span>
					</label>
				</div>
				<div class="vMiddle">
					<div class="MiddleText">
						<h4><a href="{{url('profile/'.$user->id)}}" target="_blank">{{$user->nickname}}</a><span class="avlTxt online"><samp>available</samp></span></h4>
						
						<p><?php $we = $user->work_experiences()->orderBy('id','desc')->first();?>
						@if(!empty($we))
						{{$we->title}} - {{$we->company}}
						@endif
						</p>
						<p><?php $edu = $user->educations()->orderBy('id','desc')->first();?>
						@if(!empty($edu))
						{{$edu->institution}} - {{$edu->major}}
						@endif
						</p>
						<p><?php $live = $user->user_Addresses()->where('is_default','Y')->first();?>
						@if(!empty($live))
							Live In - {{$live->address}}
						@endif
						</p>
						<!-- <p>Vietin Bank - Card Center</p>
						<p>Foreign Trade University - Advanced Finance</p>
						<p>Live In - Lorem Ipsum</p> -->
					</div>
				</div>
				
			</div>
		</div>
	</div>
	@endforeach
@endif