<!--top section open-->
<section class="test-top">
	<div class="wrapper clear">
		<h2>{{$title}} </h2>
		@if(Auth::user()->user_type==2)
		<span id="catenavbtn">
			<i class="fa fa-bars"></i>
		</span>
		<div class="cate-menu">
			<ul>
				<li class="{!! Request::segment(2) == 'tests'?'active':'' !!}">
					<a href="{{url('user/tests')}}">
						<img src="{!! asset('assets/frontend/images/exam-small.png') !!}" alt="">
						<span>tests</span>
					</a>
				</li>
				<li class="{!! Request::segment(2) == 'question-bank'?'active':'' !!}">
					<a href="{{url('user/question-bank')}}">
						<img src="{!! asset('assets/frontend/images/discuss-issue.png') !!}" alt="">
						<span>question bank</span>
					</a>
				</li>
				<li class="{!! Request::segment(2) == 'categories'?'active':'' !!}">
					<a href="{{url('user/categories')}}">
						<img src="{!! asset('assets/frontend/images/different-squares.png') !!}" alt="">
						<span>categories</span>
					</a>
				</li>
				<li class="{!! Request::segment(2) == 'groups'?'active':'' !!}">
					<a href="{{url('user/groups')}}">
						<img src="{!! asset('assets/frontend/images/command-small.png') !!}" alt="">
						<span>groups</span>
					</a>
				</li>
				<li>
					<a href="#">
						<img src="{!! asset('assets/frontend/images/broken-link-small.png') !!}" alt="">
						<span>links</span>
					</a>
				</li>
			</ul>
		@endif
		</div>
	</div>
</section>
<!--top section close-->