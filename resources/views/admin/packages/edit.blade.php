<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */
$resource = 'package';
$resource_pl = str_plural($resource);
$page_title = ucfirst(str_replace('_', ' ', $resource));
$page_title_pl = ucfirst(str_replace('_', ' ', $resource_pl));
// Localization data...
$locale = app()->getLocale();

// Resolute the data...
$data = ${$resource_pl};
//dd($data);

?>

@extends('admin.layouts.app')

@section('pageTitle', 'Edit ' . $page_title)

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {!! $page_title !!}
            <small>Edit {!! $data->firstname !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{!! url("admin/$resource") !!}"><i class="fa  fa-user"></i> {!! $page_title_pl !!}</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit {{ $page_title }}</h3>
                    </div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <p>{!! $error !!}</p>
                            @endforeach
                        </div>
                    @endif
                    @if(session('success'))
                    <div class="alert alert-success">
                        {!! session('success') !!}
                    </div>
                    @endif
                    <form method="POST" action="{!! admin_url($resource . '/' . ${$resource_pl}->id) !!}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PATCH"> {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                    <label for="plan" class="col-sm-2 control-label">Plan </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="plan" name="plan" placeholder="title" value="{!! $data->plan !!}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="duration" class="col-sm-2 control-label">Duration </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="duration" name="duration" placeholder="month" value="{!! $data->duration !!}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="price" class="col-sm-2 control-label">Price </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="price" name="price" placeholder="price" value="{!! $data->price!!}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="details" class="col-sm-2 control-label">Package details </label>
                                    <div class="col-sm-6">
                                        <textarea style="height: 200px; resize: none;" type="text" class="form-control" id="details" name="details" placeholder="content">{!! $data->details !!}</textarea>
                                    </div>
                                </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a class="btn btn-default" href="{!! url('admin/$resource') !!}">
                                Back</a>
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </div><!-- /.box-footer -->
                        </form>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection

@section('customScript')

@endsection