<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */
$resource = 'cms-page';
$resource_pl = str_plural("cms");
$page_title = ucfirst(str_replace('_', ' ', $resource));
$page_title_pl = ucfirst(str_replace('_', ' ', $resource_pl));
$locale = app()->getLocale();

?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title_pl . ' list')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $page_title }}
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">{{ $page_title }}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        {{--<h3 class="box-title">All {{ $page_title }} list</h3>--}}
                        <span class="col-xs-3">
                            <select class="form-control" id="language-set">
                                @foreach($languages as $language)
                                    <option value="{!! $language->locale !!}" {{ $locale == $language->locale ? 'selected' : '' }}>{!! $language->name !!}</option>
                                @endforeach
                            </select>
                        </span>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        @if(session('success'))
                            <div class="alert alert-success">
                                {!! session('success') !!}
                            </div>
                        @endif
                        <table id="list_table" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Slug</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(${$resource_pl} !== null)
                                @foreach(${$resource_pl} as $row)
                                    <tr>
                                        <td>{!! $row->title !!}</td>
                                        <td>{!! $row->slug !!}</td>
                                        <td>
                                            <a href="{!! admin_url($resource . '/' . $row->id . '/edit') !!}" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                            <form method="POST" action="{!! admin_url($resource . '/' . $row->id) !!}"
                                                  onsubmit="return confirm('Are you sure to remove {!! $row->title !!} (All) ?');">
                                                <input name="_method" type="hidden" value="DELETE">{{ csrf_field() }}
                                                <button class="btn btn-sm btn-danger td-btn" type="submit"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                            </form>

                                            <form method="POST" action="{!! admin_url($current_language->locale . '/' . $resource . '/' . $row->id) !!}"
                                                  onsubmit="return confirm('Are you sure to remove {!! $row->title !!} ({{ $current_language->name }}) ?');">
                                                <input name="_method" type="hidden" value="DELETE">{{ csrf_field() }}
                                                <button class="btn btn-sm btn-danger td-btn" type="submit"><i class="fa fa-trash" aria-hidden="true"></i> Delete ({{ $current_language->name }} only)</button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">No Car maker Found..</td>
                                </tr>
                            @endif

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                    <div class="paginationDiv">
                        {!! ${$resource_pl}->render() !!}
                    </div>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection

@section('customScript')
<script type="text/javascript">
    $(function () {
        $("#list_table").DataTable({
            "paging":   false,
            "ordering": false
        });

        $("#language-set").change(function(){
            var lang = $("#language-set").val();
            window.location.href = "{{ admin_url('') }}/" + lang + "/{{$resource}}";
        });

    });
</script>
@endsection