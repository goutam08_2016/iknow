<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */
$resource = 'cms-page';
$resource_pl = str_plural("cms");
$page_title = ucfirst(str_replace('_', ' ', $resource));
$page_title_pl = ucfirst(str_replace('_', ' ', $resource_pl));
// Localization data...
$locale = app()->getLocale();

// Resolute the data...
$data = ${$resource_pl};
//dd($data);
$escapePages = array('contact-us','pricing');
?>

@extends('admin.layouts.app')

@section('pageTitle', 'Edit ' . $page_title)

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {!! $page_title !!}
            <small>Edit {!! $data->firstname !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{!! url("admin/$resource") !!}"><i class="fa  fa-user"></i> {!! $page_title_pl !!}</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit {{ $page_title }}</h3>
                    </div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <p>{!! $error !!}</p>
                            @endforeach
                        </div>
                    @endif
                    @if(session('success'))
                    <div class="alert alert-success">
                        {!! session('success') !!}
                    </div>
                    @endif
                    <form method="POST" action="{!! admin_url($resource . '/' . ${$resource_pl}->id) !!}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PATCH"> {{ csrf_field() }}
                        <div class="box-body">
                            @foreach($languages as $language)
                                <div class="form-group">
                                    <label for="title_{{ $language->locale }}" class="col-sm-2 control-label">Title ({{ $language->name }}) {{ $language->fallback_locale === 'Y' ? '*' : '' }}</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="title_{{ $language->locale }}" name="title_{{ $language->locale }}" placeholder="Title" value="{{ $data->translate($language->locale) !== null ? $data->translate($language->locale)->title : '' }}" />
                                    </div>
                                </div>								@if(!in_array($data->translate($language->locale)->slug,$escapePages))
								<div class="form-group">
                                    <label for="page_content_{{ $language->locale }}" class="col-sm-2 control-label">Page content ({{ $language->name }}) {{ $language->fallback_locale === 'Y' ? '*' : '' }}</label>
                                    <div class="col-sm-6">
                                        <textarea style="height: 200px; resize: none;" type="text" class="form-control" id="page_content_{{ $language->locale }}" name="page_content_{{ $language->locale }}" placeholder="Content">{{ $data->translate($language->locale) !== null ? $data->translate($language->locale)->page_content : '' }}</textarea>
                                    </div>
                                </div>								@endif                                <div class="form-group">                                    <label for="seo_title_{{ $language->locale }}" class="col-sm-2 control-label">SEO title ({{ $language->name }}) {{ $language->fallback_locale === 'Y' ? '*' : '' }}</label>                                    <div class="col-sm-6">                                        <input type="text" class="form-control" id="seo_title_{{ $language->locale }}" name="seo_title_{{ $language->locale }}" placeholder="seo title" value="{{ $data->translate($language->locale) !== null ? $data->translate($language->locale)->seo_title : '' }}" />                                    </div>                                </div>																<div class="form-group">                                    <label for="seo_keywords_{{ $language->locale }}" class="col-sm-2 control-label">SEO keywords ({{ $language->name }}) {{ $language->fallback_locale === 'Y' ? '*' : '' }}</label>                                    <div class="col-sm-6">                                        <textarea style="height: 200px; resize: none;" type="text" class="form-control" id="seo_keywords_{{ $language->locale }}" name="seo_keywords_{{ $language->locale }}" placeholder="seo keywords">{{ $data->translate($language->locale) !== null ? $data->translate($language->locale)->seo_keywords : '' }}</textarea>                                    </div>                                </div>								<div class="form-group">                                    <label for="seo_description_{{ $language->locale }}" class="col-sm-2 control-label">SEO description ({{ $language->name }}) {{ $language->fallback_locale === 'Y' ? '*' : '' }}</label>                                    <div class="col-sm-6">                                        <textarea style="height: 200px; resize: none;" type="text" class="form-control" id="seo_description_{{ $language->locale }}" name="seo_description_{{ $language->locale }}" placeholder="seo description">{{ $data->translate($language->locale) !== null ? $data->translate($language->locale)->seo_description : '' }}</textarea>                                    </div>                                </div>								
                            @endforeach
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a class="btn btn-default" href="{!! url("admin/$resource") !!}">
                                Back</a>
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </div><!-- /.box-footer -->
                        </form>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection

@section('customScript')

@endsection