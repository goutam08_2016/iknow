<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */

$resource = 'adsences';
$resource_pl = str_plural($resource);
$page_title = ucfirst(str_replace('_', ' ', $resource));
$page_title_pl = ucfirst(str_replace('_', ' ', $resource_pl));
$locale = app()->getLocale();

?>

@extends('admin.layouts.app')

@section('pageTitle', $page_title)

@section('content')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {!! $page_title !!}
            <small>Add</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{!! url("admin/$resource") !!}"><i class="fa  fa-user"></i> {!! $page_title_pl !!}</a></li>
            <li class="active"> Add</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add {{ $page_title }}</h3>
                    </div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            @foreach($errors->all() as $error)
                                <p>{!! $error !!}</p>
                            @endforeach
                        </div>
                    @endif
                    @if(session('success'))
                        <div class="alert alert-success">
                            {!! session('success') !!}
                        </div>
                    @endif
                    <form method="POST" action="{!! admin_url($resource) !!}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">							
                            <div class="form-group">
                                <label for="ad_position" class="col-sm-2 control-label">Ad  position *</label>
                                <div class="col-sm-6">
									<select class="form-control" id="ad_position" name="ad_position">
										<option type="text" class="form-control" value="1">Home Ad 1</option>
										<option type="text" class="form-control" value="2">Home Ad 2</option>
									</select>
                                </div>								
                            </div>		
							
                            <div class="form-group">
                                <label for="adsense_code" class="col-sm-2 control-label">Adsence Code *</label>
                                <div class="col-sm-6">
                                    <textarea style="height: 200px; resize: none;" type="text" class="form-control" id="" name="adsense_code" placeholder="Adsense code">{!! old('adsense_code') !!}</textarea>
                                </div>								
                            </div>
                            <div class="form-group">
                                <label for="status" class="col-sm-2 control-label">Adsence Code *</label>
                                <div class="col-sm-6">
									<label><input type="radio" value="Y" name="status" checked />Active</label>
									<label><input type="radio" value="N" name="status" />Inactive</label>
                                </div>								
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a class="btn btn-default" href="{!! url("admin/$resource") !!}">
                                Back</a>
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </div><!-- /.box-footer -->
                    </form>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection

@section('customScript')

<script type="text/javascript">

    $(function () {
		$('#ad_position').select2({
            placeholder: "Select Ad Position"
            //allowClear: true
        });
    });

</script>

@endsection