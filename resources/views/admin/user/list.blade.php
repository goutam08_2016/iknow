<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 */
$resource = 'user';
$resource_pl = str_plural($resource);
$page_title = ucfirst($resource_pl);
?>

@extends('admin.layouts.app')

@section('pageTitle', 'Dashboard')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $page_title }}
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">{{ $page_title }}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All {{ $resource }} list</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        @if(session('success'))
                            <div class="alert alert-success">
                                {!! session('success') !!}
                            </div>
                        @endif
                        <table id="list_table" class="table table-bordered table-striped">
                            <thead>
                            <tr>								<th><input type="checkbox"  id="bulkDelete"  /> <button id="deleteTriger">Bulk Delete</button></th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Created On</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(${$resource_pl} !== null)
                                @foreach($users as $ky=>$row)                                    <tr>																								<td><input type="checkbox"  class="deleteRow" value="{{$row->id}}"  /> #{!!$ky+1!!} </td>
                                        <td>{!! $row->first_name . ' ' . $row->last_name !!}</td>
                                        <td>{!! $row->email !!}</td>
                                        <td>{!! date('d-m-Y',strtotime($row->created_at)) !!}</td>
                                        <?php
                                        /*<td>
                                            <select  class="status">
                                                <option data-id="{{ $row->id }}"  class="bg-green-active color-palette" value="Y" {{ $row->status == 'Y'?'selected':''  }}>Active</option>
                                                <option data-id="{{ $row->id }}" class="bg-red-active color-palette" value="N" {{ $row->status == 'N'?'selected':''  }}>Block</option>
                                                <option  data-id="{{ $row->id }}" class="bg-orange-active color-palette" value="E" {{ $row->status == 'E'?'selected':''  }}>Pending</option>
                                            </select>
                                        </td> */ ?>
                                        <td>
                                            <a href="{!! admin_url('user/' . $row->id . '/edit') !!}" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>                                            <form method="POST" action="{!! admin_url('user/block') !!}"                                                  onsubmit="return confirm('Are you sure to {{$row->status=='Y'?'block':'unblock'}} {!! $row->first_name !!}?');">                                                <input name="uid" type="hidden" value="{{$row->id}}">{{ csrf_field() }}                                                <button class="btn btn-sm btn-{{$row->status=='Y'?'success':'danger'}} td-btn" type="submit"><i class="fa fa-ban" aria-hidden="true"></i> {{$row->status=='Y'?'Unblocked':'Blocked'}}</button>                                            </form>
                                            <form method="POST" action="{!! admin_url('user/' . $row->id) !!}"
                                                  onsubmit="return confirm('Are you sure to remove {!! $row->first_name !!}?');">
                                                <input name="_method" type="hidden" value="DELETE">{{ csrf_field() }}
                                                <button class="btn btn-sm btn-danger td-btn" type="submit"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">No user Found..</td>
                                </tr>
                            @endif

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                    <div class="paginationDiv">
                        {!! ${$resource_pl}->render() !!}
                    </div>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection

@section('customScript')
<script type="text/javascript">
    $(function () {
        $("#list_table").DataTable({
            "paging":   false,
            "ordering": false
        });		$(document).on('click',"#bulkDelete",function() { 			var status = this.checked;			$(".deleteRow").each( function() {				$(this).prop("checked",status);			});		});		 		$('#deleteTriger').on("click", function(event){ 			if( $('.deleteRow:checked').length > 0 ){  				var ids = [];				$('.deleteRow').each(function(){					if($(this).is(':checked')) { 						ids.push($(this).val());					}				});				var ids_string = ids.toString(); 				$.ajax({					type: "POST",					headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},					url: "{{url('admin/user-delete-all')}}",					data: {data_ids:ids_string},					success: function(result) {												location.reload();					},					async:false				});			}		});
    });
</script>
@endsection