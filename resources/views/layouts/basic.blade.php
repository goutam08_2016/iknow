<!doctype html>

<html>

<head>

<?php $settings = App()->settings;

$escapePages = array('contact-us','pricing');

$currpage=Request::segment(1);

?>

<head>

<meta charset="utf-8">

<meta name="viewport" content="width=device-width">

<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=1">





<meta name="csrf-token" content="{{ csrf_token() }}">

@if(!in_array($currpage,$escapePages))

<title> {{$settings->seo_title}} | {{$settings->site_slogan}}</title>

<meta name="description" content="{{$settings->seo_description}}">

<meta name="keywords" content="{{$settings->seo_keywords}}">

<meta name="google-signin-client_id" content="781531976817-7e4fktuios98hhjonec4b6s75bmhu46j.apps.googleusercontent.com">

@else

	@yield('seo_sec')

@endif

<!--Start CSS-->



<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"/>



<!--<link href="assets/materialcharts/material-charts.css" rel="stylesheet" type="text/css">-->

 @yield('customStyle')

<link href="{!! asset('assets/frontend/css/style.css') !!}" rel="stylesheet" type="text/css"/>

<!--end CSS-->





    <!--Start Responsive CSS (Keep All CSS Above This)-->

    <link rel="stylesheet" type="text/css" href="{!! asset('assets/frontend/css/responsive.css') !!}" />

    <!--End Responsive CSS (Don't Keep Any CSS Below This)-->

    <script>
	
        var BASE_URL = "{{ url('') }}";
		
    </script>



</head>

<body>



<div class="mainSite">

	<div class="loginpage" style="background-image:url('{!! asset('assets/upload/site_logo/'.$settings->site_home_bg) !!}');">

    	<div class="wrapper">
			
        	<div class="languagebox">
            	<ul>
					<li class="mnu"><a href="{{url('topics')}}">Topic</a></li>
                	<li><a href="#"><img src="{!! asset('assets/frontend/images/map1.png') !!}" alt=""></a></li>

                    <li><a href="#"><img src="{!! asset('assets/frontend/images/map2.png') !!}" alt=""></a></li>

                </ul>

            </div>

        	<div class="logincontainer">

            	<div class="loginheader">

                	<a href="{{url('')}}"><img src="{!! asset('assets/upload/site_logo/'.$settings->site_logo2) !!}" width="178" height="52" alt=""></a>

                </div>

                @yield('content')

            </div>

        </div>

    </div>

</div>



<!--Start js-->



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script src="{!! asset('assets/frontend/js/placeholders.min.js') !!}"></script>

<script src="{!! asset('assets/frontend/js/custom.js') !!}" type="text/javascript"></script>
<script src="https://apis.google.com/js/platform.js?" async defer></script>



<!--end js-->



@yield('customScript')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTXWwzvV3zjPIPAda9hXWVrQhJgbReszo&libraries=places"></script>
<script type="text/javascript">

/******************************* FB Login ****************************/

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1701200256860694',
      xfbml      : true,
      version    : 'v2.8'
    });
 };

(function(d, s, id){
	 var js, fjs = d.getElementsByTagName(s)[0];
	 if (d.getElementById(id)) {return;}
	 js = d.createElement(s); js.id = id;
	 js.src = "//connect.facebook.net/en_US/sdk.js";
	 fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

/* FB.getLoginStatus(function(response) {
  if (response.status === 'connected') {
    console.log('Logged in.');
  }
  else {
    FB.login();
  }
}); */
function myFacebookLogin() {
  FB.login(function(){
	  getFBAPI();
  }, {scope: 'public_profile,email,user_friends',auth_type:'reauthenticate' /*  */ });
}
function getFBAPI() {
    FB.getLoginStatus(function(response) {
	  if (response.status === 'connected') {
		console.log(response);
		//console.log(response.authResponse.accessToken);
		FB.api('/me', function(response) {
			getUsrDta(response.id);
		});
	  }
	});
    
  }
  
function getUsrDta(userID) {
    FB.api(userID + "/?fields=id,email,name,first_name,last_name,gender,location", function(response) {
		console.log(response);
		response['social_type']='fb';
		registerUser(response);
    });
}
 
 
/******************************* G+ Login ****************************/

function gplusLogin() {
	gapi.load('auth2', function() {
		auth2 = gapi.auth2.init({
		client_id: '943316273194-a5b35729edp4rinrpinforgt2j8mr9s7.apps.googleusercontent.com',
		fetch_basic_profile: true,
		scope: 'profile'
		});

		// Sign the user in, and then retrieve their ID.
		auth2.signIn().then(function()
		{
			//console.log(auth2.currentUser.get().getId());
			var currentUser = auth2.currentUser.get();
			var userId = auth2.currentUser.get().getId();
			var profile = currentUser.getBasicProfile();
			/* console.log(profile);
			console.log('ID: ' + profile.getId());
			console.log('Image URL: ' + profile.getImageUrl()); */
			
			var gpUser ={ 
			"social_type":"gp",
			"first_name":profile.getGivenName(),
			"last_name":profile.getFamilyName(),
			"name":profile.getName(),
			"email":profile.getEmail() 
			};
			registerUser(gpUser);
		});
	});
}

/*************************************************************************/
function registerUser(udata)
{
	/* console.log(udata);	 */
	$.ajax({
		type:"post",
		url: "{!! url('social-login') !!}" ,
		headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
		data: udata,			
		//dataType: "json",			
		success:function(res) {
			//console.log(res);
			window.location.reload();
			//window.location.href="{{url('/user/signup-step')}}/"+res.in_step;
		}

	});
}

function heightcontrol() {
	var windowheight = $(window).height();
	var headerheight = $("#mainheader").innerHeight();
	var footerheight = $("#mainfooter").innerHeight();
	var headfootheight = (headerheight + footerheight);
	var middleheight = (windowheight - headfootheight);
	$(".pageMiddle").css({"min-height": middleheight });
}
$(window).load(function () {
heightcontrol();
	$(window).resize(function() {
	heightcontrol();
	});
});


//var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
//if (isChrome) alert("You are using Chrome!");
if (isSafari){
	$(document).on('submit',".validFrm",function(e){
		
		//alert('prevent submit'); 
		var frmObj = $(this);
		var flag = 0;
		frmObj.find('input[type=text],select').each(function(index,elem) {
			console.log(elem);
			if($(elem).attr("required"))
			{
				if($(elem).val()=='')
				{
					//alert($(elem).attr("name"));
					flag = 0;
					alert("Please fill out this field");
					$(elem).focus();
					return false;
				}else	flag = 1;
			}
		});
		//alert(flag);
		if(flag=='1'){
			frmObj.submit();
		}else{
			e.preventDefault();
		}
	});
}

</script>
</body>

</html>