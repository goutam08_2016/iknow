<?php 
$all_tropics = \App\AllTropic::all();

$settings = App()->settings;
$escapePages = array('contact-us','pricing');
$currpage=Request::segment(1);
?>
<!doctype html>
<html>
<head>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=1">


<meta name="csrf-token" content="{{ csrf_token() }}">
@if(!in_array($currpage,$escapePages))
<title> {{$settings->seo_title}} | {{$settings->site_slogan}}</title>
<meta name="description" content="{{$settings->seo_description}}">
<meta name="keywords" content="{{$settings->seo_keywords}}">
<meta name="google-signin-client_id" content="781531976817-7e4fktuios98hhjonec4b6s75bmhu46j.apps.googleusercontent.com">
@else
	@yield('seo_sec')
@endif
<!--Start CSS-->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
<link href="{!! asset('assets/frontend/fancybox/jquery.fancybox.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('assets/frontend/mCustomScrollbar/jquery.mCustomScrollbar.css') !!}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{!! asset('assets/frontend/bxslider/css/jquery.bxslider.css') !!}">
<link href="{!! asset('assets/frontend/tagJs/jquery.tagit.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('assets/frontend/tagJs/tagit.ui-zendesk.css') !!}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{!! asset('assets/frontend/chat/chat-style.css') !!}" />

<!--<link href="{!! asset('assets/frontend') !!}/materialcharts/material-charts.css" rel="stylesheet" type="text/css">-->
<link href="{!! asset('assets/frontend/css/style.css') !!}" rel="stylesheet" type="text/css"/>
<!--end CSS-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTXWwzvV3zjPIPAda9hXWVrQhJgbReszo&libraries=places"></script>
<script>
var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
function setMapLoc(id){
	/* alert(id); */
	var inputt = document.getElementById(id);
	var autocomplete = new google.maps.places.Autocomplete(inputt);	
}
</script>
@yield('customStyle')

    <!--Start Responsive CSS (Keep All CSS Above This)-->
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/frontend/css/responsive.css') !!}" />
    <!--End Responsive CSS (Don't Keep Any CSS Below This)-->
    <script>
        var BASE_URL = '{!! url("") !!}/';
        var _TOKEN = "{!! csrf_token() !!}";
		
    </script>

</head>
<body>

<!--askpopup open-->
@include('include.askpopup')
<!--askpopup close-->

<!--advance search popup-->
@include('include.advance_search')
<!--advance search popup close-->

<!--answer popup open-->
<div class="answerpopup" id="AnsPopup">
	<div class="answerpopup-wrap">
        <a href="#" class="popupreport"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></a>
    	<div class="popupqs">
        	<div class="questionview">
                <figure><img src="assets/images/userthumb.png" width="47" height="47" alt=""></figure>
                <div class="qstext">
                    <span class="nametxt">Chris Lockwood</span>
                    <p class="totalview">5.6k Views - Most Views Writer in The Human Race and Condition</p>
                </div>
                <div class="qsdate">
                    <span>5 days ago</span>
                    <span>about <a href="#">Startup</a></span>
                </div>
            </div>
            <p class="qsmain">Time travel: How much will it effect the world if I went back in time with an invention before it was invented?</p>
        </div>
        <div class="viewansbox">
        	<div class="ansrow">
                <div class="questionview">
                    <figure><img src="assets/images/userthumb.png" width="47" height="47" alt=""></figure>
                    <div class="qstext">
                       <span class="nametxt">Chris Lockwood</span>
                       <p class="totalview">5.6k Views - Most Views Writer in The Human Race and Condition</p>
                    </div>
                    <div class="qsdate">
                        <span>20:12</span>
                        <span>27/07/2016</span>
                    </div>
                </div>
                <p class="ansmain">These two inventions might advance knowledge. lead to new ideas and ways of thinking, but will not be seen as the -things- that they are today, the technology might be developed in a completely dif. terent way, particularly depending on when and where is is taken. </p>
                <div class="bottombox clear">
                    <ul class="leftlink">
                        <li><a href="#" class="bluebg">Upload<span>342</span></a></li>
                        <li><a href="#" class="pinkbg">Download</a></li>
                        <li><a href="javascript:void(0);" class="yellowbg newcomment">Comment<span>1</span></a></li>
                    </ul>
                    <ul class="sharelist">
                        <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-plus-square" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <div class="anscomment">
                	<div class="anscommentarea">
                    	<figure><img src="assets/images/userthumb.png" width="47" height="47" alt=""></figure>
                        <div class="commentform clear">
                        	<form name="comment" method="post" action="#">
                            	<textarea></textarea>
                                <input type="submit" value="comment">
                            </form>
                        </div>
                    </div>
                    <div class="anscommentarea">
                    	<div class="anscommentrow">
                            <figure><img src="assets/images/userthumb.png" width="47" height="47" alt=""></figure>
                            <div class="commentform clear">
                                <span class="username">Tien Pham</span>
                                <p class="usercomment">The question of time travel is fascinating</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ansrow">
                <div class="questionview">
                    <figure><img src="assets/images/userthumb.png" width="47" height="47" alt=""></figure>
                    <div class="qstext">
                       <span class="nametxt">Chris Lockwood</span>
                       <p class="totalview">5.6k Views - Most Views Writer in The Human Race and Condition</p>
                    </div>
                    <div class="qsdate">
                        <span>20:12</span>
                        <span>27/07/2016</span>
                    </div>
                </div>
                <p class="ansmain">These two inventions might advance knowledge. lead to new ideas and ways of thinking, but will not be seen as the -things- that they are today, the technology might be developed in a completely dif. terent way, particularly depending on when and where is is taken. </p>
                <div class="bottombox clear">
                    <ul class="leftlink">
                        <li><a href="#" class="bluebg">Upload<span>342</span></a></li>
                        <li><a href="#" class="pinkbg">Download</a></li>
                        <li><a href="javascript:void(0);" class="yellowbg newcomment">Comment<span>1</span></a></li>
                    </ul>
                    <ul class="sharelist">
                        <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-plus-square" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <div class="anscomment">
                	<div class="anscommentarea">
                    	<figure><img src="assets/images/userthumb.png" width="47" height="47" alt=""></figure>
                        <div class="commentform clear">
                        	<form name="comment" method="post" action="#">
                            	<textarea></textarea>
                                <input type="submit" value="comment">
                            </form>
                        </div>
                    </div>
                    <div class="anscommentarea">
                    	<div class="anscommentrow">
                            <figure><img src="assets/images/userthumb.png" width="47" height="47" alt=""></figure>
                            <div class="commentform clear">
                                <span class="username">Tien Pham</span>
                                <p class="usercomment">The question of time travel is fascinating</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ansrow">
                <div class="questionview">
                    <figure><img src="assets/images/userthumb.png" width="47" height="47" alt=""></figure>
                    <div class="qstext">
                       <span class="nametxt">Chris Lockwood</span>
                       <p class="totalview">5.6k Views - Most Views Writer in The Human Race and Condition</p>
                    </div>
                    <div class="qsdate">
                        <span>20:12</span>
                        <span>27/07/2016</span>
                    </div>
                </div>
                <p class="ansmain">These two inventions might advance knowledge. lead to new ideas and ways of thinking, but will not be seen as the -things- that they are today, the technology might be developed in a completely dif. terent way, particularly depending on when and where is is taken. </p>
                <div class="bottombox clear">
                    <ul class="leftlink">
                        <li><a href="#" class="bluebg">Upload<span>342</span></a></li>
                        <li><a href="#" class="pinkbg">Download</a></li>
                        <li><a href="javascript:void(0);" class="yellowbg newcomment">Comment<span>1</span></a></li>
                    </ul>
                    <ul class="sharelist">
                        <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-plus-square" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <div class="anscomment">
                	<div class="anscommentarea">
                    	<figure><img src="assets/images/userthumb.png" width="47" height="47" alt=""></figure>
                        <div class="commentform clear">
                        	<form name="comment" method="post" action="#">
                            	<textarea></textarea>
                                <input type="submit" value="comment">
                            </form>
                        </div>
                    </div>
                    <div class="anscommentarea">
                    	<div class="anscommentrow">
                            <figure><img src="assets/images/userthumb.png" width="47" height="47" alt=""></figure>
                            <div class="commentform clear">
                                <span class="username">Tien Pham</span>
                                <p class="usercomment">The question of time travel is fascinating</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="giveanswer">
        	<form name="ans" method="post" action="#">
                <textarea placeholder="Give Answer"></textarea>
                <div class="ansbtn">
                	<input type="submit" value="Send" class="bluebtn">
                    <a href="#" class="wellowbtn">Save Draff</a>
                </div>
            </form>
        </div>

    </div>
</div>
<!--answer popup close-->

<div class="mainSite">
	<!--header open-->
	@if(!Auth::check())
	<header id="headersmall" >
    	<div class="container clear">
            <!--logo open-->
            <div class="logo-cont">
                <a href="{{url('')}}"><img src="{!! asset('assets/upload/site_logo/'.$settings->site_logo) !!}" width="140" height="45" alt=""></a>
            </div>
            <!--logo close-->
            
            <!--header right open-->
			
            <div class="header-lan">
                <ul>
					<!--<li class="mnu"><a href="{{url('topics')}}">Topic</a></li>-->
                    <li><a href="#"><img src="{!! asset('assets/frontend') !!}/images/map_1.png" width="31" height="31" alt=""></a></li>
                    <li><a href="#"><img src="{!! asset('assets/frontend') !!}/images/map_2.png" width="31" height="31" alt=""></a></li>
                </ul>
            </div>
			<!--header right close-->
        </div>
    </header>
	@elseif(Auth::check() && Auth::user()->status=='E')
	<header id="headersmall" >
    	<div class="container clear">
            <!--logo open-->
            <div class="logo-cont">
                <a href="{{url('')}}"><img src="{!! asset('assets/upload/site_logo/'.$settings->site_logo) !!}" width="140" height="45" alt=""></a>
            </div>
            <!--logo close-->
            
            <!--header right open-->
			
            <div class="header-lan">
                <ul>
                    <li><a href="#"><img src="{!! asset('assets/frontend') !!}/images/map_1.png" width="31" height="31" alt=""></a></li>
                    <li><a href="#"><img src="{!! asset('assets/frontend') !!}/images/map_2.png" width="31" height="31" alt=""></a></li>
                </ul>
            </div>
			<!--header right close-->
        </div>
    </header>
	@else
    <header id="mainheader" class="clear">
    	<!--logo open-->
        <div class="logo">
            <a href="{{url('user/question-home')}}"><img src="{!! asset('assets/upload/site_logo/'.$settings->site_logo) !!}" width="140" height="45" alt=""></a>
        </div>
        <!--logo close-->
        
        <!--header middle open-->
        <div class="headermid">
            <div class="headersearch clear">
                <form name="search" method="post" action="#">
                    <input type="text" placeholder="Ask  Question" value="">
                    <button type="submit" value="Search"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
            </div>
            
            
            
            <div class="newB">
				<a id="askbtn" href="#askpopup" class="askHead ">
				<img class="tImg" src="{!! asset('assets/frontend') !!}/images/pencil.png" alt=""><img class="tHover" src="{!! asset('assets/frontend') !!}/images/pencil2.png" alt="">
				</a>
				<!--<a href="{{url('topics')}}" class="askQs ">Topic</a>-->
				<a id="askbtn" href="#askpopup" class="askQs aaq"><img src="{!! asset('assets/frontend') !!}/images/pencil.png" alt="">&nbsp; Ask Question</a>
				
				<a href="{{url('user/search-people')}}" class="askQs wQs"><img class="tImg" src="{!! asset('assets/frontend') !!}/images/fnd.png" alt=""/><span>&nbsp; Find People</span></a>

				
            </div>
            <div class="headermenu">
            	<span id="showLeft" class=""><i class="fa fa-bars"></i></span>
                <span class="smallNavbar"><i class="fa fa-info"></i></span>
                <span class="showRight"><i class="fa fa-bar-chart-o"></i></span>
                <div id="navbarOverlay"> </div>
                <nav id="nav">
                    <ul>
                        <!--<li><a href="index.html">Home</a></li>-->
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <!--header middle close-->
        
		<?php
		$unread_msg = \App\Chatmessage::where([
							['to_user_id', '=', Auth::User()->id],
							['to_user_view_status', '=', 'N'] /* */
						])
						->groupBy('from_user_id')
						->orderBy('id', 'desc')
						->get();
		?>
        <!-- -->
        	<div class="notifyLink">
                <ul>
                    <li class="homeSmall"><a href="{{url('user/question-home')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0);" class="topFrnd"><i class="fa fa-users" aria-hidden="true"></i>
					<?php $pend_req = Auth::User()->friend_requests()->where('status','N')->get();?>
						@if(count($pend_req)>0)
						<span>{!!count($pend_req)!!}</span>
						@endif
						</a>
					</li>
                    <li>
						<a href="javascript:void(0);" class="chtHead">
						<i class="fa fa-comments" aria-hidden="true"></i>
						@if(count($unread_msg)>0)
						<span>{!!count($unread_msg)!!}</span>
						@endif
						</a>
					</li>
                    <li><a href="javascript:void(0);" class="notify"><i class="fa fa-bell" aria-hidden="true"></i></a></li>
                </ul>
                <!--notify popup-->
                    <div class="notifyWrap topnotify" id="notifyPop">
                            <h2>Notification<i class="fa fa-question-circle" aria-hidden="true"></i></h2>
                            <ul>
                                <li class="clear">
                                    <a href=""><img src="{!! asset('assets/frontend') !!}/images/userthumb.png" alt=""></a>
                                    <div class="notText">
                                        <p><strong><a href="">John Smith</a></strong> voted this article</p>
                                        <em>"Lorem Ipsum is simply dummy text"</em>
                                        <span>4 mins ago</span>
                                    </div>
                                </li>
                                <li class="clear">
                                    <a href=""><img src="{!! asset('assets/frontend') !!}/images/userthumb.png" alt=""></a>
                                    <div class="notText">
                                        <p><strong><a href="">John Smith</a></strong> voted this article</p>
                                        <em>"Lorem Ipsum is simply dummy text"</em>
                                        <span>4 mins ago</span>
                                    </div>
                                </li>
                                <li class="clear">
                                    <a href=""><img src="{!! asset('assets/frontend') !!}/images/userthumb.png" alt=""></a>
                                    <div class="notText">
                                        <p><strong><a href="">John Smith</a></strong> voted this article</p>
                                        <em>"Lorem Ipsum is simply dummy text"</em>
                                        <span>4 mins ago</span>
                                    </div>
                                </li>
                                <li class="clear">
                                    <a href=""><img src="{!! asset('assets/frontend') !!}/images/userthumb.png" alt=""></a>
                                    <div class="notText">
                                        <p><strong><a href="">John Smith</a></strong> voted this article</p>
                                        <em>"Lorem Ipsum is simply dummy text"</em>
                                        <span>4 mins ago</span>
                                    </div>
                                </li>
                            </ul>
                            <div class="smallAll">
                            	<a href="#" class="">view All</a>
                            </div>
                        </div>
                <!--notify popup close-->
                <!--message popup-->
                    <div class="notifyWrap topMessage" id="msgchat">
                            <h2>Message<i class="fa fa-comments-o" aria-hidden="true"></i></h2>
                            <ul>
							@if(count($unread_msg)>0)
							@foreach($unread_msg as $msg)
                                <li class="clear">
                                    <a href="javascript:void(0);"><img src="{!! asset('assets/frontend') !!}/images/userthumb.png" alt=""></a>
                                    <div class="notText">
                                        <p><strong><a href="javascript:void(0);" class="gotomsg" uid="{{$msg->from_user_id}}">{{($msg->fromUser)?$msg->fromUser->first_name:''}} {{($msg->fromUser)?$msg->fromUser->last_name:''}}</a></strong></p>
                                        
										@if($msg->type == 'T')
											<em>{!! $msg->message !!}</em>
										@else
											@if(in_array($msg->file_ext,array('jpg','JPG','JPEG','png','PNG','bmp','BMP','gif','GIF')))
												<em><a href="javascript:void(0);" class="chat_pic_msg gotomsg" uid="{{$msg->from_user_id}}"><img style="width:80px;" src="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}"/></a></em>
											@else
												<em><a href="javascript:void(0);"  class="gotomsg" uid="{{$msg->from_user_id}}" >{!! $msg->message !!}</a></em>
											@endif
										@endif
                                        <span>{!! time_elapsed_string(strtotime($msg->created_at)) !!} ago</span>
                                    </div>
                                </li>
							@endforeach
							@endif
                            </ul>
                            <div class="smallAll">
                            	<a href="{{url('chat/messages')}}" class="">view All</a>
                            </div>
                        </div>
                <!--message popup close-->
                
                <!--frnd popup-->
                    <div class="notifyWrap frndList" id="frndl">
						<h2>Friend Request<i class="fa fa-user" aria-hidden="true"></i></h2>
						<ul>						 
						@if(count(Auth::User()->friend_requests)>0)
							@foreach(Auth::User()->friend_requests as $frndreq)
							<li class="clear">
								<a href=""><img src="{!! asset('assets/frontend') !!}/images/userthumb.png" alt=""></a>
								<div class="notText">
									<p><strong><a href="{{url('profile/'.$frndreq->form_user->id)}}">{{$frndreq->form_user->first_name}} {{$frndreq->form_user->last_name}}</a></strong></p>
									<div class="linkBox">
										<a href="javascript:void(0);" class="accBtn acptfrnd"><i class="fa fa-check" aria-hidden="true"></i> <font>Accept</font></a>
										<a href="javascript:void(0);" class="dltBtn deltereq"><i class="fa fa-ban" aria-hidden="true"></i> remove</a>
										<input type="hidden" value="{{$frndreq->from_user_id}}"/>
									</div>
								</div>
							</li>
							@endforeach
						@endif
						</ul>
						<div class="smallAll">
							<a href="{{url('user/friend-request')}}" class="">view All</a>
						</div>
					</div>
                <!--frnd popup close-->
            </div>
        <!-- -->
        <?php $authuser=Auth::user();
		$profImag = asset('assets/frontend/images/profile.jpg');
		if($authuser->profile_image !='' && file_exists('assets/upload/profile_image/'.$authuser->profile_image)==1)
		{
			$profImag =asset('assets/upload/profile_image/'.$authuser->profile_image);
		}									
		?>
        <!--header right open-->
        <div class="header-right">
            <span class="showRight"><i class="fa fa-bar-chart-o"></i></span>
            <a class="profilecont" href="javascript:void(0);"><span class="userthumb"><img src="{!! $profImag !!}" width="32" height="32" alt=""></span>{{Auth::user()->first_name}} {{Auth::user()->last_name}}</a>
            <ul class="onDesk">
            	<li><a href="{!! url('user/account') !!}"><i class="fa fa-user" aria-hidden="true"></i>Profile</a></li>
                <li><a href="{!! url('user/setting') !!}"><i class="fa fa-cog" aria-hidden="true"></i>setting</a></li>
                <li><a href="{!! url('logout') !!}"><i class="fa fa-sign-out" aria-hidden="true"></i>logout</a></li>
            </ul>
            <div class="profix">
            	<div class="proSide">
                    <figure><a href="{{url('user/account')}}"><img src="{!! asset('assets/frontend') !!}/images/profile.jpg" alt=""></a></figure>
                    <h2>{{Auth::user()->first_name}} {{Auth::user()->last_name}}</h2>
                    <ul>
                        <li><a href="{!! url('user/setting') !!}"><i class="fa fa-cog" aria-hidden="true"></i>setting</a></li>
                        <li><a href="{!! url('logout') !!}"><i class="fa fa-sign-out" aria-hidden="true"></i>logout</a></li>
                    </ul>
                </div>
                <div class="copyRight">
                </div>
            </div>
        </div>
        <!--header right close-->
        
    </header>
    
	@endif
	<!--header close-->
    
    @yield('content')
    
    <!--footer open-->
    <footer id="mainfooter">
    	<!--footer top open-->
    	<div class="footertop">
        	<div class="wrapper clear">
            	<!--<div class="footerleft">
                	<ul>
                    	<li><a href="mailto:"><img src="{!! asset('assets/frontend') !!}/images/email2.png" width="30" height="30" alt=""></a></li>
                        <li><a href="tel:0945981289"><img src="{!! asset('assets/frontend') !!}/images/call.png" width="30" height="30" alt="">0945981289</a></li>
                    </ul>
                </div>-->
                <span class="copyright">Copyright &copy; 2016 Iknow coporation</span>
                <div class="footermenu">
                	<ul>
                    	<li><a href="#">About us</a></li>
                        <li><a href="#">Terms of service</a></li>
                        <li><a href="#">Privacy</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
                <div class="footer-right">
                	<a href="#" class="homelink"><img src="{!! asset('assets/frontend') !!}/images/home.png" width="26" height="24" alt="">http://iknow.com.vn</a>
                    <select class="footerlan">
                    	<option>VNI</option>
                        <option>ENG</option>
                    </select>
                </div>
               
            </div>
        </div>
        <!--footer top close-->
        
        <!--footer bottom open-->
        <!--<div class="footerbot">
        	<div class="wrapper clear">
            	<div class="footermenu">
                	<ul>
                    	<li><a href="#">About us</a></li>
                        <li><a href="#">Terms of service</a></li>
                        <li><a href="#">Privacy</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
                <span class="copyright">Copyright &copy; 2016 Iknow coporation</span>
            </div>
        </div>-->
        <!--footer bottom close-->
    </footer>
    <!--footer close-->

    <!--scroll to top-->
    	<a href="javascript:void(0);" class="pageScroll"><img src="{!! asset('assets/frontend') !!}/images/scroll-to-top.png" alt=""></a>
    <!--scroll to top close-->
	@include('chat.chat_right_panel')
	<div class="chatpopup">

		<div class="clear"></div>
	</div>
</div>

<style type="text/css">
	/* Dilip(09-03-2017) */
	.asideRight{display:block; float:left; width:240px; border-right:1px solid #d9d9d9; background-color:#fff;}
	.allFriendsList{display:none; margin:0; height:282px; overflow:auto; border: 1px solid #ddd; border-top:none;}
	.allFriendsList li{display:block; margin:0;}
	.allFriendsList li a{display:block; position:relative; padding:5px 24px 5px 52px;; min-height:48px;}
	.allFriendsList li a .image{display:block; position:absolute; top:5px; left:8px; width:38px; height:38px; border-radius:100px; overflow:hidden;}
	.allFriendsList li a .image img{display:block; width:100%; height:auto; min-height:100%;}
	.allFriendsList li a .name {
    display: block;
    color: #5a5a5a;
    font-size: 14px;
    line-height: 38px;
    font-weight: 600;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
}
	.allFriendsList li a .status,
	.onlinechat .title .uname .status{display:block; position:absolute; top:50%; right:8px; -webkit-transform:translate(0,-50%); transform:translate(0,-50%); width:11px; height:11px; border:2px solid #a1a0a0; border-radius:100px;}
	.onlinechat .title .uname .status{right:auto; left:8px; width:10px; height:10px;}
	.allFriendsList li a .status.online,
	.onlinechat .title .uname .status.online{border-color:#78d62c; background-color:#78d62c;}
	.allFriendsList li a:hover{text-decoration:none; background-color:#f2f2f2;}
	.allFriendsList li a:hover .name{color:#3498db; text-decoration:none;}
.allUserTabtab_view {
    display: block;
    position: fixed;
    bottom: 0;
    right: 0;
    z-index: 999;
    background-color: #fff;
    width: 228px;
	border-radius:4px 4px 0 0;
    box-shadow: 0 11px 9px 2px rgba(0,0,0,0.5);
	padding-top:38px;
}
.allUserTabtab_view .ttl{display:block; width:100%; height:38px; text-align:center; background-color:#006ecb; color:#fff; font-size:16px; line-height:38px; font-weight:600; position:absolute; top:0; left:0; width:100%; border-radius:4px 4px 0 0; cursor:pointer;}
@media (max-width:767px){
	.allUserTabtab_view{background:transparent;}
	.allFriendsList{background-color:#fff;}
	.allUserTabtab_view .ttl{
		font-size: 0px;
		width: 38px;
		height: 38px;
		border-radius: 100px;
		left: auto;
		right: 10px;
		box-shadow: 0 0 3px 1px rgba(0,0,0,0.4);
		-webkit-transition:400ms;
		transition:400ms;
	}
	.allUserTabtab_view .ttl:before{
		content: "\f086";
		font-size: 21px;
		font-family: FontAwesome;
		-webkit-transition:400ms;
		transition:400ms;
		visible:visible;
		opacity:1;
		position:absolute;
		top:50%;
		left:50%;
		-webkit-transform:translate(-50%,-50%);
		transform:translate(-50%,-50%);
	}
	.allUserTabtab_view{box-shadow:none;}
	.allUserTabtab_view .ttl.showIcon{width:100%; border-radius:4px 4px 0 0; font-size:15px; right:0px;}
	.allUserTabtab_view .ttl.showIcon:before{visibility:hidden; opacity:0;}
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<script src="{!! asset('assets/frontend/tagJs/tag-it.js') !!}" type="text/javascript" charset="utf-8"></script>

<script src="{!! asset('assets/frontend/js/placeholders.min.js') !!}"></script>
<script src="{!! asset('assets/frontend/fancybox/jquery.fancybox.js') !!}"></script>
<script type="text/javascript" src="{!! asset('assets/common/jquery.form.min.js') !!}"></script>
<script src="{!! asset('assets/frontend/mCustomScrollbar/jquery.mCustomScrollbar.js') !!}"></script>

<script src="{!! asset('assets/frontend/bxslider/js/jquery.bxslider.js') !!}"></script>

<script>
$(document).ready(function() {
	$("body").on("click",".allUserTabtab_view .ttl", function(){
		if(!$(".allFriendsList").is(":visible")){
			$(".allUserTabtab_view .ttl").addClass("showIcon");
			$(".allFriendsList").slideDown(300);
		}else{
			$(".allUserTabtab_view .ttl").removeClass("showIcon");
			$(".allFriendsList").slideUp(300);
		}
	});
	
	$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    $('.shortqspopup').fancybox({
		padding:0,
		afterShow: function(){
			$(".viewansbox").mCustomScrollbar();
			setMapLoc('loction');
			setMapLoc('joblocation');
			$('.filterIcon').click(function(){
				$('.jilterList').slideToggle(200);
			});
		}
	});
	
    $('#qbxbtn').fancybox({
		padding:0,
		helpers: {
			overlay: {
			  locked: false
			}
		  },
		afterShow: function(){
			$(".viewansbox").mCustomScrollbar();
			setMapLoc('loction');
			setMapLoc('joblocation');
		},
		afterClose: function() {
			$("#askbtn").fancybox().trigger('click');
			/* $.fancybox({
                'href': '#askpopup'
            }); 
			$.fancybox.open([
				{
					type: 'ajax',
					href : '#askpopup',   
				}
			], {
				padding : 0
			});*/
		}
	});
	
    $('#askbtn').fancybox({
		padding:0,
		helpers: {
			overlay: {
			  locked: false
			}
		  },
		afterShow: function(){
			$("#qMsg").html('').hide();
			$("#qsnFrm").show();
			$(".viewansbox").mCustomScrollbar();
			var currpage = '{{$currpage}}';
			if(currpage == 'topic')
				$('#asktlt').text('Create new thread');
			else	
				$('#asktlt').text('Ask a question');
			
			$('#sec1,#sec2,#sec3,#sec4,#sec5,#sec6,#sec7,#secEmail').show();
			$('#sec1,#sec2,#sec3,#sec4,#sec5,#sec6,#sec7,#secEmail').find("input[type=text], select, textarea").prop('disabled', false);
			$('#qsnFrm').find("input[type=text], textarea").val("");
			$('#tglist').html("");
			$('#sec8').find("textarea").prop('disabled', true);
			$('#sec8').hide();
			$('#qsnFrm').find('input[type="submit"]').val('Submit Question');
			$('#qsnFrm').find('input[name="is_forword"]').val(0);
			//$('#sec4').find('.label').text('Question content:');
		},
		afterClose: function() {
			$("#qMsg").html('').hide();
			$(".emsg").remove();
		}
	});
	
	$('.newcomment').click(function(){
		$(this).parent('li').parent('ul').parent('.bottombox').next('.anscomment').slideToggle(200);
	});
	
	//
	$('.btnText').click(function(){
		$('.listWrap').find('.shortDesc').slideUp(200);
		$(this).parent('figure').parent('.listWrap').find('.shortDesc').slideToggle(200);
	})
	
	//
	$('.tabCont').hide(0);
	$('.tabCont:first-child').show(0);
	//$('.tabmenu ul li:first-child').addClass('active');
	$('#tab1').click(function() {
		$('.tabCont').hide(0);
		$('.tabCont:first-child').show(0);
		$('.tabmenu ul').find('li').removeClass('active');
		$(this).parent('li').addClass('active');
	});
	$('#tab2').click(function() {
		$('.tabCont').hide(0);
		$('.tabCont:nth-child(2)').show(0);
		$('.tabmenu ul').find('li').removeClass('active');
		$(this).parent('li').addClass('active');
	});
	$('#tab3').click(function() {
		$('.tabCont').hide(0);
		$('.tabCont:nth-child(3)').show(0);
		$('.tabmenu ul').find('li').removeClass('active');
		$(this).parent('li').addClass('active');
	});
	
	//
	$('.editSingle').click(function() {
		$(this).parent('.editAbout').find('.editmain').slideDown(200);
		$(this).parent('.editAbout').find('.saveDeta').slideUp(200);
	});
	$('.cancelEdit').click(function() {
		$(this).parent('.noclass').parent('.editmain').slideUp(200);
		$(this).parent('.noclass').parent('.editmain').prev('.saveDeta').slideDown(200);
	});
	$('.removeText').click(function() {
		$(this).parent('.pEdit').hide(0);
	});
	
	$('.editMultiple').click(function() {
		$(this).parent('.editAbout').find('.editmain').slideDown(200);
	});
	
	
	$('.actionBox .editTxt').click(function() {
		$(this).parent('.actionBox').parent('.sliderow').find('.textDeta').slideUp(200);
		$(this).parent('.actionBox').parent('.sliderow').find('.editBox').slideDown(200);
		$(this).parent('.actionBox').hide(0);
	});
	
	
	$('.editBox.addedt .cnclBtn').click(function() {
		$(this).parent('.btnRow').parent('.editBox').parent('.addEdit').slideUp(200);
	});
	
	$('.sliderow .cnclBtn').click(function() {
		$(this).parent('.btnRow').parent('.editBox').slideUp(200);
		$(this).parent('.btnRow').parent('.editBox').prev('.textDeta').slideDown(200);
		$(this).parent('.btnRow').parent('.editBox').parent('.sliderow').children('.actionBox').slideDown(200);
	});
	
	$('.editCs').click(function() {
		$(this).parent('.editAbout').find('.editmain').slideDown(200);
		$(this).parent('.editAbout').find('.noclose').slideUp(200);
		$(this).parent('.editAbout').find('.editsaveDeta').slideDown(200);
	});
	$('.cancelEditLg').click(function() {
		$(this).parent('.noclass').parent('.editmain').slideUp(200);
		$(this).parent('.noclass').parent('.editmain').parent('.editAbout').find('.editsaveDeta').slideUp(200);
		$(this).parent('.noclass').parent('.editmain').parent('.editAbout').find('.noclose').slideDown(200);
	});
	
	$('.saveDeta .closebox').click(function() {
		$(this).parent('.addbox').remove();
	});
	$('.edittxt').click(function() {
		$(this).parent('.textE').slideUp(200);
		$(this).parent('.textE').next('.contactEdit').slideDown(200);
	});
	$('.nofrm').click(function() {
		$(this).parent('.contactEdit').slideUp(200);
		$(this).parent('.contactEdit').parent('li').find('.textE').slideDown(200);
	});
	
	$(document).on('click','.edtEml',function() {
		$(this).parent('.linkAddress').find('.textE').slideUp(200);
		$(this).parent('.linkAddress').find('.contactEdit').slideDown(200);
		$(this).hide(0);
		$(this).parent('.linkAddress').find('.cnEdt').hide(0);
		$(this).parent('.linkAddress').find('.btnRow').slideDown(200);
	});
	$(document).on('click','.viwSmlAdd .cnEdt',function() {
		$(this).parent('.linkAddress').find('.textE').slideDown(200);
		$(this).parent('.linkAddress').find('.contactEdit').remove(200);
		//$(this).hide(0);
		$(this).parent('.linkAddress').find('.edtEml').show(0);
		$(this).parent('.linkAddress').slideUp(200);
	});
	$('.addEml').click(function() {
		$(this).parent('.tabContact').find('.addSmlAdd').slideDown(200);
	});
	$('.addSmlAdd .cnEdt').click(function() {
		$(this).parent('.addSmlAdd').slideUp(200);
	});
	
	$('.addSmlAdd .cnclBtn').click(function() {
		$(this).parent('.btnRow').parent('.addSmlAdd').slideUp(200);
	});
	
	$('.viwSmlAdd .cnclBtn').click(function() {
		$(this).parent('.btnRow').parent('.viwSmlAdd').find('.contactEdit').slideUp(200);
		$(this).parent('.btnRow').parent('.viwSmlAdd').find('.textE').slideDown(200);
		$(this).parent('.btnRow').parent('.viwSmlAdd').find('.cnEdt').show(0);
		$(this).parent('.btnRow').parent('.viwSmlAdd').find('.edtEml').show(0);
		$(this).parent('.btnRow').slideUp(200);
	});
	
	$('#edtNme').click(function() {
		$(this).parent('h2').slideUp(200);
		$('.editName').children('.editmain').slideDown(200);
	});
	$('.nCan').click(function() {
		$('.editName').children('h2').slideDown(200);
		$('.editName').children('.editmain').slideUp(200);
	});

});



</script>
<!--end js-->

@yield('customScript')

@include('include.footer_scripts')

<script src="{!! asset('assets/frontend/js/custom.js') !!}" type="text/javascript"></script>



<script>
function heightcontrol() {
	var windowheight = $(window).height();
	var headerheight = $("#mainheader").innerHeight();
	var footerheight = $("#mainfooter").innerHeight();
	var headfootheight = (headerheight + footerheight);
	var middleheight = (windowheight - headfootheight);
	$(".pageMiddle").css({"min-height": middleheight });
}
$(window).load(function () {
heightcontrol();
	$(window).resize(function() {
	heightcontrol();
	});
});

$(function(){
	var sampleTags = [
	@if(!empty($all_tropics))
	  @foreach($all_tropics as $tval)
		{!!'{"id": "'.$tval->id.'", "value": "'.$tval->title.'"} ,'!!}
	  @endforeach
	@endif
	];

	$('#singleFieldTags2').tagit({
		allowSpaces: true,
		//availableTags: sampleTags,
		autocomplete: {
			source: sampleTags,
			minLength: 0,
			delay: 0,
			select: function (event, ui) {
				itemId = ui.item.id;
				//alert(itemId);
				$('#singleFieldTags2').parent().append('<input name="tags[]" type="hidden" value="'+itemId+'"/>');
				//$(this).tagit("createTag", ui.item.value);
				$('#singleFieldTags2').tagit();
			}
		},
		
	});	
	$('#emails').tagit({
		allowSpaces: true,
		//availableTags: sampleTags,
		/* autocomplete: {
			source: sampleTags,
			minLength: 0,
		}, */	
		tagSource: function( request, response ) {
            $.ajax({
				url: "{!! url('user/get-user-autocomplete') !!}" ,
				headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
                data: { term:request.term },
                dataType: "json",
                success: function( data ) {
                    response( $.map( data.label, function( item ) {
                        return {
                            label: item
                        }
                    }));
                }
            });
		}
		
	});	
	
});

$(function(){
	//var sampleEmails = ['dilip@technoexponent.com', 'pritam@technoexponent.com', 'santu@technoexponent.com', 'alok@technoexponent.com', 'alapan@technoexponent.com', 'tuhin@technoexponent.com'];

	//$('#emails').tagit({
	//	allowSpaces: true,
	//	availableTags: sampleEmails
	//});
});


//var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);


//if (isChrome) alert("You are using Chrome!");
if (isSafari){
	$(document).on('submit',".validFrm",function(e){
		//e.preventDefault();
		//alert('prevent submit'); 
		var frmObj = $(this);
		var flag = 0;
		frmObj.find('input[type=text],select').each(function(index,elem) {
			console.log(elem);
			if($(elem).attr("required"))
			{
				if($(elem).val()=='')
				{
					//alert($(elem).attr("name"));
					flag = 0;
					alert("Please fill out this field");
					$(elem).focus();
					return false;
				}else	flag = 1;
			}
		});
		if(flag=='1'){
			frmObj.submit();
		}else{
			e.preventDefault();
			return false;
		}
	});
}
@if(Auth::check())
$('.filterIcon').click(function(){
	$('.jilterList').slideToggle(200);
});
@endif
$('.filterSave').click(function(){
	$('.jilterList').slideUp(200);
});

$( "form.ordrFrm" ).on( "submit", function( event ) {
//
	var frmobj = $(this);
	event.preventDefault();
	var formdata =  $( this ).serializeArray();
	/* console.log(formdata); */
	var lstyp = formdata[Object.keys(formdata).length-1].value;
	
	$(".filterRow").hide();
	$.ajax({
		type:"post",
		url: "{!! url('user/set-filter') !!}" ,
		headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
		data: formdata,			
		//dataType: "json",			
		success:function(res) {
			console.log(res);
			if(res==1)
			{
				if(lstyp==2 )	
					$( "form#searchFrm2" ).submit();
				else	
					$( "form#searchFrm" ).submit();
			}
			//window.location.reload();
			/* */
		}
	});
	$.each(formdata, function (key, val) {
        //console.log(val.name +' = '+ val.value);
		var fltobj = '.'+val.value;
		$(fltobj).show();
    });
	/* $(".filterRow").each(function(){
		
	}); */
});
 $('#shortable_area2').sortable({
	cursor: "move",
	update: function( event, ui ) {
		var sort_item = $(ui.item).find('input[type=checkbox]').val();
		var prev_item = ui.item.prev().find('input[type=checkbox]').val();
		$(".proHeight div."+sort_item).insertAfter( $( ".proHeight div."+prev_item ) );
		$(".proHeight div."+prev_item).next().remove();
	}
});

$( function() {
	/* var list_arr = [
		
		];
	var filter_arr = new Array();
	$.each(list_arr,function(index,value){
		console.log(value);
	   $('ul#shortable_area').append($('.'+value));
	});
	$.each(list_arr,function(index,value){
	   $('ul#shortable_area2').append($('.'+value));
	}); */
});

$(document).on( "click",".acptfrnd", function( event ) {

  //event.preventDefault();
  var obj =  $( this );
  var frnd_id = obj.parent().children('input').val();
  //alert(frnd_id);
	 $('.sndmsg').html('');
	$.ajax({
		type:"post",
		url: "{!! url('user/accept-friend-request') !!}" ,
		headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
		data: {'frnd_id':frnd_id},			
		//dataType: "json",			
		success:function(res) {
			/* console.log(res); */
			if(res==0){
				obj.find('font').text('Friends');
				obj.css('background-color', '#f39c11');
				//obj.parent().parent().parent('li').remove();
			}
		}
	});
	
	 event.stopPropagation();
});
$(document).on( "click",".deltereq", function( event ) {

  //event.preventDefault();
  var obj =  $( this );
  var frnd_id = obj.parent().children('input').val();
  //alert(frnd_id);
    if (confirm("Are you delete?") == true) {        
		$.ajax({
			type:"post",
			url: "{!! url('user/del-friend-request') !!}" ,
			headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
			data: {'frnd_id':frnd_id},			
			//dataType: "json",			
			success:function(res) {
				console.log(res);
				if(res==0){
					obj.parent().parent().parent('li').remove();
				}
			}
		});	
		
    }
	 event.stopPropagation();
});

$(document).on( "click",".gotomsg", function( event ) {
	var uid= $(this).attr('uid');
	
	if(uid)
	{
		$.ajax({
			type:"post",
			url: "{!! url('chat/startChat') !!}" ,
			headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
			data: {'user_id':uid},			
			//dataType: "json",			
			success:function(res) {
				//console.log(res);
				if(res!=0){
					window.location.href="{{url('chat/messages')}}";
				}
			}
		});		
	}
});	
</script>

@if(session('askqsn_success'))
	
	<script>
	$(function(){
		$("#askbtn").fancybox().trigger('click');
		});
	</script>

@endif
@if(auth::check())
    <script src="{!!url('assets/frontend/chat/chatpop.js')!!}" type="text/javascript"></script>
@endif
</body>
</html>