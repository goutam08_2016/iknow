var sender_arr= new Array();
function startChatPop(user_id,is_pop)
{
    setTimeout(function(){
        if(user_id != '' && user_id != 0 && !isNaN(user_id))
        {
            $.ajax({
                type:"POST",
                url: BASE_URL+'chat-pop/startChatPop',
				headers: {'X-CSRF-TOKEN': _TOKEN},
                data: {user_id:user_id},
                dataType: "json",
                success: function(response) {//alert(response);
                    if(response.has_error == 0)
                    {
                        if(sender_arr.indexOf(user_id)==(-1))
                        {
                            sender_arr.push(user_id);
                            $('.chatpopup').append(response.all_msg_list_html);
                            if(is_pop == 1)
                            {
                                $('#chatbox_pop_'+user_id+' .onlinechat .title').css('background-color', '#e6233f');
                            }
                            var totalheight= 0;
                            $('#showbox_'+user_id+' .list').each(function(){
                                totalheight += $(this).height() + 60;
                                });
                            $('#showbox_'+user_id).animate({
                                scrollTop: totalheight
                            });
                            bindUplodFormPop(user_id);
                            $('.chat_pic_msg').fancybox();
                        }
                        
                    }
                }
            });
        }
    },500);
}
function addToChatPop(user_id)
{
    if(user_id != '' && user_id != 0 && !isNaN(user_id))
    {
        $.ajax({
            type:"POST",
            url: BASE_URL+'chat-pop/addToChatPop',
			headers: {'X-CSRF-TOKEN': _TOKEN},
            data: {user_id:user_id},
            dataType: "json",
            success: function(response) {
                if(response.has_error == 0)
                {
                    $('.all_userchat_right_panel ul').prepend(response.li_html);
                    if(response.is_online == 1)
                    {
                        $('.online_userchat_right_panel ul').prepend(response.li_html);
                    }
                }
                startChatPop(user_id,0);
            }
        });
    }
}
$(window).load(function () {    
    $('body').on("click",".uname",function() { 
        var obj=$(this).parent().parent(".onlinechat").parent(".popupchatbox");
        
        var totheight = obj.height();
        var tiheight = $(this).parent().height();
        totheight=totheight-tiheight-8;
        if(obj.css("bottom")=="320px"){
            obj.animate({bottom: "34px"},300);
        }else{
            obj.animate({bottom: "320px",},300);
        }
    });
    $('.chatpopup').on('click','.onchatclose',function() {
        $(this).parent().parent(".onlinechat").parent(".popupchatbox").remove();
        var pop_user_id = $(this).attr('data-user-id');
        var index = sender_arr.indexOf(pop_user_id);
        if(index != (-1))
        {
            sender_arr.splice(index, 1);
        }
    });
});
$('body').on('keypress','.onlinechattype textarea.message',function(e) {
    if(e.keyCode == 13)
    {
        var message = $(this).val();
        var messagebox = $(this);
        if(message != '')
        {
            var from_user_id = $(this).parent('.onlinechattype').children('form.upload_chat_form').children(' input.from_user_id').val();
            var to_user_id = $(this).parent('.onlinechattype').children('form.upload_chat_form').children(' input.to_user_id').val();
            if(from_user_id != '' && to_user_id != '')
            {
                $.ajax({
                    type:"POST",
                    url: BASE_URL+'chat-pop/sendMessagePop',
					headers: {'X-CSRF-TOKEN': _TOKEN},
                    data: {from_user_id:from_user_id,to_user_id:to_user_id,message:message,type:'T'},
                    dataType: "json",
                    success: function(response) {
                        if(response.has_error == 0)
                        {
                            $('#showbox_'+to_user_id).append(response.ajax_msg_html);
                            var totalheight= 0;
                            $('#showbox_'+to_user_id+' .list').each(function(){
                                totalheight += $(this).height() + 60;
                                });
                            $('#showbox_'+to_user_id).animate({
                                scrollTop: totalheight
                            });
                            messagebox.val('');
                            $('.chat_pic_msg').fancybox();
                        }
                        else
                        {
                            var error_html = '<div id="warningMessageBox" class="chatSbmtformsg" style="min-height:0px;"><div class="buttonset align-center"><a class="button" href="'+BASE_URL+'user/'+LOCALE_LANG+'/credit">Buy Credits to Send Message</a></div></div>';
                            messagebox.parent('.onlinechattype').html(error_html);
                        }
                    }
                });
            }
        }
    }
});
$(document).on('click','.attachFile,textarea.message',function(){
    if($(this).attr('class') == 'message')
    {
        $(this).parent().parent('.onlinechat').find('span.title').css('background-color', '#4A4A4A');
    }
    else
    {
        $(this).parent().parent().parent().parent('.onlinechat').find('span.title').css('background-color', '#4A4A4A');
    }
});

$(document).on('change','.attachFile',function(e){
	e.preventDefault();
    //$(this).parent().parent()
	//alert($(this).parent().parent().html());
    $(this).parent().parent('form.upload_chat_form').submit();
});

function bindUplodFormPop(user_id)
{	
    $("#upload_chat_form_"+user_id).ajaxForm({
        beforeSubmit : function(formData, jqForm, options){
        },
        success: function(responseText, statusText, xhr, jform){ 
            //console.log(responseText+"=="+statusText+"=="+xhr+"=="+jform);
            
            jform.clearForm();
            if(responseText != 'error' && statusText == "success"){
                var to_user_id_pop = responseText.to_user_id_pop;
                if(responseText.has_error == 0)
                {
                    $('#showbox_'+to_user_id_pop).append(responseText.ajax_msg_html);
                    var totalheight= 0;
                    $('#showbox_'+to_user_id_pop+' .list').each(function(){
                        totalheight += $(this).height() + 60;
                        });
                    $('#showbox_'+to_user_id_pop).animate({
                        scrollTop: totalheight
                    });
                    messagebox = $('#onlinechattype_'+to_user_id_pop+' textarea.message');
                    messagebox.val('');
                    $('.chat_pic_msg').fancybox();
                }
                else if(responseText.has_error == 2)
                {
                    alert('File Type Not Supported!');
                }
                else
                {
                    var error_html = '<div id="warningMessageBox" class="chatSbmtformsg" style="min-height:0px;"><div class="buttonset align-center"><a class="button" href="'+BASE_URL+'user/'+LOCALE_LANG+'/membership">Buy Credits to Send Message</a></div></div>';
                   $('#onlinechattype_'+to_user_id_pop).html(error_html);
                }
            }
            else if(responseText == 'error')
            {
                alert('File Type Not Supported!');
            }
        }
    });
}
function loadUnreadMessagesPop()
{
	//alert( _TOKEN);
    $.ajax({
        type:"POST",
        url:BASE_URL+'chat-pop/loadUnreadMessagesPop',
		headers: {'X-CSRF-TOKEN': _TOKEN},
        data:{},
        dataType: "json",
        success:function(response){
            if(response.has_error == 0)
            {
                $.each(response.from_user_msg, function(index, msg){
                    if(sender_arr.indexOf(index)==(-1))
                    {
                        startChatPop(index,1);
                    }
                    else
                    {
                        $('#showbox_'+index).append(msg.ajax_msg_html);
                        $('#chatbox_pop_'+index+' .onlinechat .title').css('background-color', '#e6233f');
                        var totalheight= 0;
                        $('#showbox_'+index+' .list').each(function(){
                            totalheight += $(this).height() + 60;
                            });
                        $('#showbox_'+index).animate({
                            scrollTop: totalheight
                        });
                        $('.chat_pic_msg').fancybox();
                    }
                });
            }
        }
    });
}
$(document).ready(function(){
    setInterval(function() {
        loadUnreadMessagesPop();
    }, 5000);
});
