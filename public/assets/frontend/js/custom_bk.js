// JavaScript Document

//showLeft
$("#showLeft").click(function() {
	$(".leftpan").animate({"left":"0"}, 400);
	$("#navbarOverlay").addClass("show");
	$("#nav").slideUp(0);
	$(".header-right").find('.onDesk').slideUp(0);
	$(".header-right").find('.profilecont').removeClass('active');
});
$("#navbarOverlay").click(function() {
	$(".leftpan").animate({"left":"-220px"}, 400);
	$("#navbarOverlay").removeClass("show");
});

//showRight
$(".showRight").click(function() {
	$(".rightpan").animate({"right":"0"}, 400);
	$("#navbarOverlay").addClass("show");
	$("#nav").slideUp(0);
	$(".header-right").find('.onDesk').slideUp(0);
	$(".header-right").find('.profilecont').removeClass('active');
});
$("#navbarOverlay").click(function() {
	$(".rightpan").animate({"right":"-220px"}, 400);
	$("#navbarOverlay").removeClass("show");
	$(".profix").animate({"right":"-220px"}, 400);
	$(".profilecont").removeClass("active");
});

//menu
$(".smallNavbar").click(function() {
	$("#nav").slideToggle(300);
	//$("#navbarOverlay").addClass("show");
	$(".header-right").find('.onDesk').slideUp(0);
	$(".header-right").find('.profilecont').removeClass('active');
});



$(window).resize(function(){
	var getwidth = $(this).width();
	if(getwidth > 1023)
	{
		$('#nav').removeAttr('style');
		$('.rightpan').removeAttr('style');
		$('.leftpan').removeAttr('style');
		$('#navbarOverlay').removeClass('show');
	}
});
$(window).resize(function(){
	var getwidths = $(this).width();
	if(getwidths > 580)
	{
		$('.onDesk').removeAttr('style');
		$('.profilecont').removeClass('active');
	}
});


//read answer
$('.readans').click(function(){
	$(this).next('.viewqsbox').slideToggle(300);
});

//profile menu
$('.profilecont').click(function(){
	$('.onDesk').slideToggle(300);
	$(this).toggleClass('active');//.profix
	$("#nav").slideUp(0);
	$(".profix").animate({"right":"0"}, 400);
	$("#navbarOverlay").addClass("show onlyD");
});

//
$('.pageScroll').click(function(){
	var getPos = $('body').offset().top;
   $('html, body').animate({scrollTop: getPos + 'px' }, 500);
});
$(window).scroll(function(){
	if($(this).scrollTop() > 10){
		$('.pageScroll').show(300);
	}
	else{
		$('.pageScroll').hide(300);
	}
});

//
/*$('.hovernxt').click(function() {
	alert('hi')
    $('.hoverSpace').show(0);//hide element
});
$(document).click(function() {
    $('.hoverSpace').hide(0);//hide element
});
$(".hoverSpace").click(function(event) {
    event.stopPropagation();
});*/

$('.hovernxt').click(function() {
    $(this).children('.hoverSpace').show(0);//hide element
});

$(document).click(function(e) {

	var containerList = $('.hovernxt');

	if(!containerList.is(e.target) && containerList.has(e.target).length == 0)

	{

		$('.hoverSpace').fadeOut(0);

	}

});




//
/*$(document).each(function () {
    var cheight = $('.hoverBox').outerHeight();
	var positionBox = $(this).offset().top;
	//alert(positionBox);
	if(positionBox < cheight){
		$(this).children('.hoverSpace').addClass('botBox');
	}
});*/

$('.hovernxt').mouseover(function(){
	var cheight = $('.hoverBox').outerHeight();
	var positionBox = $(this).offset().top;
	//alert(positionBox);
	if(positionBox < cheight){
		$(this).children('.hoverSpace').addClass('botBox');
	}
});



$('.notify').click(function(){
	$('#notifyPop').slideToggle(200);
	$('#msgchat').hide(0);
	$('#frndl').hide(0);
	$("#nav").slideUp(100);
});
$(document).click(function() {
	$('#notifyPop').slideUp(200);//hide element
});
$(".notify").click(function(event) {
	/* event.stopPropagation(); */
});
$(".notifyWrap").click(function(event) {
	/* event.stopPropagation(); */
});

$(".notifyWrap ul").mCustomScrollbar();

//
$('.chtHead').click(function(){
	$('#msgchat').slideToggle(200);
	$('#notifyPop').hide(0);
	$('#frndl').hide(0);
	$("#nav").slideUp(100);
});
$(document).click(function() {
	$('#msgchat').slideUp(200);//hide element
});
$(".chtHead").click(function(event) {
	event.stopPropagation();
});

//
$('.topFrnd').click(function(){
	$('#frndl').slideToggle(200);
	$('#notifyPop').hide(0);
	$('#msgchat').hide(0);
	$("#nav").slideUp(100);
});
$(document).click(function() {
	$('#frndl').slideUp(200);//hide element
});
$(".topFrnd").click(function(event) {
	event.stopPropagation();
});


$(".qpop").click(function(event) {
	$(this).parent('span').parent('.topsmall').parent('.mobileCount').find('.noDesc').slideToggle(200);
});
$(".moreTo").click(function(event) {
	$(this).prev('ul').addClass('maxheight');
	$(this).hide(0);
});


$('.right-top').clone().appendTo('.copyRight');
$('.guidlines').clone().appendTo('.copyRight');

$('#engL').click(function(){
	$(this).parent('.languageSelect').addClass('newLan');
	$(this).addClass('opn');
	$(this).nextAll('.lanTab').removeClass('opn');
	$(this).prevAll('.lanTab').removeClass('opn');
});
$('#vniL').click(function(){
	$(this).parent('.languageSelect').removeClass('newLan');
	$(this).addClass('opn');
	$(this).nextAll('.lanTab').removeClass('opn');
	$(this).prevAll('.lanTab').removeClass('opn');
});

$(window).load(function(){	
	$("body").on("click",".moreComments", function(){
		$(this).css({"display":"none"});
		$(this).next(".lessComments").css({"display":"inline-block"});
		$(this).parent().parent().children(".commntList").children("ul").children("li").children(".blogcommnt").children(".commntList").slideDown(200);
		$(this).parent().parent().children(".commntList").children("ul").children("li").slideDown(200);
	});
	$("body").on("click",".lessComments", function(){
		$(this).css({"display":"none"});
		$(this).prev(".moreComments").css({"display":"inline-block"});
		$(this).parent().parent().children(".commntList").children("ul").children("li").children(".blogcommnt").children(".commntList").slideUp(200);
		$(this).parent().parent().children(".commntList").children("ul").children("li").not(".commntList ul li:first-child, .commntList ul li:nth-child(2)").slideUp(200);
	});
});

//tag
$(function(){
	var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];

	/* $('#singleFieldTags2').tagit({
		allowSpaces: true,
		availableTags: sampleTags
	}); */
});