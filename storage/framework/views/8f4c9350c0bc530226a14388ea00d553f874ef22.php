<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('content'); ?>
<section class="mainbody detailspageMiddle clear">
<?php echo $__env->make('include.left_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!--middle open-->
<div class="middlecol">
	
	<div class="contentmiddle">
		<div class="middlerow ansPage">
			<div class="ansQues">
				<div class="questionHead clear">
					<div class="middleleft">
						<h3 class="descqs"><a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i><?php echo $question->title;?></a></h3>
					</div>
					<div class="middleright">
						<div class="topsmall">
							<span><?php echo e(time_elapsed_string(strtotime($question->created_at))); ?></span>
							<span>About <a href="#">Startup</a></span>
						</div>
					</div>
				</div>
				<div class="anstopuser clear">
					<div class="usertop">
						<figure>
							<img src="<?php echo asset('assets'); ?><?php echo e($question->questionUser->profile_image!=''?'/upload/profile_image/'.$question->questionUser->profile_image:'/frontend/images/userthumb.png'); ?>" width="47" height="47" alt=""/>
						</figure>
						<span><a href="<?php echo e(url('profile/'.$question->questionUser->id)); ?>"><?php echo e(($question->questionUser->nickname)); ?></a></span>
						<i class="fa fa-users" aria-hidden="true"></i>
					</div>
					<!--<ul class="question-tags">
						<li><a href="javascript:void(0);" class="bluebg givAns">Answer</a></li>
						<li><a href="#" class="pinkbg">Pass</a></li>
						<li><a href="#" class="yellowbg">Forward</a></li>
					</ul>-->
				</div>
				<div class="answertop clear">
					<div class="middleleft">
						<p class="des-text">
						<?php echo nl2br(substr($question->content,0,200));
						if(strlen($question->content)>200){
						?>
						<span class="addDesc">
							<?php echo nl2br($question->content);?>
						</span>
						<a href="javascript:void(0);" class="SeeAll">More...</a>
						<?php }?>
						<p>
					</div>
					<div class="middleright">
						<ul class="countsec">
							<li>
								<span class="circle"><?php echo count($question->qsn_views); ?></span>
								<span class="txtn">Views</span>
							</li>
							<li>
								<span class="circle"><?php echo count($question->answers); ?></span>
								<span class="txtn">Answers</span>
							</li>
							<li>
								<span class="circle">
								<?php $cnt=0;
								if(!empty($question->answers))
								{
									foreach($question->answers as $val)
									{
										$cnt+= count($val->upvotes);
									}
								}
								echo $cnt;
								?>
								</span>
								<span class="txtn">Votes</span>
							</li>
						</ul>
					</div>
				</div>
				<a href="javascript:void(0);" class="vwans"></a>
				<ul class="question-tags lrgScreen">
					<li><a href="javascript:void(0);" class="bluebg givAns">Answer</a></li>
					<li><a href="#askpopup" class="pinkbg rqstbtn">Request<input type="hidden" value="<?php echo e($question->id); ?>"/></a></li>
					<?php if($question->question_attached!=''): ?>
					<li><a href="<?php echo asset('assets/upload/question_attached/'.$question->question_attached); ?>" class="bluebg" download>Download <?php echo substr($question->question_attached,10,(strlen($question->question_attached) )); ?></a></li>
					<?php endif; ?>
				</ul>
				<ul class="question-tags smlScreen">
					<li><a href="javascript:void(0);" class="bluebg givAns">Answer</a></li>
					<li><a href="#askpopup" class="pinkbg rqstbtn">Request<input type="hidden" value="<?php echo e($question->id); ?>"/></a></li>
					<?php if($question->question_attached!=''): ?>
					<li><a href="<?php echo asset('assets/upload/question_attached/'.$question->question_attached); ?>" class="bluebg" download>Download Attached</a></li>
					<?php endif; ?>
				</ul>
			</div>
			<div class="ansbot">
				<div class="yourans">
					<form id="ansFrm" method="post" action="<?php echo e(url('user/answer-process')); ?>">
						<?php echo csrf_field(); ?>

						<div class="anshead clear">
							<figure><a href="#"><img src="<?php echo asset('assets'); ?><?php echo e($user->profile_image!=''?'/upload/profile_image/'.$user->profile_image:'/frontend/images/userthumb.png'); ?>" alt=""></a></figure>
							<div class="ansdtl">
								<h4><a href="#"><?php echo $user->nickname;?></a></h4>
							</div>
						</div>
						<div class="typeans">
							<textarea id="anscontent" name="content" class="texteditor" required ></textarea>
							<div class="rightbtn">
								<input type="submit" value="submit" class="bluebtn">
								<input type="hidden" name="question_id" value="<?php echo $question->id;?>" />
							</div>
						</div>
					</form>
				</div>
				<div class="answerlist nextans">
					<h3>Answers <span>(<?php echo e(count($question->answers)); ?>)</span></h3>
					<ul>
					<?php if(!empty($question->answers)): ?>
						
						<?php foreach($question->answers as $answer): ?>
						<li class="ansbox">
							<div class="anshead clear">
								<figure><a href="#"><img src="<?php echo asset('assets'); ?><?php echo e($answer->answerUser->profile_image!=''?'/upload/profile_image/'.$answer->answerUser->profile_image:'/frontend/images/userthumb.png'); ?>" alt=""></a></figure>
								<div class="ansdtl">
									<h4><a href="#"><?php echo e($answer->answerUser->nickname); ?></a></h4>
								</div>
								<div class="blockArrow">
									<ul>
										<?php if($answer->user_id == Auth::User()->id): ?>
										<li><a href="javascript:void(0);" class="edtAns"><img src="<?php echo asset('assets/frontend'); ?>/images/pencil2.png" alt=""><span>Edit</span></a></li>
										<?php endif; ?>
										<li><a href="javascript:void(0);"><img src="<?php echo asset('assets/frontend'); ?>/images/a2.png" alt=""><span>report</span></a></li>
									</ul>
								</div>	
							</div>					
							<div class="ans_sec" style="display:none;">
								<form method="post" action="<?php echo e(url('user/answer-process')); ?>">
									<?php echo csrf_field(); ?>

									<textarea name="content" class="texteditor" required ><?php echo $answer->content; ?></textarea>
									<div class="rightbtn">
										<input type="submit" value="Update" class="bluebtn">
										<input type="hidden" name="ans_id" value="<?php echo $answer->id;?>" />
									</div>
								</form>
							</div>
							<div class="ansSec">
								<div class="ansbody">
									<?php echo $answer->content; ?>	
								</div>
									<div class="allAns"><span class="postTime"><?php echo e(time_elapsed_string(strtotime($answer->created_at))); ?></span>
									<?php if(strlen($answer->content)>1123): ?>
									<a href="javascript:void(0);" class="ansFull"></a>
									<?php endif; ?>
								</div>
							</div>
							<div class="cmdbox">
								<div class="cmdhead clear">
									<ul class="votelink">
										<li class="upvote <?php echo e(($answer->user_id==$user->id)?'noVote':''); ?>"><a href="javascript:void(0);" class="bluebg">Upvote<span><?php echo count($answer->upvotes); ?></span><input type="hidden" value="<?php echo e($answer->id); ?>"/></a></li>
										<li class="downvote <?php echo e(($answer->user_id==$user->id)?'noVote':''); ?>"><a href="javascript:void(0);" class="pinkbg">Downvote<span><?php echo count($answer->downvotes); ?></span><input type="hidden" value="<?php echo e($answer->id); ?>"/></a></li>
										<li><a href="javascript:void(0);" class="yellowbg cmntbtn">Comment<span><?php echo e(count($answer->all_comments)); ?></span></a></li>
									</ul>
									<ul class="socialans">
										<li><a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
										<li><a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
										<li><a href=""><i class="fa fa-share-square" aria-hidden="true"></i></a></li>
										<li><a href=""><i class="fa fa-plus" aria-hidden="true"></i></i></a></li>
									</ul>
								</div>
								<div class="openComment">
									<div class="fbComment">
										<div class="smallCommnt clear">
											<figure><img src="<?php echo asset('assets/frontend'); ?>/images/userthumb.png" alt=""></figure>
											<div class="blogcommnt">
												<form class="commentfrm" method="POST">
													<textarea name="comment" placeholder="Write a comment..."></textarea>
													<input name="answer_id" type="hidden" value="<?php echo e($answer->id); ?>"/>
													<input name="parent_id" type="hidden" value="0"/>
													<button type="submit" value="submit"><i class="fa fa-reply-all" aria-hidden="true"></i><span>Reply</span></button>
												</form>
											</div>
											
										</div>
										<div class="commntList parentComment">
											<ul>
										<?php if(count($answer->comments)>0): ?>
										<?php echo reply_comment($answer->comments,$answer,''); ?>

										<?php endif; ?>
											</ul>
										</div>
										<span class="moreText">
											<a href="javascript:void(0);" class="moreComments">More Comments</a>
											<a href="javascript:void(0);" class="lessComments">Less Comments</a>
										</span>
									</div>
									
								</div>
							</div>
						</li>
						<?php endforeach; ?>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!--middle close-->
        
<?php echo $__env->make('include.right_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('customScript'); ?>
<script src="<?php echo asset('assets/frontend'); ?>/ckeditor/ckeditor.js"></script>
<script> 
$(document).ready(function() {
	CKEDITOR.replaceClass = 'texteditor';
	
	$('.newcomment').click(function(){
		$(this).parent('li').parent('ul').parent('.bottombox').next('.anscomment').slideToggle(200);
	});
	
	//
	$('.SeeAll').click(function(){
		$(this).prev('.addDesc').slideToggle(200);
		$(this).toggleClass('active');
		$(this).parent('.des-text').parent('.middleleft').parent('.answertop').toggleClass('fullbox');
	});
	
	//
	$('.cmntbtn').click(function(){
		$(this).parent('li').parent('ul').parent('.cmdhead').parent('.cmdbox').children('.openComment').slideToggle(200);
	});
		
	//
	$('.givAns').click(function(){
		$('.ansbot').children('.yourans').slideToggle(200);
	});
	$('.ansFull').click(function(){
		$(this).parent('.allAns').prev('.ansbody').toggleClass('allShow');
		$(this).toggleClass('active');
	});
	//
	$('.ansPage').prepend('<div class="manageheight" style="height: 0px;"></div>');
	$('.vwans').click(function(){
		$(this).prev('.answertop').slideToggle(200);
		$(this).toggleClass('active');
		$('.smlScreen').slideToggle(100);
		$('.fixedans .questionHead').slideToggle(100);
		$('.fixedans .usertop').slideToggle(100);
	});
	
	//
	$(document).on('click','.replayForm',function() {
		if(!$(this).parent('span').parent('.blogmeta').next('.smallreplay').is(":visible")){
			$('.smallreplay').slideUp(200);
			$(this).parent('span').parent('.blogmeta').next('.smallreplay').slideDown(200);
		}else{
			$(this).parent('span').parent('.blogmeta').next('.smallreplay').slideUp(200);
		}
	});
	$('.closeBox').click(function() {
		$(this).parent('.smallreplay ').slideUp(200);
	});
	$('#ansFrm').click(function(e) {
		e.preventDefault();
		var textbox_data = CKEDITOR.instances['anscontent'].getData();
		
		if(textbox_data.replace(/(<([^>]+)>)/ig,"") != "")
		{
			$(this).submit();
		}
	});
	
	$(document).on( "submit","form.commentfrm", function( event ) {
	
		event.preventDefault();
		
		var formdata =  $( this ).serializeArray();
		var fobj = $(this);

		$.ajax({
			type:"post",
			url: "<?php echo url('user/comment-process'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: formdata,			
			dataType: "json",			
			success:function(res) {
				console.log(res);
				if(res.comment)
				{
					var cur_comnt = res.comment.comment;
					cur_comnt = cur_comnt.replace(/(?:\r\n|\r|\n)/g, '<br />');
					var chtml = '<li>'
								+'<figure><img src="<?php echo asset('assets/frontend'); ?>/images/userthumb.png" alt=""></figure>'
								+'<div class="blogcommnt">'
									+'<h4 class="blgTtl"><a href="">'+res.nickname+'</a></h4>'
									+'<div class="blogTxts"><p>'+cur_comnt+'</p><span class="mr">More...</span></div>'
									+'<div class="blogmeta">'
										+'<span>0 <a href=""><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <span>Like</span></a></span>'
										+'<span>1 <a href="javascript:void(0);" class="replayForm"><i class="fa fa-reply" aria-hidden="true"></i> <span>Reply</span></a></span>'
										+'<span>'+res.created_at+'</span>'
									+'</div>'
									+'<div class="smallCommnt smallreplay clear">'
										+'<figure><img src="<?php echo asset('assets/frontend'); ?>/images/userthumb.png" alt=""></figure>'
										+'<div class="blogcommnt">'
											+'<form class="commentfrm" method="POST">'
												+'<textarea name="comment" placeholder="Write a comment..."></textarea>'
												+'<input name="answer_id" type="hidden" value="'+res.comment.answer_id+'"/>'
												+'<input name="parent_id" type="hidden" value="'+res.comment.id+'"/>'
												+'<button type="submit" value="submit"><i class="fa fa-reply-all" aria-hidden="true"></i><span>Reply</span></button>'
											+'</form>'
										+'</div>'
									+'</div><div class="commntList" style="display:block;"><ul></ul></div>'
								+'</div>'
							+'</li>';
					fobj.parent().parent().next('.commntList').children('ul').prepend(chtml);		
					fobj.find("textarea").val("");
				}
			}
		});
		
	});
	
	$(document).on('click','.upvote a',function(){
		var obj = $(this);
		var ans_id = obj.children('input').val();
		upvoteDownvote(obj,ans_id,'Y');
	});
	$(document).on('click','.downvote a',function(){
		var obj = $(this);
		var ans_id = obj.children('input').val();
		upvoteDownvote(obj,ans_id,'N');
	});
	
	function upvoteDownvote(obj,ans_id,status)
	{
		$.ajax({
			type:"post",
			url: "<?php echo url('user/upvote-downvote'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {'ans_id':ans_id,'status':status},			
			dataType: "json",			
			success:function(res) {
				/* console.log(res); */
				if(res)
				{			
					obj.parent().parent().children('li.upvote').children('a').children('span').html(res.upvotes);
					obj.parent().parent().children('li.downvote').children('a').children('span').html(res.downvotes);
				}
					
			}
		});
	}
	$(document).on('click','.cmtLike',function (){
		var cur_obj = $(this);
		var cmnt_id = cur_obj.attr('cmid');
		$.ajax({
			type:"post",
			url: "<?php echo url('user/like-comment'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {'cmnt_id':cmnt_id},			
			dataType: "json",			
			success:function(res) {
				/* console.log(res); */
				if(res)
				{		
					cur_obj.parent('span').find('font').text(res);
				}
					
			}
		});
	});
	
	$('.rqstbtn').click(function(){
		$('.success_message').hide();
		$('#tglist').html('');
		$('#qsnFrm').find("input[type=text], select, textarea").val("");
		var qobj = $(this);
		var qid = qobj.children('input').val();
		//alert(qid);
		$.ajax({
			type:"post",
			url: "<?php echo url('user/get-question'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {'qid':qid},			
			dataType: "json",			
			success:function(res) {
				//console.log(res);
				if(res)
				{				
					/* $.fancybox.open([
						{
							//type: 'iframe',
							href : '#tospopup',   
						}
					], {
						padding : 0
					});	 */	
						
					//$('#askbtn').fancybox().trigger('click');	
					$('#asktlt').text('Request More');		
					$('#sec1,#sec2,#sec3,#sec4,#sec5,#sec6,#sec7').find("input[type=text], select, textarea").prop('disabled', true);			
					$('#sec1,#sec2,#sec3,#sec4,#sec5,#sec6,#sec7').css('display','none');
					$('#secEmail').find("input[type=text], select, textarea").prop('disabled', false);
					$('#secEmail').show();
					$('#inpids').val('');		
					$('#emails').val('');
					
					$('#qsn_id').val(res.question.id);
				} 
				
			}
		});
	});

	$('.rqstbtn').fancybox({
		padding:0,
		afterShow: function(){
			$(".viewansbox").mCustomScrollbar();
		},
		afterClose: function() {
		}
	});
	
});

$(document).scroll(function() {
	var xx = $('.fixedans .ansQues').outerHeight();
	var th = $('#mainheader').height();
	if($(this).scrollTop() > th){
	  $(".ansPage").addClass("fixedans"); 
	  $(".manageheight").addClass('noheight');
	} 
	else{
	  $(".ansPage").removeClass("fixedans");
	   $(".manageheight").removeClass('noheight');
	   $('.answertop').removeAttr('style');
	   $('.vwans').removeAttr('style');
	   $('.question-tags').removeAttr('style');
	   $('.vwans').removeClass('active');
	   $('.fixedans .anstopuser .question-tags').removeAttr('style');
	   $('.SeeAll').removeClass('active');
	   $('.answertop').removeClass('fullbox');
	   $('.addDesc').removeAttr('style');
	   $('.questionHead').removeAttr('style');
	   $('.usertop').removeAttr('style');
	}
	$('.manageheight').height(xx);
});

$('body').on('click','.edtAns',function() {
	var cobj = $(this);
	//$('.ans_sec').slideUp(200);
	//$('.ansSec').slideDown(200);
	var ans_sec = cobj.parent().parent().parent().parent().parent().children('.ans_sec');
	var ansSec = cobj.parent().parent().parent().parent().parent().children('.ansSec');
	ans_sec.slideToggle(200);
	ansSec.slideToggle(200);
	
});

$(window).load(function(){
	$('body').on('click','.blogTxts .mr',function() {
		$(this).parent().children("p").css({"max-height":"none"});
		$(this).addClass("ls");
		$(this).html("less");
	});
	$('body').on('click','.blogTxts .mr.ls',function() {
		$(this).parent().children("p").css({"max-height":"222px"});
		$(this).removeClass("ls");
		$(this).html("more...");
	});
	
});


</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>