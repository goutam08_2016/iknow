<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('content'); ?>
<section class="mainbody clear">
<?php echo $__env->make('include.left_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="middlecol listQuestion singleQs">
        	
	<div class="topfilter clear">
		<div class="filter-left">
			<span>Question Type</span>
			<select>
				<option>All</option>
				<option>Quick Chat</option>
				<option>Detailed Discussion</option>
			</select>
		</div>
		<div class="filter-right">
			<span>Status</span>
			<select>
				<option>Unanswered</option>
				<option>Answered</option>
			</select>
		</div>
	</div>
	<div class="pTab nextF">
		<div class="filterBtn tabmenu">
			<ul>
				<li class="active"><a href="javascript:void(0);">Sent Question</a></li>
				<li><a href="<?php echo e(url('user/received-question')); ?>">Received Question</a></li>
			</ul>
		</div>
	</div>
	<div class="contentmiddle qsPage">
		<?php if(count($quest_data)>0): ?>
		<?php foreach($quest_data as $one_quest): ?>
		<div class="middlerow clear">
			<div class="middleleft">
				<h3 class="descqs"><a href="<?php echo e(url('user/question-details/'.$one_quest->id)); ?>"><i class="fa fa-file-text-o" aria-hidden="true"></i> <?php echo e($one_quest->title); ?></a></h3>
				<div class="ansbody "><p class="des-text "><?php echo nl2br($one_quest->content); ?></p></div>
				<div style="float:right;">
					<?php if(strlen($one_quest->content)>500): ?>
						<a href="javascript:void(0);" class="ansFull" style="float:right;"></a>
					<?php endif; ?>
				</div>
				<div class="mobileCount">
					<div class="topsmall">
						<span>1 day ago</span>
						<span>About <a href="javascript:void(0);" class="qpop">Startup</a></span>
					</div>
					<div class="noDesc">
						<div class="retail">
							<span class="noLext">To</span>
							<ul>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
								<li>Chris Lockwood - Lorem</li>
							</ul>
							<a href="javascript:void(0);" class="moreTo">View All</a>
						</div>
					</div>
					<ul>
						<li><i class="fa fa-eye" aria-hidden="true"></i> <span><?php echo count($one_quest->qsn_views); ?></span> views</li>
						<li><i class="fa fa-comments-o" aria-hidden="true"></i> <span><?php echo count($one_quest->answers); ?></span> answers</li>
						<li><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <span>
						<?php $cnt=0;
							if(!empty($one_quest->answers))
							{
								foreach($one_quest->answers as $val)
								{
									$cnt+= count($val->upvotes);
								}
							}
							echo $cnt;
							?>
						</span> votes</li>
					</ul>
				</div>
				<a class="readans" href="javascript:void(0);">Read <?php echo e(count($one_quest->answers)); ?> answers</a>
				<div class="viewqsbox">
				<?php if(!empty($one_quest->answers)): ?>						
					<?php foreach($one_quest->answers as $answer): ?>
					<div class="qsbox clear">
						<figure><img src="<?php echo asset('assets'); ?><?php echo e($one_quest->questionUser->profile_image!=''?'/upload/profile_image/'.$one_quest->questionUser->profile_image:'/frontend/images/userthumb.png'); ?>" alt=""></figure>
						<h4><a href=""><?php echo e($answer->answerUser->nickname); ?></a></h4>
						<div class="ansbody nwd"><p><?php echo $answer->content; ?> </p></div>
						
						<div class="pstTime">
						<?php if(strlen($answer->content)>240): ?>
						<a href="javascript:void(0);" class="ansFull" style="float:right;"></a>
						<?php endif; ?>
						<?php echo e(time_elapsed_string(strtotime($answer->created_at))); ?></div>
					</div>
					<?php endforeach; ?>
				<?php endif; ?>
				</div>
				<ul class="question-tags">
					<li><a href="#askpopup" class="bluebg edtbtn">Edit<input type="hidden" value="<?php echo e($one_quest->id); ?>"/></a></li>
					<li><a href="javascript:void(0);" class="bluebg  black rmvqsn">Delete<input type="hidden" value="<?php echo e($one_quest->id); ?>"/></a></li>
					<li><a href="javascript:void(0);" class="yellowbg shortqspopup rmdBtn" qid="<?php echo e($one_quest->id); ?>">Remind</a></li>
					<li><a href="#askpopup" class="pinkbg rqstbtn">Request More<input type="hidden" value="<?php echo e($one_quest->id); ?>"/></a></a></li>
				</ul>
			</div>
		</div>
		<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>

<?php echo $__env->make('include.right_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</section>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('customScript'); ?>
<script>
$('.ansFull').click(function(){
		$(this).parent().prev('.ansbody').toggleClass('allShow');
		$(this).toggleClass('active');
	});
$('.rqstbtn').click(function(){
	$('.success_message').hide();
	$('#tglist').html('');
	$('#qsnFrm').find("input[type=text], select, textarea").val("");
	var qobj = $(this);
	var qid = qobj.children('input').val();
	//alert(qid);
	$.ajax({
		type:"post",
		url: "<?php echo url('user/get-question'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: {'qid':qid},			
		dataType: "json",			
		success:function(res) {
			//console.log(res);
			if(res)
			{				
				/* $.fancybox.open([
					{
						//type: 'iframe',
						href : '#tospopup',   
					}
				], {
					padding : 0
				});	 */	
					
				//$('#askbtn').fancybox().trigger('click');	
				$('#asktlt').text('Request More');		
				$('#sec1,#sec2,#sec3,#sec4,#sec5,#sec6,#sec7').find("input[type=text], select, textarea").prop('disabled', true);			
				$('#sec1,#sec2,#sec3,#sec4,#sec5,#sec6,#sec7').css('display','none');
				$('#secEmail').find("input[type=text], select, textarea").prop('disabled', false);
				$('#secEmail').show();
				$('#inpids').val(res.question.to);		
				//$('#emails').val(res.tos);
				
				$('#qsn_id').val(res.question.id);
			} 
			
		}
	});
});

$('.rqstbtn').fancybox({
	padding:0,
	afterShow: function(){
		$(".viewansbox").mCustomScrollbar();
	},
	afterClose: function() {
	}
});

$('.edtbtn').fancybox({
	padding:0,
	afterShow: function(){
		$(".viewansbox").mCustomScrollbar();
	},
	afterClose: function() {
	}
});
$('.edtbtn').click(function(){
	
	$('#tglist').html('');
	$('#sec3,#sec4').css('display','block');				
	$('#sec3,#sec4').find("input[type=text], select, textarea").prop('disabled', false);	
	$('#sec1,#sec2,#sec5,#sec6,#sec7,#secEmail').find("input[type=text], select, textarea").prop('disabled', true);
	$('#sec1,#sec2,#sec5,#sec6,#sec7,#secEmail').hide();
	$('#qsnFrm').find("input[type=text], select, textarea").val("");
	$('#qsnFrm').find("input[name=frm_type]").val("e");
	var qobj = $(this);
	var qid = qobj.children('input').val();
	//alert(qid);
	$.ajax({
		type:"post",
		url: "<?php echo url('user/get-question'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: {'qid':qid},			
		dataType: "json",			
		success:function(res) {
			//console.log(res);
			if(res)
			{
				$('#asktlt').text('Edit Question');
				//$('#askbtn').fancybox().trigger('click');				
				$('#inpids').val(res.question.to);		
				$('#emails').val(res.tos);
				$('#qsnFrm').find('select[name="post_type"]').val(res.question.post_type);
				$('#qsnFrm').find('input[name="tags"]').val(res.question.tags);
				$('#qsnFrm').find('input[name="title"]').val(res.question.title);
				$('#qsnFrm').find('textarea[name="content"]').val(res.question.content);
				$('#qsnFrm').find('input[name="qsn_id"]').val(res.question.id);
				$('#tglist').html(res.tagslist);
			} 
			//window.location.reload();
			
		}
	});	
});

$(document).on('click','.rmvqsn',function() {
	var dobj = $(this);
	var rmvid = dobj.children('input').val();
	var comfrm = confirm("Are you sure to delete");
	if(comfrm == true)
	{
		$.ajax({
			type:"post",
			url: "<?php echo url('user/del-question'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {'qid':rmvid},			
			//dataType: "json",			
			success:function(res) {
				console.log(res);
				if(res == 'true'){				
					dobj.parent().parent().parent().parent().remove();	
				}			
				
			}

		});	
		
	}
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>