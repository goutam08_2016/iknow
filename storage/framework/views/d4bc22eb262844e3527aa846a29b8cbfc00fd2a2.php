<div id="askpopup" class="answerpopup newpopup">
	<div class="answerpopup-wrap forwordForm askqsForm">
    	<h2 id="asktlt"><?php echo e(($currpage == 'topic')?'Create new thread':'Ask a question'); ?></h2>
		<?php if(session('askqsn_success')): ?>
			<div class="success_message" style="color:#cbda38;width: auto; text-align: left; display: inline-block;  float: right;">
				<p><?php echo session('askqsn_success'); ?></p>
			</div>
		<?php endif; ?>
		<div id="qMsg" class="success_message" style="display:none;"></div>
    	<form id="qsnFrm" name="qs" method="post" action="<?php echo e(url('user/post-question')); ?>" enctype="multipart/form-data">
			<?php echo e(csrf_field()); ?>

        	<div class="formrow" id="sec1">
            	<label>
					<?php if($currpage == 'topic'): ?>
					<input type="hidden" name="post_type" value="1" />
					<?php else: ?>
                	<span class="label">Post type:</span>
                    <div class="formFld">
                		<select name="post_type">
                            <option value="1">Public Discussion</option>
                        	<option value="2">Pivate question</option>
                        </select>
                    </div>
					<?php endif; ?>
                </label>
            </div>
            <div class="formrow" id="sec2">
            	<label>
                	<span class="label">From:</span>
                    <div class="formFld">
                		<span class="dontEdit"><?php echo e((Auth::check())?Auth::user()->email :''); ?></span>
                    </div>
                </label>
            </div>
            <div class="formrow" id="secEmail">
            	<label>
                	<span class="label">To: <strong>*</strong></span>
                    <div class="formFld smallAdd">
                		<input autocomplete="off" id="emails" type="text" name="users_to" value="" placeholder="" style="max-width:200px;"/>
                		<input id="inpids" type="hidden" name="to" placeholder="" />
                        <a id="qbxbtn" href="#popupAdvance" class="adSrc">Advanced Search</a>
                    </div>
                </label>
            </div>
            <div class="formrow" id="sec3">
            	<label>
                	<span class="label">Question title: <strong>*</strong></span>
                    <div class="formFld">
                		<input type="text" name="title" value="" placeholder="" />
                    </div>
                </label>
            </div>
            <div class="formrow" id="sec4">
            	<label>
                	<span class="label">Question content: <strong>*</strong></span>
                    <div class="formFld">
                		<textarea name="content"  ></textarea>
                    </div>
                </label>
            </div>
            <div class="formrow" id="sec5">
            	<label>
                	<span class="label">Tags: <strong>*</strong></span>
                    <div class="formFld">
                		<!--<input type="text" value="" placeholder="">-->
						<?php $tpic = ''; $topic_data = array();
						if($currpage == 'topic')
						{
							$tpid = Request::segment(2);
							$topic_data = \App\AllTropic::find($tpid);
						}							
						?>
                        <input id="singleFieldTags2" value="<?php echo e(!empty($topic_data)?$topic_data->title:''); ?>">
						<?php if(!empty($topic_data)): ?>
							<input name="tags[]" type="hidden" value="<?php echo e($topic_data->id); ?>"/>
						<?php endif; ?>
                        <div id="tglist"></div>
                    </div>
                </label>
            </div>
            <div class="formrow" id="sec6">
            	<span class="label attachLabel">Attach File</span>
                <div class="formFld noGap">
                    <label class="bluebtn attachBtn">
                        <input type="file" name="question_attached"/>
                        <i class="fa fa-paperclip" aria-hidden="true"></i> Attachment
                    </label>
                </div>
            </div>
            <div class="formrow" id="sec8" style="display:none">
            	<label>
                	<span class="label">Note:</span>
                    <div class="formFld">
                		<textarea name="note" disabled ></textarea>
                    </div>
                </label>
            </div>
            <div class="formrow">
            	<label class="checkstyle" id="sec7">
                	<input type="checkbox" name="send_as_anonymous" value="Y">
                    <span class="checkbox"></span>
                	Send as anonymous 
                </label>
				<div class="ansbtn textRight">
					<input type="reset" value="Discard" onClick="$.fancybox.close();" class="greybtn"/>
					<input type="submit" value="Submit Question" class="bluebtn"/>
					<input id="qsn_id" name="qsn_id" type="hidden" value="" />
					<input id="is_forword" name="is_forword" type="hidden" value="0" />
					<input id="frm_type" name="frm_type" type="hidden" value="a" />
				</div>
				<div class="spacer"></div>
            </div>
        </form>
    </div>
</div>