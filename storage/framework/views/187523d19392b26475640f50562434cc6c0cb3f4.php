<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */

$resource = 'tropic';
$resource_pl = str_plural($resource);
$page_title = ucfirst(str_replace('_', ' ', $resource));
$page_title_pl = ucfirst(str_replace('_', ' ', $resource_pl));
$locale = app()->getLocale();

?>



<?php $__env->startSection('pageTitle', $page_title); ?>

<?php $__env->startSection('content'); ?>
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Topic
            <small>Import/Export</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="<?php echo url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="<?php echo url('admin/'.$resource); ?>"><i class="fa  fa-user"></i> Topics</a></li>
            <li class="active"> Import/Export </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Import/Export Topic CSV</h3>
                    </div>
                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <?php foreach($errors->all() as $error): ?>
                                <p><?php echo $error; ?></p>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <?php if(session('success')): ?>
                        <div class="alert alert-success">
                            <?php echo session('success'); ?>

                        </div>
                    <?php endif; ?>
                    <?php if(session('error')): ?>
                        <div class="alert alert-danger">					
                           <p><?php echo session('error'); ?></p>								
                        </div>
                    <?php endif; ?>
                    <form method="POST" action="" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        <div class="box-body">							
                            <div class="form-group">	
								<label for="title" class="col-sm-2 control-label">Import Topic CSV*</label>	
								<div class="col-sm-6">	
								
									Browse <input type="file"  id="" name="topic_csv" />
								</div>									<span class="btn btn-default btn-file">
								<a href="<?php echo asset('assets/upload/topic_csv/samplecsv.csv'); ?>" download >Donload Sample</a>								</span> <!-- -->						
							</div>							
                            <div class="form-group">	
								<label for="title" class="col-sm-2 control-label">Export All Topic </label>									<span class="btn btn-default btn-file">
								<a href="<?php echo asset('assets/upload/topic_csv/tropics.csv'); ?>" download >Donload Tropics Csv</a>								</span> <!-- -->						
							</div>	
							
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a class="btn btn-default" href="<?php echo url('admin/$resource'); ?>">
                                Back</a>
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </div><!-- /.box-footer -->
                    </form>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script type="text/javascript">

    $(function () {
		
    });

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>