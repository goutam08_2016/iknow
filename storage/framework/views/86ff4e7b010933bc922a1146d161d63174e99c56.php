<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    
    <!--main content open-->  
	<section class="testquizbody">
    	<div class="testpaper-wrap">
        	<h2><?php echo e($test->title); ?></h2>
			<h3>Time Left => <span id="test_timer">&nbsp;</span></h3>
			<?php if($errors->any()): ?>
				<div class="warnings">
					<div class="error_message">
						<?php foreach($errors->all() as $error): ?>
						<p><?php echo $error; ?></p>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
            <div class="test-user"><i class="fa fa-user" aria-hidden="true"></i><?php echo e($test_eval->first_name); ?> <?php echo e($test_eval->last_name); ?></div>
            <div class="test_container">
                <div class="testpage-no">
                	<h4>Question <?php echo $atend_cnt; ?> of <?php echo count($test->questions); ?></h4>
                </div>
            	<form name="test_pepr" method="post">
				<?php echo e(csrf_field()); ?>

                <div class="testquestion">
                	<p><?php echo $question->question; ?></p>
					<?php if(!empty($question) && $question->attachment!=""): ?>
						<img src="<?php echo e(asset('assets/upload/question_attachment/'.$question->attachment)); ?>" />
					<?php endif; ?>
                </div>
                <div class="question-answer">
                    <div class="formrow">
                        <textarea name="answer" class="texteditor"></textarea>
                    </div>
                    
                    <div class="testbtnrow clear">
                        <!-- <a class="colorbtn prevbtn" href="#"><i class="fa fa-caret-left" aria-hidden="true"></i>Prev</a>--><!--this for previous page link-->
                        <a class="colorbtn nextbtn" href="javascript:$('form').submit();">Next<i class="fa fa-caret-right" aria-hidden="true"></i></a>
						<?php if($test->allow_go_back=="Y"): ?>
                        <a class="colorbtn prevbtn" href="javascript:history.back();"><i class="fa fa-caret-left" aria-hidden="true"></i>Back</a>
						<?php endif; ?>
                    </div>
                    <!--<div class="testbtnrow papersubmit">
                    	<input type="submit" value="Finish Now" class="colorbtn bluebtn">
                    </div>--><!--- this section for answer submit last page-->
                </div>
				<input type="hidden" name="ontest_id" value="<?php echo e($test_eval->id); ?>"/>
				<input type="hidden" name="test_id" value="<?php echo e($test_eval->test_id); ?>"/>
				<input type="hidden" name="question_id" value="<?php echo e(!empty($question)?$question->id:0); ?>"/>
                </form>
            </div>
        </div>
    </section>
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
<script src="<?php echo asset('assets/frontend/ckeditor/ckeditor.js'); ?>"></script>
<script> 
$(document).ready(function() {
	CKEDITOR.replaceClass = 'texteditor';
});

function quitTest() {
		//console.log(timer);
	var postdata ={};
	$.ajax({
		url: "<?php echo e(url('user/quit-test')); ?>",
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		type: 'POST',
		//dataType: 'json',
		data: postdata,
	}).done(function(response){
		console.log(response);		
		alert("Times Up!!!"); 
		window.location.reload();	
	});
}
function setDuration(timer) {
		//console.log(timer);
	var postdata ={'duration': timer};
	$.ajax({
		url: "<?php echo e(url('user/change-duration')); ?>",
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		type: 'POST',
		//dataType: 'json',
		data: postdata,
	}).done(function(response){
		//console.log(response);
	});
}
function startTimer(duration, display) {
    var timer = duration, hours, minutes, seconds;
	
    setInterval(function () {
		setDuration(timer);
        hours = parseInt(timer / 3600, 10)
        minutes = parseInt(timer / 60, 10) - (hours * 60);
        seconds = parseInt(timer % 60, 10);

        hours = hours < 10 ? "0" + hours : hours;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.text(hours + ":" +minutes + ":" + seconds);

        if (--timer < 0) {
            timer = duration;
			/* */
			quitTest();		
        }
    }, 1000);
}

jQuery(function ($) {
    var timerMnutes = parseInt(<?php echo $sess['test_duration']; ?>),
        display = $('#test_timer');
    startTimer(timerMnutes, display);
});


<?php if($test->allow_go_back=="N"): ?>
function preventBack(){window.history.forward();}
  setTimeout("preventBack()", 0);
  window.onunload=function(){null};
<?php endif; ?>
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.basic', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>