<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<link href="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.css'); ?>" rel="stylesheet" type="text/css">

<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>


	<?php echo $client_header; ?>


    	 

    <!--main content open-->

    <section class="testpage">	
		
    	<div class="deta-search"> &nbsp; </div>
    	<div class="test-deta">
			<div class="wrapper">
                <div class="questionbox">
					<?php if($errors->any()): ?>
						<div class="warnings">
							<div class="error_message">
								<?php foreach($errors->all() as $error): ?>
									<p><?php echo $error; ?></p>
								<?php endforeach; ?>
							</div>
						</div>
					<?php endif; ?>	
                	<form name="question" method="post" action="" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>

                        <div class="questionrow">
                            <h3>Question</h3>
                            <div class="questionedit">
                                <textarea name="question" class="texteditor"><?php echo e(!empty($question)?$question->question:''); ?></textarea>
                            </div>
                            <div class="fileuploded">								
                                <label class="uploadedbutton">
                                    <input name="attachment" type="file"/>
                                    <i class="fa fa-paperclip" aria-hidden="true"></i> Add Attachment
                                </label>
								
                            </div>
							<?php if(!empty($question) && $question->attachment!=""): ?>
								<img width="400" src="<?php echo e(asset('assets/upload/question_attachment/'.$question->attachment)); ?>" />
							<?php endif; ?>
                        </div>
                        <div class="questionrow nextrow">
                            <h3>Category</h3>
                            <div class="selectcategory">
                            	<select name="category">
									<option value="">Select Category</option>
									<?php if(!empty($categories)): ?>
										<?php foreach($categories as $val): ?>
										<option value="<?php echo e($val->id); ?>" <?php echo e(!empty($question) && $question->category == $val->id ?'selected':''); ?>  > <?php echo e($val->title); ?> </option>
										<?php endforeach; ?>
									<?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="questionrow nextrow">
                            <h3>Test</h3>
                            <div class="selectcategory">
                            	<select name="test" >
									<option value="">Select Test</option>
									<?php if(!empty($user->tests)): ?>
										<?php foreach($user->tests as $val): ?>
										<option value="<?php echo e($val->id); ?>" <?php echo e(!empty($question) && !empty($question->tests[0]) && $question->tests[0]->pivot->test_id == $val->id ?'selected':''); ?> > <?php echo e($val->title); ?> </option>
										<?php endforeach; ?>
									<?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="questionrow nextrow">
                            <h3>Points Available</h3>
                            <div class="pointbox" >
                            	<input type="number" name="point" min="0" value="<?php echo e(!empty($question)?$question->point:'1'); ?>">
                            </div>
                        </div>
                        <div class="buttonbox clear">
                            <input type="hidden" name="question_type" value="<?php echo e($qsn_type); ?>">
							<?php if(!empty($question)): ?>
                            <input type="hidden" name="question_id" value="<?php echo e($question->id); ?>">
							<?php endif; ?>
                            <input type="submit" value="Save">
                            <input type="reset" value="cancel">
                        </div>
                    </form>
                </div>
            </div>
        </div>
   
	</section>

    <!--main content close-->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script src="<?php echo asset('assets/frontend/js/stacktable.min.js'); ?>"></script>
<script src="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.js'); ?>"></script>
<script src="<?php echo asset('assets/frontend/ckeditor/ckeditor.js'); ?>"></script>
<script>
$(document).ready(function() {
	CKEDITOR.replaceClass = 'texteditor';
});
$(document).ready(function() {
    $('.qstable').cardtable();
	
	$('.qspopup').fancybox({
		padding:0
	});
});

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>