<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 */
$resource = 'question';
$resource_pl = str_plural($resource);
$page_title = ucfirst(str_replace('_', ' ', $resource));
$page_title_pl = ucfirst(str_replace('_', ' ', $resource_pl));

?>



<?php $__env->startSection('pageTitle', $page_title_pl . ' list'); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo e($page_title_pl); ?>

            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="<?php echo url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><?php echo e($page_title_pl); ?></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All <?php echo e($page_title); ?> list</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <?php if(session('success')): ?>
                            <div class="alert alert-success">
                                <?php echo session('success'); ?>

                            </div>
                        <?php endif; ?>
                        <table id="list_table" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><?php echo e($resource); ?></th>
                                <th style="width:300px;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(${$resource_pl} !== null): ?>
                                <?php foreach(${$resource_pl} as $row): ?>
                                    <tr>
                                        <td><?php echo $row->title; ?></td>
                                        <td>
											<a href="javascript:void(0);" rwid="<?php echo e($row->id); ?>" class="btn btn-sm btn-warning td-btn pinnedBtn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <font><?php echo e($row->is_pinned==1 ?'Pinned':'Not Pinned'); ?></font></a>
                                            <form method="POST" action="<?php echo admin_url($resource . '/' . $row->id); ?>"
                                                  onsubmit="return confirm('Are you sure to remove <?php echo $row->first_name; ?>?');">
                                                <input name="_method" type="hidden" value="DELETE"/><?php echo e(csrf_field()); ?>

                                                <button class="btn btn-sm btn-danger td-btn" type="submit"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="5">No Car maker Found..</td>
                                </tr>
                            <?php endif; ?>

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                    <div class="paginationDiv">
                        <?php echo ${$resource_pl}->render(); ?>

                    </div>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
<script type="text/javascript">
    $(function () {
        $("#list_table").DataTable({
            "paging":   false,
            "ordering": false
        });
		
		$('a.pinnedBtn').click(function(){
			var obj = $(this);
			var qid = obj.attr('rwid');
			/*alert(qid);  */
			$.ajax({
				type:"post",
				url: "<?php echo e(admin_url('question/pinned')); ?>" ,
				headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
				data: {'qid':qid},			
				dataType: "json",			
				success:function(res) {
					/* console.log(res); */
					if(res==1){
							obj.children('font').text('Pinned');
					}else	obj.children('font').text('Not Pinned');
				}
			});	
		});
		
		
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>