<?php $__env->startSection('pageTitle', 'Welcome to '); ?>
<?php $__env->startSection('seo_sec'); ?>
<?php if(!empty($cmsdata)): ?>
<title> <?php echo e($cmsdata->seo_title); ?></title>
<meta name="description" content="<?php echo e($cmsdata->seo_description); ?>">
<meta name="keywords" content="<?php echo e($cmsdata->seo_keywords); ?>">
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<?php $packduration = array('1'=>'monthly','3'=>'quarterly','6'=>'half-yearly','12'=>'yearly')?>
    <!--main content open-->
    <section class="about-top pricingpage">
    	<div class="wrapper clear">
        	<h2>Pricing table</h2>
			<div class="error_message">
				<?php
				if(!empty($current_plan))
				{
					$startd = strtotime($current_plan->pivot->created_at);
					$exp = ($current_plan->duration * 30*86400);
					$expd = (int)$startd + $exp;
				}
				?>
				<?php echo e(!empty($current_plan)? 'Package valid from '.date('d M,Y',$startd).' date to '.date('d M,Y', $expd ).' date and after '.date('d M,Y', $expd ).' date you can choose other package': ''); ?>

			</div>
			<?php if(session('upgrade_success')): ?>
				<div class="success_message">
					<p><?php echo session('upgrade_success'); ?></p>
				</div>
				<?php session()->forget('upgrade_success'); ?>
			<?php endif; ?>
			<div class="pricetable">			
				<?php if(!empty($packages)): ?>
					<?php foreach($packages as $ky=>$val): ?>
						<div class="pricebox <?php echo e((!empty($current_plan) && $current_plan->id==$val->id)?'slct':''); ?>">
							<span class="pricehead"><?php echo e($val->plan); ?></span>
							<span class="pricevalue"><?php echo e($val->price==0 ?'Free Package':'$'.$val->price); ?> / <?php echo e(!empty($packduration[$val->duration])?$packduration[$val->duration]:''); ?></span>
							<span class="pricelink">
							<?php if($val->price==0): ?>							
								<a href="javascript:void(0)" class="colorbtn free">Free</a>
							<?php else: ?>
								<?php if(Auth::check()==false): ?>
									<a href="<?php echo e(url('login')); ?>" class="colorbtn free">Upgrade</a>
								<?php else: ?>
								<form method="POST" action="<?php echo e(url('user/upgrade')); ?>">
									<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"/>
									<input type="submit" class="colorbtn" value="<?php echo e((!empty($current_plan) && $current_plan->id==$val->id)?'Current Plan':'Upgrade'); ?>" <?php echo e(!empty($current_plan)? 'disabled': ''); ?> />
									<input type="hidden" name="package_id" value="<?php echo e($val->id); ?>" />
								</form>
								<?php endif; ?>
							<?php endif; ?>
							</span>
							<ul class="pricedeta">
								<li>Lorem Ipsum is simply dummy text</li>
								<li>Lorem Ipsum is simply dummy text</li>
								<li>Lorem Ipsum is simply dummy text</li>
								<li>Lorem Ipsum is simply dummy text</li>
								<li>Lorem Ipsum is simply dummy text</li>
							</ul>
						</div>					
					<?php endforeach; ?>
				<?php endif; ?>  
			</div>
        </div>
    </section>
    <!--main content close-->    
        
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script>
$(document).ready(function() {
    
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>