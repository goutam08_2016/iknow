<?php if(count($users_data)>0): ?>
	<?php foreach($users_data as $user): ?>
	<div class="peopleBox">
		<div class="listWrap">
			<div class="topCon clear">
				<div class="conThumb">
					<?php $profImag = asset('assets/frontend/images/profile.jpg');
					if($user->profile_image !='' && file_exists('assets/upload/profile_image/'.$user->profile_image)==1)
					{
						$profImag =asset('assets/upload/profile_image/'.$user->profile_image);
					}									
					?>
					<figure>
						<a href="javascript:void(0)" class="btnText"><img src="<?php echo $profImag; ?>" width="100" height="100" alt=""></a>
					</figure>
					<span class="ageCnt"><i class="fa <?php echo e($user->gender=='M'?'fa-mars':'fa-venus'); ?>" aria-hidden="true"></i><?php echo e($user->age); ?> yrs</span>
				</div>
			</div>
				
			<div class="peopleDesc">
				<div class="chatRight">
					<label>
						<input type="checkbox" name="<?php echo e($user->email); ?>" value="<?php echo e($user->id); ?>" />
						
						<span class="checkbox"><img src="<?php echo asset('assets/frontend'); ?>/images/clr.png" alt=""></span>
					</label>
				</div>
				<div class="vMiddle">
					<div class="MiddleText">
						<h4><a href="<?php echo e(url('profile/'.$user->id)); ?>" target="_blank"><?php echo e($user->nickname); ?></a><span class="avlTxt online"><samp>available</samp></span></h4>
						
						<p><?php $we = $user->work_experiences()->orderBy('id','desc')->first();?>
						<?php if(!empty($we)): ?>
						<?php echo e($we->title); ?> - <?php echo e($we->company); ?>

						<?php endif; ?>
						</p>
						<p><?php $edu = $user->educations()->orderBy('id','desc')->first();?>
						<?php if(!empty($edu)): ?>
						<?php echo e($edu->institution); ?> - <?php echo e($edu->major); ?>

						<?php endif; ?>
						</p>
						<p><?php $live = $user->user_Addresses()->where('is_default','Y')->first();?>
						<?php if(!empty($live)): ?>
							Live In - <?php echo e($live->address); ?>

						<?php endif; ?>
						</p>
						<!-- <p>Vietin Bank - Card Center</p>
						<p>Foreign Trade University - Advanced Finance</p>
						<p>Live In - Lorem Ipsum</p> -->
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<?php endforeach; ?>
<?php endif; ?>