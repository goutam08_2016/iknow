<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	<?php echo $client_header; ?>

    <!--main content open-->
    <section class="testpage">
    	<div class="deta-search">
        	<!--<div class="wrapper clear">
            	<div class="search-box groupsearch clear">
                	<form name="search" method="get" action="">
                        <div class="formcol">
                            <input type="text" name="grp" placeholder="Search Group Name" value="<?php echo e(!empty($_GET['grp'])?$_GET['grp']:''); ?>"/>
                            <button type="submit" value="search"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </form>
                </div>
                <a href="javascript:void(0);" class="colorbtn bluebtn newtest">+ &nbsp; New Group</a>
            </div>-->
        </div>
    	<div class="test-deta">
            <div class="wrapper">
                <div class="invoicePage">
                	<a title="print" href="javascript:invoicePrint();" class="printpdf"><img src="<?php echo asset('assets/frontend/images/print.png'); ?>" alt=""/></a>
				</div>
				<table id="printInvoice">
				<tr>
				<td>
					<div class="printinvoice">
						<style>			
						@import  url(https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i);
						*{
						   font-family: 'Lato', sans-serif;
						}
						.invoicelogo {
							text-align: center;
							margin-bottom:35px;
						}
						.invoicelogo img {
							margin: 0 auto;
						}
						.invoicebox {
							width: 40%;
							float: left;
							font-size: 18px;
							line-height: 1.4;
							padding: 10px;
							border: 1px solid #ccc;
							border-radius: 7px;
						}
						.invoicebox:last-child{
							float: right;
						}
						.invoicebox h2 {
							font-size: 20px;
							font-weight: 600;
							line-height: 1;
							color: #965ace;
							margin: 0 0 13px;
						}
						.invoicebox p span{
							font-weight:400;
							float:left;
						}
						.invoicebox p em{
							display:block;
							padding-left:78px;
						}
						.invoiceDetails {
							padding-bottom: 40px;
						}
						.tableBox{
						}
						.tableBox th{
							text-align:center;
							color: #965ace;
							font-size: 20px;
							vertical-align:middle;
							background:#fafafa;
							padding:10px 10px;
							border-top:1px solid #ccc;
							border-bottom:1px solid #ccc;
						}
						.tableBox td{
							text-align:center;
							font-size: 18px;
							vertical-align:middle;
							padding:10px 10px;
							border-bottom:1px solid #ccc;
						}
						.invoicePage{
							position:relative;
						}
						.printpdf{
							position: absolute;
							right: 0;
							top:0px;
						}
						table.cstable p.ttl{
							display:inline-block;
						}
						table.cstable .assigndeta{
							width:inherit;
							padding:0 10px;
						}

						</style>
						<div class="invoicelogo">
							<img src="<?php echo asset('assets/frontend/images/logo.png'); ?>" alt=""/>
						</div>
						<div class="invoiceDetails clear">
							<div class="invoicebox">
								<h2>To</h2>
								<p><span>Name:</span><em><?php echo e($user->first_name.' '.$user->last_name); ?></em></p>
								<p><span>Address:</span><em><?php echo e($user->address); ?></em></p>
							</div>
							<div class="invoicebox">
								<h2>From</h2>
								<p><span>Name:</span><em><?php echo e($setting->contact_name); ?></em></p>
								<!--<p><span>Address:</span><em>Plot No. E2-4, Block-GP
								Sector-V, Salt Lake City
								Kolkata-700 091</em></p>-->
							</div>
						</div>
						<table width="100%">
						<tr><td height="10"></td></tr>
						</table>
						<div class="tableBox">
							<table>
								<tr>
									<th>ID</th>
									<th>Package Name</th>
									<th>Package Price</th>
									<th>Date of Subscription</th>
									<th>End Date</th>
								</tr>
								<tr>
									<?php
									if(!empty($invoice))
									{
										$startd = strtotime($invoice->pivot->created_at);
										$exp = ($invoice->duration * (30)*86400);
										$expd = (int)$startd + $exp;
									?>
									<td>#<?php echo e($invoice->pivot->id); ?></td>
									<td><?php echo e($invoice->plan); ?></td>
									<td>$<?php echo e($invoice->price); ?></td>
									<td><?php echo e(date('d/m/Y',$startd)); ?></td>
									<td><?php echo e(date('d/m/Y',$expd)); ?></td>
									
									<?php
									}
									?>
								</tr>
							</table>
						</div>
					</div>				
				</td>
				</tr>
				</table>					
			</div>
         </div>
    </section>
    <!--main content close-->
    
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script>
function invoicePrint()
{
	//window.print();
	//$("#printInvoice").printElement();
	w=window.open();
	w.document.write($('#printInvoice').html());
	w.print();
	w.close();
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>