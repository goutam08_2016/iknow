<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<link href="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.css'); ?>" rel="stylesheet" type="text/css">

<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>



	<?php echo $client_header; ?>


    	 

    <!--main content open-->

    <section class="testpage">	
		
    	<div class="deta-search"> &nbsp; </div>
    	<div class="test-deta">
			<div class="wrapper">
                <div class="questionbox connectqs">
					<?php if($errors->any()): ?>
						<div class="warnings">
							<div class="error_message">
								<?php foreach($errors->all() as $error): ?>
									<p><?php echo $error; ?></p>
								<?php endforeach; ?>
							</div>
						</div>
					<?php endif; ?>	
                	<form name="question" method="post" action="" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>

                        <div class="connectquestionwrap">
                        	<div id="myList">
                                <div class="questionrow clear">
                                    <div class="connectleft clear">
                                        <div class="qsWrapn">
										<?php if(!empty($question->sub_questions) && count($question->sub_questions)>0): ?>
											<?php foreach($question->sub_questions as $i=>$val ): ?>
												<div class="qsRow clear">
													<h3>Question</h3>
													<div class="answerhead">
														<span class="ansno"><?php echo $i+1; ?></span>
													</div>
													<div class="questionedit">
														<textarea class="ckeditor" id="question_option_<?php echo $i+1; ?>" name="question_option[]" ><?php echo e(!empty($val)?$val->sub_question:''); ?></textarea>
													</div>
												</div>
											<?php endforeach; ?>
										<?php else: ?>
											<?php for( $i=0; $i<4; $i++ ): ?>
												<div class="qsRow clear">
													<h3>Question</h3>
													<div class="answerhead">
														<span class="ansno"><?php echo $i+1; ?></span>
													</div>
													<div class="questionedit">
														<textarea class="ckeditor" id="question_option_<?php echo $i+1; ?>" name="question_option[]" ></textarea>
													</div>
												</div>
											<?php endfor; ?>
										<?php endif; ?>
                                        </div>
                                        <a id="loadMore" class="colorbtn" href="javascript:void(0)">Add More Question</a>
                                    </div>
                                    <div class="connectright clear">
                                        <div class="ansWrapn">
										<?php if(!empty($question->sub_answers) && count($question->sub_answers)>0): ?>
											<?php foreach($question->sub_answers as $i=>$val ): ?>
												<div class="ansRow clear">
													<h3>Answer</h3>
													<div class="answerhead">
														<select class="correct_qsn" name="correct_qsn[]">
															<option>Please Select</option>
															<option <?php echo e(!empty($val) && $val->correct_question==1?'selected':''); ?> >1</option>
															<option <?php echo e(!empty($val) && $val->correct_question==2?'selected':''); ?> >2</option>
															<option <?php echo e(!empty($val) && $val->correct_question==3?'selected':''); ?> >3</option>
															<option <?php echo e(!empty($val) && $val->correct_question==4?'selected':''); ?> >4</option>
														</select>
													</div>
													<div class="questionedit">
														<textarea class="ckeditor" id="question_answers_<?php echo $i+1; ?>" name="question_answers[]" ><?php echo e(!empty($val)?$val->sub_answer:''); ?></textarea>
													</div>
												</div>
											<?php endforeach; ?>
										<?php else: ?>
											<?php for( $i=0; $i<4; $i++ ): ?>
												<div class="ansRow clear">
													<h3>Answer</h3>
													<div class="answerhead">
														<select class="correct_qsn" name="correct_qsn[]">
															<option>Please Select</option>
															<option>1</option>
															<option>2</option>
															<option>3</option>
															<option>4</option>
														</select>
													</div>
													<div class="questionedit">
														<textarea class="ckeditor" id="question_answers_<?php echo $i+1; ?>" name="question_answers[]" ></textarea>
													</div>
												</div>
											<?php endfor; ?>
										<?php endif; ?>
                                        </div>
                                        <a id="loadMore2" class="colorbtn" href="javascript:void(0)">Add More Answer</a>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="questionrow nextrow">
                            <h3>Category</h3>
                            <div class="selectcategory">
                            	<select name="category">
									<option value="0">Select Category</option>
									<?php if(!empty($categories)): ?>
										<?php foreach($categories as $val): ?>
										<option value="<?php echo e($val->id); ?>" <?php echo e(!empty($question) && $question->category == $val->id ?'selected':''); ?>  > <?php echo e($val->title); ?> </option>
										<?php endforeach; ?>
									<?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="questionrow nextrow">
                            <h3>Test</h3>
                            <div class="selectcategory">
                            	<select name="test" >
									<option value="0">Select Test</option>
									<?php if(!empty($user->tests)): ?>
										<?php foreach($user->tests as $val): ?>
										<option value="<?php echo e($val->id); ?>" <?php echo e(!empty($question) && !empty($question->tests[0]) && $question->tests[0]->pivot->test_id == $val->id ?'selected':''); ?> > <?php echo e($val->title); ?> </option>
										<?php endforeach; ?>
									<?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="questionrow nextrow">
                            <h3>Points Available</h3>
                            <div class="pointbox" >
                            	<input type="number" name="point" min="0" value="<?php echo e(!empty($question)?$question->point:'1'); ?>">
                            </div>
                        </div>						
						<div class="questionrow nextrow">
							<h3>Negative Point for wrong answer</h3>
							<div class="pointbox">
								<input type="number" name="negative_point" min="0" value="<?php echo e(!empty($question)?$question->negative_point:'0'); ?>">
							</div>
						</div>
                        <div class="buttonbox clear">
                            <input type="hidden" name="question_type" value="<?php echo e($qsn_type); ?>">
							<?php if(!empty($question)): ?>
                            <input type="hidden" name="question_id" value="<?php echo e($question->id); ?>">
							<?php endif; ?>
                            <input type="submit" value="Save">
                            <input type="reset" value="cancel">
                        </div>
                    </form>
                </div>
            </div>
        </div>
   
	</section>

    <!--main content close-->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script src="<?php echo asset('assets/frontend/js/stacktable.min.js'); ?>"></script>
<script src="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.js'); ?>"></script>
<script src="<?php echo asset('assets/frontend/ckeditor/ckeditor.js'); ?>"></script>
<script>
$(document).ready(function() {
	//CKEDITOR.replaceClass = 'texteditor';
	
    $('.qstable').cardtable();
	
	$('.qspopup').fancybox({
		padding:0
	});
	
	
//////////////////////////////////////////////
	CKEDITOR.config.height='100px';//ck editor 
	//question add
	
	 $("#loadMore").click(function(e) {
		 var i=4;
		var oneplus = i+1;
		var tr_object = $( ".qsRow:first-child" ).clone().appendTo( ".qsWrapn" );
		$(tr_object).find('textarea[name="question_option[]"]').attr("id", "question_option_"+oneplus+"");
		CKEDITOR.replace("question_option_"+oneplus+"");
		
		if($('.qsRow').length < 9){
		}else{
			$('#loadMore').addClass('noclick');
		}
		var n = $('.qsRow.clear').length;
		$('.qsRow:last-child .ansno').text(n++);
		
		$('#cke_question_option_1').each(function() {
			var $ids = $('[id=' + this.id + ']');
			if ($ids.length > 1) {
				$ids.not(':first').remove();
			}
		});
		$(".correct_qsn").append(' <option>'+oneplus+'</option>');
		i=i+1;
		oneplus++;
	});
	
	//answer add
	 $("#loadMore2").click(function(e) {
		 var i=4;
		var oneplus = i+1;
		var tr_objectk = $( ".ansRow:first-child" ).clone().appendTo( ".ansWrapn" );
		$(tr_objectk).find('textarea[name="question_answers[]"]').attr("id", "question_answers_"+oneplus+"");
		CKEDITOR.replace("question_answers_"+oneplus+"");
		
		if($('.ansRow').length < 9){
		}else{
			$('#loadMore2').addClass('noclick');
		}
		
		$('#cke_question_answers_1').each(function() {
			var $ids = $('[id=' + this.id + ']');
			if ($ids.length > 1) {
				$ids.not(':first').remove();
			}
		});
		
		i=i+1;
		oneplus++;
		//select option
		var t = $('.ansRow.clear').length;
		//$('.answerhead select').append('<option></option>');
		//$('.answerhead select option:last-child').text(t++);
	 });
	 
});

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>