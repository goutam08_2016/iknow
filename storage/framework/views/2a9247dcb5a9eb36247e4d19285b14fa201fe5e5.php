<section class="sidebar">
	<!-- Sidebar user panel -->
	<div class="user-panel">
		<div class="pull-left image">
			<img src="<?php echo asset('assets/admin/dist/img/avatar5.png'); ?>" class="img-circle" alt="User Image">
		</div>
		<div class="pull-left info">
			<p>Admin</p>
			<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
		</div>
	</div>
	<ul class="sidebar-menu">
		<li class="header">MAIN NAVIGATION</li>
		<li>
			<a href="<?php echo admin_url(); ?>">
				<i class="fa fa-dashboard"></i> <span>Dashboard</span>
			</a>
		</li>
		<li class="<?php echo Request::segment(2) == 'settings'?'treeview active':'treeview'; ?>">
			<a href="<?php echo admin_url('settings'); ?>">
				<i class="fa fa-cog"></i> <span>Settings</span>
			</a>
		</li>		
		<li class="treeview<?php echo Request::segment(2) == 'email-template' || Request::segment(3) == 'email-template' ? ' active' : ''; ?>">
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Email Templates</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'email-template' && Request::segment(3) == '') || Request::segment(3) == 'email-template' ? 'active' : ''; ?>">
					<a href="<?php echo admin_url('email-template'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
				<!-- <li class="<?php echo Request::segment(2) == 'email-template' && Request::segment(3) == 'create'?'active':''; ?>">
					<a href="<?php echo admin_url('email-template/create'); ?>"><i class="fa fa-plus-circle"></i> Add</a>
				</li> -->
			</ul>
		</li>
		<li class="<?php echo Request::segment(2) == 'user'?'treeview active':'treeview'; ?>">
			<a href="#">
				<i class="fa fa-user"></i>
				<span>Manage Users</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo Request::segment(2) == 'user' && Request::segment(3) == ''?'active':''; ?>">
					<a href="<?php echo admin_url('user'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
			</ul>
		</li>
		<!-- 	
		<li class="treeview<?php echo Request::segment(2) == 'package' || Request::segment(3) == 'package' ? ' active' : ''; ?>">
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Manage Pricing</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'package' && Request::segment(3) == '') || Request::segment(3) == 'package' ? 'active' : ''; ?>">
					<a href="<?php echo admin_url('package'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
				<li class="<?php echo Request::segment(2) == 'package' && Request::segment(3) == 'create'?'active':''; ?>">
					<a href="<?php echo admin_url('package/create'); ?>"><i class="fa fa-plus-circle"></i> Add</a>
				</li>
			</ul>
		</li> -->

		<li class="treeview<?php echo Request::segment(2) == 'question' || Request::segment(3) == 'question' ? ' active' : ''; ?>">	
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Question</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'question' && Request::segment(3) == '') || Request::segment(3) == 'question' ? 'active' : ''; ?>">
					<a href="<?php echo admin_url('question'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
			</ul>			
		</li>
		
		<li class="treeview<?php echo Request::segment(2) == 'categories' || Request::segment(3) == 'categories' ? ' active' : ''; ?>">		
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Manage Blog Categories</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>

			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'categories' && Request::segment(3) == '') || Request::segment(3) == 'categories' ? 'active' : ''; ?>">

					<a href="<?php echo admin_url('categories'); ?>"><i class="fa fa-circle-o"></i> List</a>

				</li>
				<li class="<?php echo Request::segment(2) == 'categories' && Request::segment(3) == 'create'?'active':''; ?>">

					<a href="<?php echo admin_url('categories/create'); ?>"><i class="fa fa-plus-circle"></i> Add</a>

				</li>
				
			</ul>			
		</li>
		<li class="treeview<?php echo Request::segment(2) == 'blog' || Request::segment(3) == 'blog' ? ' active' : ''; ?>">		
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Manage Blog</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>

			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'blog' && Request::segment(3) == '') || Request::segment(3) == 'blog' ? 'active' : ''; ?>">

					<a href="<?php echo admin_url('blog'); ?>"><i class="fa fa-circle-o"></i>User Blogs</a>

				</li>
				<li class="<?php echo (Request::segment(2) == 'blog' && Request::segment(3) == 'admin-blog') ? 'active' : ''; ?>">

					<a href="<?php echo admin_url('blog/admin-blog'); ?>"><i class="fa fa-circle-o"></i>Admin Blogs</a>

				</li>
				<li class="<?php echo Request::segment(2) == 'blog' && Request::segment(3) == 'create'?'active':''; ?>">

					<a href="<?php echo admin_url('blog/create'); ?>"><i class="fa fa-plus-circle"></i> Add</a>

				</li>
			</ul>			
		</li>
		<li class="treeview<?php echo Request::segment(2) == 'degree' || Request::segment(3) == 'degree' ? ' active' : ''; ?>">		
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Manage Degree</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>

			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'degree' && Request::segment(3) == '') || Request::segment(3) == 'degree' ? 'active' : ''; ?>">

					<a href="<?php echo admin_url('degree'); ?>"><i class="fa fa-circle-o"></i> List</a>

				</li>
				<li class="<?php echo Request::segment(2) == 'degree' && Request::segment(3) == 'create'?'active':''; ?>">

					<a href="<?php echo admin_url('degree/create'); ?>"><i class="fa fa-plus-circle"></i> Add</a>

				</li>
			</ul>			
		</li>
		<li class="treeview<?php echo Request::segment(2) == 'group' || Request::segment(3) == 'group' ? ' active' : ''; ?>">		
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Manage Groups of Topics</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>

			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'group' && Request::segment(3) == '') || Request::segment(3) == 'group' ? 'active' : ''; ?>">

					<a href="<?php echo admin_url('group'); ?>"><i class="fa fa-circle-o"></i> List</a>

				</li>
				<li class="<?php echo Request::segment(2) == 'group' && Request::segment(3) == 'create'?'active':''; ?>">

					<a href="<?php echo admin_url('group/create'); ?>"><i class="fa fa-plus-circle"></i> Add</a>

				</li>
			</ul>			
		</li>

		<li class="treeview<?php echo Request::segment(2) == 'tropic' || Request::segment(3) == 'tropic' ? ' active' : ''; ?>">		
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Manage Topics</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>

			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'tropic' && Request::segment(3) == '') || Request::segment(3) == 'tropic' ? 'active' : ''; ?>">

					<a href="<?php echo admin_url('tropic'); ?>"><i class="fa fa-circle-o"></i> List</a>

				</li>
				<li class="<?php echo Request::segment(2) == 'tropic' && Request::segment(3) == 'create'?'active':''; ?>">

					<a href="<?php echo admin_url('tropic/create'); ?>"><i class="fa fa-plus-circle"></i> Add</a>

				</li>
				<li class="<?php echo Request::segment(2) == 'topic' && Request::segment(3) == 'add-csv'?'active':''; ?>">

					<a href="<?php echo admin_url('topic/add-csv'); ?>"><i class="fa fa-plus-circle"></i> Import/Export CSV</a>

				</li>
			</ul>			
		</li>
		
		<li class="treeview<?php echo Request::segment(2) == 'csv' ? ' active' : ''; ?>">		
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Export/Import CSV</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>

			<ul class="treeview-menu">
				<li class="<?php echo Request::segment(2) == 'csv' && Request::segment(3) == 'company-csv'?'active':''; ?>">

					<a href="<?php echo admin_url('csv/company-csv'); ?>"><i class="fa fa-plus-circle"></i> Import/Export Company</a>

				</li>
				<li class="<?php echo Request::segment(2) == 'csv' && Request::segment(3) == 'job-csv'?'active':''; ?>">

					<a href="<?php echo admin_url('csv/job-csv'); ?>"><i class="fa fa-plus-circle"></i> Import/Export Job</a>

				</li>
				<li class="<?php echo Request::segment(2) == 'csv' && Request::segment(3) == 'school-csv'?'active':''; ?>">

					<a href="<?php echo admin_url('csv/school-csv'); ?>"><i class="fa fa-plus-circle"></i> Import/Export School</a>

				</li>
				<li class="<?php echo Request::segment(2) == 'csv' && Request::segment(3) == 'major-csv'?'active':''; ?>">

					<a href="<?php echo admin_url('csv/major-csv'); ?>"><i class="fa fa-plus-circle"></i> Import/Export Major</a>

				</li>
			</ul>			
		</li>
		<li class="treeview<?php echo Request::segment(2) == 'cms-page' || Request::segment(3) == 'cms-page' ? ' active' : ''; ?>">
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>CMS Pages</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'cms-page' && Request::segment(3) == '') || Request::segment(3) == 'cms-page' ? 'active' : ''; ?>">
					<a href="<?php echo admin_url('cms-page'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
				<!--<li class="<?php echo Request::segment(2) == 'cms-page' && Request::segment(3) == 'create'?'active':''; ?>">
					<a href="<?php echo admin_url('cms-page/create'); ?>"><i class="fa fa-plus-circle"></i> Add</a>
				</li>-->
			</ul>
		</li>
		
		<!--<li class="treeview<?php echo Request::segment(2) == 'adsences' || Request::segment(3) == 'adsences' ? ' active' : ''; ?>">
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Adsences</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'adsences' && Request::segment(3) == '') || Request::segment(3) == 'adsences' ? 'active' : ''; ?>">
					<a href="<?php echo admin_url('adsences'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
				<li class="<?php echo Request::segment(2) == 'adsences' && Request::segment(3) == 'create'?'active':''; ?>">
					<a href="<?php echo admin_url('adsences/create'); ?>"><i class="fa fa-plus-circle"></i> Add</a>
				</li>
			</ul>
		</li>-->
		
		<li class="treeview<?php echo Request::segment(2) == 'company' || Request::segment(3) == 'company' ? ' active' : ''; ?>">	
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Company</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'company' && Request::segment(3) == '') || Request::segment(3) == 'company' ? 'active' : ''; ?>">
					<a href="<?php echo admin_url('company'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
			</ul>			
		</li>
		<li class="treeview<?php echo Request::segment(2) == 'job' || Request::segment(3) == 'job' ? ' active' : ''; ?>">	
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Job</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'job' && Request::segment(3) == '') || Request::segment(3) == 'job' ? 'active' : ''; ?>">
					<a href="<?php echo admin_url('job'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
			</ul>			
		</li>
		<li class="treeview<?php echo Request::segment(2) == 'school' || Request::segment(3) == 'school' ? ' active' : ''; ?>">	
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>School</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'school' && Request::segment(3) == '') || Request::segment(3) == 'school' ? 'active' : ''; ?>">
					<a href="<?php echo admin_url('school'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
			</ul>			
		</li>
		<li class="treeview<?php echo Request::segment(2) == 'major' || Request::segment(3) == 'major' ? ' active' : ''; ?>">	
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Major</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'major' && Request::segment(3) == '') || Request::segment(3) == 'major' ? 'active' : ''; ?>">
					<a href="<?php echo admin_url('major'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
			</ul>			
		</li>
		
	</ul>
</section>