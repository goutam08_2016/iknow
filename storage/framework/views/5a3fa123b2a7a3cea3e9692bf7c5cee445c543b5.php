<?php $__env->startSection('pageTitle', 'friend request'); ?>

<?php $__env->startSection('content'); ?>
<section class="mainbody clear">
<?php echo $__env->make('include.left_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!--middle open-->
<div class="middlecol">
	<div class="contactNext">
		<div class="contactHead">
			<div class="titlebox clear" style="margin:0;">
				<h2>My Blog</h2>
				<span class="bluebtn addBlogBtn"><i class="fa fa-plus"></i> Add New</a>
			</div>
		</div>
		<div class="addNewBlog" <?php echo (  $errors->any() ) ? 'style="display: block;"' : ''; ?>>
			<h2 id="frmTitle">Add New Blog</h2>
			
			<?php if($errors->any()): ?>
				<div id="login_warnings" class="warnings" style="">
					<div id="" class="error_message">
						<?php foreach($errors->all() as $error): ?>
							<p><?php echo $error; ?></p>
						<?php endforeach; ?>
					</div>
				</div>				
			<?php endif; ?>
			<form id="blogFrm" method="post" action="<?php echo e(url('user/blog-post')); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

				<div class="blogAddL">
					<label class="upBlogimage" id="picbg">
						<input id="picture" type="file" name="blog_image"/>
						<span class="txt"><i class="fa fa-cloud-upload"></i><br/>Upload Image</span>
					</label>
				</div>
				<div class="blogAddR">
					<div class="fieldBx">
						<select class="textField" id="blog_cat_id" name="cat_id">
							<?php if(!empty($catagories)): ?>
							<?php foreach($catagories as $cat): ?>
							<option value="<?php echo e($cat->id); ?>"><?php echo e($cat->title); ?></option>
							<?php endforeach; ?>
							<?php endif; ?>
						</select>
					</div>
					<div class="fieldBx">
						<input id="blog_title" name="title" type="text" placeholder="Title" value="" class="textField">
					</div>
					<div class="fieldBx">
						<textarea id="blog_description" name="description" placeholder="Description..." class="textField ckeditor"></textarea>
					</div>
				</div>
				<div class="spacer"></div>
				<div class="fieldBx align-right">
					<a href="javascript:void(0);" class="bluebtn grey addBlogCancel">Cancel</a>
					<input type="hidden" value="0" id="blogId" name="blog_id"/>
					<input type="submit" value="Save" class="bluebtn"/>
				</div>
			</form>
			
		</div>
		<div class="demoList myBlogList">
		<?php if(session('success')): ?>
			<div class="success_message">
				<p><?php echo session('success'); ?></p>
			</div>
		<?php endif; ?>
		<?php if(!empty($user->blogs)): ?>
			<?php foreach($user->blogs as $blog): ?>
			<div class="bloglist clear">
				<figure>
				<?php ($bimg = $blog->blog_image!="" ? $blog->blog_image :'blog.jpg'); ?>
					<a href="<?php echo e(url('user/blog-details/'.$blog->id)); ?>"><img src="<?php echo asset('assets/upload/blog_image/'.$bimg); ?>" alt=""></a>
				
				</figure>
				<div class="blogText">
					<h3><a href="<?php echo e(url('user/blog-details/'.$blog->id)); ?>"><?php echo e($blog->title); ?></a></h3>
					<div class="postMeta">
						 <ul>
							<li><a href="<?php echo e(url('user/blog-details/'.$blog->id)); ?>"><i class="fa fa-user"></i> <?php echo e(!empty($blog->blog_user)?$blog->blog_user->first_name.' '.$blog->blog_user->last_name:''); ?></a></li>
							<li><i class="fa fa-calendar"></i> <?php echo e(date('d/m/Y',strtotime($blog->created_at))); ?></li>
							<li><i class="fa fa-tags"></i> <?php echo e(!empty($blog->blog_category)?$blog->blog_category->title:''); ?></li>
							<li><a href="<?php echo e(url('user/blog-details/'.$blog->id)); ?>"><i class="fa fa-comments-o"></i> <?php echo count($blog->blog_comments); ?> comments</a></li>
							<li><i class="fa fa-eye"></i> <?php echo count($blog->blog_views); ?> views</li>
							<li><a class="blogedit" title="Edit" bid="<?php echo e($blog->id); ?>" href="javascript:void(0);"><i class="fa fa-pencil"></i> </a></li>
							<li><a class="blogdel" title="Delete" bid="<?php echo e($blog->id); ?>" href="javascript:void(0);"><i class="fa fa-trash-o"></i> </a></li>
						</ul>
					</div>
					<div class="blogDesc">
						<?php echo $blog->description; ?> 
						<br/><a href="<?php echo e(url('user/blog-details/'.$blog->id)); ?>" class="bluebtn">View Details</a>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		<?php endif; ?>
		</div>
	</div>
</div>
<!--middle close-->


<?php echo $__env->make('include.right_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('customScript'); ?>

<script>
	$(window).load(function(){
		$("body").on("click",".addBlogBtn", function(){
			
			$('#blogFrm').find("input[type=text], textarea, select").val("");
			CKEDITOR.instances['blog_description'].setData('');
			$('#frmTitle').html("Add New Blog");
			if(!$(".addNewBlog").is(":visible")){
				$(".addNewBlog").slideDown(400);	
			}
			$('#picbg').css('background','none');
		});	
		$("body").on("click",".addBlogCancel", function(){
			$(".addNewBlog").slideUp(400);
		});	
	});
	
	$("body").on("click",".blogedit", function(){
		var obj= $(this);
		var blog_id = obj.attr('bid');
		$.ajax({
			type:"post",
			url: "<?php echo url('user/get-blog'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {'blog_id':blog_id},			
			dataType: "json",			
			success:function(res) {
				if(res!=0)
				{
					if(!$(".addNewBlog").is(":visible")){
						$(".addNewBlog").slideDown(400);	
					}
					$('#frmTitle').html("Edit Blog");
					$('#blogId').val(blog_id);
					$('#blog_title').val(res.title);
					$('#blog_cat_id').val(res.cat_id);
					$('#picbg').css('background-image','url({{ asset("assets/upload/blog_image/"'+res.blog_image+') })');
					//$('#blog_description').val(res.description);
					CKEDITOR.instances['blog_description'].setData(res.description);
				}
			}
		});
			
	});	
	$("body").on("click",".blogdel", function(){
		var obj= $(this);
		var blog_id = obj.attr('bid');
		//alert(blog_id);
		var confm= confirm("Are you sure to delete the blog?");
		if(confm==true)
		{
			$.ajax({
				type:"post",
				url: "<?php echo url('user/delete-blog'); ?>" ,
				headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
				data: {'blog_id':blog_id},			
				//dataType: "json",			
				success:function(res) {
					if(res==1)
					{
						obj.parent().parent().parent().parent().parent().remove();
					}
				}
			});
		}
			
	});	
	
	$('#picture').change( function(event) { 
			//console.log(URL.createObjectURL(event.target.files[0]));
			tmppath = URL.createObjectURL(event.target.files[0]);
			$('#picbg').css('background-image','url('+tmppath+')');
	});
</script>
<script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>
<style>
#picbg{
	background-position: center !important;
    background-size: contain !important;
    background-repeat: no-repeat !important;
}
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>