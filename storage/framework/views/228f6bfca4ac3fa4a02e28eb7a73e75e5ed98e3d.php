<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	<!--body open-->
	<?php if(Auth::check()): ?>
    <section class="mainbody clear">
    	<!--left pan open-->
		<?php echo $__env->make('include.left_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    	
		<?php endif; ?>
        <!--left pan close-->
        
               

        <!--middle open-->
        <div class="<?php echo e(Auth::check()?'middlecol':'pageMiddle'); ?>"><!--  -->
        	<div class="topicSearch clear">
            	<h2>Topic</h2>
                <div class="rightSrc">
                	<form name="srch" method="get" action="">
                    	<input type="text" name="trpc" placeholder="Find Topic" value="<?php echo e(isset($_GET['trpc'])?$_GET['trpc']:''); ?>">
						
                    	<input type="hidden" name="page" value="<?php echo e(isset($_GET['page'])?$_GET['page']:1); ?>">
                    	<input type="hidden" name="tb" value="<?php echo e(isset($_GET['tb'])?$_GET['tb']:''); ?>">
						
                        <button type="submit" value="Search"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
        	<div class="topictab">
                <div class="filterBtn tabmenu">
                    <ul>
                        <li class="<?php echo e((isset($_GET['tb']) && $_GET['tb']=='al') || !isset($_GET['tb'])?'active':''); ?>"><a href="<?php echo e(url('topics')); ?>?tb=al" id="tab1">All topics</a></li>
                        <li class="<?php echo e(isset($_GET['tb']) && $_GET['tb']=='fv'?'active':''); ?>"><a href="<?php echo e(url('topics')); ?>?tb=fv" id="tab2">Favorite topics</a></li>
                    </ul>
                </div>
				
                <div class="tabContWrap">
                    <div class="topicWrap " style="display:<?php echo e((isset($_GET['tb']) && $_GET['tb']=='al') || !isset($_GET['tb'])?'block':'none'); ?>">
                    	<div class="allTopic">
                        	<div class="wrapnumber">
                                <ul class="listName caSlide">
                                	<li class="item"><a href="<?php echo e(url('topics?tb=al')); ?>">All</a></li>
									<?php foreach($alphabets as $val): ?>
                                    <li class="item"><a href="<?php echo e(url('topics?tb=al&alph='.$val)); ?>"><?php echo e($val); ?></a></li>
									<?php endforeach; ?>
                                </ul>
                            </div>
                            <div class="allList">
							<?php if(isset($_GET['alph'])): ?>
								<div class="topicList">
									<h3><?php echo e($_GET['alph']); ?></h3>
									<ul>
										<?php foreach($topics as $val): ?>
										<?php if($val->title[0]==$_GET['alph']): ?>
										<li class="<?php echo e(in_array($val->id,$user_favs)?'fav':''); ?>"><a href="<?php echo e(url('topic/'.$val->id)); ?>"><?php echo e($val->title); ?><span><?php echo e($val->tropic_questions->count()); ?></span></a>
											<?php if(Auth::check()): ?>
											<a href="javascript:void(0);" class="addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="<?php echo e($val->id); ?>"/></a>
											<?php endif; ?>
										</li>
										<?php endif; ?>
										<?php endforeach; ?>
									</ul>
								</div>
							<?php else: ?>	
								<?php if(!empty($all_data)): ?>
									<?php foreach($all_data as $itr=>$aplha): ?>
									<?php if($itr >= $start_ky && $itr <= $end_ky): ?>
										<div class="topicList">
											<h3><?php echo e($aplha->first_alpha); ?></h3>
											<ul>
												<?php foreach($topics as $val): ?>
												<?php if($val->title[0]==$aplha->first_alpha): ?>
												<li class="<?php echo e(in_array($val->id,$user_favs)?'fav':''); ?>">
													<a href="<?php echo e(url('topic/'.$val->id)); ?>"><?php echo e($val->title); ?><span><?php echo e($val->tropic_questions->count()); ?></span></a>
													<?php if(Auth::check()): ?>
													<a href="javascript:void(0);" class="addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="<?php echo e($val->id); ?>"/></a>
													<?php endif; ?>
												</li>
												<?php endif; ?>
												<?php endforeach; ?>
											</ul>
										</div>
									<?php endif; ?>
									<?php endforeach; ?>
								<?php endif; ?>
							<?php endif; ?>
								
                            </div>
                        </div>
                        <div class="pagination">
                            <ul>
                                <!--<li class="prev"><a href="">Prev</a></li>
                                <li><span>...</span></li>
                                <li class="next"><a href="">Next</a></li>
                                <li class="current"><span>1</span></li>-->
								<?php if(!empty($noof_pages) && !isset($_GET['alph'])): ?>
								<?php for( $i=1; $i <= $noof_pages; $i++ ): ?>
									<?php if(isset($_GET['page']) && $_GET['page']==$i ): ?>
									<li class="current"><span><?php echo e($i); ?></span></li>
									<?php else: ?>
									<li > <a href="<?php echo e(url('topics?tb=al&page='.$i)); ?>"><?php echo e($i); ?></a></li>
									<?php endif; ?>
								<?php endfor; ?>
								<?php endif; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="topicWrap " style="display:<?php echo e(isset($_GET['tb']) && $_GET['tb']=='fv'?'block':'none'); ?>">
                    	<div class="allTopic">
                        	<div class="wrapnumber">
                                <ul class="listName caSlide">
                                	<li class="item"><a href="<?php echo e(url('topics?tb=fv')); ?>">All</a></li>
                                    <?php foreach($alphabets as $val): ?>
                                    <li class="item"><a href="<?php echo e(url('topics?tb=fv&alph='.$val)); ?>"><?php echo e($val); ?></a></li>
									<?php endforeach; ?>
                                </ul>
                            </div>
							<?php if(Auth::check()): ?>
                            <div class="allList">
								<?php if(isset($_GET['alph'])): ?>
								<div class="topicList">
									<h3><?php echo e($_GET['alph']); ?></h3>
									<ul>
										<?php foreach($fav_topics as $val): ?>
										<?php if($val->title[0]==$_GET['alph']): ?>
										<li class="fav">
											<a href="<?php echo e(url('topic/'.$val->id)); ?>"><?php echo e($val->title); ?><span><?php echo e($val->tropic_questions->count()); ?></span></a>
											<?php if(Auth::check()): ?>
											<a href="javascript:void(0);" class="addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="<?php echo e($val->id); ?>"/></a>
											<?php endif; ?>
										</li>
										<?php endif; ?>
										<?php endforeach; ?>
									</ul>
								</div>
								<?php elseif(isset($_GET['trpc']) && !empty($fav_alphabets)): ?>
									<?php foreach($fav_alphabets as $itr=>$aplha): ?>
									<div class="topicList">
										<h3><?php echo e($aplha); ?></h3>
										<ul>
											<?php foreach($fav_topics as $val): ?>
											<?php if($val->title[0]==$aplha): ?>
											<li class="fav">
												<a href="<?php echo e(url('topic/'.$val->id)); ?>"><?php echo e($val->title); ?><span><?php echo e($val->tropic_questions->count()); ?></span></a>
												<?php if(Auth::check()): ?>
												<a href="javascript:void(0);" class="addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="<?php echo e($val->id); ?>"/></a>
												<?php endif; ?>
											</li>
											<?php endif; ?>
											<?php endforeach; ?>
										</ul>
									</div>
									<?php endforeach; ?>
								<?php else: ?>	
									<?php if(!empty($fav_alphabets)): ?>
										<?php foreach($fav_alphabets as $itr=>$aplha): ?>
											<div class="topicList">
												<h3><?php echo e($aplha); ?></h3>
												<ul>
													<?php foreach($fav_topics as $val): ?>
													<?php if($val->title[0]==$aplha && in_array($val->id,$user_favs)): ?>
													<li class="fav">
														<a href="<?php echo e(url('topic/'.$val->id)); ?>"><?php echo e($val->title); ?><span><?php echo e($val->tropic_questions->count()); ?></span></a>
														<?php if(Auth::check()): ?>
														<a href="javascript:void(0);" class="addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="<?php echo e($val->id); ?>"/></a>
														<?php endif; ?>
													</li>
													<?php endif; ?>
													<?php endforeach; ?>
												</ul>
											</div>
										<?php endforeach; ?>
									<?php endif; ?>
								<?php endif; ?>								
                            </div>
							<?php else: ?>
								<div class="allList loginReq">Please login first. <a href="<?php echo e(url('')); ?>">Click here.</a></div>
							<?php endif; ?>
                        </div>
							
                        <div class="pagination">
                            <?php /* <ul>
							 <?php if(!empty($noof_pages) && !isset($_GET['alph'])): ?>
								<?php for( $i=1; $i <= $noof_pages; $i++ ): ?>
									<?php if(isset($_GET['page']) && $_GET['page']==$i ): ?>
									<li class="current"><span><?php echo e($i); ?></span></li>
									<?php else: ?>
									<li > <a href="<?php echo e(url('topics?page='.$i)); ?>"><?php echo e($i); ?></a></li>
									<?php endif; ?>
								<?php endfor; ?>
								<?php endif; ?>
							
                            </ul>*/ ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--middle close-->
        <?php if(Auth::check()): ?>
        <!--right pan open-->
		<?php echo $__env->make('include.right_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<!--right pan close-->
	</section>
		<?php endif; ?>
    <!--body close-->

<?php $__env->stopSection(); ?>



<?php $__env->startSection('customScript'); ?>

<script>
 
$( function() {
	
});

$(document).on('click',".addF", function(e){
	e.preventDefault();
	var currentobj = $(this);
	var addFid = $(this).find('input').val();
	//alert(addFid);
	$.ajax({
		type:"post",
		url: "<?php echo url('user/add-favorite'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: {'tropic_id':addFid, },			
		//dataType: "json",			
		success:function(res) {
			console.log(res);
			var parentli = currentobj.parent('li');
			if(parentli.hasClass('fav')){
				parentli.removeClass('fav');
			}else	parentli.addClass('fav');
		}
	}); 
});

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>