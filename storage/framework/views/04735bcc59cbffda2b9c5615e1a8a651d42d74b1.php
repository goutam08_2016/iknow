<?php
//use App\Helpers\CustomHelper;
?>
<div class="popupchatbox" id="chatbox_pop_<?php echo $chat_messsage_data['to_user_id_pop']; ?>">
	<div class="onlinechat">
		<span class="title">
			<span class="uname"><span class="status<?php echo (count($chat_messsage_data['to_user_details_pop']->userLogin) > 0 && $chat_messsage_data['to_user_details_pop']->userLogin->login_status == 1)?' online':' offline'; ?>"></span><?php echo $chat_messsage_data['to_user_details_pop']->first_name; ?> <?php echo $chat_messsage_data['to_user_details_pop']->last_name; ?></span>
			<span class="onchatopncls"><i class="fa fa-minus"></i></span>
			<span class="onchatclose" data-user-id="<?php echo $chat_messsage_data['to_user_id_pop']; ?>"><i class="fa fa-close"></i></span>
		</span>
		<div class="msglist">
			<div class="chatallmsg" id="showbox_<?php echo $chat_messsage_data['to_user_id_pop']; ?>">
				<?php if(count($chat_messsage_data['all_msg_list_pop']) > 0): ?>
					<?php foreach($chat_messsage_data['all_msg_list_pop'] as $msg): ?>
						<?php if($msg->from_user_id == $chat_messsage_data['from_user_id_pop']): ?>
							<div class="list myreply">
								<?php if($msg->fromUser->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$msg->fromUser->profile_image)): ?>
									<a href="javascript:void(0);" class="photo">
										<img src="<?php echo asset('assets/upload/profile_pictures/'.$msg->fromUser->profile_image); ?>" alt="<?php echo $msg->fromUser->first_name; ?> <?php echo $msg->fromUser->last_name; ?>"/>
									</a>
			                    <?php else: ?>
			                    	<a href="javascript:void(0);" class="photo">
			                        	<img src="<?php echo asset('assets/common/images/default_avatar.jpg'); ?>" alt="<?php echo $msg->fromUser->first_name; ?> <?php echo $msg->fromUser->last_name; ?>"/>
			                        </a>
			                    <?php endif; ?>
								<span class="comment">
									<span class="type">
										<?php if($msg->type == 'T'): ?>
											<p><?php echo $msg->message; ?></p>
										<?php else: ?>
											<?php if(in_array($msg->file_ext,array('jpg','JPG','JPEG','png','PNG','bmp','BMP','gif','GIF'))): ?>
												<p><a href="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>" class="chat_pic_msg" ><img style="width:80px;" src="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>"/></a></p>
											<?php else: ?>
												<p><a href="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>" download ><?php echo $msg->message; ?></a></p>
											<?php endif; ?>
										<?php endif; ?>
									</span>
								</span>
								<small class="chattime"><?php echo date('h:i:s A',strtotime($msg->created_at)); ?></small>
							</div>
						<?php else: ?>
							<div class="list">
								<?php if($msg->fromUser->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$msg->fromUser->profile_image)): ?>
									<a href="javascript:void(0);" class="photo">
										<img src="<?php echo asset('assets/upload/profile_pictures/'.$msg->fromUser->profile_image); ?>" alt="<?php echo $msg->fromUser->first_name; ?> <?php echo $msg->fromUser->last_name; ?>"/>
									</a>
			                    <?php else: ?>
			                    	<a href="javascript:void(0);" class="photo">
			                        	<img src="<?php echo asset('assets/common/images/default_avatar.jpg'); ?>" alt="<?php echo $msg->fromUser->first_name; ?> <?php echo $msg->fromUser->last_name; ?>"/>
			                        </a>
			                    <?php endif; ?>
								<span class="comment">
									<span class="type">
										<?php if($msg->type == 'T'): ?>
											<p><?php echo $msg->message; ?></p>
										<?php else: ?>
											<?php if(in_array($msg->file_ext,array('jpg','JPG','JPEG','png','PNG','bmp','BMP','gif','GIF'))): ?>
												<p><a href="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>" class="chat_pic_msg" ><img style="width:80px;" src="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>"/></a></p>
											<?php else: ?>
												<p><a href="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>" download ><?php echo $msg->message; ?></a></p>
											<?php endif; ?>
										<?php endif; ?>
									</span>
								</span>
								<small class="chattime"><?php echo date('h:i:s A',strtotime($msg->created_at)); ?></small>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="onlinechattype" id="onlinechattype_<?php echo $chat_messsage_data['to_user_id_pop']; ?>">
		<?php if($chat_messsage_data['can_msg_pop'] == 1): ?>
			<textarea placeholder="write message" name="message" class="message"></textarea>
			<form name="upload_chat_form" id="upload_chat_form_<?php echo $chat_messsage_data['to_user_id_pop']; ?>" class="upload_chat_form" action="<?php echo url('chat-pop/uploadChatfilePop'); ?>" method="post" enctype="multipart/form-data">
				<label class="chatAttach">
					<input name="attachFile" type="file" class="attachFile" />
					<img src="<?php echo asset('assets/frontend/chat/img/attachment.jpg'); ?>" alt=""/>
				</label>
				<input type="hidden" name="from_user_id" class="from_user_id" value="<?php echo $chat_messsage_data['from_user_id_pop']; ?>" />
				<input type="hidden" name="to_user_id" class="to_user_id" value="<?php echo $chat_messsage_data['to_user_id_pop']; ?>" />
			</form>
		<?php else: ?>
			<div id="warningMessageBox" class="chatSbmtformsg" style="min-height:0px;">
				<div class="buttonset align-center">
					<a class="button" href="<?php echo url('user/'.app()->getLocale().'/credit'); ?>">Buy Credits to Send Message </a>
				</div>
			</div>
		<?php endif; ?>
		</div>
	</div>
</div>