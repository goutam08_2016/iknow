<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <!--body open-->
    <section class="pagecontent">
       <div class="container">
       		<div class="signupcont">
            	<h2>Sign Up</h2>
            	<!--<div class="signupstep">
                	<div class="signstepwrap">
                    	<div class="statusbar">
                        	<div class="stepbox stepcolor stepdone">
                            	<div class="stepicon">
                                	<span class="stepcircle"></span>
                                    <span class="steptxt">Step 1</span>
                                </div>
                            </div>
                            <div class="stepbox stepcolor stepdone">
                            	<div class="stepicon">
                                	<span class="stepcircle"></span>
                                    <span class="steptxt">Step 2</span>
                                </div>
                            </div>
                            <div class="stepbox stepcolor stepdone">
                            	<div class="stepicon">
                                	<span class="stepcircle"></span>
                                    <span class="steptxt">Step 3</span>
                                </div>
                            </div>
                            <div class="stepbox stepcolor">
                            	<div class="stepicon">
                                	<span class="stepcircle"></span>
                                    <span class="steptxt">Step 4</span>
                                </div>
                                <div class="stepicon">
                                	<span class="stepcircle"></span>
                                    <span class="steptxt">Step 5</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
            
            	<div class="signupwrap">					
					<div class="signupStepsCont">
						<div class="signupSteps">
							<span class="st">1</span>
							<span class="st st2">2</span>
							<span class="st st3">3</span>
							<span class="st st4">4</span>
							<span class="st st5">5</span>
							<div class="stepProgress">
								<div class="current"></div>
							</div>
						</div>
					</div>
                	<div class="signuptext">
                    	<p>Lorem Ipsum ,</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                    
                    <div class="signupbottom clear">
                    	<div class="firstsignup">
                        	<form name="signup" method="post" action="" class="validFrm">
							<?php echo e(csrf_field()); ?>

                            	<div class="formrow clear">
                                	<div class="namebox">
                                    	<label>
                                        	<span class="label">Your Name <strong>*</strong></span>
											<?php $nam= (!empty($user->first_name) || !empty($user->last_name))?$user->first_name.$user->last_name:'';
												
											?>
                                            <div class="textfld">
                                            	<input type="text" required name="user_name" value="<?php echo e(trim($nam)); ?>" placeholder="John Smith"/>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="sexbox">
                                    	<label>
                                        	<span class="label">Sex <strong>*</strong></span>
                                            <div class="textfld">
                                            	<select name="gender" required>
                                                	<option value="">Select </option>
                                                	<option value="M" <?php echo e($user->gender == 'M' ? 'selected':''); ?> >Male</option>
                                                    <option value="F" <?php echo e($user->gender == 'F' ? 'selected':''); ?> >Female</option>
                                                </select>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="formrow clear">
                                	<span class="label labelhd">Date of Birth</span>
									<?php
									$dob = $user->dob;
									$dob_arr = explode('-',$dob);
									?>
                                    <div class="bd_day">
                                        <label>
                                            <span class="label">Day</span>
                                            <select name="day" >
                                                <option value="">Select</option>
												<?php for( $i=1; $i<=31; $i++ ): ?>
                                                <option value="<?php echo e($i); ?>" <?php echo isset($dob_arr[0]) && $dob_arr[0] == $i ? "selected" : ""; ?> ><?php echo e($i); ?></option>
												<?php endfor; ?>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="bd_month">
                                    	<label>
                                            <span class="label">Month</span>
                                            <select name="month" >
                                                <option value="">Select</option>
                                                <?php for( $i=1; $i<=12; $i++ ): ?>
                                                <option value="<?php echo e($i); ?>" <?php echo isset($dob_arr[1]) && $dob_arr[1] == $i ? "selected" : ""; ?> ><?php echo e($i); ?></option>
												<?php endfor; ?>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="bd_year">
                                    	<label>
                                            <span class="label">Year <strong>*</strong></span>
                                            <select name="year" required >
                                                <option value="">Select</option>
												<?php for( $i=1950; $i<=2016; $i++ ): ?>
                                                <option value="<?php echo e($i); ?>" <?php echo isset($dob_arr[2]) && $dob_arr[2] == $i ? 'selected' : ''; ?> ><?php echo e($i); ?></option>
												<?php endfor; ?>
                                            </select>
                                        </label>
                                    </div>
                                </div>
								
                            	<div class="formrow clear">
                                	<div class="namebox">
                                    	<label>
                                        	<span class="label">Nationality <strong>*</strong></span>
											<?php $nationality= (!empty($user->nationality) )?$user->nationality:'';
												
											?>
                                            <div class="textfld">
                                            	<input type="text" required name="nationality" value="<?php echo e($nationality); ?>" placeholder="nationality"/>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="buttonbox clear">
                                	<!--<a href="#" class="prevstep assbtn">prev</a>-->
                                    <input type="hidden" name="in_step" value="<?php echo e($step); ?>" />
                                    <input type="submit" value="next" class="nextstep bluebtn">
                                </div>
                            </form>
                        </div>
                        <div class="signupthumb clear">
                        	<img src="<?php echo asset('assets/frontend'); ?>/images/signup-charecter.png" width="146" height="459" alt="">
                        </div>
                    </div>
                </div>
                
            </div>
       </div>
    </section>
    <!--body close-->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>