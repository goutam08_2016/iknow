<?php $__env->startSection('pageTitle', 'Welcome to '); ?>



<?php $__env->startSection('customStyle'); ?>

<link href="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.css'); ?>" rel="stylesheet" type="text/css">

<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>



	<?php echo $client_header; ?>


    <!--main content open-->

    <section class="testpage">

    	<div class="addnewbox">

        	<div class="wrapper">

                <div class="newformwrap">

                	<a href="javascript:void(0);" class="closebtn"><img src="<?php echo asset('assets/frontend/images'); ?>/close.png" alt=""></a>

                    <form id="addgrp" name="add" method="post" action="">

                        <div class="formrow clear">

                            <label>

                                <span class="label">Test name:</span>

                                <div class="textfld">

                                    <input type="text" id="title" name="title" value="" />

                                </div>

                            </label>

                        </div>

                        <div class="addsubmit">

                            <input type="button" id="adGpbtn" onclick="addTest(0)" class="colorbtn bluebtn" value="Add Test">

                        </div>

                    </form>


                </div>

            </div>

        </div>

    	<div class="deta-search">

        	<div class="wrapper clear">

            	<div class="search-box groupsearch clear">

                	<form name="search" method="get" action="">

                        <div class="formcol">
							
                            <input type="text" name="tst" placeholder="Search Test Name" value="<?php echo e(!empty($_GET['tst'])?$_GET['tst']:''); ?>"/>

                            <button type="submit" value="search"><i class="fa fa-search" aria-hidden="true"></i></button>

                        </div>

                    </form>

                </div>

                <a href="javascript:void(0);" class="colorbtn bluebtn newtest">+ &nbsp; New Test</a>
				<div class="qscatagorypopup" id="qscategory">
					<div class="qswrap">
						<h2>Which kind of question you like to add ?</h2>
						<ul>
							<li>
								<a href="<?php echo url('user/add-question/1'); ?>">Open question</a>
							</li>
							<li>
								<a href="<?php echo url('user/add-question/2'); ?>">Multiple question</a>
							</li>
							<li>
								<a href="<?php echo url('user/add-question/3'); ?>">Multiple choice question</a>
							</li>
							<li>
								<a href="<?php echo url('user/add-question/4'); ?>">Fill the gap</a>
							</li>
							<li>
								<a href="<?php echo url('user/add-question/5'); ?>">Connect</a>
							</li>
							<li>
								<a href="<?php echo url('user/add-question/6'); ?>">Question like: rate in points</a>
							</li>
							<li>
								<a href="<?php echo url('user/add-question/7'); ?>">Organize in order</a>
							</li>
						</ul>
					</div>
				</div>
            </div>

        </div>

    	<div class="test-deta">

            <div class="wrapper">

                <div class="detatable">

					<?php if(!empty($tests)): ?>

						<?php foreach($tests as $val): ?>
						<div class="testtable">
							<div class="testtablerow">
								<div class="detacol detafld">
									<a href="#">
										<img src="<?php echo asset('assets/frontend/images/test.png'); ?>" alt="">
										<p class="ttl"><?php echo e($val->title); ?></p>
									</a>
								</div>
								<div class="detacol copydeta">
									<a href="#qscategory" class="qspopup"><img src="<?php echo asset('assets/frontend/images/dup.png'); ?>" alt=""><span>+Question</span></a>
								</div>
								<div class="detacol copydeta">
									<a href="#"><img src="<?php echo asset('assets/frontend/images/dup.png'); ?>" alt=""><span>Copy</span></a>
								</div>
								<div class="detacol printdeta">
									<a href="#"><img src="<?php echo asset('assets/frontend/images/print.png'); ?>" alt=""><span>Print</span></a>
								</div>
								<div class="detacol assigndeta">
									<a href="javascript:void(0);"><img src="<?php echo asset('assets/frontend/images/assign.png'); ?>" alt=""><span>Assign</span></a>
								</div>
								<div class="detacol assigndeta">
									<a href="<?php echo e(url('user/test-setting/'.$val->id)); ?>"><img src="<?php echo asset('assets/frontend/images/assign.png'); ?>" alt=""><span>Setting</span></a>
								</div>
								<div class="detacol resultdeta">
									<a href="<?php echo e(url('user/test-results/'.$val->id)); ?>"><img src="<?php echo asset('assets/frontend/images/result.png'); ?>" alt=""><span>Result</span></a>
								</div>
								<div class="detacol linkdeta">
									<a href="javascript:void(0);"><img src="<?php echo asset('assets/frontend/images/links.png'); ?>" alt=""><span>Links</span></a>
								</div>
								<div class="detacol editdeta">
									<a href="javascript:void(0);" class="edt" ><img src="<?php echo asset('assets/frontend/images/new-file.png'); ?>" alt=""><span>Edit</span><input type="hidden" value="<?php echo e($val->id); ?>"/></a>
								</div>
								<div class="detacol removedeta">
									<a href="javascript:void(0);" class="rmve" >
										<img src="<?php echo asset('assets/frontend/images/remove.png'); ?>" alt="">
										<span>Remove</span>
										<input type="hidden" value="<?php echo e($val->id); ?>"/>
									</a>
								</div>
							</div>
							<div class="testlinkdeta testlinkbox">
								<h4>Three options for giving access to this test:</h4>
								<div class="formrow">
									<label>
									<span class="label">Email test link to users</span>
									<input type="text" value="https://www.classmarker.com/online-test/start/?quiz=7ff57abf77de5754">
									</label>
								</div>
								<div class="formrow">
									<label>
									<span class="label">Add the test link on your website</span>
									<input type="text" value="<a href=&quot;https://www.classmarker.com/online-test/start/?quiz=7ff57abf77de5754&quot; title=&quot;Take our online test&quot;>Take our online test</a>">
									</label>
								</div>
								<div class="formrow">
									<label>
									<span class="label">Embed test into your website</span>
									<input type="text" value="<iframe src=&quot;https://www.classmarker.com/online-test/start/?quiz=7ff57abf77de5754&amp;iframe=1&quot; frameborder=&quot;0&quot; style=&quot;width:100%;max-width:700px;&quot; height=&quot;800&quot;></iframe>">
									</label>
								</div>
							</div>
							<div class="testlinkdeta testasignbox">
								<h4>Select Group(s)</h4>								
								<?php $asn_grp =array();
								if(!empty($val->test_groups))
								{
									foreach($val->test_groups as $vl)
									{
										
										$asn_grp[]=	$vl->pivot->group_id;						
									}
								}
								?>
								<form method="post">
								<?php if(!empty(Auth::user()->groups)): ?>
									<?php foreach(Auth::user()->groups as $grp): ?>
									<div class="formrow">
										<label>
											<input type="checkbox" name="groups[]" value="<?php echo e($grp->id); ?>" <?php echo e(in_array($grp->id, $asn_grp)?'checked':''); ?> >
											<span class="label"><?php echo e($grp->title); ?></span>
										</label>
									</div>
									<?php endforeach; ?>
								<?php endif; ?>
								<input type="hidden" name="test_id" value="<?php echo e($val->id); ?>">
								<a href="javascript:void(0);"  class="svgrps" class="colorbtn">Save</a>	
								</form>							
							</div>
						</div>

						<?php endforeach; ?>

					<?php endif; ?>

                </table>

            </div>

        </div>

    </section>

    <!--main content close-->

    

	

<?php $__env->stopSection(); ?>



<?php $__env->startSection('customScript'); ?>


<script src="<?php echo asset('assets/frontend/js/stacktable.min.js'); ?>"></script>
<script src="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.js'); ?>"></script>

<script>
$('.svgrps').click(function(){
	
	var frm = $(this).parent();
	var data_arr = frm.serializeArray();
	
	//console.log(data_arr);
	if(data_arr)
	{
		$.ajax({

			type:"post",

			url: "<?php echo url('user/assign-test-group'); ?>" ,

			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},

			data: data_arr,

			//dataType: "json",

			success:function(data) {

				//console.log(data);

				window.location.reload();

			}

		});	
	}
});


function  addTest(id)
{	

	//var frmdata = $('form#addgrp').serializeArray();

	var titl = $('#title').val();

	$('#title').parent().find('.msg').remove();

	//console.log(frmdata);

	if(titl!="")

	{

		$.ajax({

			type:"post",

			url: "<?php echo url('user/add-test'); ?>" ,

			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},

			data: {title: titl,id: id},

			//dataType: "json",

			success:function(data) {

				//console.log(data);

				window.location.reload();

			}

		});		

	}else{

		$('#title').parent().append('<span class="msg">Please add Test name.<span>');

	}

}

$(".rmve").click(function() {

	var obj= $(this);

	//console.log(obj.parent().parent().find('.ttl').html());

	var txt = obj.parent().parent().find('.ttl').text();

	var gpId= obj.children('input').val();

	var r = confirm("Are you sure to delete test "+txt+"?");

	if (r == true) {

		$.ajax({

			type:"post",

			url: "<?php echo url('user/del-test'); ?>" ,

			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},

			data: {id: gpId},

			//dataType: "json",

			success:function(data) {

				//console.log(data);

				obj.parent().parent().remove();

			}

		});

	}	

	

});



$(".edt").click(function() {

	var obj= $(this);

	var gpId= obj.children('input').val();

	console.log(obj.parent().parent().find('.ttl').html());

	var txt = obj.parent().parent().find('.ttl').text();

	$('#title').val(txt);

	$('.addnewbox').slideDown(200);

	$('#adGpbtn').attr('onclick','addTest('+gpId+')');

});	

$(document).ready(function() {

    $('.newtest').click(function() {

		$('.addnewbox').slideDown(200);

		$("#addgrp")[0].reset();

		$('#adGpbtn').attr('onclick','addTest(0)');

	});

	$('.newformwrap .closebtn').click(function() {

		$('.addnewbox').slideUp(200);

	});

	

	$('.membertable .setpassword a').click(function() {

		$(this).parent('.setpassword').parent('.testtablerow').next('.testlinkdeta').slideToggle(200);

	});

	$('.testtable .linkdeta a').click(function() {
		$(this).parent('.linkdeta').parent('.testtablerow').next('.testlinkbox').slideToggle(200);
	});
	$('.testtable .assigndeta a').click(function() {
		$(this).parent('.assigndeta').parent('.testtablerow').parent('.testtable').children('.testasignbox').slideToggle(200);
	});

	
	$('.qstable').cardtable();
	$('.qspopup').fancybox({
		padding:0
	});
});

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>