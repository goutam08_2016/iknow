<aside class="leftpan">
	<div class="languageSelect clear">
		<a href="" onclick="return false;" class="lanTab opn" id="vniL" title="VNI">VNI</a>
		<a href="" onclick="return false;" class="lanTab" id="engL" title="ENG">ENG</a>
	</div>
	<div class="left-top">
		<ul>
			<li><a href="<?php echo url('user/account'); ?>"><i class="fa fa-user" aria-hidden="true"></i>Profile</a></li>
			<li><a href="<?php echo e(url('user/admin-blogs')); ?>"><i class="fa fa-book" aria-hidden="true"></i>Admin Blog</a></li>
			<li><a href="<?php echo e(url('user/blog-catagories')); ?>"><i class="fa fa-book" aria-hidden="true"></i>Read</a></li>
			<li><a href="<?php echo e(url('user/my-blogs')); ?>"><i class="fa fa-book" aria-hidden="true"></i>My Blog</a></li>
			<li><a href="#"><i class="fa fa-clipboard" aria-hidden="true"></i>Answer</a>
				<ul>
					<li><a href="<?php echo e(url('user/sent-question')); ?>"><i class="fa fa-question-circle" aria-hidden="true"></i>sent questions <span class="ttlnumber"><?php echo e((Auth::check())?sent_question_not_replied(Auth::User()->id) :0); ?></span></a></li>
					<li><a href="<?php echo e(url('user/received-question')); ?>"><i class="fa fa-question-circle" aria-hidden="true"></i>received questions <span class="ttlnumber"><?php echo e((Auth::check())?received_question_not_replied(Auth::User()->id) :0); ?></span></a></li>
				</ul>
			</li>
			<li><a href="<?php echo e(url('topics')); ?>"><i class="fa fa-th-list" aria-hidden="true"></i>Topics</a></li>
			<li><a href="<?php echo e(url('user/friends')); ?>"><i class="fa fa-phone" aria-hidden="true"></i>Contact</a></li>
			<li><a href="#"><i class="fa fa-bell" aria-hidden="true"></i>Notifications</a></li>
			<li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i>setting</a>
				<ul>
					<li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>Account</a></li>
					<li><a href="#"><i class="fa fa-lock" aria-hidden="true"></i>Privacy</a></li>
					<li><a href="#"><i class="fa fa-comment" aria-hidden="true"></i>Notification</a></li>
				</ul>
			</li>
			<li><a href="<?php echo url('logout'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i>logout</a></li>
		</ul>
	</div>
	<div class="leftbot">
		<ul>
			<li>
				<p>You answered question</p>
				<a href="#">What are good ways to learn to become the best digital marketer?.</a>
				<span>on July 22, 2016 </span>
			</li>
			<li>
				<p>You answered question</p>
				<a href="#">What are good ways to learn to become the best digital marketer?.</a>
				<span>on July 22, 2016 </span>
			</li>
			<li>
				<p>You answered question</p>
				<a href="#">What are good ways to learn to become the best digital marketer?.</a>
				<span>on July 22, 2016 </span>
			</li>
		</ul>
	</div>
</aside>