<?php $__env->startSection('pageTitle', 'Welcome to '); ?>
<?php $__env->startSection('customStyle'); ?><link href="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.css'); ?>" rel="stylesheet" type="text/css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	<?php echo $client_header; ?>
    	 
    <!--main content open-->
    <section class="testpage">
		<?php if(session('cat_success')): ?>
			<div class="success_message">
				<p><?php echo session('cat_success'); ?></p>
			</div>
			<?php session()->forget('cat_success');?>
		<?php endif; ?>				    	<div class="deta-search">        	<div class="wrapper clear">            	<!--<div class="search-box groupsearch clear">                	<form name="search" method="post" action="#">                        <div class="formcol">                            <input type="text" placeholder="Search Group Name">                            <button type="submit" value="search"><i class="fa fa-search" aria-hidden="true"></i></button>                        </div>                    </form>                </div>-->                <!--<a href="#qscategory" class="colorbtn bluebtn newtest qspopup">+ &nbsp; Add Question</a>-->                <div class="qscatagorypopup" id="qscategory">                	<div class="qswrap">                    	<ul>                        	<li><a href="#">Open question</a></li>                            <li><a href="#">Multiple question</a></li>                            <li><a href="#">Multiple choice question</a></li>                            <li><a href="#">Fill the gap</a></li>                            <li><a href="#">Connect</a></li>                            <li><a href="#">Question like: rate in points</a></li>                            <li><a href="#">Organize in order</a></li>                        </ul>                    </div>                </div>            </div>        </div>    	<div class="test-deta">            <div class="wrapper">                <div class="questionbox">                	<form name="question" method="post" action="#">                        <div class="questionrow">                            <h3>Question</h3>                            <div class="questionedit">                                <textarea name="description" class="texteditor"></textarea>                            </div>                            <div class="fileuploded">                                <label class="uploadedbutton">                                    <input type="file">                                    <i class="fa fa-paperclip" aria-hidden="true"></i> Add Attachment                                </label>                            </div>                        </div>                        <div class="questionrow nextrow">                            <h3>Category</h3>                            <div class="selectcategory">                            	<select>                                	<option>Generic</option>                                </select>                            </div>                        </div>                        <div class="questionrow nextrow">                            <h3>Points Available</h3>                            <div class="pointbox">                            	<input type="number" value="1">                            </div>                        </div>                        <div class="buttonbox clear">                            <input type="submit" value="Save">                            <input type="reset" value="cancel">                        </div>                    </form>                </div>            </div>        </div>   	</section>
    <!--main content close-->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('customScript'); ?><script src="<?php echo asset('assets/frontend/js/stacktable.min.js'); ?>"></script><script src="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.js'); ?>"></script>
<script>$(document).ready(function() {    $('.qstable').cardtable();		$('.qspopup').fancybox({		padding:0	});});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>