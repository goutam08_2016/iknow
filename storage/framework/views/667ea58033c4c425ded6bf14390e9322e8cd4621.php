<?php $__env->startSection('pageTitle', 'friend request'); ?>

<?php $__env->startSection('content'); ?>
<section class="mainbody clear">
<?php echo $__env->make('include.left_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!--middle open-->
<div class="middlecol">
	<div class="blogCate">
		<div class="blogTab">
			<div class="filterBtn tabmenu">
				<ul>
					<li><a href="javascript:void(0);" id="tab1">All</a></li>
					<li><a href="javascript:void(0);" id="tab2">Favorite</a></li>
				</ul>
			</div>
			<div class="tabContWrap">
				<div class="BlogCatList tabCont">
					<ul id="catlst">
					<?php if(count($catagories)>0): ?>
						<?php foreach($catagories as $cat): ?>
						<li class="<?php echo e(in_array($cat->id,$fav_arr)?'fav':''); ?>" id="cat_<?php echo e($cat->id); ?>">
							<div class="catThumb">
								<figure><a href="<?php echo e(url('user/blog-list/'.$cat->id)); ?>"><img src="<?php echo asset('assets/upload/category_icon/'.$cat->icon); ?>" alt=""></a></figure>
								<a href="javascript:void(0);" class="str addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="<?php echo e($cat->id); ?>"/></a>
							</div>
							<span><a href="<?php echo e(url('user/blog-list/'.$cat->id)); ?>"><?php echo e($cat->title); ?></a></span>
						</li>
						<?php endforeach; ?>
					<?php endif; ?>						
					</ul>
				</div>
				<div class="BlogCatList tabCont">
					<ul id="favLst">
					<?php if(count($user->fav_categories)>0): ?>
						<?php foreach($user->fav_categories as $val): ?>
						<?php $cat=$val->category;?>
						<li class="fav" id="cat_<?php echo e($cat->id); ?>">
							<div class="catThumb">
								<figure><a href="<?php echo e(url('user/blog-list/'.$cat->id)); ?>"><img src="<?php echo asset('assets/upload/category_icon/'.$cat->icon); ?>" alt=""></a></figure>
								<a href="javascript:void(0);" class="str addF"><i class="fa fa-star" aria-hidden="true"></i><input type="hidden" value="<?php echo e($cat->id); ?>"/></a>
							</div>
							<span><a href="<?php echo e(url('user/blog-list/'.$cat->id)); ?>"><?php echo e($cat->title); ?></a></span>
						</li>
						<?php endforeach; ?>
					<?php endif; ?>						
					</ul>
				</div>
			</div>
		</div>
	</div>        
</div>
<!--middle close-->


<?php echo $__env->make('include.right_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
<script>
$(document).on('click',".addF", function(e){
	e.preventDefault();
	var currentobj = $(this);
	var addFid = $(this).find('input').val();
	//alert(addFid);
	$.ajax({
		type:"post",
		url: "<?php echo url('user/add-fav-cat'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: {'cat_id':addFid, },			
		dataType: "json",			
		success:function(res) {
			//console.log(res);
			var parentli = currentobj.parent().parent('li');
			
			if(res.msg==0){
				//parentli.removeClass('fav');
				$("#favLst").find('li#cat_'+addFid).remove();
				$("#catlst").find('li#cat_'+addFid).removeClass('fav');
				
			}else if(res.msg==1){				
				$("#favLst").append(res.favHtml);
				$("#catlst").find('li#cat_'+addFid).addClass('fav');
			}
		}
	}); 
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>