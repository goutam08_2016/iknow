<script>

</script>
<?php $user_filter = array();
if(Auth::check() && !empty(Auth::user()->filterset_order))	$user_filter = json_decode(Auth::user()->filterset_order->filter_set);
$user_filter = (array) $user_filter; 
//print_r($user_filter);
?>
<?php $menu_arr=[
			'name'=>['Name','nickname','nam','','','','03.png'],
			'locl'=>['Location(live)','location','locationlive','loction','loction','','01.png'],
			'locv'=>['Location(visit)','location','locationvisit','joblocation','location_visit','','01.png'],
			'workpl'=>['Working/Studying Place','place','workplace','jobloc','joblocation','','04.png'],
			'prof'=>['Profession','setting','profesion','','job','jobs','02.png'],
			'dpt'=>['Department','place','depatment','department','department','','04.png'],
			'tpic'=>['Topic','brain','tpic','src_tropic','tropic','','05.png'],
			'gnder'=>['Gender','gender','gendr','','','','03.png'],
			'eduction'=>['Education','education-icon-blue','eductn','','education','education','education-icon.png'],
			'mjor'=>['Major','book','majr','','major','major','06.png'],
			'ag'=>['Age','age','agee','','','','zz.png'],
			];
			//print_r($menu_arr);
?>
<div id="popupAdvance" class="answerpopup newPageSearch">
    <div class="pageMiddle">
        <div class="contList searchPeople clear">
            <div class="searchLeft">
                <div class="searchHead clear">
                    <h2>Filters</h2>
                    <div class="filTer">
                        <span class="filterIcon"><i class="fa fa-filter" aria-hidden="true"></i></span>
                        <div class="jilterList">
							<form method="POST" class="toggFilter ordrFrm" >
								<ul id="shortable_area2">
									<?php if(!empty($user_filter)): ?>
										<?php foreach($user_filter as $ky=>$val): ?>
										<li class="<?php echo e($ky); ?>">
											<label>
												<input type="checkbox" name="<?php echo e($ky); ?>" value="<?php echo e($val); ?>" checked />
												<span class="checkbox"><img src="<?php echo asset('assets/frontend'); ?>/images/clr.png" alt=""></span>
													<?php echo e($menu_arr[$ky][0]); ?><img src="<?php echo asset('assets/frontend'); ?>/images/<?php echo e($menu_arr[$ky][1]); ?>.png">
											</label>
										</li>
										<?php endforeach; ?>
									<?php endif; ?>
								
									<?php foreach($menu_arr as $ky=>$val): ?>
										<?php if( !in_array($val[2],$user_filter)): ?>
										<li class="<?php echo e($ky); ?>">
											<label>
												<input type="checkbox" name="<?php echo e($ky); ?>" value="<?php echo e($val[2]); ?>" />
												<span class="checkbox"><img src="<?php echo asset('assets/frontend'); ?>/images/clr.png" alt=""></span>
													<?php echo e($val[0]); ?><img src="<?php echo asset('assets/frontend'); ?>/images/<?php echo e($val[1]); ?>.png">
											</label>
										</li>
										<?php endif; ?>
									<?php endforeach; ?>
									<li>
										<input type="submit" value="Done" class="bluebtn filterSave">
										<input type="hidden" name="styp" value="1" />
									</li>
								</ul>
						   </form>
                        </div>
                    </div>
                </div>
                
                <div class="peopleForm">
					<form method="post" id="searchFrm">
						<div class="proHeight">	
							<?php if(!empty($user_filter)): ?>
									<?php foreach($user_filter as $ky=>$val): ?>
									<div class="filterRow <?php echo e($val); ?>">
										<label class="clear">
											<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/<?php echo e($menu_arr[$ky][6]); ?>" alt=""><span><?php echo e($menu_arr[$ky][0]); ?></span></span>
											<div class="textfld clear">
											<?php if($val=='gendr'): ?>
												<select name="gender">
													<option value="">select</option>
													<option value="M">Male</option>
													<option value="F">Female</option>
												</select>
											<?php elseif($val=='agee'): ?>
												<div class="row02">
													<!--<samp>From</samp>-->
													<div class="smallSelect">
														<select name="age_from">
															<option value="">From</option>
															<?php for($i=18;$i<100;$i++): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
												</div>
												<div class="row02">
													<!--<samp>To</samp>-->
													<div class="smallSelect">
														<select name="age_to">
															<option value="">To</option>
															<?php for($i=18;$i<100;$i++): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
												</div>
											<?php else: ?>
												<input type="text" placeholder="<?php echo e($menu_arr[$ky][0]); ?>" class="<?php echo e($menu_arr[$ky][5]); ?>" id="<?php echo e($menu_arr[$ky][3]); ?>" name="<?php echo e($menu_arr[$ky][4]); ?>"/>
												<?php if($val=='tpic'): ?>
												<input type="hidden" value="" id="t_ids" name="tropicid"/>
												<?php endif; ?>
											<?php endif; ?>
											</div>
										</label>
									</div>	
									<?php endforeach; ?>
								<?php endif; ?>
							
								<?php foreach($menu_arr as $ky=>$val): ?>
									<?php if( !in_array($val[2],$user_filter)): ?>
									<div class="filterRow <?php echo e($val[2]); ?>" style="display:<?php echo e((Auth::check() && !empty(Auth::user()->filterset_order))?'none':'block'); ?>;">
										<label class="clear">
											<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/<?php echo e($menu_arr[$ky][6]); ?>" alt=""><span><?php echo e($menu_arr[$ky][0]); ?></span></span>
											<div class="textfld clear">
											<?php if($val[2]=='gendr'): ?>
												<select name="gender">
													<option value="">select</option>
													<option value="M">Male</option>
													<option value="F">Female</option>
												</select>
											<?php elseif($val[2]=='agee'): ?>
												<div class="row02">
													<!--<samp>From</samp>-->
													<div class="smallSelect">
														<select name="age_from">
															<option value="">From</option>
															<?php for($i=18;$i<100;$i++): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
												</div>
												<div class="row02">
													<!--<samp>To</samp>-->
													<div class="smallSelect">
														<select name="age_to">
															<option value="">To</option>
															<?php for($i=18;$i<100;$i++): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
												</div>
											<?php else: ?>
												<input type="text" placeholder="<?php echo e($menu_arr[$ky][0]); ?>" class="<?php echo e($menu_arr[$ky][5]); ?>" id="<?php echo e($menu_arr[$ky][3]); ?>" name="<?php echo e($menu_arr[$ky][4]); ?>"/>
												<?php if($val[2]=='tpic'): ?>
												<input type="hidden" value="" id="t_ids" name="tropicid"/>
												<?php endif; ?>
											<?php endif; ?>
											</div>
										</label>
									</div>	
									<?php endif; ?>
								<?php endforeach; ?>
							<!--<div class="filterRow locationlive">
								<label class="clear">
									<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/01.png" alt=""><span>Location(live)</span></span>
									<div class="textfld clear">
										<input type="text" placeholder="Location" id="loction" name="loction"/>
									</div>
								</label>
							</div>										
							<div class="filterRow locationvisit">
								<label class="clear">
									<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/01.png" alt=""><span>Location(visit)</span></span>
									<div class="textfld clear">
										<input type="text" placeholder="Location" id="joblocation" name="location_visit"/>
									</div>
								</label>
							</div>
							<div class="filterRow clear workplace">
								<label class="clear">
									<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/04.png" alt=""><span>Working Place</span></span>
									<div class="textfld">
										<input type="text" placeholder="Working/Study/ing Place" id="jobloc" name="joblocation"/>
									</div>
								</label>
							</div>
							<div class="filterRow profesion">
								<label class="clear">
									<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/02.png" alt=""><span>Profession</span></span>
									<div class="textfld">
										<input class="jobs" type="text" name="job" placeholder="Profession">
									</div>
								</label>
							</div>							
							<div class="filterRow clear depatment">
								<label class="clear">
									<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/04.png" alt=""><span>Department</span></span>
									<div class="textfld">
										<input type="text" placeholder="Department" id="department" name="department"/>
									</div>
								</label>
							</div>
							<div class="filterRow tpic">
								<label class="clear">
									<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/05.png" alt=""><span>Topic</span></span>
									<div class="textfld">
										<input class="" type="text" id="src_tropic" name="tropic" placeholder="Tropic">
										<input type="hidden" value="" id="t_ids" name="tropicid"/>
									</div>
								</label>
							</div>
							<div class="filterRow gendr">
								<label class="clear">
									<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/03.png" alt=""><span>Gender</span></span>
									<div class="textfld">
										<select name="gender">
											<option value="">select</option>
											<option value="M">Male</option>
											<option value="F">Female</option>
										</select>
									</div>
								</label>
							</div>
							<div class="filterRow agee">
								<label class="clear">
									<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/zz.png" alt=""><span>age</span></span>
									<div class="textfld">
										<div class="row02">
											
											<div class="smallSelect">
												<select name="age_from">
													<option value="">From</option>
													<?php for($i=18;$i<100;$i++): ?>
													<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
													<?php endfor; ?>
												</select>
											</div>
										</div>
										<div class="row02">
											
											<div class="smallSelect">
												<select name="age_to">
													<option value="">To</option>
													<?php for($i=18;$i<100;$i++): ?>
													<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
													<?php endfor; ?>
												</select>
											</div>
										</div>
									</div>
								</label>
							</div>							
							<div class="filterRow eductn">
								<label class="clear">
									<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/education-icon.png" alt=""><span>Education</span></span>
									<div class="textfld">
										<input id="education" type="text" name="education" placeholder="Education"/>
									</div>
								</label>
							</div>
							<div class="filterRow majr">
								<label class="clear">
									<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/06.png" alt=""><span>Major</span></span>
									<div class="textfld">
										<input class="major" type="text" name="major" placeholder="Major"/>
									</div>
								</label>
							</div>-->
						</div>
						<div class="filterBtnRow">
							<input type="reset" value="Reset" class="bluebtn"/>
							<input type="submit" value="Search" class="bluebtn">
							<input type="hidden" value="1" name="list_type"/>
						</div>
					</form>
                </div>
            </div>
                
            <div class="allpeople searchRight clear">
                <h2>Advance Search</h2>
                <span class="annce">We found <font id="match_count">0</font> people matching your description.</span>
                <div class="cleckall">
                <label><span>Select All</span>
                    <input class="slctAll" type="checkbox" name="check">
                    <span class="checkbox"><img src="<?php echo asset('assets/frontend'); ?>/images/clr.png" alt=""></span>
                </label>
                </div>
				<form id="userfrm" method="post">
                <div class="searchpeopleWrap">
                    <div class="newAll" id="usrlist">
						No user found..
                    </div>
                </div>
                    
                <div class="filterBtnRow">
                    <input type="submit" value="Send" class="bluebtn">
                </div>
				</form>
            </div>
        </div>
    </div>
</div>
<style>
.pac-container{z-index:9999!important;}
</style>
