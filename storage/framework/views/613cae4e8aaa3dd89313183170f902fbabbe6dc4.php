<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <!--body open-->
    <section class="pagecontent">
       <div class="container">
       		<div class="signupcont">
            	<h2>Sign Up</h2>
				
				
            	<div class="signupwrap">					
					<div class="signupStepsCont">
						<div class="signupSteps">
							<span class="st green"><i class="fa fa-check"></i></span>
							<span class="st st2 green"><i class="fa fa-check"></i></span>
							<span class="st st3 green"><i class="fa fa-check"></i></span>
							<span class="st st4">4</span>
							<span class="st st5">5</span>
							<div class="stepProgress">
								<div class="current" style="width:50%;"></div>
							</div>
						</div>
					</div>
                    <div class="signupbox formstep4">
                    	
                            <div class="signupsteprow clear">
                            	<span class="title">Topic you know</span>
                                <div class="signupleft noalign">
                                    <div class="formrow">
										<form class="tropic" name="" method="post" action="">
                                        <div class="frmsml clear">
                                            <input type="text" id="sp_tropic" name="title" placeholder="Language, Sport, Skills, Software..."/>
                                            <input type="submit" value="add" class="bluebtn">
                                            <input type="hidden" value="1" name="type">
											<?php $user_tropics='';
											if(!empty($user->tropics)){
												foreach($user->tropics as $it=>$val){
													if($val->type ==1)
													{
														$user_tropics.=($it!=0)?',':'';
														$user_tropics.=$val->tropic_id;
													}
												}												
											}													
											?>
                                            <input type="hidden" value="<?php echo e($user_tropics); ?>" id="sp_ids" name="ids">
                                        </div>
										</form>
                                    </div>
                                </div>
                                <div class="frmImg clear">
                                    <div class="signupright">
                                        <ul>
                                            <li>
                                                <img src="<?php echo asset('assets/frontend'); ?>/images/img01.jpg" width="150" height="150" alt="">
                                            </li>
                                            <li>
                                                <img src="<?php echo asset('assets/frontend'); ?>/images/img02.jpg" width="150" height="150" alt="">
                                            </li>
                                             <li>
                                                <img src="<?php echo asset('assets/frontend'); ?>/images/img01.jpg" width="150" height="150" alt="">
                                            </li>
                                        </ul>
                                    </div> <!-- -->
                                    <div class="signupleft">
										<div class="formrow">
										<?php  $know_tropics =  $user->tropics()->where('type',1)->orderBy('group_id','asc')->get();
										
										$group_arr=array();	
										
										if(!empty($know_tropics))
										{
											foreach($know_tropics as $ky=>$val)
											{
												if(!in_array($val->tropic->group_id,$group_arr))
												{	$group_arr[]= $val->tropic->group_id;
												?>
										   
												<div class="addrow clear">
													<h4><?php echo e($val->tropic->group_id !=0? ucfirst($val->tropic->group->name):'Others'); ?></h4>
													<ul>
														<li>
															<span class="addbox"><?php echo e($val->tropic->title); ?>

																<a href="javascript:void(0)" class="closebox">
																<img src="<?php echo asset('assets/frontend'); ?>/images/close.png" alt="">
																<input type="hidden" value="<?php echo e($val->id); ?>" />
																</a>
															</span>
														</li>
													
										<?php	}
												else
												{
										?>				<li>
															<span class="addbox"><?php echo e($val->tropic->title); ?>

																<a href="javascript:void(0)" class="closebox">
																<img src="<?php echo asset('assets/frontend'); ?>/images/close.png" alt="">
																<input type="hidden" value="<?php echo e($val->id); ?>" />
																</a>
															</span>
														</li>
										<?php	}
													
												if(isset($know_tropics[$ky+1]) && !in_array($know_tropics[$ky+1]->tropic->group_id,$group_arr))
												{
													echo "
													</ul>
												</div>";
												}
											}
										}
										?>
										</div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="signupsteprow clear">
                            	<span class="title">Topic you like</span>
                                <div class="signupleft noalign">
                                    <div class="formrow">
										<form class="tropic" name="" method="post" action="">
                                        <div class="frmsml clear">
                                            <input type="text" id="lg_tropic" name="title" placeholder="Language, Sport, Skills, Software..."/>
                                            <input type="submit" value="add" class="bluebtn">
											<input type="hidden" value="2" name="type">
											<?php $user_tropics='';
											if(!empty($user->tropics)){
												foreach($user->tropics as $it=>$val){
													if($val->type ==2)
													{
														$user_tropics.=($it!=0)?',':'';
														$user_tropics.=$val->tropic_id;
													}
												}												
											}													
											?>
                                            <input type="hidden" value="<?php echo e($user_tropics); ?>" id="lg_ids" name="ids">
                                        </div>
										</form>
                                    </div>
                                </div>
                                <div class="frmImg clear">
                                    <div class="signupright">
                                        <ul>
                                            <li>
                                                <img src="<?php echo asset('assets/frontend'); ?>/images/img03.jpg" width="150" height="150" alt="">
                                            </li>
                                            <li>
                                                <img src="<?php echo asset('assets/frontend'); ?>/images/img04.jpg" width="150" height="150" alt="">
                                            </li>
                                            <li>
                                                <img src="<?php echo asset('assets/frontend'); ?>/images/img03.jpg" width="150" height="150" alt="">
                                            </li>
                                        </ul>
                                    </div> <!-- -->
                                    <div class="signupleft">
										<div class="formrow">
										<?php  $like_tropics =  $user->tropics()->where('type',2)->orderBy('group_id','asc')->get();
										
										$group_arr=array();	
										
										if(!empty($like_tropics))
										{
											foreach($like_tropics as $ky=>$val)
											{
												if(!in_array($val->tropic->group_id,$group_arr))
												{	$group_arr[]= $val->tropic->group_id;
												?>
										   
												<div class="addrow clear">
													<h4><?php echo e($val->tropic->group_id !=0? ucfirst($val->tropic->group->name):'Others'); ?></h4>
													<ul>
														<li>
															<span class="addbox"><?php echo e($val->tropic->title); ?>

																<a href="javascript:void(0)" class="closebox">
																<img src="<?php echo asset('assets/frontend'); ?>/images/close.png" alt="">
																<input type="hidden" value="<?php echo e($val->id); ?>" />
																</a>
															</span>
														</li>
													
										<?php	}
												else
												{
										?>				<li>
															<span class="addbox"><?php echo e($val->tropic->title); ?>

																<a href="javascript:void(0)" class="closebox">
																<img src="<?php echo asset('assets/frontend'); ?>/images/close.png" alt="">
																<input type="hidden" value="<?php echo e($val->id); ?>" />
																</a>
															</span>
														</li>
										<?php	}
													
												if(isset($like_tropics[$ky+1]) && !in_array($like_tropics[$ky+1]->tropic->group_id,$group_arr))
												{
													echo "
													</ul>
												</div>";
												}
											}
										}
										?>
										</div>
                                    </div>
                                </div>
                            </div>
                        <form name="signup" method="post" action="">   
							<?php echo e(csrf_field()); ?>

                            <div class="buttonbox clear">
                                <a href="<?php echo e(url('user/signup-step/3')); ?>" class="prevstep assbtn">prev</a>
                                <input type="submit" value="next" class="nextstep bluebtn">								
                                <input type="hidden" name="in_step" value="<?php echo e($step); ?>" />
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
       </div>
    </section>
    <!--body close-->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKqvPeyXND6or2BXGG1O-qp-bC_lIz7Uk&libraries=places"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$(document).ready(function() {
    $('.closebox').click(function() {
		var dobj = $(this);
		var delid = dobj.children("input").val();
		$.ajax({
			type:"post",
			url: "<?php echo url('user/del-tropic'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {id:delid},			
			//dataType: "json",			
			success:function(res) {
				//console.log(res);
				if(res == 1)
				{
					dobj.parent().parent().remove();
				}
			}
		});
		//$(this).parent('.addbox').parent('li').remove();
	});
});

$( "form.tropic" ).on( "submit", function( event ) {
//
  event.preventDefault();
  var formdata =  $( this ).serialize();
  //console.log( $( this ).serialize() );
  $.ajax({
		type:"post",
		url: "<?php echo url('user/add-tropic'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: formdata,			
		//dataType: "json",			
		success:function(res) {
			/* console.log(res);
			$('#'+fid).val('');
			var lhtml = '<li><span class="addbox">'+addrval+'<a href="javascript:void(0)" class="closebox"><img src="'+'<?php echo asset('assets/frontend'); ?>'+'/images/close.png" alt=""></a></span></li>';
			$('#'+fid+'_lst').html(lhtml); */
			window.location.reload();
		}

	});
});


    var availableTags1 = [
	<?php if(!empty($all_tropics)): ?>
	  <?php foreach($all_tropics as $jval): ?>
		<?php echo '{"label":"'.$jval->title.'","id":"'.$jval->id.'"} ,'; ?>

	  <?php endforeach; ?>
	<?php endif; ?>
    ];
    $("#sp_tropic").autocomplete({
		source: availableTags1,
		select: function (event, ui) {
			//console.log(ui);
			var preVal = $("#sp_ids").val();
			if(preVal!='')	preVal+=',';
			preVal+=ui.item.id;
			$("#sp_ids").val(preVal);			
		}
    });
    $("#lg_tropic").autocomplete({
      source: availableTags1,
		select: function (event, ui) {
			//console.log(ui);
			var preVal = $("#lg_ids").val();
			if(preVal!='')	preVal+=',';
			preVal+=ui.item.id;
			$("#lg_ids").val(preVal);			
		}
    });
	
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>