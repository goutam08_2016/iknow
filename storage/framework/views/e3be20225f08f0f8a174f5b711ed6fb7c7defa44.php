<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	<?php echo $client_header; ?>
    <!--main content open-->
    <section class="testpage">
    	<div class="addnewbox">
        	<div class="wrapper">
                <div class="newformwrap">
                	<a href="javascript:void(0);" class="closebtn"><img src="<?php echo asset('assets/frontend/images'); ?>/close.png" alt=""></a>
                    <form id="addgrp" name="add" method="post" action="">
                        <div class="formrow clear">
                            <label>
                                <span class="label">Group name:</span>
                                <div class="textfld">
                                    <input type="text" id="title" name="title" value="" />
                                </div>
                            </label>
                        </div>
                        <div class="addsubmit">
                            <input type="button" id="adGpbtn" onclick="addGroup(0)" class="colorbtn bluebtn" value="Create Group">
                        </div>
                    </form>
                    <div class="newtext">
                        <h4>About Groups:</h4>
                        <p>Registered Group Users will log in via the ClassMarker.com homepage where they can easily take the Tests you have assigned to their Group/s.</p>

<p>If you prefer to give Tests without the need to pre-register users, try our Links option instead.</p>

<p>Learn more about the difference between: <a href="#">Groups & Links.</a></p>
                    </div>
                </div>
            </div>
        </div>
    	<div class="deta-search">
        	<div class="wrapper clear">
            	<div class="search-box groupsearch clear">
                	<form name="search" method="get" action="">
                        <div class="formcol">
                            <input type="text" name="grp" placeholder="Search Group Name" value="<?php echo e(!empty($_GET['grp'])?$_GET['grp']:''); ?>"/>
                            <button type="submit" value="search"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </form>
                </div>
                <a href="javascript:void(0);" class="colorbtn bluebtn newtest">+ &nbsp; New Group</a>
            </div>
        </div>
    	<div class="test-deta">
            <div class="wrapper">
                <table class="detatable grouptable">
					<?php if(!empty($groups)): ?>
						<?php foreach($groups as $val): ?>
						<tr>
							<td class="detacol detafld">
								<a href="#">
									<img src="<?php echo asset('assets/frontend/images/grp.png'); ?>" alt="">
									<p class="ttl"><?php echo e($val->title); ?></p>
								</a>
							</td>
						   <?php $memurl = 'user/'.$val->id.'/members';?>
							<td class="detacol memberdeta">
								<a href="<?php echo e(url($memurl)); ?>"><img src="<?php echo asset('assets/frontend/images/member.png'); ?>" alt=""><span>Member</span></a>
							</td>
							<!--<td class="detacol assigndeta">
								<a href="#"><img src="<?php echo asset('assets/frontend/images/assign.png'); ?>" alt=""><span>Assign</span></a>
							</td>-->
							<td class="detacol resultdeta">
								<a href="#"><img src="<?php echo asset('assets/frontend/images/result.png'); ?>" alt=""><span>Result</span></a>
							</td>
							<td class="detacol editdeta">
								<a href="javascript:void(0);" class="edt" ><img src="<?php echo asset('assets/frontend/images/new-file.png'); ?>" alt=""><span>Edit</span><input type="hidden" value="<?php echo e($val->id); ?>"/></a>
							</td>
							<td class="detacol removedeta">
								<a href="javascript:void(0);" class="rmve" >
									<img src="<?php echo asset('assets/frontend/images/remove.png'); ?>" alt=""><span>Remove</span>
									<input type="hidden" value="<?php echo e($val->id); ?>"/>
								</a>
							</td>
						</tr>
						<?php endforeach; ?>
					<?php endif; ?>
                </table>
            </div>
        </div>
    </section>
    <!--main content close-->
    
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script>

function  addGroup(id)
{	
	//var frmdata = $('form#addgrp').serializeArray();
	var titl = $('#title').val();
	$('#title').parent().find('.msg').remove();
	//console.log(frmdata);
	if(titl!="")
	{
		$.ajax({
			type:"post",
			url: "<?php echo url('user/add-group'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {title: titl,id: id},
			//dataType: "json",
			success:function(data) {
				//console.log(data);
				window.location.reload();
			}
		});		
	}else{
		$('#title').parent().append('<span class="msg">Please add Group name.<span>');
	}
}
$(".rmve").click(function() {
	var obj= $(this);
	//console.log(obj.parent().parent().find('.ttl').html());
	var txt = obj.parent().parent().find('.ttl').text();
	var gpId= obj.children('input').val();
	var r = confirm("Are you sure to delete group "+txt+"?");
	if (r == true) {
		$.ajax({
			type:"post",
			url: "<?php echo url('user/del-group'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {id: gpId},
			//dataType: "json",
			success:function(data) {
				//console.log(data);
				obj.parent().parent().remove();
			}
		});
	}	
	
});

$(".edt").click(function() {
	var obj= $(this);
	var gpId= obj.children('input').val();
	//console.log(obj.parent().parent().find('.ttl').html());
	var txt = obj.parent().parent().find('.ttl').text();
	$('#title').val(txt);
	$('.addnewbox').slideDown(200);
	$('#adGpbtn').attr('onclick','addGroup('+gpId+')');
});	
$(document).ready(function() {
    $('.newtest').click(function() {
		$('.addnewbox').slideDown(200);
		$("#addgrp")[0].reset();
		$('#adGpbtn').attr('onclick','addGroup(0)');
	});
	$('.newformwrap .closebtn').click(function() {
		$('.addnewbox').slideUp(200);
	});
	
	$('.membertable .setpassword a').click(function() {
		$(this).parent('.setpassword').parent('.testtablerow').next('.testlinkdeta').slideToggle(200);
	});
	
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>