<!doctype html>
<html>
<head>
<?php $settings = App()->settings;
$escapePages = array('contact-us','pricing');
$currpage=Request::segment(1);
?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=1">
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<?php if(!in_array($currpage,$escapePages)): ?>
<title> <?php echo e($settings->seo_title); ?></title>
<meta name="description" content="<?php echo e($settings->seo_description); ?>">
<meta name="keywords" content="<?php echo e($settings->seo_keywords); ?>">
<meta name="google-signin-client_id" content="781531976817-7e4fktuios98hhjonec4b6s75bmhu46j.apps.googleusercontent.com">
<?php else: ?>
	<?php echo $__env->yieldContent('seo_sec'); ?>
<?php endif; ?>
<!--Start CSS-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<link href="<?php echo asset('assets/frontend/slikslider/slick.css'); ?>" rel="stylesheet" type="text/css">
<!--<link href="assets/materialcharts/material-charts.css" rel="stylesheet" type="text/css">-->
 <?php echo $__env->yieldContent('customStyle'); ?>
<link href="<?php echo asset('assets/frontend/css/style.css'); ?>" rel="stylesheet" type="text/css">
<!--end CSS-->


    <!--Start Responsive CSS (Keep All CSS Above This)-->
    <link rel="stylesheet" type="text/css" href="<?php echo asset('assets/frontend/css/responsive.css'); ?>" />
    <!--End Responsive CSS (Don't Keep Any CSS Below This)-->
    <script>
        var BASE_URL = "<?php echo e(url('')); ?>";
    </script>

</head>
<body>
<div class="mainSite">
    <!--header open-->
	<header id="header">
    	<div class="wrapper clear">
        	<div class="logo">
            	<a href="<?php echo url(''); ?>"><img src="<?php echo asset('assets/frontend/images/logo.png'); ?>" width="159" height="32" alt=""></a>
            </div>
            <div class="header-right">
                <div id="navbarOverlay"></div>
            	<nav id="nav">
                	<ul>
                    	<li><a href="<?php echo e(url('')); ?>">Home</a></li>
                        <!-- <li><a href="<?php echo e(url('')); ?>">Tour</a></li> -->
                        <li><a href="javascript:void(0);">Uses</a>
                        	<ul>
                            	<li><a href="<?php echo e(url('')); ?>">Business</a></li>
                                <li><a href="<?php echo e(url('')); ?>">Education</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo e(url('pricing')); ?>">Pricing</a></li>
                        <li><a href="<?php echo e(url('stories')); ?>">Stories</a></li>
                        <li><a href="<?php echo e(url('about-us')); ?>">About</a></li>
                        <li><a href="blog.html">Blog</a></li>
                        <li><a href="<?php echo e(url('contact-us')); ?>">Contact us</a></li>
                    </ul>
                </nav>
                <div class="header-small">
                	<ul>
						<?php if(!Auth::check()): ?>
							<li class="login"><a href="<?php echo url('login'); ?>">Login</a></li>
							<li class="login"><a href="<?php echo url('register-as'); ?>" class="">Register</a></li>
						<?php else: ?>
						<?php $user= Auth::user();?>
						<li class="login">
							<a href="<?php echo url('user/account'); ?>">
								<figure>
								<?php if(!empty($user->profile_image)): ?>
								<img src="<?php echo asset('assets/upload/profile_image/'.$user->profile_image); ?>" width="35" height="35" alt=""/>
								<?php endif; ?>
								</figure><span><?php echo e($user->first_name); ?> <?php echo e($user->last_name); ?></span>
							</a>
							<ul>
								<li><a href="<?php echo url('user/my-details'); ?>"><figure><img src="<?php echo asset('assets/frontend/images/paper.png'); ?>" alt=""></figure><span>My Details</span></a></li>
								<!--<li>
									<a href="<?php echo url(''); ?>">
									<figure><img src="<?php echo asset('assets/frontend/images/users.png'); ?>" alt=""></figure><span>My Users</span>
									</a>
								</li>-->
								<li>
								<a href="<?php echo url('user/invoices'); ?>">
								<figure><img src="<?php echo asset('assets/frontend/images/tag.png'); ?>" alt=""></figure><span>Subscription</span>
								</a>
								</li>
								<?php if(Auth::user()->user_type==2): ?>
								<li>
								<a href="<?php echo url('user/groups'); ?>">
								<figure><img src="<?php echo asset('assets/frontend/images/command.png'); ?>" alt=""></figure><span>Groups</span>
								</a>
								</li>
								<li>
								<a href="<?php echo url('user/tests'); ?>">
								<figure><img src="<?php echo asset('assets/frontend/images/exam.png'); ?>" alt=""></figure><span>Tests</span>
								</a>
								</li>
								<li>
								<a href="<?php echo url('user/question-bank'); ?>">
								<figure><img src="<?php echo asset('assets/frontend/images/command.png'); ?>" alt=""></figure><span>Question bank</span>
								</a>
								</li>
								
								<?php else: ?>
								<li>
								<a href="<?php echo url('user/give-exam'); ?>">
								<figure><img src="<?php echo asset('assets/frontend/images/exam.png'); ?>" alt=""></figure><span>Give Exam</span>
								</a>
								</li>
								
								<?php endif; ?>
								<li>
								<a href="#">
								<figure><img src="<?php echo asset('assets/frontend/images/broken-link.png'); ?>" alt=""></figure><span>Links</span>
								</a>
								</li>
								<li>
								<a href="#">
								<figure><img src="<?php echo asset('assets/frontend/images/settings.png'); ?>" alt=""></figure><span>Settings</span>
								</a>
								</li>
								<li>
								<a href="<?php echo url('logout'); ?>">
								<figure><img src="<?php echo asset('assets/frontend/images/signout.png'); ?>" alt=""></figure><span>Logout</span>
								</a>
								</li>
							</ul>
						</li>
						<?php endif; ?>
                        <li class="quizhead"><a href="#" class="colorbtn">Create a Quiz</a></li>
                        <li class="lanbox"><a href="#" class="lanbtn"><img src="<?php echo asset('assets/frontend/images/flug.png'); ?>" width="29" height="29" alt=""></a>
                        	<ul>
                            	<li><a href="<?php echo e(url('sl/en')); ?>"><img src="<?php echo asset('assets/frontend/images/flug.png'); ?>" width="29" height="29" alt=""></a></li>
                                <li><a href="<?php echo e(url('sl/pl')); ?>"><img src="<?php echo asset('assets/frontend/images/flug.png'); ?>" width="29" height="29" alt=""></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <span id="smallNavbar">
                    <i class="fa fa-bars"></i>
                </span>
            </div>
        </div>
    </header>
    <!--header close-->
	
    <?php echo $__env->yieldContent('content'); ?>

    <!--customer support open-->
    <div class="customer-support">
        <div class="wrapper clear">
            <div class="customer-supp">
                <img src="<?php echo asset('assets/frontend/images/customer-support.png'); ?>" width="81" height="89" alt="">
                <a href="#" class="colorbtn bluebtn">Sales and Customer Service</a>
            </div>
        </div>
    </div>
    <!--customer support close-->
    <!--footer open-->
    <footer id="footer">
    	<div class="footertop">
        	<div class="wrapper clear">
            	<div class="footerbox logobox">
                	<div class="footerlogo"><a href="index.html"><img src="<?php echo asset('assets/frontend/images/footer-logo.png'); ?>" width="159" height="32" alt=""></a></div>
                </div>
                <div class="footerbox menubox">
                	<nav>
                    	<ul>
                        	<li><a href="#">Home</a></li>
                            <li><a href="#">Tour</a></li>
                            <li><a href="#">Uses</a></li>
                            <li><a href="#">Pricing</a></li>
                            <li><a href="#">Stories</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="footerbox socialbox">
                	<nav>
                    	<ul>
                        	<li><a href="#">About</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#" class="userlink">Login</a><a href="#" class="userlink">Register</a></li>
                        </ul>
                    </nav>
                    <div class="socialicon">
                    	<ul>
                        	<li><a href="#" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="footerbox helpbox">
                	<div class="footer-col">
                    	<span>Language</span>
                        <select>
                        	<option>English</option>
                        </select>
                    </div>
                    <div class="footer-col">
                    	<span>Help line</span>
                        <a href="tel:1 - 111 - JKJ - BBBI" class="callto"><i class="fa fa-phone" aria-hidden="true"></i>1 - 111 - JKJ - BBBI</a>
                    </div>
                </div>
                <div class="footerbox fimgbox">
                	<img src="<?php echo asset('assets/frontend/images/pay.png'); ?>" width="109" height="59" alt="">
                    <img src="<?php echo asset('assets/frontend/images/visa.png'); ?>" width="284" height="34" alt="">
                    <img src="<?php echo asset('assets/frontend/images/secu.png'); ?>" width="298" height="26" alt="">
                </div>
            </div>
        </div>
        <div class="footerbot">
        	<div class="wrapper">
            	<span>Copyright &copy; 2016. All rights reserved</span>
            </div>
        </div>
    </footer>
	
</div>

<script src="<?php echo asset('assets/frontend/js/jquery.min.js'); ?>" type="text/javascript"></script>

<!--Start js-->
<script src="<?php echo asset('assets/frontend/js/placeholders.min.js'); ?>"></script>
<script src="<?php echo asset('assets/frontend/slikslider/slick.min.js'); ?>"></script>

<!--end js-->

<?php echo $__env->yieldContent('customScript'); ?>

<script src="<?php echo asset('assets/frontend/js/custom.js'); ?>" type="text/javascript"></script>

<script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"></script>
<script src="https://apis.google.com/js/platform.js?" async defer></script>
<script language="javascript">
/**************** Vk Login ***************************/

VK.init({
  apiId: 5668955,
});
function authInfo(response) {
	//console.log(response);
  if (response.session) {
    //alert('user: '+response.session.mid);
	getUser(response.session.mid);
  } else {
    
  }
}
//VK.Auth.getLoginStatus(authInfo);
//VK.UI.button('login_button');
function getUser(uid)
{
	var infos = "nickname, screen_name, sex, bdate (birthdate), city, country, timezone, status, photo, photo_medium, photo_big, has_mobile, contacts, education, online, counters, relation, last_seen, activity, can_write_private_message, can_see_all_posts, can_post, universities, photo_50, city, verified, followers_count";
	VK.api("users.get", {uids:uid,fields:infos}, function(data) { 
		// actions with received data 
		//console.log(data.response);		
		var udata=data.response[0];
		//alert(Object.keys(udata).length);
		if(Object.keys(udata).length > 1)
		{
			udata['social_type']='vk';
			registerUser(udata);
		}
		//alert('Hello, '+udata['first_name']+udata['last_name']);
	});
	/*VK.Api.call('getVariable', {key: 5668955}, function(r) {
	  if(r.response) {
		console.log(r.response);
		alert('Hello, ' + r.response);
	  }
	});*/
}

/******************************* FB Login ****************************/

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1184275914966230',
      xfbml      : true,
      version    : 'v2.8'
    });
 };

(function(d, s, id){
	 var js, fjs = d.getElementsByTagName(s)[0];
	 if (d.getElementById(id)) {return;}
	 js = d.createElement(s); js.id = id;
	 js.src = "//connect.facebook.net/en_US/sdk.js";
	 fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

/* FB.getLoginStatus(function(response) {
  if (response.status === 'connected') {
    console.log('Logged in.');
  }
  else {
    FB.login();
  }
}); */
function myFacebookLogin() {
  FB.login(function(){
	  testAPI();
  }, {scope: 'public_profile,email,user_friends',/*  auth_type:'reauthenticate' */ });
}
function testAPI() {
    FB.getLoginStatus(function(response) {
	  if (response.status === 'connected') {
		//console.log(response);
		//console.log(response.authResponse.accessToken);
		FB.api('/me', function(response) {
			getUsrDta(response.id);
		});
	  }
	});
    
  }
  
function getUsrDta(userID) {
    FB.api(userID + "/?fields=id,email,name,first_name,last_name,gender,location", function(response) {
		//console.log(response);
		response['social_type']='fb';
		registerUser(response);
    });
}
 
 
/******************************* G+ Login ****************************/
/*'781531976817-7e4fktuios98hhjonec4b6s75bmhu46j.apps.googleusercontent.com'*/
function gplusLogin() {
	gapi.load('auth2', function() {
		auth2 = gapi.auth2.init({
		client_id: '781531976817-7e4fktuios98hhjonec4b6s75bmhu46j.apps.googleusercontent.com',
		fetch_basic_profile: true,
		scope: 'profile'
		});

		// Sign the user in, and then retrieve their ID.
		auth2.signIn().then(function()
		{
			//console.log(auth2.currentUser.get().getId());
			var currentUser = auth2.currentUser.get();
			var userId = auth2.currentUser.get().getId();
			var profile = currentUser.getBasicProfile();
			/* console.log(profile);
			console.log('ID: ' + profile.getId());
			console.log('Image URL: ' + profile.getImageUrl()); */
			
			var gpUser ={ 
			"social_type":"gp",
			"first_name":profile.getGivenName(),
			"last_name":profile.getFamilyName(),
			"name":profile.getName(),
			"email":profile.getEmail() 
			};
			registerUser(gpUser);
		});
	});
}

/*************************************************************************/
function registerUser(udata)
{
	/* console.log(udata);	 */
	$.ajax({
		type:"post",
		url: "<?php echo url('social-login'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: udata,			
		//dataType: "json",			
		success:function(res) {
			console.log(res);
			window.location.reload();
		}

	});
}
</script>

</body>
</html>