<script type="text/javascript">

var availableTags1 = [
	<?php if(!empty($all_tropics)): ?>
	  <?php foreach($all_tropics as $jval): ?>
		<?php echo '{"label":"'.$jval->title.'","id":"'.$jval->id.'"} ,'; ?>

	  <?php endforeach; ?>
	<?php endif; ?>
];
$( ".jobs" ).autocomplete({
      source: '<?php echo e(url("user/job-autocomplete")); ?>'
});
$( ".major" ).autocomplete({
      source: '<?php echo e(url("user/major-autocomplete")); ?>'
});
$("#src_tropic").autocomplete({
	source: availableTags1,
	select: function (event, ui) {
		//console.log(ui);
		/* var preVal = $("#t_ids").val();
		if(preVal!='')	preVal+=',';
		preVal+=ui.item.id; */
		$("#t_ids").val(ui.item.id);			
	}
});
$( "form#searchFrm" ).on( "submit", function( event ) {
//
  event.preventDefault();
  var formdata =  $( this ).serialize();
  //console.log( $( this ).serialize() );
  searchUsers(formdata);
});
$( "form#searchFrm2" ).on( "submit", function( event ) {
//
  event.preventDefault();
  var formdata =  $( this ).serialize();
  //console.log( $( this ).serialize() );
  searchUsers(formdata);
});

function searchUsers(formdata)
{
	$.ajax({
		type:"post",
		url: "<?php echo url('user/search-users'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: formdata,			
		//dataType: "json",			
		success:function(res) {
			/* console.log(res); */
			if(res.has_error==0)
			{
				if(res.list_type==1)
				{
				$('#usrlist').html(res.ajax_html);
				$('#match_count').html(res.match_count);					
				}else{
				$('#srchusrlist').html(res.ajax_html);
				$('#srch_count').html(res.match_count);					
				}
			}
		}
	});
}


$( "form#userfrm" ).on( "submit", function( event ) {
//
  event.preventDefault();
  var formdata =  $( this ).serializeArray();
  //console.log(formdata );
  var uids='';var emails='';
  $.each(formdata, function( index, el ) {
	   //console.log('k='+el.value+'v='+el.name );
	   uids += ((index!=0)?',':'')+el.value;
	   emails += el.name+',';
  });
  $.fancybox.close();
  $("#askbtn").fancybox().trigger('click');
   $('#emails').tagit('destroy');
  $("#qbxbtn").parent().children('input#emails').val(emails);
  $("#qbxbtn").parent().children('input#inpids').val(uids);
  

});
$( "#jobloc" ).autocomplete({
      source: '<?php echo e(url("user/company-autocomplete")); ?>'
});
$( "#education" ).autocomplete({
      source: '<?php echo e(url("user/university-autocomplete")); ?>'
});
$('a.followbtn').click(function(){
	var fobj = $(this);
	var followto = fobj.next('input').val();
	/* alert(followto); */
	$.ajax({
		type:"post",
		url: "<?php echo e(url('user/follow-unfollow')); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: {'followto':followto},			
		dataType: "json",			
		success:function(res) {
			/* console.log(res); */
			if(res==0){
				//fobj.children('span').text('Unfollow').css('color','#3498db');
				fobj.children('span').text('Unfollow');
				//fobj.addClass('factive');
			}
			else if(res==2){
				//fobj.children('span').text('Follow').css('color','#6c6c6c');
				fobj.children('span').text('Follow');
				//fobj.removeClass('factive');
			}
		}
	});	
});


$(document).ready(function(){
   
	
	
	$("#frndlist").click(function(){
        $(".memberPnl, .overlay").addClass("show");
    });
	$(".overlay, .chatMmbrList").click(function(){
		$(".overlay").removeClass("show");
        $(" .memberPnl").removeClass("show");
    });
	
});

$("#qsnFrm").ajaxForm({
	beforeSubmit : function(formData, jqForm, options){
		var frmobj = $("#qsnFrm");
		$(".emsg").remove();
		var isValid=1;
		var post_type = frmobj.find("input[name=post_type]").val();
		if(post_type==undefined)
		{
			var post_type = frmobj.find("select[name=post_type]").val();
		}
		//alert(post_type);
		
		var tagObj = $('#singleFieldTags2');
		var emailObj = $('#emails');
		var titleObj = frmobj.find("input[name=title]");
		var contentObj = frmobj.find("textarea[name=content]");
		
		var is_forword = frmobj.find("input[name=is_forword]").val();
		var frm_type = frmobj.find("input[name=frm_type]").val();
		/* alert(is_forword); */
		if(frm_type=='a')
		{
			if( titleObj.val()=='' && is_forword==0)
			{
				titleObj.parent().append('<span class="emsg">Please fill out this field.</span>');
				isValid=0;
				return false;
			}
			if( contentObj.val()=='' && is_forword==0)
			{
				contentObj.parent().append('<span class="emsg">Please fill out this field.</span>');
				isValid=0;
				return false;
			}
			
			if(post_type==1 && tagObj.val()=='' && is_forword==0)
			{
				tagObj.parent().append('<span class="emsg">Please fill out this field.</span>');
				isValid=0;
				return false;
			}
			else if((post_type==2 ||  is_forword==1) && emailObj.val()=='')
			{
				emailObj.parent().append('<span class="emsg">Please fill out this field.</span>');
				isValid=0;
				return false;
			}
			
		}
		
	},
	success: function(responseText, statusText, xhr, jform){ 
		console.log(responseText);
		var data = responseText;
		jform.clearForm();
		if(data && statusText == 'success')
		{					
			$("#qMsg").css('display','inline-block').html(data);
			//frmObj.find('input[type=text],select,textarea,checkbox').val('');
			$("#emails").val('');
			$("#singleFieldTags2").val('');
			$("#qsnFrm").hide();
		}
		else if(statusText == 'error')
		{
			alert('Sorry! internal server error.');
		}
	}
});


$(document).on('click','.rmdBtn',function(){	
	var cobj = $(this);
	var qsn_id = cobj.attr('qid');
	
	if(qsn_id != '')
	{
		
		$.ajax({
			type:"POST",
			url:"<?php echo url('user/send-remind-msg'); ?>",
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data:{qsn_id:qsn_id,type:'T'},
			dataType: "json",
			success:function(response){
			   if(response.has_error==0)
			   {
				   alert("Remind sucussesfully send.");
			   }
			}
		});
	}	
});

$(document).on('click','.passBtn',function(){	
	var cobj = $(this);
	var qsn_id = cobj.attr('qid');
	
	if(qsn_id != '')
	{
		
		$.ajax({
			type:"POST",
			url:"<?php echo url('user/pass-question'); ?>",
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data:{question_id:qsn_id},
			dataType: "json",
			success:function(response){
			   if(response.has_error==0)
			   {
				   //alert("Pass sucussesfully send.");
				  cobj.parent().parent().parent().parent('.middlerow').css('background-color','#ececec');
			   }
			}
		});
	}	
});
$(document).on('click','.slctAll',function(){
	
	if($(this).is(":checked")==true)
	{
		$("#userfrm").find('input:checkbox').attr('checked',true);
	}else{
		$("#userfrm").find('input:checkbox').attr('checked',false);
	}
});
$(document).on('click','#slctAll2',function(){
	
	if($(this).is(":checked")==true)
	{
		$("#sendReqstfrm").find('input:checkbox').attr('checked',true);
	}else{
		$("#sendReqstfrm").find('input:checkbox').attr('checked',false);
	}
});

</script>




