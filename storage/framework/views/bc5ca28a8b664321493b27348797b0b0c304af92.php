<?php

/**

 * Page resource built upon CoC

 * You can leave this as it is

 * or feel free to remove these configuration and customize

 * @author Tuhin | <tuhin@technoexponent.com>

 */

$resource = 'add-invoice';

$resource_pl = str_plural($resource);

$page_title = ucfirst($resource);

// Resolute the data

//$data = ${$resource};



?>







<?php $__env->startSection('pageTitle', $page_title); ?>



<?php $__env->startSection('content'); ?>

<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>

            <?php echo $page_title; ?>


            <small>Add </small>

        </h1>

        <ol class="breadcrumb">

            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>

            <li><a href="<?php echo url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>

            <li><a href="<?php echo url('admin/user/'.$user->id.'/invoices'); ?>"><i class="fa  fa-user"></i> Invoices </a></li>

            <li class="active">Add Invoice</li>

        </ol>

    </section>

    <!-- Main content -->

    <section class="content">

        <div class="row">

            <div class="col-md-12">

                <div class="box box-info">

                    <div class="box-header with-border">

                        <h3 class="box-title">Edit User</h3>

                    </div>

                    <?php if($errors->any()): ?>

                        <div class="alert alert-danger">

                            <?php foreach($errors->all() as $error): ?>

                                <p><?php echo $error; ?></p>

                            <?php endforeach; ?>

                        </div>

                    <?php endif; ?>

                    <?php if(session('success')): ?>

                    <div class="alert alert-success">

                        <?php echo session('success'); ?>


                    </div>

                    <?php endif; ?>

                    <form method="POST" action="<?php echo admin_url('user/add-invoice/'); ?>" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
					
					<?php echo e(csrf_field()); ?>

						<input type="hidden" name="user_id" value="<?php echo e($user->id); ?>" />
						<div class="box-body">
							<div class="invoiceNew">
								<div class="fullrow clear">
									<div class="col02">
										<label>
											<span class="label">Bill to:</span>
											<div class="textfld"><input name="to_email" type="text" placeholder="Email Address" value="<?php echo e(!empty(old('to_email')) ? old('to_email'):$user->email); ?>" /></div>
										</label>
									</div>
									<div class="col02">
										<label>
											<span class="label">Sender Email:</span>
											<div class="textfld"><input name="from_email" type="text" placeholder="Email Address" value="<?php echo e(!empty(old('from_email')) ? old('from_email'):$setting->contact_email); ?>" /></div>
										</label>
									</div>
								</div>
								<div class="customBox clear">
									<span class="custmLbl">Plan</span>
									<div class="customRight clear">
										<div class="col03">
											<select id="package_id" name="package_id">
												<option value="">Choose package</option>
												<?php if(!empty($packages)): ?>
													<?php foreach($packages as $val): ?>
													<option value="<?php echo e($val->id); ?>" <?php echo e(old('package_id')==$val->id ? 'selected' : ''); ?> > <?php echo e($val->plan); ?> </option>
													<?php endforeach; ?>
												<?php endif; ?>
											</select>
										</div>
										<!--<div class="col03">
											<select><option>Add / Remove Detail</option></select>
										</div>
										<div class="col03">
											<select><option>U.S.D - U.S Dollars</option></select>
										</div>-->
									</div>
								</div>
								<div class="fullrow">
									<table class="lrgtable">
										<tr>
											<th class="descinvoice">Description</th>
											<th class="ptbl">Price</th>
											<th class="txtbl">Vat</th>
											<th>Amount</th>
										</tr>
										<tr>
											<td class="descinvoice"> <textarea id="descrp" name="description" placeholder="Description" ><?php echo e(old('description')); ?></textarea></td>
											<td><input type="text" id="price" name="price" value="<?php echo e(old('price')); ?>" placeholder="0" onChange="clacTotal(this.value,document.getElementById('vat').value)" /></td>
											<td><input type="text" id="vat" name="vat" value="<?php echo e(old('vat')); ?>"  placeholder="0" onChange="clacTotal(document.getElementById('price').value,this.value)" /></td>
											<td>$<span id="tot_amount">00.00</span></td>
										</tr>
									</table>
								</div>
								<!--<div class="fullrow clear">
									<div class="col02">
										<table class="smallTable">
											<tr>
												<td><span>Subtotal</span></td>
												<td class="smlbox">$0.00</td>
											</tr>
											<tr>
												<td>
													<table class="inTable">
														<tr>
															<td>Discount</td>
															<td><input type="text" placeholder="0"><select><option>%</option></select></td>
														</tr>
													</table>
												</td>
												<td class="smlbox">$0.00</td>
											</tr>
											<tr>
												<td>
													<table class="inTable">
														<tr>
															<td>Shipping/handing</td>
															<td><input type="text" placeholder="" class="fullbox"></td>
														</tr>
													</table>
												</td>
												<td class="smlbox">$0.00</td>
											</tr>
											<tr>
												<td><span><strong>Total</strong></span></td>
												<td class="smlbox"><strong>$0.00 USD</strong></td>
											</tr>
										</table>
									</div>
								</div>-->
								<div class="fullrow clear">
									<div class="col02">
										<h3>Note to recipient</h3>
										<textarea name="note" placeholder=""><?php echo e(old('note')); ?></textarea>
									</div>
									<div class="col02">
										<h3>Terms and conditions</h3>
										<textarea name="tc" placeholder=""><?php echo e(old('tc')); ?></textarea>
									</div>
								</div>
								<!--<div class="fullrow rightTxt">
									<label class="attachFile colorbtn"><input type="file" value="uploade">Attach File</label>
								</div>
								<input type="submit" value="Submit" class="bluebtn"/>-->
							</div>
						</div>
                        <div class="box-footer">

                            <a class="btn btn-default" href="<?php echo url('admin/$resource'); ?>"> Back</a>

                            <button type="submit" class="btn btn-info pull-right">Submit</button>

                        </div><!-- /.box-footer -->

                   </form>

                </div><!-- /.box -->

            </div>

        </div>

    </section>

    <!-- /.content -->

</div><!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>



<?php $__env->startSection('customScript'); ?>

<script type="text/javascript">
function clacTotal(price,vat)
{
	if(price=='')	price=0;
	if(vat=='')		vat=0;
	var tot_amount = parseFloat(price)+parseFloat(vat);
	$("#tot_amount").html(parseFloat(tot_amount).toFixed(2));
}

$("#package_id").change(function() {

	var obj= $(this);
	var package_id = obj.val();
	//alert(package_id);
	if ( package_id!='') {

		$.ajax({

			type:"post",

			url: "<?php echo url('get-package'); ?>" ,

			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},

			data: {id: package_id},

			dataType: "json",

			success:function(data) {

				//console.log(data);
				if(data!="error")
				{
					$("#descrp").text(data['details']);
					$("#price").val(data['price']);
					$("#tot_amount").text(data['price']);
				}
			}

		});

	}		

});
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>