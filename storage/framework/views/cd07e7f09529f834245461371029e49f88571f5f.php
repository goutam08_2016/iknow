<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<div class="logincontent">
		<p class="logintag">Get the right answer from the right person in the right time</p>		
		<?php if($errors->any()): ?>

			<div id="login_warnings" class="warnings" style="">

				<div id="login_error_message" class="error_message">

					<?php foreach($errors->all() as $error): ?>
					
						<p><?php echo $error; ?></p>

					<?php endforeach; ?>

				</div>

			</div>

		<?php elseif(session('register_success')): ?>

			<div class="success_message">

				<p><?php echo session('register_success'); ?></p>

			</div>

			<?php session()->forget('register_success');?>
		<?php endif; ?>

		<form method="post" id="loginForm" action="<?php echo e(url('/login')); ?>" data-parsley-validate="">
			<?php echo e(csrf_field()); ?>

			<div class="formrow sociallogin clear">
				<a href="javascript:void(0);" class="fblogin" onclick="myFacebookLogin();"><span><img src="<?php echo asset('assets/frontend/images/fb.png'); ?>" alt=""></span>Login with Facebook</a>
				<a href="javascript:void(0);" class="glogin" onclick="gplusLogin();"><span><img src="<?php echo asset('assets/frontend/images/google.png'); ?>" alt=""></span>Login with Gmail</a>
			</div>
			<div class="formrow alteroption">
				<span class="alter">or</span>
			</div>
			<div class="formrow borderbox">
				<input type="email" name="email" data-parsley-trigger="change" required  Placeholder="Email">
			</div>
			<div class="formrow borderbox">
				<input type="password" name="password" Placeholder="Password" required />
			</div>
			<div class="formrow remember clear">
				<div class="rememberbox">
					<label>
						<input type="checkbox" name="rem">
						<span class="checkbox"></span>
						<span class="label">Remember me</span>
					</label>
				</div>
				<a href="#" class="forgotpass">Forgot  your  password?</a>
			</div>
			<div class="formrow loginbutton clear">
				<input type="submit" value="Login  as member" class="memberlogin">
				<input type="submit" value="Login  as guest" class="guestlogin">
			</div>
			<span class="notmember">Not a member? <a href="<?php echo e(url('register')); ?>">Sign up now</a></span>
		</form>
	</div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.basic', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>