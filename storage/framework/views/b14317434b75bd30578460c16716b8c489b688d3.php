<?php $__env->startSection('pageTitle', 'Welcome to '); ?>



<?php $__env->startSection('customStyle'); ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
setMapLoc('address');
 setMapLoc('current_address');

</script>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>

	<!--body open-->
    
    	<!--left pan open-->
        <aside class="leftpan invisibleLeft">
        	<div class="languageSelect clear">
            	<a href="" onclick="return false;" class="lanTab opn" id="vniL" title="VNI">VNI</a>
                <a href="" onclick="return false;" class="lanTab" id="engL" title="ENG">ENG</a>
            </div>
        	<div class="left-top">
            	<ul>
                	<li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>Profile</a></li>
                	<li><a href="#"><i class="fa fa-book" aria-hidden="true"></i>Read</a></li>
                    <li><a href="#"><i class="fa fa-clipboard" aria-hidden="true"></i>Answer</a>
                    	<ul>
                        	<li><a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>sent questions</a></li>
                            <li><a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>received questions</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-th-list" aria-hidden="true"></i>Topics</a></li>
                    <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i>Contact</a></li>
                    <li><a href="#"><i class="fa fa-bell" aria-hidden="true"></i>Notifications</a></li>
                    <li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i>setting</a>
                    	<ul>
                        	<li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>Account</a></li>
                            <li><a href="#"><i class="fa fa-lock" aria-hidden="true"></i>Privacy</a></li>
                            <li><a href="#"><i class="fa fa-comment" aria-hidden="true"></i>Notification</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-sign-out" aria-hidden="true"></i>logout</a></li>
                </ul>
            </div>
            <div class="leftbot">
            	<ul>
                	<li>
                    	<p>You answered question</p>
                        <a href="#">What are good ways to learn to become the best digital marketer?.</a>
                        <span>on July 22, 2016 </span>
                    </li>
                    <li>
                    	<p>You answered question</p>
                        <a href="#">What are good ways to learn to become the best digital marketer?.</a>
                        <span>on July 22, 2016 </span>
                    </li>
                    <li>
                    	<p>You answered question</p>
                        <a href="#">What are good ways to learn to become the best digital marketer?.</a>
                        <span>on July 22, 2016 </span>
                    </li>
                </ul>
            </div>
        </aside>
        <!--left pan close-->
        
        <!--middle open-->
        
        <div class="pageMiddle">
        	<div class="contactNext clear">
            
            	<div class="nextLeft">
                	<div class="profileTop">
                    	<div class="editName">
                            <h2 class="nicknameTl">
								<span>
									<font class="nickname" ><?php echo e($user->nickname?$user->nickname:'No Nickname'); ?></font>
									<?php if(Auth::check() && $user->id == Auth::user()->id): ?>
									<a href="javascript:void(0);" id="edtNme"><img src="<?php echo asset('assets/frontend'); ?>/images/edit.png" alt=""></a>
									<?php endif; ?>
								</span>
							</h2>
                            <div class="editmain">
                                <div class="noclass clear">
                                    <div class="detaRow smallText clear">
										<form class="userdt" name="" method="post" action="">
                                        <input type="text" class="nicknameinp" name="nickname" placeholder="Enter NickName" value="<?php echo e($user->nickname); ?>">
										<input type="submit" value="Done" class="bluebtn">
										</form>
                                    </div>
                                    <a href="javascript:void(0);" class="bluebtn lightbtn nCan">Cancel</a>
                                </div>
                            </div>
                        </div>
                    	<div class="topProRow clear">
                        	<div class="editImg">
                        		<div class="addP">
									<?php 
									if(file_exists('assets/upload/profile_image/'.$user->profile_image))
									{
										$profImag =asset('assets/upload/profile_image/'.$user->profile_image);
									}else{
										$profImag = asset('assets/frontend/images/profile.jpg');
									}									
									?>
                                    <figure><img id="dpImg" src="<?php echo $profImag; ?>" alt=""></figure>
									<form id="dpFrm" action="<?php echo e(url('user/upload-dp')); ?>" method="post" enctype="multipart-formData">
										<label class="addBtn"><input id="profile_image" name="profile_image" type="file"/><i class="fa fa-camera"></i><span>Upload</span></label>
									</form>
                                </div>
                            	<!--<label class="chgPhoto bluebtn"><input type="file"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</label>-->
                            </div>
							<?php
							$today = time();
							$dobtime = strtotime($user->dob);
							$age = round((int)($today - $dobtime)/(86400*365));
							?>
                            <div class="topProText">
                            	<ul>
                                	<li><i class="fa fa-plus" aria-hidden="true"></i> Registered : <?php echo date('d-m-Y',strtotime($user->created_at)); ?></li>
									<?php $curr_addr = $user->user_Addresses()->where('is_default','Y')->first();?>
                                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> Location : <font id="curr_addrs"><?php echo e(!empty($curr_addr)?$curr_addr->address:''); ?></font></li>
                                    <li><i class="fa fa-heart" aria-hidden="true"></i> Age : <font class="ag"><?php echo $user->age; ?></font></li>
                                    <li><i class="fa fa-transgender" aria-hidden="true"></i> Sex : <font class="gender"><?php echo e($user->gender=='M'?'Male':'Female'); ?></font></li>
                                </ul>
                            </div>
                        </div>
                        <div class="descProShort editAbout">
							<?php if(Auth::check() && $user->id == Auth::user()->id): ?>
								<a href="javascript:void(0);" class="editForm editSingle" style="right:0; top:0;"><img src="<?php echo asset('assets/frontend'); ?>/images/edit.png" alt=""></a>
							<?php endif; ?>
							<h3>About Me</h3>
							<div class="saveDeta details aboutContArea">
								<?php echo $user->details; ?>

							</div>
							<div class="editmain aboutEditarea">
								<div class="noclass clear">
									<div class="detaRow smallText clear aboutDatarow">
									<form class="userdt" name="" method="post" action="">
										<textarea type="text" class="" name="details" placeholder="Enter about yourself"><?php echo $user->details; ?></textarea><input type="submit" value="Done" class="bluebtn"/>
										<a href="javascript:void(0);" class="bluebtn lightbtn cancelEdit">Cancel</a>
									</form>
									</div>
								</div>
							</div>
							<?php if(Auth::check() && $user->id != Auth::user()->id): ?>
                            <ul>
                                <li>
									<a href="javascript:void(0);" class="frndReq <?php echo e(!empty($frequest)?'sent':''); ?>">
										<i class="fa fa-user-plus" aria-hidden="true"></i>
										<span>
										<?php if(!empty($frequest) && $frequest->status=='Y'): ?>
											<?php echo e('Friends'); ?>

										<?php elseif(!empty($frequest) && $frequest->status=='N'): ?>
											<?php echo e('Request Sent'); ?>

										<?php else: ?>
											<?php echo e('Add Friend'); ?>

										<?php endif; ?>
										</span>
										<input type="hidden" value="<?php echo e($user->id); ?>"/>
									</a>
								</li>
                                <li><a href="javascript:void(0);" class="followbtn " ><i class="fa fa-plus-square-o" aria-hidden="true"></i><span><?php echo e(in_array($user->id,$userfollows)?'Unfollow':'Follow'); ?></span></a><input type="hidden" value="<?php echo e($user->id); ?>"/></li>
                                <li><a href="<?php echo e(url('chat/messages/'.$user->id)); ?>"><i class="fa fa-comments-o" aria-hidden="true"></i><span>message</span></a></li>
                                <li><a href=""><i class="fa fa-question-circle" aria-hidden="true"></i><span>ask question</span></a></li>
                            </ul>
							<?php endif; ?>
                        </div>
                    </div>
                    
                    <div class="profileTab">
                        <div class="filterBtn tabmenu">
                            <ul>
                                <li><a href="javascript:void(0);" id="tab1">About</a></li>
                                <li><a href="javascript:void(0);" id="tab2">Recent activities</a></li>
                                <li><a href="javascript:void(0);" id="tab3">Contact information</a></li>
                                
                            </ul>
                        </div>
                        
                        <div class="tabContWrap">
                            <div class="tabAbout tabCont">
                            	<div class="smallRow editAbout">
									<?php if(Auth::check() && $user->id == Auth::user()->id): ?>
                                	<a href="javascript:void(0);" class="editForm editSingle"><img src="<?php echo asset('assets/frontend'); ?>/images/edit.png" alt=""></a>
									<?php endif; ?>
                                	<h3><i class="fa fa-user-o"></i> Full Name</h3>
                                    <div class="saveDeta"><p class="fullnam"><?php echo e($user->first_name); ?> <?php echo e($user->last_name); ?> </p></div>
                                    <div class="editmain">
                                    	<div class="noclass clear">
                                            <div class="detaRow smallText clear">
											<form class="userdt" name="" method="post" action="">
                                                <input type="text" class="fullnaminp" name="fullname" placeholder="Enter Full Name" value="<?php echo e($user->first_name); ?> <?php echo e($user->last_name); ?>"/><input type="submit" value="Done" class="bluebtn"/>
											</form>
                                            </div>
                                            <a href="javascript:void(0);" class="bluebtn lightbtn cancelEdit">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="smallRow editAbout">
									<?php if(Auth::check() && $user->id == Auth::user()->id): ?> 
                                	<a href="javascript:void(0);" class="editForm editSingle"><img src="<?php echo asset('assets/frontend'); ?>/images/edit.png" alt=""></a>
									<?php endif; ?>
                                	<h3><i class="fa fa-intersex"></i> Sex</h3>
                                    <div class="saveDeta"><p class="gender" ><?php echo e($user->gender=='M'?'Male':'Female'); ?></p></div>
                                    <div class="editmain">
                                    	<div class="noclass clear">
                                            <div class="detaRow smallText clear">
												<form class="userdt" name="" method="post" action="">
                                                <div class="dobBox sbBox clear">
                                                    <div class="datebox">													
                                                        <select class="gendersl" name="gender">
															<option value="M" <?php echo e($user->gender=='M'?'selected':''); ?> >Male</option>
															<option value="F" <?php echo e($user->gender=='F'?'selected':''); ?> >Female</option>
														</select>													
                                                    </div>
                                                </div>
                                                <input type="submit" value="Done" class="bluebtn">
												</form>
                                            </div>
                                            <a href="javascript:void(0);" class="bluebtn lightbtn cancelEdit">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="smallRow editAbout">
									<?php if(Auth::check() && $user->id == Auth::user()->id): ?> 
                                	<a href="javascript:void(0);" class="editForm editSingle"><img src="<?php echo asset('assets/frontend'); ?>/images/edit.png" alt=""></a>
									<?php endif; ?>
                                	<h3><i class="fa fa-calendar"></i> Date of Birth</h3>
									<?php
									$dob = $user->dob;
									$dob_arr = explode('-',$dob);
									?>
                                    <div class="saveDeta">
										<p class="dob"><?php echo e($user->dob); ?></p>
									</div>
                                    <div class="editmain">
                                    	<div class="noclass clear">
                                            <div class="detaRow smallText clear">
												<form class="userdt" name="" method="post" action="">
                                                <div class="dobBox clear">
                                                    <div class="datebox">
                                                        <select name="day">
														<option value="">Day</option>
															<?php for( $i=1; $i<=31; $i++ ): ?>
															<?php $slct = isset($dob_arr[0]) && $dob_arr[0] == $i ? "selected":""; ?>
															<option value="<?php echo e($i); ?>" <?php echo e($slct); ?> ><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
                                                    </div>
                                                    <div class="datebox">
                                                        <select name="month">
															<option value="">Month</option>
															<?php for( $i=1; $i<=12; $i++ ): ?>
															<?php $slct = isset($dob_arr[1]) && $dob_arr[1] == $i ? "selected":"";?>
															<option value="<?php echo e($i); ?>" <?php echo e($slct); ?> ><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
                                                    </div>
                                                    <div class="datebox">
                                                        <select name="year" required>
															<option value="">Year <strong>*</strong></option>
															<?php for( $i=1950; $i<=2016; $i++ ): ?>
															<?php $slct = isset($dob_arr[2]) && $dob_arr[2] == $i ? "selected":"";?>
															<option value="<?php echo e($i); ?>" <?php echo e($slct); ?> ><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
                                                    </div>
                                                </div>
                                                <input type="submit" value="Done" class="bluebtn">
												</form>
                                            </div>
                                            <a href="javascript:void(0);" class="bluebtn lightbtn cancelEdit">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="smallRow editAbout">
									<?php if(Auth::check() && $user->id == Auth::user()->id): ?>
                                	<a href="javascript:void(0);" class="editForm editSingle"><img src="<?php echo asset('assets/frontend'); ?>/images/edit.png" alt=""></a>
									<?php endif; ?>
                                	<h3><i class="fa fa-map-marker"></i> Place you live (now/in the past)</h3>
                                    <div class="saveDeta">
										<div class="detaBox" id="current_address_shw">
										<?php if(!empty($user->user_Addresses)): ?>
										<?php foreach($user->user_Addresses as $adr): ?>
										<?php if($adr->type==1): ?>
                                        	<span class="pEdit withLocation" id="myadr_<?php echo e($adr->id); ?>">
											<?php echo e($adr->address); ?>

											<a href="javascript:void(0);" class="removeText"></a>
											<input type="hidden" value="<?php echo e($adr->id); ?>"/>
											<span class="loc <?php echo e($adr->is_default=='Y' ?'active':''); ?>"><i class="fa fa-map-marker"></i></span>
											</span>
										<?php endif; ?>
										<?php endforeach; ?>
										<?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="editmain">
                                    	<div class="noclass clear">
                                            <div class="detaRow smallText clear locField">
                                                <input class="loc" id="current_address" name="current_address" type="text" value=""/>
												<input type="hidden" value="1" class="addType"/>
												<input type="submit" value="Add" class="bluebtn addAddr"/>
                                            </div>
                                        	<a href="javascript:void(0);" class="bluebtn lightbtn cancelEdit">Cancel</a>
                                        </div>
                                        <div class="detaBox" id="current_address_lst">
											<?php if(!empty($user->user_Addresses)): ?>
												<?php foreach($user->user_Addresses as $adr): ?>
												<?php if($adr->type==1): ?>								
												<span class="pEdit"><?php echo e($adr->address); ?>	
													<a href="javascript:void(0);" class="removeText closebox rmvaddr"><img src="<?php echo asset('assets/frontend'); ?>/images/close.png" alt=""></a>
													<input type="hidden" value="<?php echo e($adr->id); ?>"/>
												</span>
												<?php endif; ?>
												<?php endforeach; ?>
											<?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="smallRow editAbout">
									<?php if(Auth::check() && $user->id == Auth::user()->id): ?> 
                                	<a href="javascript:void(0);" class="editForm editSingle"><img src="<?php echo asset('assets/frontend'); ?>/images/edit.png" alt=""></a>
									<?php endif; ?>
                                	<h3><i class="fa fa-map-marker"></i> Place you have been</h3>
                                    <div class="saveDeta">
										<div class="detaBox" id="address_shw">
										<?php if(!empty($user->user_Addresses)): ?>
										<?php foreach($user->user_Addresses as $adr): ?>
										<?php if($adr->type==2): ?>
                                        	<span class="pEdit" id="myadr_<?php echo e($adr->id); ?>"><?php echo e($adr->address); ?><a href="javascript:void(0);" class="removeText"></a></span>
										<?php endif; ?>
										<?php endforeach; ?>
										<?php endif; ?>
                                        </div>
									</div>
                                    <div class="editmain">
                                    	<div class="noclass clear">
                                            <div class="detaRow smallText clear locField">
                                                <input class="loc" id="address" name="address" type="text" value=""/>
												<input type="hidden" value="2" class="addType"/>
												<input type="submit" value="Add" class="bluebtn addAddr">
                                            </div>
                                            <a href="javascript:void(0);" class="bluebtn lightbtn cancelEdit">Cancel</a>
                                        </div>
                                        <div class="detaBox" id="address_lst">
                                        	<?php if(!empty($user->user_Addresses)): ?>
												<?php foreach($user->user_Addresses as $adr): ?>
												<?php if($adr->type==2): ?>								
												<span class="pEdit"><?php echo e($adr->address); ?>	
													<a href="javascript:void(0);" class="removeText closebox rmvaddr"><img src="<?php echo asset('assets/frontend'); ?>/images/close.png" alt=""></a>
													<input type="hidden" value="<?php echo e($adr->id); ?>"/>
												</span>
												<?php endif; ?>
												<?php endforeach; ?>
											<?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="smallRow editAbout">
									<?php if(Auth::check() && $user->id == Auth::user()->id): ?> 
                                	<a href="javascript:void(0);" class="editForm editMultiple"><img src="<?php echo asset('assets/frontend'); ?>/images/add2.png" alt=""></a>
									<?php endif; ?>
                                	<h3><i class="fa fa-briefcase"></i> Work Experience</h3>
									<form class="workexperience_form" action="">
										<div class="editmain addEdit">
											<div class="editBox addedt">
												<div class="formLine">
													<label>
														<span class="label">Company: <strong>*</strong></span>
														<div class="textfld">
															<input type="text" required class="compny" name="company" value=""/>
														</div>
													</label>
												</div>
												<div class="formLine">
													<label>
														<span class="label">Title: <strong>*</strong></span>
														<div class="textfld">
															<input type="text" class="jobs" name="title" value="" required/>
														</div>
													</label>
												</div>
												<div class="formLine">
													<label>
														<span class="label">Department:</span>
														<div class="textfld">
															<input type="text" name="department" value=""/>
														</div>
													</label>
												</div>
												<div class="formLine">
													<label>
														<span class="label">Location: <strong>*</strong></span>
														<div class="textfld smlFld">
															<input required type="text" id="lcity" name="city" value="">
															<script>setMapLoc('lcity');</script>
														</div>
													</label>
												</div>
												<div class="formLine">
													<span class="label">Time Period:</span>
													<div class="textfld clear">
														<span class="dstext fText">From</span>
														<div class="strtdate clear">
															<div class="mnth">
																<select name="from_month">
																	<option value="">Month</option>
																	<?php for( $i=1; $i<=12; $i++ ): ?>
																	<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
															<div class="yer">
																<select id="wfrom_year" name="from_year" required onChange="validateYear('#wfrom_year','#wto_year');">
																	<option value="">Year <strong>*</strong></option>
																	<?php for( $i=1970; $i<=2016; $i++ ): ?>
																	<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
														</div>
														<span class="dstext">To</span>
														<div class="endDate clear">
															<div class="mnth">
																<select name="to_month">
																	<option value="">Month</option>
																	<?php for( $i=1; $i<=12; $i++ ): ?>
																	<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
															<div class="yer">
																<select id="wto_year" name="to_year" onChange="validateYear('#wfrom_year','#wto_year');">
																	<option value="<?php echo date('Y'); ?>">Now</option>
																	<?php for( $i=1970; $i<=2016; $i++ ): ?>
																	<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
														</div>
													</div>
												</div>
												<div class="textfld btnRow">
													<a href="javascript:void(0)" class="cnclBtn">Cancel</a>
													<input type="submit" value="Save" class="bluebtn">
												</div>
											</div>
										</div>
									</form>
									
                                    <?php if(!empty($user->work_experiences)): ?>
									<?php foreach($user->work_experiences as $ir=>$val): ?>
                                    <div class="saveDeta sliderow clear workexps">
										<?php if(Auth::check() && $user->id == Auth::user()->id): ?> 
                                        <div class="actionBox">
                                            <a href="javascript:void(0);" class="editTxt">
											<img src="<?php echo asset('assets/frontend'); ?>/images/edit.png" alt="">
											<input type="hidden" value="<?php echo e($val->id); ?>" />
											</a>
                                            <a href="javascript:void(0);" class="delTxt">
											<img src="<?php echo asset('assets/frontend'); ?>/images/cancel.png" alt=""/>
											<input type="hidden" value="<?php echo e($val->id); ?>" />
											</a>
                                        </div>
										<?php endif; ?>
                                        <div class="textDeta">
                                            <h3><?php echo e($val->company); ?></h3>
                                            <h4><?php echo e($val->title); ?> <?php echo e($val->title!='' && $val->department!='' ?'-':''); ?> <?php echo e($val->department); ?></h4>
                                            <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
											<?php echo e($val->city); ?>

											</p>
                                            <p>
											<?php $dt = DateTime::createFromFormat('!m', $val->from_month);
											echo $val->from_month !=0 ?$dt->format('F') :'';?> <?php echo e($val->from_year); ?> - 
											<?php $dt = DateTime::createFromFormat('!m', $val->to_month);
											echo $val->to_month>0 ?$dt->format('F'):'';?> <?php echo e($val->to_year); ?>

											</p>
                                        </div>
										<div class="editBox">
											<form class="workexperience_form addedt" action="">
												<input type="hidden" name="wrkexp_id" value="<?php echo e($val->id); ?>" />
												<div class="formLine">
													<label>
														<span class="label">Company: <strong>*</strong></span>
														<div class="textfld">
															<input type="text" class="compny" name="company" value="<?php echo e($val->company); ?>" required />
														</div>
													</label>
												</div>
												<div class="formLine">
													<label>
														<span class="label">Title: <strong>*</strong></span>
														<div class="textfld">
															<input type="text" class="jobs" name="title" value="<?php echo e($val->title); ?>" required/>
														</div>
													</label>
												</div>
												<div class="formLine">
													<label>
														<span class="label">Department:</span>
														<div class="textfld">
															<input type="text" name="department" value="<?php echo e($val->department); ?>"/>
														</div>
													</label>
												</div>
												<div class="formLine">
													<label>
														<span class="label">Location: <strong>*</strong></span>
														<div class="textfld smlFld">
															<input type="text" id="lrcity_<?php echo e($val->id); ?>" name="city" value="<?php echo e($val->city); ?>" required >
															<script>setMapLoc("lrcity_<?php echo e($val->id); ?>");</script>
														</div>
													</label>
												</div>
												<div class="formLine">
													<span class="label">Time Period:</span>
													<div class="textfld clear">
														<span class="dstext fText">From</span>
														<div class="strtdate clear">
															<div class="mnth">
																<select name="from_month">
																	<option value="">Month</option>
																	<?php for( $i=1; $i<=12; $i++ ): ?>
																	<option value="<?php echo e($i); ?>" <?php echo e($i==$val->from_month ?'selected':''); ?> ><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
															<div class="yer">
																<select id="wfrom_year<?php echo e($val->id); ?>" name="from_year" required  onChange="validateYear('#wfrom_year<?php echo e($val->id); ?>','#wto_year<?php echo e($val->id); ?>');">
																	<option value="">Year <strong>*</strong></option>
																	<?php for( $i=1970; $i<=2016; $i++ ): ?>
																	<option value="<?php echo e($i); ?>" <?php echo e($i==$val->from_year ?'selected':''); ?> ><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
														</div>
														<span class="dstext">To</span>
														<div class="endDate clear">
															<div class="mnth">
																<select name="to_month">
																	<option value="">Month</option>
																	<?php for( $i=1; $i<=12; $i++ ): ?>
																	<option value="<?php echo e($i); ?>" <?php echo e($i==$val->to_month ?'selected':''); ?> ><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
															<div class="yer">
																<select id="wto_year<?php echo e($val->id); ?>" name="to_year" onChange="validateYear('#wfrom_year<?php echo e($val->id); ?>','#wto_year<?php echo e($val->id); ?>');">
																	<option value="<?php echo date('Y'); ?>">Now</option>
																	<?php for( $i=1970; $i<=2016; $i++ ): ?>
																	<option value="<?php echo e($i); ?>" <?php echo e($i==$val->to_year ?'selected':''); ?> ><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
														</div>
													</div>
												</div>
												<div class="textfld btnRow">
													<a href="javascript:void(0)" class="cnclBtn">Cancel</a>
													<input type="submit" value="Save" class="bluebtn">
												</div>
											</form>
										</div>
                                    </div>
                                    <?php endforeach; ?>
									<?php endif; ?>
                                </div>
                                
                                <div class="smallRow editAbout">
									<?php if(Auth::check() && $user->id == Auth::user()->id): ?> 
                                	<a href="javascript:void(0);" class="editForm editMultiple"><img src="<?php echo asset('assets/frontend'); ?>/images/add2.png" alt=""></a>
									<?php endif; ?>
                                	<h3><i class="fa fa-graduation-cap"></i> Education</h3>
									<form class="education_form" action="">
										<div class="editmain addEdit">
											<div class="editBox addedt">
												<div class="formLine">
													<label>
														<span class="label">School: <strong>*</strong></span>
														<div class="textfld">
															<input type="text" class="institution" name="institution" value="" required />
														</div>
													</label>
												</div>
												<div class="formLine">
													<label>
														<span class="label">Major: <strong>*</strong></span>
														<div class="textfld">
															<input type="text" class="major" name="major" value="" required >
														</div>
													</label>
												</div>
												<div class="formLine">
													<label>
														<span class="label">Degree: </span>
														<div class="textfld smlFld">
															<select name="degree">
																<option value="">Select</option>
																<?php if(!empty($degrees)): ?>
																<?php foreach($degrees as $dgr): ?>
																<option value="<?php echo e($dgr->id); ?>"><?php echo e($dgr->title); ?></option>
																<?php endforeach; ?>
																<?php endif; ?>
															</select>
														</div>
													</label>
												</div>
												<div class="formLine">
													<label>
														<span class="label">Location: <strong>*</strong></span>
														<div class="textfld smlFld">
															<input type="text" id="edcity" name="city" value="" required >
															<script>setMapLoc('edcity');</script>
														</div>
													</label>
												</div>
												<div class="formLine">
													<span class="label">Time Period:</span>
													<div class="textfld clear">
														<span class="dstext fText">From</span>
														<div class="strtdate clear">
															<div class="mnth">
																<select id="efrom_month" name="from_month">
																	<option value="">Month</option>
																	<?php for( $i=1; $i<=12; $i++ ): ?>
																	<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
															<div class="yer">
																<select id="efrom_year" name="from_year" required >
																	<option value="">Year <strong>*</strong></option>
																	<?php for( $i=1970; $i<=2016; $i++ ): ?>
																	<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
														</div>
														<span class="dstext">To</span>
														<div class="endDate clear">
															<div class="mnth">
																<select id="eto_month" name="to_month">
																	<option value="">Month</option>
																	<?php for( $i=1; $i<=12; $i++ ): ?>
																	<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
															<div class="yer">
																<select id="eto_year" name="to_year">
																	<option value="<?php echo date('Y'); ?>">Now</option>
																	<?php for( $i=1970; $i<=2016; $i++ ): ?>
																	<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
														</div>
													</div>
												</div>
												<div class="textfld btnRow">
													<a href="javascript:void(0)" class="cnclBtn">Cancel</a>
													<input type="submit" value="Save" class="bluebtn">
												</div>
											</div>
										</div>
                                    </form>
                                    <?php if(!empty($user->educations)): ?>
									<?php foreach($user->educations as $ir=>$val): ?>
									<div class="saveDeta sliderow clear educationals">
										<?php if(Auth::check() && $user->id == Auth::user()->id): ?> 
                                        <div class="actionBox">
                                            <a href="javascript:void(0);" class="editTxt"><img src="<?php echo asset('assets/frontend'); ?>/images/edit.png" alt=""></a>
                                            <a href="javascript:void(0);" class="delTxt">
											<img src="<?php echo asset('assets/frontend'); ?>/images/cancel.png" alt="">
											<input type="hidden" value="<?php echo e($val->id); ?>" />
											</a>
                                        </div>
										<?php endif; ?>
                                        <div class="textDeta">
                                            <h3><?php echo $val->institution; ?></h3>
                                            <h4><?php echo $val->major; ?> <?php echo e($val->major!='' && !empty($val->edu_degree) ?'-':''); ?> <?php echo e(!empty($val->edu_degree)?$val->edu_degree->title:''); ?></h4>
                                            <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
											<?php echo e($val->city); ?>

											</p>
                                            <p>
											<?php $dt = DateTime::createFromFormat('!m', $val->from_month);
											echo $val->from_month !=0 ?$dt->format('F') :'';?> <?php echo e($val->from_year); ?> - 
											<?php $dt = DateTime::createFromFormat('!m', $val->to_month);
											echo $val->to_month>0 ?$dt->format('F'):'';?> <?php echo e($val->to_year); ?>

											</p>
                                        </div>
                                        <div class="editBox">
											<form class="education_form addedt" action="">
												<input type="hidden" name="edu_id" value="<?php echo e($val->id); ?>" />
												<div class="formLine">
													<label>
														<span class="label">School: <strong>*</strong> </span>
														<div class="textfld">
															<input type="text" class="institution" name="institution" value="<?php echo e($val->institution); ?>" required />
														</div>
													</label>
												</div>
												<div class="formLine">
													<label>
														<span class="label">Major:</span>
														<div class="textfld">
															<input type="text" class="major" name="major" value="<?php echo e($val->major); ?>" />
														</div>
													</label>
												</div>
												<div class="formLine">
													<label>
														<span class="label">Degree: <strong>*</strong></span>
														<div class="textfld smlFld">
															<select name="degree" required>
																<option value="">Select</option>
																<?php if(!empty($degrees)): ?>
																<?php foreach($degrees as $dgr): ?>
																<option value="<?php echo e($dgr->id); ?>" <?php echo e($val->degree==$dgr->id ?'selected':''); ?>><?php echo e($dgr->title); ?></option>
																<?php endforeach; ?>
																<?php endif; ?>
															</select>
														</div>
													</label>
												</div>
												<div class="formLine">
													<label>
														<span class="label">Location: <strong>*</strong></span>
														<div class="textfld smlFld">
															<input type="text" id="educity_<?php echo e($val->id); ?>" name="city" value="<?php echo e($val->city); ?>" required />
															<script>setMapLoc("educity_<?php echo e($val->id); ?>");</script>
														</div>
													</label>
												</div>
												<div class="formLine">
													<span class="label">Time Period:</span>
													<div class="textfld clear">
														<span class="dstext fText">From</span>
														<div class="strtdate clear">
															<div class="mnth">
																<select name="from_month">
																	<option value="">Month</option>
																	<?php for( $i=1; $i<=12; $i++ ): ?>
																	<option value="<?php echo e($i); ?>" <?php echo e($i==$val->from_month ?'selected':''); ?> ><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
															<div class="yer">
																<select name="from_year" required >
																	<option value="">Year <strong>*</strong></option>
																	<?php for( $i=1970; $i<=2016; $i++ ): ?>
																	<option value="<?php echo e($i); ?>" <?php echo e($i==$val->from_year ?'selected':''); ?> ><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
														</div>
														<span class="dstext">To</span>
														<div class="endDate clear">
															<div class="mnth">
																<select name="to_month">
																	<option value="">Month</option>
																	<?php for( $i=1; $i<=12; $i++ ): ?>
																	<option value="<?php echo e($i); ?>" <?php echo e($i==$val->to_month ?'selected':''); ?> ><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
															<div class="yer">
																<select name="to_year">
																	<option value="<?php echo date('Y'); ?>">Now</option>
																	<?php for( $i=1970; $i<=2016; $i++ ): ?>
																	<option value="<?php echo e($i); ?>" <?php echo e($i==$val->to_year ?'selected':''); ?> ><?php echo e($i); ?></option>
																	<?php endfor; ?>
																</select>
															</div>
														</div>
													</div>
												</div>
												<div class="textfld btnRow">
													<a href="javascript:void(0)" class="cnclBtn">Cancel</a>
													<input type="submit" value="Save" class="bluebtn">
												</div>
											</form>
                                        </div>
                                    </div>
									<?php endforeach; ?>		
									<?php endif; ?>
                                </div>
                                
                                <div class="smallRow editAbout">
									<?php if(Auth::check() && $user->id == Auth::user()->id): ?> 
                                	<a href="javascript:void(0);" class="editForm editCs"><img src="<?php echo asset('assets/frontend'); ?>/images/edit.png" alt=""></a>
									<?php endif; ?>
                                	<h3><i class="fa fa-question-circle-o"></i> Topic you know</h3>
                                    <div class="editmain">
                                    	<div class="noclass clear">
                                            <div class="detaRow smallText clear">
												<form class="tropic" name="" method="post" action="">
                                                <input type="text" id="sp_tropic" name="title" placeholder=""/>
												<input type="hidden" value="1" name="type">
												<input type="submit" value="Add" class="bluebtn">
												<?php $user_tropics='';
												if(!empty($user->tropics)){
													foreach($user->tropics as $it=>$val){
														if($val->type ==1)
														{
															$user_tropics.=($user_tropics=='')?'':',';
															$user_tropics.=$val->tropic_id;
														}
													}												
												}													
												?>
												<input type="hidden" value="<?php echo e($user_tropics); ?>" id="sp_ids" name="ids">
												</form>
                                            </div>
                                            <a href="javascript:void(0);" class="bluebtn lightbtn cancelEditLg">Cancel</a>
                                        </div>
                                    </div>
                                    <div class="noclose">
                                        <div class="saveDeta" id="sp_ts">
										
										<?php  $know_tropics =  $user->tropics()->where('type',1)->orderBy('group_id','asc')->get();										
										$group_arr=array();	
										
										if(!empty($know_tropics))
										{
											foreach($know_tropics as $ky=>$val)
											{
												if(!in_array($val->tropic->group_id,$group_arr))
												{	$group_arr[]= $val->tropic->group_id;
												?>										   
													<br/>
													<h4><?php echo e($val->tropic->group_id !=0? ucfirst($val->tropic->group->name):'Others'); ?></h4>
													<span class="addbox" id="tp_<?php echo e($val->id); ?>"><?php echo e($val->tropic->title); ?></span>
													
										<?php	}
												else
												{
										?>			<span class="addbox" id="tp_<?php echo e($val->id); ?>"><?php echo e($val->tropic->title); ?></span>
										<?php	}
													
											}
										}
										?>
                                        </div>
                                    </div>
                                    <div class="editsaveDeta">
                                        <div class="saveDeta hideclose" id="sp_tps">                                      
										<?php  										
										$group_arr=array();	
										
										if(!empty($know_tropics))
										{
											foreach($know_tropics as $ky=>$val)
											{
												if(!in_array($val->tropic->group_id,$group_arr))
												{	$group_arr[]= $val->tropic->group_id;
												?>										   
													<br/>
													<h4><?php echo e($val->tropic->group_id !=0? ucfirst($val->tropic->group->name):'Others'); ?></h4>
													<span class="addbox"><?php echo e($val->tropic->title); ?><a href="javascript:void(0)" class="closebox rmvtp"><img src="<?php echo asset('assets/frontend'); ?>/images/close.png" alt=""><input type="hidden" value="<?php echo e($val->id); ?>" /></a></span>
													
										<?php	}
												else
												{
										?>			<span class="addbox"><?php echo e($val->tropic->title); ?><a href="javascript:void(0)" class="closebox rmvtp"><img src="<?php echo asset('assets/frontend'); ?>/images/close.png" alt=""><input type="hidden" value="<?php echo e($val->id); ?>" /></a></span>
										<?php	}													
											}
										}
										?>
                                        </div>
                                    </div>
                                </div>
                                <div class="smallRow editAbout">
									<?php if(Auth::check() && $user->id == Auth::user()->id): ?> 
                                	<a href="javascript:void(0);" class="editForm editCs"><img src="<?php echo asset('assets/frontend'); ?>/images/edit.png" alt=""></a>
									<?php endif; ?>
                                	<h3><i class="fa fa-thumbs-o-up"></i> Topic you like</h3>
                                    <div class="editmain">
                                    	<div class="noclass clear">
                                            <div class="detaRow smallText clear">
												<form class="tropic" name="" method="post" action="">
                                                <input type="text" id="lg_tropic" name="title" placeholder="">
												<input type="submit" value="Add" class="bluebtn">
												<input type="hidden" value="2" name="type">
												<?php $user_tropics='';
												if(!empty($user->tropics)){
													foreach($user->tropics as $it=>$val){
														if($val->type ==2)
														{
															$user_tropics.=($user_tropics=='')?'':',';
															$user_tropics.=$val->tropic_id;
														}
													}												
												}													
												?>
												<input type="hidden" value="<?php echo e($user_tropics); ?>" id="lg_ids" name="ids">
												</form>
                                            </div>
                                            <a href="javascript:void(0);" class="bluebtn lightbtn cancelEditLg">Cancel</a>
                                        </div>
                                    </div>
                                    <div class="noclose">
                                        <div class="saveDeta" id="lg_ts">
										<?php  $like_tropics =  $user->tropics()->where('type',2)->orderBy('group_id','asc')->get();										
										$group_arr=array();	
										
										if(!empty($like_tropics))
										{
											foreach($like_tropics as $ky=>$val)
											{
												if(!in_array($val->tropic->group_id,$group_arr))
												{	$group_arr[]= $val->tropic->group_id;
												?>										   
													<br/>
													<h4><?php echo e($val->tropic->group_id !=0? ucfirst($val->tropic->group->name):'Others'); ?></h4>
													 <span class="addbox" id="tp_<?php echo e($val->id); ?>"><?php echo e($val->tropic->title); ?></span>
													
										<?php	}
												else
												{
										?>			 <span class="addbox" id="tp_<?php echo e($val->id); ?>"><?php echo e($val->tropic->title); ?></span>
										<?php	}
													
											}
										}
										?>
                                        </div>
                                    </div>
                                    <div class="editsaveDeta">
                                        <div class="saveDeta hideclose" id="lg_tps">
										<?php  										
										$group_arr=array();	
										
										if(!empty($like_tropics))
										{
											foreach($like_tropics as $ky=>$val)
											{
												if(!in_array($val->tropic->group_id,$group_arr))
												{	$group_arr[]= $val->tropic->group_id;
												?>										   
													<br/>
													<h4><?php echo e($val->tropic->group_id !=0? ucfirst($val->tropic->group->name):'Others'); ?></h4>
													<span class="addbox"><?php echo e($val->tropic->title); ?><a href="javascript:void(0)" class="closebox rmvtp"><img src="<?php echo asset('assets/frontend'); ?>/images/close.png" alt=""><input type="hidden" value="<?php echo e($val->id); ?>" /></a></span>
													
										<?php	}
												else
												{
										?>			<span class="addbox"><?php echo e($val->tropic->title); ?><a href="javascript:void(0)" class="closebox rmvtp"><img src="<?php echo asset('assets/frontend'); ?>/images/close.png" alt=""><input type="hidden" value="<?php echo e($val->id); ?>" /></a></span>
										<?php	}													
											}
										}
										?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                           <div class="profileBot tabCont">
                           		<div class="homeQues rcntAct">
                                    <div class="contentmiddle qsPage">
										<div class="middlerow clear">
											<div class="middleleft">
												<div class="usertop">
													<figure><img src="<?php echo asset('assets/frontend'); ?>/images/userthumb.png" width="47" height="47" alt=""></figure>
													<span><a href="">Chris Lockwood</a></span>
													<samp>asked a Question</samp>
													<div class="blockArrow">
														<ul>
															<li><a href="#"><img src="<?php echo asset('assets/frontend'); ?>/images/a3.png" alt=""><span>follow</span></a></li>
															<li><a href="#"><img src="<?php echo asset('assets/frontend'); ?>/images/a1.png" alt=""><span>save link</span></a></li>
															<li><a href="#"><img src="<?php echo asset('assets/frontend'); ?>/images/a2.png" alt=""><span>report</span></a></li>
														</ul>
													</div>
												</div>
												<h3 class="descqs"><a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i>Can singing a duet with a girl help bond you?</a></h3>
												<p class="des-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
												<div class="mobileCount">
													<div class="topsmall">
														<span>1 day ago</span>
														<span>About <a href="javascript:void(0);" class="qpop">Startup</a></span>
													</div>
													<div class="noDesc">
														<div class="retail">
															<span class="noLext">To</span>
															<ul>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
															</ul>
															<a href="javascript:void(0);" class="moreTo">View All</a>
														</div>
													</div>
													<ul>
														<li><i class="fa fa-eye" aria-hidden="true"></i> <span>9</span> views</li>
														<li><i class="fa fa-comments-o" aria-hidden="true"></i> <span>9</span> answers</li>
														<li><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <span>9</span> votes</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="middlerow clear">
											<div class="middleleft">
												<div class="usertop">
													<figure><img src="<?php echo asset('assets/frontend'); ?>/images/userthumb.png" width="47" height="47" alt=""></figure>
													<span><a href="">Chris Lockwood</a></span>
													<samp>shared an Article</samp>
													<div class="blockArrow">
														<ul>
															<li><a href="#"><img src="<?php echo asset('assets/frontend'); ?>/images/a3.png" alt=""><span>follow</span></a></li>
															<li><a href="#"><img src="<?php echo asset('assets/frontend'); ?>/images/a1.png" alt=""><span>save link</span></a></li>
															<li><a href="#"><img src="<?php echo asset('assets/frontend'); ?>/images/a2.png" alt=""><span>report</span></a></li>
														</ul>
													</div>
												</div>
												<h3 class="shortqs"><a class="" href=""><i class="fa fa-clock-o" aria-hidden="true"></i>Can singing a duet with a girl help bond you?</a></h3>
												<div class="mobileCount">
													<div class="topsmall">
														<span>1 day ago</span>
														<span>About <a href="javascript:void(0);" class="qpop">Startup</a></span>
													</div>
													<div class="noDesc">
														<div class="retail">
															<span class="noLext">To</span>
															<ul>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
															</ul>
															<a href="javascript:void(0);" class="moreTo">View All</a>
														</div>
													</div>
													<ul>
														<li><i class="fa fa-eye" aria-hidden="true"></i> <span>9</span> views</li>
														<li><i class="fa fa-comments-o" aria-hidden="true"></i> <span>9</span> answers</li>
														<li><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <span>9</span> votes</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="middlerow clear">
											<div class="middleleft">
												<div class="usertop">
													<figure><img src="<?php echo asset('assets/frontend'); ?>/images/userthumb.png" width="47" height="47" alt=""></figure>
													<span><a href="">Chris Lockwood</a></span>
													<samp>shared an Article</samp>
													<div class="blockArrow">
														<ul>
															<li><a href="#"><img src="<?php echo asset('assets/frontend'); ?>/images/a3.png" alt=""><span>follow</span></a></li>
															<li><a href="#"><img src="<?php echo asset('assets/frontend'); ?>/images/a1.png" alt=""><span>save link</span></a></li>
															<li><a href="#"><img src="<?php echo asset('assets/frontend'); ?>/images/a2.png" alt=""><span>report</span></a></li>
														</ul>
													</div>
												</div>
												<h3 class="descqs"><a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i>Can singing a duet with a girl help bond you?</a></h3>
												<p class="des-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
												<div class="mobileCount">
													<div class="topsmall">
														<span>1 day ago</span>
														<span>About <a href="javascript:void(0);" class="qpop">Startup</a></span>
													</div>
													<div class="noDesc">
														<div class="retail">
															<span class="noLext">To</span>
															<ul>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
															</ul>
															<a href="javascript:void(0);" class="moreTo">View All</a>
														</div>
													</div>
													<ul>
														<li><i class="fa fa-eye" aria-hidden="true"></i> <span>9</span> views</li>
														<li><i class="fa fa-comments-o" aria-hidden="true"></i> <span>9</span> answers</li>
														<li><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <span>9</span> votes</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="middlerow clear">
											<div class="middleleft">
												<div class="usertop">
													<figure><img src="<?php echo asset('assets/frontend'); ?>/images/userthumb.png" width="47" height="47" alt=""></figure>
													<span><a href="">Chris Lockwood</a></span>
													<samp>asked a Question</samp>
													<div class="blockArrow">
														<ul>
															<li><a href="#"><img src="<?php echo asset('assets/frontend'); ?>/images/a3.png" alt=""><span>follow</span></a></li>
															<li><a href="#"><img src="<?php echo asset('assets/frontend'); ?>/images/a1.png" alt=""><span>save link</span></a></li>
															<li><a href="#"><img src="<?php echo asset('assets/frontend'); ?>/images/a2.png" alt=""><span>report</span></a></li>
														</ul>
													</div>
												</div>
												<h3 class="shortqs"><a class="" href=""><i class="fa fa-clock-o" aria-hidden="true"></i>Can singing a duet with a girl help bond you?</a></h3>
												<div class="mobileCount">
													<div class="topsmall">
														<span>1 day ago</span>
														<span>About <a href="javascript:void(0);" class="qpop">Startup</a></span>
													</div>
													<div class="noDesc">
														<div class="retail">
															<span class="noLext">To</span>
															<ul>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
																<li>Chris Lockwood - Lorem</li>
															</ul>
															<a href="javascript:void(0);" class="moreTo">View All</a>
														</div>
													</div>
													<ul>
														<li><i class="fa fa-eye" aria-hidden="true"></i> <span>9</span> views</li>
														<li><i class="fa fa-comments-o" aria-hidden="true"></i> <span>9</span> answers</li>
														<li><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <span>9</span> votes</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
                                </div>
                           </div>
                           
                            
                            <div class="tabContact tabCont">
								<?php if(Auth::check() && $user->id == Auth::user()->id): ?>
                            	<a href="javascript:void(0)" class="addEml"><strong>+</strong> Add More Contact Information</a>
                                <br>
								<?php endif; ?>
								<form id="contactinfofrm" class="contactinfofrm">
									<div class="linkAddress addSmlAdd">
									   <!--<a href="javascript:void(0)" class="cnEdt"><img src="<?php echo asset('assets/frontend'); ?>/images/cancel.png" alt=""></a>-->
									   <!--<a href="javascript:void(0)" class="edtEml"><img src="<?php echo asset('assets/frontend'); ?>/images/edit.png" alt=""></a>-->
										<ul>
											<li>
												<div class="contactEdit clear">
													<img src="<?php echo asset('assets/frontend'); ?>/images/gmail.png" alt="">
													<strong>*</strong>
													<input name="info1" type="email" value="" required />
												</div>
											</li>
											<li>
												<div class="contactEdit clear">
													<img src="<?php echo asset('assets/frontend'); ?>/images/message.png" alt="">
													<input name="info2" type="email" value="" />
												</div>
											</li>
											<li>
												<div class="contactEdit clear">
													<img src="<?php echo asset('assets/frontend'); ?>/images/fb1.png" alt="">
													<input name="info3" type="text" value="">
												</div>
											</li>
											<li>
												<div class="contactEdit clear">
													<img src="<?php echo asset('assets/frontend'); ?>/images/skype.png" alt="">
													<input name="info4" type="text" value="">
												</div>
											</li>
										</ul>
										<div class="textfld btnRow">
											<a href="javascript:void(0)" class="cnclBtn">Cancel</a>
											<input type="submit" value="Save" class="bluebtn">
										</div>
									</div>
								</form>
								<div id="cntInfo">
									<?php echo $__env->make('include.user_contact_info', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
								</div>
                            </div>
                            
                       </div>
                   </div>
                   
            	</div>
        
                <!--right pan open-->
                <aside class="rightpan newRight">
                    <div class="right-top">
                        <span class="questioncount">Questions<strong>18</strong></span>
                        <span class="membercount">Member<strong>9</strong></span>
                    </div>
                    <!---<div class="most-tag">
                        <h2>Most Used tags</h2>
                        <ul>
                            <li><a href="#">business</a>x  6</li>
                            <li><a href="#">technology</a>x  3</li>
                            <li><a href="#">marketing</a>x  3</li>
                            <li><a href="#">google</a>x  4</li>
                            <li><a href="#">apps</a>x  4</li>
                            <li><a href="#">billionaire</a>x  3</li>
                            <li><a href="#">movie</a>x  2</li>
                            <li><a href="#">glass</a>x  1</li>
                        </ul>
                        <a href="#" class="more-tags">See more tags</a>
                    </div>-->
                    <div class="guidlines">
                        <h2>Guideline </h2>
                        <ul>
                            <li><a href="" class="tooltiplst"><i class="fa fa-check-circle" aria-hidden="true"></i> Update Profile</a>
                                <div class="tltip">
                                    <h3>Lorem Ipsum</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                                </div>
                            </li>
                            <li><a href="" class="tooltiplst"><i class="fa fa-check-circle" aria-hidden="true"></i> Ask a Question</a>
                                <div class="tltip">
                                    <h3>Lorem Ipsum</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                                </div>
                            </li>
                            <li class="active"><a href="" class="tooltiplst"><i class="fa fa-check-circle" aria-hidden="true"></i> Answer a Question</a>
                                <div class="tltip">
                                    <h3>Lorem Ipsum</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </aside>
                <!--right pan close-->              
                                
            </div>
        </div>
        <!--middle close-->

    <!--body close-->

<?php $__env->stopSection(); ?>



<?php $__env->startSection('customScript'); ?>

<script>
setMapLoc('address');
setMapLoc('current_address');
 
$('.editBox .addedt .cnclBtn').click(function() {
	$(this).parent('.btnRow').parent('.addedt').parent('.editBox').slideUp(200);
});
$('.sliderow .cnclBtn').click(function() {
	$(this).parent('.btnRow').parent('.addedt').parent('.editBox').slideUp(200);
	$(this).parent('.btnRow').parent('.addedt').parent('.editBox').prev('.textDeta').slideDown(200);
	$(this).parent('.btnRow').parent('.addedt').parent('.editBox').parent('.sliderow').children('.actionBox').slideDown(200);
});
$( "form.userdt" ).on( "submit", function( event ) {
//
  event.preventDefault();
  var formdata =  $( this ).serialize();
  console.log( $( this ).serialize() );
  updateUsers(formdata);
});

function updateUsers(formdata)
{
	$.ajax({
		type:"post",
		url: "<?php echo url('user/update-userdata'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: formdata,			
		//dataType: "json",			
		success:function(res) {
			/* console.log(res); */
			$(".fullnaminp").val(res.first_name+' '+res.last_name);
			$(".fullnam").html(res.first_name+' '+res.last_name);
			$(".nicknameinp").val(res.nickname);
			$(".nickname").html(res.nickname);
			$('.gendersl option[value='+res.gender+']').attr('selected','selected');
			$(".gender").html(res.gender=='M'?'Male':'Female');
			$(".dob").html(res.dob);
			$(".ag").html(res.age);
			$(".details").html(res.details);
			
			$('.cancelEdit').trigger( "click" );
			$('.nCan').trigger( "click" );
		}

	});
}

 

$('.addAddr').click(function(){
	//current_address
	var addbtn = $(this);
	var addrval = addbtn.parent().find('input.loc').val();
	var fid = addbtn.parent().find('input.loc').attr('id');
	var addType = addbtn.parent().find('input.addType').val();
	if(addrval=='')
	{
		$('#'+fid).focus();
	}else{
		$.ajax({
			type:"post",
			url: "<?php echo url('user/add-address'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {'address':addrval, 'addType':addType},			
			//dataType: "json",			
			success:function(res) {
				/* console.log(res); */
				$('#'+fid).val('');
				var lhtml = '<span class="pEdit">'+addrval+'<a href="javascript:void(0);" class="removeText closebox rmvaddr"><img src="<?php echo asset('assets/frontend'); ?>/images/close.png" alt=""></a>	<input type="hidden" value="'+res.id+'"/></span>';	
				var shtml = '<span class="pEdit withLocation" id="myadr_'+res.id+'">'+addrval+'';
				if(addType==1)
				{
				shtml += '<a href="javascript:void(0);" class="removeText"></a><input type="hidden" value="'+res.id+'"/><span class="loc '+(res.is_default && res.is_default=='Y'?'active':'')+'"><i class="fa fa-map-marker"></i></span>';					
				}				
				shtml += '</span>';
				
				$('#'+fid+'_shw').append(shtml);
				$('#'+fid+'_lst').append(lhtml);	
				if(res.is_default && res.is_default=='Y')
				{
					$("#curr_addrs").text(res.address);
				}
				//$('.cancelEdit').trigger( "click" );
			}

		});
		
	}
});

$(document).on('click','.rmvaddr',function() {
	var dobj = $(this);
	var rmvid = dobj.next('input').val();

	$.ajax({
		type:"post",
		url: "<?php echo url('user/del-address'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: {id:rmvid},			
		//dataType: "json",			
		success:function(res) {
			//console.log(res);
			/* if(res == 1){} */
			
				$('#myadr_'+rmvid).remove();
				dobj.parent().remove();		
				//$('.cancelEdit').trigger( "click" );
			
		}

	});	
});
$( function() {
  
    $( ".compny" ).autocomplete({
      source: '<?php echo e(url("user/company-autocomplete")); ?>'
    });
	
    $( ".jobs" ).autocomplete({
      source: '<?php echo e(url("user/job-autocomplete")); ?>'
    });
	
    $( ".institution" ).autocomplete({
      source: '<?php echo e(url("user/university-autocomplete")); ?>'
    });
	
    $( ".major" ).autocomplete({
      source: '<?php echo e(url("user/major-autocomplete")); ?>'
    });	
	
});
$( "form.workexperience_form" ).on( "submit", function( event ) {
//
  event.preventDefault();
  var formdata =  $( this ).serialize();
  //console.log( $( this ).serialize() );
  $.ajax({
		type:"post",
		url: "<?php echo url('user/add-workexp'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: formdata,			
		//dataType: "json",			
		success:function(res) {
			console.log(res);
			/* $('#'+fid).val('');
			var lhtml = '<li><span class="addbox">'+addrval+'<a href="javascript:void(0)" class="closebox"><img src="'+'<?php echo asset('assets/frontend'); ?>'+'/images/close.png" alt=""></a></span></li>';
			$('#'+fid+'_lst').html(lhtml); */
			window.location.reload();
		}

	});
});

$(document).on('click','.workexps .actionBox .delTxt',function() {	
	var dobj = $(this);
	var delid = $(this).children("input").val();
	//alert(delid);
	$.ajax({
		type:"post",
		url: "<?php echo url('user/del-workexp'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: {id:delid},			
		//dataType: "json",			
		success:function(res) {
			//console.log(res);
			if(res == 1)
			{
				dobj.parent().parent().remove();
			}
		}

	});
});
$( "form.education_form" ).on( "submit", function( event ) {
//
  event.preventDefault();
  var formdata =  $( this ).serialize();
  //console.log( $( this ).serialize() );
  $.ajax({
		type:"post",
		url: "<?php echo url('user/add-education'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: formdata,			
		//dataType: "json",			
		success:function(res) {
			//console.log(res);
			window.location.reload();
		}

	});
});
 $(document).on('click','.educationals .actionBox .delTxt', function() {
		var dobj = $(this);
		var delid = $(this).children("input").val();
		//alert(delid);
		$.ajax({
			type:"post",
			url: "<?php echo url('user/del-education'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {id:delid},			
			//dataType: "json",			
			success:function(res) {
				//console.log(res);
				if(res == 1)
				{
					dobj.parent().parent().remove();
					
				}
			}

		});
	});
/* 	
var availableTropics = [
	<?php if(!empty($all_tropics)): ?>
	  <?php foreach($all_tropics as $jval): ?>
		<?php echo '"'.$jval->title.'" ,'; ?>

	  <?php endforeach; ?>
	<?php endif; ?>
    ];
$("#sp_tropic").autocomplete({
  source: availableTropics
});
$("#lg_tropic").autocomplete({
  source: availableTropics
}); */
	

$( "form.tropic" ).on( "submit", function( event ) {
//
	var frmobj = $(this);
  event.preventDefault();
  var formdata =  $( this ).serialize();
  //console.log( $( this ).serialize() );
  $.ajax({
		type:"post",
		url: "<?php echo url('user/add-tropic'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: formdata,			
		//dataType: "json",			
		success:function(res) {
			/* console.log(res); */
			window.location.reload();
			/* frmobj.children('input[name="title"]').val('');
			if(res.type == 1)
			{
				var adtp = ' <span class="addbox">'+res.title+'</span>';
				var adtp2 = '<span class="addbox">'+res.title+'<a href="javascript:void(0)" class="closebox"><img src="<?php echo asset('assets/frontend'); ?>/images/close.png" alt=""><input type="hidden" value="'+res.id+'" /></a></span>';
				$('#sp_ts').append(adtp);
				$('#sp_tps').append(adtp2);
			}
			if(res.type == 2)
			{
				var adtp = ' <span class="addbox">'+res.title+'</span>';
				var adtp2 = '<span class="addbox">'+res.title+'<a href="javascript:void(0)" class="closebox"><img src="<?php echo asset('assets/frontend'); ?>/images/close.png" alt=""><input type="hidden" value="'+res.id+'" /></a></span>';
				$('#lg_ts').append(adtp);
				$('#lg_tps').append(adtp2);
			}
			frmobj.parent('.detaRow').parent('.noclass').parent('.editmain').slideUp(200);
			frmobj.parent('.detaRow').parent('.noclass').parent('.editmain').parent('.editAbout').find('.editsaveDeta').slideUp(200);
			frmobj.parent('.detaRow').parent('.noclass').parent('.editmain').parent('.editAbout').find('.noclose').slideDown(200); */
		}

	});
});
	$(document).on('click','.rmvtp',function() {
		var dobj = $(this);
		var delid = dobj.children("input").val();
		$.ajax({
			type:"post",
			url: "<?php echo url('user/del-tropic'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {id:delid},			
			//dataType: "json",			
			success:function(res) {
				//console.log(res);
				if(res == 1)
				{
					dobj.parent().parent('.addbox').parent('.saveDeta').parent('.editsaveDeta').slideUp(200);
					dobj.parent().parent('.addbox').parent('.saveDeta').parent('.editsaveDeta').parent('.editAbout').find('.editsaveDeta').slideUp(200);
					dobj.parent().parent('.addbox').parent('.saveDeta').parent('.editsaveDeta').parent('.editAbout').find('.noclose').slideDown(200);
					dobj.parent().parent().remove();
					$('#tp_'+delid).remove();
					
				}
			}
		});
		//$(this).parent('.addbox').parent('li').remove();
	});


var availableTags1 = [
	<?php if(!empty($all_tropics)): ?>
	  <?php foreach($all_tropics as $jval): ?>
		<?php echo '{"label":"'.$jval->title.'","id":"'.$jval->id.'"} ,'; ?>

	  <?php endforeach; ?>
	<?php endif; ?>
];
$("#sp_tropic").autocomplete({
	source: availableTags1,
	select: function (event, ui) {
		//console.log(ui);
		var preVal = $("#sp_ids").val();
		if(preVal!='')	preVal+=',';
		preVal+=ui.item.id;
		$("#sp_ids").val(preVal);			
	}
});
$("#lg_tropic").autocomplete({
  source: availableTags1,
	select: function (event, ui) {
		//console.log(ui);
		var preVal = $("#lg_ids").val();
		if(preVal!='')	preVal+=',';
		preVal+=ui.item.id;
		$("#lg_ids").val(preVal);			
	}
});



$(document).on('click',".pEdit.withLocation .loc", function(){
	var currentobj = $(this);
	var addrid = $(this).parent().find('input').val();
	$.ajax({
		type:"post",
		url: "<?php echo url('user/setdeflt-address'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: {'address_id':addrid, },			
		//dataType: "json",			
		success:function(res) {
			//console.log(res);
			if(res!=0)
			{
			$(".pEdit.withLocation .loc").removeClass('active');
				currentobj.toggleClass('active');	
				$("#curr_addrs").text(res.address);
			}
		}
	});
});

function validateYear(from_year,to_year)
{
	var fromyear = parseInt($(from_year).val());
	var toyear = parseInt($(to_year).val());
	if(fromyear>toyear)
	{
		$(from_year).val('');
	}
}


$(document).on( "submit", "form.contactinfofrm", function( event ) {
//
	var frmobj = $(this);
	event.preventDefault();
	var formdata =  $( this ).serialize();
  //console.log( $( this ).serialize() );
  $.ajax({
		type:"post",
		url: "<?php echo url('user/add-contactinfo'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: formdata,			
		//dataType: "json",			
		success:function(res) {
			/* console.log(res); */
			
			if(res!=0)
			{
				$("#contactinfofrm")[0].reset();
			$("#cntInfo").html(res);
			}
		}

	});
});
$(document).on( "click", ".rnvinfo", function( event ) {
	var info_id = $(this).parent('.viwSmlAdd').find('input[name=info_id]').val();

	 $.ajax({
		type:"post",
		url: "<?php echo url('user/del-contactinfo'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: {'info_id':info_id},			
		//dataType: "json",			
		success:function(res) {
			/* console.log(res);  */
			
		}

	});
});


$(document).on( "click", ".aboutDatarow .bluebtn", function( event ) {
	$(".aboutEditarea").slideUp(500);
	$(".aboutContArea").slideDown(500);
});
$(document ).on( "click", ".frndReq", function( event ) {
//
  //event.preventDefault();
  //var formdata =  $( this ).serializeArray();
  //console.log(formdata );
	//$('.sndmsg').html('');
	var obj = $(this);
	var cnfrm = true;
	if(obj.hasClass('sent'))
	{
		cnfrm = confirm("Are you sure to remove friend?");		
	}
	
	if(cnfrm == true)
	{
		var frndid = obj.children('input').val();
		$.ajax({
			type:"post",
			url: "<?php echo url('user/send-friend-request'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {'frnd':frndid},			
			//dataType: "json",			
			error:function(res) {	
				console.log(res);
				if(res.status == 401)
				{
					window.location.href="<?php echo url('login'); ?>";
				}
			},
			success:function(res) {
				/* console.log(res); */
				if(res==1){
					obj.addClass('sent');
					obj.children('span').text('Request sent');
				}				
				else if(res==2){
					obj.removeClass('sent');
					obj.children('span').text('Add Friend');
				}
				
				//if(res==1)	$('.sndmsg').html('Request send successfull');
			}
		});
	}
	
});
$(document).on('change','#profile_image',function(){
	$("#dpFrm").submit();
});

$("#dpFrm").ajaxForm({
	beforeSubmit : function(formData, jqForm, options){
		
		
	},
	success: function(responseText, statusText, xhr, jform){ 
		console.log(responseText);
		var data = responseText;
		jform.clearForm();
		if(data && statusText == 'success')
		{					
			$("#dpImg").attr('src',data);
		}
		else if(statusText == 'error')
		{
			alert('Sorry! internal server error.');
		}
	}
});
</script>
<style>

.aboutEditarea ul li a.factive img{
    -moz-filter: grayscale(0%);
    -webkit-filter: grayscale(0%);
    -ms-filter: grayscale(0%);
    filter: grayscale(0%);
}
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>