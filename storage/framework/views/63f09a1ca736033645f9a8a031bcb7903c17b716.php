<?php
//use App\Helpers\CustomHelper;
$loguser = Auth::User();
?>


<?php $__env->startSection('pageTitle', 'iKnow|Messages'); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="pageMiddle msgpageMiddle">
	<h2>Message</h2>
	<a href="javascript:void(0)" id="frndlist"> <i class="fa fa-users" aria-hidden="true"></i></a>
	<div class="messgSection">
		<div class="chatHeader">
			<a href="javascript:void(0);" class="lnks active">Inbox (8)</a>
			<a href="javascript:void(0);" class="lnks">All</a>
		</div>
		<div class="overlay"></div>
		<div id="msgOverlay"></div>             
		<div class="chatMmbrSec">			
				<div id="mmbrPnl" class="memberPnl">
					<div class="msgSrchUser">
						<div class="fldArea">
							<input type="text" id="srcInput" onkeyup="searchUser();" placeholder="Search Friends..." class="fld"/>
							<button class="btn"><i class="fa fa-search"></i></button>
						</div>
					</div>
					<?php echo $__env->make('chat.chat_message_leftpanel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>

			<div class="warnings" id="membership_warnings" style="display:none;">
				<div class="error_message" id="membership_error_message">
				</div>
			</div>
			<div class="chatSec">
				<div class="chatMsgCont">                                   
					<div class="chatListArea" id="message_list_area">
						<?php echo $__env->make('chat.chat_touser_messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>                                  
					</div>                                  
				</div>                                  
				<div class="chatSbmtform">
					<div class="chatSbmtformsg" id="warningMessageBox" <?php if($message_limit==0): ?> style="display:none;" <?php endif; ?>>
						
						<div id="warningMessageSubBox" <?php if($message_expired == 1): ?> style="display:none;" <?php endif; ?>>
							<!--<p>Write a message!</p>
							<p>It will be deleted if you do not show any interest!</p>
							<span class="tm" id="countDown"></span>-->
						</div>
					   
						<input type="hidden" id="userMessageDeleteTime" value="<?php echo Auth::user()->message_delete_time*1000; ?>" />
					</div>
					<div class="chatSbmtformBX" id="postMessageBox" <?php if($message_limit==1): ?> style="display:none;" <?php endif; ?> >
						<div class="formRow">
							<textarea id="message_reply" name="message" placeholder="Enter your message..."></textarea>
						</div>
						<div class="formRow formRWbtns">
							<div class="attachFld">
								<form id="upload_form" name="upload_form" action="<?php echo url('chat/uploadChatfile'); ?>" method="post" enctype="multipart/form-data" >     
									<?php echo e(csrf_field()); ?>

									<label class="attachFileBtn">
										<input name="attachFile" type="file" id="attachFile">
										<i class="fa fa-paperclip"></i>
										<!-- <img src="<?php echo asset('assets/frontend/images/msg-attach-icon.png'); ?>" alt=""/>-->
									</label>
									<input type="hidden" name="from_user_id" id="from_user_id" value="" />
									<input type="hidden" name="to_user_id" id="to_user_id" value="" />
									<span id="errmsg">&nbsp;</span>
								</form>
							</div>
							<span class="chatSubmit">
								<button class="newButton sm" id="reply_btn" onclick="sendMessage('message_reply','boxscroll2');"><i class="fa fa-paper-plane-o"></i></button>
							</span>
						</div>
					</div>
				</div>                          
			</div>
		</div>                          
	</div><div class="overlay"></div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
<!--Start Javascript-->
<style type="text/css">
	body{background-color:#f4f4f4 !important;}
</style>
<link href="<?php echo asset('assets/frontend/css/message-style.css'); ?>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo asset('assets/common/jquery.form.min.js'); ?>"></script>

<script type="text/javascript">
    var to_user_id = 0;
	<?php if(count($chat_messsage_data['chat_users']) > 0): ?>
		<?php if(session('touser_id') != ''): ?>
			to_user_id = <?php echo session('touser_id'); ?>;
		<?php else: ?>
			<?php if(count($chat_messsage_data['chat_users']) > 0): ?>
				<?php if($chat_messsage_data['chat_users'][0]->from_user_id == $loguser->id ): ?>
					to_user_id = <?php echo $chat_messsage_data['chat_users'][0]->to_user_id; ?>;
				<?php else: ?> if($chat_messsage_data['chat_users'][0]->to_user_id == $loguser->id )
					to_user_id = <?php echo $chat_messsage_data['chat_users'][0]->from_user_id; ?>;
				<?php endif; ?>
			<?php endif; ?>
			alert(to_user_id);
		<?php endif; ?>
	<?php endif; ?>
	
    function startChatMessage(toUserID)
    {
        if(toUserID == 0)
        {
            toUserID = to_user_id;
        }
        $('#chtMmbrlst ul li').each(function(){
            if($(this).hasClass('selectli'))
            {
                $(this).removeClass('selectli');
            }
        });
        $('#chtMmbrlst ul li#user_list_'+toUserID).addClass('selectli');
		$('#to_user_id').val(toUserID);
		$('#user_list_'+toUserID+' a span.nameTm span.name span.count').css('display','none');
		$('#user_list_'+toUserID+' a span.nameTm span.name span.count').html('');
		
        $.ajax({
                type:"POST",
                url: '<?php echo url('chat/startChatMessage'); ?>',
				headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
                data: {toUserID:toUserID},
                dataType: "json",
                success: function(response) {
                    //console.log(response);
                    if(response.has_error == 0)
                    {
                        $('#message_list_area').html(response.all_msg_list_html);
                        var totalheight= 0;
                        $('#message_list_area ul li').each(function(){
                            totalheight += $(this).height() + 50;
                            });
                        $('#message_list_area').animate({
                            scrollTop: totalheight
                        });
                        $('.chat_pic_msg').fancybox();
                    }
                }
            });
    }
    $(document).ready(function(){
        startChatMessage(to_user_id);
        $('#from_user_id').val('<?php echo Auth::user()->id; ?>');
        $('#to_user_id').val(to_user_id);
    });
</script>
<script type="text/javascript">
    function sendMessage(message_reply,mssg_div)
    {
        var to_user_id = $("#to_user_id").val();
        var from_user_id = $("#from_user_id").val();
        
        if(to_user_id == 0)
        {
            alert('Please select a user first!');
        }else
        {
            var message = $("#"+message_reply).val();
            if(message == '' && $("#attachFile").val() == '')
            {
                alert('Please write something.');
            }
            else
            {
                if(message != '')
                {
                    $.ajax({
                        type:"POST",
                        url:"<?php echo url('chat/sendMessage'); ?>",
						headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
                        data:{to_user_id:to_user_id,from_user_id:from_user_id,message:message,type:'T'},
                        dataType: "json",
                        success:function(response){
                            if(response.has_error == 0)
                            {
                                $("#"+message_reply).val('');
                                $("#"+mssg_div).append(response.ajax_msg_html);
                                var totalheight= 0;
                                $('#message_list_area ul li').each(function(){
                                    totalheight += $(this).height() + 50;
                                    });
                                $('#message_list_area').animate({
                                    scrollTop: totalheight
                                });
                                $('.chat_pic_msg').fancybox();
                            }
                            else
                            {
                                $("#"+message_reply).val('');
                                $('#postMessageBox').hide();
                                $('#warningMessageBox').show();
                                $('#userMessageDeleteTime').val(response.message_delete_time*1000);
                                
                                  /* $("#countDown")
                                  .countdown(response.message_delete_time*1000, function(event) {
                                    $(this).text(
                                      event.strftime('%d days %H:%M:%S')
                                    );
                                  }); */

                                /*$('#membership_error_message').html('<p><strong>Your message limit has been exceeded. Please Upgrade your membership.</strong></p>');
                                $('#membership_warnings').show();
                                setTimeout(function(){
                                    $('#membership_warnings').hide(); 
                                }, 5000);*/
                            }
                        }
                    });
                }
                if($("#attachFile").val() != '')
                {
                    $("#upload_form").submit();
                }
            }
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function()
    {
        $("#attachFile").change(function(){
            var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
            $('#errmsg').html(filename);
        });
    });

    $("#upload_form").ajaxForm({
        beforeSubmit : function(formData, jqForm, options){
        },
        success: function(responseText, statusText, xhr, jform){ 
            //console.log(responseText+"=="+statusText+"=="+xhr+"=="+jform);
            
            jform.clearForm();
            if(responseText != 'error' && statusText == "success"){
                if(responseText.has_error == 0)
                {
                    $('#message_reply').val('');
                    $('#boxscroll2').append(responseText.ajax_msg_html);
                    var totalheight= 0;
                    $('#message_list_area ul li').each(function(){
                        totalheight += $(this).height() + 50;
                        });
                    $('#message_list_area').animate({
                        scrollTop: totalheight
                    });
                    setTimeout(function(){
                        $('#errmsg').html("");
                    }, 2000);
                    $('.chat_pic_msg').fancybox();
                }
                else if(responseText.has_error == 2)
                {
                    $("#message_reply").val('');
                    $('#membership_error_message').html('<p><strong><?php echo (111); ?></strong></p>');
                    $('#membership_warnings').show();
                    setTimeout(function(){
                        $('#membership_warnings').hide(); 
                    }, 5000);
                }
                else
                {
                    $("#message_reply").val('');                     
                    $('#postMessageBox').hide();
                    $('#warningMessageBox').show();
                    /*$('#membership_error_message').html('<p><strong>Your message limit has been exceeded. Please Upgrade your membership.</strong></p>');
                    $('#membership_warnings').show();
                    setTimeout(function(){
                        $('#membership_warnings').hide(); 
                    }, 5000);*/
                }
            }
            else if(responseText == 'error')
            {
                $('#errmsg').html("<?php echo (112); ?>");
            }
        }
    });
    $('#message_reply').keypress(function(e) { // Attach the form handler to the keypress event
        if (e.keyCode == 13) { // If the the enter key was pressed.
            $('#reply_btn').click(); // Trigger the button(elementId) click event.
            return e.preventDefault(); // Prevent the form submit.
        }
    });
</script>
<script type="text/javascript">
function loadUnreadMessages()
{
	$.ajax({
		type:"POST",
		url:"<?php echo url('chat/loadUnreadMessages'); ?>",
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data:{},
		dataType: "json",
		success:function(response){
			if(response.has_error == 0)
			{
				$('#boxscroll2').append(response.ajax_msg_html);
				var totalheight= 0;
				$('#message_list_area ul li').each(function(){
					totalheight += $(this).height() + 50;
					});
				$('#message_list_area').animate({
					scrollTop: totalheight
				});
				$.each(response.from_user_msg_count, function(index, value){
					if(value > 0)
					{
						$('#user_list_'+index+' a span.nameTm span.name span.count').html(value);
						$('#user_list_'+index+' a span.nameTm span.name span.count').css('display','block');
					}
				});
                $('.chat_pic_msg').fancybox();
			}
		}
	});
}
</script>
<script type="text/javascript">
$(document).ready(function(){
	setInterval(function() {
		loadUnreadMessages();
	}, 5000);
});
</script>
<!--End Javascript-->
<script type="text/javascript">


function deleteJunkMessage()
{
    $.ajax({
        type:"POST",
        url:"<?php echo url('ajax/deleteJunkMessage'); ?>",
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
        data:{},
        dataType: "json",
        success:function(response){
            if(response.delete_message == 1)
            {
                $('#warningMessageSubBox').hide();
            }
        }
    });
}
</script>


<!--<script type="text/javascript" src="<?php echo asset('assets/frontend/js/jquery.countdown.min.js'); ?>"></script>-->

<script type="text/javascript">  
<?php if(Auth::user()->message_delete_time != '0'): ?>
  var message_delete_time = $('#userMessageDeleteTime').val();
  /* $("#countDown")
  .countdown(message_delete_time, function(event) {
    $(this).text(
      event.strftime('%d days %H:%M:%S')
    );
  }); */
 <?php endif; ?> 
 
function searchUser() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("srcInput");
    filter = input.value.toUpperCase();
    ul = $("#chtMmbrlst ul");
    li = ul.children("li");
	$( li ).each(function( index ) {
        nam = $(this).children('a').children('.nameTm').children('.name').text();
		/* console.log(a); */
        if (nam.toUpperCase().indexOf(filter) > -1) {
            $(this).show();
        } else {
            $(this).hide();

        }
    });
}

$(document).ready(function(){
  /* deleteJunkMessage(); */
  startChatMessage('<?php echo e($chat_user); ?>');
  
});
</script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>