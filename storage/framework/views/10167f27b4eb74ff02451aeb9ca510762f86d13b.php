<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    
    <!--main content open-->  
	<section class="testquizbody">
    	<div class="testpaper-wrap">
        	<h2><?php echo e($test->title); ?></h2>
			<h3>Time Left => <span id="test_timer">&nbsp;</span></h3>
			<?php if($errors->any()): ?>
				<div class="warnings">
					<div class="error_message">
						<?php foreach($errors->all() as $error): ?>
						<p><?php echo $error; ?></p>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
            <div class="test-user"><i class="fa fa-user" aria-hidden="true"></i><?php echo e($test_eval->first_name); ?> <?php echo e($test_eval->last_name); ?></div>
            <div class="test_container">
                <div class="testpage-no">
                	<h4>Question <?php echo $atend_cnt; ?> of <?php echo count($test->questions); ?></h4>
                </div>
				<div class="question-answer">
					 <div id="svgbasics"></div>   
					 <p><strong>The objective is</strong> to MAP the Question from the left to the answer to the right. Once the link is set, the same question cannot have multiple sources or targets.  The target can only accept one answer to be linked to.</p>
					  
					  <div id="pnlAllIn">
						<div id="leftPanel">
						  
						</div>
						<div id="rightPanel">
						  
						</div>
					  </div>
					  <div class="clearfix"></div>
						<script >
						var leftPersons = [
							<?php 
							if(isset($question->sub_questions))
							{
								foreach($question->sub_questions as $ky=>$val)
								{	
									echo "{id:".$val->id.",name:'".trim(strip_tags($val->sub_question))."',position:'desc', cls:'draggable'},";
								}								
							}
							?>
						];
						var rightPersons = [
							<?php 
							if(isset($question->sub_answers))
							{
								foreach($question->sub_answers as $ky=>$val)
								{	
									echo "{id:".$val->id.",name:'".trim(strip_tags($val->sub_answer))."',position:'desc', cls:'droppable'},";
								}								
							}
							?>
						];
						console.log(rightPersons);
						</script>
					  <script id="personTemplate" type="text/x-jsrender">
							<div class="person <?php echo '{{:cls}}';?>" >
								<input type="hidden" value="<?php echo '{{:id}}';?>" />
								<ul class="buttons">
									<li> <span class="ui-icon-arrow-4 ui-icon" ></span></li>
									<li><span class="ui-icon-shuffle ui-icon"></span></li>
								</ul>
								<img alt="" src="" />
								<ul class="name">
									<li><h2><?php echo '{{:id}}';?>. <?php echo '{{:name}}';?></h2></li>
									<li><?php echo '{{:position}}';?></li>
								</ul>
							</div>
					  </script>
					  <script src="<?php echo asset('assets/frontend/jsdraw.js'); ?>"></script>
					  <div id="dialog" title="Reset persons mapping?">
						<p>
						  <span class="ui-icon ui-icon-alert"></span>Do you wish to delete all connection resetting the mapping to it's original positions?</p>
					  </div>
					  <div id="dialogMappingResult" title="Current Mapping">
						<p>
						  Here is a list of the current mapping:</p>
						<ul>
						  <li>No mapping was done yet</li></ul>
					  </div>
					  
					  <a id="popButton" href="#">reset mapping</a> 
					  <!--<a id="getMappings" href="#">show mapping</a>-->
				</div>
            	<form name="test_pepr" method="post">
					<?php echo e(csrf_field()); ?>                
					
					<div class="question-answer">
						<div class="formrow"></div>
						<input type="hidden" name="ontest_id" value="<?php echo e($test_eval->id); ?>"/>
						<input type="hidden" name="test_id" value="<?php echo e($test_eval->test_id); ?>"/>
						<input type="hidden" name="question_id" value="<?php echo e(!empty($question)?$question->id:0); ?>"/>
						<input type="hidden" name="question_type" value="<?php echo e($question->question_type); ?>"/>
						
						<input type="hidden" id="qns_ans" name="answer" value="" />
						<div class="testbtnrow clear">
							<!-- <a class="colorbtn prevbtn" href="#"><i class="fa fa-caret-left" aria-hidden="true"></i>Prev</a>--><!--this for previous page link-->
							<a class="colorbtn nextbtn" href="javascript:$('form').submit();">Next<i class="fa fa-caret-right" aria-hidden="true"></i></a>
							<?php if($test->allow_go_back=="Y"): ?>
							<a class="colorbtn prevbtn" href="javascript:history.back();"><i class="fa fa-caret-left" aria-hidden="true"></i>Back</a>
							<?php endif; ?>
						</div>
						<!--<div class="testbtnrow papersubmit">
							<input type="submit" value="Finish Now" class="colorbtn bluebtn">
						</div>--><!--- this section for answer submit last page-->
					</div>
                </form>
            </div>
        </div>
    </section>
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script src="<?php echo asset('assets/frontend/jquery-ui.min.js'); ?>"></script>
<link href="<?php echo asset('assets/frontend/jquery-ui.css'); ?>" rel="stylesheet" type="text/css" />
  
  <!-- Raphael 2.0.1
  <script class="jsbin" src="http://cdn.jsdelivr.net/raphael/2.1.0/raphael-min.js"></script>
  -->
  <script class="jsbin" src="<?php echo asset('assets/frontend/raphael-min.js'); ?>"></script>

  <!-- JsRender 1.0 pre 35 (optional) 
  <script src="http://cdn.jsdelivr.net/jsrender/1.0pre35/jsrender.min.js"></script> --> 
  <script src="<?php echo asset('assets/frontend/jsrender.min.js'); ?>"></script>
  
<script> 

function quitTest() {
		//console.log(timer);
	var postdata ={};
	$.ajax({
		url: "<?php echo e(url('user/quit-test')); ?>",
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		type: 'POST',
		//dataType: 'json',
		data: postdata,
	}).done(function(response){
		console.log(response);	
		alert("Times Up!!!"); 
		window.location.reload();
	});
}
function setDuration(timer) {
		//console.log(timer);
	var postdata ={'duration': timer};
	$.ajax({
		url: "<?php echo e(url('user/change-duration')); ?>",
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		type: 'POST',
		//dataType: 'json',
		data: postdata,
	}).done(function(response){
		//console.log(response);
	});
}
function startTimer(duration, display) {
    var timer = duration, hours, minutes, seconds;
	
    setInterval(function () {
		setDuration(timer);
        hours = parseInt(timer / 3600, 10)
        minutes = parseInt(timer / 60, 10) - (hours * 60);
        seconds = parseInt(timer % 60, 10);

        hours = hours < 10 ? "0" + hours : hours;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.text(hours + ":" +minutes + ":" + seconds);

        if (--timer < 0) {
            timer = duration;
			quitTest();
			
        }
    }, 1000);
}

jQuery(function ($) {
    var timerMnutes = parseInt(<?php echo $sess['test_duration']; ?>),
        display = $('#test_timer');
    startTimer(timerMnutes, display);
});

<?php if($test->allow_go_back=="N"): ?>
function preventBack(){window.history.forward();}
  setTimeout("preventBack()", 0);
  window.onunload=function(){null};
<?php endif; ?>
  
</script>
<style>
article, aside, figure, footer, header, hgroup, menu, nav, section { display: block; }        
body { font-family: Arial; font-size: 11px; }
ul { list-style: none; }

#svgbasics { 
  position: absolute; left: 0px; top: 0px; height:836px !important; width:100% !important;
  border: solid 0px #484; z-index: -100; 
}

.draggable { }
.droppable { }  


#pnlAllIn, #dialog, #dialogMappingResult,
#popButton, #getMappings { 
  z-index: 10;
}
#pnlAllIn {
  margin: 20px; 
}

#leftPanel, #rightPanel {
  width: 50%;
  float: left;
}

/* Persons */
.person { width: 80%; padding: 10px; border: 1px red solid; margin: 10px 0; }
.person ul.buttons { float:right; }
.person ul.name { margin-left: 20px; }
.person ul.name h2 { margin-bottom: 0; }
.person img { float:left; }

.clearfix { clear: both; }
.question-answer {
    overflow: hidden;
}
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.basic', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>