<!doctype html>
<html>
<head>
<?php $settings = App()->settings;
$escapePages = array('contact-us','pricing');
$currpage=Request::segment(1);
?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=1">
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<?php if(!in_array($currpage,$escapePages)): ?>
<title> <?php echo e($settings->seo_title); ?></title>
<meta name="description" content="<?php echo e($settings->seo_description); ?>">
<meta name="keywords" content="<?php echo e($settings->seo_keywords); ?>">
<meta name="google-signin-client_id" content="781531976817-7e4fktuios98hhjonec4b6s75bmhu46j.apps.googleusercontent.com">
<?php else: ?>
	<?php echo $__env->yieldContent('seo_sec'); ?>
<?php endif; ?>
<!--Start CSS-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<script src="<?php echo asset('assets/frontend/js/jquery.min.js'); ?>" type="text/javascript"></script>
<link href="<?php echo asset('assets/frontend/slikslider/slick.css'); ?>" rel="stylesheet" type="text/css">
<!--<link href="assets/materialcharts/material-charts.css" rel="stylesheet" type="text/css">-->
 <?php echo $__env->yieldContent('customStyle'); ?>
<link href="<?php echo asset('assets/frontend/css/style.css'); ?>" rel="stylesheet" type="text/css">
<!--end CSS-->


    <!--Start Responsive CSS (Keep All CSS Above This)-->
    <link rel="stylesheet" type="text/css" href="<?php echo asset('assets/frontend/css/responsive.css'); ?>" />
    <!--End Responsive CSS (Don't Keep Any CSS Below This)-->
    <script>
        var BASE_URL = "<?php echo e(url('')); ?>";
    </script>

</head>
<body>

<div class="mainSite">
    
    <!--testheader open-->
    <header class="testheader">
    	<div class="wrapper">
           <a href="<?php echo url(''); ?>"><img src="<?php echo asset('assets/frontend/images/logo.png'); ?>" width="159" height="32" alt=""></a>
        </div>
    </header>
    <!--testheader close-->
    <?php echo $__env->yieldContent('content'); ?>
	
</div>




<!--Start js-->
<script src="<?php echo asset('assets/frontend/js/placeholders.min.js'); ?>"></script>
<script src="<?php echo asset('assets/frontend/slikslider/slick.min.js'); ?>"></script>

<!--end js-->

<?php echo $__env->yieldContent('customScript'); ?>

<script src="<?php echo asset('assets/frontend/js/custom.js'); ?>" type="text/javascript"></script>

<script language="javascript">
</script>

</body>
</html>