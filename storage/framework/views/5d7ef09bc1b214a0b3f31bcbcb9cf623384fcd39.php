<?php $__env->startSection('pageTitle', 'Dashboard'); ?>

<?php $__env->startSection('content'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Settings
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="<?php echo admin_url(''); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li class="active">Settings</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Update Settings</h3>
					</div>
					<?php if($errors->any()): ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<?php foreach($errors->all() as $error): ?>
								<p><?php echo $error; ?></p>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
					<?php if(session('success')): ?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<?php echo session('success'); ?>

					</div>
					<?php endif; ?>
					<!-- form start -->
					<form class="form-horizontal" name="settings_form" action="<?php echo admin_url('settings'); ?>" method="post" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"/>
						<div class="box-body">
							<div class="form-group">
								<label for="admin_email" class="col-sm-2 control-label">Admin Email</label>
								<div class="col-sm-6">
									<input type="email" class="form-control" id="admin_email" name="admin_email" value="<?php echo $admin_user->email; ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="firstName" class="col-sm-2 control-label">Admin Password</label>
								<div class="col-sm-6">
									<input type="password" class="form-control" id="admin_pass" name="admin_pass" autocomplete="new-password" />
								</div>
							</div>
							<div class="form-group">
								<label for="firstName" class="col-sm-2 control-label">Site Title</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="site_title" name="site_title" value="<?php echo $settings->site_title; ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="contact_email" class="col-sm-2 control-label">Contact Email</label>
								<div class="col-sm-6">
									<input type="email" class="form-control" id="contact_email" name="contact_email" value="<?php echo $settings->contact_email; ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="contact_name" class="col-sm-2 control-label">Contact Email Name</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="contact_name" name="contact_name" value="<?php echo $settings->contact_name; ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="contact_phone" class="col-sm-2 control-label">Contact Phone</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="contact_phone" name="contact_phone" value="<?php echo $settings->contact_phone; ?>" />
								</div>
							</div>

							<div class="form-group">
								<label for="site_logo" class="col-sm-2 control-label">Logo Image</label>
								<div class="col-sm-6">
								<span class="btn btn-default btn-file">
									Browse <input type="file"  id="site_logo" name="site_logo" />
								</span>
									<p class="help-block" id="thumb_image_help">Current site logo</p>
									<img class="list_table_img" src="<?php echo asset('assets/upload/site_logo/'.$settings->site_logo); ?>" alt="No Logo">
								</div>
							</div>
							
							<div class="form-group">
								<label for="seo_title" class="col-sm-2 control-label">SEO Title</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="seo_title" name="seo_title" value="<?php echo $settings->seo_title; ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="seo_keywords" class="col-sm-2 control-label">SEO Keywords</label>
								<div class="col-sm-6">
									<textarea class="form-control" id="seo_keywords" name="seo_keywords" ><?php echo $settings->seo_keywords; ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="seo_description" class="col-sm-2 control-label">SEO Description</label>
								<div class="col-sm-6">
									<textarea class="form-control" id="seo_description" name="seo_description" ><?php echo $settings->seo_description; ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="contact_address" class="col-sm-2 control-label">Contact address</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="contact_address" name="contact_address" value="<?php echo $settings->contact_address; ?>" />
									<input type="hidden"  id="lat" name="lat" value="<?php echo $settings->lat; ?>" />
									<input type="hidden"  id="lng" name="lng" value="<?php echo $settings->lng; ?>" />
								</div>
							</div>
							<?php /*
							<div class="form-group">
								<label for="facebook_link" class="col-sm-2 control-label">Facebook Link</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="facebook_link" name="facebook_link" value="{!! $settings->site_fb_link !!}" />
								</div>
							</div>
							<div class="form-group">
								<label for="twitter_link" class="col-sm-2 control-label">Twitter Link</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="twitter_link" name="twitter_link" value="{!! $settings->site_twitter_link !!}" />
								</div>
							</div>
							<div class="form-group">
								<label for="youtube_link" class="col-sm-2 control-label">Youtube Link</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="youtube_link" name="youtube_link" value="{!! $settings->site_youtube_link !!}" />
								</div>
							</div>
							<div class="form-group">
								<label for="linkedin_link" class="col-sm-2 control-label">Linkedin Link</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="linkedin_link" name="linkedin_link" value="{!! $settings->site_linkedin_link !!}" />
								</div>
							</div>
							*/?>

						</div><!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-info pull-right">Save</button>
						</div><!-- /.box-footer -->
					</form>
				</div><!-- /.box -->
			</div>
		</div>
	</section>
	<!-- /.content -->
</div><!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script type="text/javascript">

function setMapLoc(){
	
	var inputt = document.getElementById("contact_address");
	var options = {
		//componentRestrictions: {country: cc},		
	  //bounds: defaultBounds,
	 // types: ['establishment']
	};
	var autocomplete = new google.maps.places.Autocomplete(inputt, options);
	
	autocomplete.addListener('place_changed', function() {
		var place = autocomplete.getPlace();
		if (place.geometry.location) {
			var lat = place.geometry.location.lat();
			var lng = place.geometry.location.lng();
			$("#lat").val(lat);
			$("#lng").val(lng);
		}
	});
	
}
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6g0i6uEsWrZ3mRuSddpe_-pObwo_BGbU&libraries=places&callback=setMapLoc"></script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>