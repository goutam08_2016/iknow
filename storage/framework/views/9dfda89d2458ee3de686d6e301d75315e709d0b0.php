<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<link href="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.css'); ?>" rel="stylesheet" type="text/css">

<link href="<?php echo asset('assets/frontend/zebradatepicker/zebra_datepicker.css'); ?>" rel="stylesheet" type="text/css">
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>



	<?php echo $client_header; ?>


    	 

    <!--main content open-->
    <section class="assignpage">
    	<div class="wrapper">
        	<div class="mainhead"><h2>Setting</h2></div>
			<form method="POST">
				<?php echo e(csrf_field()); ?>

				<div class="assigncontwrap">
					<div class="assignbox">
						<h3><?php echo e($test->title); ?> Test Access</h3>
						<div class="assignarea">
							<div class="assignhead">
								<h4>Availability</h4>
								<i class="fa fa-calendar" aria-hidden="true"></i>
							</div>
							<div class="assignform">
								<div class="assigninner-row">
									<h5>Available Form</h5>
									<label class="strtnw">
										<input type="checkbox" name="from_now" class="nowstrt">
										<span class="label">From Now</span>
									</label>
									<div class="formrow toptime">
										<input name="start_time" type="text" placeholder="mm/dd/year" class="datebox" value="<?php echo ($test->start_time >0)? date('m/d/Y',$test->start_time) : ''; ?>" >
										<!--<select class="selecthr">
											<?php for($i=01; $i<=12; $i++): ?>
											<option><?php echo e($i); ?></option>
											<?php endfor; ?>
										</select>
										<select class="selectmin">
											<?php for($i=0; $i<12; $i++): ?>
												<option><?php echo $i*5; ?></option>
											<?php endfor; ?>
										</select>
										<select class="selectampm">
											<option>AM</option>
											<option>PM</option>
										</select>-->
									</div>
								</div>
								<div class="assigninner-row">
									<h5>Available until</h5>
									<div class="formrow">
										<input name="end_time" type="text" placeholder="mm/dd/year" class="datebox" value="<?php echo ($test->end_time >0)? date('m/d/Y',$test->end_time) : ''; ?>"/>
										<!--<select class="selecthr">
											<?php for($i=01; $i<=12; $i++): ?>
											<option><?php echo e($i); ?></option>
											<?php endfor; ?>
										</select>
										<select class="selectmin">
											<?php for($i=0; $i<12; $i++): ?>
												<option><?php echo $i*5; ?></option>
											<?php endfor; ?>
										</select>
										<select class="selectampm">
											<option>AM</option>
											<option>PM</option>
										</select>-->
									</div>
								</div>
							</div>
						</div>
						
						<div class="assignarea">
							<div class="assignhead">
								<h4>Attempts</h4>
								<i class="fa fa-retweet" aria-hidden="true"></i>
							</div>
							<div class="assignform">
								<div class="assigninner-row">
									<h5>Attempts</h5>
									<div class="formrow">
										<label>
											<input name="attempt_type" type="radio" <?php echo e($test->attempt_type == 1 ? 'checked' : ''); ?>  value="1" >
											<span class="label">One</span>
										</label>
									</div>
									<div class="formrow">
										<label>
											<input type="radio" name="attempt_type" value="2" <?php echo e($test->attempt_type == 2 ? 'checked' : ''); ?> >
											<span class="numfld"><input type="number" min="2" name="attempt_count" value="<?php echo $test->attempt_type == 2 ? $test->attempt_count : ''; ?>"></span>
											<span class="label">Multiple (set as 2 or higher)</span>
										</label>
									</div>
									<div class="formrow">
										<label>
											<input type="radio" name="attempt_type" value="3"  <?php echo e($test->attempt_type == 3 ? 'checked' : ''); ?> >
											<span class="label">Unlimited</span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="assignbox">
						<h3>Test Introduction</h3>
						<div class="assignarea">
							<div class="assignhead">
								<h4>Instructions</h4>
								<i class="fa fa-info-circle" aria-hidden="true"></i>
							</div>
							<div class="assignform">
								<div class="assigninner-row">
									<h5>Guidelines (optional)</h5>
									<div class="formrow">
										<label>
											<input type="checkbox" name="show_guideline" <?php echo e($test->show_guideline=="Y"?"checked":""); ?> >
											<span class="label">Display guidelines before Test starts</span>
										</label>
									</div>
								</div>
								<div class="assigninner-row">
									<h5>Example Guidelines:</h5>
									<div class="formrow">
										<ul>
											<li>* Will allow you to save and finish at a later date</li>
											<li>* Will not allow you to go back and change your answers</li>
											<li>* Has a time limit of 20 minutes</li>
											<li>* Has a pass mark of 70%</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="assignbox">
						<h3>Taking Test</h3>
						<div class="assignarea">
							<div class="assignhead">
								<h4>Time Limit</h4>
								<i class="fa fa-clock-o" aria-hidden="true"></i>
								<div class="headassign-txt">
									<label>
										<div class="textfld"><input type="number" min="1" name="time_limit" value="<?php echo e($test->time_limit >0 ? $test->time_limit:0); ?>"></div>
										<span class="label">Minutes</span>
									</label>
								</div>
							</div>
						</div>
						
						<!--<div class="assignarea">
							<div class="assignhead">
								<h4>Resume Later</h4>
								<i class="fa fa-pause-circle-o" aria-hidden="true"></i>
								<div class="headassign-txt">
									<label>
										<input name="resume_later" type="checkbox" <?php echo e($test->resume_later=='Y'?'checked':''); ?>  >
										<span class="label">Allow save and resume later</span>
									</label>
								</div>
							</div>
						</div>-->
					</div>
					
					<div class="assignbox">
						<h3>Test Questions</h3>
						<!--<div class="assignarea">
							<div class="assignhead">
								<h4>Display</h4>
								<i class="fa fa-clone" aria-hidden="true"></i>
							</div>
							<div class="assignform">
								<div class="assigninner-row">
									<h5>Display</h5>
									<div class="formrow">
										<label>
											<div class="pointbox">
												<select name="noof_guidelines">
												<?php for( $i=1; $i<=10; $i++): ?>
													<option <?php echo e($test->noof_guidelines == $i ? 'selected':''); ?> > <?php echo e($i); ?> </option>
												<?php endfor; ?>
												</select>
											</div>
											<span class="label">Display guidelines before Test starts</span>
										</label>
									</div>
								</div>
								<div class="assigninner-row">
									<h5>Points</h5>
									<div class="formrow">
										<label>
											<input type="checkbox" name="show_points" <?php echo e($test->show_points=='Y'?'checked':''); ?> >
											<span class="label">Display Points each Question is worth during Test</span>
										</label>
									</div>
								</div>
							</div>
						</div>-->
						
						<div class="assignarea">
							<div class="assignhead">
								<h4>Randomize</h4>
								<i class="fa fa-random" aria-hidden="true"></i>
								<div class="headassign-txt">
									<label>
										<input type="checkbox" name="randomize_question" <?php echo e($test->randomize_question == 'Y'?'checked':''); ?> >
										<span class="label">Give Questions in random order</span>
									</label>
								</div>
							</div>
						</div>
						
						<div class="assignarea">
							<div class="assignhead">
								<h4>Answers</h4>
								<i class="fa fa-check-square-o" aria-hidden="true"></i>
							</div>
							<div class="assignform">
								<div class="assigninner-row">
									<h5>Mandatory</h5>
									<div class="formrow">
										<label>
											<input type="checkbox" name="must_answer_qsn" <?php echo e($test->must_answer_qsn == 'Y'?'checked':''); ?> >
											<span class="label">Must answer Questions</span>
										</label>
									</div>
									<div class="formrow">
										<label>
											<input type="checkbox" name="must_answer_correct" <?php echo e($test->must_answer_correct == 'Y'?'checked':''); ?> >
											<span class="label">Must answer correctly to continue</span>
										</label>
									</div>
								</div>
								<div class="assigninner-row">
									<h5>Instant review</h5>
									<div class="formrow">
										<label>
											<input type="checkbox" name="instant_grading" <?php echo e($test->instant_grading=='Y'?'checked':''); ?> >
											<span class="label">Question grading and Feedback during Test</span>
										</label>
									</div>
									<div class="formrow">
										<label>
											<input type="checkbox" name="show_correct_answers" <?php echo e($test->show_correct_answers == 'Y'?'checked':''); ?> >
											<span class="label">Reveal correct answers during Test</span>
										</label>
									</div>
								</div>
								<div class="assigninner-row">
									<h5>Change answers</h5>
									<div class="formrow">
										<label>
											<input type="checkbox" name="allow_go_back" <?php echo e($test->allow_go_back == 'Y'?'checked':''); ?> >
											<span class="label">Allow Test takers to go back during Test</span>
										</label>
									</div>
								</div>
								<span class="notes">
									<strong>Note:</strong> With <strong>Instant review</strong> and <strong>Change answers</strong> both enabled, users can review gradings and reattempt Questions during the Test to achieve 100%.
								</span>
							</div>
						</div>
					</div>
					<input type="hidden" name="test_id" value="<?php echo e($test->id); ?>"/>
					<div class="submitarea">
						<input type="submit" value="Assign" class="colorbtn bluebtn">
						<a href="<?php echo e(url('user/tests')); ?>" class="colorbtn">Cancel</a>
					</div>				
				</div>
			</form>
        </div>
    </section>
    <!--main content close-->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script src="<?php echo asset('assets/frontend/js/stacktable.min.js'); ?>"></script>
<script src="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.js'); ?>"></script>
<script src="<?php echo asset('assets/frontend/ckeditor/ckeditor.js'); ?>"></script>
<script src="<?php echo asset('assets/frontend/zebradatepicker/zebra_datepicker.js'); ?>"></script>
<script>
$(document).ready(function() {
   
    $('.newtest').click(function() {
		$('.addnewbox').slideDown(200);
	});
	$('.newformwrap .closebtn').click(function() {
		$('.addnewbox').slideUp(200);
	});
	
	
	$('.testtable .linkdeta a').click(function() {
		$(this).parent('.linkdeta').parent('.testtablerow').next('.testlinkbox').slideToggle(200);
	});
	$('.testtable .assigndeta a').click(function() {
		$(this).parent('.assigndeta').parent('.testtablerow').parent('.testtable').children('.testasignbox').slideToggle(200);
	});
	
	$('.datebox').Zebra_DatePicker({
		direction: true,
		format: 'm/d/Y'
	});
	
	$('.nowstrt').change(function () {
        $('.toptime .datebox')[0].disabled = this.checked;
		$('.toptime .selecthr')[0].disabled = this.checked;
		$('.toptime .selectmin')[0].disabled = this.checked;
		$('.toptime .selectampm')[0].disabled = this.checked;
    }).change();
});



</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>