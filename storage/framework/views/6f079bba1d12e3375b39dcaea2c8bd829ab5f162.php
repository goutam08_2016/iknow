<?php $__env->startSection('pageTitle', 'Welcome to '); ?>
<?php $__env->startSection('customStyle'); ?><link href="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.css'); ?>" rel="stylesheet" type="text/css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	<?php echo $client_header; ?>

    	 
    <!--main content open-->
    <section class="testpage">
		<div class="deta-search"> &nbsp; </div>
		<div class="test-deta">
			<div class="wrapper">
				<div class="questionbox"> 
					<?php if($errors->any()): ?>						
					<div class="warnings">
						<div class="error_message">	
						<?php foreach($errors->all() as $error): ?>									
							<p><?php echo $error; ?></p>	
						<?php endforeach; ?>							
						</div>
					</div>	
					<?php endif; ?>	                	
					<form name="question" method="post" action="" enctype="multipart/form-data">						<?php echo e(csrf_field()); ?>                        
						<div class="questionrow">
							<h3>Question</h3>
							<div class="questionedit">
								<textarea name="question" class="texteditor"><?php echo e(!empty($question)?$question->question:''); ?></textarea>
							</div>
							<div class="fileuploded">
								<label class="uploadedbutton">
								<input name="attachment" type="file"/>
								<i class="fa fa-paperclip" aria-hidden="true"></i> Add Attachment                                
								</label>
							</div>	
							<?php if(!empty($question) && $question->attachment!=""): ?>								
							<img width="400" src="<?php echo e(asset('assets/upload/question_attachment/'.$question->attachment)); ?>" /><?php endif; ?>                        
						</div>
						<div id="myList">
							<h3>Answer </h3>
							<?php if(!empty($question->answers)): ?>
								<?php foreach($question->answers as $ky=>$val): ?>
								<div class="questionrow">
									<div class="answerhead">
										<span class="ansno"><?php echo $ky+1; ?></span>
										<label>
											<input type="checkbox" <?php echo e($val->is_correct=='Y'? 'checked' : ''); ?>  name="is_correct[<?php echo e($ky); ?>]" value="<?php echo e($ky); ?>">
											<span class="label">This answer option is correct</span>
										</label>
									</div>
									<div class="answerbox">
										<input type="text" name="answer[]" value="<?php echo e($val->answer); ?>">
									</div>
								</div>							
								<?php endforeach; ?>
							<?php else: ?>
							<div class="questionrow">
								<div class="answerhead">
									<span class="ansno">1</span>
									<label>
										<input type="checkbox" name="is_correct[0]" value="0">
										<span class="label">This answer option is correct</span>
									</label>
								</div>
								<div class="answerbox">
									<input type="text" name="answer[]" value="">
								</div>
							</div>
							<div class="questionrow">
								<div class="answerhead">
									<span class="ansno">2</span>
									<label>
									<input type="checkbox" name="is_correct[1]" value="1">
									<span class="label">This answer option is correct</span>
									</label>
								</div>
								<div class="answerbox">
									<input type="text" name="answer[]" value="">
								</div>
							</div>
							<div class="questionrow">
								<div class="answerhead">
									<span class="ansno">3</span>
									<label>
									<input type="checkbox" name="is_correct[2]" value="2">
									<span class="label">This answer option is correct</span>
									</label>
								</div>
								<div class="answerbox">
									<input type="text" name="answer[]" value="">
								</div>
							</div>
							<div class="questionrow">
								<div class="answerhead">
									<span class="ansno">4</span>
									<label>
									<input type="checkbox" name="is_correct[3]" value="3">
									<span class="label">This answer option is correct</span>
									</label>
								</div>
								<div class="answerbox">
									<input type="text" name="answer[]" value="">
								</div>
							</div>							
							<?php endif; ?>
						</div>
						<div class="questionrow">
							<a id="loadMore" class="colorbtn" href="javascript:void(0)">Add more answer options</a>
						</div>
                        <div class="questionrow nextrow">
                            <h3>Category</h3>
                            <div class="selectcategory">
                            	<select name="category">
									<option value="">Select Category</option>
									<?php if(!empty($categories)): ?>
										<?php foreach($categories as $val): ?>
										<option value="<?php echo e($val->id); ?>" <?php echo e(!empty($question) && $question->category == $val->id ?'selected':''); ?>  > <?php echo e($val->title); ?> </option>
										<?php endforeach; ?>
									<?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="questionrow nextrow">
                            <h3>Test</h3>
                            <div class="selectcategory">
                            	<select name="test" >
									<option value="">Select Test</option>
									<?php if(!empty($user->tests)): ?>
										<?php foreach($user->tests as $val): ?>
										<option value="<?php echo e($val->id); ?>" <?php echo e(!empty($question) && !empty($question->tests[0] ) && $question->tests[0]->pivot->test_id == $val->id ?'selected':''); ?> > <?php echo e($val->title); ?> </option>
										<?php endforeach; ?>
									<?php endif; ?>
                                </select>
                            </div>
                        </div>
						<div class="hoz-row clear">
							<div class="questionrow nextrow">
								<h3>Points Available</h3>
								<div class="pointbox">
									<input type="number" name="point" min="0" value="<?php echo e(!empty($question)?$question->point:'1'); ?>">
								</div>
							</div>
							<div class="questionrow nextrow">
								<h3>Negative Point for wrong answer</h3>
								<div class="pointbox">
									<input type="number" name="negative_point" min="0" value="<?php echo e(!empty($question)?$question->negative_point:'0'); ?>">
								</div>
							</div>
						</div>
						<div class="buttonbox clear">
							<input type="hidden" name="question_type" value="<?php echo e($qsn_type); ?>">							<?php if(!empty($question)): ?>                            
							<input type="hidden" name="question_id" value="<?php echo e($question->id); ?>">							<?php endif; ?>                            
							<input type="submit" value="Save">
							<input type="reset" value="cancel">
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
    <!--main content close-->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('customScript'); ?><script src="<?php echo asset('assets/frontend/js/stacktable.min.js'); ?>"></script><script src="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.js'); ?>"></script><script src="<?php echo asset('assets/frontend/ckeditor/ckeditor.js'); ?>"></script>
<script>
$(document).ready(function() {
	CKEDITOR.replaceClass = 'texteditor';
	$('.qstable').cardtable();
	$('.qspopup').fancybox({	padding:0	});
	var k = $('#myList .questionrow').length+1;	$('#loadMore').click(function () { 
		if($('#myList .questionrow').length < 7) { 
		// Hope you add a class to the answer 
			$("#myList .questionrow:last-child").clone().appendTo("#myList"); 
		}
		if($('#myList .questionrow').length < 7){
			}
		else{
			$('#loadMore').addClass('noclick');		
		}
		$('#myList .questionrow:last-child .ansno').text(k++);
		//$('#myList .questionrow:last-child .ansno').text("#");
	});
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>