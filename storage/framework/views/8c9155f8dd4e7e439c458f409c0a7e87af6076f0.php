<?php $__env->startSection('pageTitle', 'friend request'); ?>

<?php $__env->startSection('content'); ?>
<section class="mainbody clear">
<?php echo $__env->make('include.left_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<!--middle open-->
<div class="middlecol">
	<div class="contactNext">
		<div class="contactHead">
			<div class="titlebox clear">
				<h2><i class="fa fa-user" aria-hidden="true"></i> Contact</h2>
				<a href="">Friend Request<span><?php echo count($user->user_friend_requests); ?></span></a>
			</div>
			<div class="smallHead clear">
				<ul class="quickLink">
					<li><a href="">Friends</a></li>
					<li><a href="">Followers</a></li>
					<!--<li><a href="#">Friend Request<span>24</span></a></li>-->
				</ul>
				<div class="frndSrch">
					<a href="" class="srchTool">Advance Search</a>
					<div class="adnanceSrc">
						<form name="srch" method="get" action="">
							<input type="text" placeholder="Search Friends">
							<button type="submit" value="search"><i class="fa fa-search" aria-hidden="true"></i></button>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		<!--<div class="topPeople clear">
				<div class="filLeft clear">
					<div class="filCol">
						<span class="label">Location</span>
						<div class="selectbox">
							<input type="text" value="">
						</div>
					</div>
					<div class="filCol">
						<span class="label">Profession</span>
						<div class="selectbox">
							<input type="text" value="">
						</div>
					</div>
				</div>
				<!--<a href="" class="recLink bluebtn">Recommendation</a>-->
			<!--</div>-->
		
		<div class="conTab">
			
			<div class="closebar">
				<ul>
					<li class="active"><a href="javascript:void(0);">Friend Request</a></li>
				</ul>
			</div>
			
			<div class="contList tabContWrap">
				
				<div class="allpeople tabCont">
					<div class="wrapnumber">
						<ul class="listName caSlide">
							<li class="item"><a href="">All</a></li>
							<li class="item"><a href="">A</a></li>
							<li class="item"><a href="">b</a></li>
							<li class="item"><a href="">c</a></li>
							<li class="item"><a href="">d</a></li>
							<li class="item"><a href="">e</a></li>
							<li class="item"><a href="">f</a></li>
							<li class="item"><a href="">g</a></li>
							<li class="item"><a href="">h</a></li>
							<li class="item"><a href="">i</a></li>
							<li class="item"><a href="">j</a></li>
							<li class="item"><a href="">k</a></li>
							<li class="item"><a href="">l</a></li>
							<li class="item"><a href="">m</a></li>
							<li class="item"><a href="">n</a></li>
							<li class="item"><a href="">o</a></li>
							<li class="item"><a href="">p</a></li>
							<li class="item"><a href="">q</a></li>
							<li class="item"><a href="">r</a></li>
							<li class="item"><a href="">s</a></li>
							<li class="item"><a href="">t</a></li>
							<li class="item"><a href="">u</a></li>
							<li class="item"><a href="">v</a></li>
							<li class="item"><a href="">w</a></li>
							<li class="item"><a href="">x</a></li>
							<li class="item"><a href="">y</a></li>
							<li class="item"><a href="">z</a></li>
						</ul>
					</div>
					<div class="newAll srcCont clear">					
					<?php if(count($user->user_friend_requests)>0): ?>
						<?php foreach($user->user_friend_requests as $frndreq): ?>
						<div class="peopleBox">
							<div class="listWrap">
								<div class="topCon clear">
									<div class="conThumb">
										<figure>
											<a href="<?php echo e(url('profile/'.$frndreq->form_user->id)); ?>" target="_blank" class="btnText"><img src="<?php echo asset('assets/frontend'); ?>/images/profile.jpg" width="100" height="100" alt=""></a>
										</figure>
										<span class="ageCnt"><i class="fa <?php echo e($frndreq->form_user->gender=='M'?'fa-mars':'fa-venus'); ?>" aria-hidden="true"></i><?php echo e($frndreq->form_user->age); ?> yrs</span>
									</div>
								</div>
								<div class="peopleDesc">
									<div class="vMiddle">
										<div class="MiddleText">
											<h4><a href="<?php echo e(url('profile/'.$frndreq->form_user->id)); ?>" target="_blank"><?php echo e($frndreq->form_user->first_name); ?> <?php echo e($frndreq->form_user->last_name); ?></a><span class="avlTxt online"><samp>available</samp></span></h4>
											
											<p><?php $we = $frndreq->form_user->work_experiences()->orderBy('id','desc')->first();?>
											<?php if(!empty($we)): ?>
											<?php echo e($we->title); ?> - <?php echo e($we->company); ?>

											<?php endif; ?>
											</p>
											<p><?php $edu = $frndreq->form_user->educations()->orderBy('id','desc')->first();?>
											<?php if(!empty($edu)): ?>
											<?php echo e($edu->institution); ?> - <?php echo e($edu->major); ?>

											<?php endif; ?>
											</p>
											<p><?php $live = $frndreq->form_user->user_Addresses()->where('is_default','Y')->first();?>
											<?php if(!empty($live)): ?>
												Live In - <?php echo e($live->address); ?>

											<?php endif; ?>
											</p>
										</div>
									</div>
									<div class="chatRight">
										<a href="javascript:void(0);" class="acceptfrnd" style="<?php echo e($frndreq->status=='Y'?'background-color: #f39c11':''); ?>"><i class="fa fa-check" aria-hidden="true"></i> <span><?php echo e($frndreq->status=='Y'?'Accepted':'Accept'); ?></span></a>
										<a href="javascript:void(0);" class="delfrnd"><i class="fa fa-ban" aria-hidden="true"></i> Delete</a>
										<input type="hidden" value="<?php echo e($frndreq->from_user_id); ?>"/>
									</div>
								</div>
								<div class="shortDesc">
									<div class="descText">
										<div class="descTop clear">
											<figure><img src="<?php echo asset('assets/frontend'); ?>/images/profile.jpg" alt=""></figure>
											<h4><a href=""><?php echo e($frndreq->form_user->first_name); ?> <?php echo e($frndreq->form_user->last_name); ?></a><span><i class="fa fa-mars" aria-hidden="true"></i><?php echo e($frndreq->form_user->age); ?> yrs</span></h4>
											<p>
											<?php if(!empty($we)): ?>
											<?php echo e($we->title); ?> - <?php echo e($we->company); ?>

											<?php endif; ?>
											</p>
											<p>
											<?php if(!empty($edu)): ?>
											<?php echo e($edu->institution); ?> - <?php echo e($edu->major); ?>

											<?php endif; ?>
											</p>
											<p>
											<?php if(!empty($live)): ?>
												Live In - <?php echo e($live->address); ?>

											<?php endif; ?>
											</p>
										</div>
										<div class="deskBtm clear">
											<a href=""><i class="fa fa-commenting-o" aria-hidden="true"></i> Message</a><a href=""><i class="fa fa-phone" aria-hidden="true"></i> Call</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach; ?>						
					<?php else: ?>
						<h3>No Request Found..</h3>
					<?php endif; ?>
					</div>
					<!--<div class="pagination">
						<ul>
							<li class="prev"><a href="">Prev</a></li>
							<li class="current"><span>1</span></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
							<li><span>...</span></li>
							<li><a href="">6</a></li>
							<li class="next"><a href="">Next</a></li>
						</ul>
					</div>-->
				</div>
				
			</div>
		</div>
	</div>
</div>
<!--middle close-->

<?php echo $__env->make('include.right_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('customScript'); ?>
<script>

$(document).on( "click",".acceptfrnd", function( event ) {

  //event.preventDefault();
  var obj =  $( this );
  var frnd_id = obj.parent().children('input').val();
  //alert(frnd_id);
	 $('.sndmsg').html('');
	$.ajax({
		type:"post",
		url: "<?php echo url('user/accept-friend-request'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: {'frnd_id':frnd_id},			
		//dataType: "json",			
		success:function(res) {
			/* console.log(res); */
			if(res==0){
				obj.find('span').text('Accepted');
				obj.css('background-color', '#f39c11');
			}
		}
	});/* */
});
$(document).on( "click",".delfrnd", function( event ) {

  //event.preventDefault();
  var obj =  $( this );
  var frnd_id = obj.parent().children('input').val();
  //alert(frnd_id);
    if (confirm("Are you delete?") == true) {        
		$.ajax({
			type:"post",
			url: "<?php echo url('user/del-friend-request'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {'frnd_id':frnd_id},			
			//dataType: "json",			
			success:function(res) {
				/* console.log(res); */
				if(res==0){
					obj.parent().parent().parent().parent('.peopleBox').remove();
				}
			}
		});		
    }
	
});


</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>