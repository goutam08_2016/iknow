<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	<?php echo $client_header; ?>

    	 
    <!--main content open-->
    <section class="testpage">
		<?php if(session('cat_success')): ?>
			<div class="success_message">
				<p><?php echo session('cat_success'); ?></p>
			</div>
			<?php session()->forget('cat_success');?>
		<?php endif; ?>		<div class="deta-search">		   <div class="wrapper clear">           			  <div class="qscatagorypopup" id="qscategory">				 <div class="qswrap">					<ul>					   <li><a href="#">Open question</a></li>					   <li><a href="#">Multiple question</a></li>					   <li><a href="#">Multiple choice question</a></li>					   <li><a href="#">Fill the gap</a></li>					   <li><a href="#">Connect</a></li>					   <li><a href="#">Question like: rate in points</a></li>					   <li><a href="#">Organize in order</a></li>					</ul>				 </div>			  </div>		   </div>		</div> 		<div class="test-deta">		   <div class="wrapper">			  <div class="resultsarea">				 <div class="displayrslt">					<strong>Displaying results</strong>: 1 to 1 ordered by                         					<span class="inlinebox">					   <select class="flomfield">						  <option>Date</option>					   </select>					</span>				 </div>				 <!--<section class="pagination">					 <span><a href=""><i class="fa fa-angle-left"></i> Previous</a></span>					 <span class="active"><a href="">1</a></span>					 <span><a href="">2</a></span>					 <span><a href="">3</a></span>					 <span><a href="">Next <i class="fa fa-angle-right"></i></a></span>				 </section>-->				 <section class="resulttable">					<table>					   <thead>						  <tr>							 <th>								<select class="flomfield">								   <option>Date</option>								</select>							 </th>							 <th>Percentage</th>							 <th>Score</th>							 <th>Duration</th>							 <th class="date">Date</th>							 <th>								<select class="flomfield">								   <option>Statistics</option>								</select>							 </th>						  </tr>					   </thead>					   <tbody>						<?php if(!empty($test_evals)): ?>						  <?php foreach($test_evals as $val): ?>							  <?php if($val->total_point == 0)	$val->total_point=1;?>                            							  <tr class="average">							 <td><i><img src="<?php echo asset('assets/frontend/images/broken-link-small.png'); ?>"></i><a href="<?php echo e(url('user/show-report/'.$val->id)); ?>"><?php echo e($val->first_name); ?> <?php echo e($val->last_name); ?></a></td>							 <td>								<div class="progressbarPnl"><samp><?php echo round(($val->score/$val->total_point)*100); ?>%</samp> <span class="progressbar"><span class="progressbar-rule" style="width:<?php echo round(($val->score/$val->total_point)*100); ?>%;"></span></span></div>							 </td>							 <td><?php echo e($val->score); ?>/<?php echo e($val->total_point); ?></td>							 <td>
							 <?php $timer = $val->time_spend;
								$hours = (int)($timer / 3600);
								$minutes = (int)($timer / 60) - ($hours * 60);
								$seconds = (int)($timer % 60);

								$hours = $hours < 10 ? "0" . $hours : $hours;
								$minutes = $minutes < 10 ? "0" . $minutes : $minutes;
								$seconds = $seconds < 10 ? "0" . $seconds : $seconds;
								
								echo($hours . ":" . $minutes . ":" . $seconds);
							 ?>						 
							 </td>							 <td class="date"><?php echo e(date("D d M'y h:ia",strtotime($val->updated_at))); ?> </td>							 <td><a href="<?php echo e(url('user/answers-report/'.$val->id)); ?>" class="button green">Answers</a></td>						  </tr>						  <?php endforeach; ?>							<?php endif; ?>                            					   </tbody>					</table>				 </section>			  </div>		   </div>		</div>	
    </section>
    <!--main content close-->
    
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script>
$(document).ready(function() {
	 $('.newtest').click(function() {
		$('.addnewbox').slideDown(200);
		$("#addNwcat")[0].reset();
	});
	$('.newformwrap .closebtn').click(function() {
		$('.addnewbox').slideUp(200);
	});
	<?php if($errors->any()): ?>
		$('.addnewbox').slideDown(200);
	<?php endif; ?>
});
/* $(".rmve").click(function() {
	var obj= $(this);
	//console.log(obj.parent().parent().find('.ttl').html());
	var txt = obj.parent().parent().find('.ttl').text();
	var catId= obj.children('input').val();
	var r = confirm("Are you sure to delete group "+txt+"?");
	if (r == true) {
		$.ajax({
			type:"post",
			url: "<?php echo url('user/del-cat'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {id: catId},
			//dataType: "json",
			success:function(data) {
				//console.log(data);
				obj.parent().parent().remove();
			}
		});
	}	
	
}); */

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>