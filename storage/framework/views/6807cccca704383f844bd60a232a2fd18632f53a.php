<?php $__env->startSection('pageTitle', 'Register a new account | '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

 
    <!--top section open-->
    <section class="all-top">
    	<div class="wrapper">
        	<h2>Register free for online testing</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it 
 make a type specimen book. It has survived not only five centuries, but also the leap into electronic </p>
        </div>
    </section>
    <!--top section close-->
    
    <!--main content open-->
    <section class="innerpages">
    	<div class="wrapper clear">
        	<div class="middle-border-box registerwrap clear">
                <div class="registerbox">
                	<div class="reg-icon"><figure><img src="<?php echo asset('assets/frontend/images/small-img.png'); ?>" width="238" height="133" alt=""></figure></div>
                    <h3>Business</h3>
                    <span>Lorem Ipsum is simply dummy text 
of the printing and typesetting</span>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it  make a type specimen book. It has survived not only five centuries, but also the leap into electronic.... </p>
					<a href="<?php echo e(url('register/2')); ?>" class="colorbtn bluebtn">Register Now</a>
                </div>
                <div class="registerbox">
                	<div class="reg-icon"><figure><img src="<?php echo asset('assets/frontend/images/small-img02.png'); ?>" width="200" height="69" alt=""></figure></div>
                    <h3>Education</h3>
                    <span>Lorem Ipsum is simply dummy text 
of the printing and typesetting</span>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it  make a type specimen book. It has survived not only five centuries, but also the leap into electronic.... </p>
					<a href="<?php echo e(url('register/1')); ?>" class="colorbtn bluebtn">Register Now</a>
                </div>
            </div>
        </div>
    </section>
    <!--main content close-->
    
        
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>