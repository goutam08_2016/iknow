<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */
$resource = 'cms-page';
$resource_pl = str_plural("cms");
$page_title = ucfirst(str_replace('_', ' ', $resource));
$page_title_pl = ucfirst(str_replace('_', ' ', $resource_pl));
// Localization data...
$locale = app()->getLocale();

// Resolute the data...
$data = ${$resource_pl};
//dd($data);
$escapePages = array('contact-us','pricing');
?>



<?php $__env->startSection('pageTitle', 'Edit ' . $page_title); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $page_title; ?>
            <small>Edit <?php echo $data->firstname; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="<?php echo url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="<?php echo url("admin/$resource"); ?>"><i class="fa  fa-user"></i> <?php echo $page_title_pl; ?></a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit <?php echo e($page_title); ?></h3>
                    </div>
                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger">
                            <?php foreach($errors->all() as $error): ?>
                                <p><?php echo $error; ?></p>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <?php if(session('success')): ?>
                    <div class="alert alert-success">
                        <?php echo session('success'); ?>
                    </div>
                    <?php endif; ?>
                    <form method="POST" action="<?php echo admin_url($resource . '/' . ${$resource_pl}->id); ?>" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PATCH"> <?php echo e(csrf_field()); ?>
                        <div class="box-body">
                            <?php foreach($languages as $language): ?>
                                <div class="form-group">
                                    <label for="title_<?php echo e($language->locale); ?>" class="col-sm-2 control-label">Title (<?php echo e($language->name); ?>) <?php echo e($language->fallback_locale === 'Y' ? '*' : ''); ?></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="title_<?php echo e($language->locale); ?>" name="title_<?php echo e($language->locale); ?>" placeholder="Title" value="<?php echo e($data->translate($language->locale) !== null ? $data->translate($language->locale)->title : ''); ?>" />
                                    </div>
                                </div>								<?php if(!in_array($data->translate($language->locale)->slug,$escapePages)): ?>
								<div class="form-group">
                                    <label for="page_content_<?php echo e($language->locale); ?>" class="col-sm-2 control-label">Page content (<?php echo e($language->name); ?>) <?php echo e($language->fallback_locale === 'Y' ? '*' : ''); ?></label>
                                    <div class="col-sm-6">
                                        <textarea style="height: 200px; resize: none;" type="text" class="form-control" id="page_content_<?php echo e($language->locale); ?>" name="page_content_<?php echo e($language->locale); ?>" placeholder="Content"><?php echo e($data->translate($language->locale) !== null ? $data->translate($language->locale)->page_content : ''); ?></textarea>
                                    </div>
                                </div>								<?php endif; ?>                                <div class="form-group">                                    <label for="seo_title_<?php echo e($language->locale); ?>" class="col-sm-2 control-label">SEO title (<?php echo e($language->name); ?>) <?php echo e($language->fallback_locale === 'Y' ? '*' : ''); ?></label>                                    <div class="col-sm-6">                                        <input type="text" class="form-control" id="seo_title_<?php echo e($language->locale); ?>" name="seo_title_<?php echo e($language->locale); ?>" placeholder="seo title" value="<?php echo e($data->translate($language->locale) !== null ? $data->translate($language->locale)->seo_title : ''); ?>" />                                    </div>                                </div>																<div class="form-group">                                    <label for="seo_keywords_<?php echo e($language->locale); ?>" class="col-sm-2 control-label">SEO keywords (<?php echo e($language->name); ?>) <?php echo e($language->fallback_locale === 'Y' ? '*' : ''); ?></label>                                    <div class="col-sm-6">                                        <textarea style="height: 200px; resize: none;" type="text" class="form-control" id="seo_keywords_<?php echo e($language->locale); ?>" name="seo_keywords_<?php echo e($language->locale); ?>" placeholder="seo keywords"><?php echo e($data->translate($language->locale) !== null ? $data->translate($language->locale)->seo_keywords : ''); ?></textarea>                                    </div>                                </div>								<div class="form-group">                                    <label for="seo_description_<?php echo e($language->locale); ?>" class="col-sm-2 control-label">SEO description (<?php echo e($language->name); ?>) <?php echo e($language->fallback_locale === 'Y' ? '*' : ''); ?></label>                                    <div class="col-sm-6">                                        <textarea style="height: 200px; resize: none;" type="text" class="form-control" id="seo_description_<?php echo e($language->locale); ?>" name="seo_description_<?php echo e($language->locale); ?>" placeholder="seo description"><?php echo e($data->translate($language->locale) !== null ? $data->translate($language->locale)->seo_description : ''); ?></textarea>                                    </div>                                </div>								
                            <?php endforeach; ?>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a class="btn btn-default" href="<?php echo url("admin/$resource"); ?>">
                                Back</a>
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </div><!-- /.box-footer -->
                        </form>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>