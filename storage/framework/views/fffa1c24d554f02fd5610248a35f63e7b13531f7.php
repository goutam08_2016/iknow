<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    
    <!--main content open-->
    
    <section class="testquizbody">
    	<div class="testpaper-wrap">
        	<h2><?php echo e($test->title); ?></h2>
            <div class="test_container">
				<?php if($errors->any()): ?>
					<div class="warnings">
						<div class="error_message">
							<?php foreach($errors->all() as $error): ?>
							<p><?php echo $error; ?></p>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
            	<form name="test" method="post" >
				<?php echo e(csrf_field()); ?>

                	<div class="formrow">
                    	<label>
                        	<span class="label">First name</span>
                            <div class="textfld">
                            	<input type="text" name="first_name" value="">
                            </div>
                        </label>
                    </div>
                    <div class="formrow">
                    	<label>
                        	<span class="label">Last name</span>
                            <div class="textfld">
                            	<input type="text" name="last_name" value="">
                            </div>
                        </label>
                    </div>
                    <div class="formrow">
                    	<label>
                        	<span class="label">Email address</span>
                            <div class="textfld">
                            	<input type="email" name="email_address" value="">
                            </div>
                        </label>
                    </div>
                    <div class="testbtnrow">
                        <input type="hidden" name="test_id" value="<?php echo e($test->id); ?>" />
                        <input type="submit" value="Start Test" class="colorbtn" />
                    </div>
                </form>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
<script>
<?php if($test->allow_go_back=="N"): ?>
function preventBack(){window.history.forward();}
  setTimeout("preventBack()", 0);
  window.onunload=function(){null};
<?php endif; ?>
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.basic', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>