<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('seo_sec'); ?>
<title> <?php echo e($cmsdata->seo_title); ?></title>
<meta name="description" content="<?php echo e($cmsdata->seo_description); ?>">
<meta name="keywords" content="<?php echo e($cmsdata->seo_keywords); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>


    <!--top section open-->
    <section class="test-top">
    	<div class="wrapper clear">
        	<h2>Contact</h2>
        </div>
    </section>
    <!--top section close-->
    
    <!--main content open-->
    <section class="testpage blogpage">
    	<div class="wrapper clear">
            <div class="contactMap" >
                <div id="map_cnv" style="height:300px;"></div>
            </div>
            <div class="contactBottom clear">
                <div class="conLeft">
                    <div class="conWrap">
                        <h2>Contact Us</h2>
                        <div class="conForm">
                            <p class="formTag">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravida odio, sit amet suscipit risus ultrices eu. Fusce viverra neque at purus laoreet consequat.</p>
							<?php if($errors->any()): ?>
								<div class="warnings">
									<div class="error_message">
										<?php foreach($errors->all() as $error): ?>
											<p><?php echo $error; ?></p>
										<?php endforeach; ?>
									</div>
								</div>
							<?php elseif(session('success')): ?>
								<div class="success_message">
									<p><?php echo session('success'); ?></p>
								</div>
								<?php session()->forget('success');?>
							<?php endif; ?>
                            <form name="confrm" method="post" action="<?php echo e(url('contact-us')); ?>">
							   <?php echo e(csrf_field()); ?>

                                <div class="formLine">
                                    <label>
                                        <span class="label">Your name</span>
                                        <div class="textfld">
                                            <input type="text" name="name" value="" />
                                        </div>
                                    </label>
                                </div>
                                <div class="formLine">
                                    <label>
                                        <span class="label">Your email</span>
                                        <div class="textfld">
                                            <input type="email" name="email" value="" />
                                        </div>
                                    </label>
                                </div>
                                <div class="formLine">
                                    <label>
                                        <span class="label">Your Message</span>
                                        <div class="textfld">
                                            <textarea name="comments"></textarea>
                                        </div>
                                    </label>
                                </div>
                                <div class="textfld submitBtn"><input type="submit" value="Send" class="colorbtn"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="conRight">
                    <div class="conWrap">
                        <h2>Contact Us</h2>
                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravida odio, sit amet suscipit risus ultrices eu. Fusce viverra neque at purus laoreet consequat. Vivamus vulputate posuere nisl quis consequat. Donec congue commodo mi, sed commodo velit fringilla ac. </p>
                        <ul> -->
                            <li>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>Address :
                                <p><?php echo e($setting->contact_address); ?></p>
                            </li>
                            <li>
                                <i class="fa fa-phone" aria-hidden="true"></i>Phone Number :
                                <p><?php echo e($setting->contact_phone); ?></p>
                            </li>
                            <li>
                                <i class="fa fa-envelope" aria-hidden="true"></i>Email Id :
                                <p><?php echo e($setting->contact_email); ?></p>
                            </li>
                            <!-- <li>
                                <i class="fa fa-share-square-o" aria-hidden="true"></i>Social Links :
                                <p>
                                    <a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                                    <a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                                    <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                                    <a href=""><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
                                    <a href=""><i class="fa fa-rss-square" aria-hidden="true"></i></a>
                                </p>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--main content close-->
    

<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
<script type="text/javascript">
function initMap() {
	var map = new google.maps.Map(document.getElementById('map_cnv'), {
	  center: { lat: <?php echo e($setting->lat); ?>, lng: <?php echo e($setting->lng); ?> },
	  zoom: 8
	});
	var myLatlng = {lat: <?php echo e($setting->lat); ?>, lng: <?php echo e($setting->lng); ?> };
	var marker = new google.maps.Marker({
	  position: myLatlng,
	  map: map,
	});
}
</script>	
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6g0i6uEsWrZ3mRuSddpe_-pObwo_BGbU&libraries=places&callback=initMap"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>