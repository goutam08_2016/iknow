<section class="sidebar">
	<!-- Sidebar user panel -->
	<div class="user-panel">
		<div class="pull-left image">
			<img src="<?php echo asset('assets/admin/dist/img/avatar5.png'); ?>" class="img-circle" alt="User Image">
		</div>
		<div class="pull-left info">
			<p>Admin</p>
			<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
		</div>
	</div>
	<ul class="sidebar-menu">
		<li class="header">MAIN NAVIGATION</li>
		<li>
			<a href="<?php echo admin_url(); ?>">
				<i class="fa fa-dashboard"></i> <span>Dashboard</span>
			</a>
		</li>
		<li class="<?php echo Request::segment(2) == 'settings'?'treeview active':'treeview'; ?>">
			<a href="<?php echo admin_url('settings'); ?>">
				<i class="fa fa-cog"></i> <span>Settings</span>
			</a>
		</li>		
		<li class="treeview<?php echo Request::segment(2) == 'email-template' || Request::segment(3) == 'email-template' ? ' active' : ''; ?>">
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Email Templates</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'email-template' && Request::segment(3) == '') || Request::segment(3) == 'email-template' ? 'active' : ''; ?>">
					<a href="<?php echo admin_url('email-template'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
				<!-- <li class="<?php echo Request::segment(2) == 'email-template' && Request::segment(3) == 'create'?'active':''; ?>">
					<a href="<?php echo admin_url('email-template/create'); ?>"><i class="fa fa-plus-circle"></i> Add</a>
				</li> -->
			</ul>
		</li>
		<li class="<?php echo Request::segment(2) == 'user'?'treeview active':'treeview'; ?>">
			<a href="#">
				<i class="fa fa-user"></i>
				<span>Manage Users</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo Request::segment(2) == 'user' && Request::segment(3) == ''?'active':''; ?>">
					<a href="<?php echo admin_url('user'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
			</ul>
		</li>
		<!-- <li class="<?php echo Request::segment(2) == 'dealer'?'treeview active':'treeview'; ?>">
			<a href="#">
				<i class="fa fa-user"></i>
				<span>Manage Dealer</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo Request::segment(2) == 'dealer' && Request::segment(3) == ''?'active':''; ?>">
					<a href="<?php echo admin_url('dealer'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
			</ul>
		</li> -->		
		
		<li class="treeview<?php echo Request::segment(2) == 'package' || Request::segment(3) == 'package' ? ' active' : ''; ?>">
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Manage Pricing</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'package' && Request::segment(3) == '') || Request::segment(3) == 'package' ? 'active' : ''; ?>">
					<a href="<?php echo admin_url('package'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
				<li class="<?php echo Request::segment(2) == 'package' && Request::segment(3) == 'create'?'active':''; ?>">
					<a href="<?php echo admin_url('package/create'); ?>"><i class="fa fa-plus-circle"></i> Add</a>
				</li>
			</ul>
		</li>
		
		<li class="treeview<?php echo Request::segment(2) == 'cms-page' || Request::segment(3) == 'cms-page' ? ' active' : ''; ?>">
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>CMS Pages</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'cms-page' && Request::segment(3) == '') || Request::segment(3) == 'cms-page' ? 'active' : ''; ?>">
					<a href="<?php echo admin_url('cms-page'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
				<li class="<?php echo Request::segment(2) == 'cms-page' && Request::segment(3) == 'create'?'active':''; ?>">
					<a href="<?php echo admin_url('cms-page/create'); ?>"><i class="fa fa-plus-circle"></i> Add</a>
				</li>
			</ul>
		</li>
		
		<!--<li class="treeview<?php echo Request::segment(2) == 'adsences' || Request::segment(3) == 'adsences' ? ' active' : ''; ?>">
			<a href="#">
				<i class="fa fa-bars"></i>
				<span>Adsences</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="<?php echo (Request::segment(2) == 'adsences' && Request::segment(3) == '') || Request::segment(3) == 'adsences' ? 'active' : ''; ?>">
					<a href="<?php echo admin_url('adsences'); ?>"><i class="fa fa-circle-o"></i> List</a>
				</li>
				<li class="<?php echo Request::segment(2) == 'adsences' && Request::segment(3) == 'create'?'active':''; ?>">
					<a href="<?php echo admin_url('adsences/create'); ?>"><i class="fa fa-plus-circle"></i> Add</a>
				</li>
			</ul>
		</li>-->
		
	</ul>
</section>