<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>    
    <!--main content open-->
    
    <section class="testquizbody">
    	<div class="testpaper-wrap">
        	<h2>Thank you for give exam <?php echo e($test->title); ?>.</h2>
            <div class="test_container">
				<div class="formrow">
					<label>
						<span class="label"><?php echo e($test_eval->first_name); ?> <?php echo e($test_eval->last_name); ?> soon we will response you.</span>
					</label>
				</div>
				<div class="testbtnrow">
					<input type="hidden" name="test_id" value="<?php echo e($test->id); ?>">
					<a href="<?php echo e(url('user/give-exam')); ?>" class="colorbtn">Go to exam</a>
				</div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.basic', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>