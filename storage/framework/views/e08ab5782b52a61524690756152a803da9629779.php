<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	<?php echo $client_header; ?>

    	 
	<!--main content open-->
    <section class="testpage">
    	<div class="deta-search">
        	<div class="wrapper clear">
            	
            </div>
        </div>
    	<div class="test-deta">
            <div class="wrapper">
                <div class="allresultarea">
                	<div class="answearreport">
                    	<span><img src="<?php echo asset('assets/frontend/images/check.jpg'); ?>"> Correct Answer</span>
                        <span><img src="<?php echo asset('assets/frontend/images/cross.jpg'); ?>"> Incorrect Answer</span>
                        <span><img src="<?php echo asset('assets/frontend/images/missed.jpg'); ?>"> Missed correct multiple choise options</span>
                    </div>
                	<div class="testpaper-wrap">
						<?php 
						if(!empty($test->questions))
						{
							foreach($test->questions as $ky=>$val)
							{
								//echo $val->question_type;
								switch ($val->question_type) 
								{
									case 1:
						?>
								<section class="type2">
									<h2>Q<?php echo $ky+1; ?>. <?php echo strip_tags($val->question)==''?$qsn_type_arr[$val->question_type]:strip_tags($val->question); ?></h2>	
									<?php if($val->attachment!=""): ?>
										<img width="400" src="<?php echo e(asset('assets/upload/question_attachment/'.$val->attachment)); ?>" />
									<?php endif; ?>
									<?php $tval = $val->test_answer()->where('test_eavl_id',$test_eval->id)->first();
									if(!empty($tval))
									{
									?>
									<article class="finalans">
										<div class="adv">Answer :
										<?php echo $tval->answer; ?> 										
										</div>
										<div class="givemarks">
											<?php if($user->user_type==2): ?>
											<label><input class="isc" type="radio" name="is_correct<?php echo e($val->id); ?>" value="Y" <?php echo e($tval->is_correct=='Y'?'checked':''); ?> > <samp>Correct</samp> <span class="mark check"></span></label>
											<label><input class="isc" type="radio" name="is_correct<?php echo e($val->id); ?>" value="N" <?php echo e($tval->is_correct=='N'?'checked':''); ?> > <samp>incorrect</samp> <span class="mark cross"></span></label>
											<input class="ansid" type="hidden" value="<?php echo e($tval->id); ?>"/>
											<?php else: ?>
											<span class="mark <?php echo e($tval->is_correct=='Y'?'check':'cross'); ?> "></span>
											<?php endif; ?>
										</div>
									</article>
									<?php 
									}
									?>
								</section>
						<?php
									break;
									case 2:
						?>
								<section class="type3">
									<h2>Q<?php echo $ky+1; ?>. <?php echo strip_tags($val->question)==''?$qsn_type_arr[$val->question_type]:strip_tags($val->question); ?></h2>
									<?php
									if(!empty($val->test_answer()->where('test_eavl_id',$test_eval->id)->first()))
									{
									$tval = $val->test_answer()->where('test_eavl_id',$test_eval->id)->first();
									$ans_arr = explode(',',$tval->answer);
									/* print_r($ans_arr); */
									?>
									<article>										
										<div class="question-answer">
										<?php if(!empty($val->answers)): ?>
											<?php foreach($val->answers as $ir=>$ans): ?>
											<div class="formrow">
												<label><input type="Checkbox" name="answer" <?php echo e(in_array($ans->id,$ans_arr)?'checked':''); ?> disabled /><span class="label"><strong><?php echo $ir+1; ?></strong><?php echo e($ans->answer); ?></span></label>
												<?php if(in_array($ans->id,$ans_arr) && $ans->is_correct=='Y'): ?>
												<span class="mark check"></span>
												<?php elseif(!in_array($ans->id,$ans_arr) && $ans->is_correct=='Y'): ?>
												<span class="mark missed"></span>	
												<?php elseif(in_array($ans->id,$ans_arr) && $ans->is_correct=='N'): ?>
												<span class="mark cross"></span>												
												<?php endif; ?>
											</div>
											<?php endforeach; ?>
										<?php endif; ?>										
										</div>
									</article>
									<?php 										
									}									
									?>
								</section>
						<?php
									break;
									case 3:
						?>
								<section class="type1">
									<h2>Q<?php echo $ky+1; ?>. <?php echo strip_tags($val->question)==''?$qsn_type_arr[$val->question_type]:strip_tags($val->question); ?></h2>
									<?php
									$tval = $val->test_answer()->where('test_eavl_id',$test_eval->id)->first();
									if(!empty($tval))
									{
									?>
									<article>
										<div class="question-answer">
										<?php if(!empty($val->answers)): ?>
											<?php foreach($val->answers as $ir=>$ans): ?>
											<div class="formrow">
												<label><input type="radio" name="ans<?php echo e($val->id); ?>" disabled <?php echo e(($ans->id==$tval->answer)?'checked':''); ?>  /><span class="label"><strong><?php echo $ir+1; ?></strong><?php echo e($ans->answer); ?></span></label>
												<?php if($ans->id==$tval->answer && $ans->is_correct=='Y'): ?>
												<span class="mark check"></span>
												<?php elseif($ans->id==$tval->answer && $ans->is_correct=='N'): ?>
												<span class="mark cross"></span>												
												<?php endif; ?>
											</div>
											<?php endforeach; ?>
										<?php endif; ?>	
										</div>
									</article>
									<?php
									}
									?>
								</section>
						<?php
									break;
									case 4:
						?>
								<section class="type3">
									<h2>Q<?php echo $ky+1; ?>. <?php echo strip_tags($val->question)==''?$qsn_type_arr[$val->question_type]:strip_tags($val->question); ?></h2>
									<?php
									$tval = $val->test_answer()->where('test_eavl_id',$test_eval->id)->first();
									$ans_arr = explode(",",$tval->answer);
									?>
									<article>
										<ul>
										<?php if(!empty($ans_arr)): ?>
										<?php foreach($ans_arr as $ir=>$ans): ?>
											<li><strong>1)</strong> <?php echo e($ans); ?> <span class="mark <?php echo e($ans==$val->answers[$ir]->answer ?'check':'cross'); ?>"></span></li>
										<?php endforeach; ?>
										<?php endif; ?>
										</ul>
									</article>
								</section>
						<?php
									break;
									case 5:
						?>
								<section class="type4">
									<h2>Q<?php echo $ky+1; ?>. <?php echo strip_tags($val->question)==''?$qsn_type_arr[$val->question_type]:strip_tags($val->question); ?></h2>
									
									<?php if(!empty($val->test_answer))	 $ans_arr = $val->test_answer()->where('test_eavl_id',$test_eval->id)->first();
									if(!empty($ans_arr->answer))	$ans_arr = explode(',' , $ans_arr->answer);
									if(!empty($val->test_answer) && !empty($ans_arr))
									{
										foreach($ans_arr as $vl)
										{
											$exparr = explode('>>',$vl);
											$given_answer[trim($exparr[0])] = trim($exparr[1]);
										}
									
									//print_r($given_answer);
									?>
									<article>
										<table class="matchtable">
										<?php if(!empty($val->sub_questions)): ?>
											<?php foreach($val->sub_questions as $itr=>$subq): ?>
											<tr>
												<td><?php echo $subq->sub_question; ?></td>
												<?php if(isset($given_answer[$subq->id])): ?>
												<td><?php echo $val->sub_answers()->where('id',$given_answer[$subq->id])->first()->sub_answer; ?>

												</td>
												<td>
													<?php if($val->sub_answers()->where('id',$given_answer[$subq->id])->first()->correct_question == ($itr+1)): ?>
														<span class="mark check"></span>
													<?php else: ?>
														<span class="mark cross"></span>
													<?php endif; ?>
												</td>
												<?php endif; ?>
											</tr>
											<?php endforeach; ?>
										<?php endif; ?>
										</table>
									</article>
									<?php
									}
									?>
								</section>
						<?php
									break;
									case 6:
						?>
								<section class="type2">
									<h2>Q<?php echo $ky+1; ?>. <?php echo strip_tags($val->question)==''?$qsn_type_arr[$val->question_type]:strip_tags($val->question); ?></h2>
									<?php
									$tval = $val->test_answer()->where('test_eavl_id',$test_eval->id)->first();
									$val->answers;
									$ans_arr= explode(',',$tval->answer);
									?>
									<article class="finalans" >
										<p style="font-size:20px;"> Rate from <?php echo e($val->answers[0]->min_range); ?> till <?php echo e($val->answers[0]->max_range); ?> - <strong>Ans: <?php echo e($ans_arr[0]); ?></strong></p>
										<p><?php echo e(str_replace($ans_arr[0].',' ,'', $tval->answer)); ?></p>
										<div class="givemarks">
										<?php if($user->user_type==2): ?>
											<label><input class="isc" type="radio"name="is_correct<?php echo e($val->id); ?>" value="Y" <?php echo e($tval->is_correct=='Y'?'checked':''); ?> > <samp>Correct</samp> <span class="mark check"></span></label>
											<label><input class="isc" type="radio" name="is_correct<?php echo e($val->id); ?>" value="N" <?php echo e($tval->is_correct=='N'?'checked':''); ?> > <samp>incorrect</samp> <span class="mark cross"></span></label>
											<input class="ansid" type="hidden" value="<?php echo e($tval->id); ?>"/>
										<?php else: ?>
											<span class="mark <?php echo e($tval->is_correct=='Y'?'check':'cross'); ?> "></span>
										<?php endif; ?>
										</div>
									</article>
								</section>
						<?php
									break;
									case 7:
						?>
								<?php if(!empty($val->question)): ?>
								<section class="type3">
									<h2>Q<?php echo $ky+1; ?>. <?php echo strip_tags($val->question)==''?$qsn_type_arr[$val->question_type]:strip_tags($val->question); ?></h2>
									<?php $testval = $val->test_answer()->where('test_eavl_id',$test_eval->id)->first();
									if(!empty($testval))
									{
									$ans_arr = explode(',',$testval->answer);
									?>
									<article>
										<ul>										
										<?php if(!empty($ans_arr)): ?>
											<?php foreach($ans_arr as $itr=>$ansr): ?>
											<li><strong><?php echo $itr+1; ?>)</strong> <?php echo strip_tags($val->answers[$ansr-1]->answer); ?> <span class="mark <?php echo e(strip_tags($val->answers[$ansr-1]->answer) == strip_tags($val->answers[$itr]->answer) ?'check':'cross'); ?>"></span></li>
											<?php endforeach; ?>
										<?php endif; ?>
										</ul>
									</article>
									<?php
									}?>
								</section>
								<?php endif; ?>
						<?php
									break;
								}
							
							}
						}?>
						
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--main content close-->
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script>
$(document).ready(function() {
	 $('.newtest').click(function() {
		$('.addnewbox').slideDown(200);
		$("#addNwcat")[0].reset();
	});
	$('.newformwrap .closebtn').click(function() {
		$('.addnewbox').slideUp(200);
	});
	<?php if($errors->any()): ?>
		$('.addnewbox').slideDown(200);
	<?php endif; ?>
});
/* $(".rmve").click(function() {
	var obj= $(this);
	//console.log(obj.parent().parent().find('.ttl').html());
	var txt = obj.parent().parent().find('.ttl').text();
	var catId= obj.children('input').val();
	var r = confirm("Are you sure to delete group "+txt+"?");
	if (r == true) {
		$.ajax({
			type:"post",
			url: "<?php echo url('user/del-cat'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {id: catId},
			//dataType: "json",
			success:function(data) {
				//console.log(data);
				obj.parent().parent().remove();
			}
		});
	}	
	
}); */

$(".givemarks input.isc").click(function() {
	var obj = $(this);
	var inpval = obj.val();
	//console.log(obj.parent().parent().html());
	var ansid = obj.parent().parent().find('.ansid').val();
	//alert(obj.parent().parent().find('.ansid').val());
	if(inpval!='' && ansid!='')
	{
		$.ajax({
			type:"post",
			url: "<?php echo url('user/change-correction'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {iscrr: inpval, ansid:ansid},
			//dataType: "json",
			success:function(data) {
				console.log(data);
			}
		});
		
	}
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>