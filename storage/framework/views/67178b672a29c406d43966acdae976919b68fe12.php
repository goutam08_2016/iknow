<?php $__env->startSection('pageTitle', 'Register a new account | '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<!--top section open-->
    <section class="all-top">
    	<div class="wrapper">
        	<h2>Register free for online testing</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it 
 make a type specimen book. It has survived not only five centuries, but also the leap into electronic </p>
        </div>
    </section>
    <!--top section close-->
    
    <!--main content open-->
    <section class="innerpages">
    	<div class="wrapper clear">
        	<div class="middle-border-box registerform clear">
                <div class="register-left">
                	<h2>Register to Create &amp; Give Tests!</h2>
                    <p class="formtag">Lorem Ipsum is simply dummy text of the printing...</p>
					
					<?php if($errors->any()): ?>
						<div class="warnings">
							<div class="error_message">
								<?php foreach($errors->all() as $error): ?>
									<p><?php echo $error; ?></p>
								<?php endforeach; ?>
							</div>
						</div>
					<?php elseif(session('register_success')): ?>
						<div class="success_message">
							<p><?php echo session('register_success'); ?></p>
						</div>
						<?php session()->forget('register_success');?>
					<?php endif; ?>
                    <form method="post" id="registerForm" action="<?php echo e(url('/register')); ?>" data-parsley-validate="">
						<?php echo e(csrf_field()); ?>

						<input type="hidden" name="user_type" value="1"/>
						<input type="hidden" name="utype" value="<?php echo e($type); ?>"/>
                    	<div class="formrow clear">
                        	<div class="formcol">
                            	<label>
                                	<span class="label">First Name * </span>
                                    <div class="textfld">
                                    	<input type="text" name="first_name" required value="<?php echo e(old('first_name')); ?>" />
                                    </div>
                                </label>
                            </div>
                            <div class="formcol">
                            	<label>
                                	<span class="label">Last Name * </span>
                                    <div class="textfld">
                                    	<input type="text" name="last_name" required value="<?php echo e(old('last_name')); ?>" />
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="formrow clear">
                        	<div class="formcol">
                            	<label>
                                	<span class="label">Username * </span>
                                    <div class="textfld">
                                    	<input type="text" name="user_name" data-parsley-trigger="change" required value="<?php echo e(old('user_name')); ?>"/>
                                    </div>
                                </label>
                            </div>
                            <div class="formcol">
                            	<label>
                                	<span class="label">Password * </span>
                                    <div class="textfld">
                                    	<input type="password" name="password" required  />
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="formrow clear">
                        	<div class="formcol">
                            	<label>
                                	<span class="label">Email address * </span>
                                    <div class="textfld">
                                    	<input type="email" name="email" data-parsley-trigger="change" required value="<?php echo e(old('email')); ?>" />
                                    </div>
                                </label>
                            </div>
                            <div class="formcol">
                            	<label>
                                	<span class="label">Enter your address * </span>
                                    <div class="textfld">
                                    	<input type="text" id="address" name="address" required value="<?php echo e(old('address')); ?>" />
										<input type="hidden" name="postal_code" id="postal_code" value="<?php echo e(old('postal_code')); ?>" />
										<input type="hidden" name="lat" id="lat" />
										<input type="hidden" name="lng" id="lng" />
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="formrow clear">
                        	
                            <div class="formcol">
                            	<label>
                                	<span class="label">Select your Time-Zone  * </span>
                                    <div class="textfld">
                                    	<select name="utc_timezone">
											<option value=""> Please select </option>
										<?php if(!empty($timezones)): ?>
											<?php foreach($timezones as $val): ?>
												<option value="<?php echo $val->id; ?>">( GMT <?php echo e($val->UTC_offset); ?> ) <?php echo e($val->TimeZone); ?> </option>
											<?php endforeach; ?>
										<?php endif; ?>                                        	
                                        </select>
                                    </div>
                                </label>
                            </div><!---->
							<div class="formcol captchabox">
                            	<label>
                                	<span class="label">&nbsp;</span>
                                    <div class="textfld">
                                    	<!--<img src="<?php echo asset('assets/frontend/images/captcha.png'); ?>" width="257" height="77" alt="">-->
										<div class="g-recaptcha" data-sitekey="6LcO6gYUAAAAAJKTMn09SxBQymPjhVLOWnygIVEm"></div>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="formrow terms">
                        	<label>
                            <input type="checkbox" name="terms" />
                            <span class="icon"></span>
                            I agree with the ClassMarker.com <a href="#">Terms &nbsp; Conditions</a> and <a href="#">Privacy Policy.</a></label>
                        </div>
                        <div class="submit-row">
                        	<input type="submit" class="colorbtn bluebtn" value="Register Now">
                        </div>
                    </form>
                </div>
                <div class="register-right">
                	<div class="others-btn">
                    	<a href="javascript:void(0)" class="fb-btn" onclick="myFacebookLogin();"><i class="fa fa-facebook" aria-hidden="true"></i>Sign Up with Facebook</a>

                        <a href="javascript:void(0)" class="google-btn " onclick="gplusLogin()"><i class="fa fa-google-plus" aria-hidden="true"></i>Sign Up with Google</a>
						
						<a href="javascript:void(0)" class="fb-btn" id="login_button" onclick="VK.Auth.login(authInfo);">Sign Up with Vk.com</a>
                    </div>
                    <h3>Account type: Business</h3>
                    <ul>
                    	<li>Free 30 day trial</li>
                        <li>No credit card required</li>
                        <li>Register in under 1 minute</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--main content close-->
    
        
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo e(config('constants.GM_API_KEY')); ?>&libraries=places"></script>
<script type="text/javascript">

function initialize() {
	var input = document.getElementById('address');
	
	var options = {
		//componentRestrictions: {country: 'CA'},		
		//bounds: defaultBounds,
		//types: ['establishment']
	};
	var autocomplete = new google.maps.places.Autocomplete(input,options);

	// Get the full place details when the user selects a place from the
	// list of suggestions.
	google.maps.event.addListener(autocomplete, 'place_changed', function() {
		var place = autocomplete.getPlace();
		if (!place.geometry) {
			return;
		}

		if(!place.address_components) {
			return;
		}
		var postal_code;
		place.address_components.forEach(function (item, index) {
			if(item.types[0] === "postal_code")
				postal_code =item.long_name;
		});
		if(postal_code)
			$("#postal_code").val(postal_code);

		if (place.geometry.location) {
			var lat = place.geometry.location.lat();
			var lng = place.geometry.location.lng();
			$("#lat").val(lat);
			$("#lng").val(lng);
		}
	});
	// Stop form submission when enter key pressed.
	google.maps.event.addDomListener(input, 'keydown', function(e) {
		if (e.keyCode == 13) {
			e.preventDefault();
		}
	});
	
	 /* if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			//console.log(position);
		  var pos = {
			lat: position.coords.latitude,
			lng: position.coords.longitude
		  };
			successHandler(position.coords.latitude,position.coords.longitude);
		  
		}, function() {
		  handleLocationError(true);
		});
	  } else {
		// Browser doesn't support Geolocation
		handleLocationError(false);
	  } */
}
 google.maps.event.addDomListener(window, 'load', initialize);
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>