<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */
$resource = 'invoice';
$resource_pl = str_plural($resource);
$page_title = ucfirst($resource);
// Resolute the data
$data = $user;

?>



<?php $__env->startSection('pageTitle', $page_title); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $page_title; ?>

            <small>Details <?php echo $data->firstname; ?></small>
			
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="<?php echo url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="<?php echo url('admin/user/'.$user->id.'/'.$resource_pl); ?>"><i class="fa  fa-user"></i> <?php echo ucfirst($resource_pl); ?></a></li>
            <li class="active">Details</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!--<div class="box box-info">
					<div class="box-body">
						<div class="form-group">
							<label for="business_name" class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-6">
							</div>
						</div>
						
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="business_name" class="col-sm-2 control-label">Email</label>
							<div class="col-sm-6">
							<?php echo e($user->email); ?>

							</div>
						</div>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="business_name" class="col-sm-2 control-label">Address</label>
							<div class="col-sm-6">
							<?php echo e($user->address); ?>

							</div>
						</div>
					</div>
					
				</div>-->
                <div class="box box-info">
					<div class="invoicePage">
						<a title="print" href="javascript:invoicePrint();" class="printpdf"><img src="<?php echo asset('assets/frontend/images/print.png'); ?>" alt=""/></a>
					</div>
					<table id="printInvoice" style="width:100%;">
					<tr>
					<td>
						<div class="printinvoice">
							<style>			
							@import  url(https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i);
							*{
							   font-family: 'Lato', sans-serif;
							}
							.invoicelogo {
								text-align: center;
								margin-bottom:35px;
							}
							.invoicelogo img {
								margin: 0 auto;
							}
							.invoicebox {
								width: 40%;
								float: left;
								font-size: 18px;
								line-height: 1.4;
								padding: 10px;
								border: 1px solid #ccc;
								border-radius: 7px;
							}
							.invoicebox:last-child{
								float: right;
							}
							.invoicebox h2 {
								font-size: 20px;
								font-weight: 600;
								line-height: 1;
								color: #965ace;
								margin: 0 0 13px;
							}
							.invoicebox p span{
								font-weight:400;
								float:left;
							}
							.invoicebox p em{
								display:block;
								padding-left:78px;
							}
							.invoiceDetails {
								padding-bottom: 40px;
							}
							.tableBox{
							}
							.tableBox th{
								text-align:center;
								color: #965ace;
								font-size: 20px;
								vertical-align:middle;
								background:#fafafa;
								padding:10px 10px;
								border-top:1px solid #ccc;
								border-bottom:1px solid #ccc;
							}
							.tableBox td{
								text-align:center;
								font-size: 18px;
								vertical-align:middle;
								padding:10px 10px;
								border-bottom:1px solid #ccc;
							}
							.invoicePage{
								position:relative;
							}
							.printpdf{
								position: absolute;
								right: 0;
								top:0px;
							}
							table.cstable p.ttl{
								display:inline-block;
							}
							table.cstable .assigndeta{
								width:inherit;
								padding:0 10px;
							}

							</style>
							<div class="invoicelogo">
								<img src="<?php echo asset('assets/frontend/images/logo.png'); ?>" alt=""/>
							</div>
							<div class="invoiceDetails clear">
								<div class="invoicebox">
									<h2>To</h2>
									<p><span>Name:</span><em><?php echo e($user->first_name.' '.$user->last_name); ?></em></p>
									<p><span>Address:</span><em><?php echo e($user->address); ?></em></p>
								</div>
								<div class="invoicebox">
									<h2>From</h2>
									<p><span>Name:</span><em><?php echo e($setting->contact_name); ?></em></p>
									<!--<p><span>Address:</span><em>Plot No. E2-4, Block-GP
									Sector-V, Salt Lake City
									Kolkata-700 091</em></p>-->
								</div>
							</div>
							<table width="100%">
							<tr><td height="10"></td></tr>
							</table>
							<div class="tableBox">
								<table  style="width:100%;">
									<tr>
										<th>ID</th>
										<th>Package Name</th>
										<th>Package Price</th>
										<th>Date of Subscription</th>
										<th>End Date</th>
									</tr>
									<tr>
										<?php
										if(!empty($invoice))
										{
											$startd = strtotime($invoice->pivot->created_at);
											$exp = ($invoice->duration * (30)*86400);
											$expd = (int)$startd + $exp;
										}
										?>
										<td>#<?php echo e($invoice->pivot->id); ?></td>
										<td><?php echo e($invoice->plan); ?></td>
										<td>$<?php echo e($invoice->price); ?></td>
										<td><?php echo e(date('d/m/Y',$startd)); ?></td>
										<td><?php echo e(date('d/m/Y',$expd)); ?></td>
									</tr>
								</table>
							</div>
						</div>				
					</td>
					</tr>
					</table>	
				 
                </div>				
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
<script>
function invoicePrint()
{
	//window.print();
	//$("#printInvoice").printElement();
	w=window.open();
	w.document.write($('#printInvoice').html());
	w.print();
	w.close();
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>