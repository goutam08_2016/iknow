<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

   <header class="testheader">
    	<div class="wrapper">
             <!--<img src="<?php echo asset('assets/frontend/images/logo.png'); ?>" alt="">-->
        </div>
    </header>
    <!--testheader close-->
    
    <!--main content open-->
    <section class="testquizbody">
    	<div class="testpaper-wrap">
        	<h2>Demo Test</h2>
            <div class="test_container-intro">
            	<h3>Instructions:</h3>
                <ul>
                    <li>Number of questions: <strong>2</strong></li>
                    <li>Has no time limit</li>
                    <li>Must be finished in one sitting. You cannot save and finish later.</li>
                    <li>Questions displayed per page: <strong>1</strong></li>
                    <li>Will allow you to go back and change your answers.</li>
                    <li>Will not let you finish with any questions unattempted.</li>
                </ul>
                <div class="testbtnrow">
                    <a class="colorbtn" href="javascript:void(0)">Continue</a>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>