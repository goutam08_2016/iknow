<?php $__env->startSection('pageTitle', 'Welcome to '); ?>



<?php $__env->startSection('customStyle'); ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>

	<!--body open-->
    <section class="mainbody clear">
    	<!--left pan open-->
    	<?php echo $__env->make('include.left_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!--left pan close-->
        
        <!--middle open-->
        <div class="middlecol">
            <div class="pTab">
				<?php echo $__env->make('include.setting_tabs', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="tabContWrap settingcont">
                    <div class="settingbox">
                        <!--<h3>Account</h3>-->
                        <div class="settingrow clear">
                            <span class="boxlabel">Primary Email</span>
                            <div class="boxtext editAbout">
                                <span class="boxdeta"><?php echo e($user->email); ?></span>								
                                <span class="boxdeta saveDeta" id="recmail"><?php echo e($user->recovery_email); ?></span>
                                <a href="javascript:void(0)" class="settinglink saveDeta editSingle"><?php echo e($user->recovery_email!=''?'Edit Altenative':'Add Another'); ?> Email Address</a>
								<div class="editmain">
									<div class="noclass clear">
                                    <div class="detaRow smallText clear">
									<form class="userdt" name="" method="post" action="">
										<input type="email" required class="" name="recovery_email" placeholder="Enter Another Email" value="<?php echo e($user->recovery_email); ?>"/><input type="submit" value="Done" class="bluebtn"/>
									</form>
                                    </div>
									<a href="javascript:void(0);" class="bluebtn lightbtn cancelEdit">Cancel</a>
									</div>
								</div>
                            </div>
                        </div>
                        <div class="settingrow editAbout clear">
                            <span class="boxlabel clear">Password</span>
                            <div class="boxtext saveDeta editSingle">
                                <a href="javascript:void(0);" class="settinglink">Change Password </a>
                            </div>
							
							<div class="editmain">
								<div class="noclass clear changepass">
									<form class="userdt" id="passfrm" name="" method="post" action="">
										<p><input type="password" required class="" name="old_password" placeholder="Enter Current password" value=""/></p>
										<p><input type="password" required class="" name="password" placeholder="Enter password" value=""/></p>
										<p><input type="password" required class="" name="password_confirmation" placeholder="Retype new password" value=""/></p>
										<p><input type="submit" value="Done" class="bluebtn"/></p>
									</form>
                                    <a href="javascript:clearMsg();" class="bluebtn lightbtn cancelEdit">Cancel</a>
								</div>
							</div>
							<small id="passmsg">&nbsp;</small>
                        </div>
                        <div class="settingrow clear">
                            <span class="boxlabel">Logout</span>
                            <div class="boxtext">
                                <a href="#" class="settinglink">Logout of all other browsers</a>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="settingbox">
                        <h3>Profile</h3>
                        <div class="settingrow clear">
                            <span class="boxlabel">Gmail</span>
                            <div class="boxtext">
                                <span class="boxdeta">tianisp11690@gmail.com</span>
                                <a href="#" class="settinglink">Add Another Email Address</a>
                            </div>
                        </div>
                        <div class="settingrow clear">
                            <span class="boxlabel">Facebook</span>
                            <div class="boxtext">
                                <span class="boxdeta">Connected as: <a href="">P.h. Tien</a></span>
                                <p>Find <a href="">Facebook Friends</a> on <strong>iKnow</strong></p>
                                <p><strong>iKnow</strong> enabled on your Facebook timeline: <a href="">Yes</a> | <a href="">No</a></p>
                            </div>
                        </div>
                        <div class="settingrow clear">
                            <span class="boxlabel">LinkedIn</span>
                            <div class="boxtext">
                                <a href="#" class="linkdnbtn bluebtn">Connect LinkedIn Account</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="settingbox">
                        <h3>Manage Filter</h3>
                        <ul class="sortable">
                            <li class="ui-state-default">Location<img src="assets/images/location.png"></li>
                            <li class="ui-state-default">Working/Studying Place<img src="assets/images/place.png"></li>
                            <li class="ui-state-default">Profession<img src="assets/images/setting.png"></li>
                            <li class="ui-state-default">Topic<img src="assets/images/brain.png"></li>
                            <li class="ui-state-default">Gender<img src="assets/images/gender.png"></li>
                            <li class="ui-state-default">Major<img src="assets/images/book.png"></li>
                        </ul>
                    </div>
                    -->
                </div>
            </div>
        </div>
        <!--middle close-->
        
        <!--right pan open-->
        <?php echo $__env->make('include.right_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!--right pan close-->
    </section>
    <!--body close-->

<?php $__env->stopSection(); ?>



<?php $__env->startSection('customScript'); ?>

<script>
$( "form.userdt" ).on( "submit", function( event ) {
//
  event.preventDefault();
  var formdata =  $( this ).serialize();
  //console.log( $( this ).serialize() );
  updateUsers(formdata);
});

function updateUsers(formdata)
{
	$("#passmsg").text('');
	$.ajax({
		type:"post",
		url: "<?php echo url('user/update-userdata'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: formdata,			
		//dataType: "json",			
		error:function(erres) {	
			console.log(erres.responseText); 
			var reponse = JSON.parse(erres.responseText);
			if(reponse)
			{
				if(reponse['old_password']!='' && reponse['old_password']=='validation.pwdvalidation')
				{
					$("#passmsg").css('color','#ff0000').text("Enter current password properly.");
				}
				else if(reponse['password']!='')
				{
					$("#passmsg").css('color','#ff0000').text(reponse['password']);	
				}
				$( "form#passfrm" ).reset();
			}
		},
		success:function(res) {
			console.log(res); 
			if(res.change_pass!='')
			{
			$("#passmsg").css('color','#1a8414').text("Password successfully updated!");				
			}
			$("#recmail").html(res.recovery_email);
			$("#recmail").show();
			$("#recmail").next('a').html('Edit Altenative Email Address');
			$('.cancelEdit').trigger( "click" );
			$('.nCan').trigger( "click" );
			$( "form#passfrm" ).reset();
		}

	});
}

function clearMsg(){
	$('#passmsg').html('');
}
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>