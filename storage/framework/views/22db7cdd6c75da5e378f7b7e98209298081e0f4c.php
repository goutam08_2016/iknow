<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <!--body open-->
    <section class="pagecontent">
       <div class="container">
       		<div class="signupcont">
            	<h2>Sign Up</h2>			
				
            	<div class="signupwrap">					
					<div class="signupStepsCont">
						<div class="signupSteps">
							<span class="st green"><i class="fa fa-check"></i></span>
							<span class="st st2 green"><i class="fa fa-check"></i></span>
							<span class="st st3">3</span>
							<span class="st st4">4</span>
							<span class="st st5">5</span>
							<div class="stepProgress">
								<div class="current" style="width:25%;"></div>
							</div>
						</div>
					</div>
                    <div class="signupbox slidestep clear">
                    	<div class="signupleft">
							<div class="formrow">
								<span class="titleHead">Work Experience</span>
								<a href="javascript:void(0)" class="addwork addworkexperienceForm"><strong>+</strong> Add Work Experience</a>
								<div class="editBox" id="workexperienceForm">
									<form id="workexperience_form" action="" class="">		
										<input type="hidden" id="wrkexp" name="wrkexp_id" />
										<div class="formLine">
											<label>
												<span class="label">Company: <strong>*</strong></span>
												<div class="textfld">
													<input type="text" id="compny" name="company" value="" required >
												</div>
											</label>
										</div>
										<div class="formLine">
											<label>
												<span class="label">Title: <strong>*</strong></span>
												<div class="textfld">
													<input type="text" id="jobs" name="title" value="" required >
												</div>
											</label>
										</div>
										<div class="formLine">
											<label>
												<span class="label">Department:</span>
												<div class="textfld">
													<input type="text" id="department" name="department" value="">
												</div>
											</label>
										</div>
										<div class="formLine">
											<label>
												<span class="label">Location: <strong>*</strong></span>
												<div class="textfld smlFld">
													<input type="text" id="wcity" name="city" value="" required >
												</div>
											</label>
										</div>
										<div class="formLine">
											<span class="label">Time Period:</span>
											<div class="textfld clear">
												<span class="dstext fText">From</span>
												<div class="strtdate clear">
													<div class="mnth">
														<select id="wfrom_month" name="from_month">
															<option value="">Month</option>
															<?php for( $i=1; $i<=12; $i++ ): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
													<div class="yer">
														<select id="wfrom_year" name="from_year" onChange="validateYear('#wfrom_year','#wto_year');" required >
															<option value="">Year <strong>*</strong></option>
															<?php for( $i=1970; $i<=2016; $i++ ): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
												</div>
												<span class="dstext">To</span>
												<div class="endDate clear">
													<div class="mnth">
														<select id="wto_month" name="to_month">
															<option value="">Month</option>
															<?php for( $i=1; $i<=12; $i++ ): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
													<div class="yer">
														<select id="wto_year" name="to_year" onChange="validateYear('#wfrom_year','#wto_year');">
															<option value="<?php echo date('Y'); ?>">Now</option>
															<?php for( $i=1970; $i<=2016; $i++ ): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="textfld btnRow">
											<a href="javascript:void(0)" class="cnclBtn">Cancel</a>
											<input id="addWorkExp" type="submit" value="Save" class="bluebtn">
										</div>
									</form>	
								</div>
							   
							   <div id="workexperienceslider">                 
									<div class="formslide" >                                    	
									<?php if(!empty($user->work_experiences)): ?>
										<?php foreach($user->work_experiences as $ir=>$val): ?>
											<?php if($ir % 2 == 0): ?>
											<?php endif; ?>
										<div class="slidecont">
											
												<div class="sliderow clear">
													<div class="actionBox">
														<a href="javascript:void(0);" class="editTxt">
															<img src="<?php echo asset('assets/frontend'); ?>/images/edit.png" alt="">
															<input type="hidden" value="<?php echo e($val->id); ?>" />
														</a>
														<a href="javascript:void(0);" class="delTxt">
															<img src="<?php echo asset('assets/frontend'); ?>/images/cancel.png" alt="">
															<input type="hidden" value="<?php echo e($val->id); ?>" />
														</a>
													</div>
													<div class="textDeta">
														<h3><?php echo e($val->company); ?></h3>
														<h4><?php echo e($val->title); ?> <?php echo e($val->title!='' && $val->department!='' ?'-':''); ?> <?php echo e($val->department); ?></h4>
														<p>
															<i class="fa fa-map-marker" aria-hidden="true"></i>
															<span><?php echo e($val->city); ?></span>
														</p>
														<p class="dt">
														<?php $dt = DateTime::createFromFormat('!m', $val->from_month);
															echo $val->from_month !=0 ?$dt->format('F') :'';?> <?php echo e($val->from_year); ?> - 
														<?php $dt = DateTime::createFromFormat('!m', $val->to_month);
															echo $val->to_month>0 ?$dt->format('F'):'';?> <?php echo e($val->to_year); ?>

														</p>
														
														<input type="hidden" class="ttl" value="<?php echo e($val->title); ?>"/>
														<input type="hidden" class="dpt" value="<?php echo e($val->department); ?>"/>
														<input type="hidden" class="cty" value="<?php echo e($val->city); ?>"/>
														<input type="hidden" class="cntry" value="<?php echo e($val->country); ?>"/>
														<input type="hidden" class="frmmnt" value="<?php echo e($val->from_month); ?>"/>
														<input type="hidden" class="frmyr" value="<?php echo e($val->from_year); ?>"/>
														<input type="hidden" class="tomnt" value="<?php echo e($val->to_month); ?>"/>
														<input type="hidden" class="toyr" value="<?php echo e($val->to_year); ?>"/>
													   <!--  <p>January 2013 - Present ( 2 years, 9 months )</p> -->
													</div>
													<div class="textEdit">
														<h3>Techno Exponent</h3>
														
													</div>
												</div>
											<?php if($ir % 2 == 1): ?>	
											<?php endif; ?>
										</div>
											
										<?php endforeach; ?>
									<?php endif; ?>
									</div>
							   </div> 
							</div>
                            
						
							<div class="formrow">
								<span class="titleHead">Education</span>
								<a href="javascript:void(0)" class="addwork addeducationalForm"><strong>+</strong> Add Education</a>
								<div class="editBox" id="educationalForm">
									<form id="education_form" action="" class="">	
										<input type="hidden" id="educatn" name="edu_id" />
										<div class="formLine">
											<label>
												<span class="label">University: <strong>*</strong></span>
												<div class="textfld">
													<input type="text" id="institution" name="institution" value="" required >
												</div>
											</label>
										</div>
										<div class="formLine">
											<label>
												<span class="label">School Major: <strong>*</strong></span>
												<div class="textfld">
													<input type="text" id="major" name="major" value="" required />
												</div>
											</label>
										</div>
										<div class="formLine">
											<label>
												<span class="label">Degree:</span>
												<div class="textfld smlFld">
													<select id="degree" name="degree">
														<option value="">Select</option>
														<?php if(!empty($degrees)): ?>
														<?php foreach($degrees as $dgr): ?>
														<option value="<?php echo e($dgr->id); ?>"><?php echo e($dgr->title); ?></option>
														<?php endforeach; ?>
														<?php endif; ?>
													</select>
												</div>
											</label>
										</div>
										<div class="formLine">
											<label>
												<span class="label">Location: <strong>*</strong></span>
												<div class="textfld smlFld">
													<input type="text" id="ecity" name="city" value="" required >
												</div>
											</label>
										</div>
												
										<div class="formLine">
											<span class="label">Time Period:</span>
											<div class="textfld clear">
												<span class="dstext fText">From</span>
												<div class="strtdate clear">
													<div class="mnth">
														<select id="efrom_month" name="from_month">
															<option value="">Month</option>
															<?php for( $i=1; $i<=12; $i++ ): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
													<div class="yer">
														<select id="efrom_year" name="from_year" required  onChange="validateYear('#efrom_year','#eto_year');">
															<option value="">Year <strong>*</strong></option>
															<?php for( $i=1970; $i<=2016; $i++ ): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
												</div>
												<span class="dstext">To</span>
												<div class="endDate clear">
													<div class="mnth">
														<select id="eto_month" name="to_month">
															<option value="">Month</option>
															<?php for( $i=1; $i<=12; $i++ ): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
													<div class="yer">
														<select id="eto_year" name="to_year" onChange="validateYear('#efrom_year','#eto_year');">
															<option value="<?php echo date('Y'); ?>">Now</option>
															<?php for( $i=1970; $i<=2016; $i++ ): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
												</div>
											</div>
										</div>
									
										<div class="textfld btnRow">
											<a href="javascript:void(0)" class="cnclBtn">Cancel</a>
											<input type="submit" value="Save" class="bluebtn"/>
										</div>
									</form>
								</div>
								
								<div id="educationalslider">  
									<div class="formslide">									
									<?php if(!empty($user->educations)): ?>
										<?php foreach($user->educations as $ir=>$val): ?>
											<?php if($ir % 2 == 0): ?>
											<?php endif; ?>
										<div class="slidecont">
											
											<div class="sliderow clear">
												<div class="actionBox">
													<a href="javascript:void(0);" class="editTxt">
													<img src="<?php echo asset('assets/frontend'); ?>/images/edit.png" alt="">
													<input type="hidden" value="<?php echo e($val->id); ?>" />
													</a>
													<a href="javascript:void(0);" class="delTxt">
													<img src="<?php echo asset('assets/frontend'); ?>/images/cancel.png" alt="">
													<input type="hidden" value="<?php echo e($val->id); ?>" />
													</a>
												</div>
												<div class="textDeta">
													<h3><?php echo $val->institution; ?></h3>
													<h4><?php echo e($val->major); ?> <?php echo e($val->major!='' && !empty($val->edu_degree) ?'-':''); ?> <?php echo e(!empty($val->edu_degree)?$val->edu_degree->title:''); ?></h4>
													<p><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo e($val->city); ?></p>
													<p><?php $dt = DateTime::createFromFormat('!m', $val->from_month);
															echo $val->from_month !=0 ?$dt->format('F') :'';?> <?php echo e($val->from_year); ?> - 
														<?php $dt = DateTime::createFromFormat('!m', $val->to_month);
															echo $val->to_month>0 ?$dt->format('F'):'';?> <?php echo e($val->to_year); ?>

													</p>
													
														<input type="hidden" class="dgr" value="<?php echo e($val->degree); ?>"/>
														<input type="hidden" class="majr" value="<?php echo e($val->major); ?>"/>
														<input type="hidden" class="cty" value="<?php echo e($val->city); ?>"/>
														<input type="hidden" class="cntry" value="<?php echo e($val->country); ?>"/>
														<input type="hidden" class="frmmnt" value="<?php echo e($val->from_month); ?>"/>
														<input type="hidden" class="frmyr" value="<?php echo e($val->from_year); ?>"/>
														<input type="hidden" class="tomnt" value="<?php echo e($val->to_month); ?>"/>
														<input type="hidden" class="toyr" value="<?php echo e($val->to_year); ?>"/>
												</div>
												<div class="textEdit">
													<h3>KGEC</h3>
													
												</div>
											</div>
											<?php if($ir % 2 == 1): ?>
											<?php endif; ?>
										</div>
											
										<?php endforeach; ?>		
									<?php endif; ?>	
									</div>
								</div>	
							</div>
                            
							<div class="buttonbox clear">						
								<form name="signup" method="post" action="">
									<?php echo e(csrf_field()); ?>							
									<a href="<?php echo e(url('user/signup-step/2')); ?>" class="prevstep assbtn">prev</a>
									<input type="submit" value="next" class="nextstep bluebtn">
									
									<input type="hidden" name="in_step" value="<?php echo e($step); ?>" />
								</form>
							</div>
						</div>
                        
                        <div class="signupthumb clear">
                        	<img src="<?php echo asset('assets/frontend'); ?>/images/signup-charecter.png" width="146" height="459" alt="">
                        </div>
                        </div>
						
                    </div><!-- /signupwrap -->
                </div>
                
            </div>
       </div>
    </section>
    <!--body close-->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>

$( "form#workexperience_form" ).on( "submit", function( event ) {
//
  event.preventDefault();
  var formdata =  $( this ).serialize();
  //console.log( $( this ).serialize() );
  if (isSafari){
	  var frmObj = $(this);
		var flag = 0;
		frmObj.find('input[type=text],select').each(function(index,elem) {
			console.log(elem);
			if($(elem).attr("required"))
			{
				if($(elem).val()=='')
				{
					//alert($(elem).attr("name"));
					flag = 1;
					alert("Please fill out this field");
					$(elem).focus();
					return false;
				}
			}
		});
		
		if(flag==0){
			$.ajax({
				type:"post",
				url: "<?php echo url('user/add-workexp'); ?>" ,
				headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
				data: formdata,			
				//dataType: "json",			
				success:function(res) {
					console.log(res);
					/* $('#'+fid).val('');
					var lhtml = '<li><span class="addbox">'+addrval+'<a href="javascript:void(0)" class="closebox"><img src="'+'<?php echo asset('assets/frontend'); ?>'+'/images/close.png" alt=""></a></span></li>';
					$('#'+fid+'_lst').html(lhtml); */
					window.location.reload();
				}
			}); 
		}else{
			event.preventDefault();
		}
  }else{
	
	$.ajax({
		type:"post",
		url: "<?php echo url('user/add-workexp'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: formdata,			
		//dataType: "json",			
		success:function(res) {
			console.log(res);
			/* $('#'+fid).val('');
			var lhtml = '<li><span class="addbox">'+addrval+'<a href="javascript:void(0)" class="closebox"><img src="'+'<?php echo asset('assets/frontend'); ?>'+'/images/close.png" alt=""></a></span></li>';
			$('#'+fid+'_lst').html(lhtml); */
			if(!isSafari)	window.location.reload();
		}
	});  
  }
});

$( "form#education_form" ).on( "submit", function( event ) {
//
  event.preventDefault();
  var formdata =  $( this ).serialize();
  //console.log( $( this ).serialize() );
  if (isSafari){
	  var frmObj = $(this);
		var flag = 0;
		frmObj.find('input[type=text],select').each(function(index,elem) {
			console.log(elem);
			if($(elem).attr("required"))
			{
				if($(elem).val()=='')
				{
					//alert($(elem).attr("name"));
					flag = 1;
					alert("Please fill out this field");
					$(elem).focus();
					return false;
				}
			}
		});
		
		if(flag==0){
			$.ajax({
				type:"post",
				url: "<?php echo url('user/add-education'); ?>" ,
				headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
				data: formdata,			
				//dataType: "json",			
				success:function(res) {
					//console.log(res);
					if(!isSafari)	window.location.reload();
				}

			});
		}else{
			event.preventDefault();
		}
  }else{
	$.ajax({
		type:"post",
		url: "<?php echo url('user/add-education'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: formdata,			
		//dataType: "json",			
		success:function(res) {
			//console.log(res);
			if(!isSafari)	window.location.reload();
		}

	});
  }
});

$(document).ready(function() {
   $('.formslide').bxSlider({
	   controls:false,
   });
   
   $('#workexperienceslider .editTxt').click(function() {
		var cobj = $(this);
		//console.log(cobj.parent().parent().children('.textDeta').children('h3').html());
		var company = cobj.parent().parent().children('.textDeta').children('h3').html();
		$("#compny").val(company);
		var jobstitle = cobj.parent().parent().children('.textDeta').children('h4').html();
		$("#jobs").val(cobj.parent().parent().children('.textDeta').children('input.ttl').val());
		$("#wrkexp").val(cobj.children('input').val());
		$("#department").val(cobj.parent().parent().children('.textDeta').children('input.dpt').val());
		$("#wcity").val(cobj.parent().parent().children('.textDeta').children('input.cty').val());
		$("#wcountry").val(cobj.parent().parent().children('.textDeta').children('input.cntry').val());
		$("#wfrom_month").val(cobj.parent().parent().children('.textDeta').children('input.frmmnt').val());
		$("#wfrom_year").val(cobj.parent().parent().children('.textDeta').children('input.frmyr').val());
		$("#wto_month").val(cobj.parent().parent().children('.textDeta').children('input.tomnt').val());
		$("#wto_year").val(cobj.parent().parent().children('.textDeta').children('input.toyr').val());
		//console.log(cobj.parent().parent().children('.textDeta').children('.adr').children('span').html());
		//console.log(cobj.parent().parent().html());
		$('#workexperienceForm').slideDown(200);
	    $('#workexperienceslider').hide();
   });
   
    $('.addworkexperienceForm').click(function() {
	   $('#workexperienceForm').slideDown(200);
	   $('#workexperienceslider').hide();
   });
   
   $('#workexperienceForm .cnclBtn').click(function() {
	   $('#workexperienceForm').slideUp(200);
	    $('#workexperienceslider, .actionBox, .textDeta').show();
   });
   
    $('#educationalslider .editTxt').click(function() {
		var cobj = $(this);
		var institution = cobj.parent().parent().children('.textDeta').children('h3').html();
		$("#institution").val(institution);
		var degree = cobj.parent().parent().children('.textDeta').children('h4').html();
		$("#degree").val(cobj.parent().parent().children('.textDeta').children('input.dgr').val());
		
		$("#educatn").val(cobj.children('input').val());
		$("#major").val(cobj.parent().parent().children('.textDeta').children('input.majr').val());
		$("#ecity").val(cobj.parent().parent().children('.textDeta').children('input.cty').val());
		$("#ecountry").val(cobj.parent().parent().children('.textDeta').children('input.cntry').val());
		$("#efrom_month").val(cobj.parent().parent().children('.textDeta').children('input.frmmnt').val());
		$("#efrom_year").val(cobj.parent().parent().children('.textDeta').children('input.frmyr').val());
		$("#eto_month").val(cobj.parent().parent().children('.textDeta').children('input.tomnt').val());
		$("#eto_year").val(cobj.parent().parent().children('.textDeta').children('input.toyr').val());
		$('#educationalForm').slideDown(200);
	    $('#educationalslider').hide();
   });
   
    $('.addeducationalForm').click(function() {
	   $('#educationalForm').slideDown(200);
	   $('#educationalslider').hide();
   });
   
   $('#educationalForm .cnclBtn').click(function() {
	   $('#educationalForm').slideUp(200);
	    $('#educationalslider, .actionBox, .textDeta').show();
   }); 
   
   
    $('#workexperienceslider .delTxt').click(function() {	
		var dobj = $(this);
		var delid = $(this).children("input").val();
		//alert(delid);
		$.ajax({
			type:"post",
			url: "<?php echo url('user/del-workexp'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {id:delid},			
			//dataType: "json",			
			success:function(res) {
				//console.log(res);
				if(res == 1)
				{
				dobj.parent().parent().remove();
				window.location.reload();
				}
			}

		});
	});
    $('#educationalslider .delTxt').click(function() {
		var dobj = $(this);
		var delid = $(this).children("input").val();
		//alert(delid);
		$.ajax({
			type:"post",
			url: "<?php echo url('user/del-education'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {id:delid},			
			//dataType: "json",			
			success:function(res) {
				//console.log(res);
				if(res == 1)
				{
					dobj.parent().parent().remove();
					window.location.reload();
				}
			}

		});
	});
    
});

$( function() {

    $( "#compny" ).autocomplete({
      //source: availableTags
      source: '<?php echo e(url("user/company-autocomplete")); ?>'
    });

    $( "#jobs" ).autocomplete({
      source: '<?php echo e(url("user/job-autocomplete")); ?>'
    });

    $( "#institution" ).autocomplete({
      source: '<?php echo e(url("user/university-autocomplete")); ?>'
    });

    $( "#major" ).autocomplete({
      source: '<?php echo e(url("user/major-autocomplete")); ?>'
    });
	
		
});
setMapLoc('wcity');
setMapLoc('ecity');
function setMapLoc(id){
	var inputt = document.getElementById(id);
	var autocomplete = new google.maps.places.Autocomplete(inputt);	
}

function validateYear(from_year,to_year)
{
	var fromyear = parseInt($(from_year).val());
	var toyear = parseInt($(to_year).val());
	if(fromyear>toyear)
	{
		$(from_year).val('');
	}
}

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>