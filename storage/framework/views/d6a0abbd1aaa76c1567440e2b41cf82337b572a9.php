<?php
//use App\Helpers\CustomHelper;
?>


<?php $__env->startSection('pageTitle', 'Hotcupido.com | Forgot Password'); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    
<div class="middleadjstcont">    
    
<div class="popupDiv frgtpassform">
  <div class="popupDivHeader"> 
    <span class="popupTtl">Forgot Password</span>
    <div class="clear"></div>
  </div>
  <div class="popupDivBody">
  <?php if($errors->any()): ?>
  <div class="warnings">
      <strong>
          <?php foreach($errors->all() as $error): ?>
          <p><?php echo e($error); ?></p>
          <?php endforeach; ?>
      </strong>
  </div>
  <?php elseif(session('forget_pass_success')): ?>
  <div class="success">
      <strong><?php echo e(session('forget_pass_success')); ?></strong>
  </div>
  <?php endif; ?>
  <?php if(session('error_msg')): ?>
      <div class="warnings">
          <?php echo session('error_msg'); ?>

      </div>
  <?php endif; ?>

    <?php echo csrf_field(); ?>

    <form method="post" action="<?php echo e(url('forget-password')); ?>" name="forgot_pass" data-parsley-validate>
    <?php echo csrf_field(); ?>

      <div class="popupCont inline">
        <div class="logForm">
          <div class="formRow">
              <span class="formTtl">Registered Email <font>*</font></span>
              <span class="formFld">
                <input type="email" name="email" placeholder="Please put the registered email" value="<?php echo e(old('email')); ?>" required>
              </span>
          </div>          
        </div>
        <div class="clear"></div>
      </div>
      <div class="popupFooter">
        <button type="submit" class="full">Send Password Reset Link</button>
      </div>
    </form>
  </div>
</div>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.basic', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>