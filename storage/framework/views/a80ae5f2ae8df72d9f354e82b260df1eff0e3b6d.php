<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	
<?php $user_filter = array();
if(Auth::check() && !empty(Auth::user()->filterset_order))	$user_filter = json_decode(Auth::user()->filterset_order->filter_set);
//$user_filter = json_decode(json_encode($user_filter));
$user_filter = (array) $user_filter; 
//print_r($user_filter);
?>
	<!--body open-->
<?php $menu_arr=[
			'name'=>['Name','nickname','nam','','','','03.png'],
			'locl'=>['Location(live)','location','locationlive','sloction','loction','','01.png'],
			'locv'=>['Location(visit)','location','locationvisit','location_visit','location_visit','','01.png'],
			'workpl'=>['Working/ Studying Place','place','workplace','sjoblocation','joblocation','','04.png'],
			'prof'=>['Profession','setting','profesion','','job','jobs','02.png'],
			'dpt'=>['Department','place','depatment','department','department','','04.png'],
			'tpic'=>['Topic','brain','tpic','srchtropic','tropic','','05.png'],
			'gnder'=>['Gender','gender','gendr','','','','03.png'],
			'eduction'=>['Education','education-icon-blue','eductn','','education','education','education-icon.png'],
			'mjor'=>['Major','book','majr','','major','major','06.png'],
			'ag'=>['Age','age','agee','','','','zz.png'],
			];
			//print_r($menu_arr);
?>
		<div class="pageMiddle">
			<div class="contList searchPeople clear">
				<div class="searchLeft">
					<div class="searchHead clear">
						<h2>Filters</h2>
						<div class="filTer">
							<?php if(Auth::check()): ?>
							<span class="filterIcon"><i class="fa fa-filter" aria-hidden="true"></i></span>
							<?php endif; ?>
							<div class="jilterList">
								<form method="POST" class="toggFilter ordrFrm" >
									<ul id="shortable_area">
										<?php if(!empty($user_filter)): ?>
											<?php foreach($user_filter as $ky=>$val): ?>
											<li class="<?php echo e($ky); ?>">
												<label>
													<input type="checkbox" name="<?php echo e($ky); ?>" value="<?php echo e($val); ?>" checked />
													<span class="checkbox"><img src="<?php echo asset('assets/frontend'); ?>/images/clr.png" alt=""></span>
														<?php echo e($menu_arr[$ky][0]); ?><img src="<?php echo asset('assets/frontend'); ?>/images/<?php echo e($menu_arr[$ky][1]); ?>.png">
												</label>
											</li>
											<?php endforeach; ?>
										<?php endif; ?>
									
										<?php foreach($menu_arr as $ky=>$val): ?>
											<?php if( !in_array($val[2],$user_filter)): ?>
											<li class="<?php echo e($ky); ?>">
												<label>
													<input type="checkbox" name="<?php echo e($ky); ?>" value="<?php echo e($val[2]); ?>" />
													<span class="checkbox"><img src="<?php echo asset('assets/frontend'); ?>/images/clr.png" alt=""></span>
														<?php echo e($val[0]); ?><img src="<?php echo asset('assets/frontend'); ?>/images/<?php echo e($val[1]); ?>.png">
												</label>
											</li>
											<?php endif; ?>
										<?php endforeach; ?>
										<li>
											<input type="submit" value="Done" class="bluebtn filterSave"/>
											<input type="hidden" name="styp" value="2" />
										</li>
									</ul>
								</form>
							</div>
						</div>
					</div>
					
					<div class="peopleForm">
						<form method="post" id="searchFrm2">
							<div class="proHeight">	
								<?php if(!empty($user_filter)): ?>
									<?php foreach($user_filter as $ky=>$val): ?>
									<div class="filterRow <?php echo e($val); ?>">
										<label class="clear">
											<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/<?php echo e($menu_arr[$ky][6]); ?>" alt=""><span><?php echo e($menu_arr[$ky][0]); ?></span></span>
											<div class="textfld clear">
											<?php if($val=='gendr'): ?>
												<select name="gender">
													<option value="">select</option>
													<option value="M">Male</option>
													<option value="F">Female</option>
												</select>
											<?php elseif($val=='agee'): ?>
												<div class="row02">
													<!--<samp>From</samp>-->
													<div class="smallSelect">
														<select name="age_from">
															<option value="">From</option>
															<?php for($i=18;$i<100;$i++): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
												</div>
												<div class="row02">
													<!--<samp>To</samp>-->
													<div class="smallSelect">
														<select name="age_to">
															<option value="">To</option>
															<?php for($i=18;$i<100;$i++): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
												</div>
											<?php else: ?>
												<input type="text" placeholder="<?php echo e($menu_arr[$ky][0]); ?>" class="<?php echo e($menu_arr[$ky][5]); ?>" id="<?php echo e($menu_arr[$ky][3]); ?>" name="<?php echo e($menu_arr[$ky][4]); ?>"/>
												<?php if($val=='tpic'): ?>
												<input type="hidden" value="" id="trpid" name="tropicid"/>
												<?php endif; ?>
											<?php endif; ?>
											</div>
										</label>
									</div>	
									<?php endforeach; ?>
								<?php endif; ?>
								
								<?php foreach($menu_arr as $ky=>$val): ?>
									<?php if( !in_array($val[2],$user_filter)): ?>
									<div class="filterRow <?php echo e($val[2]); ?>" style="display:<?php echo e((Auth::check() && !empty(Auth::user()->filterset_order))?'none':'block'); ?>;">
										<label class="clear">
											<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/<?php echo e($menu_arr[$ky][6]); ?>" alt=""><span><?php echo e($menu_arr[$ky][0]); ?></span></span>
											<div class="textfld clear">
											<?php if($val[2]=='gendr'): ?>
												<select name="gender">
													<option value="">select</option>
													<option value="M">Male</option>
													<option value="F">Female</option>
												</select>
											<?php elseif($val[2]=='agee'): ?>
												<div class="row02">
													<!--<samp>From</samp>-->
													<div class="smallSelect">
														<select name="age_from">
															<option value="">From</option>
															<?php for($i=18;$i<100;$i++): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
												</div>
												<div class="row02">
													<!--<samp>To</samp>-->
													<div class="smallSelect">
														<select name="age_to">
															<option value="">To</option>
															<?php for($i=18;$i<100;$i++): ?>
															<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
															<?php endfor; ?>
														</select>
													</div>
												</div>
											<?php else: ?>
												<input type="text" placeholder="<?php echo e($menu_arr[$ky][0]); ?>" class="<?php echo e($menu_arr[$ky][5]); ?>" id="<?php echo e($menu_arr[$ky][3]); ?>" name="<?php echo e($menu_arr[$ky][4]); ?>"/>
												<?php if($val[2]=='tpic'): ?>
												<input type="hidden" value="" id="t_ids" name="tropicid"/>
												<?php endif; ?>
											<?php endif; ?>
											</div>
										</label>
									</div>	
									<?php endif; ?>
								<?php endforeach; ?>
								<!--<div class="filterRow locationlive">
									<label class="clear">
										<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/01.png" alt=""><span>Location(live)</span></span>
										<div class="textfld clear">
											<input type="text" placeholder="Location" id="sloction" name="loction"/>
										</div>
									</label>
								</div>				
								<div class="filterRow locationvisit">
									<label class="clear">
										<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/01.png" alt=""><span>Location(visit)</span></span>
										<div class="textfld clear">
											<input type="text" placeholder="Location" id="location_visit" name="location_visit"/>
										</div>
									</label>
								</div>
								<div class="filterRow clear workplace">
									<label class="clear">
										<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/04.png" alt=""><span>Working Place</span></span>
										<div class="textfld">
											<input type="text" placeholder="Working/Study/ing Place" id="sjoblocation" name="joblocation"/>
										</div>
									</label>
								</div>
								<div class="filterRow profesion">
									<label class="clear">
										<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/02.png" alt=""><span>Profession</span></span>
										<div class="textfld">
											<input class="jobs" type="text" name="job" placeholder="Profession">
										</div>
									</label>
								</div>
								<div class="filterRow clear depatment">
									<label class="clear">
										<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/04.png" alt=""><span>Department</span></span>
										<div class="textfld">
											<input type="text" placeholder="Department" id="department" name="department"/>
										</div>
									</label>
								</div>
								<div class="filterRow tpic">
									<label class="clear">
										<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/05.png" alt=""><span>Topic</span></span>
										<div class="textfld">
											<input class="" type="text" id="srchtropic" name="tropic" placeholder="Tropic">
											<input type="hidden" value="" id="trpid" name="tropicid"/>
										</div>
									</label>
								</div>
								<div class="filterRow gendr">
									<label class="clear">
										<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/03.png" alt=""><span>Gender</span></span>
										<div class="textfld">
											<select name="gender">
												<option value="">select</option>
												<option value="M">Male</option>
												<option value="F">Female</option>
											</select>
										</div>
									</label>
								</div>
								<div class="filterRow agee">
									<label class="clear">
										<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/zz.png" alt=""><span>age</span></span>
										<div class="textfld">
											<div class="row02">
												
												<div class="smallSelect">
													<select name="age_from">
														<option value="">From</option>
														<?php for($i=18;$i<100;$i++): ?>
														<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
														<?php endfor; ?>
													</select>
												</div>
											</div>
											<div class="row02">
												
												<div class="smallSelect">
													<select name="age_to">
														<option value="">To</option>
														<?php for($i=18;$i<100;$i++): ?>
														<option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
														<?php endfor; ?>
													</select>
												</div>
											</div>
										</div>
									</label>
								</div>
								<div class="filterRow eductn">
									<label class="clear">
										<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/06.png" alt=""><span>Education</span></span>
										<div class="textfld">
											<input class="education" type="text" name="education" placeholder="Education"/>
										</div>
									</label>
								</div>
								<div class="filterRow majr">
									<label class="clear">
										<span class="label"><img src="<?php echo asset('assets/frontend'); ?>/images/06.png" alt=""><span>Major</span></span>
										<div class="textfld">
											<input class="major" type="text" name="major" placeholder="Major"/>
										</div>
									</label>
								</div>-->
							</div>
							<div class="filterBtnRow">
								<input type="reset" value="Reset" class="bluebtn"/>
								<input type="submit" value="Search" class="bluebtn">
								<input type="hidden" value="2" name="list_type"/>
							</div>
						</form>
					</div>
				</div>
					
				<div class="allpeople searchRight clear">
					<h2>Search Results</h2>
					<span class="annce">We found <font id="srch_count">0</font> people matching your description.</span>
					<div class="cleckall">
					<label><span>Select All</span>
						<input id="slctAll2" type="checkbox" name="check"/>
						<span class="checkbox"><img src="<?php echo asset('assets/frontend'); ?>/images/clr.png" alt=""></span>
					</label>
					</div>
					<form id="sendReqstfrm" method="post">
					<div class="searchpeopleWrap">
						<div class="newAll" id="srchusrlist">
							No user found..
						</div>
					</div>
					<?php if(Auth::check()): ?>
					<div class="filterBtnRow">
						<input type="submit" value="Send Friend Request" class="bluebtn">
						<br/><span class="sndmsg">&nbsp;</span>
					</div>
					<?php endif; ?>
					</form>
				</div>
			</div>
		</div>
		
	<style>
	.pac-container{z-index:9999!important;}

	</style>
    <!--body close-->

<?php $__env->stopSection(); ?>



<?php $__env->startSection('customScript'); ?>

<script>
 
$( function() {
	setMapLoc('sloction');
	setMapLoc('location_visit');
});
var aTags1 = [
	<?php if(!empty($all_tropics)): ?>
	  <?php foreach($all_tropics as $jval): ?>
		<?php echo '{"label":"'.$jval->title.'","id":"'.$jval->id.'"} ,'; ?>

	  <?php endforeach; ?>
	<?php endif; ?>
];
$("#srchtropic").autocomplete({
	source: aTags1,
	select: function (event, ui) {
		/* console.log(ui);
		var preVal = $("#t_ids").val();
		if(preVal!='')	preVal+=',';
		preVal+=ui.item.id; */
		$("#trpid").val(ui.item.id);			
	}
});
$( "#sjoblocation" ).autocomplete({
      source: '<?php echo e(url("user/company-autocomplete")); ?>'
});
$( ".education" ).autocomplete({
      source: '<?php echo e(url("user/university-autocomplete")); ?>'
});
 $('#shortable_area').sortable({
	cursor: "move",
	/* start: function(event, ui){
		sort_item_radio = $(ui.item).find('input.is_required[type=radio]:checked').val();
	}, */
	update: function( event, ui ) {
		var sort_item = $(ui.item).find('input[type=checkbox]').val();
		var prev_item = ui.item.prev().find('input[type=checkbox]').val();
		$(".proHeight div."+sort_item).insertAfter( $( ".proHeight div."+prev_item ) );
		$(".proHeight div."+prev_item).next().remove();
	}
});

$( "form#sendReqstfrm" ).on( "submit", function( event ) {
//
  event.preventDefault();
  var formdata =  $( this ).serializeArray();
  //console.log(formdata );
	$('.sndmsg').html('');
	$.ajax({
		type:"post",
		url: "<?php echo url('user/send-friend-request'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: formdata,			
		//dataType: "json",			
		success:function(res) {
			/* console.log(res); */
			if(res==1)	$('.sndmsg').html('Request send successfull');
		}
	});
});

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>