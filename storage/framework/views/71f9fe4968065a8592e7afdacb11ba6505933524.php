<?php
//use App\Helpers\CustomHelper;
//dd($chat_messsage_data['chat_users']->toArray());
$loguser = Auth::User();
?>
<div id="chtMmbrlst" class="chatMmbrList mCustomScrollbar">
<?php if(count($chat_messsage_data['chat_users']) > 0): ?>
    <ul>
    <?php foreach($chat_messsage_data['chat_users'] as $cht_usr): ?>
		
        <?php if($cht_usr->from_user_id == $loguser->id ): ?>
			
            <li <?php echo session('touser_id') == $cht_usr->to_user->id?'class="selectli"':''; ?> id="user_list_<?php echo $cht_usr->to_user->id; ?>">
                <a href="javascript:void(0);" onclick="startChatMessage('<?php echo $cht_usr->to_user->id; ?>');">
                    <span class="user">
                        <?php if($cht_usr->to_user->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$cht_usr->to_user->profile_image)): ?>
                            <img src="<?php echo asset('assets/upload/profile_pictures/'.$cht_usr->to_user->profile_image); ?>" alt="<?php echo $cht_usr->to_user->first_name; ?> <?php echo $cht_usr->to_user->last_name; ?>"/>
                        <?php else: ?>
                            <img src="<?php echo asset('assets/frontend/images/user.jpg'); ?>" alt="<?php echo $cht_usr->to_user->first_name; ?> <?php echo $cht_usr->to_user->last_name; ?>"/>
                        <?php endif; ?>
                    </span>
                    <span class="nameTm">
                        <span class="name">
                            <span class="<?php echo (count($cht_usr->to_user->userLogin) > 0 && $cht_usr->to_user->userLogin->login_status == 1)?'online':'offline'; ?>"></span>
                            <?php echo $cht_usr->to_user->first_name; ?> <?php echo $cht_usr->to_user->last_name; ?>

    						<span class="count" style="display:none;"></span>
                        </span>
						<span class="msgUTxt">
							<i class="fa fa-envelope-o"></i> <?php echo last_received_msg($loguser->id,$cht_usr->to_user_id); ?>

						</span>
                        <?php if(count($cht_usr->toUserMessage)): ?>
                        <p><?php echo $cht_usr->toUserMessage->message; ?></p>
                        <?php endif; ?>
                    </span>
                    <?php if($cht_usr): ?>
                        <span class="lastOnline"><?php echo date('D,M-d',strtotime($cht_usr->created_at)); ?></span>
                    <?php endif; ?>
                </a>
            </li>
        <?php elseif($cht_usr->to_user_id == $loguser->id ): ?>
			
            <li <?php echo session('touser_id') == $cht_usr->form_user->id?'class="selectli"':''; ?> id="user_list_<?php echo $cht_usr->form_user->id; ?>">
                <a href="javascript:void(0);" onclick="startChatMessage('<?php echo $cht_usr->form_user->id; ?>');">
                    <span class="user">
                        <?php if($cht_usr->form_user->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$cht_usr->form_user->profile_image)): ?>
                            <img src="<?php echo asset('assets/upload/profile_pictures/'.$cht_usr->form_user->profile_image); ?>" alt="<?php echo $cht_usr->form_user->first_name; ?>  <?php echo $cht_usr->form_user->last_name; ?>"/>
                        <?php else: ?>
                            <img src="<?php echo asset('assets/frontend/images/user.jpg'); ?>" alt="<?php echo $cht_usr->form_user->first_name; ?> <?php echo $cht_usr->form_user->last_name; ?>"/>
                        <?php endif; ?>
                    </span>
                    <span class="nameTm">
                        <span class="name">
                            <span class="<?php echo (count($cht_usr->form_user->userLogin) > 0 && $cht_usr->form_user->userLogin->login_status == 1)?'online':'offline'; ?>"></span>
                            <?php echo $cht_usr->form_user->first_name; ?> <?php echo $cht_usr->form_user->last_name; ?>

    						<span class="count" style="display:none;"></span>
                        </span>
						<span class="msgUTxt">
							<i class="fa fa-envelope-o"></i> <?php echo last_received_msg($loguser->id,$cht_usr->from_user_id); ?>

						</span>
                        <?php if(count($cht_usr->toUserMessage)): ?>
                        <p><?php echo $cht_usr->toUserMessage->message; ?></p>
                        <?php endif; ?>
                    </span>
                    <?php if($cht_usr): ?>
                        <span class="lastOnline"><?php echo date('D,M-d',strtotime($cht_usr->created_at)); ?></span>
                    <?php endif; ?>
                </a>
            </li>
         <?php endif; ?>
    <?php endforeach; ?>
    </ul>
<?php else: ?>
<ul><li> cc msg </li></ul>
<?php endif; ?>      
</div>