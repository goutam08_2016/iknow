<?php $__env->startSection('pageTitle', 'Dashboard'); ?>

<?php $__env->startSection('customStyles'); ?>
    <?php /* You can write styles or other scripts here, it will be added to <head> */ ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        Welcome to admin panel

    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
    <?php /* You can write scripts here, it will be added before </body> */ ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>