<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<link href="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.css'); ?>" rel="stylesheet" type="text/css">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	<?php echo $client_header; ?>    	 

    <!--main content open-->

    <section class="testpage">	
		
    	<div class="deta-search"> &nbsp; </div>
    	<div class="test-deta">
			<div class="wrapper">
                <div class="questionbox">
					<?php if($errors->any()): ?>
						<div class="warnings">
							<div class="error_message">
								<?php foreach($errors->all() as $error): ?>
									<p><?php echo $error; ?></p>
								<?php endforeach; ?>
							</div>
						</div>
					<?php endif; ?>	
                	<form name="question" method="post" action="" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>

                        
						<div class="newQs">
							<h3>Question</h3>
                            <div id="myList">
                            	<div class="questionrow clear">
                                    <div class="questionedit">
                                        <textarea class="ckeditor" name="question" ><?php echo e(!empty($question)?$question->question:''); ?></textarea>
                                    </div>
                                    <div class="hoz-row clear">
                                        <div class="questionrow2">
                                            <h3>Range</h3>
                                            <div class="rangefield clear">
                                                <div class="rangesmall">
                                                    <input type="number" min="0" name="min_range" value="<?php echo e(!empty($question->answers)?$question->answers[0]->min_range:''); ?>" placeholder="From" />
                                                </div>
                                                <div class="rangesmall">
                                                    <input type="number" name="max_range" min="0"  value="<?php echo e(!empty($question->answers)?$question->answers[0]->max_range:''); ?>" placeholder="To" />
                                                </div>
                                            </div>
                                        </div>										
										<div class="questionrow2 nextrow">
											<h3>Minimum Point</h3>
											<div class="selectcategory">
												<input type="number" name="min_point" min="0" value="<?php echo e(!empty($question->answers)?$question->answers[0]->min_point:''); ?>">
											</div>
										</div>
                                    </div>
                                </div>
								
                            </div>
                            
                        </div>
                        <!--<div class="questionrow">
                        	<a id="loadMore" class="colorbtn" href="javascript:void(0)">Add more options</a>
                        </div>-->
						<div class="questionrow">
							<div class="hoz-row clear">
								<div class="questionrow clear">
									<h3>Next Opened Question</h3>
								</div>	
							</div>
							<div class="questionedit">
								<textarea class="ckeditor" name="next_open_qsn" ><?php echo e(!empty($question)?$question->next_open_qsn:''); ?></textarea>
							</div>							
						</div>
                        <div class="questionrow nextrow">
                            <h3>Category</h3>
                            <div class="selectcategory">
                            	<select name="category">
									<option value="">Select Category</option>
									<?php if(!empty($categories)): ?>
										<?php foreach($categories as $val): ?>
										<option value="<?php echo e($val->id); ?>" <?php echo e(!empty($question) && $question->category == $val->id ?'selected':''); ?>  > <?php echo e($val->title); ?> </option>
										<?php endforeach; ?>
									<?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="questionrow nextrow">
                            <h3>Test</h3>
                            <div class="selectcategory">
                            	<select name="test" >
									<option value="">Select Test</option>
									<?php if(!empty($user->tests)): ?>
										<?php foreach($user->tests as $val): ?>
										<option value="<?php echo e($val->id); ?>" <?php echo e(!empty($question) && !empty($question->tests[0]) && $question->tests[0]->pivot->test_id == $val->id ?'selected':''); ?> > <?php echo e($val->title); ?> </option>
										<?php endforeach; ?>
									<?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <!--<div class="questionrow nextrow">
                            <h3>Points Available</h3>
                            <div class="pointbox" >
                            	<input type="number" name="point" min="0" value="<?php echo e(!empty($question)?$question->point:'1'); ?>">
                            </div>
                        </div>-->
                        <div class="buttonbox clear">
                            <input type="hidden" name="question_type" value="<?php echo e($qsn_type); ?>">
							<?php if(!empty($question)): ?>
                            <input type="hidden" name="question_id" value="<?php echo e($question->id); ?>">
							<?php endif; ?>
                            <input type="submit" value="Save">
                            <input type="reset" value="cancel">
                        </div>
                    </form>
                </div>
            </div>
        </div>
   
	</section>

    <!--main content close-->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script src="<?php echo asset('assets/frontend/js/stacktable.min.js'); ?>"></script>
<script src="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.js'); ?>"></script>
<script src="<?php echo asset('assets/frontend/ckeditor/ckeditor.js'); ?>"></script>
<script>
$(document).ready(function() {
	//CKEDITOR.replaceClass = 'texteditor';
});
$(document).ready(function() {
    $('.qstable').cardtable();
	
	$('.qspopup').fancybox({
		padding:0
	});
	
	var REMOVE = '';
    var i=<?php echo !empty($question->sub_questions) && count($question->sub_questions)>0 ? count($question->sub_questions):1; ?>;
	var t = $('#myList .questionrow.clear').length+1;
	var k = t;
	$('#loadMore').click(function () {
		var oneplus = i+1;
		var tr_object = $('#myList').find('.questionrow:first-child').clone();
		
		$(tr_object).find('textarea[name="question_option[]"]').attr("id", "question_option_"+oneplus+"");
		if($('#myList .questionrow').length < 9) { 
			$(tr_object).appendTo('#myList');
		}
		
		if($('#myList .questionrow').length < 9){
		}else{
			$('#loadMore').addClass('noclick');
		}
		CKEDITOR.replace("question_option_"+oneplus+"");
		$('#cke_question_option_1').each(function() {
			var $ids = $('[id=' + this.id + ']');
			if ($ids.length > 1) {
				$ids.not(':first').remove();
			}
		});
		i=i+1;
		oneplus++;
	});	
	
});

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>