<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    
    <!--main content open-->  
	<section class="testquizbody">
    	<div class="testpaper-wrap">
        	<h2>Demo Test</h2>
            <div class="test-user"><i class="fa fa-user" aria-hidden="true"></i>Student Name</div>
            <div class="test_container">
                <div class="testpage-no">
                	<h4>Question 1 of 2</h4>
                </div>
            	<form name="test" method="post" action="#">
                <div class="testquestion">
                	<p>Easy Way</p>
                </div>
                <div class="question-answer">
                    <div class="formrow">
                        <label>
                        	<input type="radio" name="answer">
                            <span class="label"><strong>1</strong>Yes</span>
                        </label>
                    </div>
                    <div class="formrow">
                        <label>
                        	<input type="radio" name="answer">
                            <span class="label"><strong>2</strong>No</span>
                        </label>
                    </div>
                    <div class="testbtnrow clear">
                        <!--<a class="colorbtn prevbtn" href="#"><i class="fa fa-caret-left" aria-hidden="true"></i>Prev</a>--><!--this for previous page link-->
                        <a class="colorbtn nextbtn" href="#">Next<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    </div>
                    <!--<div class="testbtnrow papersubmit">
                    	<input type="submit" value="Finish Now" class="colorbtn bluebtn">
                    </div>--><!--- this section for answer submit last page-->
                </div>
                </form>
            </div>
        </div>
    </section>
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.basic', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>