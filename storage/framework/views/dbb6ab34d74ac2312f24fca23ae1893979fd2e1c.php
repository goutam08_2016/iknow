<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	<?php echo $client_header; ?>

    <!--main content open-->
    <section class="testpage">
    	<div class="deta-search">
        	<!--<div class="wrapper clear">
            	<div class="search-box groupsearch clear">
                	<form name="search" method="get" action="">
                        <div class="formcol">
                            <input type="text" name="grp" placeholder="Search Group Name" value="<?php echo e(!empty($_GET['grp'])?$_GET['grp']:''); ?>"/>
                            <button type="submit" value="search"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </form>
                </div>
                <a href="javascript:void(0);" class="colorbtn bluebtn newtest">+ &nbsp; New Group</a>
            </div>-->
        </div>
    	<div class="test-deta">
            <div class="wrapper">
                <table class="detatable grouptable cstable">
						<tr>
							<td class="detacol">
								<a href="#">
									<img src="<?php echo asset('assets/frontend/images/tag.png'); ?>" alt=""/>
									<p class="ttl"><strong>Plan</strong></p>
								</a>
							</td>
							<td class="detacol assigndeta">
								<p class="ttl"><strong>Amount</strong></p>
							</td>
							<td class="detacol assigndeta">
								<p class="ttl"><strong>Invoice Date</strong></p>
							</td>
							<td class="detacol assigndeta">
								<p class="ttl"><strong>Expired On </strong></p>
							</td>
							<td class="detacol assigndeta">
								<p class="ttl"><strong>Status</strong></p>
							</td>
							<td class="detacol assigndeta">
								<p class="ttl"><strong>Action</strong></p>
							</td>
						</tr>
					<?php if(!empty($invoices)): ?>
						<?php foreach($invoices as $val): ?>
						<tr>
							<td class="detacol">
								<a href="#">
									<img src="<?php echo asset('assets/frontend/images/tag.png'); ?>" alt=""/>
									<p class="ttl"><?php echo e($val->plan); ?></p>
								</a>
							</td>
							<td class="detacol assigndeta">
								<p class="ttl">$<?php echo e($val->price); ?></p>
							</td>
							<?php
							if(!empty($val))
							{
								$startd = strtotime($val->pivot->created_at);
								$exp = ($val->duration * (30)*86400);
								$expd = (int)$startd + $exp;
							}
							?>
							<td class="detacol assigndeta">
								<p class="ttl"><?php echo e(date('d M,Y',$startd)); ?></p>
							</td>
							<td class="detacol assigndeta">
								<p class="ttl"><?php echo e(date('d M,Y',$expd)); ?></p>
							</td>
							<td class="detacol assigndeta">
								<p class="ttl"><?php echo e($val->pivot->valid == 'Y'?'Current':'Expired'); ?></p>
							</td>
							<td class="detacol resultdeta">
								<?php if($val->pivot->add_by_admin=='Y' && $val->pivot->valid != 'Y'): ?>
								<form method="POST" action="<?php echo e(url('user/upgrade')); ?>">
									<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"/>
									<input type="submit" class="colorbtn" value="Pay"  />
									<input type="hidden" name="package_id" value="<?php echo e($val->pivot->package_id); ?>" />
									<input type="hidden" name="trans_id" value="<?php echo e($val->pivot->id); ?>" />
								</form>
								<?php else: ?>
								<a href="<?php echo e(url('user/invoice-details/'.$val->pivot->id)); ?>"><img src="<?php echo asset('assets/frontend/images/result.png'); ?>" alt=""><span>View</span></a>
								<?php endif; ?>
								
							</td>
						</tr>
						<?php endforeach; ?>
					<?php endif; ?>
                </table>
            </div>
        </div>
    </section>
    <!--main content close-->
    
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script>

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>