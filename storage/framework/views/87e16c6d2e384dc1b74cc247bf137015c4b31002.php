<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	<?php echo $client_header; ?>
	
    <!--main content open-->
    <section class="testpage">
		<?php if(session('member_success')): ?>
			<div class="success_message">
				<p><?php echo session('member_success'); ?></p>
			</div>
			<?php session()->forget('member_success');?>
		<?php endif; ?>
    	<div class="addnewbox">
        	<div class="wrapper">
                <div class="newformwrap">
                	<a href="javascript:void(0);" class="closebtn"><img src="<?php echo asset('assets/frontend/images/'); ?>/close.png" alt=""></a>
                    <form id="addNwfrm" name="add" method="post" action="<?php echo e(url('user/add-member')); ?>">
						<?php if($errors->any()): ?>
							<div id="login_warnings" class="warnings" style="">
								<div id="login_error_message" class="error_message">
									<?php foreach($errors->all() as $error): ?>
										<p><?php echo $error; ?></p>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif; ?>
						<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"/>
						<input type="hidden" name="group_id" value="<?php echo e($group->id); ?>"/>
                        <div class="formrow clear">
                            <label>
                                <span class="label">First name</span>
                                <div class="textfld">
                                    <input type="text" id="first_name" name="first_name" value="<?php echo e(old('first_name')); ?>">
                                </div>
                            </label>
                        </div>
                        <div class="formrow clear">
                            <label>
                                <span class="label">Last name</span>
                                <div class="textfld">
                                    <input type="text" id="last_name" name="last_name" value="<?php echo e(old('last_name')); ?>">
                                </div>
                            </label>
                        </div>
                        <div class="formrow clear">
                            <label>
                                <span class="label">Email ID</span>
                                <div class="textfld">
                                    <input type="email" id="email_id" name="email" value="<?php echo e(old('email')); ?>">
                                </div>
                            </label>
                        </div>
                        <div class="formrow clear">
                            <label>
                                <span class="label">Set Password</span>
                                <div class="textfld">
                                    <input type="Password" name="password" value="">
                                </div>
                            </label>
                        </div>
                        <div class="formrow checkrow">
                        	<label>
                            	<input type="checkbox" name="chk" value="1" checked />
                            	<span class="checklabel">Send Login credential to the email.</span>
                            </label>
                        </div>
                        <div class="addsubmit membersubmit">
                            <input type="submit" class="colorbtn bluebtn" value="Add Member"/>
                        </div>
						<input type="hidden" id="member_id" name="member_id" value=""/>
                    </form>
                </div>
            </div>
        </div>
    	<div class="deta-search">
        	<div class="wrapper clear">
            	<div class="search-box groupsearch clear">
                	<form name="search" method="get" action="">
                        <div class="formcol">
                            <input type="text" name="mem" placeholder="Search Member" value="<?php echo e(!empty($_GET['mem'])?$_GET['mem']:''); ?>">
                            <button type="submit" value="search"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </form>
                </div>
                <a href="javascript:void(0);" id="nwBtn" class="colorbtn bluebtn newtest">+ &nbsp; New Member</a>
            </div>
        </div>
    	<div class="test-deta">
            <div class="wrapper">
                <div class="detatable">
				<?php if(!empty($group_members)): ?>
					<?php foreach($group_members as $member): ?>
                	<div class="testtable membertable">
                    	<div class="testtablerow">
                            <div class="detacol detafld">
                                <p class="ttl"><?php echo e($member->first_name); ?> <?php echo e($member->last_name); ?></p>
                            </div>
                            <div class="detacol setpassword">
                                <a href="javascript:void(0);"><img src="<?php echo asset('assets/frontend/images/pass.png'); ?>" alt=""><span>Reset Password</span></a>
                            </div>
                            <div class="detacol blockmember">
                                <a href="javascript:void(0);" class="blok" >
									<img src="<?php echo asset('assets/frontend/images/block.png'); ?>" alt="">
									<span class="blktxt"><?php echo e($member->status=='Y'?'Block':'Unblock'); ?></span>
									<input type="hidden" value="<?php echo e($member->id); ?>"/>
								</a>
                            </div>
                            <div class="detacol editdeta">
                                <a href="javascript:void(0);" class="edt" >
									<img src="<?php echo asset('assets/frontend/images/new-file.png'); ?>" alt=""><span>Edit</span>
									<input id="memid" type="hidden" value="<?php echo e($member->id); ?>"/>
									<input id="fname" type="hidden" value="<?php echo e($member->first_name); ?>"/>
									<input id="lname" type="hidden" value="<?php echo e($member->last_name); ?>"/>
									<input id="mailid" type="hidden" value="<?php echo e($member->email); ?>"/>									
								</a>
                            </div>
                            <div class="detacol removedeta">
                                <a href="javascript:void(0);" class="rmve">
								<img src="<?php echo asset('assets/frontend/images/remove.png'); ?>" alt=""><span>Remove</span>
								<input type="hidden" value="<?php echo e($member->id); ?>"/>
								</a>
                            </div>
                        </div>
                        <div class="testlinkdeta">
                        	<h4>Reset Password:</h4>
							<form class="rstfrm">
							<span class="rstmsg">&nbsp;</span>
							<input type="hidden" name="user_id" value="<?php echo e($member->id); ?>"/>
                            <div class="formrow">
                            	<label>
                                <span class="label">New Password</span>
                            	<input name="password" type="password" value="">
                                </label>
                            </div>
                            <div class="formrow">
                            	<label>
                                <span class="label">Confarm New Password</span>
                            	<input name="password_confirmation" type="text" value="">
                                </label>
                            </div>
                            <div class="membersubmit">
                            	<input class="colorbtn chngPass" type="button" value="Change Password" />
                            	<input class="colorbtn" style="background-color:#757575" type="reset" value="Clear" />
                            </div>
							</form>
                        </div>
                    </div>
					<?php endforeach; ?>
				<?php endif; ?>
                </div>
            </div>
        </div>
    </section>
    <!--main content close-->
    
        
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script>
$(".chngPass").click(function() {
	var obj = $(this);
	var frm = obj.parent().parent();
	frm.find(".rstmsg").text('');
	//console.log(frm.html());
	var pass = frm.find('input[name="password"]').val();
	var cnfpass = frm.find('input[name="password_confirmation"]').val();
	var userid = frm.find('input[name="user_id"]').val();
	$.ajax({
			type:"post",
			url: "<?php echo url('user/member-pass-reset'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {password: pass, password_confirmation: cnfpass, id: userid},
			//dataType: "json",
			error: function (xhr, ajaxOptions, thrownError) {
				//console.log(xhr.responseText);
				var res = JSON.parse(xhr.responseText);
				//console.log(res.password[0]);
				frm.find(".rstmsg").text(res.password[0]);
			},
			success:function(data) {
				console.log(data);
				if(data=='s'){
					frm.find(".rstmsg").text('Password Reset Successfull!');
					$('.rstfrm')[0].reset();
				}
			}
		});
	//console.log(frm.find('input[name="password"]').val());
});

$(".edt").click(function() {
	var obj= $(this);
	var memid= obj.children('input#memid').val();
	var fname= obj.children('input#fname').val();
	var lname= obj.children('input#lname').val();
	var mailid= obj.children('input#mailid').val();
	//console.log(obj.parent().parent().find('.ttl').html());
	$('#member_id').val(memid);
	$('#first_name').val(fname);
	$('#last_name').val(lname);
	$('#email_id').val(mailid);
	$('.addnewbox').slideDown(200);
});	
$(".rmve").click(function() {
	var obj= $(this);
	//console.log(obj.parent().parent().find('.ttl').html());
	var txt = obj.parent().parent().find('.ttl').text();
	var gpId= obj.children('input').val();
	var r = confirm("Are you sure to delete group "+txt+"?");
	if (r == true) {
		$.ajax({
			type:"post",
			url: "<?php echo url('user/del-group-member'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {id: gpId},
			//dataType: "json",
			success:function(data) {
				//console.log(data);
				obj.parent().parent().remove();
			}
		});/*  */
	}	
	
});
$(".blok").click(function() {
	var obj= $(this);
	//console.log(obj.parent().parent().find('.ttl').html());
	var txt = obj.parent().parent().find('.ttl').html();
	var memId = obj.children('input').val();
	//alert(memId);
	/* var r = confirm("Are you sure to block group "+txt+"?");
	if (r == true) {
	}	*/
		$.ajax({
			type:"post",
			url: "<?php echo url('user/member-block'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {id: memId},
			//dataType: "json",
			success:function(data) {
				//console.log(data);
				if(data=='Y')
				{
					obj.children('.blktxt').text('Block');
				}else{
					obj.children('.blktxt').text('Unblock');
				}
			}
		}); 
	
});
$(document).ready(function() {
    $('.newtest').click(function() {
		$('.addnewbox').slideDown(200);
		$("#addNwfrm")[0].reset();
		$("#member_id").val('');
	});
	$('.newformwrap .closebtn').click(function() {
		$('.addnewbox').slideUp(200);
	});
	
	$('.membertable .setpassword a').click(function() {
		$(this).parent('.setpassword').parent('.testtablerow').next('.testlinkdeta').slideToggle(200);
	});
	<?php if($errors->any()): ?>
		$('.addnewbox').slideDown(200);
	<?php endif; ?>
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>