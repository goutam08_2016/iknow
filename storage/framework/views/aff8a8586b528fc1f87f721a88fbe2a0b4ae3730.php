<?php $__env->startSection('pageTitle', 'Welcome to '); ?>



<?php $__env->startSection('customStyle'); ?>



<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>



	<?php echo $client_header; ?>


    <!--main content open-->

    <section class="testpage">

    	<div class="addnewbox">

        	<div class="wrapper">

                <div class="newformwrap">

                	<a href="javascript:void(0);" class="closebtn"><img src="<?php echo asset('assets/frontend/images'); ?>/close.png" alt=""></a>

                    <form id="addgrp" name="add" method="post" action="">

                        <div class="formrow clear">

                            <label>

                                <span class="label">Test name:</span>

                                <div class="textfld">

                                    <input type="text" id="title" name="title" value="" />

                                </div>

                            </label>

                        </div>

                        <div class="addsubmit">

                            <input type="button" id="adGpbtn" onclick="addTest(0)" class="colorbtn bluebtn" value="Add Test">

                        </div>

                    </form>


                </div>

            </div>

        </div>

    	<div class="deta-search">
        	<div class="wrapper clear">
			&nbsp;
			<?php if(session('error_message')): ?>
				<div class="warnings">
					<div class="error_message">
						<p><?php echo session('error_message'); ?></p>
					</div>
				</div>
			<?php endif; ?>	
            </div>
        </div>
    	<div class="test-deta">

            <div class="wrapper">
				
                <div class="detatable">
		
					<?php if(!empty($tests)): ?>

						<?php foreach($tests as $val): ?>
						<div class="testtable">
							<div class="testtablerow">
								<div class="detacol detafld">
									<a href="<?php echo e(url('user/test-inro/'.$val->id)); ?>">
										<img src="<?php echo asset('assets/frontend/images/test.png'); ?>" alt="">
										<p class="ttl"><?php echo e($val->title); ?></p>
									</a>									
								</div>
								
								<div class="detacol copydeta">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</div>
								<div class="detacol copydeta">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</div>
								<div class="detacol copydeta">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</div>
								<div class="detacol copydeta">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</div>
								
								<div class="detacol resultdeta">
									<a href="<?php echo e(url('user/test-results/'.$val->id)); ?>"><img src="<?php echo asset('assets/frontend/images/result.png'); ?>" alt=""><span>Result</span></a>
								</div>
								<div class="detacol resultdeta">
									<a href="<?php echo e(url('user/test-inro/'.$val->id)); ?>"><img src="<?php echo asset('assets/frontend/images/new-file.png'); ?>" alt=""><span>Start Test</span></a>
								</div>
							</div>
						</div>

						<?php endforeach; ?>

					<?php endif; ?>

                </table>

            </div>

        </div>

    </section>

    <!--main content close-->

    

	

<?php $__env->stopSection(); ?>



<?php $__env->startSection('customScript'); ?>



<script>
$('.svgrps').click(function(){
	
	var frm = $(this).parent();
	var data_arr = frm.serializeArray();
	
	//console.log(data_arr);
	if(data_arr)
	{
		$.ajax({

			type:"post",

			url: "<?php echo url('user/assign-test-group'); ?>" ,

			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},

			data: data_arr,

			//dataType: "json",

			success:function(data) {

				//console.log(data);

				window.location.reload();

			}

		});	
	}
});


</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>