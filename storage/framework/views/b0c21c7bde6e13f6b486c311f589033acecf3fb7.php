<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */
$resource = 'categories';
$resource_pl = str_plural($resource);
$page_title = ucfirst(str_replace('_', ' ', $resource));
$page_title_pl = ucfirst(str_replace('_', ' ', $resource_pl));

$adpos_arr= array(1=>'Home Ad 1',2=>'Home Ad 2');
?>



<?php $__env->startSection('pageTitle', $page_title_pl . ' list'); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Categories
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="<?php echo url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Categories</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All Categories list</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <?php if(session('success')): ?>
                            <div class="alert alert-success">
                                <?php echo session('success'); ?>

                            </div>
                        <?php endif; ?>
                        <table id="list_table" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Category</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(${$resource_pl} !== null): ?>
                                <?php foreach(${$resource_pl} as $row): ?>
                                    <tr>
                                        <td><?php echo $row->title; ?></td>
                                        <td><img class="list_table_img" src="<?php echo asset('assets/upload/category_icon/'.$row->icon); ?>" alt="No icon"></td>
                                        <td>
                                            <a href="<?php echo admin_url($resource . '/' . $row->id . '/edit'); ?>" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                            <form method="POST" action="<?php echo admin_url($resource . '/' . $row->id); ?>"
                                                  onsubmit="return confirm('Are you sure to remove <?php echo $row->first_name; ?>?');">
                                                <input name="_method" type="hidden" value="DELETE"/><?php echo e(csrf_field()); ?>

                                                <button class="btn btn-sm btn-danger td-btn" type="submit"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="5">No Car maker Found..</td>
                                </tr>
                            <?php endif; ?>

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                    <div class="paginationDiv">
                        <?php echo ${$resource_pl}->render(); ?>

                    </div>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
<script type="text/javascript">
    $(function () {
        $("#list_table").DataTable({
            "paging":   false,
            "ordering": false
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>