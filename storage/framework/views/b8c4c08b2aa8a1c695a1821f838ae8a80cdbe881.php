<!--top section open-->
<section class="test-top">
	<div class="wrapper clear">
		<h2><?php echo e($title); ?> </h2>
		<?php if(Auth::user()->user_type==2): ?>
		<span id="catenavbtn">
			<i class="fa fa-bars"></i>
		</span>
		<div class="cate-menu">
			<ul>
				<li class="<?php echo Request::segment(2) == 'tests'?'active':''; ?>">
					<a href="<?php echo e(url('user/tests')); ?>">
						<img src="<?php echo asset('assets/frontend/images/exam-small.png'); ?>" alt="">
						<span>tests</span>
					</a>
				</li>
				<li class="<?php echo Request::segment(2) == 'question-bank'?'active':''; ?>">
					<a href="<?php echo e(url('user/question-bank')); ?>">
						<img src="<?php echo asset('assets/frontend/images/discuss-issue.png'); ?>" alt="">
						<span>question bank</span>
					</a>
				</li>
				<li class="<?php echo Request::segment(2) == 'categories'?'active':''; ?>">
					<a href="<?php echo e(url('user/categories')); ?>">
						<img src="<?php echo asset('assets/frontend/images/different-squares.png'); ?>" alt="">
						<span>categories</span>
					</a>
				</li>
				<li class="<?php echo Request::segment(2) == 'groups'?'active':''; ?>">
					<a href="<?php echo e(url('user/groups')); ?>">
						<img src="<?php echo asset('assets/frontend/images/command-small.png'); ?>" alt="">
						<span>groups</span>
					</a>
				</li>
				<li>
					<a href="#">
						<img src="<?php echo asset('assets/frontend/images/broken-link-small.png'); ?>" alt="">
						<span>links</span>
					</a>
				</li>
			</ul>
		<?php endif; ?>
		</div>
	</div>
</section>
<!--top section close-->