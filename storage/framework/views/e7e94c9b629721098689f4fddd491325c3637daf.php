<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <!--banner open-->
    <section class="homebanner">
    	<div class="bannerslider">
        	<div class="banner-slide" style="background-image:url(<?php echo asset('assets/frontend/images/banner.jpg'); ?>);">
            	<div class="wrapper">
                	<div class="banner-caption">
                    	<h2>Choose your <strong>Platform  
Business or Education.</strong>
Create your<br> 
Own Test</h2>
                        <a href="#">Request a demo</a>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
    <!--banner close-->
    
    <!--blue section open-->
    <section class="blue-section">
    	<div class="wrapper clear">
        	<div class="blue-left">
            	<h2>Carrying out recruitment</h2>
                <h3>Create your test for students and pupils</h3>
                <ul>
                	<li>
                    	<figure class="iconbox">
                        	<img src="<?php echo asset('assets/frontend/images/icon01.png'); ?>" alt="">
                        </figure>
                    	<div class="req-text">
                            <h4>Easy Step to create Test and Questions</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                        </div>
                    </li>
                    <li>
                        <figure class="iconbox">
                        	<img src="<?php echo asset('assets/frontend/images/icon02.png'); ?>" alt="">
                        </figure>
                    	<div class="req-text">
                            <h4>Easy to define Test settings</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                        </div>
                    </li>
                    <li>
                        <figure class="iconbox">
                        	<img src="<?php echo asset('assets/frontend/images/icon03.png'); ?>" alt="">
                        </figure>
                    	<div class="req-text">
                            <h4>No software installations required</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="blue-right">
            	<div class="small-bar">
                	<img src="<?php echo asset('assets/frontend/images/color-dot.png'); ?>" width="44" height="10" alt="">
                    <span>https://www.google.co.in/</span>
                </div>
                <div class="orange-bar">
                	<span>The Quiz Maker for Professionals</span>
                </div>
                <div class="video-wrap">
                	<!--<img src="assets/images/video.jpg" alt="">-->
                    <!--<img src="assets/images/video.jpg" alt="">-->
                    <iframe style="background-image:url(<?php echo asset('assets/frontend/images/video.jpg'); ?>);"></iframe>
                    <a href="javascript:void(0);" class="play-video" data-video="https://www.youtube.com/embed/9gTw2EDkaDQ">
                    	<i class="fa fa-play-circle" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!--blue section close-->
    
    <!--your staf open-->
    <section class="yourstaf">
    	<div class="wrapper clear">
        	<div class="stafright">
            	<h2>Manage your staff</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,  sed do eiusmod tempor incididunt ut labore et dolore magna.Lorem ipsum dolor sit amet, consectetur adipiscing elit,  sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,  sed do eiusmod tempor incididunt ut labore et dolore magna.Lorem ipsum dolor sit amet, </p>
            </div>
        	<div class="stafleft">
            	<div class="staf-login">
                	<h3>Staff Login</h3>
                	<div class="form-box">
                        <form name="form" method="post" action="#">
                            <div class="formrow">
                                <input type="email" placeholder="Email">
                            </div>
                            <div class="formrow">
                                <input type="password" placeholder="Password">
                            </div>
                            <input type="submit" value="login" class="colorbtn">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--your staf close-->
    
    <!--track result open-->
    <section class="resultsection">
    	<div class="wrapper clear">
        	<div class="chart-left">
            	<h3>Activity Over Time</h3>
                <div class="chart-wrap">
                    <!--<div id="bar-chart-example"></div>-->
                    <img src="<?php echo asset('assets/frontend/images/chart.png'); ?>" alt="">
                </div>
            </div>
            <div class="track-text">
            	<h2>Track your results</h2>
                <ul>
                	<li>
                    	<h3>Review stats</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                    </li>
                    <li>
                    	<h3>Analyze responses</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                    </li>
                    <li>
                    	<h3>Export leads</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                    </li>
                </ul>
                <a href="#" class="colorbtn bluebtn">Get Started Now</a>
            </div>
        </div>
    </section>
    <!--track result close-->
    
    <!--testimonial open-->
    <section class="testimonial">
    	<div class="wrapper">
        	<h2>Testimonial</h2>
        	<div class="testimonialslider">
            	<div class="testislide">
                	<figure><img src="<?php echo asset('assets/frontend/images/testi-thumb.png'); ?>" width="136" height="136" alt=""></figure>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                    <h4>- Director, John Smith</h4>
                    <a href="#" class="colorbtn bluebtn">View all ></a>
                </div>
                <div class="testislide">
                	<figure><img src="<?php echo asset('assets/frontend/images/testi-thumb.png'); ?>" width="136" height="136" alt=""></figure>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                    <h4>- Director, John Smith</h4>
                    <a href="#" class="colorbtn bluebtn">View all ></a>
                </div>
            </div>
        </div>
    </section>
    <!--testimonial close-->
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>