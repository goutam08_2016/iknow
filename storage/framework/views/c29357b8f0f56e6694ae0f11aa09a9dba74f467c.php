<?php $__env->startSection('pageTitle', 'Welcome to '); ?>



<?php $__env->startSection('customStyle'); ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>

	<!--body open-->
    <section class="mainbody clear">
    	<!--left pan open-->
    	<?php echo $__env->make('include.left_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!--left pan close-->
        
        <!--middle open-->
        <div class="middlecol">
            <div class="pTab">
            	<?php echo $__env->make('include.setting_tabs', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<div class="tabContWrap settingcont">
					<form id="privacy" method="post" action="">
						<div class="settingbox">
							<h3>Privacy</h3>
							<div class="settingrow">
								<label>
									<input type="checkbox" name="online" value="Y" onChange="$('#privacy').submit();" <?php echo e(!empty($prevdata) && $prevdata->online=='Y'?'checked':''); ?>  />
									<span class="chkicon"></span>
									<span class="chklabel">Allow other users to see whether i'm online.</span>
								</label>
							</div>
							<div class="settingrow">
								<label>
									<input type="checkbox" value="Y" name="other_view_account" onChange="$('#privacy').submit();" <?php echo e(!empty($prevdata) && $prevdata->other_view_account=='Y'?'checked':''); ?> />
									<span class="chkicon"></span>
									<span class="chklabel">Allow other users to view my account.</span>
								</label>
							</div>
						</div>
						<div class="settingbox">
							<h3>Inbox Preferences</h3>
							<h4>Who do you want to receive message from?</h4>
							<div class="settingrow">
								<label>
									<input type="radio" name="want_receive_message" value="S" onChange="$('#privacy').submit();" <?php echo e(!empty($prevdata) && $prevdata->want_receive_message=='S'?'checked':''); ?> >
									<span class="radioicon"></span>
									<span class="chklabel">Receive messages from people</span>
								</label>
							</div>
							<div class="settingrow">
								<label>
									<input type="radio" name="want_receive_message" value="A" onChange="$('#privacy').submit();" <?php echo e(!empty($prevdata) && $prevdata->want_receive_message=='A'?'checked':''); ?>>
									<span class="radioicon"></span>
									<span class="chklabel">Receive messages from anyone</span>
								</label>
							</div>
							<div class="settingrow">
								<label>
									<input type="radio" name="want_receive_message" value="N" onChange="$('#privacy').submit();" <?php echo e(!empty($prevdata) && $prevdata->want_receive_message=='N'?'checked':''); ?>>
									<span class="radioicon"></span>
									<span class="chklabel">Do not receive messages</span>
								</label>
							</div>
						</div>
						<div class="settingbox">
							<h3>Answer Request</h3>
							<div class="settingrow">
								<label>
									<input type="checkbox" name="decline_request" value="Y" onChange="$('#privacy').submit();" <?php echo e(!empty($prevdata) && $prevdata->decline_request=='Y'?'checked':''); ?>>
									<span class="chkicon"></span>
									<span class="chklabel">Decline anonymous request to answer.</span>
								</label>
							</div>
							<div class="settingrow">
								<span class="label">The maximum number of request to answer.</span>
								<div class="smallsetting">
									<input type="number" placeholder="00" name="max_request" onChange="$('#privacy').submit();" value="<?php echo e(!empty($prevdata) && $prevdata->max_request>0?$prevdata->max_request:''); ?>" />
									<div class="reqday">
										<span>Request per</span>
										<select name="request_intelval" onChange="$('#privacy').submit();">
											<option value="D" <?php echo e(!empty($prevdata) && $prevdata->request_intelval=='D'?'selected':''); ?>>Day</option>
											<option value="M" <?php echo e(!empty($prevdata) && $prevdata->request_intelval=='M'?'selected':''); ?>>Month</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="settingbox">
							<h3>Comment Preferences</h3>
							<div class="settingrow">
								<label>
									<input type="checkbox" name="allow_comments" value="Y" onChange="$('#privacy').submit();" <?php echo e(!empty($prevdata) && $prevdata->allow_comments=='Y'?'checked':''); ?> />
									<span class="chkicon"></span>
									<span class="chklabel">Allow comments on your answer and post.</span>
								</label>
							</div>
						</div>
					</form>
					<div class="settingbox">
						<div class="settingrow deletepro">
							<h4>Delete or Deactivate Your Account</h4>
							<ul>
								<li><a href="javascript:void(0);" id="deactv"><?php echo e(!empty($prevdata) && $prevdata->deactivate_account=='Y'?'Activate Account':'Deactivate Account'); ?></a></li>
								<li><a href="#">Delete Account</a></li>
							</ul>
						</div>
					</div>
                </div>				
            </div>
        </div>
        <!--middle close-->
        
        <!--right pan open-->
        <?php echo $__env->make('include.right_pan', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!--right pan close-->
    </section>
    <!--body close-->

<?php $__env->stopSection(); ?>



<?php $__env->startSection('customScript'); ?>

<script>
$("#deactv").click(function(){
	$.ajax({
		type:"post",
		url: "<?php echo url('user/update-privacy-settings'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: {'deactivate_account':1},			
		dataType: "json",			
		error:function(erres) {	
			console.log(erres.responseText); 			
		},
		success:function(res) {
			console.log(res); 
			//alert(res.deactivate_account);
			if(res.deactivate_account=='Y')
			{
				actvtxt = "Activate Account";
			}else	actvtxt = "Deactivate Account";
			$("#deactv").text(actvtxt);
		}
	});
});
$( "form#privacy" ).on( "submit", function( event ) {
//
  event.preventDefault();
  var formdata =  $( this ).serialize();
  //console.log( $( this ).serialize() );
  updatePrivacySettings(formdata);
});

function updatePrivacySettings(formdata)
{
	$.ajax({
		type:"post",
		url: "<?php echo url('user/update-privacy-settings'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: formdata,			
		//dataType: "json",			
		error:function(erres) {	
			console.log(erres.responseText); 
			var reponse = JSON.parse(erres.responseText);
			
		},
		success:function(res) {
			//console.log(res); 
			
		}

	});
}
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>