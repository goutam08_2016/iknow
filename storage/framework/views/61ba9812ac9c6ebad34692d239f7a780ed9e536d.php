<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	<?php echo $client_header; ?>

    	 
    <!--main content open-->
    <section class="testpage">
		<div class="deta-search">
        	<div class="wrapper clear">
            	<!--<div class="search-box groupsearch clear">
                	<form name="search" method="post" action="#">
                        <div class="formcol">
                            <input type="text" placeholder="Search Group Name">
                            <button type="submit" value="search"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </form>
                </div>-->
                <!--<a href="#qscategory" class="colorbtn bluebtn newtest qspopup">+ &nbsp; Add Question</a>-->
               
            </div>
        </div>
		<div class="test-deta">
            <div class="wrapper">
                <div class="resultsarea">
                	<div class="printbar"><a href="" class="smallbtn"><i class="fa fa-print"></i> Print Result</a></div>
                    <div class="resultbox">
                        <section class="resultuser">
                          <article class="resultuserbox">
                          	<p class="mainline"><i><img src="<?php echo asset('assets/frontend/images/usericon.png'); ?>"></i> <?php echo e($test_eval->first_name); ?> <?php echo e($test_eval->last_name); ?> </p>
                           	<p><?php echo e($test_eval->email_address); ?></p>
                           <!-- <p>IP: 11.22.33.44</p>-->
                          </article>
                          <article class="resultuserbox">
                          	<p class="mainline"><i><img src="<?php echo asset('assets/frontend/images/result.png'); ?>"></i> <?php echo e($test->title); ?></p>
                          </article>
                          <article class="resultuserbox">
                          	<p class="mainline"><i><img src="<?php echo asset('assets/frontend/images/broken-link.png'); ?>"></i> Test Link</p>
                          </article>
                        	
                        </section>
                        <section class="resultnumber">
                        	<div class="markspercent pass"><?php echo round(($test_eval->score/($test_eval->total_point>0?$test_eval->total_point:1))*100); ?>%</div>
                            <div class="pointtable">
                            	<section>
                                	<span>Points:</span>
                                    <span><?php echo e($test_eval->score); ?> out of <?php echo e($test_eval->total_point); ?> </span>
                                </section>
                                <section>
                                	<span>Duration:</span>
                                    <span>
									<?php $timer = $test_eval->time_spend;
										$hours = (int)($timer / 3600);
										$minutes = (int)($timer / 60) - ($hours * 60);
										$seconds = (int)($timer % 60);

										$hours = $hours < 10 ? "0" . $hours : $hours;
										$minutes = $minutes < 10 ? "0" . $minutes : $minutes;
										$seconds = $seconds < 10 ? "0" . $seconds : $seconds;
										
										echo($hours . ":" . $minutes . ":" . $seconds);
									 ?>										
									</span>
                                </section>
                                <section>
                                	<span>Date strated:</span>
                                    <span><?php echo e(date("D d M'y h:ia",strtotime($test_eval->created_at))); ?> </span>
                                </section>
                                <section>
                                	<span>Date Finished:</span>
                                    <span><?php echo e(date("D d M'y h:ia",strtotime($test_eval->updated_at))); ?> </span>
                                </section>
                            </div>
                        </section>
                    	<div class="clear"></div>
                    </div>
                    <div class="btnpnl">
                    	<!--<span><a href="" class="smallbtn blue">Reopen</a></span>-->
                        <span><a href="" class="smallbtn blue">Email Result</a></span>
                        <span><a href="" class="smallbtn red">Delete</a></span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--main content close-->
    
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script>
$(document).ready(function() {
	 $('.newtest').click(function() {
		$('.addnewbox').slideDown(200);
		$("#addNwcat")[0].reset();
	});
	$('.newformwrap .closebtn').click(function() {
		$('.addnewbox').slideUp(200);
	});
	<?php if($errors->any()): ?>
		$('.addnewbox').slideDown(200);
	<?php endif; ?>
});
/* $(".rmve").click(function() {
	var obj= $(this);
	//console.log(obj.parent().parent().find('.ttl').html());
	var txt = obj.parent().parent().find('.ttl').text();
	var catId= obj.children('input').val();
	var r = confirm("Are you sure to delete group "+txt+"?");
	if (r == true) {
		$.ajax({
			type:"post",
			url: "<?php echo url('user/del-cat'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {id: catId},
			//dataType: "json",
			success:function(data) {
				//console.log(data);
				obj.parent().parent().remove();
			}
		});
	}	
	
}); */

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>