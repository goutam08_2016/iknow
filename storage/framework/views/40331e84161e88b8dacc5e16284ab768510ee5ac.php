<?php if(count($chat_messsage_data['chat_msg_arr_pop']) > 0): ?>
	<?php foreach($chat_messsage_data['chat_msg_arr_pop'] as $msg): ?>
		<?php if($msg->from_user_id == $chat_messsage_data['from_user_id_pop']): ?>
			<div class="list myreply">
				<?php if($msg->fromUser->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$msg->fromUser->profile_image)): ?>
					<a href="javascript:void(0);" class="photo">
						<img src="<?php echo asset('assets/upload/profile_pictures/'.$msg->fromUser->profile_image); ?>" alt="<?php echo $msg->fromUser->first_name; ?> <?php echo $msg->fromUser->last_name; ?>"/>
					</a>
                <?php else: ?>
                	<a href="javascript:void(0);" class="photo">
                    	<img src="<?php echo asset('assets/common/images/default_avatar.jpg'); ?>" alt="<?php echo $msg->fromUser->first_name; ?> <?php echo $msg->fromUser->last_name; ?>"/>
                    </a>
                <?php endif; ?>
				<span class="comment">
					<span class="type">
						<?php if($msg->type == 'T'): ?>
							<p><?php echo $msg->message; ?></p>
						<?php else: ?>
							<?php if(in_array($msg->file_ext,array('jpg','JPG','JPEG','png','PNG','bmp','BMP','gif','GIF'))): ?>
								<p><a href="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>" class="chat_pic_msg" ><img style="width:80px;" src="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>"/></a></p>
							<?php else: ?>
								<p><a href="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>" download ><?php echo $msg->message; ?></a></p>
							<?php endif; ?>
						<?php endif; ?>
					</span>
				</span>
				<small class="chattime"><?php echo date('h:i:s A',strtotime($msg->created_at)); ?></small>
			</div>
		<?php else: ?>
			<div class="list">
				<?php if($msg->fromUser->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$msg->fromUser->profile_image)): ?>
					<a href="javascript:void(0);" class="photo">
						<img src="<?php echo asset('assets/upload/profile_pictures/'.$msg->fromUser->profile_image); ?>" alt="<?php echo $msg->fromUser->first_name; ?> <?php echo $msg->fromUser->last_name; ?>"/>
					</a>
                <?php else: ?>
                	<a href="javascript:void(0);" class="photo">
                    	<img src="<?php echo asset('assets/common/images/default_avatar.jpg'); ?>" alt="<?php echo $msg->fromUser->first_name; ?> <?php echo $msg->fromUser->last_name; ?>"/>
                    </a>
                <?php endif; ?>
				<span class="comment">
					<span class="type">
						<?php if($msg->type == 'T'): ?>
							<p><?php echo $msg->message; ?></p>
						<?php else: ?>
							<?php if(in_array($msg->file_ext,array('jpg','JPG','JPEG','png','PNG','bmp','BMP','gif','GIF'))): ?>
								<p><a href="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>" class="chat_pic_msg" ><img style="width:80px;" src="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>"/></a></p>
							<?php else: ?>
								<p><a href="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>" download ><?php echo $msg->message; ?></a></p>
							<?php endif; ?>
						<?php endif; ?>
					</span>
				</span>
				<small class="chattime"><?php echo date('h:i:s A',strtotime($msg->created_at)); ?></small>
			</div>
		<?php endif; ?>
	<?php endforeach; ?>
<?php endif; ?>
			