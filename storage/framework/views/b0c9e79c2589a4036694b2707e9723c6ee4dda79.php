<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>


    <!--top section open-->
    <section class="all-top">
    	<div class="wrapper">
        	<h2>my account</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it 
 make a type specimen book. It has survived not only five centuries, but also the leap into electronic </p>
        </div>
    </section>
    <!--top section close-->
    
    <!--main content open-->
    <section class="innerpages">
    	<div class="wrapper clear">
        	<div class="middle-border-box clear">
                <div class="accountbox">
                	<ul>
                    	<li>
                        	<figure class="listicon"><img src="<?php echo asset('assets/frontend/images/paper.png'); ?>" alt=""></figure>
                            <div class="account-txt">
                            	<h3>My Details</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and 
typesetting industry. Lorem Ipsum has...<a href="#">Read More</a></p>
                            </div>
                        </li>
                        <li>
                        	<figure class="listicon"><img src="<?php echo asset('assets/frontend/images/users.png'); ?>" alt=""></figure>
                            <div class="account-txt">
                            	<h3>My Users</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and 
typesetting industry. Lorem Ipsum has...<a href="#">Read More</a></p>
                            </div>
                        </li>
                        <li>
                        	<figure class="listicon"><img src="<?php echo asset('assets/frontend/images/command.png'); ?>" alt=""></figure>
                            <div class="account-txt">
                            	<h3>Groups</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and 
typesetting industry. Lorem Ipsum has...<a href="groups.html">Read More</a></p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="accountbox">
                	<ul>
                    	<li>
                        	<figure class="listicon"><img src="<?php echo asset('assets/frontend/images/exam.png'); ?>" alt=""></figure>
                            <div class="account-txt">
                            	<h3>Tests</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and 
typesetting industry. Lorem Ipsum has...<a href="#">Read More</a></p>
                            </div>
                        </li>
                        <li>
                        	<figure class="listicon"><img src="<?php echo asset('assets/frontend/images/broken-link.png'); ?>" alt=""></figure>
                            <div class="account-txt">
                            	<h3>Links</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and 
typesetting industry. Lorem Ipsum has...<a href="#">Read More</a></p>
                            </div>
                        </li>
                        <li>
                        	<figure class="listicon"><img src="<?php echo asset('assets/frontend/images/settings.png'); ?>" alt=""></figure>
                            <div class="account-txt">
                            	<h3>Settings</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and 
typesetting industry. Lorem Ipsum has...<a href="#">Read More</a></p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--main content close-->
    
        
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>