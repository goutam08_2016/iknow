<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <!--body open-->
    <section class="pagecontent">
       <div class="container">
       		<div class="signupcont">
            	<h2>Sign Up</h2>
				
            	<div class="signupwrap">					
					<div class="signupStepsCont">
						<div class="signupSteps">
							<span class="st green"><i class="fa fa-check"></i></span>
							<span class="st st2">2</span>
							<span class="st st3">3</span>
							<span class="st st4">4</span>
							<span class="st st5">5</span>
							<div class="stepProgress">
								<div class="current"></div>
							</div>
						</div>
					</div>
                    <div class="signupbox clear">
                    	<div class="signupleft">                        	
							<div class="formrow">
								<span class="title">Place you live (now/in the past)<strong style="color:#f42517"> *</strong></span>
								
								<div class="frmsml clear">
									<input required class="loc" id="current_address" name="current_address" type="text" value=""/>
									<input type="hidden" value="1" class="addType"/>
									<input type="button" value="add" class="bluebtn addAddr">
								</div>
								<div class="addrow locationadd clear">
									<ul id="current_address_lst">
									<?php if(!empty($user_address)): ?>
										<?php foreach($user_address as $adr): ?>
										<?php if($adr->type==1): ?>
										<li>
											<span class="addbox withLocation"><?php echo e($adr->address); ?>

												<a href="javascript:void(0)" class="closebox rmvaddr"><img src="<?php echo asset('assets/frontend'); ?>/images/close.png" alt=""></a>
												<input type="hidden" value="<?php echo e($adr->id); ?>"/>
												<span class="loc <?php echo e($adr->is_default=='Y' ?'active':''); ?>"><i class="fa fa-map-marker"></i></span>
											</span>
										</li>
										<?php endif; ?>
										<?php endforeach; ?>
									<?php endif; ?>
									</ul>
								</div>
							</div>
							<div class="formrow">
								<span class="title">Place you have been</span>
								<div class="frmsml clear">
									<input required class="loc" id="address" name="address" type="text" value="">
									<input type="hidden" value="2" class="addType"/>
									<input type="button" value="add" class="bluebtn addAddr">
								</div>
								<div class="addrow clear">
									<ul  id="address_lst">
									<?php if(!empty($user_address)): ?>
										<?php foreach($user_address as $adr): ?>
										<?php if($adr->type==2): ?>
										<li>
											<span class="addbox"><?php echo e($adr->address); ?>

												<a href="javascript:void(0)" class="closebox rmvaddr"><img src="<?php echo asset('assets/frontend'); ?>/images/close.png" alt=""></a>
												<input type="hidden" value="<?php echo e($adr->id); ?>"/>
											</span>
										</li>
										<?php endif; ?>
										<?php endforeach; ?>
									<?php endif; ?>
									</ul>
								</div>
							</div>
							<form id="signupstep" name="signup" method="post" action="">
							<?php echo e(csrf_field()); ?>

                                <div class="buttonbox clear">
                                	<a href="<?php echo e(url('user/signup-step/1')); ?>" class="prevstep assbtn">prev</a>
                                    <input type="button" onclick="proceedToNext()" value="next" class="nextstep bluebtn">
									
                                    <input type="hidden" name="in_step" value="<?php echo e($step); ?>" />
                                </div>
                            </form>
                        </div>
                        <div class="signupthumb clear">
                        	<img src="<?php echo asset('assets/frontend'); ?>/images/signup-charecter.png" width="146" height="459" alt="">
                        </div>
                    </div>
                </div>
                
            </div>
       </div>
    </section>
    <!--body close-->
	<?php $curr_addr = $user->user_Addresses()->where('is_default','Y')->first();?>
<input type="hidden" id="curr_addr" value="<?php echo e(!empty($curr_addr)?$curr_addr->address:''); ?>"/>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKqvPeyXND6or2BXGG1O-qp-bC_lIz7Uk&libraries=places"></script>
<script>
$(document).ready(function() {
    $(document).on('click','.rmvaddr',function() {
		var dobj = $(this);
		var rmvid = dobj.next('input').val();
		$.ajax({
			type:"post",
			url: "<?php echo url('user/del-address'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {id:rmvid},			
			//dataType: "json",			
			success:function(res) {
				console.log(res);
				if(res != 0)
				{}
					if(res.address)
					{
						$("#curr_addr").val(res.address);						
					}
					//dobj.parent('.addbox').parent('li').remove();
					dobj.parent().remove();	
				
			}

		});
		
	});
});
 setMapLoc('address');
 setMapLoc('current_address');
function setMapLoc(id){
	var inputt = document.getElementById(id);
	var autocomplete = new google.maps.places.Autocomplete(inputt);	
}
$('.addAddr').click(function(){
	//current_address
	var addbtn = $(this);
	var addrval = addbtn.parent().find('input.loc').val();
	var fid = addbtn.parent().find('input.loc').attr('id');
	var addType = addbtn.parent().find('input.addType').val();
	if(addrval=='')
	{
		$('#'+fid).focus();
	}else{
		$.ajax({
			type:"post",
			url: "<?php echo url('user/add-address'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {'address':addrval, 'addType':addType},			
			//dataType: "json",			
			success:function(res) {
				//console.log(res);
				$('#'+fid).val('');
				var lhtml = '<li><span class="addbox">'+addrval+'<a href="javascript:void(0)" class="closebox rmvaddr"><img src="'+'<?php echo asset('assets/frontend'); ?>'+'/images/close.png" alt=""></a><input type="hidden" value="'+res.id+'"/></span></li>';
				$('#'+fid+'_lst').append(lhtml);
				/* window.location.reload(); */
			}

		});
		
	}

});
function proceedToNext()
{
	var cntaddr = $("#curr_addr").val();
	//alert(cntaddr);
	if(cntaddr!='')
	{
		$("#signupstep").submit();
	}else{
		alert("Please set your current location..");
		$("#current_address").focus();
	}
}

$(document).on('click',".locationadd .addbox .loc", function(){
	var currentobj = $(this);
	var addrid = $(this).parent().find('input').val();
	$.ajax({
		type:"post",
		url: "<?php echo url('user/setdeflt-address'); ?>" ,
		headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
		data: {'address_id':addrid, },			
		//dataType: "json",			
		success:function(res) {
			//console.log(res);
			if(res!=0)
			{
				$(".locationadd .addbox .loc").removeClass('active');
				currentobj.toggleClass('active');	
				$("#curr_addr").val(res.address);
			}
		}
	});
	//console.log($(this).parent().html());
});


</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>