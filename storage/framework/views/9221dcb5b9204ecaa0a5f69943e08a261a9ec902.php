<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	<?php echo $client_header; ?>
    	 
    <!--main content open-->
    <section class="testpage">
		<?php if(session('cat_success')): ?>
			<div class="success_message">
				<p><?php echo session('cat_success'); ?></p>
			</div>
			<?php session()->forget('cat_success');?>
		<?php endif; ?>
    	<div class="addnewbox">
        	<div class="wrapper">
                <div class="newformwrap">
                	<a href="javascript:void(0);" class="closebtn"><img src="<?php echo asset('assets/frontend/images/close.png'); ?>" alt=""></a>
                    <form id="addNwcat" name="add" method="post" action="<?php echo e(url('user/add-category')); ?>">
						<?php if($errors->any()): ?>
							<div id="login_warnings" class="warnings" style="">
								<div id="login_error_message" class="error_message">
									<?php foreach($errors->all() as $error): ?>
										<p><?php echo $error; ?></p>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif; ?>
						<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"/>
                        <div class="formrow clear">
                            <label>
                                <span class="label">Category name:</span>
                                <div class="textfld">
                                    <input type="text" id="title" name="title" value="">
                                    <input type="hidden" id="cat_id" name="id" value="">
                                </div>
                            </label>
                        </div>
                        <div class="addsubmit">
                            <input type="submit" class="colorbtn bluebtn" value="Add Category">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    	<div class="deta-search">
        	<div class="wrapper clear">
            	<!--<div class="search-box groupsearch clear">
                	<form name="search" method="post" action="#">
                        <div class="formcol">
                            <input type="text" placeholder="Search Group Name">
                            <button type="submit" value="search"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </form>
                </div>-->
                <a href="javascript:void(0);" class="colorbtn bluebtn newtest">+ &nbsp; New Category</a>
            </div>
        </div>
    	<div class="test-deta">
            <div class="wrapper">
                <table class="detatable catetable">
                    <tr>
                        <th class="detacol detafld detatag">Categories</th>
                        <th class="detacol detacount">Questions</th>
                        <th class="detacol detacount" colspan="2">Action</th>
                    </tr>
					<?php if(!empty($categories)): ?>
						<?php foreach($categories as $val): ?>
						<tr>
							<td class="detacol detafld detatag">
								<a href="#">
									<img src="<?php echo asset('assets/frontend/images/tag.png'); ?>" alt="">
									<p class="ttl"><?php echo e($val->title); ?></p>
								</a>
							</td>
							<td class="detacol detacount">
								<a href="#"><span>2</span></a>
							</td>
							<td class="detacol editdeta">
								<a href="javascript:void(0)" class="edt" ><img src="<?php echo asset('assets/frontend/images/new-file.png'); ?>" alt=""><span>Edit</span><input type="hidden" value="<?php echo e($val->id); ?>" /></a>
							</td>
							<td class="detacol removedeta">
								<a href="javascript:void(0)" class="rmve" ><img src="<?php echo asset('assets/frontend/images/remove.png'); ?>" alt=""><span>Remove</span><input type="hidden" value="<?php echo e($val->id); ?>" /></a>
							</td>
						</tr>
						<?php endforeach; ?>
					<?php endif; ?>
                </table>
            </div>
        </div>
    </section>
    <!--main content close-->
    
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<script>
$(document).ready(function() {
	 $('.newtest').click(function() {
		$('.addnewbox').slideDown(200);
		$("#addNwcat")[0].reset();
	});
	$('.newformwrap .closebtn').click(function() {
		$('.addnewbox').slideUp(200);
	});
	<?php if($errors->any()): ?>
		$('.addnewbox').slideDown(200);
	<?php endif; ?>
});
$(".rmve").click(function() {
	var obj= $(this);
	//console.log(obj.parent().parent().find('.ttl').html());
	var txt = obj.parent().parent().find('.ttl').text();
	var catId= obj.children('input').val();
	var r = confirm("Are you sure to delete group "+txt+"?");
	if (r == true) {
		$.ajax({
			type:"post",
			url: "<?php echo url('user/del-cat'); ?>" ,
			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
			data: {id: catId},
			//dataType: "json",
			success:function(data) {
				//console.log(data);
				obj.parent().parent().remove();
			}
		});
	}	
	
});

$(".edt").click(function() {
	var obj= $(this);
	var cat_id= obj.children('input').val();
	//console.log(obj.parent().parent().find('.ttl').html());
	var txt = obj.parent().parent().find('.ttl').text();
	$('#title').val(txt);
	$('#cat_id').val(cat_id);
	$('.addnewbox').slideDown(200);
});	/* 
$(document).ready(function() {
    $('.newtest').click(function() {
		$('.addnewbox').slideDown(200);
		$("#addgrp")[0].reset();
		$('#adGpbtn').attr('onclick','addGroup(0)');
	});
	$('.newformwrap .closebtn').click(function() {
		$('.addnewbox').slideUp(200);
	});
	
	$('.membertable .setpassword a').click(function() {
		$(this).parent('.setpassword').parent('.testtablerow').next('.testlinkdeta').slideToggle(200);
	});
	
}); */
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>