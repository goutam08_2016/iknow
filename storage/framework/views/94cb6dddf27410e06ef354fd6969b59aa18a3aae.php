<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */
$resource = 'dealer';
$resource_pl = str_plural($resource);
$page_title = ucfirst($resource_pl);
?>



<?php $__env->startSection('pageTitle', 'Dashboard'); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo e($page_title); ?>

            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="<?php echo url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><?php echo e($page_title); ?></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All <?php echo e($resource); ?> list</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <?php if(session('success')): ?>
                            <div class="alert alert-success">
                                <?php echo session('success'); ?>

                            </div>
                        <?php endif; ?>
                        <table id="list_table" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Business name</th>
                                <th>Subscription</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if($users !== null): ?>
                                <?php foreach($users as $row): ?>
                                    <tr>
                                        <td><?php echo $row->first_name . ' ' . $row->last_name; ?></td>
                                        <td> <a href="<?php echo admin_url('dealer/' . $row->id); ?>" class=""><?php echo $row->business_name; ?></a></td>
                                        <td><?php echo (count($row->subscriptions)>0)?$row->subscriptions[0]->plan->title:''; ?></td>
                                        <td><?php echo $row->email; ?></td>
                                        <?php
                                        /*<td>
                                            <select  class="status">
                                                <option data-id="{{ $row->id }}"  class="bg-green-active color-palette" value="Y" {{ $row->status == 'Y'?'selected':''  }}>Active</option>
                                                <option data-id="{{ $row->id }}" class="bg-red-active color-palette" value="N" {{ $row->status == 'N'?'selected':''  }}>Block</option>
                                                <option  data-id="{{ $row->id }}" class="bg-orange-active color-palette" value="E" {{ $row->status == 'E'?'selected':''  }}>Pending</option>
                                            </select>
                                        </td> */ ?>
                                        <td>
                                            <a href="<?php echo admin_url('dealer/' . $row->id . '/edit'); ?>" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                            <form method="POST" action="<?php echo admin_url('dealer/' . $row->id); ?>"
                                                  onsubmit="return confirm('Are you sure to <?php echo e($row->status=='Y'?'disable':'enable'); ?> <?php echo $row->first_name; ?>?');">
                                                <input name="_method" type="hidden" value="DELETE"/><?php echo e(csrf_field()); ?>

                                                <button class="btn btn-sm btn-<?php echo e($row->status=='Y'?'success':'danger'); ?> td-btn" type="submit"><i class="fa" aria-hidden="true"></i> <?php echo $row->status=='Y'?'Enabled':'Disabled'; ?></button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="5">No user Found..</td>
                                </tr>
                            <?php endif; ?>

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                    <div class="paginationDiv">
                        <?php echo $users->render(); ?>

                    </div>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
<script type="text/javascript">
    $(function () {
        $("#list_table").DataTable({
            "paging":   false,
            "ordering": false
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>