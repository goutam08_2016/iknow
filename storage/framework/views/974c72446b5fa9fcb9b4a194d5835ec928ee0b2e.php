<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<div class="logincontent">
		<p class="logintag">Get the right answer from the right person in the right time</p>
		<h3>register now</h3>
		<?php if($errors->any()): ?>
			<div class="warnings">
				<div class="error_message">
					<?php foreach($errors->all() as $error): ?>
						<p><?php echo $error; ?></p>
					<?php endforeach; ?>
				</div>
			</div>
		<?php elseif(session('register_success')): ?>
			<div class="success_message">
				<p><?php echo session('register_success'); ?></p>
			</div>
			<?php session()->forget('register_success');?>
		<?php endif; ?>
		<form method="post" id="loginForm" action="<?php echo e(url('/register')); ?>" class="" data-parsley-validate="">
			<?php echo e(csrf_field()); ?>

			<div class="formrow borderbox">
				<input type="email" Placeholder="Email" id="email" name="email" data-parsley-trigger="change" required value="<?php echo e(old('email')); ?>" />
			</div>
			<div class="formrow borderbox">
				<input type="password" id="password" name="password" required placeholder="Password"/>
			</div>
			<div class="formrow borderbox">
				<input type="password" id="password_confirmation" name="password_confirmation" required  Placeholder="Confirm Password"/>
			</div>
			<div class="formrow remember turmslink clear">
				<div class="rememberbox new">
					<label>
						<input type="checkbox" id="terms_of_services" name="terms_of_services" required value="1" />
						<span class="checkbox"></span>
						<span class="label">I agree with <a href="#">Terms of services</a> and <a href="#">Privacy Policy</a></span>
					</label>
				</div>
			</div>
			<div class="formrow loginbutton signupbtn clear">
				<input type="submit" value="Signup Now" class="memberlogin">
			</div>
		</form>
	</div>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('customScript'); ?>
<script>var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
if (isSafari){
	$(document).on('submit',"#loginForm",function(e){
		var sflg=0;
		if($("#email").val()=='')
		{
			sflg=1;
			alert("Please fill out this field");
			$("#email").focus();
			
		}else if($("#password").val()=='')		
		{
			sflg=1;
			alert("Please fill out this field");
			$("#password").focus();
		}else if($("#password_confirmation").val()=='')
		{
			sflg=1;
			alert("Please fill out this field");
			$("#password_confirmation").focus();		
		}else if($("#terms_of_services").is(':checked')==false)		
		{
			sflg=1;
			alert("Please fill out this field");
			$("#terms_of_services").focus();		
		}
		/* alert(sflg); */
		if(sflg==0){
			$("#loginForm").submit();		
		}else	e.preventDefault();
	});
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.basic', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>