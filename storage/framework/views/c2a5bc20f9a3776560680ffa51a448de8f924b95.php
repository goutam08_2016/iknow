<?php $__env->startSection('pageTitle', 'Welcome to '); ?>

<?php $__env->startSection('customStyle'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    
    <!--main content open-->
    <section class="testquizbody">
    	<div class="testpaper-wrap">
        	<h2><?php echo e($test->title); ?></h2>
            <div class="test_container-intro">
            	<h3>Instructions:</h3>
                <ul>
                    <li>Number of questions:  <strong><?php echo e(!empty($test->questions)?count($test->questions):0); ?></strong></li>
                    <li>Has <?php echo e($test->time_limit >0 ? $test->time_limit.' minutes':' no'); ?> time limit</li>
                    <li>Must be finished in one sitting. You cannot save and finish later.</li>
                    <li>Questions displayed per page: <strong>1</strong></li>
                    <li>Will allow you to go back and change your answers.</li>
                    <li>Will not let you finish with any questions unattended.</li>
                </ul>
                <div class="testbtnrow">
                    <a class="colorbtn" href="<?php echo e(url('user/start-test/'.$test->id)); ?>">Continue</a>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.basic', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>