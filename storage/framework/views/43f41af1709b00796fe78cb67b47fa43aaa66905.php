<?php $__env->startSection('pageTitle', 'Welcome to '); ?>
<?php $__env->startSection('customStyle'); ?>
<link href="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.css'); ?>" rel="stylesheet" type="text/css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	<?php echo $client_header; ?>

    	 
    <!--main content open-->
    	<section class="testpage">			<?php if(session('cat_success')): ?>						<div class="success_message">			<p><?php echo session('cat_success'); ?></p>		</div>		<?php session()->forget('cat_success');?>			<?php endif; ?>				    			<div class="deta-search">			<div class="wrapper clear">				<!--<div class="search-box groupsearch clear"><form name="search" method="post" action="#"><div class="formcol"><input type="text" placeholder="Search Group Name"><button type="submit" value="search"><i class="fa fa-search" aria-hidden="true"></i></button></div></form></div>-->				<a href="#qscategory" class="colorbtn bluebtn newtest qspopup">+ &nbsp; Add Question</a>				<div class="qscatagorypopup" id="qscategory">					<div class="qswrap">						<h2>Which kind of question you like to add ?</h2>						<ul>							<li>								<a href="<?php echo url('user/add-question/1'); ?>">Open question</a>							</li>							<li>								<a href="<?php echo url('user/add-question/2'); ?>">Multiple question</a>							</li>							<li>								<a href="<?php echo url('user/add-question/3'); ?>">Multiple choice question</a>							</li>							<li>								<a href="<?php echo url('user/add-question/4'); ?>">Fill the gap</a>							</li>							<li>								<a href="<?php echo url('user/add-question/5'); ?>">Connect</a>							</li>							<li>								<a href="<?php echo url('user/add-question/6'); ?>">Question like: rate in points</a>							</li>							<li>								<a href="<?php echo url('user/add-question/7'); ?>">Organize in order</a>							</li>						</ul>					</div>				</div>			</div>		</div>		<div class="test-deta">			<div class="wrapper">			<?php if(session('success')): ?>									<div class="success_message">					<p><?php echo session('success'); ?></p>				</div>				<?php session()->forget('success');?>					<?php endif; ?>       							<table class="detatable qstable">					<?php if(!empty($questions)): ?>						<?php foreach($questions as $ky=>$val): ?>                    					<tr>						<td class="detacol qsdeta">							<p class="ttl"><?php echo $ky+1; ?>) <?php echo $val->question==''?$qsn_type_arr[$val->question_type]:strip_tags($val->question); ?></p>						</td>						<td class="detacol ansdeta">							<a href="#">								<img src="<?php echo asset('assets/frontend/images/ans.png'); ?>" alt="">								<span>Answer</span>							</a>						</td>						<td class="detacol dupdeta">							<a href="#">								<img src="<?php echo asset('assets/frontend/images/dup.png'); ?>" alt="">								<span>Duplicate</span>							</a>						</td>						<td class="detacol assigndeta">							<a href="#">								<img src="<?php echo asset('assets/frontend/images/assign.png'); ?>" alt="">									<span>Assign</span>							</a>						</td>						<td class="detacol editdeta">							<a href="<?php echo e(url('user/add-question/'.$val->question_type.'/'.$val->id)); ?>">								<img src="<?php echo asset('assets/frontend/images/new-file.png'); ?>" alt="">									<span>Edit</span>							</a>						</td>						<td class="detacol removedeta">							<a href="javascript:void(0);" class="rmve">								<img src="<?php echo asset('assets/frontend/images/remove.png'); ?>" alt="">								<span>Remove</span>
								<input type="hidden" value="<?php echo e($val->id); ?>"/>							</a>						</td>					</tr>					<?php endforeach; ?>						<?php endif; ?>                				</table>			</div>		</div>	</section>
    <!--main content close-->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('customScript'); ?>
<script src="<?php echo asset('assets/frontend/js/stacktable.min.js'); ?>"></script>
<script src="<?php echo asset('assets/frontend/lightbox/jquery.fancybox.js'); ?>"></script>

<script>$(document).ready(function() {    $('.qstable').cardtable();	$('.qspopup').fancybox({		padding:0	});});


$(".rmve").click(function() {

	var obj= $(this);

	//console.log(obj.parent().parent().find('.ttl').html());

	var txt = obj.parent().parent().find('.ttl').text();

	var gpId= obj.children('input').val();

	var r = confirm("Are you sure to delete Question "+txt+"?");

	if (r == true) {

		$.ajax({

			type:"post",

			url: "<?php echo url('user/del-question'); ?>" ,

			headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},

			data: {id: gpId},

			//dataType: "json",

			success:function(data) {

				//console.log(data);

				obj.parent().parent().remove();

			}

		});

	}	

	

});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>