<?php
//use App\Helpers\CustomHelper;
?>
<?php if(isset($chat_messsage_data['chat_msg_arr']) && count($chat_messsage_data['chat_msg_arr'])): ?>
    <?php foreach($chat_messsage_data['chat_msg_arr'] as $msg): ?>
        <?php if($msg->from_user_id == $chat_messsage_data['from_user_id']): ?>
            <li class="list meChat">
                <span class="name" style="display:block;"><a href="<?php echo url('profile/'.$msg->fromUser->id); ?>" target="_blank"><?php echo $msg->fromUser->first_name; ?> <?php echo $msg->fromUser->last_name; ?></a></span>
               <span class="time"><?php echo time_elapsed_string(strtotime($msg->created_at)); ?> ago</span>
                <?php if($msg->type == 'T'): ?>
					<p><?php echo $msg->message; ?></p>
				<?php else: ?>
					<?php if(in_array($msg->file_ext,array('jpg','JPG','JPEG','png','PNG','bmp','BMP','gif','GIF'))): ?>
						<p><a href="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>" class="chat_pic_msg" ><img style="width:120px;" src="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>"/></a></p>
					<?php else: ?>
						<p><a href="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>" download ><?php echo $msg->message; ?></a></p>
					<?php endif; ?>
				<?php endif; ?>                                                
            </li>
        <?php else: ?>
            <li class="list userChat">
               <span class="name" style="display:block;"><a href="<?php echo url('profile/'.$msg->fromUser->id); ?>" target="_blank"><?php echo $msg->fromUser->first_name; ?> <?php echo $msg->fromUser->last_name; ?></a></span>
                <span class="time"><?php echo time_elapsed_string(strtotime($msg->created_at)); ?> ago</span>
                <?php if($msg->type == 'T'): ?>
					<p><?php echo $msg->message; ?></p>
				<?php else: ?>
					<?php if(in_array($msg->file_ext,array('jpg','JPG','JPEG','png','PNG','bmp','BMP','gif','GIF'))): ?>
						<p><a href="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>" class="chat_pic_msg" ><img style="width:120px;" src="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>"/></a></p>
					<?php else: ?>
						<p><a href="<?php echo asset('assets/upload/chat_files/'.$msg->file_name); ?>" download ><?php echo $msg->message; ?></a></p>
					<?php endif; ?>
				<?php endif; ?>                                               
            </li>
        <?php endif; ?>
    <?php endforeach; ?>
</ul>
<?php endif; ?>    