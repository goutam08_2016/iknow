<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */
$resource = 'user';
$resource_pl = str_plural($resource);
$page_title = ucfirst($resource);
// Resolute the data
$data = ${$resource};

?>



<?php $__env->startSection('pageTitle', $page_title); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $page_title; ?>
            <small>Edit <?php echo $data->firstname; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="<?php echo url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="<?php echo url("admin/$resource"); ?>"><i class="fa  fa-user"></i> <?php echo ucfirst($resource_pl); ?></a></li>
            <li class="active">Edit User</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit User</h3>
                    </div>
                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger">
                            <?php foreach($errors->all() as $error): ?>
                                <p><?php echo $error; ?></p>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <?php if(session('success')): ?>
                    <div class="alert alert-success">
                        <?php echo session('success'); ?>
                    </div>
                    <?php endif; ?>
                    <form method="POST" action="<?php echo admin_url('user/' . ${$resource}->id); ?>" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PATCH"> <?php echo e(csrf_field()); ?>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="first_name" class="col-sm-2 control-label">First Name</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Name" value="<?php echo $data->first_name; ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="last_name" class="col-sm-2 control-label">Last Name</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Name" value="<?php echo $data->last_name; ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-6">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $data->email; ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address" class="col-sm-2 control-label">Last Name</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="address" name="address" placeholder="Name" value="<?php echo $data->address; ?>" />
                                </div>
                            </div>


                            <?php /*
                            <div class="form-group">
                                <label for="status" class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-10">
                                    <label>
                                        <input type="radio" name="status" id="optionsRadios1" value="Y" {!! $data->status=='Y'?'checked':'' !!} >
                                        Active
                                    </label>
                                    <label>
                                        <input type="radio" name="status" id="optionsRadios2" value="N" {!! $data->status=='N'?'checked':'' !!} />
                                        Block
                                    </label>
                                    @if($data->status=='E')
                                        <label>
                                            <input type="radio" name="status" id="optionsRadios3" value="E" {!! $data->status=='E'?'checked':'' !!} />
                                            Pending Activation
                                        </label>
                                    @endif
                                </div>
                            </div>
                            */ ?>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a class="btn btn-default" href="<?php echo url("admin/$resource"); ?>">
                                Back</a>
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </div><!-- /.box-footer -->
                        </form>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>