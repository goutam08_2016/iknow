<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */
$resource = 'invoice';
$resource_pl = 'invoices';
$page_title = ucfirst($resource_pl);
?>



<?php $__env->startSection('pageTitle', 'Dashboard'); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo e($page_title); ?>

            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="<?php echo url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><?php echo e($page_title); ?></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All <?php echo e($resource); ?> list</h3>
						<a style="float:right !important" href="<?php echo e(admin_url('user/'.$user->id.'/add-invoice')); ?>" class="btn btn-sm btn-warning td-btn">Add <?php echo e($resource); ?></a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <?php if(session('success')): ?>
                            <div class="alert alert-success">
                                <?php echo session('success'); ?>

                            </div>
                        <?php endif; ?>
                        <table id="list_table" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Plan</th>
                                <th>Amount</th>
                                <th>Invoice Date</th>
                                <th>Expired On </th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(${$resource_pl} !== null): ?>
                                <?php foreach($invoices as $row): ?>
								<?php 
								if(!empty($row))
								{
									$startd = strtotime($row->pivot->created_at);
									$exp = ($row->duration * (30)*86400);
									$expd = (int)$startd + $exp;
								}
								?>
                                    <tr>
                                        <td><?php echo $row->plan; ?></td>
                                        <td>$<?php echo $row->price; ?></td>
                                        <td><?php echo e(date('d M,Y',$startd)); ?></td>
                                        <td><?php echo e(date('d M,Y',$expd)); ?></td>
                                        <td><?php echo e($row->pivot->valid == 'Y'?'Current':'Expired'); ?></td>
                                        <?php
                                        /*<td>
                                            <select  class="status">
                                                <option data-id="{{ $row->id }}"  class="bg-green-active color-palette" value="Y" {{ $row->status == 'Y'?'selected':''  }}>Active</option>
                                                <option data-id="{{ $row->id }}" class="bg-red-active color-palette" value="N" {{ $row->status == 'N'?'selected':''  }}>Block</option>
                                                <option  data-id="{{ $row->id }}" class="bg-orange-active color-palette" value="E" {{ $row->status == 'E'?'selected':''  }}>Pending</option>
                                            </select>
                                        </td> */ ?>
                                        <td>
										<a href="<?php echo e(admin_url('user/'.$row->pivot->user_id.'/invoice-details/'.$row->pivot->id)); ?>" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> View</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="5">No user Found..</td>
                                </tr>
                            <?php endif; ?>

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                    <!-- <div class="paginationDiv">
					{{${$resource_pl}->render() } }
                    </div> -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
<script type="text/javascript">
    $(function () {
        $("#list_table").DataTable({
            "paging":   false,
            "ordering": false
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>