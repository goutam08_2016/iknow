<?php
$loguser = Auth::User();
if($loguser):
?>
<div id="tab1" class="allUserTabtab_view online_userchat_right_panel">
	<span class="ttl">All Users</span>
    <ul class="allFriendsList">
    <?php if(count($chat_messsage_data['chat_users']) > 0): ?>
        <?php foreach($chat_messsage_data['chat_users'] as $cht_usr): ?>
            <?php if($cht_usr->from_user_id == $loguser->id ): ?>				
				<li>
					<a href="javascript:void(0);" onclick="startChatPop('<?php echo $cht_usr->to_user->id; ?>', 0);">
						<span class="image">
							<?php if($cht_usr->to_user->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$cht_usr->to_user->profile_image)): ?>
								<img src="<?php echo asset('assets/upload/profile_pictures/'.$cht_usr->to_user->profile_image); ?>" alt="<?php echo $cht_usr->to_user->first_name; ?> <?php echo $cht_usr->to_user->last_name; ?>"/>
							<?php else: ?>
								<img src="<?php echo asset('assets/common/images/default_avatar.jpg'); ?>" alt="<?php echo $cht_usr->to_user->first_name; ?> <?php echo $cht_usr->to_user->last_name; ?>"/>
							<?php endif; ?>
						</span>
						<span class="name"><?php echo $cht_usr->to_user->first_name; ?> <?php echo $cht_usr->to_user->last_name; ?></span>
						<span class="status online"></span>
					</a>
				</li>
            <?php elseif($cht_usr->to_user_id == $loguser->id ): ?>				
				<li>
					<a href="javascript:void(0);" onclick="startChatPop('<?php echo $cht_usr->form_user->id; ?>', 0);">
						<span class="image">
							<?php if($cht_usr->form_user->profile_image != '' && file_exists('assets/upload/profile_pictures/'.$cht_usr->form_user->profile_image)): ?>
								<img src="<?php echo asset('assets/upload/profile_pictures/'.$cht_usr->form_user->profile_image); ?>" alt="<?php echo $cht_usr->form_user->first_name; ?> <?php echo $cht_usr->form_user->last_name; ?>"/>
							<?php else: ?>
								<img src="<?php echo asset('assets/common/images/default_avatar.jpg'); ?>" alt="<?php echo $cht_usr->form_user->first_name; ?> <?php echo $cht_usr->form_user->last_name; ?>"/>
							<?php endif; ?>
						</span>
						<span class="name"><?php echo $cht_usr->form_user->first_name; ?> <?php echo $cht_usr->form_user->last_name; ?></span>
						<span class="status online"></span>
					</a>
				</li>
					
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
    </ul>
</div>
<?php
endif;
?>