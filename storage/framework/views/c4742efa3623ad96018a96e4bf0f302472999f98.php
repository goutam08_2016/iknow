<?php if(count($user->contact_ifno)>0): ?>
	<?php foreach($user->contact_ifno as $val): ?>
	<form class="contactinfofrm">
		<div class="linkAddress viwSmlAdd">
			<?php if(Auth::check() && $user->id == Auth::user()->id): ?> 
			<a href="javascript:void(0)" class="cnEdt rnvinfo"><img src="<?php echo asset('assets/frontend'); ?>/images/cancel.png" alt=""></a>
			<a href="javascript:void(0)" class="edtEml"><img src="<?php echo asset('assets/frontend'); ?>/images/edit.png" alt=""></a>
			<?php endif; ?>
			<ul>
				<li>
					<?php if($val->info1): ?>
					<div class="textE clear"><img src="<?php echo asset('assets/frontend'); ?>/images/gmail.png" alt=""> <?php echo e($val->info1); ?></div>
					<?php endif; ?>
					<div class="contactEdit clear">
						<img src="<?php echo asset('assets/frontend'); ?>/images/gmail.png" alt=""><strong>*</strong><input name="info1" type="email" value="<?php echo e($val->info1); ?>"/>
					</div>
				</li>
				<li>
					<?php if($val->info2): ?>
					<div class="textE clear"><img src="<?php echo asset('assets/frontend'); ?>/images/message.png" alt=""> <?php echo e($val->info2); ?></div>
					<?php endif; ?>
					<div class="contactEdit clear">
						<img src="<?php echo asset('assets/frontend'); ?>/images/message.png" alt=""><input name="info2" type="text" value="<?php echo e($val->info2); ?>"/>
					</div>
				</li>
				<li>
					<?php if($val->info3): ?>
					<div class="textE clear"><img src="<?php echo asset('assets/frontend'); ?>/images/fb1.png" alt=""> <?php echo e($val->info3); ?></div>
					<?php endif; ?>
					<div class="contactEdit clear">
						<img src="<?php echo asset('assets/frontend'); ?>/images/fb1.png" alt=""><input name="info3" type="text" value="<?php echo e($val->info3); ?>"/>
					</div>
				</li>
				<li>
					<?php if($val->info4): ?>
					<div class="textE clear"><img src="<?php echo asset('assets/frontend'); ?>/images/skype.png" alt=""><?php echo e($val->info4); ?></div>
					<?php endif; ?>
					<div class="contactEdit clear">
						<img src="<?php echo asset('assets/frontend'); ?>/images/skype.png" alt=""><input name="info4" type="text" value="<?php echo e($val->info4); ?>">
					</div>
				</li>
			</ul>
			<div class="textfld btnRow">
				<a href="javascript:void(0)" class="cnclBtn">Cancel</a>
				<input type="submit" value="Save" class="bluebtn">
			</div>									
				<input type="hidden" name="info_id" value="<?php echo e($val->id); ?>"/>
		</div>
	</form>
	<?php endforeach; ?>
<?php endif; ?>