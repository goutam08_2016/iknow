<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */
$resource = 'package';
$resource_pl = str_plural($resource);
$page_title = ucfirst(str_replace('_', ' ', $resource));
$page_title_pl = ucfirst(str_replace('_', ' ', $resource_pl));
// Localization data...
$locale = app()->getLocale();

// Resolute the data...
$data = ${$resource_pl};
//dd($data);

?>



<?php $__env->startSection('pageTitle', 'Edit ' . $page_title); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $page_title; ?>

            <small>Edit <?php echo $data->firstname; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="<?php echo url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="<?php echo url("admin/$resource"); ?>"><i class="fa  fa-user"></i> <?php echo $page_title_pl; ?></a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit <?php echo e($page_title); ?></h3>
                    </div>
                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger">
                            <?php foreach($errors->all() as $error): ?>
                                <p><?php echo $error; ?></p>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <?php if(session('success')): ?>
                    <div class="alert alert-success">
                        <?php echo session('success'); ?>

                    </div>
                    <?php endif; ?>
                    <form method="POST" action="<?php echo admin_url($resource . '/' . ${$resource_pl}->id); ?>" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PATCH"> <?php echo e(csrf_field()); ?>

                        <div class="box-body">
                            <div class="form-group">
                                    <label for="plan" class="col-sm-2 control-label">Plan </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="plan" name="plan" placeholder="title" value="<?php echo $data->plan; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="duration" class="col-sm-2 control-label">Duration </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="duration" name="duration" placeholder="month" value="<?php echo $data->duration; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="price" class="col-sm-2 control-label">Price </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="price" name="price" placeholder="price" value="<?php echo $data->price; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="details" class="col-sm-2 control-label">Package details </label>
                                    <div class="col-sm-6">
                                        <textarea style="height: 200px; resize: none;" type="text" class="form-control" id="details" name="details" placeholder="content"><?php echo $data->details; ?></textarea>
                                    </div>
                                </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a class="btn btn-default" href="<?php echo url('admin/$resource'); ?>">
                                Back</a>
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </div><!-- /.box-footer -->
                        </form>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>