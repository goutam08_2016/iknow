<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */
$resource = 'email_template';
$resource_pl = str_plural($resource);
$page_title = ucfirst(str_replace('_', ' ', $resource));
$page_title_pl = ucfirst(str_replace('_', ' ', $resource_pl));
// Localization data...
$locale = app()->getLocale();

// Resolute the data...
$data = ${$resource_pl};
//dd($data);

?>



<?php $__env->startSection('pageTitle', 'Edit ' . $page_title); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $page_title; ?>

            <small>Edit <?php echo $data->firstname; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="<?php echo url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="<?php echo url("admin/$resource"); ?>"><i class="fa  fa-user"></i> <?php echo $page_title_pl; ?></a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit <?php echo e($page_title); ?></h3>
                    </div>
                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger">
                            <?php foreach($errors->all() as $error): ?>
                                <p><?php echo $error; ?></p>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <?php if(session('success')): ?>
                    <div class="alert alert-success">
                        <?php echo session('success'); ?>

                    </div>
                    <?php endif; ?>
					
					<select size="10" style="font-size:11px;width:20%">
						<option>{USER_NAME} User Name</option>											
						<option>{ACTIVATION_LINK} Activation Link</option>	
						<option>{SITE_URL} Site URL</option>	
						<option>{CONTACT_MAIL} Site URL</option>
					</select>
                    <form method="POST" action="<?php echo admin_url('email-template/' . ${$resource_pl}->id); ?>" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PATCH"> <?php echo e(csrf_field()); ?>

                        <div class="box-body">
                            <div class="form-group">
								<label for="email_title" class="col-sm-2 control-label">Title </label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="email_title" name="email_title" placeholder="title" value="<?php echo $data->email_title; ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="description" class="col-sm-2 control-label">Description </label>
								<div class="col-sm-6">
									<textarea class="form-control ckeditor" id="description" name="description" placeholder="description" ><?php echo $data->description; ?></textarea>
								</div>
							</div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a class="btn btn-default" href="<?php echo url('admin/email-template'); ?>">
                                Back</a>
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </div><!-- /.box-footer -->
					</form>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customScript'); ?>
<script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>