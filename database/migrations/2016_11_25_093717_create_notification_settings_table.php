<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('notification_settings', function (Blueprint $table) {
            $table->increments('id');			
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');		
            $table->enum('answer_question',['Y','N'])->default('Y')->comment = 'Y = Yes, N = No';		
            $table->enum('upvote',['Y','N'])->default('Y')->comment = 'Y = Yes, N = No';			
            $table->enum('new_follower',['Y','N'])->default('Y')->comment = 'Y = Yes, N = No';		
            $table->enum('new_nessage',['Y','N'])->default('Y')->comment = 'Y = Yes, N = No';	
            $table->enum('tag_me',['Y','N'])->default('Y')->comment = 'Y = Yes, N = No';		
            $table->enum('comment_my',['Y','N'])->default('Y')->comment = 'Y = Yes, N = No';
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('notification_settings');
    }
}
