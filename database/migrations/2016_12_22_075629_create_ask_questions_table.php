<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAskQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ask_questions', function (Blueprint $table) {
            $table->increments('id');			
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('title',255);	
            $table->string('to',255);		
            $table->text('content');			
            $table->integer('post_type')->unsigned()->index()->default('1')->comment = '1 = public , 2 = private ';	
            $table->string('tags',255);					
            $table->string('question_attached',255);					
            $table->enum('send_as_anonymous',['Y','N'])->default('N')->comment = 'Y = Active, N = Inactive';		
            //$table->enum('status',['Y','N'])->default('Y')->comment = 'Y = Active, N = Inactive';			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ask_questions');
    }
}
