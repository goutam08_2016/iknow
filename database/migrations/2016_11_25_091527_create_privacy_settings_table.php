<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrivacySettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('privacy_settings', function (Blueprint $table) {
            $table->increments('id');			
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');		
            $table->enum('online',['Y','N'])->default('Y')->comment = 'Y = Yes, N = No';		
            $table->enum('other_view_account',['Y','N'])->default('Y')->comment = 'Y = Yes, N = No';	
            $table->enum('want_receive_message',['S','A','N'])->default('A')->comment = 'Y = Specific People, A = Anyone, N = No';			
            $table->enum('decline_request',['Y','N'])->default('N')->comment = 'Y = Yes, N = No';	
            $table->integer('max_request')->unsigned()->index();
            $table->enum('request_intelval',['D','M'])->default('D')->comment = 'D = Per Day, M = Per Month';	
            $table->enum('allow_comments',['Y','N'])->default('N')->comment = 'Y = Yes, N = No';		
            $table->enum('deactivate_account',['Y','N'])->default('N')->comment = 'Y = Yes, N = No';			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('privacy_settings');
    }
}
