<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('user_address', function (Blueprint $table) {
            $table->increments('id');			
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->text('address');			
            $table->integer('type')->unsigned()->index()->default('1')->comment = '1 = address1, 2 = address2 ';			
            //$table->enum('status',['Y','N'])->default('Y')->comment = 'Y = Active, N = Inactive';			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_address');
    }
}
