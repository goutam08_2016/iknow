-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 23, 2017 at 11:33 PM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sabya08_iKnow`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Y','N','E') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'E' COMMENT 'Y = Active, N = Block, E = Pending Activation',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', '$2y$10$3W5Mq9tTnDmG0/n.kvCDoun9TIjWFAFqHwGxNWgspKxis4olhyuGu', 'Y', 'WLkmN1XbT52AlNYFUcJFxyIF1FJngpMAqpOkus3KrDl3v2R1zM3ueam71QmB', NULL, '2017-01-18 17:20:26');

-- --------------------------------------------------------

--
-- Table structure for table `all_tropics`
--

CREATE TABLE IF NOT EXISTS `all_tropics` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `all_tropics`
--

INSERT INTO `all_tropics` (`id`, `title`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 'Street football', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(2, 'Racquet or racket', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(3, 'Interdependent team sports', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(4, 'Water sports', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(5, 'Underwater target shooting', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(6, 'Goal ', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(7, 'Sports that involve teams.', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(8, 'Bat-and-ball', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(9, 'Astronomy', 0, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(10, 'Australian rules football', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(11, 'Auto audiophilia', 2, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(12, 'Auto racing', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(13, 'Backpacking', 0, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(14, 'Badminton', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(15, 'BASE jumping', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(16, 'Baseball', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(17, 'Basketball', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(18, 'Baton twirling', 1, '2016-11-14 18:30:00', '2016-11-28 20:53:17'),
(20, 'Computer Science', 6, '2016-11-28 13:47:21', '2016-11-29 10:00:40'),
(23, 'Badminton', 1, '2016-11-28 15:14:55', '2016-11-29 10:00:06'),
(24, 'IT ', 0, '2016-11-28 15:16:29', '2016-12-11 21:02:31'),
(25, 'Japanese ', 2, '2016-11-28 17:39:19', '2016-11-28 17:39:19'),
(27, 'Finance ', 6, '2016-11-29 09:33:15', '2016-11-29 09:33:15'),
(28, 'Doctor ', 7, '2016-11-29 09:34:20', '2016-11-29 09:34:20'),
(29, 'Auditor ', 7, '2016-11-29 09:35:19', '2016-11-29 09:35:19'),
(30, 'Avatar ', 8, '2016-11-30 07:58:19', '2016-11-30 07:58:19'),
(31, 'VietinBank ', 9, '2016-11-30 10:15:03', '2016-11-30 10:15:03'),
(32, 'Vietnam ', 10, '2016-11-30 10:15:32', '2016-11-30 10:15:32'),
(43, 'kích hoạt thẻ RSA doanh nghiệp', 0, '2017-01-02 22:46:18', '2017-01-02 22:46:18'),
(44, 'Xử lý lỗi điện, giao dịch eFAST', 0, '2017-01-02 22:47:09', '2017-01-02 22:47:09'),
(45, 'chương trình thi đua khách hàng doanh nghiệp', 0, '2017-01-02 22:47:38', '2017-01-02 22:47:38'),
(46, 'chương trình tín dụng khách hàng doanh nghiệp', 0, '2017-01-02 22:47:53', '2017-01-02 22:47:53'),
(47, 'cho vay mua ô tô doanh nghiệp', 0, '2017-01-02 22:48:10', '2017-01-02 22:48:10'),
(48, 'Tiền gửi khách hàng doanh nghiệp', 0, '2017-01-07 21:52:03', '2017-01-07 21:52:03'),
(49, 'Chương trình tín dụng - Khách hàng doanh nghiệp', 0, '2017-01-07 22:28:22', '2017-01-07 22:28:52'),
(50, 'Chương trình thi đua - Khách hàng doanh nghiệp', 0, '2017-01-07 22:29:06', '2017-01-07 22:29:06'),
(51, 'Dịch vụ Ngân hàng điện tử - VietinBank eFAST', 0, '2017-01-07 22:36:54', '2017-01-07 22:36:54');

-- --------------------------------------------------------

--
-- Table structure for table `ask_questions`
--

CREATE TABLE IF NOT EXISTS `ask_questions` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `post_type` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '1 = public , 2 = private ',
  `tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question_attached` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `send_as_anonymous` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Y = Active, N = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ask_questions`
--

INSERT INTO `ask_questions` (`id`, `user_id`, `title`, `to`, `content`, `post_type`, `tags`, `question_attached`, `send_as_anonymous`, `created_at`, `updated_at`) VALUES
(1, 1, 'test qsn', '1,2,17', 'sssss s asas ', 1, 'aaa', '1482496774gTKUElFjLa.jpg', 'Y', '2016-12-23 18:39:34', '2016-12-23 18:39:34'),
(2, 48, 'dfas', '', 'dfasdfds', 2, 'ja', '', 'N', '2016-12-26 23:02:53', '2016-12-26 23:02:53'),
(3, 68, 'Vehicle Godrej', '', 'aasdsa', 2, 'av', '', 'N', '2016-12-27 15:48:19', '2016-12-27 15:48:19'),
(4, 19, 'Test', '22,24,49,50,52,53,61', 'lorem ipsum', 1, 'vietnam, vietinbank', '', 'N', '2016-12-27 17:43:53', '2016-12-27 17:43:53'),
(5, 1, 'check qsn', '', 'test deass c xvvc', 1, '', '', 'N', '2016-12-28 20:18:15', '2016-12-28 20:18:15'),
(6, 1, 'gdgh', '', 'dfgfd g', 1, '', '', 'N', '2016-12-28 20:19:39', '2016-12-28 20:19:39'),
(7, 1, 'Baton twirling', '', 'hfjghj', 1, '', '', 'N', '2016-12-28 20:21:25', '2016-12-28 20:21:25'),
(8, 1, 'sadsd', '', 'sdsd', 1, '', '', 'N', '2016-12-28 20:25:00', '2016-12-28 20:25:00'),
(9, 61, 'test', '1,48', 'test', 1, 'Backpacking,VietinBank', '', 'Y', '2016-12-28 22:52:09', '2016-12-28 22:52:09'),
(10, 19, 'fđsà', '49,52', 'dfsad', 1, 'ebank,thanh toán,efast', '', 'Y', '2016-12-29 11:11:48', '2016-12-29 11:11:48'),
(11, 19, 'dfas', '48,52', 'dfsad', 1, 'Astronomy', '', 'N', '2016-12-29 17:01:26', '2016-12-29 17:01:26'),
(12, 72, 'aaa', '30,31', 'sss asas ass xx', 1, 'Astronomy,Street football', '1483019301YHp2NNYMy7.jpg', 'Y', '2016-12-29 19:48:21', '2016-12-29 19:48:21'),
(13, 48, 'iKnow', '48,49', 'gdf', 1, 'VietinBank', '', 'N', '2016-12-30 23:12:54', '2016-12-30 23:12:54'),
(14, 76, 'Gj', '19,24', 'Tghh', 1, 'Goal', '', 'N', '2016-12-30 23:28:52', '2016-12-30 23:28:52'),
(15, 48, 'dfsdf', '23,48', 'fdas', 1, 'VietinBank', '', 'Y', '2017-01-02 22:24:08', '2017-01-02 22:24:08');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=354 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Công ty Cổ phần Tư vấn thiết kế và Phát triển đô thị (CDDC.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(2, 'Công ty Cổ phần Ánh Dương Việt Nam (Vinasun corp.)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(3, 'Công ty cổ phần Bamboo Capital (BCG)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(4, 'Công ty Cổ phần Bao bì Biên Hòa (SOVI)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(5, 'Công ty Cổ phần Bao bì dầu thực vật (V.Pack )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(6, 'Công ty Cổ phần Bao bì Nhựa Tân Tiến (TAPACK )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(7, 'Công ty Cổ phần Basa (BASACO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(8, 'Công ty Cổ phần Bất động sản Du lịch Ninh Vân Bay (Ninh Van Bay)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(9, 'Công ty cổ phần Bê tông Becamex (BECAMEX ACC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(10, 'Công ty Cổ phần Beton 6 (BT6 Corp)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(11, 'Công ty Cổ phần Bibica (BIBICA)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(12, 'Công ty Cổ phần Bông Bạch Tuyết (COBOVINA)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(13, 'Công ty Cổ phần Bóng đèn Điện Quang (DQ JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(14, 'Công ty Cổ phần Bóng đèn Phích nước Rạng Đông (RALACO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(15, 'Công ty Cổ phần Bột giặt Lix (Lixco)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(16, 'Công ty cổ phần Cảng Cát Lái (Cat Lai Port JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(17, 'Công ty Cổ phần Cảng Đồng Nai (Dong Nai Port)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(18, 'Công ty Cổ phần Cao su Bến Thành (BERUB JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(19, 'Công ty Cổ phần Cao su Đà Nẵng (DRC )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(20, 'Công ty Cổ phần Cao su Đồng Phú (DORUCO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(21, 'Công ty Cổ phần Cao su Hòa Bình (HRC )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(22, 'Công ty cổ phần Cao su Phước Hòa (PHURUCO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(23, 'Công ty Cổ phần Cao Su Sao Vàng (SRC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(24, 'Công ty Cổ phần Cao su Tây Ninh (TANIRUCO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(25, 'Công ty Cổ phần Cao su Thống Nhất (TRC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(26, 'Công ty Cổ phần Cấp nước Chợ Lớn (CHOLON WASUCO JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(27, 'Công ty Cổ phần Cấp nước Thủ Đức (Thu Duc Wasuco.J.S.C)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(28, 'Công ty Cổ phần Cáp Sài Gòn (SCC )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(29, 'Công ty Cổ phần Cáp treo Núi Bà Tây Ninh (CARTOUR )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(30, 'Công ty Cổ phần Cát Lợi (CLC. )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(31, 'Công ty Cổ phần Cavico Việt Nam Khai thác Mỏ và Xây dựng (CVCM.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(32, 'Công ty Cổ phần Chế biến Gỗ Đức Thành (DTWOODVN)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(33, 'Công ty Cổ phần Chế biến Gỗ Thuận An (T.A.C )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(34, 'Công ty Cổ phần Chế biến Hàng xuất khẩu Long An (LAFOOCO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(35, 'Công ty Cổ phần Chế biến Thực phẩm Kinh Đô Miền Bắc (NORTH.KINHDO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(36, 'Công ty Cổ phần Chế biến và Xuất nhập khẩu Thuỷ sản Cà Mau (CAMIMEX CORPORATION)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(37, 'Công ty Cổ phần Chiếu xạ An Phú (API.Co.)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(38, 'Công ty cổ phần Chứng khoán Ngân hàng Đầu tư và Phát triển Việt Nam (BSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(39, 'Công ty Cổ phần Chứng khoán Nông nghiệp và Phát triển Nông thôn (Agriseco)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(40, 'Công ty cổ phần Chứng khoán Sài Gòn (SSI)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(41, 'Công ty Cổ phần Chứng khoán Thành phố Hồ Chí Minh (HSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(42, 'Công ty Cổ phần Chứng khoán Thiên Việt (TVSC.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(43, 'Công ty Cổ phần Chương Dương (Chương Dương ACIC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(44, 'Công ty cổ phần CNG Việt Nam (CNG VietNam)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(45, 'Công ty Cổ phần Cơ điện lạnh (REE Corp )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(46, 'Công ty Cổ phần Cơ điện và Xây dựng Việt Nam (MECO JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(47, 'Công ty Cổ phần COMA18 (Coma18.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(48, 'Công ty Cổ phần Công nghệ Mạng và Truyền thông (INFONET.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(49, 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(50, 'Công ty Cổ phần Công nghệ Tiên Phong (ITD Corporation)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(51, 'Công ty Cổ phần Công nghệ Viễn thông Sài Gòn (SAIGONTEL)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(52, 'Công ty Cổ phần Công nghiệp Cao su Miền Nam (CASUMINA)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(53, 'Công ty Cổ phần Công nghiệp Gốm sứ Taicera (TAICERA )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(54, 'Công ty Cổ phần Công nghiệp Khoáng sản Bình Thuận (BINH THUAN MINERAL)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(55, 'Công ty Cổ phần Công viên nước Đầm Sen (DASECO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(56, 'Công ty Cổ phần Đá Núi Nhỏ (NNC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(57, 'Công ty Cổ phần Đại lý Giao nhận Vận tải Xếp dỡ Tân Cảng (TAN CANG LOGISTICS)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(58, 'Công ty Cổ phần Đại lý Vận tải SAFI (SAFI)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(59, 'Công ty Cổ phần Đại Thiên Lộc (DTLS)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(60, 'Công ty Cổ phần Dầu Thực vật Tường An (Dầu Tường An )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(61, 'Công ty Cổ phần Đầu tư - Kinh doanh nhà (INTRESCO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(62, 'Công ty Cổ phần Đầu tư Alphanam (Alphanam JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(63, 'Công ty cổ phần Đầu tư AMD Group (AMD GROUP.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(64, 'Công ty cổ phần Đầu tư Căn nhà Mơ ước (Dream House Corp)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(65, 'Công ty Cổ phần Đầu tư Cao Su Quảng Nam (Quang Nam Rubber Investment JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(66, 'Công ty Cổ phần Đầu tư Cầu đường CII (CII Bridge & Road)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(67, 'Công ty Cổ phần Đầu tư Địa ốc Khang An (KHANG AN JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(68, 'Công ty Cổ phần Đầu tư Dịch vụ Hoàng Huy (HHS)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(69, 'Công ty Cổ phần Đầu tư Điện Tây Nguyên (TIC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(70, 'Công ty Cổ phần Đầu tư Du lịch và Phát triển Thủy sản (TRISEDCO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(71, 'Công ty cổ phần Đầu tư F.I.T (FIT Investment JSC.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(72, 'Công ty cổ phần Đầu tư Hạ tầng Kỹ thuật T.P Hồ Chí Minh (CII )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(73, 'Công ty Cổ phần Đầu tư Hạ tầng và Đô thị Dầu khí PVC (PVC - PETROLAND JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(74, 'Công ty Cổ phần Đầu tư Kinh doanh nhà Khang Điền (KhadiHouse.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(75, 'Công ty Cổ phần Đầu tư LDG (Dragonland JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(76, 'Công ty Cổ phần Đầu tư Năm Bảy Bảy (NBB Corp)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(77, 'Công ty cổ phần Đầu tư Nam Long (NLG)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(78, 'Công ty Cổ phần Đầu tư Phát triển Công nghệ Điện tử Viễn thông (ELCOM CORP)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(79, 'Công ty Cổ phần Đầu tư Phát triển Công nghiệp - Thương mại Củ Chi (CIDICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(80, 'Công ty Cổ phần Đầu tư Phát triển Cường Thuận IDICO (CTC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(81, 'Công ty Cổ phần Đầu tư Phát triển Đô thị và Khu Công nghiệp Sông Đà (SUDICO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(82, 'Công ty Cổ phần Đầu tư phát triển hạ tầng IDICO (IDICO-IDI.JSC )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(83, 'Công ty Cổ phần Đầu tư Phát triển Nhà và Đô thị Idico (IDICO - UDICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(84, 'Công ty Cổ phần Đầu tư Phát triển Thương mại Viễn Đông (VIDON )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(85, 'Công ty cổ phần Đầu tư Thế giới Di động (MWI CORP)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(86, 'Công ty Cổ phần Đầu tư Thương mại Bất động sản An Dương Thảo Điền (Công ty Cổ phần An Dương Thảo Điền)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(87, 'Công ty Cổ phần Ðầu tư Thương mại SMC (SMC )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(88, 'Công ty Cổ phần Đầu tư Thương mại Thủy Sản (INCOMFISH)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(89, 'Công ty Cổ phần Đầu tư và Công nghiệp Tân Tạo (ITACO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(90, 'Công ty Cổ phần Đầu tư và Dịch vụ Khánh Hội (Khahomex )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(91, 'Công ty cổ phần Đầu tư và Phát triển Cảng Đình Vũ (Dinh Vu Port J.S.C)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(92, 'Công ty Cổ phần Đầu tư và Phát triển Đa Quốc Gia I.D.I (IDI)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(93, 'Công ty cổ phần Đầu tư và Phát triển Đô thị Dầu khí Cửu Long (PVCL)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(94, 'Công ty cổ phần Đầu tư và Phát triển Đô thị Long Giang (Long Giang Land)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(95, 'Công ty Cổ phần Đầu tư và Phát triển Dự án Hạ tầng Thái Bình Dương (PPI JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(96, 'Công ty Cổ phần Đầu tư và Phát triển KSH (HAMICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(97, 'Công ty Cổ phần Đầu tư và Phát triển Nhà đất COTEC (COTECLAND JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(98, 'Công ty Cổ phần Đầu tư và Phát triển Sacom (SACOM )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(99, 'Công ty cổ phần Đầu tư và Sản xuất Thống Nhất (Công ty cổ phần Đầu tư và Sản Xuất Thống Nhất)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(100, 'Công ty Cổ phần Đầu tư và Thương mại DIC (DIC- INTRACO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(101, 'Công ty Cổ phần Đầu tư và Xây dựng Bưu điện (PTIC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(102, 'Công ty Cổ phần Đầu tư và Xây dựng Cấp thoát nước (WASECO.)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(103, 'Công ty Cổ phần Đầu tư và Xây dựng HUD1 (HUD1.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(104, 'Công ty cổ phần Đầu tư và Xây dựng HUD3 (HUD3)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(105, 'Công ty Cổ phần Đầu tư và Xây dựng Tiền Giang (TICCO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(106, 'Công ty Cổ phần Đầu tư Xây dựng 3-2 (CIC3-2)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(107, 'Công ty Cổ phần Đầu tư Xây dựng Bình Chánh (BCCI)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(108, 'Công ty cổ phần Đầu tư Xây dựng Thương mại Dầu khí-IDICO (PVC-IDICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(109, 'Công ty cổ phần Dây Cáp Điện Việt Nam (CADIVI)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(110, 'Công ty Cổ phần Dây và Cáp điện Taya Việt Nam (TAYA VIỆT NAM )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(111, 'Công ty Cổ phần Đệ Tam (DETACO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(112, 'Công ty Cổ phần Dệt may - Đầu tư - Thương mại Thành Công (TCG)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(113, 'Công ty Cổ phần Dịch vụ Hàng hóa Nội Bài (NCTS)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(114, 'Công ty Cổ phần Dịch vụ Ô tô Hàng Xanh (HAXACO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(115, 'Công ty Cổ phần Dịch vụ tổng hợp Sài Gòn (SAVICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(116, 'Công ty Cổ phần Dịch vụ và Xây dựng Địa ốc Đất Xanh (ĐẤT XANH)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(117, 'Công ty Cổ phần Điện lực Dầu khí Nhơn Trạch 2 (PV Power NT2)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(118, 'Công ty Cổ phần Điện lực Khánh Hòa (KHPC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(119, 'Công ty Cổ phần Docimexco (Docimexco)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(120, 'Công ty Cổ phần Đông Hải Bến Tre (DOHACO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(121, 'Công ty cổ phần Du lịch - Dịch vụ Hội An (HOIAN TOURIST SERVICE Co)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(122, 'Công ty Cổ phần Du lịch Thành Thành Công (Thanh Thanh Cong Tourist Joint Stock Company)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(123, 'Công ty Cổ phần Dược Hậu Giang (DHG)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(124, 'Công ty Cổ phần Dược phẩm Cửu Long (PHARIMEXCO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(125, 'Công ty Cổ phần Dược phẩm IMEXPHARM (IMEXPHARM )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(126, 'Công ty cổ phần Dược phẩm OPC (OPC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(127, 'Công ty Cổ phần Dược phẩm Viễn Đông (VIEN DONG PHARMA JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(128, 'Công ty Cổ phần Đường Biên Hoà (BSJC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(129, 'Công ty Cổ phần Đường Ninh Hòa (NSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(130, 'Công ty cổ phần Everpia Việt Nam (Everpia)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(131, 'Công ty Cổ phần FPT (FPT Corp)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(132, 'Công ty Cổ phần Full Power (FULL POWER JS CO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(133, 'Công ty Cổ phần Gạch men Chang Yih (Chang Yih )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(134, 'Công ty Cổ phần Gemadept (Gemadept )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(135, 'Công ty Cổ phần Giống cây trồng Miền Nam (SSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(136, 'Công ty cổ phần Giống cây trồng Trung ương (NSC )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(137, 'Công ty Cổ phần Gò Đàng (GODACO_SEAFOOD)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(138, 'Công ty cổ phần Hạ tầng nước Sài Gòn (SII JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(139, 'Công ty Cổ phần HACISCO (HACISCO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(140, 'Công ty Cổ phần Hóa - Dược phẩm MEKOPHAR (MEKOPHAR)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(141, 'Công ty Cổ phần Hóa An (DHA )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(142, 'Công ty Cổ phần Hóa chất Cơ bản miền Nam (SBCC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(143, 'Công ty Cổ phần Hoàng Anh Gia Lai (HAGL)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(144, 'Công ty Cổ phần Hợp tác kinh tế và Xuất nhập khẩu SAVIMEX (SAVIMEX )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(145, 'Công ty Cổ phần Hùng Vương (HV Corp)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(146, 'Công ty cổ phần In và Bao bì Mỹ Châu (MCP )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(147, 'Công ty Cổ phần Kết cấu Kim loại và Lắp máy Dầu khí (PVC-MS)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(148, 'Công ty Cổ phần Khai thác và Chế biến Khoáng sản Bắc Giang (BAC GIANG EXPLOITABLE.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(149, 'Công ty Cổ phần Khai thác và Chế biến Khoáng sản Lào Cai (LAOCAI JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(150, 'Công ty Cổ phần Kho vận Miền Nam (Sotrans)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(151, 'Công ty cổ phần Khoáng sản Bình Định (BIMICO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(152, 'Công ty cổ phần Khoáng sản FECON (Fecon Mining)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(153, 'Công ty Cổ phần Khoáng sản Na Rì Hamico (NA RI HAMICO JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(154, 'Công ty Cổ phần Khoáng sản và Vật liệu xây dựng Lâm Đồng (LBM )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(155, 'Công ty Cổ phần Khoáng sản và Xây dựng Bình Dương (BIMICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(156, 'Công ty Cổ phần Khử trùng Việt Nam (VFC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(157, 'Công ty Cổ phần Kim khí Thành phố Hồ Chí Minh (HMC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(158, 'Công ty Cổ phần Kinh doanh và Phát triển Bình Dương (TDC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(159, 'Công ty Cổ phần Kỹ nghệ Đô Thành (DTT )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(160, 'Công ty Cổ phần Kỹ Nghệ Lạnh (SEAREFICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(161, 'Công ty cổ phần Kỹ thuật nền móng và Công trình ngầm FECON', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(162, 'Công ty Cổ phần Kỹ thuật và Ô tô Trường Long (Truong Long JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(163, 'Công ty cổ phần LICOGI 16 (LICOGI 16)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(164, 'Công ty cổ phần Lilama 10 (Lilama 10.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(165, 'Công ty Cổ phần Lilama 18 (LILAMA 18 JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(166, 'Công ty cổ phần Logistics Vinalink (VINALINK)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(167, 'Công ty Cổ phần Long Hậu (LHC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(168, 'Công ty Cổ phần Lương thực Thực phẩm Vĩnh Long (VinhlongFood)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(169, 'Công ty Cổ phần MHC (Marina Holdings)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(170, 'Công ty Cổ phần Mía đường Lam Sơn (LASUCO JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(171, 'Công ty Cổ phần Mía đường Nhiệt điện Gia Lai (SEC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(172, 'Công ty cổ phần Mía đường Thành Thành Công Tây Ninh (SBT)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(173, 'Công ty Cổ phần miền Đông', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(174, 'Công ty Cổ phần Mirae (MIRAE.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(175, 'Công ty Cổ phần Nafoods Group (NAFOODS GROUP)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(176, 'Công ty Cổ phần Nam Việt (NAVICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(177, 'Công ty Cổ phần Nam Việt (NAVIFICO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(178, 'Công ty Cổ phần Ngô Han (NHW)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(179, 'Công ty Cổ phần Ngoại thương và Phát triển Đầu tư Thành phố Hồ Chí Minh (FIDECO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(180, 'Công ty Cổ phần Nhà Việt Nam (HVN J.S CO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(181, 'Công ty Cổ phần Cavico Việt Nam Khai thác Mỏ và Xây dựng (CVCM., JSC )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(182, 'Công ty Cổ phần Chứng khoán Thiên Việt (TVSC.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(183, 'Công ty Cổ phần COMA18 (Coma18.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(184, 'Công ty Cổ phần Công nghệ Mạng và Truyền thông (INFONET.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(185, 'Công ty cổ phần Đầu tư AMD Group (AMD GROUP.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(186, 'Công ty cổ phần Đầu tư F.I.T (FIT Investment JSC.,)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(187, 'Công ty Cổ phần Đầu tư Kinh doanh nhà Khang Điền (KhadiHouse., JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(188, 'Công ty Cổ phần Đầu tư và Xây dựng HUD1 (HUD1.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(189, 'Công ty Cổ phần Khai thác và Chế biến Khoáng sản Bắc Giang (BAC GIANG EXPLOITABLE.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(190, 'Công ty cổ phần Lilama 10 (Lilama 10.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(191, 'Công ty Cổ phần Mirae (MIRAE., JSC )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(192, 'Công ty Cổ phần Nhiên liệu Sài Gòn (SFC )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(193, 'Công ty Cổ phần Nhiệt điện Bà Rịa', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(194, 'Công ty Cổ phần Nhiệt điện Phả Lại (PLPC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(195, 'Công ty Cổ phần Nhựa Bình Minh (BMPLASCO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(196, 'Công ty Cổ phần Nhựa Rạng Đông (RDP JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(197, 'Công ty Cổ phần Nhựa Tân Đại Hưng (TAN DAI HUNG PLASTIC J.S. CO.,)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(198, 'Công ty Cổ phần Nông dược H.A.I (HAI jsc)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(199, 'Công ty cổ phần Nông nghiệp Quốc tế Hoàng Anh Gia Lai (HAGL AGRICO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(200, 'Công ty Cổ phần NTACO (NTACO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(201, 'Công ty Cổ phần Nước giải khát Chương Dương (CDBECO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(202, 'Công ty Cổ phần Nước giải khát Sài Gòn (TRIBECO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(203, 'Công ty Cổ phần Ô tô TMT (TMT AUTO., JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(204, 'Công ty cổ phần Phân bón Bình Điền (BFC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(205, 'Công ty Cổ phần Phân bón Dầu khí Cà Mau (PVCFC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(206, 'Công ty Cổ phần Phân bón Miền Nam (SFJC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(207, 'Công ty cổ phần Phân lân nung chảy Văn Điển (VADFCO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(208, 'Công ty Cổ phần Phân phối Khí thấp áp Dầu khí Việt Nam (PV GAS D JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(209, 'Công ty cổ phần Phát triển Bất động sản Phát Đạt', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(210, 'Công ty Cổ phần Phát triển Đô thị Công nghiệp Số 2 (D2D)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(211, 'Công ty Cổ phần Phát triển Đô thị Từ Liêm (LIDECO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(212, 'Công ty Cổ phần Phát triển Hạ tầng Kỹ thuật (BECAMEX IJC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(213, 'Công ty Cổ phần Phát triển nhà Bà Rịa-Vũng Tàu (Hodeco)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(214, 'Công ty Cổ phần Phát triển Nhà Thủ Đức (THUDUC HOUSE )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(215, 'Công ty Cổ phần Phú Tài (Phutaico)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(216, 'Công ty Cổ phần Pin Ắc quy Miền Nam (PINACO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(217, 'Công ty Cổ phần Quốc Cường Gia Lai (QCGL)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(218, 'Công ty Cổ phần Quốc tế Hoàng Gia (Royal International Corp )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(219, 'Công ty cổ phần Quốc tế Sơn Hà (SONHA.,CORP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(220, 'Công ty Cổ phần S.P.M (S.P.M CORP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(221, 'Công ty Cổ phần Sản xuất Kinh doanh Xuất nhập khẩu Bình Thạnh (GILIMEX )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(222, 'Công ty cổ phần Sản xuất Kinh doanh Xuất nhập khẩu Dịch vụ và Đầu tư Tân Bình (Tanimex J.S.C)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(223, 'Công ty Cổ phần Sản xuất Thương mại May Sài Gòn (GARMEX SAIGON JS)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(224, 'Công ty Cổ phần Sản xuất và Thương mại Phúc Tiến (PHT JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(225, 'Công ty Cổ phần Siêu Thanh (SJC )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(226, 'Công ty cổ phần Sợi Thế Kỷ', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(227, 'Công ty cổ phần Sonadezi Long Thành (SZL)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(228, 'Công ty Cổ phần Sông Ba (SBA)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(229, 'Công ty Cổ phần Sữa Việt Nam (VINAMILK )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(230, 'Công ty Cổ phần Tài nguyên (TAI NGUYEN CORP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(231, 'Công ty Cổ phần Tập đoàn Công nghệ CMC (CMC Corp)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(232, 'Công ty cổ phần Tập đoàn Container Việt Nam (Viconship)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(233, 'Công ty Cổ phần Tập đoàn Đại Dương', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(234, 'Công ty Cổ phần Tập đoàn Dầu khí An Pha (AN PHA S.G PETROL JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(235, 'Công ty Cổ phần Tập đoàn Đức Long Gia Lai (DUCLONG GIA LAI GROUP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(236, 'Công ty cổ phần Tập đoàn FLC (FLC., JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(237, 'Công ty Cổ phần Tập đoàn Hà Đô (Hadoco., JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(238, 'Công ty Cổ phần Tập đoàn Hapaco (HAPACO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(239, 'Công ty cổ phần Tập đoàn Hòa Phát (Hoa Phat Group)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(240, 'Công ty Cổ phần Tập đoàn Hoa Sen (HOASEN GROUP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(241, 'Công ty Cổ phần Tập đoàn Hoàng Long (Hoang Long)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(242, 'Công ty Cổ phần Tập đoàn Kido (Kido Group)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(243, 'Công ty Cổ phần Tập đoàn Kỹ nghệ Gỗ Trường Thành (TTFC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(244, 'Công ty Cổ phần Tập đoàn MaSan (Masan Group)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(245, 'Công ty Cổ phần Tập đoàn Nhựa Đông Á (Tập đoàn Đông Á)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(246, 'Công ty Cổ phần Tập đoàn PAN (Pan Pacific)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(247, 'Công ty Cổ phần Tập đoàn Sao Mai (SAOMAI GROUP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(248, 'Công ty Cổ phần Tập đoàn Thép Tiến Lên (TLC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(249, 'Công ty Cổ phần Tập đoàn Thiên Long (Tập đoàn Thiên Long)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(250, 'Công ty Cổ phần Tập đoàn Thủy sản Minh Phú (Minh Phu Seafood Corp)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(251, 'Công ty Cổ phần Tàu cao tốc Superdong – Kiên Giang (Superdong FF-(KG) Joint Stock Company)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(252, 'Công ty cổ phần Thế giới số (DIGIWORLD)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(253, 'Công ty cổ phần Thế kỷ 21 (C21)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(254, 'Công ty Cổ phần Thép Nam Kim (Nakisco)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(255, 'Công ty Cổ phần Thép Pomina (CÔNG TY THÉP POMINA)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(256, 'Công ty Cổ phần Thép Việt Ý (VISCO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(257, 'Công ty Cổ phần Thiết bị Phụ tùng Sài Gòn (SAIGON MACHINCO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(258, 'Công ty cổ phần Thiết bị Y tế Việt Nhật (JV., JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(259, 'Công ty Cổ phần Thuận Thảo (GTT)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(260, 'Công ty Cổ phần Thức ăn Chăn nuôi Việt Thắng (VTFEED)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(261, 'Công ty Cổ phần Thực phẩm Quốc tế (INTERFOOD )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(262, 'Công ty Cổ phần Thực phẩm Sao Ta (Sao Ta Foods Joint Stock Company)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(263, 'Công ty Cổ phần Thương mại - Dịch vụ Bến Thành (BEN THANH TSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(264, 'Công ty cổ phần Thương mại và Khai thác Khoáng sản Dương Hiếu (DH JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(265, 'Công ty Cổ phần Thương mại Xuất nhập khẩu Thiên Nam (TENIMEX )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(266, 'Công ty Cổ phần Thương nghiệp Cà Mau (Camex)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(267, 'Công ty Cổ phần Thủy điện – Điện lực 3 (PC3,HP.Co)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(268, 'Công ty Cổ phần Thủy điện Cần Đơn (CAN DON HSC )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(269, 'Công ty cổ phần Thủy điện Miền Nam (SHP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(270, 'Công ty cổ phần Thủy điện Thác Bà (TBHPC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(271, 'Công ty cổ phần Thủy điện Thác Mơ (TMHPC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(272, 'Công ty Cổ phần Thủy điện Vĩnh Sơn - Sông Hinh (VSHPC )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(273, 'Công ty Cổ phần Thủy hải sản Việt Nhật (VISEA CORP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(274, 'Công ty Cổ phần Thủy Sản Cửu Long (CUULONG SEAPRO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(275, 'Công ty Cổ phần Thủy sản Mekong (MEKONGFISH)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(276, 'Công ty cổ phần Thủy sản số 4 (Seapriexco No 4 )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(277, 'Công ty Cổ phần TIE (TIE)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(278, 'Công ty Cổ phần Transimex-Saigon (Transforwarding Warehousing Joint Stock Corporatio)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(279, 'Công ty Cổ phần TRAPHACO (TRAPHACO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(280, 'Công ty cổ phần Tư vấn Xây dựng Điện 1 (PECC1)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(281, 'Công ty cổ phần Tư vấn-Thương mại-Dịch vụ Địa ốc Hoàng Quân (HOANG QUAN CORP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(282, 'Công ty cổ phần Vận chuyển Sài Gòn Tourist (SATRACO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(283, 'Công ty Cổ phần Văn hóa Phương Nam (PNC )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(284, 'Công ty Cổ phần Vạn Phát Hưng (VPH Corp)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(285, 'Công ty Cổ phần Vận tải biển Việt Nam (VOSCO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(286, 'Công ty Cổ phần Vận tải Biển Vinaship (VINASHIP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(287, 'Công ty Cổ phần Vận tải Đa phương thức Duyên Hải (TASA DUYENHAI)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(288, 'Công ty Cổ phần Vận tải Hà Tiên (HATIEN TRANSCO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(289, 'Công ty cổ phần Vận tải Sản phẩm khí quốc tế (GAS SHIPPING JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(290, 'Công ty Cổ phần Vận tải và Giao nhận bia Sài Gòn (SABETRAN JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(291, 'Công ty Cổ phần Vận tải và Xếp dỡ Hải An (HAIANTS.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(292, 'Công ty Cổ phần Vận tải Xăng dầu Đường thủy Petrolimex (PJTACO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(293, 'Công ty Cổ phần Vận tải Xăng dầu VIPCO (VIPCO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(294, 'Công ty Cổ phần Vận tải Xăng dầu VITACO (VITACO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(295, 'Công ty Cổ phần Vàng bạc Đá quý Phú Nhuận (PNJ)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(296, 'Công ty Cổ phần Vật tư - Xăng dầu (COMECO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(297, 'Công ty Cổ phần Vật tư kỹ thuật Nông nghiệp Cần Thơ (TSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(298, 'Công ty Cổ phần VICEM Vật liệu Xây dựng Đà Nẵng (COXIVA)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(299, 'Công ty Cổ phần Viettronics Tân Bình (VTB )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(300, 'Công ty Cổ phần VinaCafé Biên Hòa (VINACAFÉ BH)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(301, 'Công ty Cổ phần Vĩnh Hoàn (VINHHOAN CORP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(302, 'Công ty Cổ phần Xây dựng 47 (CC47)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(303, 'Công ty Cổ phần Xây dựng Công nghiệp DESCON (DESCON)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(304, 'Công ty Cổ phần Xây dựng công nghiệp và dân dụng Dầu khí (PVC-IC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(305, 'Công ty Cổ phần Xây dựng Cotec (COTECCONS)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(306, 'Công ty Cổ phần Xây dựng số 5 (SC5)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(307, 'Công ty Cổ phần Xây dựng và Giao thông Bình Dương (Becamex BCE)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(308, 'Công ty cổ phần Xây dựng và Kinh doanh Địa ốc Hoà Bình (HoaBinh Corporation)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(309, 'Công ty cổ phần Xây dựng và Phát triển Đô thị tỉnh Bà Rịa-Vũng Tàu (UDEC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(310, 'Công ty Cổ phần Xây lắp Đường ống Bể chứa Dầu khí (PVC – PT)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(311, 'Công ty Cổ phần Xây lắp và Địa ốc Vũng Tàu (VTRECJ. Co.)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(312, 'Công ty Cổ phần Xi Măng Hà Tiên 1 (HA TIEN 1.J.S.CO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(313, 'Công ty Cổ phần Xi măng Hà Tiên 2 (HATIEN2.Co)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(314, 'Công ty Cổ phần Xi măng Vicem Hải Vân (Vicem Hải Vân)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(315, 'Công ty cổ phần Xuất nhập khẩu An Giang (Angimex)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(316, 'Công ty Cổ phần Xuất nhập khẩu Lâm Thủy sản Bến Tre (FAQUIMEX)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(317, 'Công ty Cổ phần Xuất nhập khẩu Petrolimex (PITCO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(318, 'Công ty Cổ phần Xuất nhập khẩu Quảng Bình (QUANG BINH JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(319, 'Công ty Cổ phần Xuất nhập khẩu Thủy sản An Giang (AGIFISH Co. )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(320, 'Công ty Cổ phần Xuất nhập khẩu Thủy sản Bến Tre (AQUATEX BENTRE )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(321, 'Công ty cổ phần Xuất nhập khẩu Thủy sản Cửu Long An Giang (CL - Fish Corp )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(322, 'Công ty Cổ phần Xuất nhập khẩu Y tế Domesco (DOMESCO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(323, 'Công ty cổ phần Y Dược phẩm Vimedimex (Vimedimex)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(324, 'Công ty TNHH Một thành viên Vinpearl (VINPEARL)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(325, 'CTCP Thuốc sát trùng Việt Nam (Vipesco)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(326, 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(327, 'Ngân hàng Thương mại cổ phần Đầu tư và Phát triển Việt Nam (BIDV)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(328, 'Ngân hàng Thương mại cổ phần Ngoại thương Việt Nam (VIETCOMBANK)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(329, 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(330, 'Ngân hàng Thương mại Cổ phần Sài Gòn Thương Tín (SACOMBANK)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(331, 'Ngân hàng Thương mại Cổ phần Xuất nhập khẩu Việt Nam (Vietnam Eximbank)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(332, 'Quỹ Đầu tư Cân bằng Prudential (PRUBF1 )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(333, 'Quỹ Đầu tư Chứng khoán Việt Nam (VF1)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(334, 'Quỹ đầu tư Doanh nghiệp Hàng đầu Việt Nam (Quỹ đầu tư VF4)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(335, 'Quỹ Đầu tư Năng động Việt Nam (VFM)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(336, 'Quỹ đầu tư tăng trưởng ACB (ACBGF)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(337, 'Quỹ đầu tư tăng trưởng Manulife (MAFPF1)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(338, 'Quỹ ETF VFMVN30 (VFMVN30 ETF)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(339, 'Tập đoàn Bảo Việt (TẬP ĐOÀN BẢO VIỆT)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(340, 'Tập đoàn Vingroup - Công ty Cổ phần (VINGROUP JOINT STOCK COMPANY)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(341, 'Tổng Công ty Cổ phần Bảo hiểm Ngân hàng Đầu tư và phát triển Việt Nam (BIC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(342, 'Tổng Công ty cổ phần Bảo hiểm Petrolimex (PJICO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(343, 'Tổng Công ty Cổ phần Bảo Minh (Bao Minh)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(344, 'Tổng Công ty Cổ phần Đầu tư Phát triển Xây dựng (DIC CORP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(345, 'Tổng Công ty Cổ phần Dịch vụ Tổng hợp Dầu khí (PETROSETCO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(346, 'Tổng Công ty Cổ phần Khoan và Dịch vụ Khoan Dầu khí (PV DRILLING )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(347, 'Tổng công ty Cổ phần Vận tải Dầu khí (PV Trans Corp)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(348, 'Tổng công ty Cổ phần Xây dựng điện Việt Nam (VNECO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(349, 'Tổng Công ty Gas Petrolimex-CTCP (PGC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(350, 'Tổng Công ty Khí Việt Nam-CTCP (PVGas)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(351, 'Tổng Công ty Phân bón và Hóa chất Dầu khí-CTCP (PVFCCo)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(352, 'Tổng Công ty Phát triển Đô thị Kinh Bắc-CTCP (KinhBac City)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(353, 'Tổng Công ty Tài chính Cổ phần Dầu khí Việt Nam (PetroVietnam Finance Joint Stock Corporation)', '2017-01-19 22:21:15', '2017-01-19 22:21:15');

-- --------------------------------------------------------

--
-- Table structure for table `contact_ifnos`
--

CREATE TABLE IF NOT EXISTS `contact_ifnos` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `info1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `info2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `info3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `info4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contact_ifnos`
--

INSERT INTO `contact_ifnos` (`id`, `user_id`, `info1`, `info2`, `info3`, `info4`, `created_at`, `updated_at`) VALUES
(1, 1, 'test2@test.com', 'abcsdsd@gmail.com', 'test123', 'sk1212', '2016-12-28 17:29:46', '2016-12-28 17:32:39'),
(3, 1, 'asas@test.com', 'kkk@gmail.com', 'fb42452', 'sk1111', '2016-12-28 17:36:09', '2016-12-28 17:36:09'),
(4, 1, 'ppt@test.com', 'roy@gmail.com', '', '', '2016-12-28 17:37:46', '2016-12-28 17:37:46'),
(7, 72, 's@d.c', 'dd@dd', '', '', '2016-12-29 19:32:28', '2016-12-29 19:32:52'),
(8, 68, 'avoy.php@gmail.com', 'avoy.php@gmail.com', 'facebook.com', 'avoydebnath', '2016-12-29 20:06:37', '2016-12-29 20:06:37'),
(9, 48, '', 'pht11690@gmail.com', 'P.h. Tien', '', '2016-12-30 23:06:31', '2017-01-02 22:32:11'),
(11, 82, 'test04@g', '', 'Linh si', '0987654321', '2017-01-14 22:41:14', '2017-01-14 22:41:14');

-- --------------------------------------------------------

--
-- Table structure for table `contact_messages`
--

CREATE TABLE IF NOT EXISTS `contact_messages` (
  `id` int(10) unsigned NOT NULL,
  `sender_name` varchar(255) NOT NULL,
  `sender_email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `idCountry` int(5) NOT NULL,
  `countryCode` char(2) NOT NULL DEFAULT '',
  `countryName` varchar(45) NOT NULL DEFAULT '',
  `currencyCode` char(3) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`idCountry`, `countryCode`, `countryName`, `currencyCode`) VALUES
(1, 'AD', 'Andorra', 'EUR'),
(2, 'AE', 'United Arab Emirates', 'AED'),
(3, 'AF', 'Afghanistan', 'AFN'),
(4, 'AG', 'Antigua and Barbuda', 'XCD'),
(5, 'AI', 'Anguilla', 'XCD'),
(6, 'AL', 'Albania', 'ALL'),
(7, 'AM', 'Armenia', 'AMD'),
(8, 'AO', 'Angola', 'AOA'),
(9, 'AQ', 'Antarctica', ''),
(10, 'AR', 'Argentina', 'ARS'),
(11, 'AS', 'American Samoa', 'USD'),
(12, 'AT', 'Austria', 'EUR'),
(13, 'AU', 'Australia', 'AUD'),
(14, 'AW', 'Aruba', 'AWG'),
(15, 'AX', 'Aland', 'EUR'),
(16, 'AZ', 'Azerbaijan', 'AZN'),
(17, 'BA', 'Bosnia and Herzegovina', 'BAM'),
(18, 'BB', 'Barbados', 'BBD'),
(19, 'BD', 'Bangladesh', 'BDT'),
(20, 'BE', 'Belgium', 'EUR'),
(21, 'BF', 'Burkina Faso', 'XOF'),
(22, 'BG', 'Bulgaria', 'BGN'),
(23, 'BH', 'Bahrain', 'BHD'),
(24, 'BI', 'Burundi', 'BIF'),
(25, 'BJ', 'Benin', 'XOF'),
(26, 'BL', 'Saint Barthelemy', 'EUR'),
(27, 'BM', 'Bermuda', 'BMD'),
(28, 'BN', 'Brunei', 'BND'),
(29, 'BO', 'Bolivia', 'BOB'),
(30, 'BQ', 'Bonaire', 'USD'),
(31, 'BR', 'Brazil', 'BRL'),
(32, 'BS', 'Bahamas', 'BSD'),
(33, 'BT', 'Bhutan', 'BTN'),
(34, 'BV', 'Bouvet Island', 'NOK'),
(35, 'BW', 'Botswana', 'BWP'),
(36, 'BY', 'Belarus', 'BYR'),
(37, 'BZ', 'Belize', 'BZD'),
(38, 'CA', 'Canada', 'CAD'),
(39, 'CC', 'Cocos [Keeling] Islands', 'AUD'),
(40, 'CD', 'Democratic Republic of the Congo', 'CDF'),
(41, 'CF', 'Central African Republic', 'XAF'),
(42, 'CG', 'Republic of the Congo', 'XAF'),
(43, 'CH', 'Switzerland', 'CHF'),
(44, 'CI', 'Ivory Coast', 'XOF'),
(45, 'CK', 'Cook Islands', 'NZD'),
(46, 'CL', 'Chile', 'CLP'),
(47, 'CM', 'Cameroon', 'XAF'),
(48, 'CN', 'China', 'CNY'),
(49, 'CO', 'Colombia', 'COP'),
(50, 'CR', 'Costa Rica', 'CRC'),
(51, 'CU', 'Cuba', 'CUP'),
(52, 'CV', 'Cape Verde', 'CVE'),
(53, 'CW', 'Curacao', 'ANG'),
(54, 'CX', 'Christmas Island', 'AUD'),
(55, 'CY', 'Cyprus', 'EUR'),
(56, 'CZ', 'Czech Republic', 'CZK'),
(57, 'DE', 'Germany', 'EUR'),
(58, 'DJ', 'Djibouti', 'DJF'),
(59, 'DK', 'Denmark', 'DKK'),
(60, 'DM', 'Dominica', 'XCD'),
(61, 'DO', 'Dominican Republic', 'DOP'),
(62, 'DZ', 'Algeria', 'DZD'),
(63, 'EC', 'Ecuador', 'USD'),
(64, 'EE', 'Estonia', 'EUR'),
(65, 'EG', 'Egypt', 'EGP'),
(66, 'EH', 'Western Sahara', 'MAD'),
(67, 'ER', 'Eritrea', 'ERN'),
(68, 'ES', 'Spain', 'EUR'),
(69, 'ET', 'Ethiopia', 'ETB'),
(70, 'FI', 'Finland', 'EUR'),
(71, 'FJ', 'Fiji', 'FJD'),
(72, 'FK', 'Falkland Islands', 'FKP'),
(73, 'FM', 'Micronesia', 'USD'),
(74, 'FO', 'Faroe Islands', 'DKK'),
(75, 'FR', 'France', 'EUR'),
(76, 'GA', 'Gabon', 'XAF'),
(77, 'GB', 'United Kingdom', 'GBP'),
(78, 'GD', 'Grenada', 'XCD'),
(79, 'GE', 'Georgia', 'GEL'),
(80, 'GF', 'French Guiana', 'EUR'),
(81, 'GG', 'Guernsey', 'GBP'),
(82, 'GH', 'Ghana', 'GHS'),
(83, 'GI', 'Gibraltar', 'GIP'),
(84, 'GL', 'Greenland', 'DKK'),
(85, 'GM', 'Gambia', 'GMD'),
(86, 'GN', 'Guinea', 'GNF'),
(87, 'GP', 'Guadeloupe', 'EUR'),
(88, 'GQ', 'Equatorial Guinea', 'XAF'),
(89, 'GR', 'Greece', 'EUR'),
(90, 'GS', 'South Georgia and the South Sandwich Islands', 'GBP'),
(91, 'GT', 'Guatemala', 'GTQ'),
(92, 'GU', 'Guam', 'USD'),
(93, 'GW', 'Guinea-Bissau', 'XOF'),
(94, 'GY', 'Guyana', 'GYD'),
(95, 'HK', 'Hong Kong', 'HKD'),
(96, 'HM', 'Heard Island and McDonald Islands', 'AUD'),
(97, 'HN', 'Honduras', 'HNL'),
(98, 'HR', 'Croatia', 'HRK'),
(99, 'HT', 'Haiti', 'HTG'),
(100, 'HU', 'Hungary', 'HUF'),
(101, 'ID', 'Indonesia', 'IDR'),
(102, 'IE', 'Ireland', 'EUR'),
(103, 'IL', 'Israel', 'ILS'),
(104, 'IM', 'Isle of Man', 'GBP'),
(105, 'IN', 'India', 'INR'),
(106, 'IO', 'British Indian Ocean Territory', 'USD'),
(107, 'IQ', 'Iraq', 'IQD'),
(108, 'IR', 'Iran', 'IRR'),
(109, 'IS', 'Iceland', 'ISK'),
(110, 'IT', 'Italy', 'EUR'),
(111, 'JE', 'Jersey', 'GBP'),
(112, 'JM', 'Jamaica', 'JMD'),
(113, 'JO', 'Jordan', 'JOD'),
(114, 'JP', 'Japan', 'JPY'),
(115, 'KE', 'Kenya', 'KES'),
(116, 'KG', 'Kyrgyzstan', 'KGS'),
(117, 'KH', 'Cambodia', 'KHR'),
(118, 'KI', 'Kiribati', 'AUD'),
(119, 'KM', 'Comoros', 'KMF'),
(120, 'KN', 'Saint Kitts and Nevis', 'XCD'),
(121, 'KP', 'North Korea', 'KPW'),
(122, 'KR', 'South Korea', 'KRW'),
(123, 'KW', 'Kuwait', 'KWD'),
(124, 'KY', 'Cayman Islands', 'KYD'),
(125, 'KZ', 'Kazakhstan', 'KZT'),
(126, 'LA', 'Laos', 'LAK'),
(127, 'LB', 'Lebanon', 'LBP'),
(128, 'LC', 'Saint Lucia', 'XCD'),
(129, 'LI', 'Liechtenstein', 'CHF'),
(130, 'LK', 'Sri Lanka', 'LKR'),
(131, 'LR', 'Liberia', 'LRD'),
(132, 'LS', 'Lesotho', 'LSL'),
(133, 'LT', 'Lithuania', 'LTL'),
(134, 'LU', 'Luxembourg', 'EUR'),
(135, 'LV', 'Latvia', 'EUR'),
(136, 'LY', 'Libya', 'LYD'),
(137, 'MA', 'Morocco', 'MAD'),
(138, 'MC', 'Monaco', 'EUR'),
(139, 'MD', 'Moldova', 'MDL'),
(140, 'ME', 'Montenegro', 'EUR'),
(141, 'MF', 'Saint Martin', 'EUR'),
(142, 'MG', 'Madagascar', 'MGA'),
(143, 'MH', 'Marshall Islands', 'USD'),
(144, 'MK', 'Macedonia', 'MKD'),
(145, 'ML', 'Mali', 'XOF'),
(146, 'MM', 'Myanmar [Burma]', 'MMK'),
(147, 'MN', 'Mongolia', 'MNT'),
(148, 'MO', 'Macao', 'MOP'),
(149, 'MP', 'Northern Mariana Islands', 'USD'),
(150, 'MQ', 'Martinique', 'EUR'),
(151, 'MR', 'Mauritania', 'MRO'),
(152, 'MS', 'Montserrat', 'XCD'),
(153, 'MT', 'Malta', 'EUR'),
(154, 'MU', 'Mauritius', 'MUR'),
(155, 'MV', 'Maldives', 'MVR'),
(156, 'MW', 'Malawi', 'MWK'),
(157, 'MX', 'Mexico', 'MXN'),
(158, 'MY', 'Malaysia', 'MYR'),
(159, 'MZ', 'Mozambique', 'MZN'),
(160, 'NA', 'Namibia', 'NAD'),
(161, 'NC', 'New Caledonia', 'XPF'),
(162, 'NE', 'Niger', 'XOF'),
(163, 'NF', 'Norfolk Island', 'AUD'),
(164, 'NG', 'Nigeria', 'NGN'),
(165, 'NI', 'Nicaragua', 'NIO'),
(166, 'NL', 'Netherlands', 'EUR'),
(167, 'NO', 'Norway', 'NOK'),
(168, 'NP', 'Nepal', 'NPR'),
(169, 'NR', 'Nauru', 'AUD'),
(170, 'NU', 'Niue', 'NZD'),
(171, 'NZ', 'New Zealand', 'NZD'),
(172, 'OM', 'Oman', 'OMR'),
(173, 'PA', 'Panama', 'PAB'),
(174, 'PE', 'Peru', 'PEN'),
(175, 'PF', 'French Polynesia', 'XPF'),
(176, 'PG', 'Papua New Guinea', 'PGK'),
(177, 'PH', 'Philippines', 'PHP'),
(178, 'PK', 'Pakistan', 'PKR'),
(179, 'PL', 'Poland', 'PLN'),
(180, 'PM', 'Saint Pierre and Miquelon', 'EUR'),
(181, 'PN', 'Pitcairn Islands', 'NZD'),
(182, 'PR', 'Puerto Rico', 'USD'),
(183, 'PS', 'Palestine', 'ILS'),
(184, 'PT', 'Portugal', 'EUR'),
(185, 'PW', 'Palau', 'USD'),
(186, 'PY', 'Paraguay', 'PYG'),
(187, 'QA', 'Qatar', 'QAR'),
(188, 'RE', 'Reunion', 'EUR'),
(189, 'RO', 'Romania', 'RON'),
(190, 'RS', 'Serbia', 'RSD'),
(191, 'RU', 'Russia', 'RUB'),
(192, 'RW', 'Rwanda', 'RWF'),
(193, 'SA', 'Saudi Arabia', 'SAR'),
(194, 'SB', 'Solomon Islands', 'SBD'),
(195, 'SC', 'Seychelles', 'SCR'),
(196, 'SD', 'Sudan', 'SDG'),
(197, 'SE', 'Sweden', 'SEK'),
(198, 'SG', 'Singapore', 'SGD'),
(199, 'SH', 'Saint Helena', 'SHP'),
(200, 'SI', 'Slovenia', 'EUR'),
(201, 'SJ', 'Svalbard and Jan Mayen', 'NOK'),
(202, 'SK', 'Slovakia', 'EUR'),
(203, 'SL', 'Sierra Leone', 'SLL'),
(204, 'SM', 'San Marino', 'EUR'),
(205, 'SN', 'Senegal', 'XOF'),
(206, 'SO', 'Somalia', 'SOS'),
(207, 'SR', 'Suriname', 'SRD'),
(208, 'SS', 'South Sudan', 'SSP'),
(209, 'ST', 'Sao Tome and Principe', 'STD'),
(210, 'SV', 'El Salvador', 'USD'),
(211, 'SX', 'Sint Maarten', 'ANG'),
(212, 'SY', 'Syria', 'SYP'),
(213, 'SZ', 'Swaziland', 'SZL'),
(214, 'TC', 'Turks and Caicos Islands', 'USD'),
(215, 'TD', 'Chad', 'XAF'),
(216, 'TF', 'French Southern Territories', 'EUR'),
(217, 'TG', 'Togo', 'XOF'),
(218, 'TH', 'Thailand', 'THB'),
(219, 'TJ', 'Tajikistan', 'TJS'),
(220, 'TK', 'Tokelau', 'NZD'),
(221, 'TL', 'East Timor', 'USD'),
(222, 'TM', 'Turkmenistan', 'TMT'),
(223, 'TN', 'Tunisia', 'TND'),
(224, 'TO', 'Tonga', 'TOP'),
(225, 'TR', 'Turkey', 'TRY'),
(226, 'TT', 'Trinidad and Tobago', 'TTD'),
(227, 'TV', 'Tuvalu', 'AUD'),
(228, 'TW', 'Taiwan', 'TWD'),
(229, 'TZ', 'Tanzania', 'TZS'),
(230, 'UA', 'Ukraine', 'UAH'),
(231, 'UG', 'Uganda', 'UGX'),
(232, 'UM', 'U.S. Minor Outlying Islands', 'USD'),
(233, 'US', 'United States', 'USD'),
(234, 'UY', 'Uruguay', 'UYU'),
(235, 'UZ', 'Uzbekistan', 'UZS'),
(236, 'VA', 'Vatican City', 'EUR'),
(237, 'VC', 'Saint Vincent and the Grenadines', 'XCD'),
(238, 'VE', 'Venezuela', 'VEF'),
(239, 'VG', 'British Virgin Islands', 'USD'),
(240, 'VI', 'U.S. Virgin Islands', 'USD'),
(241, 'VN', 'Vietnam', 'VND'),
(242, 'VU', 'Vanuatu', 'VUV'),
(243, 'WF', 'Wallis and Futuna', 'XPF'),
(244, 'WS', 'Samoa', 'WST'),
(245, 'XK', 'Kosovo', 'EUR'),
(246, 'YE', 'Yemen', 'YER'),
(247, 'YT', 'Mayotte', 'EUR'),
(248, 'ZA', 'South Africa', 'ZAR'),
(249, 'ZM', 'Zambia', 'ZMW'),
(250, 'ZW', 'Zimbabwe', 'ZWL');

-- --------------------------------------------------------

--
-- Table structure for table `degrees`
--

CREATE TABLE IF NOT EXISTS `degrees` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `degrees`
--

INSERT INTO `degrees` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'School', '2016-11-29 15:10:06', '2016-11-29 15:10:26'),
(2, 'Graduate', '2016-11-29 15:10:34', '2016-11-29 15:10:34'),
(4, 'Post Graduate', '2016-11-29 15:11:34', '2016-11-29 15:11:34'),
(5, 'Bachelor', '2016-11-29 22:31:02', '2016-11-29 22:31:02'),
(6, 'Master', '2016-11-29 22:31:26', '2016-11-29 22:31:26'),
(7, 'Ph.D', '2016-11-29 22:31:37', '2016-11-29 22:31:37'),
(8, 'Professor', '2016-11-29 22:31:52', '2016-12-20 22:23:29'),
(9, 'High School', '2016-11-30 10:59:33', '2016-11-30 10:59:33');

-- --------------------------------------------------------

--
-- Table structure for table `educations`
--

CREATE TABLE IF NOT EXISTS `educations` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `institution` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `major` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `degree` int(11) NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` int(10) unsigned NOT NULL,
  `from_month` int(10) unsigned NOT NULL,
  `from_year` int(10) unsigned NOT NULL,
  `to_month` int(10) unsigned NOT NULL,
  `to_year` int(10) unsigned NOT NULL,
  `status` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Active, N = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `educations`
--

INSERT INTO `educations` (`id`, `user_id`, `institution`, `major`, `degree`, `city`, `country`, `from_month`, `from_year`, `to_month`, `to_year`, `status`, `created_at`, `updated_at`) VALUES
(5, 95, 'Học viện Ngoại giao', 'Chế biến lương thực, thực phẩm và đồ uống', 5, 'Hanoi, Vietnam', 0, 0, 1978, 0, 2017, 'Y', '2017-01-19 23:03:48', '2017-01-19 23:04:03'),
(6, 96, 'Trường Đại học Sân khấu - Điện ảnh Hà Nội', 'Diễn viên kịch - điện ảnh', 5, 'Hanoi, Vietnam', 0, 0, 1985, 0, 2017, 'Y', '2017-01-19 23:26:57', '2017-01-19 23:26:57'),
(12, 99, 'Trường Đại học Ngoại thương', 'Kinh tế quốc tế', 0, 'Hanoi, Việt Nam', 0, 0, 2001, 0, 2017, 'Y', '2017-01-20 21:04:02', '2017-01-20 21:04:02'),
(13, 24, 'Trường Đại học Kinh tế Quốc dân', 'Thống kê', 0, 'Hanoi, Vietnam', 0, 0, 2012, 0, 2017, 'Y', '2017-01-20 22:52:34', '2017-01-20 22:52:34');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(10) unsigned NOT NULL,
  `email_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `email_title`, `description`, `locale`, `created_at`, `updated_at`) VALUES
(1, 'Activate accounts', '<header style=" margin: 0px auto; width: 600px;">{SITE_URL}</header>\r\n\r\n<section style=" margin: 0px auto; width: 600px; border: 1px solid #EBEBEB; padding: 10px;">\r\n\r\n    <div style="font-family:Verdana,Arial;font-weight:normal;font-size:16px; font-weight: bold;line-height:18px;margin-bottom:10px;margin-top:20px">Hi {USER_NAME},</div>\r\n\r\n    <div style="font-family:Verdana,Arial;font-weight:normal;font-size:12px;line-height:18px;margin-bottom:10px;margin-top:10px">Thanks for creating an account on {SITE_URL}.\r\n\r\n        Please follow the link below to verify your email address: </div>\r\n\r\n\r\n\r\n    <div style="font-family:Verdana,Arial;font-weight:normal;text-align:center;font-size:16px;line-height:18px;margin-bottom:30px;margin-top:30px"><a href="{ACTIVATION_LINK}" style="text-decoration: none; padding: 10px 40px; background: #3696C2; color: #fff;">ACTIVATE ACCOUNT</a></div>\r\n\r\n\r\n\r\n    <div style="font-family:Verdana,Arial;font-weight:normal;font-size:12px;margin-bottom:10px;margin-top:10px">If you have any questions, please feel free to contact us at {CONTACT_MAIL}.</div>\r\n\r\n</section>\r\n\r\n\r\n<footer style="font-family:Verdana,Arial;font-weight:normal;text-align:center;font-size:12px;line-height:18px;margin-bottom:75px;margin-top:30px">Thank you, {SITE_URL}</footer>', 'en', '2016-09-30 18:30:00', '2016-09-30 18:30:00'),
(3, 'New Invoice', 'Congrats.. A new invoice is created.  ', 'en', '2016-10-12 18:30:00', '2016-10-12 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `favorite_tropics`
--

CREATE TABLE IF NOT EXISTS `favorite_tropics` (
  `id` int(10) unsigned NOT NULL,
  `tropic_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `favorite_tropics`
--

INSERT INTO `favorite_tropics` (`id`, `tropic_id`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 8, 1, '2016-12-13 20:44:31', '2016-12-13 20:44:31'),
(3, 14, 1, '2016-12-13 20:44:33', '2016-12-13 20:44:33'),
(4, 17, 1, '2016-12-13 20:44:33', '2016-12-13 20:44:33'),
(5, 28, 1, '2016-12-13 20:44:36', '2016-12-13 20:44:36'),
(9, 10, 1, '2016-12-13 20:44:54', '2016-12-13 20:44:54'),
(10, 13, 1, '2016-12-13 20:45:00', '2016-12-13 20:45:00'),
(12, 9, 1, '2016-12-13 20:45:27', '2016-12-13 20:45:27'),
(13, 9, 24, '2016-12-16 22:14:01', '2016-12-16 22:14:01'),
(14, 16, 24, '2016-12-16 22:14:05', '2016-12-16 22:14:05'),
(15, 28, 24, '2016-12-16 22:14:24', '2016-12-16 22:14:24'),
(16, 8, 24, '2016-12-16 22:15:17', '2016-12-16 22:15:17'),
(17, 11, 1, '2016-12-19 20:32:10', '2016-12-19 20:32:10'),
(18, 20, 60, '2016-12-20 22:01:42', '2016-12-20 22:01:42'),
(20, 16, 60, '2016-12-20 22:05:17', '2016-12-20 22:05:17'),
(21, 13, 24, '2016-12-22 21:56:04', '2016-12-22 21:56:04'),
(22, 30, 48, '2016-12-26 23:00:39', '2016-12-26 23:00:39'),
(23, 16, 48, '2016-12-26 23:00:42', '2016-12-26 23:00:42'),
(24, 13, 48, '2016-12-26 23:00:51', '2016-12-26 23:00:51'),
(25, 18, 48, '2016-12-26 23:00:58', '2016-12-26 23:00:58'),
(26, 31, 48, '2016-12-26 23:01:33', '2016-12-26 23:01:33'),
(29, 20, 49, '2016-12-26 23:20:56', '2016-12-26 23:20:56'),
(30, 28, 49, '2016-12-26 23:20:57', '2016-12-26 23:20:57'),
(31, 9, 49, '2016-12-26 23:21:06', '2016-12-26 23:21:06'),
(33, 28, 19, '2016-12-27 12:37:15', '2016-12-27 12:37:15'),
(34, 20, 19, '2016-12-27 12:37:17', '2016-12-27 12:37:17'),
(35, 14, 73, '2016-12-29 22:24:11', '2016-12-29 22:24:11'),
(36, 20, 73, '2016-12-29 22:24:12', '2016-12-29 22:24:12'),
(37, 16, 73, '2016-12-29 22:24:33', '2016-12-29 22:24:33'),
(38, 17, 73, '2016-12-29 22:24:34', '2016-12-29 22:24:34'),
(39, 18, 73, '2016-12-29 22:24:37', '2016-12-29 22:24:37'),
(40, 32, 73, '2016-12-29 22:40:21', '2016-12-29 22:40:21'),
(41, 50, 31, '2017-01-11 22:41:56', '2017-01-11 22:41:56');

-- --------------------------------------------------------

--
-- Table structure for table `filter_order_sets`
--

CREATE TABLE IF NOT EXISTS `filter_order_sets` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `filter_set` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `filter_order_sets`
--

INSERT INTO `filter_order_sets` (`id`, `user_id`, `filter_set`) VALUES
(1, 1, '{"prof":"profesion","tpic":"tpic","gnder":"gendr","dpt":"depatment","locl":"locationlive","locv":"locationvisit","workpl":"workplace","eduction":"eductn","mjor":"majr","ag":"agee"}'),
(2, 79, '{"locv":"locationvisit","workpl":"workplace","prof":"profesion"}'),
(3, 68, '{"eduction":"eductn","workpl":"workplace","prof":"profesion","mjor":"majr","dpt":"depatment","tpic":"tpic"}'),
(4, 31, '{"prof":"profesion","dpt":"depatment","eduction":"eductn","workpl":"workplace"}'),
(5, 82, '{"workpl":"workplace","prof":"profesion","eduction":"eductn","ag":"agee"}'),
(6, 77, '{"tpic":"tpic"}'),
(7, 90, '{"workpl":"workplace","prof":"profesion","dpt":"depatment","locl":"locationlive","eduction":"eductn"}'),
(8, 19, '{"workpl":"workplace","prof":"profesion","dpt":"depatment","eduction":"eductn"}'),
(9, 93, '{"prof":"profesion","dpt":"depatment","workpl":"workplace","eduction":"eductn","locl":"locationlive"}'),
(10, 48, '{"dpt":"depatment","prof":"profesion","workpl":"workplace"}'),
(11, 60, '{"workpl":"workplace","prof":"profesion"}'),
(12, 95, '{"workpl":"workplace","prof":"profesion","dpt":"depatment"}'),
(13, 96, '{"workpl":"workplace","prof":"profesion","dpt":"depatment"}'),
(14, 24, '{"locv":"locationvisit","workpl":"workplace","prof":"profesion"}');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `weight` int(10) unsigned NOT NULL,
  `fallback_locale` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Deafult language',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `locale`, `weight`, `fallback_locale`, `created_at`, `updated_at`) VALUES
(1, 'English', 'en', 1, 'Y', '2016-07-12 07:51:59', '2016-07-12 07:51:59'),
(2, 'Polish ', 'pl', 2, 'N', '2016-09-28 18:30:00', '2016-09-28 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_07_06_075604_create_admins_table', 1),
('2016_07_06_112919_create_settings_table', 1),
('2016_07_12_092132_create_languages_table', 1),
('2016_09_16_092105_create_pages_table', 2),
('2016_09_16_093651_create_pages_table', 3),
('2016_09_16_093953_create_page_translations_table', 4),
('2016_09_16_110017_create_users_table', 5),
('2016_09_22_075004_create_groups_table', 6),
('2016_09_22_095444_create_group_members_table', 7),
('2016_09_22_130046_create_groups_table', 8),
('2016_09_22_130253_create_group_members_table', 9),
('2016_09_23_132931_create_group_user_table', 10),
('2016_09_23_134032_create_group_user_table', 11),
('2016_09_24_133840_create_categorys_table', 12),
('2016_09_26_071915_create_packages_table', 13),
('2016_09_26_105120_create_user_package_table', 14),
('2016_09_28_071834_add_column_to_settings', 15),
('2016_09_28_123624_add_column_to_package_user', 16),
('2016_10_01_081042_add_column_to_users', 17),
('2016_10_01_095334_create_email_templates_table', 18),
('2016_10_03_123350_add_column_to_setting', 19),
('2016_10_07_102140_create_invoices_table', 20),
('2016_10_18_140838_create_questions_table', 21),
('2016_10_19_141649_create_answers_table', 22),
('2016_10_20_061252_create_tests_tabel', 23),
('2016_10_20_102743_create_test_question_tabel', 24),
('2016_10_21_133133_add_column_to_questions', 25),
('2016_10_25_131934_create_sub_questions_table', 26),
('2016_10_25_135636_create_sub_amswers_table', 26),
('2016_10_26_075209_add_column_to_sub_answers', 27),
('2016_10_26_114058_add_column_to_tests', 27),
('2016_10_27_075703_add_column_to_question', 28),
('2016_10_27_080117_add_column_to_answers', 29),
('2016_10_27_111850_create_test_group_table', 30),
('2016_10_28_074023_create_test_evaluations_table', 31),
('2016_10_31_062736_create_test_answers_table', 32),
('2016_11_07_130827_add_column_to_test_evaluations', 33),
('2016_11_11_134633_create_work_experience_table', 34),
('2016_11_11_155027_create_educations_table', 35),
('2016_11_11_162456_create_tropic_table', 36);

-- --------------------------------------------------------

--
-- Table structure for table `notification_settings`
--

CREATE TABLE IF NOT EXISTS `notification_settings` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `answer_question` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `upvote` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `new_follower` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `new_nessage` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `tag_me` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `comment_my` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notification_settings`
--

INSERT INTO `notification_settings` (`id`, `user_id`, `answer_question`, `upvote`, `new_follower`, `new_nessage`, `tag_me`, `comment_my`, `created_at`, `updated_at`) VALUES
(6, 1, 'Y', 'Y', 'N', 'Y', 'N', 'N', '2016-11-25 07:40:49', '2016-11-25 19:15:03'),
(7, 24, 'Y', 'N', 'N', 'N', 'N', 'N', '2016-12-22 22:08:09', '2016-12-22 22:08:09'),
(8, 48, 'N', 'N', 'N', 'N', 'Y', 'N', '2016-12-26 23:07:11', '2016-12-26 23:07:13'),
(9, 82, 'N', 'N', 'N', 'Y', 'Y', 'Y', '2017-01-14 22:57:22', '2017-01-14 22:57:25');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `created_at`, `updated_at`) VALUES
(2, '2016-09-29 03:48:02', '2016-09-29 03:48:02'),
(5, '2016-10-13 02:43:30', '2016-10-13 02:43:30'),
(6, '2016-10-13 03:48:30', '2016-10-13 03:48:30'),
(7, '2016-10-14 01:17:00', '2016-10-14 01:17:00'),
(8, '2016-10-14 01:19:47', '2016-10-14 01:19:47'),
(9, '2016-10-14 01:21:13', '2016-10-14 01:21:13'),
(10, '2016-10-14 01:23:29', '2016-10-14 01:23:29'),
(11, '2016-10-14 01:24:34', '2016-10-14 01:24:34');

-- --------------------------------------------------------

--
-- Table structure for table `page_translations`
--

CREATE TABLE IF NOT EXISTS `page_translations` (
  `id` int(10) unsigned NOT NULL,
  `page_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_content` text COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `page_translations`
--

INSERT INTO `page_translations` (`id`, `page_id`, `slug`, `title`, `page_content`, `locale`, `seo_title`, `seo_keywords`, `seo_description`) VALUES
(3, 2, 'stories', 'Stories', 'EnLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\r\n\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\r\n\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'en', 'Stories', '\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(5, 6, 'about-us', 'About Us', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'en', 'ttt', 'ing, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the indu', 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the indu'),
(8, 6, 'about-us', 'About Pl', 'PLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'pl', 'ddd', 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the indu', ' Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ing, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the indu'),
(11, 2, 'stories', 'Stories', '\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'pl', 'ddd', '\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', ''),
(12, 11, 'contact-us', 'Contact Us', '', 'en', 'Contact Us', 'Contact Us', 'Contact Us'),
(13, 11, 'contact-us', 'Contact Us', '', 'pl', 'Contact Us', 'Contact Us', 'Contact Us');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `privacy_settings`
--

CREATE TABLE IF NOT EXISTS `privacy_settings` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `online` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `other_view_account` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `want_receive_message` enum('S','A','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A' COMMENT 'Y = Specific People, A = Anyone, N = No',
  `decline_request` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Y = Yes, N = No',
  `max_request` int(10) unsigned NOT NULL,
  `request_intelval` enum('D','M') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'D' COMMENT 'D = Per Day, M = Per Month',
  `allow_comments` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Y = Yes, N = No',
  `deactivate_account` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Y = Yes, N = No',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `privacy_settings`
--

INSERT INTO `privacy_settings` (`id`, `user_id`, `online`, `other_view_account`, `want_receive_message`, `decline_request`, `max_request`, `request_intelval`, `allow_comments`, `deactivate_account`, `created_at`, `updated_at`) VALUES
(1, 1, 'N', 'N', 'A', 'N', 0, 'D', 'N', 'N', '2016-11-25 06:18:39', '2016-11-25 20:36:18');

-- --------------------------------------------------------

--
-- Table structure for table `school_majors`
--

CREATE TABLE IF NOT EXISTS `school_majors` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=406 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school_majors`
--

INSERT INTO `school_majors` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Biên phòng ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(2, 'Phục hồi chức năng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(3, 'Công nghệ kỹ thuật cơ khí', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(4, 'Âm nhạc học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(5, 'An ninh và trật tự xã hội', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(6, 'An ninh, Quốc phòng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(7, 'Bản đồ học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(8, 'Báo chí', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(9, 'Báo chí và thông tin', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(10, 'Báo chí và truyền thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(11, 'Bảo dưỡng công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(12, 'Bảo hiểm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(13, 'Bảo hộ lao động', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(14, 'Bảo tàng học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(15, 'Bảo vệ thực vật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(16, 'Bất động sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(17, 'Bệnh học thủy sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(18, 'Biên đạo múa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(19, 'Biên kịch điện ảnh - truyền hình', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(20, 'Biên kịch sân khấu', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(21, 'Biểu diễn nhạc cụ phương tây', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(22, 'Biểu diễn nhạc cụ truyền thống', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(23, 'Chăn nuôi', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(24, 'Chế biến lương thực, thực phẩm và đồ uống', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(25, 'Chỉ huy âm nhạc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(26, 'Chỉ huy kỹ thuật ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(27, 'Chỉ huy kỹ thuật Công binh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(28, 'Chỉ huy kỹ thuật Hoá học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(29, 'Chỉ huy kỹ thuật Phòng không', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(30, 'Chỉ huy kỹ thuật Tác chiến điện tử', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(31, 'Chỉ huy kỹ thuật Tăng - thiết giáp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(32, 'Chỉ huy kỹ thuật Thông tin', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(33, 'Chỉ huy tham mưu Đặc công', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(34, 'Chỉ huy tham mưu Hải quân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(35, 'Chỉ huy tham mưu Không quân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(36, 'Chỉ huy tham mưu Lục quân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(37, 'Chỉ huy tham mưu Pháo binh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(38, 'Chỉ huy tham mưu Phòng không', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(39, 'Chỉ huy tham mưu Tăng - thiết giáp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(40, 'Chính trị học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(41, 'Cơ kỹ thuật ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(42, 'Công nghệ chế biến lâm sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(43, 'Công nghệ chế biến thuỷ sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(44, 'Công nghệ chế tạo máy', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(45, 'Công nghệ da giày', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(46, 'Công nghệ điện ảnh - truyền hình', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(47, 'Công nghệ giấy và bột giấy', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(48, 'Công nghệ hoá học, vật liệu, luyện kim và môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(49, 'Công nghệ in', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(50, 'Công nghệ kỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(51, 'Công nghệ kỹ thuật cơ điện tử', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(52, 'Công nghệ kỹ thuật công trình xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(53, 'Công nghệ kỹ thuật địa chất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(54, 'Công nghệ kỹ thuật địa chất, địa vật lý và trắc địa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(55, 'Công nghệ kỹ thuật điện tử, truyền thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(56, 'Công nghệ kỹ thuật điện, điện tử', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(57, 'Công nghệ kỹ thuật điện, điện tử và viễn thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(58, 'Công nghệ kỹ thuật điều khiển và tự động hóa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(59, 'Công nghệ kỹ thuật giao thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(60, 'Công nghệ kỹ thuật hạt nhân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(61, 'Công nghệ kỹ thuật hoá học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(62, 'Công nghệ kỹ thuật kiến trúc ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(63, 'Công nghệ kỹ thuật kiến trúc và công trình xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(64, 'Công nghệ kỹ thuật máy tính', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(65, 'Công nghệ kỹ thuật mỏ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(66, 'Công nghệ kỹ thuật môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(67, 'Công nghệ kỹ thuật nhiệt', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(68, 'Công nghệ kỹ thuật ô tô', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(69, 'Công nghệ kỹ thuật tài nguyên nước', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(70, 'Công nghệ kỹ thuật trắc địa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(71, 'Công nghệ kỹ thuật vật liệu xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(72, 'Công nghệ kỹ thuật xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(73, 'Công nghệ may', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(74, 'Công nghệ rau hoa quả và cảnh quan', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(75, 'Công nghệ sản xuất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(76, 'Công nghệ sau thu hoạch', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(77, 'Công nghệ sinh học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(78, 'Công nghệ sợi, dệt', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(79, 'Công nghệ thông tin', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(80, 'Công nghệ thực phẩm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(81, 'Công nghệ truyền thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(82, 'Công nghệ tuyển khoáng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(83, 'Công nghệ vật liệu', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(84, 'Công tác thanh thiếu niên', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(85, 'Công tác xã hội', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(86, 'Công thôn                       ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(87, 'Đạo diễn điện ảnh - truyền hình', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(88, 'Đạo diễn sân khấu', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(89, 'Đào tạo giáo viên', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(90, 'Địa chất học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(91, 'Địa kỹ thuật xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(92, 'Địa lý học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(93, 'Địa lý tự nhiên', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(94, 'Dịch vụ an toàn lao động và vệ sinh công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(95, 'Dịch vụ pháp lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(96, 'Dịch vụ thú y', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(97, 'Dịch vụ vận tải', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(98, 'Dịch vụ xã hội', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(99, 'Dịch vụ y tế', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(100, 'Diễn viên kịch - điện ảnh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(101, 'Diễn viên múa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(102, 'Diễn viên sân khấu kịch hát', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(103, 'Điều dưỡng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(104, 'Điều dưỡng, hộ sinh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(105, 'Điêu khắc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(106, 'Điều khiển tàu biển', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(107, 'Điều tra hình sự', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(108, 'Điều tra trinh sát', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(109, 'Đồ hoạ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(110, 'Đông Nam Á học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(111, 'Đông phương học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(112, 'Dược học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(113, 'Giáo dục Chính trị', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(114, 'Giáo dục Công dân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(115, 'Giáo dục Đặc biệt', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(116, 'Giáo dục học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(117, 'Giáo dục Mầm non', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(118, 'Giáo dục Quốc phòng - An ninh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(119, 'Giáo dục Thể chất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(120, 'Giáo dục Tiểu học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(121, 'Gốm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(122, 'Hải dương học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(123, 'Hán Nôm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(124, 'Hàn Quốc học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(125, 'Hậu cần công an nhân dân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(126, 'Hậu cần quân sự', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(127, 'Hệ thống thông tin', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(128, 'Hệ thống thông tin quản lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(129, 'Hộ sinh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(130, 'Hoá dược', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(131, 'Hoá học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(132, 'Hội hoạ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(133, 'Huấn luyện múa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(134, 'Huấn luyện thể thao*', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(135, 'Kế toán', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(136, 'Kế toán – Kiểm toán', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(137, 'Khách sạn, du lịch, thể thao và dịch vụ cá nhân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(138, 'Khách sạn, nhà hàng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(139, 'Khai thác vận tải', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(140, 'Khí tượng học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(141, 'Khoa học cây trồng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(142, 'Khoa học chính trị', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(143, 'Khoa học đất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(144, 'Khoa học giáo dục', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(145, 'Khoa học hàng hải', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(146, 'Khoa học máy tính', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(147, 'Khoa học môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(148, 'Khoa học quản lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(149, 'Khoa học sự sống', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(150, 'Khoa học thư viện', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(151, 'Khoa học trái đất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(152, 'Khoa học tự nhiên', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(153, 'Khoa học vật chất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(154, 'Khoa học vật liệu', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(155, 'Khoa học xã hội và hành vi', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(156, 'Khu vực Thái Bình Dương học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(157, 'Khuyến nông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(158, 'Kiểm soát và bảo vệ môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(159, 'Kiểm toán', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(160, 'Kiến trúc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(161, 'Kiến trúc cảnh quan', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(162, 'Kiến trúc và quy hoạch', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(163, 'Kiến trúc và xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(164, 'Kinh doanh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(165, 'Kinh doanh nông nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(166, 'Kinh doanh quốc tế ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(167, 'Kinh doanh thương mại', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(168, 'Kinh doanh và quản lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(169, 'Kinh doanh xuất bản phẩm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(170, 'Kinh tế', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(171, 'Kinh tế công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(172, 'Kinh tế gia đình', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(173, 'Kinh tế học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(174, 'Kinh tế nông nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(175, 'Kinh tế quốc tế', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(176, 'Kinh tế tài nguyên thiên nhiên', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(177, 'Kinh tế vận tải', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(178, 'Kinh tế xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(179, 'Kỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(180, 'Kỹ thuật biển', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(181, 'Kỹ thuật cơ - điện tử', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(182, 'Kỹ thuật cơ khí', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(183, 'Kỹ thuật cơ khí và cơ kỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(184, 'Kỹ thuật cơ sở hạ tầng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(185, 'Kỹ thuật công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(186, 'Kỹ thuật công trình biển', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(187, 'Kỹ thuật công trình thuỷ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(188, 'Kỹ thuật công trình xây dựng ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(189, 'Kỹ thuật dầu khí', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(190, 'Kỹ thuật dệt', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(191, 'Kỹ thuật địa chất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(192, 'Kỹ thuật địa chất, địa vật lý và trắc địa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(193, 'Kỹ thuật địa vật lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(194, 'Kỹ thuật điện tử, truyền thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(195, 'Kỹ thuật điện, điện tử', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(196, 'Kỹ thuật điện, điện tử và viễn thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(197, 'Kỹ thuật điều khiển và tự động hoá', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(198, 'Kỹ thuật hàng không', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(199, 'Kỹ thuật hạt nhân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(200, 'Kỹ thuật hệ thống công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(201, 'Kỹ thuật hình ảnh y học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(202, 'Kỹ thuật hình sự', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(203, 'Kỹ thuật hoá học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(204, 'Kỹ thuật hoá học, vật liệu, luyện kim và môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(205, 'Kỹ thuật khai thác thủy sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(206, 'Kỹ thuật máy tính', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(207, 'Kỹ thuật mỏ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(208, 'Kỹ thuật môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(209, 'Kỹ thuật nhiệt', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(210, 'Kỹ thuật phần mềm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(211, 'Kỹ thuật phục hình răng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(212, 'Kỹ thuật sinh học*', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(213, 'Kỹ thuật tài nguyên nước', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(214, 'Kỹ thuật tàu thuỷ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(215, 'Kỹ thuật trắc địa - bản đồ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(216, 'Kỹ thuật tuyển khoáng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(217, 'Kỹ thuật vật liệu', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(218, 'Kỹ thuật vật liệu kim loại', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(219, 'Kỹ thuật xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(220, 'Kỹ thuật xây dựng công trình giao thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(221, 'Kỹ thuật y sinh*', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(222, 'Lâm nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(223, 'Lâm nghiệp đô thị', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(224, 'Lâm sinh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(225, 'Lịch sử', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(226, 'Luật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(227, 'Luật kinh tế', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(228, 'Luật quốc tế', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(229, 'Lưu trữ học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(230, 'Lý luận và phê bình điện ảnh - truyền hình', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(231, 'Lý luận và phê bình sân khấu', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(232, 'Lý luận, lịch sử và phê bình mỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(233, 'Lý luận, phê bình múa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(234, 'Marketing', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(235, 'Máy tính', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(236, 'Máy tính và công nghệ thông tin', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(237, 'Môi trường và bảo vệ môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(238, 'Mỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(239, 'Mỹ thuật ứng dụng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(240, 'Nghệ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(241, 'Nghệ thuật nghe nhìn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(242, 'Nghệ thuật trình diễn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(243, 'Ngôn ngữ Anh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(244, 'Ngôn ngữ Ảrập', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(245, 'Ngôn ngữ Bồ Đào Nha', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(246, 'Ngôn ngữ Chăm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(247, 'Ngôn ngữ Đức', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(248, 'Ngôn ngữ Hàn Quốc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(249, 'Ngôn ngữ H''mong', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(250, 'Ngôn ngữ học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(251, 'Ngôn ngữ Italia', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(252, 'Ngôn ngữ Jrai', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(253, 'Ngôn ngữ Khme', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(254, 'Ngôn ngữ Nga', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(255, 'Ngôn ngữ Nhật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(256, 'Ngôn ngữ Pháp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(257, 'Ngôn ngữ Tây Ban Nha', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(258, 'Ngôn ngữ Trung Quốc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(259, 'Ngôn ngữ và văn hoá nước ngoài', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(260, 'Ngôn ngữ và văn hoá Việt Nam', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(261, 'Nhạc Jazz', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(262, 'Nhân học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(263, 'Nhân văn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(264, 'Nhân văn khác', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(265, 'Nhật Bản học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(266, 'Nhiếp ảnh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(267, 'Nông học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(268, 'Nông nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(269, 'Nông, lâm nghiệp và thuỷ sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(270, 'Nuôi trồng thuỷ sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(271, 'Pháp luật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(272, 'Phát triển nông thôn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(273, 'Phòng cháy chữa cháy và cứu hộ cứu nạn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(274, 'Piano', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(275, 'Quan hệ công chúng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(276, 'Quan hệ quốc tế', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(277, 'Quản lý bệnh viện', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(278, 'Quản lý công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(279, 'Quản lý đất đai ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(280, 'Quản lý giáo dục', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(281, 'Quản lý hoạt động bay', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(282, 'Quản lý nguồn lợi thủy sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(283, 'Quản lý nhà nước', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(284, 'Quản lý nhà nước về an ninh trật tự', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(285, 'Quản lý tài nguyên rừng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(286, 'Quản lý tài nguyên và môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(287, 'Quản lý thể dục thể thao*', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(288, 'Quản lý văn hoá', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(289, 'Quản lý xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(290, 'Quản lý, giáo dục và cải tạo phạm nhân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(291, 'Quân sự', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(292, 'Quân sự cơ sở', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(293, 'Quản trị – Quản lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(294, 'Quản trị dịch vụ du lịch và lữ hành', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(295, 'Quản trị khách sạn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(296, 'Quản trị kinh doanh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(297, 'Quản trị nhà hàng và dịch vụ ăn uống', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(298, 'Quản trị nhân lực', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(299, 'Quản trị văn phòng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(300, 'Quay phim', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(301, 'Quốc tế học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(302, 'Quy hoạch vùng và đô thị', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(303, 'Răng - Hàm - Mặt', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(304, 'Sản xuất và chế biến', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(305, 'Sản xuất, chế biến khác', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(306, 'Sản xuất, chế biến sợi, vải, giày, da', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(307, 'Sáng tác âm nhạc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(308, 'Sáng tác văn học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(309, 'Sinh học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(310, 'Sinh học ứng dụng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(311, 'Sư phạm Âm nhạc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(312, 'Sư phạm Công tác Đội thiếu niên Tiền phong HCM', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(313, 'Sư phạm Địa lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(314, 'Sư phạm Hoá học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(315, 'Sư phạm Kinh tế gia đình', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(316, 'Sư phạm Kỹ thuật công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(317, 'Sư phạm Kỹ thuật nông nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(318, 'Sư phạm Lịch sử', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(319, 'Sư phạm Mỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(320, 'Sư phạm Ngữ văn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(321, 'Sư phạm Sinh học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(322, 'Sư phạm Tiếng Anh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(323, 'Sư phạm Tiếng Bahna', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(324, 'Sư phạm Tiếng Bana', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(325, 'Sư phạm Tiếng Chăm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(326, 'Sư phạm Tiếng Đức', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(327, 'Sư phạm Tiếng Êđê', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(328, 'Sư phạm Tiếng H''mong', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(329, 'Sư phạm Tiếng Jrai', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(330, 'Sư phạm Tiếng Khme', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(331, 'Sư phạm Tiếng M''nông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(332, 'Sư phạm Tiếng Nga', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(333, 'Sư phạm Tiếng Nhật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(334, 'Sư phạm Tiếng Pháp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(335, 'Sư phạm Tiếng Trung Quốc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(336, 'Sư phạm Tiếng Xêđăng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(337, 'Sư phạm Tin học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(338, 'Sư phạm Toán học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(339, 'Sư phạm Vật lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(340, 'Sức khoẻ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(341, 'Tài chính – Ngân hàng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(342, 'Tài chính – Ngân hàng – Bảo hiểm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(343, 'Tâm lý học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(344, 'Tâm lý học giáo dục', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(345, 'Tham mưu, chỉ huy vũ trang bảo vệ an ninh trật tự', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(346, 'Thanh nhạc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(347, 'Thiên văn học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(348, 'Thiết kế âm thanh - ánh sáng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(349, 'Thiết kế công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(350, 'Thiết kế đồ họa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(351, 'Thiết kế mỹ thuật sân khấu - điện ảnh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(352, 'Thiết kế nội thất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(353, 'Thiết kế thời trang', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(354, 'Thống kê', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(355, 'Thông tin - Thư viện', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(356, 'Thông tin học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(357, 'Thông tin -Thư viện', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(358, 'Thư ký văn phòng ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(359, 'Thú y', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(360, 'Thuỷ sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(361, 'Thuỷ văn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(362, 'Tiếng Anh                       ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(363, 'Tiếng Hàn Quốc ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(364, 'Tiếng Khơ me', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(365, 'Tiếng Lào', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(366, 'Tiếng Nhật      ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(367, 'Tiếng Pháp           ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(368, 'Tiếng Thái', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(369, 'Tiếng Trung Quốc           ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(370, 'Tiếng Việt và văn hoá Việt Nam', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(371, 'Tin học ứng dụng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(372, 'Tình báo an ninh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(373, 'Tình báo quân sự', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(374, 'Toán cơ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(375, 'Toán học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(376, 'Toán ứng dụng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(377, 'Toán và thống kê', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(378, 'Triết học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(379, 'Trinh sát kỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(380, 'Trung Quốc học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(381, 'Truyền thông đa phương tiện', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(382, 'Truyền thông quốc tế', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(383, 'Truyền thông và mạng máy tính', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(384, 'Vận hành khai thác máy tàu', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(385, 'Văn hoá các dân tộc thiểu số Việt Nam', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(386, 'Văn hoá học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(387, 'Văn học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(388, 'Văn thư - Lưu trữ - Bảo tàng ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(389, 'Vật lý hạt nhân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(390, 'Vật lý học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(391, 'Vật lý kỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(392, 'Việt Nam học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(393, 'Xã hội học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(394, 'Xã hội học và Nhân học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(395, 'Xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(396, 'Xây dựng Đảng và chính quyền nhà nước', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(397, 'Xét nghiệm y học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(398, 'Xuất bản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(399, 'Xuất bản - Phát hành', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(400, 'Y đa khoa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(401, 'Y học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(402, 'Y học cổ truyền', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(403, 'Y học dự phòng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(404, 'Y sinh học thể dục thể thao', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(405, 'Y tế công cộng', '2017-01-19 22:30:26', '2017-01-19 22:30:26');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL,
  `admin_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_fb_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_twitter_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_gplus_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_linkedin_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_pinterest_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_address` text COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lng` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `admin_name`, `admin_email`, `site_title`, `contact_email`, `contact_name`, `contact_phone`, `site_logo`, `site_fb_link`, `site_twitter_link`, `site_gplus_link`, `site_linkedin_link`, `site_pinterest_link`, `created_at`, `updated_at`, `seo_title`, `seo_keywords`, `seo_description`, `contact_address`, `lat`, `lng`) VALUES
(1, 'Admin', 'admin@admin.com', 'iKnow', 'info@admin.com', 'Contact iKnow', '9234567890', '14809006755mGTvG5Elh.png', 'https://www.facebook.com/example', 'https://www.twitter.com/example', 'https://plus.google.com/u/0/+example', 'https://www.linkedin.com/example', 'https://www.pinterest.com/example', NULL, '2016-12-05 07:18:16', 'iKnow', '', '', '141 Lê Duẩn, Cửa Nam, Hanoi, Vietnam', '21.0231979', '105.84164680000004');

-- --------------------------------------------------------

--
-- Table structure for table `timezones`
--

CREATE TABLE IF NOT EXISTS `timezones` (
  `id` int(10) unsigned NOT NULL,
  `CountryCode` char(2) NOT NULL,
  `Coordinates` char(15) NOT NULL,
  `TimeZone` char(32) NOT NULL,
  `Comments` varchar(85) NOT NULL,
  `UTC_offset` char(8) NOT NULL,
  `UTC_DST_offset` char(8) NOT NULL,
  `Notes` varchar(79) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=549 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `timezones`
--

INSERT INTO `timezones` (`id`, `CountryCode`, `Coordinates`, `TimeZone`, `Comments`, `UTC_offset`, `UTC_DST_offset`, `Notes`) VALUES
(1, 'CI', '+0519-00402', 'Africa/Abidjan', '', '+00:00', '+00:00', ''),
(2, 'GH', '+0533-00013', 'Africa/Accra', '', '+00:00', '+00:00', ''),
(3, 'ET', '+0902+03842', 'Africa/Addis_Ababa', '', '+03:00', '+03:00', ''),
(4, 'DZ', '+3647+00303', 'Africa/Algiers', '', '+01:00', '+01:00', ''),
(5, 'ER', '+1520+03853', 'Africa/Asmara', '', '+03:00', '+03:00', ''),
(6, '', '', 'Africa/Asmera', '', '+03:00', '+03:00', 'Link to Africa/Asmara'),
(7, 'ML', '+1239-00800', 'Africa/Bamako', '', '+00:00', '+00:00', ''),
(8, 'CF', '+0422+01835', 'Africa/Bangui', '', '+01:00', '+01:00', ''),
(9, 'GM', '+1328-01639', 'Africa/Banjul', '', '+00:00', '+00:00', ''),
(10, 'GW', '+1151-01535', 'Africa/Bissau', '', '+00:00', '+00:00', ''),
(11, 'MW', '-1547+03500', 'Africa/Blantyre', '', '+02:00', '+02:00', ''),
(12, 'CG', '-0416+01517', 'Africa/Brazzaville', '', '+01:00', '+01:00', ''),
(13, 'BI', '-0323+02922', 'Africa/Bujumbura', '', '+02:00', '+02:00', ''),
(14, 'EG', '+3003+03115', 'Africa/Cairo', '', '+02:00', '+02:00', 'DST has been canceled since 2011'),
(15, 'MA', '+3339-00735', 'Africa/Casablanca', '', '+00:00', '+01:00', ''),
(16, 'ES', '+3553-00519', 'Africa/Ceuta', 'Ceuta & Melilla', '+01:00', '+02:00', ''),
(17, 'GN', '+0931-01343', 'Africa/Conakry', '', '+00:00', '+00:00', ''),
(18, 'SN', '+1440-01726', 'Africa/Dakar', '', '+00:00', '+00:00', ''),
(19, 'TZ', '-0648+03917', 'Africa/Dar_es_Salaam', '', '+03:00', '+03:00', ''),
(20, 'DJ', '+1136+04309', 'Africa/Djibouti', '', '+03:00', '+03:00', ''),
(21, 'CM', '+0403+00942', 'Africa/Douala', '', '+01:00', '+01:00', ''),
(22, 'EH', '+2709-01312', 'Africa/El_Aaiun', '', '+00:00', '+00:00', ''),
(23, 'SL', '+0830-01315', 'Africa/Freetown', '', '+00:00', '+00:00', ''),
(24, 'BW', '-2439+02555', 'Africa/Gaborone', '', '+02:00', '+02:00', ''),
(25, 'ZW', '-1750+03103', 'Africa/Harare', '', '+02:00', '+02:00', ''),
(26, 'ZA', '-2615+02800', 'Africa/Johannesburg', '', '+02:00', '+02:00', ''),
(27, 'SS', '+0451+03136', 'Africa/Juba', '', '+03:00', '+03:00', ''),
(28, 'UG', '+0019+03225', 'Africa/Kampala', '', '+03:00', '+03:00', ''),
(29, 'SD', '+1536+03232', 'Africa/Khartoum', '', '+03:00', '+03:00', ''),
(30, 'RW', '-0157+03004', 'Africa/Kigali', '', '+02:00', '+02:00', ''),
(31, 'CD', '-0418+01518', 'Africa/Kinshasa', 'west Dem. Rep. of Congo', '+01:00', '+01:00', ''),
(32, 'NG', '+0627+00324', 'Africa/Lagos', '', '+01:00', '+01:00', ''),
(33, 'GA', '+0023+00927', 'Africa/Libreville', '', '+01:00', '+01:00', ''),
(34, 'TG', '+0608+00113', 'Africa/Lome', '', '+00:00', '+00:00', ''),
(35, 'AO', '-0848+01314', 'Africa/Luanda', '', '+01:00', '+01:00', ''),
(36, 'CD', '-1140+02728', 'Africa/Lubumbashi', 'east Dem. Rep. of Congo', '+02:00', '+02:00', ''),
(37, 'ZM', '-1525+02817', 'Africa/Lusaka', '', '+02:00', '+02:00', ''),
(38, 'GQ', '+0345+00847', 'Africa/Malabo', '', '+01:00', '+01:00', ''),
(39, 'MZ', '-2558+03235', 'Africa/Maputo', '', '+02:00', '+02:00', ''),
(40, 'LS', '-2928+02730', 'Africa/Maseru', '', '+02:00', '+02:00', ''),
(41, 'SZ', '-2618+03106', 'Africa/Mbabane', '', '+02:00', '+02:00', ''),
(42, 'SO', '+0204+04522', 'Africa/Mogadishu', '', '+03:00', '+03:00', ''),
(43, 'LR', '+0618-01047', 'Africa/Monrovia', '', '+00:00', '+00:00', ''),
(44, 'KE', '-0117+03649', 'Africa/Nairobi', '', '+03:00', '+03:00', ''),
(45, 'TD', '+1207+01503', 'Africa/Ndjamena', '', '+01:00', '+01:00', ''),
(46, 'NE', '+1331+00207', 'Africa/Niamey', '', '+01:00', '+01:00', ''),
(47, 'MR', '+1806-01557', 'Africa/Nouakchott', '', '+00:00', '+00:00', ''),
(48, 'BF', '+1222-00131', 'Africa/Ouagadougou', '', '+00:00', '+00:00', ''),
(49, 'BJ', '+0629+00237', 'Africa/Porto-Novo', '', '+01:00', '+01:00', ''),
(50, 'ST', '+0020+00644', 'Africa/Sao_Tome', '', '+00:00', '+00:00', ''),
(51, '', '', 'Africa/Timbuktu', '', '+00:00', '+00:00', 'Link to Africa/Bamako'),
(52, 'LY', '+3254+01311', 'Africa/Tripoli', '', '+01:00', '+02:00', ''),
(53, 'TN', '+3648+01011', 'Africa/Tunis', '', '+01:00', '+01:00', ''),
(54, 'NA', '-2234+01706', 'Africa/Windhoek', '', '+01:00', '+02:00', ''),
(55, '', '', 'AKST9AKDT', '', '−09:00', '−08:00', 'Link to America/Anchorage'),
(56, 'US', '+515248-1763929', 'America/Adak', 'Aleutian Islands', '−10:00', '−09:00', ''),
(57, 'US', '+611305-1495401', 'America/Anchorage', 'Alaska Time', '−09:00', '−08:00', ''),
(58, 'AI', '+1812-06304', 'America/Anguilla', '', '−04:00', '−04:00', ''),
(59, 'AG', '+1703-06148', 'America/Antigua', '', '−04:00', '−04:00', ''),
(60, 'BR', '-0712-04812', 'America/Araguaina', 'Tocantins', '−03:00', '−03:00', ''),
(61, 'AR', '-3436-05827', 'America/Argentina/Buenos_Aires', 'Buenos Aires (BA, CF)', '−03:00', '−03:00', ''),
(62, 'AR', '-2828-06547', 'America/Argentina/Catamarca', 'Catamarca (CT), Chubut (CH)', '−03:00', '−03:00', ''),
(63, '', '', 'America/Argentina/ComodRivadavia', '', '−03:00', '−03:00', 'Link to America/Argentina/Catamarca'),
(64, 'AR', '-3124-06411', 'America/Argentina/Cordoba', 'most locations (CB, CC, CN, ER, FM, MN, SE, SF)', '−03:00', '−03:00', ''),
(65, 'AR', '-2411-06518', 'America/Argentina/Jujuy', 'Jujuy (JY)', '−03:00', '−03:00', ''),
(66, 'AR', '-2926-06651', 'America/Argentina/La_Rioja', 'La Rioja (LR)', '−03:00', '−03:00', ''),
(67, 'AR', '-3253-06849', 'America/Argentina/Mendoza', 'Mendoza (MZ)', '−03:00', '−03:00', ''),
(68, 'AR', '-5138-06913', 'America/Argentina/Rio_Gallegos', 'Santa Cruz (SC)', '−03:00', '−03:00', ''),
(69, 'AR', '-2447-06525', 'America/Argentina/Salta', '(SA, LP, NQ, RN)', '−03:00', '−03:00', ''),
(70, 'AR', '-3132-06831', 'America/Argentina/San_Juan', 'San Juan (SJ)', '−03:00', '−03:00', ''),
(71, 'AR', '-3319-06621', 'America/Argentina/San_Luis', 'San Luis (SL)', '−03:00', '−03:00', ''),
(72, 'AR', '-2649-06513', 'America/Argentina/Tucuman', 'Tucuman (TM)', '−03:00', '−03:00', ''),
(73, 'AR', '-5448-06818', 'America/Argentina/Ushuaia', 'Tierra del Fuego (TF)', '−03:00', '−03:00', ''),
(74, 'AW', '+1230-06958', 'America/Aruba', '', '−04:00', '−04:00', ''),
(75, 'PY', '-2516-05740', 'America/Asuncion', '', '−04:00', '−03:00', ''),
(76, 'CA', '+484531-0913718', 'America/Atikokan', 'Eastern Standard Time - Atikokan, Ontario and Southampton I, Nunavut', '−05:00', '−05:00', ''),
(77, '', '', 'America/Atka', '', '−10:00', '−09:00', 'Link to America/Adak'),
(78, 'BR', '-1259-03831', 'America/Bahia', 'Bahia', '−03:00', '−03:00', ''),
(79, 'MX', '+2048-10515', 'America/Bahia_Banderas', 'Mexican Central Time - Bahia de Banderas', '−06:00', '−05:00', ''),
(80, 'BB', '+1306-05937', 'America/Barbados', '', '−04:00', '−04:00', ''),
(81, 'BR', '-0127-04829', 'America/Belem', 'Amapa, E Para', '−03:00', '−03:00', ''),
(82, 'BZ', '+1730-08812', 'America/Belize', '', '−06:00', '−06:00', ''),
(83, 'CA', '+5125-05707', 'America/Blanc-Sablon', 'Atlantic Standard Time - Quebec - Lower North Shore', '−04:00', '−04:00', ''),
(84, 'BR', '+0249-06040', 'America/Boa_Vista', 'Roraima', '−04:00', '−04:00', ''),
(85, 'CO', '+0436-07405', 'America/Bogota', '', '−05:00', '−05:00', ''),
(86, 'US', '+433649-1161209', 'America/Boise', 'Mountain Time - south Idaho & east Oregon', '−07:00', '−06:00', ''),
(87, '', '', 'America/Buenos_Aires', '', '−03:00', '−03:00', 'Link to America/Argentina/Buenos_Aires'),
(88, 'CA', '+690650-1050310', 'America/Cambridge_Bay', 'Mountain Time - west Nunavut', '−07:00', '−06:00', ''),
(89, 'BR', '-2027-05437', 'America/Campo_Grande', 'Mato Grosso do Sul', '−04:00', '−03:00', ''),
(90, 'MX', '+2105-08646', 'America/Cancun', 'Central Time - Quintana Roo', '−06:00', '−05:00', ''),
(91, 'VE', '+1030-06656', 'America/Caracas', '', '−04:30', '−04:30', ''),
(92, '', '', 'America/Catamarca', '', '−03:00', '−03:00', 'Link to America/Argentina/Catamarca'),
(93, 'GF', '+0456-05220', 'America/Cayenne', '', '−03:00', '−03:00', ''),
(94, 'KY', '+1918-08123', 'America/Cayman', '', '−05:00', '−05:00', ''),
(95, 'US', '+415100-0873900', 'America/Chicago', 'Central Time', '−06:00', '−05:00', ''),
(96, 'MX', '+2838-10605', 'America/Chihuahua', 'Mexican Mountain Time - Chihuahua away from US border', '−07:00', '−06:00', ''),
(97, '', '', 'America/Coral_Harbour', '', '−05:00', '−05:00', 'Link to America/Atikokan'),
(98, '', '', 'America/Cordoba', '', '−03:00', '−03:00', 'Link to America/Argentina/Cordoba'),
(99, 'CR', '+0956-08405', 'America/Costa_Rica', '', '−06:00', '−06:00', ''),
(100, 'CA', '+4906-11631', 'America/Creston', 'Mountain Standard Time - Creston, British Columbia', '−07:00', '−07:00', ''),
(101, 'BR', '-1535-05605', 'America/Cuiaba', 'Mato Grosso', '−04:00', '−03:00', ''),
(102, 'CW', '+1211-06900', 'America/Curacao', '', '−04:00', '−04:00', ''),
(103, 'GL', '+7646-01840', 'America/Danmarkshavn', 'east coast, north of Scoresbysund', '+00:00', '+00:00', ''),
(104, 'CA', '+6404-13925', 'America/Dawson', 'Pacific Time - north Yukon', '−08:00', '−07:00', ''),
(105, 'CA', '+5946-12014', 'America/Dawson_Creek', 'Mountain Standard Time - Dawson Creek & Fort Saint John, British Columbia', '−07:00', '−07:00', ''),
(106, 'US', '+394421-1045903', 'America/Denver', 'Mountain Time', '−07:00', '−06:00', ''),
(107, 'US', '+421953-0830245', 'America/Detroit', 'Eastern Time - Michigan - most locations', '−05:00', '−04:00', ''),
(108, 'DM', '+1518-06124', 'America/Dominica', '', '−04:00', '−04:00', ''),
(109, 'CA', '+5333-11328', 'America/Edmonton', 'Mountain Time - Alberta, east British Columbia & west Saskatchewan', '−07:00', '−06:00', ''),
(110, 'BR', '-0640-06952', 'America/Eirunepe', 'W Amazonas', '−04:00', '−04:00', ''),
(111, 'SV', '+1342-08912', 'America/El_Salvador', '', '−06:00', '−06:00', ''),
(112, '', '', 'America/Ensenada', '', '−08:00', '−07:00', 'Link to America/Tijuana'),
(113, 'BR', '-0343-03830', 'America/Fortaleza', 'NE Brazil (MA, PI, CE, RN, PB)', '−03:00', '−03:00', ''),
(114, '', '', 'America/Fort_Wayne', '', '−05:00', '−04:00', 'Link to America/Indiana/Indianapolis'),
(115, 'CA', '+4612-05957', 'America/Glace_Bay', 'Atlantic Time - Nova Scotia - places that did not observe DST 1966-1971', '−04:00', '−03:00', ''),
(116, 'GL', '+6411-05144', 'America/Godthab', 'most locations', '−03:00', '−02:00', ''),
(117, 'CA', '+5320-06025', 'America/Goose_Bay', 'Atlantic Time - Labrador - most locations', '−04:00', '−03:00', ''),
(118, 'TC', '+2128-07108', 'America/Grand_Turk', '', '−05:00', '−04:00', ''),
(119, 'GD', '+1203-06145', 'America/Grenada', '', '−04:00', '−04:00', ''),
(120, 'GP', '+1614-06132', 'America/Guadeloupe', '', '−04:00', '−04:00', ''),
(121, 'GT', '+1438-09031', 'America/Guatemala', '', '−06:00', '−06:00', ''),
(122, 'EC', '-0210-07950', 'America/Guayaquil', 'mainland', '−05:00', '−05:00', ''),
(123, 'GY', '+0648-05810', 'America/Guyana', '', '−04:00', '−04:00', ''),
(124, 'CA', '+4439-06336', 'America/Halifax', 'Atlantic Time - Nova Scotia (most places), PEI', '−04:00', '−03:00', ''),
(125, 'CU', '+2308-08222', 'America/Havana', '', '−05:00', '−04:00', ''),
(126, 'MX', '+2904-11058', 'America/Hermosillo', 'Mountain Standard Time - Sonora', '−07:00', '−07:00', ''),
(127, 'US', '+394606-0860929', 'America/Indiana/Indianapolis', 'Eastern Time - Indiana - most locations', '−05:00', '−04:00', ''),
(128, 'US', '+411745-0863730', 'America/Indiana/Knox', 'Central Time - Indiana - Starke County', '−06:00', '−05:00', ''),
(129, 'US', '+382232-0862041', 'America/Indiana/Marengo', 'Eastern Time - Indiana - Crawford County', '−05:00', '−04:00', ''),
(130, 'US', '+382931-0871643', 'America/Indiana/Petersburg', 'Eastern Time - Indiana - Pike County', '−05:00', '−04:00', ''),
(131, 'US', '+375711-0864541', 'America/Indiana/Tell_City', 'Central Time - Indiana - Perry County', '−06:00', '−05:00', ''),
(132, 'US', '+384452-0850402', 'America/Indiana/Vevay', 'Eastern Time - Indiana - Switzerland County', '−05:00', '−04:00', ''),
(133, 'US', '+384038-0873143', 'America/Indiana/Vincennes', 'Eastern Time - Indiana - Daviess, Dubois, Knox & Martin Counties', '−05:00', '−04:00', ''),
(134, 'US', '+410305-0863611', 'America/Indiana/Winamac', 'Eastern Time - Indiana - Pulaski County', '−05:00', '−04:00', ''),
(135, '', '', 'America/Indianapolis', '', '−05:00', '−04:00', 'Link to America/Indiana/Indianapolis'),
(136, 'CA', '+682059-1334300', 'America/Inuvik', 'Mountain Time - west Northwest Territories', '−07:00', '−06:00', ''),
(137, 'CA', '+6344-06828', 'America/Iqaluit', 'Eastern Time - east Nunavut - most locations', '−05:00', '−04:00', ''),
(138, 'JM', '+1800-07648', 'America/Jamaica', '', '−05:00', '−05:00', ''),
(139, '', '', 'America/Jujuy', '', '−03:00', '−03:00', 'Link to America/Argentina/Jujuy'),
(140, 'US', '+581807-1342511', 'America/Juneau', 'Alaska Time - Alaska panhandle', '−09:00', '−08:00', ''),
(141, 'US', '+381515-0854534', 'America/Kentucky/Louisville', 'Eastern Time - Kentucky - Louisville area', '−05:00', '−04:00', ''),
(142, 'US', '+364947-0845057', 'America/Kentucky/Monticello', 'Eastern Time - Kentucky - Wayne County', '−05:00', '−04:00', ''),
(143, '', '', 'America/Knox_IN', '', '−06:00', '−05:00', 'Link to America/Indiana/Knox'),
(144, 'BQ', '+120903-0681636', 'America/Kralendijk', '', '−04:00', '−04:00', 'Link to America/Curacao'),
(145, 'BO', '-1630-06809', 'America/La_Paz', '', '−04:00', '−04:00', ''),
(146, 'PE', '-1203-07703', 'America/Lima', '', '−05:00', '−05:00', ''),
(147, 'US', '+340308-1181434', 'America/Los_Angeles', 'Pacific Time', '−08:00', '−07:00', ''),
(148, '', '', 'America/Louisville', '', '−05:00', '−04:00', 'Link to America/Kentucky/Louisville'),
(149, 'SX', '+180305-0630250', 'America/Lower_Princes', '', '−04:00', '−04:00', 'Link to America/Curacao'),
(150, 'BR', '-0940-03543', 'America/Maceio', 'Alagoas, Sergipe', '−03:00', '−03:00', ''),
(151, 'NI', '+1209-08617', 'America/Managua', '', '−06:00', '−06:00', ''),
(152, 'BR', '-0308-06001', 'America/Manaus', 'E Amazonas', '−04:00', '−04:00', ''),
(153, 'MF', '+1804-06305', 'America/Marigot', '', '−04:00', '−04:00', 'Link to America/Guadeloupe'),
(154, 'MQ', '+1436-06105', 'America/Martinique', '', '−04:00', '−04:00', ''),
(155, 'MX', '+2550-09730', 'America/Matamoros', 'US Central Time - Coahuila, Durango, Nuevo León, Tamaulipas near US border', '−06:00', '−05:00', ''),
(156, 'MX', '+2313-10625', 'America/Mazatlan', 'Mountain Time - S Baja, Nayarit, Sinaloa', '−07:00', '−06:00', ''),
(157, '', '', 'America/Mendoza', '', '−03:00', '−03:00', 'Link to America/Argentina/Mendoza'),
(158, 'US', '+450628-0873651', 'America/Menominee', 'Central Time - Michigan - Dickinson, Gogebic, Iron & Menominee Counties', '−06:00', '−05:00', ''),
(159, 'MX', '+2058-08937', 'America/Merida', 'Central Time - Campeche, Yucatán', '−06:00', '−05:00', ''),
(160, 'US', '+550737-1313435', 'America/Metlakatla', 'Metlakatla Time - Annette Island', '−08:00', '−08:00', ''),
(161, 'MX', '+1924-09909', 'America/Mexico_City', 'Central Time - most locations', '−06:00', '−05:00', ''),
(162, 'PM', '+4703-05620', 'America/Miquelon', '', '−03:00', '−02:00', ''),
(163, 'CA', '+4606-06447', 'America/Moncton', 'Atlantic Time - New Brunswick', '−04:00', '−03:00', ''),
(164, 'MX', '+2540-10019', 'America/Monterrey', 'Mexican Central Time - Coahuila, Durango, Nuevo León, Tamaulipas away from US border', '−06:00', '−05:00', ''),
(165, 'UY', '-3453-05611', 'America/Montevideo', '', '−03:00', '−02:00', ''),
(166, 'CA', '+4531-07334', 'America/Montreal', 'Eastern Time - Quebec - most locations', '−05:00', '−04:00', ''),
(167, 'MS', '+1643-06213', 'America/Montserrat', '', '−04:00', '−04:00', ''),
(168, 'BS', '+2505-07721', 'America/Nassau', '', '−05:00', '−04:00', ''),
(169, 'US', '+404251-0740023', 'America/New_York', 'Eastern Time', '−05:00', '−04:00', ''),
(170, 'CA', '+4901-08816', 'America/Nipigon', 'Eastern Time - Ontario & Quebec - places that did not observe DST 1967-1973', '−05:00', '−04:00', ''),
(171, 'US', '+643004-1652423', 'America/Nome', 'Alaska Time - west Alaska', '−09:00', '−08:00', ''),
(172, 'BR', '-0351-03225', 'America/Noronha', 'Atlantic islands', '−02:00', '−02:00', ''),
(173, 'US', '+471551-1014640', 'America/North_Dakota/Beulah', 'Central Time - North Dakota - Mercer County', '−06:00', '−05:00', ''),
(174, 'US', '+470659-1011757', 'America/North_Dakota/Center', 'Central Time - North Dakota - Oliver County', '−06:00', '−05:00', ''),
(175, 'US', '+465042-1012439', 'America/North_Dakota/New_Salem', 'Central Time - North Dakota - Morton County (except Mandan area)', '−06:00', '−05:00', ''),
(176, 'MX', '+2934-10425', 'America/Ojinaga', 'US Mountain Time - Chihuahua near US border', '−07:00', '−06:00', ''),
(177, 'PA', '+0858-07932', 'America/Panama', '', '−05:00', '−05:00', ''),
(178, 'CA', '+6608-06544', 'America/Pangnirtung', 'Eastern Time - Pangnirtung, Nunavut', '−05:00', '−04:00', ''),
(179, 'SR', '+0550-05510', 'America/Paramaribo', '', '−03:00', '−03:00', ''),
(180, 'US', '+332654-1120424', 'America/Phoenix', 'Mountain Standard Time - Arizona', '−07:00', '−07:00', ''),
(181, 'HT', '+1832-07220', 'America/Port-au-Prince', '', '−05:00', '−04:00', ''),
(182, '', '', 'America/Porto_Acre', '', '−04:00', '−04:00', 'Link to America/Rio_Branco'),
(183, 'BR', '-0846-06354', 'America/Porto_Velho', 'Rondonia', '−04:00', '−04:00', ''),
(184, 'TT', '+1039-06131', 'America/Port_of_Spain', '', '−04:00', '−04:00', ''),
(185, 'PR', '+182806-0660622', 'America/Puerto_Rico', '', '−04:00', '−04:00', ''),
(186, 'CA', '+4843-09434', 'America/Rainy_River', 'Central Time - Rainy River & Fort Frances, Ontario', '−06:00', '−05:00', ''),
(187, 'CA', '+624900-0920459', 'America/Rankin_Inlet', 'Central Time - central Nunavut', '−06:00', '−05:00', ''),
(188, 'BR', '-0803-03454', 'America/Recife', 'Pernambuco', '−03:00', '−03:00', ''),
(189, 'CA', '+5024-10439', 'America/Regina', 'Central Standard Time - Saskatchewan - most locations', '−06:00', '−06:00', ''),
(190, 'CA', '+744144-0944945', 'America/Resolute', 'Central Standard Time - Resolute, Nunavut', '−06:00', '−05:00', ''),
(191, 'BR', '-0958-06748', 'America/Rio_Branco', 'Acre', '−04:00', '−04:00', ''),
(192, '', '', 'America/Rosario', '', '−03:00', '−03:00', 'Link to America/Argentina/Cordoba'),
(193, 'BR', '-0226-05452', 'America/Santarem', 'W Para', '−03:00', '−03:00', ''),
(194, 'MX', '+3018-11452', 'America/Santa_Isabel', 'Mexican Pacific Time - Baja California away from US border', '−08:00', '−07:00', ''),
(195, 'CL', '-3327-07040', 'America/Santiago', 'most locations', '−04:00', '−03:00', ''),
(196, 'DO', '+1828-06954', 'America/Santo_Domingo', '', '−04:00', '−04:00', ''),
(197, 'BR', '-2332-04637', 'America/Sao_Paulo', 'S & SE Brazil (GO, DF, MG, ES, RJ, SP, PR, SC, RS)', '−03:00', '−02:00', ''),
(198, 'GL', '+7029-02158', 'America/Scoresbysund', 'Scoresbysund / Ittoqqortoormiit', '−01:00', '+00:00', ''),
(199, 'US', '+364708-1084111', 'America/Shiprock', 'Mountain Time - Navajo', '−07:00', '−06:00', 'Link to America/Denver'),
(200, 'US', '+571035-1351807', 'America/Sitka', 'Alaska Time - southeast Alaska panhandle', '−09:00', '−08:00', ''),
(201, 'BL', '+1753-06251', 'America/St_Barthelemy', '', '−04:00', '−04:00', 'Link to America/Guadeloupe'),
(202, 'CA', '+4734-05243', 'America/St_Johns', 'Newfoundland Time, including SE Labrador', '−03:30', '−02:30', ''),
(203, 'KN', '+1718-06243', 'America/St_Kitts', '', '−04:00', '−04:00', ''),
(204, 'LC', '+1401-06100', 'America/St_Lucia', '', '−04:00', '−04:00', ''),
(205, 'VI', '+1821-06456', 'America/St_Thomas', '', '−04:00', '−04:00', ''),
(206, 'VC', '+1309-06114', 'America/St_Vincent', '', '−04:00', '−04:00', ''),
(207, 'CA', '+5017-10750', 'America/Swift_Current', 'Central Standard Time - Saskatchewan - midwest', '−06:00', '−06:00', ''),
(208, 'HN', '+1406-08713', 'America/Tegucigalpa', '', '−06:00', '−06:00', ''),
(209, 'GL', '+7634-06847', 'America/Thule', 'Thule / Pituffik', '−04:00', '−03:00', ''),
(210, 'CA', '+4823-08915', 'America/Thunder_Bay', 'Eastern Time - Thunder Bay, Ontario', '−05:00', '−04:00', ''),
(211, 'MX', '+3232-11701', 'America/Tijuana', 'US Pacific Time - Baja California near US border', '−08:00', '−07:00', ''),
(212, 'CA', '+4339-07923', 'America/Toronto', 'Eastern Time - Ontario - most locations', '−05:00', '−04:00', ''),
(213, 'VG', '+1827-06437', 'America/Tortola', '', '−04:00', '−04:00', ''),
(214, 'CA', '+4916-12307', 'America/Vancouver', 'Pacific Time - west British Columbia', '−08:00', '−07:00', ''),
(215, '', '', 'America/Virgin', '', '−04:00', '−04:00', 'Link to America/St_Thomas'),
(216, 'CA', '+6043-13503', 'America/Whitehorse', 'Pacific Time - south Yukon', '−08:00', '−07:00', ''),
(217, 'CA', '+4953-09709', 'America/Winnipeg', 'Central Time - Manitoba & west Ontario', '−06:00', '−05:00', ''),
(218, 'US', '+593249-1394338', 'America/Yakutat', 'Alaska Time - Alaska panhandle neck', '−09:00', '−08:00', ''),
(219, 'CA', '+6227-11421', 'America/Yellowknife', 'Mountain Time - central Northwest Territories', '−07:00', '−06:00', ''),
(220, 'AQ', '-6617+11031', 'Antarctica/Casey', 'Casey Station, Bailey Peninsula', '+11:00', '+08:00', ''),
(221, 'AQ', '-6835+07758', 'Antarctica/Davis', 'Davis Station, Vestfold Hills', '+05:00', '+07:00', ''),
(222, 'AQ', '-6640+14001', 'Antarctica/DumontDUrville', 'Dumont-d''Urville Station, Terre Adelie', '+10:00', '+10:00', ''),
(223, 'AQ', '-5430+15857', 'Antarctica/Macquarie', 'Macquarie Island Station, Macquarie Island', '+11:00', '+11:00', ''),
(224, 'AQ', '-6736+06253', 'Antarctica/Mawson', 'Mawson Station, Holme Bay', '+05:00', '+05:00', ''),
(225, 'AQ', '-7750+16636', 'Antarctica/McMurdo', 'McMurdo Station, Ross Island', '+12:00', '+13:00', ''),
(226, 'AQ', '-6448-06406', 'Antarctica/Palmer', 'Palmer Station, Anvers Island', '−04:00', '−03:00', ''),
(227, 'AQ', '-6734-06808', 'Antarctica/Rothera', 'Rothera Station, Adelaide Island', '−03:00', '−03:00', ''),
(228, 'AQ', '-9000+00000', 'Antarctica/South_Pole', 'Amundsen-Scott Station, South Pole', '+12:00', '+13:00', 'Link to Antarctica/McMurdo'),
(229, 'AQ', '-690022+0393524', 'Antarctica/Syowa', 'Syowa Station, E Ongul I', '+03:00', '+03:00', ''),
(230, 'AQ', '-7824+10654', 'Antarctica/Vostok', 'Vostok Station, Lake Vostok', '+06:00', '+06:00', ''),
(231, 'SJ', '+7800+01600', 'Arctic/Longyearbyen', '', '+01:00', '+02:00', 'Link to Europe/Oslo'),
(232, 'YE', '+1245+04512', 'Asia/Aden', '', '+03:00', '+03:00', ''),
(233, 'KZ', '+4315+07657', 'Asia/Almaty', 'most locations', '+06:00', '+06:00', ''),
(234, 'JO', '+3157+03556', 'Asia/Amman', '', '+03:00', '+03:00', ''),
(235, 'RU', '+6445+17729', 'Asia/Anadyr', 'Moscow+08 - Bering Sea', '+12:00', '+12:00', ''),
(236, 'KZ', '+4431+05016', 'Asia/Aqtau', 'Atyrau (Atirau, Gur''yev), Mangghystau (Mankistau)', '+05:00', '+05:00', ''),
(237, 'KZ', '+5017+05710', 'Asia/Aqtobe', 'Aqtobe (Aktobe)', '+05:00', '+05:00', ''),
(238, 'TM', '+3757+05823', 'Asia/Ashgabat', '', '+05:00', '+05:00', ''),
(239, '', '', 'Asia/Ashkhabad', '', '+05:00', '+05:00', 'Link to Asia/Ashgabat'),
(240, 'IQ', '+3321+04425', 'Asia/Baghdad', '', '+03:00', '+03:00', ''),
(241, 'BH', '+2623+05035', 'Asia/Bahrain', '', '+03:00', '+03:00', ''),
(242, 'AZ', '+4023+04951', 'Asia/Baku', '', '+04:00', '+05:00', ''),
(243, 'TH', '+1345+10031', 'Asia/Bangkok', '', '+07:00', '+07:00', ''),
(244, 'LB', '+3353+03530', 'Asia/Beirut', '', '+02:00', '+03:00', ''),
(245, 'KG', '+4254+07436', 'Asia/Bishkek', '', '+06:00', '+06:00', ''),
(246, 'BN', '+0456+11455', 'Asia/Brunei', '', '+08:00', '+08:00', ''),
(247, '', '', 'Asia/Calcutta', '', '+05:30', '+05:30', 'Link to Asia/Kolkata'),
(248, 'MN', '+4804+11430', 'Asia/Choibalsan', 'Dornod, Sukhbaatar', '+08:00', '+08:00', ''),
(249, 'CN', '+2934+10635', 'Asia/Chongqing', 'central China - Sichuan, Yunnan, Guangxi, Shaanxi, Guizhou, etc.', '+08:00', '+08:00', 'Covering historic Kansu-Szechuan time zone.'),
(250, '', '', 'Asia/Chungking', '', '+08:00', '+08:00', 'Link to Asia/Chongqing'),
(251, 'LK', '+0656+07951', 'Asia/Colombo', '', '+05:30', '+05:30', ''),
(252, '', '', 'Asia/Dacca', '', '+06:00', '+06:00', 'Link to Asia/Dhaka'),
(253, 'SY', '+3330+03618', 'Asia/Damascus', '', '+02:00', '+03:00', ''),
(254, 'BD', '+2343+09025', 'Asia/Dhaka', '', '+06:00', '+06:00', ''),
(255, 'TL', '-0833+12535', 'Asia/Dili', '', '+09:00', '+09:00', ''),
(256, 'AE', '+2518+05518', 'Asia/Dubai', '', '+04:00', '+04:00', ''),
(257, 'TJ', '+3835+06848', 'Asia/Dushanbe', '', '+05:00', '+05:00', ''),
(258, 'PS', '+3130+03428', 'Asia/Gaza', 'Gaza Strip', '+02:00', '+03:00', ''),
(259, 'CN', '+4545+12641', 'Asia/Harbin', 'Heilongjiang (except Mohe), Jilin', '+08:00', '+08:00', 'Covering historic Changpai time zone.'),
(260, 'PS', '+313200+0350542', 'Asia/Hebron', 'West Bank', '+02:00', '+03:00', ''),
(261, 'HK', '+2217+11409', 'Asia/Hong_Kong', '', '+08:00', '+08:00', ''),
(262, 'MN', '+4801+09139', 'Asia/Hovd', 'Bayan-Olgiy, Govi-Altai, Hovd, Uvs, Zavkhan', '+07:00', '+07:00', ''),
(263, 'VN', '+1045+10640', 'Asia/Ho_Chi_Minh', '', '+07:00', '+07:00', ''),
(264, 'RU', '+5216+10420', 'Asia/Irkutsk', 'Moscow+05 - Lake Baikal', '+09:00', '+09:00', ''),
(265, '', '', 'Asia/Istanbul', '', '+02:00', '+03:00', 'Link to Europe/Istanbul'),
(266, 'ID', '-0610+10648', 'Asia/Jakarta', 'Java & Sumatra', '+07:00', '+07:00', ''),
(267, 'ID', '-0232+14042', 'Asia/Jayapura', 'west New Guinea (Irian Jaya) & Malukus (Moluccas)', '+09:00', '+09:00', ''),
(268, 'IL', '+3146+03514', 'Asia/Jerusalem', '', '+02:00', '+03:00', ''),
(269, 'AF', '+3431+06912', 'Asia/Kabul', '', '+04:30', '+04:30', ''),
(270, 'RU', '+5301+15839', 'Asia/Kamchatka', 'Moscow+08 - Kamchatka', '+12:00', '+12:00', ''),
(271, 'PK', '+2452+06703', 'Asia/Karachi', '', '+05:00', '+05:00', ''),
(272, 'CN', '+3929+07559', 'Asia/Kashgar', 'west Tibet & Xinjiang', '+08:00', '+08:00', 'Covering historic Kunlun time zone.'),
(273, 'NP', '+2743+08519', 'Asia/Kathmandu', '', '+05:45', '+05:45', ''),
(274, '', '', 'Asia/Katmandu', '', '+05:45', '+05:45', 'Link to Asia/Kathmandu'),
(275, 'IN', '+2232+08822', 'Asia/Kolkata', '', '+05:30', '+05:30', 'Note: Different zones in history, see Time in India.'),
(276, 'RU', '+5601+09250', 'Asia/Krasnoyarsk', 'Moscow+04 - Yenisei River', '+08:00', '+08:00', ''),
(277, 'MY', '+0310+10142', 'Asia/Kuala_Lumpur', 'peninsular Malaysia', '+08:00', '+08:00', ''),
(278, 'MY', '+0133+11020', 'Asia/Kuching', 'Sabah & Sarawak', '+08:00', '+08:00', ''),
(279, 'KW', '+2920+04759', 'Asia/Kuwait', '', '+03:00', '+03:00', ''),
(280, '', '', 'Asia/Macao', '', '+08:00', '+08:00', 'Link to Asia/Macau'),
(281, 'MO', '+2214+11335', 'Asia/Macau', '', '+08:00', '+08:00', ''),
(282, 'RU', '+5934+15048', 'Asia/Magadan', 'Moscow+08 - Magadan', '+12:00', '+12:00', ''),
(283, 'ID', '-0507+11924', 'Asia/Makassar', 'east & south Borneo, Sulawesi (Celebes), Bali, Nusa Tenggara, west Timor', '+08:00', '+08:00', ''),
(284, 'PH', '+1435+12100', 'Asia/Manila', '', '+08:00', '+08:00', ''),
(285, 'OM', '+2336+05835', 'Asia/Muscat', '', '+04:00', '+04:00', ''),
(286, 'CY', '+3510+03322', 'Asia/Nicosia', '', '+02:00', '+03:00', ''),
(287, 'RU', '+5345+08707', 'Asia/Novokuznetsk', 'Moscow+03 - Novokuznetsk', '+07:00', '+07:00', ''),
(288, 'RU', '+5502+08255', 'Asia/Novosibirsk', 'Moscow+03 - Novosibirsk', '+07:00', '+07:00', ''),
(289, 'RU', '+5500+07324', 'Asia/Omsk', 'Moscow+03 - west Siberia', '+07:00', '+07:00', ''),
(290, 'KZ', '+5113+05121', 'Asia/Oral', 'West Kazakhstan', '+05:00', '+05:00', ''),
(291, 'KH', '+1133+10455', 'Asia/Phnom_Penh', '', '+07:00', '+07:00', ''),
(292, 'ID', '-0002+10920', 'Asia/Pontianak', 'west & central Borneo', '+07:00', '+07:00', ''),
(293, 'KP', '+3901+12545', 'Asia/Pyongyang', '', '+09:00', '+09:00', ''),
(294, 'QA', '+2517+05132', 'Asia/Qatar', '', '+03:00', '+03:00', ''),
(295, 'KZ', '+4448+06528', 'Asia/Qyzylorda', 'Qyzylorda (Kyzylorda, Kzyl-Orda)', '+06:00', '+06:00', ''),
(296, 'MM', '+1647+09610', 'Asia/Rangoon', '', '+06:30', '+06:30', ''),
(297, 'SA', '+2438+04643', 'Asia/Riyadh', '', '+03:00', '+03:00', ''),
(298, '', '', 'Asia/Saigon', '', '+07:00', '+07:00', 'Link to Asia/Ho_Chi_Minh'),
(299, 'RU', '+4658+14242', 'Asia/Sakhalin', 'Moscow+07 - Sakhalin Island', '+11:00', '+11:00', ''),
(300, 'UZ', '+3940+06648', 'Asia/Samarkand', 'west Uzbekistan', '+05:00', '+05:00', ''),
(301, 'KR', '+3733+12658', 'Asia/Seoul', '', '+09:00', '+09:00', ''),
(302, 'CN', '+3114+12128', 'Asia/Shanghai', 'east China - Beijing, Guangdong, Shanghai, etc.', '+08:00', '+08:00', 'Covering historic Chungyuan time zone.'),
(303, 'SG', '+0117+10351', 'Asia/Singapore', '', '+08:00', '+08:00', ''),
(304, 'TW', '+2503+12130', 'Asia/Taipei', '', '+08:00', '+08:00', ''),
(305, 'UZ', '+4120+06918', 'Asia/Tashkent', 'east Uzbekistan', '+05:00', '+05:00', ''),
(306, 'GE', '+4143+04449', 'Asia/Tbilisi', '', '+04:00', '+04:00', ''),
(307, 'IR', '+3540+05126', 'Asia/Tehran', '', '+03:30', '+04:30', ''),
(308, '', '', 'Asia/Tel_Aviv', '', '+02:00', '+03:00', 'Link to Asia/Jerusalem'),
(309, '', '', 'Asia/Thimbu', '', '+06:00', '+06:00', 'Link to Asia/Thimphu'),
(310, 'BT', '+2728+08939', 'Asia/Thimphu', '', '+06:00', '+06:00', ''),
(311, 'JP', '+353916+1394441', 'Asia/Tokyo', '', '+09:00', '+09:00', ''),
(312, '', '', 'Asia/Ujung_Pandang', '', '+08:00', '+08:00', 'Link to Asia/Makassar'),
(313, 'MN', '+4755+10653', 'Asia/Ulaanbaatar', 'most locations', '+08:00', '+08:00', ''),
(314, '', '', 'Asia/Ulan_Bator', '', '+08:00', '+08:00', 'Link to Asia/Ulaanbaatar'),
(315, 'CN', '+4348+08735', 'Asia/Urumqi', 'most of Tibet & Xinjiang', '+08:00', '+08:00', 'Covering historic Sinkiang-Tibet time zone.'),
(316, 'LA', '+1758+10236', 'Asia/Vientiane', '', '+07:00', '+07:00', ''),
(317, 'RU', '+4310+13156', 'Asia/Vladivostok', 'Moscow+07 - Amur River', '+11:00', '+11:00', ''),
(318, 'RU', '+6200+12940', 'Asia/Yakutsk', 'Moscow+06 - Lena River', '+10:00', '+10:00', ''),
(319, 'RU', '+5651+06036', 'Asia/Yekaterinburg', 'Moscow+02 - Urals', '+06:00', '+06:00', ''),
(320, 'AM', '+4011+04430', 'Asia/Yerevan', '', '+04:00', '+04:00', ''),
(321, 'PT', '+3744-02540', 'Atlantic/Azores', 'Azores', '−01:00', '+00:00', ''),
(322, 'BM', '+3217-06446', 'Atlantic/Bermuda', '', '−04:00', '−03:00', ''),
(323, 'ES', '+2806-01524', 'Atlantic/Canary', 'Canary Islands', '+00:00', '+01:00', ''),
(324, 'CV', '+1455-02331', 'Atlantic/Cape_Verde', '', '−01:00', '−01:00', ''),
(325, '', '', 'Atlantic/Faeroe', '', '+00:00', '+01:00', 'Link to Atlantic/Faroe'),
(326, 'FO', '+6201-00646', 'Atlantic/Faroe', '', '+00:00', '+01:00', ''),
(327, '', '', 'Atlantic/Jan_Mayen', '', '+01:00', '+02:00', 'Link to Europe/Oslo'),
(328, 'PT', '+3238-01654', 'Atlantic/Madeira', 'Madeira Islands', '+00:00', '+01:00', ''),
(329, 'IS', '+6409-02151', 'Atlantic/Reykjavik', '', '+00:00', '+00:00', ''),
(330, 'GS', '-5416-03632', 'Atlantic/South_Georgia', '', '−02:00', '−02:00', ''),
(331, 'FK', '-5142-05751', 'Atlantic/Stanley', '', '−03:00', '−03:00', ''),
(332, 'SH', '-1555-00542', 'Atlantic/St_Helena', '', '+00:00', '+00:00', ''),
(333, '', '', 'Australia/ACT', '', '+10:00', '+11:00', 'Link to Australia/Sydney'),
(334, 'AU', '-3455+13835', 'Australia/Adelaide', 'South Australia', '+09:30', '+10:30', ''),
(335, 'AU', '-2728+15302', 'Australia/Brisbane', 'Queensland - most locations', '+10:00', '+10:00', ''),
(336, 'AU', '-3157+14127', 'Australia/Broken_Hill', 'New South Wales - Yancowinna', '+09:30', '+10:30', ''),
(337, '', '', 'Australia/Canberra', '', '+10:00', '+11:00', 'Link to Australia/Sydney'),
(338, 'AU', '-3956+14352', 'Australia/Currie', 'Tasmania - King Island', '+10:00', '+11:00', ''),
(339, 'AU', '-1228+13050', 'Australia/Darwin', 'Northern Territory', '+09:30', '+09:30', ''),
(340, 'AU', '-3143+12852', 'Australia/Eucla', 'Western Australia - Eucla area', '+08:45', '+08:45', ''),
(341, 'AU', '-4253+14719', 'Australia/Hobart', 'Tasmania - most locations', '+10:00', '+11:00', ''),
(342, '', '', 'Australia/LHI', '', '+10:30', '+11:00', 'Link to Australia/Lord_Howe'),
(343, 'AU', '-2016+14900', 'Australia/Lindeman', 'Queensland - Holiday Islands', '+10:00', '+10:00', ''),
(344, 'AU', '-3133+15905', 'Australia/Lord_Howe', 'Lord Howe Island', '+10:30', '+11:00', ''),
(345, 'AU', '-3749+14458', 'Australia/Melbourne', 'Victoria', '+10:00', '+11:00', ''),
(346, '', '', 'Australia/North', '', '+09:30', '+09:30', 'Link to Australia/Darwin'),
(347, '', '', 'Australia/NSW', '', '+10:00', '+11:00', 'Link to Australia/Sydney'),
(348, 'AU', '-3157+11551', 'Australia/Perth', 'Western Australia - most locations', '+08:00', '+08:00', ''),
(349, '', '', 'Australia/Queensland', '', '+10:00', '+10:00', 'Link to Australia/Brisbane'),
(350, '', '', 'Australia/South', '', '+09:30', '+10:30', 'Link to Australia/Adelaide'),
(351, 'AU', '-3352+15113', 'Australia/Sydney', 'New South Wales - most locations', '+10:00', '+11:00', ''),
(352, '', '', 'Australia/Tasmania', '', '+10:00', '+11:00', 'Link to Australia/Hobart'),
(353, '', '', 'Australia/Victoria', '', '+10:00', '+11:00', 'Link to Australia/Melbourne'),
(354, '', '', 'Australia/West', '', '+08:00', '+08:00', 'Link to Australia/Perth'),
(355, '', '', 'Australia/Yancowinna', '', '+09:30', '+10:30', 'Link to Australia/Broken_Hill'),
(356, '', '', 'Brazil/Acre', '', '−04:00', '−04:00', 'Link to America/Rio_Branco'),
(357, '', '', 'Brazil/DeNoronha', '', '−02:00', '−02:00', 'Link to America/Noronha'),
(358, '', '', 'Brazil/East', '', '−03:00', '−02:00', 'Link to America/Sao_Paulo'),
(359, '', '', 'Brazil/West', '', '−04:00', '−04:00', 'Link to America/Manaus'),
(360, '', '', 'Canada/Atlantic', '', '−04:00', '−03:00', 'Link to America/Halifax'),
(361, '', '', 'Canada/Central', '', '−06:00', '−05:00', 'Link to America/Winnipeg'),
(362, '', '', 'Canada/East-Saskatchewan', '', '−06:00', '−06:00', 'Link to America/Regina'),
(363, '', '', 'Canada/Eastern', '', '−05:00', '−04:00', 'Link to America/Toronto'),
(364, '', '', 'Canada/Mountain', '', '−07:00', '−06:00', 'Link to America/Edmonton'),
(365, '', '', 'Canada/Newfoundland', '', '−03:30', '−02:30', 'Link to America/St_Johns'),
(366, '', '', 'Canada/Pacific', '', '−08:00', '−07:00', 'Link to America/Vancouver'),
(367, '', '', 'Canada/Saskatchewan', '', '−06:00', '−06:00', 'Link to America/Regina'),
(368, '', '', 'Canada/Yukon', '', '−08:00', '−07:00', 'Link to America/Whitehorse'),
(369, '', '', 'CET', '', '+01:00', '+02:00', ''),
(370, '', '', 'Chile/Continental', '', '−04:00', '−03:00', 'Link to America/Santiago'),
(371, '', '', 'Chile/EasterIsland', '', '−06:00', '−05:00', 'Link to Pacific/Easter'),
(372, '', '', 'CST6CDT', '', '−06:00', '−05:00', ''),
(373, '', '', 'Cuba', '', '−05:00', '−04:00', 'Link to America/Havana'),
(374, '', '', 'EET', '', '+02:00', '+03:00', ''),
(375, '', '', 'Egypt', '', '+02:00', '+02:00', 'Link to Africa/Cairo'),
(376, '', '', 'Eire', '', '+00:00', '+01:00', 'Link to Europe/Dublin'),
(377, '', '', 'EST', '', '−05:00', '−05:00', ''),
(378, '', '', 'EST5EDT', '', '−05:00', '−04:00', ''),
(379, '', '', 'Etc./GMT', '', '+00:00', '+00:00', 'Link to UTC'),
(380, '', '', 'Etc./GMT+0', '', '+00:00', '+00:00', 'Link to UTC'),
(381, '', '', 'Etc./UCT', '', '+00:00', '+00:00', 'Link to UTC'),
(382, '', '', 'Etc./Universal', '', '+00:00', '+00:00', 'Link to UTC'),
(383, '', '', 'Etc./UTC', '', '+00:00', '+00:00', 'Link to UTC'),
(384, '', '', 'Etc./Zulu', '', '+00:00', '+00:00', 'Link to UTC'),
(385, 'NL', '+5222+00454', 'Europe/Amsterdam', '', '+01:00', '+02:00', ''),
(386, 'AD', '+4230+00131', 'Europe/Andorra', '', '+01:00', '+02:00', ''),
(387, 'GR', '+3758+02343', 'Europe/Athens', '', '+02:00', '+03:00', ''),
(388, '', '', 'Europe/Belfast', '', '+00:00', '+01:00', 'Link to Europe/London'),
(389, 'RS', '+4450+02030', 'Europe/Belgrade', '', '+01:00', '+02:00', ''),
(390, 'DE', '+5230+01322', 'Europe/Berlin', '', '+01:00', '+02:00', 'In 1945, the Trizone did not follow Berlin''s switch to DST, see Time in Germany'),
(391, 'SK', '+4809+01707', 'Europe/Bratislava', '', '+01:00', '+02:00', 'Link to Europe/Prague'),
(392, 'BE', '+5050+00420', 'Europe/Brussels', '', '+01:00', '+02:00', ''),
(393, 'RO', '+4426+02606', 'Europe/Bucharest', '', '+02:00', '+03:00', ''),
(394, 'HU', '+4730+01905', 'Europe/Budapest', '', '+01:00', '+02:00', ''),
(395, 'MD', '+4700+02850', 'Europe/Chisinau', '', '+02:00', '+03:00', ''),
(396, 'DK', '+5540+01235', 'Europe/Copenhagen', '', '+01:00', '+02:00', ''),
(397, 'IE', '+5320-00615', 'Europe/Dublin', '', '+00:00', '+01:00', ''),
(398, 'GI', '+3608-00521', 'Europe/Gibraltar', '', '+01:00', '+02:00', ''),
(399, 'GG', '+4927-00232', 'Europe/Guernsey', '', '+00:00', '+01:00', 'Link to Europe/London'),
(400, 'FI', '+6010+02458', 'Europe/Helsinki', '', '+02:00', '+03:00', ''),
(401, 'IM', '+5409-00428', 'Europe/Isle_of_Man', '', '+00:00', '+01:00', 'Link to Europe/London'),
(402, 'TR', '+4101+02858', 'Europe/Istanbul', '', '+02:00', '+03:00', ''),
(403, 'JE', '+4912-00207', 'Europe/Jersey', '', '+00:00', '+01:00', 'Link to Europe/London'),
(404, 'RU', '+5443+02030', 'Europe/Kaliningrad', 'Moscow-01 - Kaliningrad', '+03:00', '+03:00', ''),
(405, 'UA', '+5026+03031', 'Europe/Kiev', 'most locations', '+02:00', '+03:00', ''),
(406, 'PT', '+3843-00908', 'Europe/Lisbon', 'mainland', '+00:00', '+01:00', ''),
(407, 'SI', '+4603+01431', 'Europe/Ljubljana', '', '+01:00', '+02:00', 'Link to Europe/Belgrade'),
(408, 'GB', '+513030-0000731', 'Europe/London', '', '+00:00', '+01:00', ''),
(409, 'LU', '+4936+00609', 'Europe/Luxembourg', '', '+01:00', '+02:00', ''),
(410, 'ES', '+4024-00341', 'Europe/Madrid', 'mainland', '+01:00', '+02:00', ''),
(411, 'MT', '+3554+01431', 'Europe/Malta', '', '+01:00', '+02:00', ''),
(412, 'AX', '+6006+01957', 'Europe/Mariehamn', '', '+02:00', '+03:00', 'Link to Europe/Helsinki'),
(413, 'BY', '+5354+02734', 'Europe/Minsk', '', '+03:00', '+03:00', ''),
(414, 'MC', '+4342+00723', 'Europe/Monaco', '', '+01:00', '+02:00', ''),
(415, 'RU', '+5545+03735', 'Europe/Moscow', 'Moscow+00 - west Russia', '+04:00', '+04:00', ''),
(416, '', '', 'Europe/Nicosia', '', '+02:00', '+03:00', 'Link to Asia/Nicosia'),
(417, 'NO', '+5955+01045', 'Europe/Oslo', '', '+01:00', '+02:00', ''),
(418, 'FR', '+4852+00220', 'Europe/Paris', '', '+01:00', '+02:00', ''),
(419, 'ME', '+4226+01916', 'Europe/Podgorica', '', '+01:00', '+02:00', 'Link to Europe/Belgrade'),
(420, 'CZ', '+5005+01426', 'Europe/Prague', '', '+01:00', '+02:00', ''),
(421, 'LV', '+5657+02406', 'Europe/Riga', '', '+02:00', '+03:00', ''),
(422, 'IT', '+4154+01229', 'Europe/Rome', '', '+01:00', '+02:00', ''),
(423, 'RU', '+5312+05009', 'Europe/Samara', 'Moscow+00 - Samara, Udmurtia', '+04:00', '+04:00', ''),
(424, 'SM', '+4355+01228', 'Europe/San_Marino', '', '+01:00', '+02:00', 'Link to Europe/Rome'),
(425, 'BA', '+4352+01825', 'Europe/Sarajevo', '', '+01:00', '+02:00', 'Link to Europe/Belgrade'),
(426, 'UA', '+4457+03406', 'Europe/Simferopol', 'central Crimea', '+02:00', '+03:00', ''),
(427, 'MK', '+4159+02126', 'Europe/Skopje', '', '+01:00', '+02:00', 'Link to Europe/Belgrade'),
(428, 'BG', '+4241+02319', 'Europe/Sofia', '', '+02:00', '+03:00', ''),
(429, 'SE', '+5920+01803', 'Europe/Stockholm', '', '+01:00', '+02:00', ''),
(430, 'EE', '+5925+02445', 'Europe/Tallinn', '', '+02:00', '+03:00', ''),
(431, 'AL', '+4120+01950', 'Europe/Tirane', '', '+01:00', '+02:00', ''),
(432, '', '', 'Europe/Tiraspol', '', '+02:00', '+03:00', 'Link to Europe/Chisinau'),
(433, 'UA', '+4837+02218', 'Europe/Uzhgorod', 'Ruthenia', '+02:00', '+03:00', ''),
(434, 'LI', '+4709+00931', 'Europe/Vaduz', '', '+01:00', '+02:00', ''),
(435, 'VA', '+415408+0122711', 'Europe/Vatican', '', '+01:00', '+02:00', 'Link to Europe/Rome'),
(436, 'AT', '+4813+01620', 'Europe/Vienna', '', '+01:00', '+02:00', ''),
(437, 'LT', '+5441+02519', 'Europe/Vilnius', '', '+02:00', '+03:00', ''),
(438, 'RU', '+4844+04425', 'Europe/Volgograd', 'Moscow+00 - Caspian Sea', '+04:00', '+04:00', ''),
(439, 'PL', '+5215+02100', 'Europe/Warsaw', '', '+01:00', '+02:00', ''),
(440, 'HR', '+4548+01558', 'Europe/Zagreb', '', '+01:00', '+02:00', 'Link to Europe/Belgrade'),
(441, 'UA', '+4750+03510', 'Europe/Zaporozhye', 'Zaporozh''ye, E Lugansk / Zaporizhia, E Luhansk', '+02:00', '+03:00', ''),
(442, 'CH', '+4723+00832', 'Europe/Zurich', '', '+01:00', '+02:00', ''),
(443, '', '', 'GB', '', '+00:00', '+01:00', 'Link to Europe/London'),
(444, '', '', 'GB-Eire', '', '+00:00', '+01:00', 'Link to Europe/London'),
(445, '', '', 'GMT', '', '+00:00', '+00:00', 'Link to UTC'),
(446, '', '', 'GMT+0', '', '+00:00', '+00:00', 'Link to UTC'),
(447, '', '', 'GMT-0', '', '+00:00', '+00:00', 'Link to UTC'),
(448, '', '', 'GMT0', '', '+00:00', '+00:00', 'Link to UTC'),
(449, '', '', 'Greenwich', '', '+00:00', '+00:00', 'Link to UTC'),
(450, '', '', 'Hong Kong', '', '+08:00', '+08:00', 'Link to Asia/Hong_Kong'),
(451, '', '', 'HST', '', '−10:00', '−10:00', ''),
(452, '', '', 'Iceland', '', '+00:00', '+00:00', 'Link to Atlantic/Reykjavik'),
(453, 'MG', '-1855+04731', 'Indian/Antananarivo', '', '+03:00', '+03:00', ''),
(454, 'IO', '-0720+07225', 'Indian/Chagos', '', '+06:00', '+06:00', ''),
(455, 'CX', '-1025+10543', 'Indian/Christmas', '', '+07:00', '+07:00', ''),
(456, 'CC', '-1210+09655', 'Indian/Cocos', '', '+06:30', '+06:30', ''),
(457, 'KM', '-1141+04316', 'Indian/Comoro', '', '+03:00', '+03:00', ''),
(458, 'TF', '-492110+0701303', 'Indian/Kerguelen', '', '+05:00', '+05:00', ''),
(459, 'SC', '-0440+05528', 'Indian/Mahe', '', '+04:00', '+04:00', ''),
(460, 'MV', '+0410+07330', 'Indian/Maldives', '', '+05:00', '+05:00', ''),
(461, 'MU', '-2010+05730', 'Indian/Mauritius', '', '+04:00', '+04:00', ''),
(462, 'YT', '-1247+04514', 'Indian/Mayotte', '', '+03:00', '+03:00', ''),
(463, 'RE', '-2052+05528', 'Indian/Reunion', '', '+04:00', '+04:00', ''),
(464, '', '', 'Iran', '', '+03:30', '+04:30', 'Link to Asia/Tehran'),
(465, '', '', 'Israel', '', '+02:00', '+03:00', 'Link to Asia/Jerusalem'),
(466, '', '', 'Jamaica', '', '−05:00', '−05:00', 'Link to America/Jamaica'),
(467, '', '', 'Japan', '', '+09:00', '+09:00', 'Link to Asia/Tokyo'),
(468, '', '', 'JST-9', '', '+09:00', '+09:00', 'Link to Asia/Tokyo'),
(469, '', '', 'Kwajalein', '', '+12:00', '+12:00', 'Link to Pacific/Kwajalein'),
(470, '', '', 'Libya', '', '+02:00', '+02:00', 'Link to Africa/Tripoli'),
(471, '', '', 'MET', '', '+01:00', '+02:00', ''),
(472, '', '', 'Mexico/BajaNorte', '', '−08:00', '−07:00', 'Link to America/Tijuana'),
(473, '', '', 'Mexico/BajaSur', '', '−07:00', '−06:00', 'Link to America/Mazatlan'),
(474, '', '', 'Mexico/General', '', '−06:00', '−05:00', 'Link to America/Mexico_City'),
(475, '', '', 'MST', '', '−07:00', '−07:00', ''),
(476, '', '', 'MST7MDT', '', '−07:00', '−06:00', ''),
(477, '', '', 'Navajo', '', '−07:00', '−06:00', 'Link to America/Denver'),
(478, '', '', 'NZ', '', '+12:00', '+13:00', 'Link to Pacific/Auckland'),
(479, '', '', 'NZ-CHAT', '', '+12:45', '+13:45', 'Link to Pacific/Chatham'),
(480, 'WS', '-1350-17144', 'Pacific/Apia', '', '+13:00', '+14:00', ''),
(481, 'NZ', '-3652+17446', 'Pacific/Auckland', 'most locations', '+12:00', '+13:00', ''),
(482, 'NZ', '-4357-17633', 'Pacific/Chatham', 'Chatham Islands', '+12:45', '+13:45', ''),
(483, 'FM', '+0725+15147', 'Pacific/Chuuk', 'Chuuk (Truk) and Yap', '+10:00', '+10:00', ''),
(484, 'CL', '-2709-10926', 'Pacific/Easter', 'Easter Island & Sala y Gomez', '−06:00', '−05:00', ''),
(485, 'VU', '-1740+16825', 'Pacific/Efate', '', '+11:00', '+11:00', ''),
(486, 'KI', '-0308-17105', 'Pacific/Enderbury', 'Phoenix Islands', '+13:00', '+13:00', ''),
(487, 'TK', '-0922-17114', 'Pacific/Fakaofo', '', '+13:00', '+13:00', ''),
(488, 'FJ', '-1808+17825', 'Pacific/Fiji', '', '+12:00', '+13:00', ''),
(489, 'TV', '-0831+17913', 'Pacific/Funafuti', '', '+12:00', '+12:00', ''),
(490, 'EC', '-0054-08936', 'Pacific/Galapagos', 'Galapagos Islands', '−06:00', '−06:00', ''),
(491, 'PF', '-2308-13457', 'Pacific/Gambier', 'Gambier Islands', '−09:00', '−09:00', ''),
(492, 'SB', '-0932+16012', 'Pacific/Guadalcanal', '', '+11:00', '+11:00', ''),
(493, 'GU', '+1328+14445', 'Pacific/Guam', '', '+10:00', '+10:00', ''),
(494, 'US', '+211825-1575130', 'Pacific/Honolulu', 'Hawaii', '−10:00', '−10:00', ''),
(495, 'UM', '+1645-16931', 'Pacific/Johnston', 'Johnston Atoll', '−10:00', '−10:00', ''),
(496, 'KI', '+0152-15720', 'Pacific/Kiritimati', 'Line Islands', '+14:00', '+14:00', ''),
(497, 'FM', '+0519+16259', 'Pacific/Kosrae', 'Kosrae', '+11:00', '+11:00', ''),
(498, 'MH', '+0905+16720', 'Pacific/Kwajalein', 'Kwajalein', '+12:00', '+12:00', ''),
(499, 'MH', '+0709+17112', 'Pacific/Majuro', 'most locations', '+12:00', '+12:00', ''),
(500, 'PF', '-0900-13930', 'Pacific/Marquesas', 'Marquesas Islands', '−09:30', '−09:30', ''),
(501, 'UM', '+2813-17722', 'Pacific/Midway', 'Midway Islands', '−11:00', '−11:00', ''),
(502, 'NR', '-0031+16655', 'Pacific/Nauru', '', '+12:00', '+12:00', ''),
(503, 'NU', '-1901-16955', 'Pacific/Niue', '', '−11:00', '−11:00', ''),
(504, 'NF', '-2903+16758', 'Pacific/Norfolk', '', '+11:30', '+11:30', ''),
(505, 'NC', '-2216+16627', 'Pacific/Noumea', '', '+11:00', '+11:00', ''),
(506, 'AS', '-1416-17042', 'Pacific/Pago_Pago', '', '−11:00', '−11:00', ''),
(507, 'PW', '+0720+13429', 'Pacific/Palau', '', '+09:00', '+09:00', ''),
(508, 'PN', '-2504-13005', 'Pacific/Pitcairn', '', '−08:00', '−08:00', ''),
(509, 'FM', '+0658+15813', 'Pacific/Pohnpei', 'Pohnpei (Ponape)', '+11:00', '+11:00', ''),
(510, '', '', 'Pacific/Ponape', '', '+11:00', '+11:00', 'Link to Pacific/Pohnpei'),
(511, 'PG', '-0930+14710', 'Pacific/Port_Moresby', '', '+10:00', '+10:00', ''),
(512, 'CK', '-2114-15946', 'Pacific/Rarotonga', '', '−10:00', '−10:00', ''),
(513, 'MP', '+1512+14545', 'Pacific/Saipan', '', '+10:00', '+10:00', ''),
(514, '', '', 'Pacific/Samoa', '', '−11:00', '−11:00', 'Link to Pacific/Pago_Pago'),
(515, 'PF', '-1732-14934', 'Pacific/Tahiti', 'Society Islands', '−10:00', '−10:00', ''),
(516, 'KI', '+0125+17300', 'Pacific/Tarawa', 'Gilbert Islands', '+12:00', '+12:00', ''),
(517, 'TO', '-2110-17510', 'Pacific/Tongatapu', '', '+13:00', '+13:00', ''),
(518, '', '', 'Pacific/Truk', '', '+10:00', '+10:00', 'Link to Pacific/Chuuk'),
(519, 'UM', '+1917+16637', 'Pacific/Wake', 'Wake Island', '+12:00', '+12:00', ''),
(520, 'WF', '-1318-17610', 'Pacific/Wallis', '', '+12:00', '+12:00', ''),
(521, '', '', 'Pacific/Yap', '', '+10:00', '+10:00', 'Link to Pacific/Chuuk'),
(522, '', '', 'Poland', '', '+01:00', '+02:00', 'Link to Europe/Warsaw'),
(523, '', '', 'Portugal', '', '+00:00', '+01:00', 'Link to Europe/Lisbon'),
(524, '', '', 'PRC', '', '+08:00', '+08:00', 'Link to Asia/Shanghai'),
(525, '', '', 'PST8PDT', '', '−08:00', '−07:00', ''),
(526, '', '', 'ROC', '', '+08:00', '+08:00', 'Link to Asia/Taipei'),
(527, '', '', 'ROK', '', '+09:00', '+09:00', 'Link to Asia/Seoul'),
(528, '', '', 'Singapore', '', '+08:00', '+08:00', 'Link to Asia/Singapore'),
(529, '', '', 'Turkey', '', '+02:00', '+03:00', 'Link to Europe/Istanbul'),
(530, '', '', 'UCT', '', '+00:00', '+00:00', 'Link to UTC'),
(531, '', '', 'Universal', '', '+00:00', '+00:00', 'Link to UTC'),
(532, '', '', 'US/Alaska', '', '−09:00', '−08:00', 'Link to America/Anchorage'),
(533, '', '', 'US/Aleutian', '', '−10:00', '−09:00', 'Link to America/Adak'),
(534, '', '', 'US/Arizona', '', '−07:00', '−07:00', 'Link to America/Phoenix'),
(535, '', '', 'US/Central', '', '−06:00', '−05:00', 'Link to America/Chicago'),
(536, '', '', 'US/East-Indiana', '', '−05:00', '−04:00', 'Link to America/Indiana/Indianapolis'),
(537, '', '', 'US/Eastern', '', '−05:00', '−04:00', 'Link to America/New_York'),
(538, '', '', 'US/Hawaii', '', '−10:00', '−10:00', 'Link to Pacific/Honolulu'),
(539, '', '', 'US/Indiana-Starke', '', '−06:00', '−05:00', 'Link to America/Indiana/Knox'),
(540, '', '', 'US/Michigan', '', '−05:00', '−04:00', 'Link to America/Detroit'),
(541, '', '', 'US/Mountain', '', '−07:00', '−06:00', 'Link to America/Denver'),
(542, '', '', 'US/Pacific', '', '−08:00', '−07:00', 'Link to America/Los_Angeles'),
(543, '', '', 'US/Pacific-New', '', '−08:00', '−07:00', 'Link to America/Los_Angeles'),
(544, '', '', 'US/Samoa', '', '−11:00', '−11:00', 'Link to Pacific/Pago_Pago'),
(545, '', '', 'UTC', '', '+00:00', '+00:00', ''),
(546, '', '', 'W-SU', '', '+04:00', '+04:00', 'Link to Europe/Moscow'),
(547, '', '', 'WET', '', '+00:00', '+01:00', ''),
(548, '', '', 'Zulu', '', '+00:00', '+00:00', 'Link to UTC');

-- --------------------------------------------------------

--
-- Table structure for table `tropics`
--

CREATE TABLE IF NOT EXISTS `tropics` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `tropic_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `type` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '1 = Know, 2 = Like',
  `status` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Active, N = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=588 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tropics`
--

INSERT INTO `tropics` (`id`, `user_id`, `tropic_id`, `group_id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(78, 1, 8, 1, 2, 'Y', '2016-11-28 19:49:28', '2016-11-28 19:49:28'),
(79, 1, 6, 1, 2, 'Y', '2016-11-28 19:49:28', '2016-11-28 19:49:28'),
(80, 1, 25, 2, 2, 'Y', '2016-11-28 19:49:28', '2016-11-28 19:49:28'),
(145, 48, 13, 0, 2, 'Y', '2016-11-29 10:10:55', '2016-12-11 21:02:31'),
(173, 49, 25, 2, 1, 'Y', '2016-11-30 11:02:43', '2016-11-30 11:02:43'),
(174, 49, 14, 1, 1, 'Y', '2016-11-30 11:02:43', '2016-11-30 11:02:43'),
(195, 49, 13, 0, 2, 'Y', '2016-11-30 11:05:12', '2016-11-30 11:05:12'),
(196, 49, 30, 8, 2, 'Y', '2016-11-30 11:05:12', '2016-11-30 11:05:12'),
(197, 49, 1, 1, 2, 'Y', '2016-11-30 11:05:12', '2016-11-30 11:05:12'),
(198, 49, 9, 0, 2, 'Y', '2016-11-30 11:05:12', '2016-11-30 11:05:12'),
(199, 49, 12, 1, 2, 'Y', '2016-11-30 11:05:12', '2016-11-30 11:05:12'),
(200, 49, 7, 1, 2, 'Y', '2016-11-30 11:05:12', '2016-11-30 11:05:12'),
(201, 49, 3, 1, 2, 'Y', '2016-11-30 11:05:12', '2016-11-30 11:05:12'),
(388, 53, 14, 1, 1, 'Y', '2016-12-01 22:37:38', '2016-12-01 22:37:38'),
(389, 55, 14, 1, 1, 'Y', '2016-12-03 22:13:59', '2016-12-03 22:13:59'),
(391, 57, 1, 1, 1, 'Y', '2016-12-04 09:10:00', '2016-12-04 09:10:00'),
(410, 47, 3, 1, 1, 'Y', '2016-12-08 17:43:39', '2016-12-08 17:43:39'),
(411, 47, 4, 1, 1, 'Y', '2016-12-08 17:43:39', '2016-12-08 17:43:39'),
(412, 47, 5, 1, 1, 'Y', '2016-12-08 17:43:39', '2016-12-08 17:43:39'),
(413, 47, 11, 2, 1, 'Y', '2016-12-08 17:43:39', '2016-12-08 17:43:39'),
(414, 47, 5, 1, 1, 'Y', '2016-12-08 17:43:39', '2016-12-08 17:43:39'),
(415, 47, 3, 1, 1, 'Y', '2016-12-08 17:43:39', '2016-12-08 17:43:39'),
(416, 47, 7, 1, 1, 'Y', '2016-12-08 17:43:39', '2016-12-08 17:43:39'),
(417, 47, 3, 1, 1, 'Y', '2016-12-08 17:43:39', '2016-12-08 17:43:39'),
(418, 47, 3, 1, 1, 'Y', '2016-12-08 17:43:39', '2016-12-08 17:43:39'),
(419, 47, 3, 1, 1, 'Y', '2016-12-08 17:43:39', '2016-12-08 17:43:39'),
(420, 47, 4, 1, 1, 'Y', '2016-12-08 17:43:39', '2016-12-08 17:43:39'),
(481, 60, 23, 1, 1, 'Y', '2016-12-20 21:40:30', '2016-12-20 21:40:30'),
(482, 60, 9, 0, 1, 'Y', '2016-12-20 21:40:30', '2016-12-20 21:40:30'),
(483, 60, 1, 1, 1, 'Y', '2016-12-20 21:40:30', '2016-12-20 21:40:30'),
(484, 60, 5, 1, 1, 'Y', '2016-12-20 21:40:30', '2016-12-20 21:40:30'),
(485, 60, 15, 1, 1, 'Y', '2016-12-20 21:40:30', '2016-12-20 21:40:30'),
(492, 19, 25, 2, 1, 'Y', '2016-12-27 12:33:27', '2016-12-27 12:33:27'),
(493, 19, 31, 9, 1, 'Y', '2016-12-27 12:33:27', '2016-12-27 12:33:27'),
(494, 19, 32, 10, 1, 'Y', '2016-12-27 12:33:27', '2016-12-27 12:33:27'),
(495, 48, 23, 1, 1, 'Y', '2016-12-27 21:51:30', '2016-12-27 21:51:30'),
(496, 48, 25, 2, 1, 'Y', '2016-12-27 21:51:30', '2016-12-27 21:51:30'),
(497, 48, 28, 7, 1, 'Y', '2016-12-27 21:51:30', '2016-12-27 21:51:30'),
(498, 48, 20, 6, 1, 'Y', '2016-12-27 21:51:30', '2016-12-27 21:51:30'),
(499, 48, 27, 6, 1, 'Y', '2016-12-27 21:51:30', '2016-12-27 21:51:30'),
(500, 48, 16, 1, 1, 'Y', '2016-12-27 21:51:30', '2016-12-27 21:51:30'),
(501, 48, 17, 1, 1, 'Y', '2016-12-27 21:51:30', '2016-12-27 21:51:30'),
(502, 72, 2, 1, 1, 'Y', '2016-12-29 19:33:20', '2016-12-29 19:33:20'),
(522, 24, 1, 1, 1, 'Y', '2017-01-02 22:57:48', '2017-01-02 22:57:48'),
(523, 24, 7, 1, 1, 'Y', '2017-01-02 22:57:48', '2017-01-02 22:57:48'),
(524, 24, 3, 1, 1, 'Y', '2017-01-02 22:57:48', '2017-01-02 22:57:48'),
(525, 24, 5, 1, 1, 'Y', '2017-01-02 22:57:48', '2017-01-02 22:57:48'),
(526, 24, 10, 1, 1, 'Y', '2017-01-02 22:57:48', '2017-01-02 22:57:48'),
(527, 24, 9, 0, 1, 'Y', '2017-01-02 22:57:48', '2017-01-02 22:57:48'),
(528, 24, 31, 9, 1, 'Y', '2017-01-02 22:57:48', '2017-01-02 22:57:48'),
(529, 24, 15, 1, 1, 'Y', '2017-01-02 22:57:48', '2017-01-02 22:57:48'),
(530, 24, 44, 0, 1, 'Y', '2017-01-02 22:57:48', '2017-01-02 22:57:48'),
(531, 31, 46, 0, 1, 'Y', '2017-01-02 22:58:32', '2017-01-02 22:58:32'),
(532, 52, 14, 1, 1, 'Y', '2017-01-02 22:59:16', '2017-01-02 22:59:16'),
(533, 52, 5, 1, 1, 'Y', '2017-01-02 22:59:16', '2017-01-02 22:59:16'),
(534, 52, 47, 0, 1, 'Y', '2017-01-02 22:59:16', '2017-01-02 22:59:16'),
(536, 68, 1, 1, 2, 'Y', '2017-01-03 14:18:36', '2017-01-03 14:18:36'),
(547, 1, 9, 0, 1, 'Y', '2017-01-06 20:38:50', '2017-01-06 20:38:50'),
(548, 1, 24, 0, 1, 'Y', '2017-01-06 20:38:50', '2017-01-06 20:38:50'),
(549, 1, 28, 7, 1, 'Y', '2017-01-06 20:38:50', '2017-01-06 20:38:50'),
(550, 1, 29, 7, 1, 'Y', '2017-01-06 20:38:50', '2017-01-06 20:38:50'),
(551, 1, 27, 6, 1, 'Y', '2017-01-06 20:38:50', '2017-01-06 20:38:50'),
(552, 1, 7, 1, 1, 'Y', '2017-01-06 20:38:50', '2017-01-06 20:38:50'),
(553, 1, 20, 6, 1, 'Y', '2017-01-06 20:38:50', '2017-01-06 20:38:50'),
(554, 1, 11, 2, 1, 'Y', '2017-01-06 20:38:50', '2017-01-06 20:38:50'),
(555, 1, 2, 1, 1, 'Y', '2017-01-06 20:38:50', '2017-01-06 20:38:50'),
(556, 1, 3, 1, 1, 'Y', '2017-01-06 20:38:50', '2017-01-06 20:38:50'),
(563, 76, 49, 0, 1, 'Y', '2017-01-07 22:29:40', '2017-01-07 22:29:40'),
(564, 76, 50, 0, 1, 'Y', '2017-01-07 22:29:40', '2017-01-07 22:29:40'),
(565, 74, 51, 0, 1, 'Y', '2017-01-07 22:37:07', '2017-01-07 22:37:07'),
(573, 85, 31, 9, 1, 'Y', '2017-01-16 16:05:19', '2017-01-16 16:05:19'),
(574, 85, 32, 10, 1, 'Y', '2017-01-16 16:05:19', '2017-01-16 16:05:19'),
(575, 85, 13, 0, 1, 'Y', '2017-01-16 16:05:19', '2017-01-16 16:05:19'),
(576, 77, 14, 1, 1, 'Y', '2017-01-16 16:36:13', '2017-01-16 16:36:13'),
(577, 77, 31, 9, 1, 'Y', '2017-01-16 16:36:13', '2017-01-16 16:36:13'),
(578, 77, 48, 0, 1, 'Y', '2017-01-16 16:36:13', '2017-01-16 16:36:13'),
(579, 77, 30, 8, 1, 'Y', '2017-01-16 16:36:13', '2017-01-16 16:36:13'),
(580, 90, 31, 9, 1, 'Y', '2017-01-16 17:11:29', '2017-01-16 17:11:29'),
(581, 93, 31, 9, 1, 'Y', '2017-01-17 22:31:37', '2017-01-17 22:31:37'),
(585, 95, 51, 0, 1, 'Y', '2017-01-19 23:04:44', '2017-01-19 23:04:44'),
(586, 95, 32, 10, 1, 'Y', '2017-01-19 23:04:44', '2017-01-19 23:04:44'),
(587, 96, 51, 0, 1, 'Y', '2017-01-19 23:27:26', '2017-01-19 23:27:26');

-- --------------------------------------------------------

--
-- Table structure for table `tropic_groups`
--

CREATE TABLE IF NOT EXISTS `tropic_groups` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tropic_groups`
--

INSERT INTO `tropic_groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Sports', '2016-11-25 18:30:00', '2016-11-25 18:30:00'),
(2, 'Language', '2016-11-25 18:30:00', '2016-11-25 18:30:00'),
(6, 'Major', '2016-11-29 09:33:15', '2016-11-29 09:33:15'),
(7, 'Profession', '2016-11-29 09:34:20', '2016-11-29 09:34:20'),
(8, 'Movie', '2016-11-30 07:58:19', '2016-11-30 07:58:19'),
(9, 'Company', '2016-11-30 10:15:03', '2016-11-30 10:15:03'),
(10, 'Country', '2016-11-30 10:15:32', '2016-11-30 10:15:32'),
(13, 'KHDN', '2017-01-07 22:28:22', '2017-01-07 22:28:22');

-- --------------------------------------------------------

--
-- Table structure for table `universities`
--

CREATE TABLE IF NOT EXISTS `universities` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `universities`
--

INSERT INTO `universities` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Trường Đại học Bách khoa Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(2, 'Trường Đại học Công đoàn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(3, 'Trường Đại học Công nghệ', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(4, 'Trường Đại học Công nghệ Giao thông Vận tải', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(5, 'Trường Đại học Công nghệ Dệt may Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(6, 'Trường Đại học Công nghệ và Quản lý Hữu nghị', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(7, 'Trường Đại học Công nghiệp Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(8, 'Trường Đại học Công nghiệp Việt Hung', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(9, 'Trường Đại học Dược Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(10, 'Trường Đại học Đại Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(11, 'Trường Đại học Điện lực', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(12, 'Trường Đại học Đông Đô', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(13, 'Trường Đại học FPT', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(14, 'Trường Đại học Giao thông Vận tải', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(15, 'Trường Đại học Giáo dục', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(16, 'Trường Đại học Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(17, 'Trường Đại học Hòa Bình', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(18, 'Trường Đại học Khoa học Tự nhiên', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(19, 'Trường Đại học Khoa học và Công nghệ Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(20, 'Trường Đại học Khoa học Xã hội và Nhân văn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(21, 'Trường Đại học Kiến trúc Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(22, 'Trường Đại học Kinh doanh và Công nghệ Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(23, 'Trường Đại học Kinh tế', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(24, 'Trường Đại học Kinh tế - Kỹ thuật Công nghiệp', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(25, 'Trường Đại học Kinh tế Quốc dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(26, 'Trường Đại học Kỹ thuật Lê Quý Đôn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(27, 'Trường Đại học Lao động - Xã hội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(28, 'Trường Đại học Lâm nghiệp Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(29, 'Trường Đại học Luật Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(30, 'Trường Đại học Mỏ - Địa chất', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(31, 'Trường Đại học Mỹ thuật Công nghiệp Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(32, 'Trường Đại học Mỹ thuật Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(33, 'Trường Đại học Ngoại ngữ', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(34, 'Trường Đại học Ngoại thương', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(35, 'Trường Đại học Nguyễn Trãi', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(36, 'Trường Đại học Nội vụ Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(37, 'Trường Đại học Phương Đông', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(38, 'Trường Đại học Quốc tế Bắc Hà', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(39, 'Trường Đại học Răng Hàm Mặt', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(40, 'Trường Đại học Sân khấu - Điện ảnh Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(41, 'Trường Đại học Sư phạm Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(42, 'Trường Đại học Sư phạm Nghệ thuật Trung ương', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(43, 'Trường Đại học Sư phạm Thể dục Thể thao Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(44, 'Trường Đại học Tài chính Ngân hàng Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(45, 'Trường Đại học Tài nguyên và Môi trường Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(46, 'Trường Đại học Thành Đô', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(47, 'Trường Đại học Thành Tây', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(48, 'Trường Đại học Thăng Long', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(49, 'Trường Đại học Thủ đô Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(50, 'Trường Đại học Thủy lợi', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(51, 'Trường Đại học Thương mại', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(52, 'Trường Đại học Văn hóa Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(53, 'Trường Đại học Xây dựng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(54, 'Trường Đại học Y Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(55, 'Trường Đại học Y tế Công cộng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(56, 'Học viện Âm nhạc Quốc gia Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(57, 'Học viện Thiết kế và Thời trang London', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(58, 'Học viện Báo chí và Tuyên truyền', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(59, 'Học viện Biên phòng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(60, 'Học viện Công nghệ Thông tin Bách Khoa', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(61, 'Học viện Công nghệ Bưu chính Viễn thông', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(62, 'Học viện Chính trị', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(63, 'Học viện Chính trị - Hành chính Quốc gia Hồ Chí Minh', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(64, 'Học viện Hành chính', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(65, 'Học viện Kỹ thuật Mật mã', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(66, 'Học viện Ngân hàng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(67, 'Học viện Ngoại giao', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(68, 'Học viện Nông nghiệp Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(69, 'Học viện Phụ nữ Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(70, 'Học viện Quản lý Giáo dục', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(71, 'Học viện Tư pháp Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(72, 'Học viện Tài chính', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(73, 'Học viện Quân y', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(74, 'Học viện Y dược học cổ truyền Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(75, 'Học viện Chính sách và Phát triển', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(76, 'Học viện Thanh thiếu niên Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(77, 'Viện Đại học Mở Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(78, 'Học viện An ninh Nhân dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(79, 'Học viện Cảnh sát Nhân dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(80, 'Học viện Phòng không - Không quân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(81, 'Học viện Hàng không Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(82, 'Học viện Hành chính cơ sở phía Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(83, 'Học viện Kỹ thuật Mật mã cơ sở phía Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(84, 'Học viện Kỹ thuật Quân sự cơ sở 2', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(85, 'Nhạc viện Thành phố Hồ Chí Minh', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(86, 'Trường ĐH An ninh Nhân dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(87, 'Trường ĐH Bách khoa', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(88, 'Trường ĐH Công nghiệp Thực phẩm TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(89, 'Trường ĐH Công nghiệp TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(90, 'Trường ĐH Công nghệ Sài Gòn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(91, 'Trường ĐH Công nghệ thông tin Gia Định', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(92, 'Trường ĐH Công nghệ Thông tin', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(93, 'Trường ĐH Công nghệ TP.HCM (HUTECH)', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(94, 'Trường ĐH Cảnh sát Nhân dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(95, 'Trường ĐH Dân lập Văn Lang', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(96, 'Trường ĐH FPT', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(97, 'Trường ĐH Giao thông Vận tải - cơ sở 2 phía Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(98, 'Trường ĐH Giao thông Vận tải TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(99, 'Trường ĐH Hoa Sen', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(100, 'Trường ĐH Hùng Vương', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(101, 'Trường ĐH Khoa học Tự nhiên', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(102, 'Trường ĐH Khoa học Xã hội và Nhân văn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(103, 'Trường ĐH Kinh tế - Luật', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(104, 'Trường ĐH Kinh tế - Tài chính TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(105, 'Trường ĐH Kinh tế TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(106, 'Trường ĐH Kiến trúc TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(107, 'Trường ĐH Lao động - Xã hội (cơ sở 2 TP.HCM)', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(108, 'Trường ĐH Luật TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(109, 'Trường ĐH Mở TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(110, 'Trường ĐH Mỹ thuật TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(111, 'Trường ĐH Ngoại ngữ - Tin học TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(112, 'Trường ĐH Ngoại thương cơ sỏ phía Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(113, 'Trường ĐH Nguyễn Tất Thành', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(114, 'Trường ĐH Ngân hàng TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(115, 'Trường ĐH Nông Lâm TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(116, 'Trường ĐH Quốc tế Hồng Bàng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(117, 'Trường ĐH Quốc tế RMIT Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(118, 'Trường ĐH Quốc tế', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(119, 'Trường ĐH Sài Gòn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(120, 'Trường ĐH Sân khấu', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(121, 'Trường ĐH Sư phạm Kỹ thuật TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(122, 'Trường ĐH Sư phạm Thể dục Thể thao TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(123, 'Trường ĐH Sư phạm TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(124, 'Trường ĐH Thể dục Thể thao TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(125, 'Trường ĐH Thủy lợi cơ sở 2', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(126, 'Trường ĐH Trần Đại Nghĩa', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(127, 'Trường ĐH Tài chính - Marketing', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(128, 'Trường ĐH Tài nguyên - Môi trường TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(129, 'Trường ĐH Tôn Đức Thắng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(130, 'Trường ĐH Tư thục Quốc tế Sài Gòn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(131, 'Trường ĐH Việt Đức', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(132, 'Trường ĐH Văn Hiến', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(133, 'Trường ĐH Văn hóa TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(134, 'Trường ĐH Y Dược TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(135, 'Trường ĐH Y khoa Phạm Ngọc Thạch', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(136, 'ĐH Quốc gia Thành phố Hồ Chí Minh', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(137, 'Trường Cao đẳng Phát thanh Truyền hình II', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(138, 'Trường Đại học Tây Bắc', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(139, 'Học viện Chính trị Quân sự', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(140, 'Học viện Hải quân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(141, 'Học viện Hậu cần', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(142, 'Học viện Khoa học Quân sự', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(143, 'Học viện Kỹ thuật Quân sự', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(144, 'Học viện Lục quân Đà Lạt', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(145, 'Học viện Quốc phòng Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(146, 'Trường Đại học Ngô Quyền', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(147, 'Trường Sĩ quan Không quân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(148, 'Trường Đại học Chính trị', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(149, 'Trường Đại học Trần Đại Nghĩa', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(150, 'Trường Đại học Trần Quốc Tuấn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(151, 'Trường Đại học Nguyễn Huệ', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(152, 'Trường Đại học Thông tin liên lạc', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(153, 'Trường Sĩ quan Tăng-Thiết giáp', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(154, 'Trường Đại học Văn hóa - Nghệ thuật Quân đội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(155, 'Trường Đại học An ninh Nhân dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(156, 'Trường Đại học Cảnh sát Nhân dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(157, 'Trường Đại học Phòng cháy Chữa cháy', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(158, 'Trường Đại học Kỹ thuật - Hậu cần Công an Nhân dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(159, 'Trường Cao đẳng An ninh Nhân dân I', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(160, 'Trường Cao đẳng An ninh Nhân dân II', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(161, 'Trường Cao đẳng Cảnh sát Nhân dân I', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(162, 'Trường Cao đẳng Cảnh sát Nhân dân II', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(163, 'Đại học Thái Nguyên', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(164, 'Trường Đại học Vinh', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(165, 'Đại học Huế', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(166, 'Đại học Đà Nẵng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(167, 'Trường Đại học Quy Nhơn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(168, 'Trường Đại học Tây Nguyên', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(169, 'Trường Đại học Cần Thơ', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(170, 'Trường Đại học An Giang', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(171, 'Trường Đại học Bạc Liêu', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(172, 'Trường Đại học Đồng Nai', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(173, 'Trường Đại học Hà Tĩnh', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(174, 'Trường Đại học Hạ Long', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(175, 'Trường Đại học Hải Dương', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(176, 'Trường Đại học Hải Phòng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(177, 'Trường Đại học Hồng Đức', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(178, 'Trường Đại học Hoa Lư', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(179, 'Trường Đại học Hùng Vương', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(180, 'Trường Đại học Khánh Hòa', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(181, 'Trường Đại học Kinh tế Nghệ An', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(182, 'Trường Đại học Kỹ thuật - Công nghệ Cần Thơ', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(183, 'Trường Đại học Phạm Văn Đồng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(184, 'Trường Đại học Phú Yên', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(185, 'Trường Đại học Quảng Bình', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(186, 'Trường Đại học Quảng Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(187, 'Trường Đại học Sài Gòn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(188, 'Trường Đại học Thủ Dầu Một', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(189, 'Trường Đại học Tân Trào', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(190, 'Trường Đại học Tiền Giang', '2017-01-19 16:57:24', '2017-01-19 16:57:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nickname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci,
  `user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `recovery_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 = Normal user, 2 = Client',
  `gender` enum('M','F') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'M' COMMENT '''M''=>''Male'', ''F''=>''Female''',
  `dob` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `profile_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vat_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `current_address` text COLLATE utf8_unicode_ci NOT NULL,
  `postal_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lng` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `confirmation_code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `utc_timezone` int(11) NOT NULL,
  `status` enum('Y','N','E') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'E' COMMENT 'Y = Active, N = Block, E = Pending Activation',
  `in_step` int(10) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `nickname`, `details`, `user_name`, `email`, `recovery_email`, `password`, `user_type`, `gender`, `dob`, `age`, `profile_image`, `company_name`, `company_number`, `vat_number`, `address`, `current_address`, `postal_code`, `lat`, `lng`, `confirmation_code`, `utc_timezone`, `status`, `in_step`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Pritam', ' Das ', '', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n\r\n\r\n\r\n\r\n', 'pritam12', 'pritam@technoexponent.com', 'das@technoexponent.com', '$2y$10$8uDbsNe6znBBzFqoWTQLP.4nEg2S6.PhfG3m6S5a8Rwb1RCERs0ge', 2, 'M', '1-1-1978', 39, '1474462256FHaWxEj6bb.jpg', 'test agaency', 'gaen65265', 'abc12355', 'Baguiati Flyover, Baguihati, Kolkata, West Bengal, India', '', '700059', '22.6096872', '88.42756839999993', '', 5, 'Y', 3, 'dxSPLKuNP7PTxdHPQUBjq715LKbVMv1uBEyfNbyfw5C0CnPPPn5H8p8yWItM', '2016-09-16 07:23:42', '2017-01-19 19:53:14'),
(2, 'krishanu', 'Das', '', NULL, 'pritam@technoexponent.com', 'krishanu@technoexponent.com', '', '$2y$10$xyDisnr8XetBk4/PeUsXo.Cy0BD9N7Nzo2s7Gvhgis.qFKu1XEjXu', 1, 'M', '', 24, '', '', '', '', '', '', '', '', '', '', 0, 'E', 0, NULL, '2016-09-16 08:15:09', '2016-09-16 08:15:09'),
(17, '', '', '', NULL, 'Raju Das', 'p@technoexponent.com', '', '$2y$10$63yqm2NEK1Dl6qLhbBehfe71V3/bOlYIgtksj63ej1ffHMLgx6h7.', 1, 'M', '14-7-1987', 24, '', '', '', '', 'Kolkata, West Bengal, India', 'San Diego State University, Campanile Drive, San Diego, CA, United States', '', '', '', '', 0, 'Y', 5, 'YraTvvr2KjYepKxvCnopxkn9LvglFjviT70HKNO4eTUm5vTahaESXPa4ZyIf', '2016-11-11 01:38:13', '2016-11-11 23:12:28'),
(19, 'Nguyễn', 'Phương Thảo', 'Phuong Thao', NULL, '', 'pht11690@gmail.com', '', '$2y$10$gzjEy8T.4b0s0QjBpz.EountCU1wWHW3yHMlQuP6Nsn4KGIBWJkAK', 1, 'F', '1-1-1990', 27, '', '', '', '', 'Hong Kong', '', '', '', '', '', 0, 'Y', 5, 'eo5bsc4GQoXGhuOpmVBTieaooHDWhtuY8htJikfQDsgqYm1JiPyYvx5FrqiB', '2016-11-12 11:41:02', '2017-01-14 08:46:45'),
(22, 'Nguyễn', 'Hà', '', NULL, NULL, 'tienhp1990@yahoo.com', '', '$2y$10$7R2UhiEMxi6ltE.I9rambOqd6i0oBQjOTD/UeokOn14715IJOT2tO', 1, 'F', '6-7-1993', 23, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'cSjGCavvUkTdFPn00K4csdiWVTNlQJevcVS9P4b9mEObJ6LcMnuAzw5DUvaS', '2016-11-12 14:15:46', '2016-12-30 22:57:35'),
(23, 'Nguyen', ' Van Trang', 'NguyenVanTrang', NULL, NULL, 'tienhp1990@gmail.com', '', '$2y$10$IUo.VhUZcFbyUQvKNTOloeEVkvuGPMvMegyUuO23X/LNrrlpC7JVO', 1, 'F', '1-1-1992', 25, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, '6L5cgML4WDg1aGff6q1totas1kP28eyEhy40YekGohZ1TYJJ0udstxvcl2wu', '2016-11-14 15:02:54', '2016-12-27 22:31:22'),
(24, 'Trinh', 'Thi Hoai Thu', 'Tung Nguyen', NULL, NULL, 'tienhp19901@yahoo.com', 'tienhp1992@yahoo.com', '$2y$10$Uqvay87W9B3iO1Ev564iPOfPgpM5A3tk/b7cPz9wXcHT7IzHtSx8O', 1, 'M', '11-6-1990', 24, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'jXLOldvbZbrrCZBLMQQyWpdW0dIM1g7nD2WtNXsWMJ3gyCisKySk9Vx1jY8E', '2016-11-14 18:41:14', '2017-01-11 22:33:20'),
(26, '', '', '', NULL, 'Dilip Patra', 'dilip@technoexponent.com', '', '$2y$10$urBP8UPqZWG6Xye01gS4V.V8xk2aq5pFgnYuO9zjXMcey.iSkaZ.G', 1, 'M', '1-1-1990', 34, '', '', '', '', 'Digha Beach, Contai, West Bengal, India', 'Digha, West Bengal, India', '', '', '', '', 0, 'E', 3, NULL, '2016-11-15 11:50:17', '2016-11-15 11:53:02'),
(28, '', '', '', NULL, NULL, 'sandip1@technoexponent.com', '', '$2y$10$4136IChd4CAlOjudj3G6JO//c0ZD2PkKTEmBL1nRWD/ycFdAQPW4.', 1, 'M', '', 24, '', '', '', '', '', '', '', '', '', '', 0, 'E', 1, NULL, '2016-11-15 21:13:37', '2016-11-15 21:13:37'),
(29, 'santur', '', '', NULL, NULL, 'fu@technoexponent.com', '', '$2y$10$1SEkVu1PQ.Cg2L7ZUCuvSuY8hCxa6sBGVlfHJxqtPu7Zzluw3QR0W', 1, 'M', '17-5-1988', 29, '', '', '', '', 'South Carolina, United States', 'San Francisco, CA, United States', '', '', '', '', 0, 'E', 5, 'c4SZf2Vd1NdUcr9p5Sd1B4rcaMUJKq0p6G3ZJvcQ1Ul9cAeeqQgeIeX2wLXt', '2016-11-15 21:17:37', '2016-11-15 21:26:46'),
(30, 'Sabyasachi', '', '', NULL, NULL, 'papan1@technoexponent.com', '', '$2y$10$1rUw7xzSrwHpZnhYDe290u1Gwc4skTjiq9DgpSS81MVSFuapYEP6C', 1, 'M', '1-1-1987', 31, '', '', '', '', 'Canada', 'Canada', '', '', '', '', 0, 'Y', 5, '1rL7UzAoi2usgEl10R4LUc8tEKJn90EK8EaAHyHLZfZQAOTYAMPgMDZwEQZ1', '2016-11-15 21:28:01', '2016-11-15 21:31:09'),
(31, 'Nguyễn', 'Thị Minh Hải', 'LongNhut', NULL, NULL, 'tienhp19902@yahoo.com', '', '$2y$10$QlYiHcWtn7WuSUfmsCyFduxVqET/NKL31ZadQIIsx34lHhO159ymG', 1, 'F', '--', 24, '', '', '', '', 'Vinh Phuc Province, Vietnam', '', '', '', '', '', 0, 'Y', 5, 'LHgpeO1cDTKR61QChxRYRydzKdgtPpO6DF3qUyvIL1mkvApCyJzyetsG4sYb', '2016-11-15 23:02:40', '2017-01-08 22:09:19'),
(33, 'Dev', 'D', 'Fb user', NULL, 'DevDev', 'test2@technoexponent.com', '', '$2y$10$PtLkjcj0KQ4D6Ddr/5oOYuJaEjOdjSbxI/gHR0MYHCpPv6VO5y.oe', 1, 'M', '14-9-1988', 41, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'ZG0EYphVRjCrwOm5SBtHkfIpIAcFM9PBQR9a8sbN2OlnzPJjGdm6vQCikptb', '2016-11-16 15:51:49', '2016-12-22 16:26:07'),
(35, '', 'TrầnThanhHoài', 'Hoài Trần', 'Hi everyone!', NULL, 'tienhp19904@yahoo.com', '', '$2y$10$D92bOB79MGt//l/5H3V2VOVdoD.bYHz1oBq.eD8AjizvpPZCfPiNS', 1, 'F', '7-11-1992', 24, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'tjMpq5CDIJPQcsmdWamkghT1v6WMO8KOYeyrQQEFtpOZExXp7SRnCbDbiNNv', '2016-11-19 00:39:46', '2017-01-19 22:50:26'),
(41, 'Suman', 'Chowdhury', '', NULL, NULL, 'chowdhurysuman27@gmail.com', '', '$2y$10$hhlW6gsFExJHbiREtM087.tG/Xz1wFSLziJRrBMa/uHh6gN/9z6v2', 1, 'M', '', 32, '', '', '', '', '', '', '', '', '', '', 0, 'E', 0, 'sLDvblgNy5tOCobgO6lBer0kbMiga7nESmK9PXWFaPX6IiyqkS2k5uA4jVS2', '2016-11-22 19:08:48', '2016-11-22 19:09:33'),
(42, 'Suman', 'Chowdhury', '', NULL, NULL, 'suman.chowdhury@technoexponent.com', '', '$2y$10$1bry.eGV4BqnhjVSdMKGpOE7ql5JRtoVvqQU5S6MYJpIONAhMt6jW', 1, 'F', '30-8-1988', 29, '', '', '', '', '', '', '', '', '', '', 0, 'E', 0, 'NuHaDZNUO3oaZtIzQtFS7YSD1u7Xmtv3n8Sr1bmzwBr74AvGgsE6lhk66zgI', '2016-11-22 19:10:20', '2016-11-22 19:12:16'),
(43, 'Sanjoy', ' Saha', 'SanjoySaha', NULL, NULL, 'sanjoy@technoexponent.com', '', '$2y$10$QaZkC7x1gy9jBtaDZwcs6ed6u/sfCWABt6rnSXrzKhnNFmtWUMLCu', 1, 'M', '10-10-1982', 19, '', '', '', '', '', '', '', '', '', '', 0, 'E', 3, NULL, '2016-11-22 20:39:35', '2016-11-24 13:29:20'),
(44, 'Sabyasachi', '  Saha', 'SabyasachiSaha', NULL, NULL, 'technoexponent05@gmail.com', '', '$2y$10$hblCUtzgxtag05FqOI2OUueVDji3U9aQoNQ2V7/DEp1gbPkYbUBkm', 1, 'M', '14-10-1985', 24, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'BNbamHgXlHMmaRbFQbajE3EeJRfEOB26ApImCsURpkKa1LuV1J1VSfDylH5J', '2016-11-23 18:13:15', '2016-11-24 12:43:54'),
(45, 'anurag', '   basu', 'anuragbasu', NULL, NULL, 'anurag@technoexponent.com', '', '$2y$10$Pft44DsnDTZYJyNo59hnau8pZtQ/zBHaYOwuQR9JmvXrNEvxy9XOa', 1, 'M', '--1973', 24, '', '', '', '', '', '', '', '', '', '', 0, 'E', 4, 'b4Wnn2eAcLi351VXdDYFfrWlw8lzD40dhvQpeyHEoaSk9aEqXBd3B3tEa11Z', '2016-11-23 18:28:29', '2016-11-23 20:30:28'),
(46, 'Jhon', '', 'Jhon', NULL, NULL, 'alok@technoexponent.com', '', '$2y$10$2./TZDkQ5gVTfuTC1dP4Me1tsmUCfZsOvCmvvTq8Bako8wcym9/1K', 1, 'M', '--1985', 21, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 3, 'My4ELuerK7EWqEINebOwNfBKsCvYW0LLXwDIfc7w37T3GGOKjXvZoiW7fsnz', '2016-11-23 19:28:54', '2016-11-23 20:26:12'),
(47, 'technoexponent', ' 02', 'technoexponent02', NULL, NULL, 'technoexponent02@gmail.com', '', '$2y$10$v4htvrFzd43B3hz37IY7YuSJTiOkr.eJmyQZf8V3nJRcNDUDG.BNW', 1, 'F', '--', 22, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'IZY8uyvwiA26bXQCKsZJTCkISX3LFgEnGYisLSxuPJMcSKLCjk1CUEa9uoQG', '2016-11-24 12:49:15', '2016-12-01 13:32:53'),
(48, 'Đặng', 'Thi Thu Phương', 'Thu Phương', NULL, NULL, 'tienhp11690@gmail.com', '', '$2y$10$CYruPbd82unc28NjGY6ux.1cevwMedn5bm2jQWxF5TL0rMUabA3im', 1, 'F', '1-1-1988', 29, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'cjxAp7OOxzCHmIRvFDR0AeMGmpOcdvzo7NsoA68VW8tkImELfhNwNai8vtH5', '2016-11-24 13:05:08', '2017-01-09 21:50:20'),
(49, 'Pham', ' Hong Tien', 'P.h. Tien', NULL, NULL, 'tienhp1106@yahoo.com', '', '$2y$10$971AzqppeTDE8nEc8XSIeOr472tZZuePmT.ZBB9yEg6JaEiKoZqRC', 1, 'M', '1-1-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'jKVqrna0x6eScSWLJaKYF5NX3bJaowncDnIFz14QqX6yDwMRTrGUJju6Jlw3', '2016-11-24 13:24:26', '2017-01-16 22:42:02'),
(52, 'Ngô', 'Thị Thu Hà', 'V.t Duc', NULL, NULL, 'tienhp19903@yahoo.com', '', '$2y$10$GdUskuy4gejuKg1XT9cWfe115puu0RxC6iTbYyxMZmU4MjKJtLdoq', 1, 'M', '23-7-1989', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'a8w1mc5n6XJpcIVkJKzULB02mE2W9mJuHtWopKfmMCcGsQMpDK9tdUDkCIt6', '2016-11-30 22:32:46', '2017-01-02 22:59:32'),
(53, 'Nguyễn', 'Huy Nam', 'LongNhut', NULL, NULL, 'tienhp19905@yahoo.com', '', '$2y$10$nA3ZQ70PK6JK6V1oAuf1SeFLoimRzPDU9Scjo0OQmyg8D8S2ZwmW2', 1, 'M', '1-1-1987', 30, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'XFPgfRac5h1c76h5TzhAbvRS7aO3x5IPREJLzGAgWtkWoPBdFcNrwZhJzPM0', '2016-12-01 22:05:28', '2017-01-07 23:08:10'),
(54, 'Tri', '  bui', 'Tribui', NULL, NULL, 'tri.bui@eatagile.com', '', '$2y$10$WDafvBPFAEhcEwSTOR/6puAqx4Xmg3k7jigxfbq1ULW10R4oal00G', 1, 'M', '2-2-1990', 24, '', '', '', '', '', '', '', '', '', '', 0, 'E', 2, NULL, '2016-12-03 19:11:11', '2016-12-03 19:19:50'),
(55, 'Bui', ' Minh Tri', 'BuiMinhTri', NULL, NULL, 'tienhp19907@yahoo.com', '', '$2y$10$fGbShoEaw/opdH1DzZJW4.m4a8dNPXX/aUsNfqOk6NsGhvGIFeYAa', 1, 'M', '1-1-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'kEkSSa2jWjQGNXZv8jKWtSDWM9JbapRwcL85xyNeMpzkNmYM7EsdrJ7UMMUU', '2016-12-03 22:10:04', '2017-01-07 23:04:44'),
(57, 'Tri', '  Bui ABC', 'TriBui', NULL, NULL, 'tri.bui@eastagile.com', '', '$2y$10$JQAVI..P0qecAjikg58TKORVKLhaJs2JCQRm6U/qgyOi/zuwyFrvy', 1, 'M', '1-3-1972', 21, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, NULL, '2016-12-04 08:45:56', '2016-12-04 09:04:27'),
(59, 'admin', '', 'admin', NULL, NULL, 'pritam2@technoexponent.com', '', '$2y$10$nxabkRru2O7guAZFqZBmkuGziVD4w1ABYsKnf88yUoQsOW2TnaIB.', 1, 'M', '1-1-1986', 27, '', '', '', '', '', '', '', '', '', '', 0, 'E', 2, 'H2nSrK5KodDFT9HCaqw5ShSoDXiyyV8OursdPOz9PCLyNok98vGkHjXGzjlB', '2016-12-15 19:52:01', '2016-12-15 19:53:41'),
(60, 'Ho', '           Sy Long', 'Ho Sy Long', 'Gjjgdgj', NULL, 'tienhp1991@yahoo.com', '', '$2y$10$miX982qcCB70RpC3Hq5wCOu77RO1W09emvIO0XT6Oz3qZUngNOaN6', 1, 'M', '1-1-1988', 29, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'lhQBcHzX6gCoqwz88CBJKzBk86UgdJdvCIOCnPIiKQk3lXcLxSQ9v7adn7Uv', '2016-12-19 19:12:53', '2017-01-19 17:31:39'),
(61, 'Nguyễn', 'Vũ Thịnh', 'Vũ Thịnh Nguyễn', NULL, NULL, 'tienhp1991@gmail.com', '', '$2y$10$JHJ25zhoThuJIR6piRT4bO5iyzsndZvfsIJYwMMU181p58FnC0fby', 1, 'M', '1-1-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'jT8jvLbc10ZhUzYYuu0NbYZXdzhnmC21RiOWcuJ1Wft9epqk1F0ADhyIuqPk', '2016-12-20 21:21:33', '2017-01-07 23:02:06'),
(66, 'Dev', 'Dev', 'DevDev', NULL, NULL, 'test@technoexponent.com', '', '$2y$10$qr5dNhgA06kSmNtFMhGz4.FPJSfNddc/UwRgOsRSfFdAIFLTRTrIi', 1, 'M', '', 0, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'uhk0HfXoN3bhwOxfo0CiPf3U6e4G0yG0N7tJxKJ6lphMX8e7ZNdCqa1kfW1J', '2016-12-22 16:35:16', '2016-12-29 18:28:10'),
(68, 'Avoy', 'Debnath', 'Avoy Debnath', NULL, NULL, 'avoy.php@gmail.com', '', '$2y$10$pRRHjoB1otQ8plLXcezVouyTm/zVmsIjgii6/5.Kd3ljRpMZuGG.6', 1, 'M', '22-4-1988', 29, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, '5a4AdCqZY80n2po8ZIc8ulDuADGpHCoITPpTRs644okdtKpJ6rYEWP2PtOtx', '2016-12-27 15:47:35', '2017-01-13 20:16:09'),
(69, 'Mai', ' Thị Phương Thảo', 'Mai Thao', NULL, NULL, 'tienhp19906@yahoo.com', '', '$2y$10$7SohtYVKk24wL9NgTp7OIOKfzsedH/17/mJA7X6/ateZMp8.PhNj.', 1, 'F', '11-9-1990', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'mXsIln6m3fGC97CSOn5e2SVn05mRL5f07XQH266D0bHwrcYGk3pf4UNKGAr3', '2016-12-27 22:48:19', '2017-01-07 22:58:37'),
(70, 'Nguyễn', 'Thị Như Ngọc', 'Như Ngọc Nguyễn', NULL, NULL, 'tienhp19908@yahoo.com', '', '$2y$10$fBr8k85l9st/SCdmWeVVYOe0femJN368i9rR35wFGGUk2SQDxtfWu', 1, 'F', '3-4-1991', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'gD8NL0pt6372AeXsX5r6q65CPPR2zxffkkQ9w0qhP9sz8npTjksM8hXVFqVe', '2016-12-28 23:08:59', '2017-01-07 22:54:55'),
(72, 'Santu', 'Roy', 'Santu Roy', NULL, NULL, 'santu@technoexponent.com', '', '$2y$10$u0CooZnFQ4uSClasJEaLremEN6TtkaurA35UvG.CUWm6CBLYWweta', 1, 'M', '1-1-1989', 28, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'Czi4o4JfuhETQ076eVHFLiBCLdhc7xudVqBzAC57Q4tYBznus1kcG5gB7YcB', '2016-12-29 18:57:02', '2016-12-29 19:31:15'),
(73, 'Đặng', '    Thế Anh', 'Thế Anh', NULL, NULL, 'tienhp19909@yahoo.com', '', '$2y$10$0SS4L3sz4mqxMeSateAi6ev8/MpoASH6e.3GjNRj8X6PK9uQR.B6W', 1, 'M', '29-8-1990', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'VdnIB25Xmwi2iGDt3rtvxy7kLz9yxSR3uSOEMuq9xJF5SeMaxuNnxTLe3WTc', '2016-12-29 22:13:27', '2017-01-07 22:44:47'),
(74, 'Nguyễn', 'Thị Thu Trang', 'Thu Trang', NULL, NULL, 'tienhp1999@yahoo.com', '', '$2y$10$.I9UgRfYStrj6aRw.zvfbuR7cTTFWzsu/TevBSEnOgmi9V4EEvcei', 1, 'F', '1-1-1989', 28, '', '', '', '', 'tien address', '', '', '', '', '', 0, 'Y', 5, 'ehEs8BqMx3sdED8vJhz6udEkQ9dXPjdNt9odW3uPLkRzQ2nB4ngB3xvMhayl', '2016-12-30 10:06:31', '2017-01-07 22:37:14'),
(75, 'Avoy', '', '', NULL, NULL, 'avoy.php008@gmail.com', '', '$2y$10$5bhrYucKUAbY0s0n1K0hweN3hKn8xBHPXiwHVWdrZFVd6HEA9D632', 1, 'M', '1-1-1952', 65, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, NULL, '2016-12-30 16:33:47', '2016-12-30 16:35:28'),
(76, 'Doãn', 'Thị Thanh An', 'An Thanh', NULL, NULL, 'tienhp1995@gmail.com', '', '$2y$10$5utuAO95ebNarKrsnpZC6uQoowLV252XEBP/6porpb80qeGO5pGbm', 1, 'F', '3-10-1983', 33, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'QRoYsdha6f9BZxoDmYL4gsjjrRdMBEFxYpZr1tTjNCLGmY6WffusQTCUuhs7', '2016-12-30 19:03:33', '2017-01-07 22:40:46'),
(77, 'Phạm', 'Thu Yến', 'Yến Phạm', NULL, NULL, 'tienhp1996@gmail.com', '', '$2y$10$32cZbMnFvBt3pEzEBsNK8u0YYSSkObR2CqjuazGSj5Vr8g4Rd1E9y', 1, 'F', '23-6-1989', 28, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'rfoeUHT0VfjmeXcugAEhOOluznlzv2p30UOKKW8vlS7ZCLgADNBNRJ6fwtrX', '2016-12-30 22:54:25', '2017-01-16 17:17:07'),
(78, '', '', '', NULL, NULL, 'avoy.php0088@gmail.com', '', '$2y$10$cJkBoN0JUaQIQah671NVGewwNpOTisCV8StqsFSCz5TuMDBGtDXUS', 1, 'M', '', 0, '', '', '', '', '', '', '', '', '', '', 0, 'E', 1, NULL, '2017-01-03 14:26:35', '2017-01-03 14:26:35'),
(79, 'Avoy08', '', 'Avo08', NULL, NULL, 'avoy.php08@gmail.com', '', '$2y$10$7aqLTBTbwX0pRO7XqOiZ7OEP1EKRTlceGtBT7Lhd9G4TNDCxpjgPm', 1, 'M', '20-10-1988', 28, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, NULL, '2017-01-11 18:51:43', '2017-01-11 18:55:42'),
(80, 'Avoy', 'Debnath', 'Avoy Debnath', NULL, NULL, 'avoy.kgecit08@gmail.com', '', '$2y$10$5Jei.Jw32Crh9Wehc9U4yukCvHat7tO92u6RzlBiEwErvcuK4JLgO', 1, 'M', '', 0, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, NULL, '2017-01-11 21:15:48', '2017-01-11 21:15:48'),
(81, '', '', '', NULL, NULL, 'test02@gmail.com', '', '$2y$10$wbujUtvuB9QGnaOiutDVpOgN/Ct3lhp6u9kkkGhsmqEL3AdLwyLJe', 1, 'M', '', 0, '', '', '', '', '', '', '', '', '', '', 0, 'E', 1, NULL, '2017-01-12 14:38:25', '2017-01-12 14:38:25'),
(82, 'Tien', ' Pham', 'Hailey', NULL, NULL, 'test04@gmail.com', '', '$2y$10$FDDnfYIhWxPFKnoUvpa5F.8/VBSXHKgR1umu74KQi43SklJZBtDGy', 1, 'M', '1-1-1994', 23, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'mwv8IumwkU3WBcNgcQRlRlzCDCermS6NIkEUxi2BTNdziJMV9nG5Iq89GEr0', '2017-01-14 22:29:12', '2017-01-14 22:58:37'),
(85, 'Dao', ' Cam Thuy', '', NULL, NULL, 'test07@gmail.com', '', '$2y$10$i.3KDSO9UmVD0wHzFfqF7ugwGuyBSRgfT7ToZWab1MQo2TzkrLFNC', 1, 'F', '1-1-1986', 31, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'hO20b4pT0JAGXQgFcKJ6FQ8tUDyafdpUIkVdC6b5Dg0Mv4YYF35ttGyJIpAK', '2017-01-16 15:59:48', '2017-01-16 16:59:18'),
(89, 'Add', ' fh', '', NULL, NULL, 'avoy.php00888@gmail.com', '', '$2y$10$kG2FKFBiUyjjPuCWwehiAOxEnUdKL9YWyCCtw/7Fb8ZETkXGeTGL2', 1, 'M', '1-1-1962', 55, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'BYYCgL4GPs0XjPKDGhWuY9qgpWfSYBN94KgsQ5Fzg7GTY6UcphQPjSj2oc0V', '2017-01-16 16:45:40', '2017-01-16 17:12:07'),
(90, 'Nguyen', ' Ngoc Huong', '', NULL, NULL, 'test09@gmail.com', '', '$2y$10$0Zmcbu7VrmAE61klhWd6L.xYatR1F7Ez0kf32Tjxu1nVzBf8DVXGW', 1, 'F', '1-1-1989', 28, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, NULL, '2017-01-16 17:00:08', '2017-01-16 17:14:00'),
(91, 'Avou', '', '', NULL, NULL, 'avoy.php0000@gmail.com', '', '$2y$10$cwaaK/iDUXKM8SHvYJsfp.3aqBJ0P/MR4J3pC2S1DAFoQjnx/PehC', 1, 'M', '1-1-1975', 42, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'lDEd36nS7KHzE6sWQkZ9xgRQVnRhLconCp54vlSRMETpkRzGBK4IP5H0domI', '2017-01-16 17:12:55', '2017-01-16 17:16:13'),
(92, 'k', ' Das', 'k Das', NULL, NULL, 'k@technoexponent.com', '', '$2y$10$JMckc/tzeCxHNhMekx0bqeW1N/Heg16Vw72WvUOXaX/ujk3FhJP/S', 1, 'M', '1-1-1950', 67, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 2, 'BUFd3SYdMaoSvoVcN8LQcyYBjxBTZ9jM8XvR0k4vSZnCwZEy23UHXnGN4Eaw', '2017-01-16 21:00:10', '2017-01-16 21:16:37'),
(93, 'Nguyễn', ' Thị Xuân', 'Xu Xù', NULL, NULL, 'test10@gmail.com', '', '$2y$10$m8Jz2ESsKrQHeuFaAVbkYecniRPzZXPcxpKWll3N2hVlmxtXnfYrq', 1, 'F', '1-1-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'L1t7QbHcXY0XnUkwKSrAB2UR3AmS4uHnUDOlp3zJu3c0mzSiiFkwY3iZRqqT', '2017-01-17 22:10:31', '2017-01-17 23:22:00'),
(94, 'Tien', ' Pham', 'Tienhp', NULL, NULL, 'test12@gmail.com', '', '$2y$10$VKW.L8yMPVvh0EVzFZqqwOhQfQ8L7FqluY8xr.8/WqGj2Sz7fusqC', 1, 'M', '1-1-1986', 31, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, NULL, '2017-01-19 17:32:21', '2017-01-19 17:36:18'),
(95, 'Tien', ' Pham', 'Tien Pham', NULL, NULL, 'test13@gmail.com', '', '$2y$10$hyGtJlml.Uf6k6ILXzGPj.Kdkpx2.pZK3haOsPP9hONxG3lStXoVO', 1, 'M', '1-1-1960', 57, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'eEIZzXSZDiV9DIjUjb4OXJPMSiofof3vyUtAzk503ED1u7PmXw66bNeeVtCc', '2017-01-19 22:55:52', '2017-01-19 23:05:15'),
(96, 'Tien', ' pham', 'Tien pham', NULL, NULL, 'test15@gmail.com', '', '$2y$10$7ojY/krRrt31AkhECiryuueLNNRA4bup7u2.qjhbrzMGjQL0oSmYq', 1, 'M', '1-1-1959', 58, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, NULL, '2017-01-19 23:25:46', '2017-01-19 23:27:34'),
(97, 'Pritam', '', 'Pritam', NULL, NULL, 'pritam008@technoexponent.com', '', '$2y$10$1oEL4GgNWI0Exg9SS.eZu.YH7d1pkz/8TE8kCSYyTfRSVCXRWrjLK', 1, 'M', '14-5-1964', 53, '', '', '', '', '', '', '', '', '', '', 0, 'E', 3, NULL, '2017-01-20 18:04:43', '2017-01-20 18:05:18'),
(98, 'Pritam', '', 'Pritam', NULL, NULL, 'pritam0008@technoexponent.com', '', '$2y$10$Js9O6t1LxQQVepq3XO9aE.G0nisL1/WBOrZ5sKxiAnxVUe0aohbl2', 1, 'M', '1-1-1984', 33, '', '', '', '', '', '', '', '', '', '', 0, 'E', 3, NULL, '2017-01-20 18:14:01', '2017-01-20 18:14:29'),
(99, 'Tien', ' Pham', 'Tien Pham', NULL, NULL, 'test14@gmail.com', '', '$2y$10$xjNbItVT335Ryc.PV.AuleHLr4EW5lx21dlhkueGqRPgnErNZXM0O', 1, 'M', '1-1-1965', 52, '', '', '', '', '', '', '', '', '', '', 0, 'E', 4, NULL, '2017-01-20 19:25:06', '2017-01-20 21:04:10'),
(100, 'Hshsh', '', 'Hshsh', NULL, NULL, 'ppp@tttt.com', '', '$2y$10$vaxcMbVXMomyrlr8uWfIgOW.Adu70WcUx9BT4KHV8nC1mqAd0qFMO', 1, 'M', '1-1-1985', 32, '', '', '', '', '', '', '', '', '', '', 0, 'E', 3, NULL, '2017-01-20 19:54:45', '2017-01-20 19:55:20');

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE IF NOT EXISTS `user_address` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `type` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '1 = address1, 2 = address2 ',
  `is_default` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT '''Y''=>''Yes'', ''N''=>''No''',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=311 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`id`, `user_id`, `address`, `type`, `is_default`, `created_at`, `updated_at`) VALUES
(13, 33, 'Arizona, United States', 1, 'N', '2016-11-16 20:18:47', '2016-11-16 20:18:47'),
(14, 33, 'Florida, United States', 2, 'N', '2016-11-16 20:18:52', '2016-11-16 20:18:52'),
(25, 33, 'FNB Stadium, Johannesburg, South Africa', 1, 'N', '2016-11-22 12:18:59', '2016-11-22 12:18:59'),
(27, 33, 'London, United Kingdom', 2, 'N', '2016-11-22 12:23:50', '2016-11-22 12:23:50'),
(33, 22, 'Hanoi, Vietnam', 1, 'Y', '2016-11-22 15:06:54', '2016-12-27 22:36:43'),
(36, 44, 'Cologne, Germany', 1, 'N', '2016-11-23 18:15:08', '2016-11-23 18:15:08'),
(37, 44, 'Cologne, Germany', 2, 'N', '2016-11-23 18:18:27', '2016-11-23 18:18:27'),
(40, 45, 'Dallas, TX, United States', 1, 'N', '2016-11-23 19:22:33', '2016-11-23 19:22:33'),
(41, 46, 'Kolkata, Belgachia, West Bengal, India', 1, 'N', '2016-11-23 19:29:27', '2016-11-23 19:29:27'),
(53, 24, 'Hoàng Mai, Hanoi, Vietnam', 1, 'N', '2016-11-24 22:47:19', '2016-12-22 21:54:07'),
(54, 24, 'Hong Kong', 2, 'N', '2016-11-24 22:50:11', '2016-12-22 21:54:07'),
(57, 1, 'San Francisco, CA, United States', 2, 'N', '2016-11-28 15:27:53', '2017-01-06 13:04:30'),
(70, 48, 'Bangkok Thailand', 2, 'N', '2016-11-29 09:45:00', '2017-01-07 23:10:04'),
(79, 47, 'West Java, Indonesia', 1, 'N', '2016-11-29 19:39:34', '2016-11-29 19:39:34'),
(81, 47, 'Paris, France', 1, 'N', '2016-11-30 15:06:13', '2016-11-30 15:06:13'),
(84, 47, 'Kolkata, West Bengal, India', 1, 'N', '2016-11-30 20:20:23', '2016-11-30 20:20:23'),
(85, 47, 'Pennsylvania, United States', 1, 'N', '2016-11-30 20:21:03', '2016-11-30 20:21:03'),
(105, 52, 'Bắc Kạn Province, Vietnam', 1, 'N', '2016-11-30 22:45:34', '2016-11-30 22:46:44'),
(106, 52, 'Hanoi, Vietnam', 1, 'Y', '2016-11-30 22:46:38', '2016-11-30 22:46:44'),
(109, 52, 'Singapore', 2, 'N', '2016-11-30 22:47:51', '2016-11-30 22:47:51'),
(110, 52, 'Thua Thien Hue, Vietnam', 2, 'N', '2016-11-30 22:48:07', '2016-11-30 22:48:07'),
(112, 53, 'Hanoi, Vietnam', 1, 'Y', '2016-12-01 22:09:28', '2017-01-07 23:05:42'),
(119, 31, 'Nghe An, Vietnam', 1, 'Y', '2016-12-01 23:08:11', '2016-12-01 23:08:34'),
(120, 31, 'Hanoi', 1, 'N', '2016-12-01 23:08:18', '2016-12-01 23:08:34'),
(122, 54, '126 Princes Highway, Bolwarra, Victoria, Úc', 2, 'N', '2016-12-03 19:17:24', '2016-12-03 19:17:24'),
(129, 57, '126 Princes Highway, Bolwarra, Victoria, Úc', 1, 'Y', '2016-12-04 08:46:33', '2016-12-04 08:46:41'),
(130, 57, '126 Princes Highway, Bolwarra, Victoria, Úc', 2, 'N', '2016-12-04 08:46:45', '2016-12-04 08:46:45'),
(131, 57, 'SD, Hoa Kỳ', 1, 'N', '2016-12-04 09:04:49', '2016-12-04 09:04:49'),
(132, 47, 'Paris, France', 1, 'N', '2016-12-05 17:58:54', '2016-12-05 17:58:54'),
(140, 24, 'Hải Phòng, Vietnam', 1, 'Y', '2016-12-11 21:03:36', '2016-12-22 21:54:07'),
(142, 24, 'Singapore', 2, 'N', '2016-12-11 21:04:57', '2016-12-22 21:54:07'),
(143, 24, 'Dubai - United Arab Emirates', 2, 'N', '2016-12-11 21:05:04', '2016-12-22 21:54:07'),
(155, 24, 'Ninh Bình Province, Vietnam', 1, 'N', '2016-12-16 21:35:33', '2016-12-22 21:54:07'),
(156, 24, 'Boracay, Malay, Western Visayas, Philippines', 2, 'N', '2016-12-16 21:53:34', '2016-12-22 21:54:07'),
(158, 24, 'Singapore', 2, 'N', '2016-12-16 21:54:05', '2016-12-22 21:54:07'),
(159, 24, 'Penang Malaysia', 2, 'N', '2016-12-16 21:54:17', '2016-12-22 21:54:07'),
(186, 1, 'Japan', 2, 'N', '2016-12-20 20:51:02', '2017-01-06 13:04:30'),
(187, 61, 'Hanoi, Vietnam', 1, 'Y', '2016-12-20 21:22:34', '2017-01-07 23:00:09'),
(201, 1, 'Dubai - United Arab Emirates', 1, 'N', '2016-12-26 19:00:11', '2017-01-06 13:04:30'),
(209, 68, 'Kolkata, West Bengal, India', 2, 'N', '2016-12-27 15:52:27', '2016-12-27 15:52:27'),
(211, 49, 'Hà Nội, Việt Nam', 1, 'Y', '2016-12-27 21:55:37', '2016-12-27 21:55:40'),
(212, 60, 'Hanoi, Việt Nam', 1, 'Y', '2016-12-27 22:03:18', '2016-12-27 22:03:18'),
(213, 60, 'Quỳnh Lưu, Nghệ An, Việt Nam', 1, 'N', '2016-12-27 22:03:35', '2016-12-27 22:03:35'),
(215, 55, 'Hanoi, Việt Nam', 1, 'Y', '2016-12-27 22:05:51', '2016-12-27 22:05:51'),
(216, 55, 'Hải Phòng, Việt Nam', 1, 'N', '2016-12-27 22:06:49', '2016-12-27 22:06:49'),
(217, 55, 'Michigan, Hoa Kỳ', 1, 'N', '2016-12-27 22:07:01', '2016-12-27 22:07:01'),
(218, 35, 'Hanoi, Việt Nam', 1, 'Y', '2016-12-27 22:19:36', '2017-01-08 23:04:08'),
(219, 23, 'Portsmouth, Vương Quốc Anh', 1, 'Y', '2016-12-27 22:29:09', '2016-12-27 22:29:09'),
(220, 23, 'Hà Nội, Việt Nam', 1, 'N', '2016-12-27 22:29:16', '2016-12-27 22:29:16'),
(221, 22, 'Lào Cai, Việt Nam', 1, 'N', '2016-12-27 22:37:00', '2016-12-27 22:37:00'),
(223, 69, 'Hà Nội, Việt Nam', 1, 'Y', '2016-12-27 22:49:07', '2016-12-27 22:49:07'),
(225, 48, 'Hanoi, Việt Nam', 1, 'Y', '2016-12-28 22:28:46', '2017-01-07 23:10:04'),
(227, 70, 'Hanoi, Vietnam', 1, 'Y', '2016-12-28 23:10:22', '2016-12-28 23:10:22'),
(230, 70, 'Thai Binh, Vietnam', 1, 'N', '2016-12-28 23:13:44', '2016-12-28 23:13:44'),
(232, 72, 'State of São Paulo, Brazil', 1, 'N', '2016-12-29 19:33:37', '2016-12-29 19:34:10'),
(233, 72, 'Houston, TX, United States', 1, 'Y', '2016-12-29 19:34:01', '2016-12-29 19:34:10'),
(234, 73, 'Lạng Sơn, Việt Nam', 1, 'N', '2016-12-29 22:16:09', '2017-01-07 22:41:54'),
(235, 73, 'Hanoi, Việt Nam', 1, 'Y', '2016-12-29 22:16:18', '2017-01-07 22:41:54'),
(237, 74, 'Hanoi, Vietnam', 1, 'Y', '2016-12-30 10:07:35', '2016-12-30 10:07:35'),
(238, 75, 'Panama City, Panama', 2, 'N', '2016-12-30 16:34:47', '2016-12-30 16:34:47'),
(239, 75, 'Kitchener Public Library, Queen Street North, Kitchener, ON, Canada', 1, 'Y', '2016-12-30 16:35:04', '2016-12-30 16:35:04'),
(240, 77, 'Hanoi, Vietnam', 1, 'Y', '2016-12-30 22:55:13', '2016-12-30 22:55:13'),
(245, 68, 'Kolkata, West Bengal, India', 1, 'Y', '2017-01-03 14:17:59', '2017-01-03 14:17:59'),
(246, 77, 'Cửa Ông, Quảng Ninh, Việt Nam', 1, 'N', '2017-01-03 22:22:00', '2017-01-03 22:22:00'),
(247, 77, 'Kuala Lumpur Ma-lai-xi-a', 2, 'N', '2017-01-03 22:23:46', '2017-01-03 22:23:46'),
(249, 1, 'San Francisco, CA, United States', 1, 'Y', '2017-01-06 13:04:01', '2017-01-06 13:04:30'),
(250, 76, 'Hanoi, Việt Nam', 1, 'Y', '2017-01-07 22:24:43', '2017-01-07 22:24:43'),
(251, 76, 'Thành phố Hạ Long, Quảng Ninh, Việt Nam', 1, 'Y', '2017-01-07 22:25:06', '2017-01-08 22:51:22'),
(252, 74, 'Ninh Bình, Việt Nam', 1, 'N', '2017-01-07 22:32:04', '2017-01-07 22:32:04'),
(253, 74, 'Kuala Lumpur Ma-lai-xi-a', 2, 'N', '2017-01-07 22:35:32', '2017-01-07 22:35:32'),
(254, 74, 'Pahang Ma-lai-xi-a', 2, 'N', '2017-01-07 22:35:38', '2017-01-07 22:35:38'),
(255, 74, 'Vinh, Nghệ An, Việt Nam', 2, 'N', '2017-01-07 22:35:44', '2017-01-07 22:35:44'),
(256, 74, 'Cao Bằng, Việt Nam', 2, 'N', '2017-01-07 22:35:48', '2017-01-07 22:35:48'),
(257, 74, 'Đà Nẵng, Việt Nam', 2, 'N', '2017-01-07 22:36:02', '2017-01-07 22:36:02'),
(258, 74, 'Hàm Yên, Tuyên Quang, Việt Nam', 2, 'N', '2017-01-07 22:36:07', '2017-01-07 22:36:07'),
(259, 74, 'Hồ Chí Minh, Việt Nam', 2, 'N', '2017-01-07 22:36:12', '2017-01-07 22:36:12'),
(260, 76, 'Hồ Chí Minh, Việt Nam', 2, 'N', '2017-01-07 22:38:47', '2017-01-07 22:38:47'),
(261, 76, 'Thành phố Qui Nhơn, Bình Định, Việt Nam', 2, 'N', '2017-01-07 22:38:52', '2017-01-07 22:38:52'),
(262, 76, 'Nha Trang, Khánh Hòa, Việt Nam', 2, 'N', '2017-01-07 22:39:45', '2017-01-07 22:39:45'),
(263, 76, 'Yangon Region, Mi-an-ma (Miến Điện)', 2, 'N', '2017-01-07 22:40:05', '2017-01-07 22:40:05'),
(265, 76, 'Bagan, Mi-an-ma (Miến Điện)', 2, 'N', '2017-01-07 22:40:32', '2017-01-07 22:40:32'),
(266, 70, 'Malaysia', 2, 'N', '2017-01-07 22:54:38', '2017-01-07 22:54:38'),
(267, 70, 'Singapore', 2, 'N', '2017-01-07 22:54:43', '2017-01-07 22:54:43'),
(268, 69, 'Việt Trì, Phú Thọ, Việt Nam', 1, 'N', '2017-01-07 22:56:02', '2017-01-07 22:56:02'),
(269, 69, 'Sam Nuea, Hủa phăn, Lào', 2, 'N', '2017-01-07 22:58:13', '2017-01-07 22:58:13'),
(270, 69, 'Cô Tô, Quảng Ninh, Việt Nam', 2, 'N', '2017-01-07 22:58:28', '2017-01-07 22:58:28'),
(271, 69, 'Đà Nẵng, Việt Nam', 2, 'N', '2017-01-07 22:58:32', '2017-01-07 22:58:32'),
(272, 61, 'Lạng Sơn, Việt Nam', 1, 'N', '2017-01-07 23:00:16', '2017-01-07 23:00:16'),
(273, 60, 'Vũng Tàu, Bà Rịa - Vũng Tàu, Việt Nam', 2, 'N', '2017-01-07 23:02:37', '2017-01-07 23:02:37'),
(274, 53, 'Bắc Giang, Việt Nam', 1, 'N', '2017-01-07 23:05:38', '2017-01-07 23:05:42'),
(275, 53, 'Singapore', 2, 'N', '2017-01-07 23:07:46', '2017-01-07 23:07:46'),
(276, 53, 'Kuala Lumpur Ma-lai-xi-a', 2, 'N', '2017-01-07 23:07:55', '2017-01-07 23:07:55'),
(277, 53, 'Dubai - Các tiểu Vương quốc Ả rập Thống nhất', 2, 'N', '2017-01-07 23:07:59', '2017-01-07 23:07:59'),
(278, 53, 'New York, Hoa Kỳ', 2, 'N', '2017-01-07 23:08:06', '2017-01-07 23:08:06'),
(279, 49, 'Hải Phòng, Việt Nam', 1, 'N', '2017-01-07 23:09:41', '2017-01-07 23:09:41'),
(280, 35, 'Thái Bình, Việt Nam', 1, 'N', '2017-01-07 23:16:24', '2017-01-08 23:04:08'),
(281, 35, 'Bangkok Thái Lan', 2, 'N', '2017-01-07 23:18:50', '2017-01-08 23:04:08'),
(282, 19, 'Hanoi, Việt Nam', 1, 'Y', '2017-01-07 23:20:00', '2017-01-07 23:20:00'),
(283, 19, 'Hải Phòng, Việt Nam', 1, 'N', '2017-01-07 23:20:05', '2017-01-07 23:20:05'),
(284, 35, 'Đà Lạt, Lâm Đồng, Vietnam', 2, 'N', '2017-01-08 21:12:57', '2017-01-08 23:04:08'),
(285, 35, 'Bãi Cháy, Quảng Ninh Province, Vietnam', 2, 'N', '2017-01-08 21:13:22', '2017-01-08 23:04:08'),
(286, 35, 'Cao nguyên đá Đồng Văn, Quốc lộ 4C, Sà Phìn, Ha Giang, Vietnam', 2, 'N', '2017-01-08 21:14:03', '2017-01-08 23:04:08'),
(287, 79, 'Pandua Road, Pandua, West Bengal, India', 1, 'Y', '2017-01-11 18:52:27', '2017-01-11 18:52:27'),
(288, 80, 'Chanda Nagar, Hyderabad, Telangana, India', 1, 'Y', '2017-01-11 21:16:46', '2017-01-11 21:16:46'),
(289, 82, 'Thai Nguyen, Vietnam', 1, 'N', '2017-01-14 22:30:28', '2017-01-14 22:31:10'),
(291, 82, 'Hanoi, Vietnam', 1, 'Y', '2017-01-14 22:31:05', '2017-01-14 22:31:10'),
(293, 85, 'Hanoi, Vietnam', 1, 'Y', '2017-01-16 16:01:13', '2017-01-16 16:01:13'),
(295, 77, 'Da Nang, Vietnam', 2, 'N', '2017-01-16 16:35:57', '2017-01-16 16:35:57'),
(298, 89, 'Kolkata, West Bengal, India', 1, 'Y', '2017-01-16 16:53:31', '2017-01-16 16:53:31'),
(299, 90, 'Hanoi, Vietnam', 1, 'Y', '2017-01-16 17:00:46', '2017-01-16 17:00:46'),
(300, 91, 'L.P. Leviste Street, Makati, NCR, Philippines', 1, 'Y', '2017-01-16 17:13:24', '2017-01-16 17:13:24'),
(301, 92, 'Dallas, TX, United States', 1, 'Y', '2017-01-16 21:01:38', '2017-01-16 21:01:38'),
(303, 93, 'Thai Nguyen, Vietnam', 1, 'Y', '2017-01-17 22:13:34', '2017-01-17 22:15:34'),
(304, 94, 'Hinganghat, Maharashtra, India', 1, 'Y', '2017-01-19 17:32:58', '2017-01-19 17:32:58'),
(305, 95, 'Hanoi, Vietnam', 1, 'Y', '2017-01-19 22:56:44', '2017-01-19 22:56:44'),
(306, 96, 'Hanoi, Vietnam', 1, 'Y', '2017-01-19 23:26:12', '2017-01-19 23:26:12'),
(307, 97, 'Kolkata, West Bengal, India', 1, 'Y', '2017-01-20 18:05:13', '2017-01-20 18:05:13'),
(308, 98, 'Kolkata, West Bengal, India', 1, 'Y', '2017-01-20 18:14:26', '2017-01-20 18:14:26'),
(309, 99, 'Hanoi, Vietnam', 1, 'Y', '2017-01-20 19:26:19', '2017-01-20 19:26:19'),
(310, 100, 'LPLK Tri Sakti, Bendogerit, Blitar City, East Java, Indonesia', 1, 'Y', '2017-01-20 19:55:15', '2017-01-20 19:55:15');

-- --------------------------------------------------------

--
-- Table structure for table `work_experiences`
--

CREATE TABLE IF NOT EXISTS `work_experiences` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` int(10) unsigned NOT NULL,
  `from_month` int(10) unsigned NOT NULL,
  `from_year` int(10) unsigned NOT NULL,
  `to_month` int(10) unsigned NOT NULL,
  `to_year` int(10) unsigned NOT NULL,
  `status` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Active, N = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=343 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `work_experiences`
--

INSERT INTO `work_experiences` (`id`, `user_id`, `title`, `company`, `department`, `city`, `country`, `from_month`, `from_year`, `to_month`, `to_year`, `status`, `created_at`, `updated_at`) VALUES
(1, 17, 'Automation', 'test agaency', 'asadad', 'tst', 13, 2, 1975, 6, 1985, 'Y', '2016-11-11 10:06:04', '2016-11-11 10:06:04'),
(2, 17, 'Automation', 'abctest', 'Software', 'ANy', 9, 7, 1983, 10, 1987, 'Y', '2016-11-11 10:19:07', '2016-11-11 10:19:07'),
(3, 17, 'new cat', 'abctest', 'asadad', 'city', 14, 5, 1987, 9, 2011, 'Y', '2016-11-11 10:19:36', '2016-11-11 10:19:36'),
(5, 26, 'City Council Member', 'Công ty cổ phần Bamboo Capital (BCG)', 'City Council', 'any', 6, 10, 2000, 12, 2006, 'Y', '2016-11-15 12:42:13', '2016-11-15 12:42:13'),
(12, 30, 'Chief Operating Officer', 'Công ty cổ phần Bamboo Capital (BCG)', 'IT', 'kolkata', 9, 2, 1985, 2, 1994, 'Y', '2016-11-15 21:30:26', '2016-11-15 21:30:26'),
(20, 45, 'Chief Operating Officer,Department Store', 'Công ty cổ phần Bê tông Becamex (BECAMEX ACC)', 'Department Store', 'Brazilian Plast Embalagens - Rua Barão de Duprat - Sé - State of São Paulo, Brazil', 0, 2, 1985, 11, 2016, 'Y', '2016-11-23 19:34:54', '2016-11-23 19:39:38'),
(22, 46, 'Department Store General Manager', 'Công ty Cổ phần Ánh Dương Việt Nam (Vinasun corp.)', 'sal', 'Kentucky, United States', 0, 9, 1988, 11, 2016, 'Y', '2016-11-23 20:21:14', '2016-11-23 20:21:14'),
(25, 1, 'County Commissioner', 'Công ty Cổ phần Bao bì Biên Hòa (SOVI)', 'City Council', 'San Francisco, CA, United States', 0, 10, 1982, 10, 1988, 'Y', '2016-11-24 12:15:28', '2016-11-29 12:55:42'),
(28, 24, 'Trade Finance', 'Standard Charter', '', 'Hanoi, Vietnam', 0, 0, 2008, 0, 2005, 'Y', '2016-11-24 22:54:37', '2016-12-27 22:16:12'),
(37, 47, 'test', 'techno exponent dfgdfg dfgdfgdfgfd gdfg dfg dfgdfgdfg', '', 'Kolkata, West Bengal, India', 0, 10, 1986, 9, 1998, 'Y', '2016-11-29 19:37:43', '2016-11-30 14:05:11'),
(39, 49, 'Marketing Excutive', 'VietinBank', 'Product Development & Marketing', 'Hanoi, Vietnam', 0, 0, 0, 11, 2016, 'Y', '2016-11-30 10:50:03', '2016-11-30 15:50:47'),
(40, 49, 'Sales Supervisor', 'Unilever', 'North Region', 'Bac Ninh Province, Vietnam', 0, 0, 2012, 11, 2013, 'Y', '2016-11-30 10:52:32', '2016-11-30 10:52:32'),
(44, 1, 'Government Service Executive', 'Công ty Cổ phần Bao bì Biên Hòa (SOVI)', '', 'South Carolina, United States', 0, 11, 2006, 0, 2016, 'Y', '2016-11-30 18:51:11', '2016-12-07 15:46:03'),
(46, 52, 'Payment ', 'VietinBank', 'Product Developent & Marketingp', 'Hanoi, Vietnam', 0, 0, 2011, 11, 2016, 'Y', '2016-11-30 22:53:29', '2016-12-01 23:09:32'),
(50, 57, '12', '12', '12', 'Trường THPT Nguyễn Thị Minh Khai, Điện Biên Phủ, Hồ Chí Minh, Việt Nam', 0, 2, 1985, 11, 2014, 'Y', '2016-12-04 09:05:43', '2016-12-04 09:06:07'),
(51, 1, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Bất động sản Du lịch Ninh Vân Bay (Ninh Van Bay)', '', 'South Carolina, United States', 0, 5, 1993, 4, 2004, 'Y', '2016-12-07 17:03:37', '2017-01-06 19:53:03'),
(57, 60, 'Kế toán', 'PVC', '', 'Vũng Tàu, Bà Rịa - Vũng Tàu, Việt Nam', 0, 0, 2009, 0, 2012, 'Y', '2016-12-19 22:55:37', '2016-12-27 22:01:26'),
(63, 60, 'Quan hệ khách hàng', 'VietinBank', '', 'Hanoi, Việt Nam', 0, 0, 2013, 0, 2016, 'Y', '2016-12-27 22:02:10', '2016-12-27 22:02:10'),
(64, 55, 'Developer', 'East Agile', '', 'Hanoi, Việt Nam', 0, 0, 2012, 0, 2016, 'Y', '2016-12-27 22:07:26', '2016-12-27 22:07:26'),
(65, 53, 'Trưởng phòng ', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng KHDN - Chi nhánh Bắc Giang', 'Hà Nội, Việt Nam', 0, 0, 1981, 0, 1985, 'Y', '2016-12-27 22:11:28', '2017-01-07 23:07:11'),
(68, 31, 'Founder', 'KissStartup', '', 'Hà Nội, Việt Nam', 0, 0, 1985, 0, 2003, 'Y', '2016-12-27 22:23:35', '2016-12-27 22:23:35'),
(69, 23, 'FDI Executive', 'VietinBank', '', 'Hà Nội, Việt Nam', 0, 0, 1974, 0, 1981, 'Y', '2016-12-27 22:29:37', '2016-12-27 22:29:37'),
(70, 19, 'Administrator', 'iFact', '', 'Hà Nội, Việt Nam', 0, 0, 1974, 0, 1974, 'Y', '2016-12-27 22:32:39', '2016-12-27 22:32:39'),
(73, 77, 'Chuyên viên tiền gửi khách hàng doanh nghiệp', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', '', 'Hanoi, Việt Nam', 0, 0, 2013, 0, 2017, 'Y', '2017-01-03 23:02:43', '2017-01-03 23:02:43'),
(89, 76, 'Phó phòng', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng PTSP&Marketing - Khối KHDN', 'Hanoi, Việt Nam', 0, 0, 2015, 0, 2017, 'Y', '2017-01-07 22:27:48', '2017-01-07 22:27:48'),
(90, 74, 'Chuyên viên Ngân hàng điện tử', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng PTSP&Marketing - Khối KHDN', 'Hanoi, Việt Nam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-07 22:34:14', '2017-01-07 22:34:32'),
(91, 73, 'Chuyên viên quan hệ khách hàng', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', 'Sở giao dịch', 'Hanoi, Việt Nam', 0, 0, 2013, 0, 2017, 'Y', '2017-01-07 22:42:46', '2017-01-07 22:42:46'),
(92, 70, 'Chuyên viên marketing', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng Marketing - Khối bán lẻ', 'Hanoi, Việt Nam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-07 22:50:51', '2017-01-07 22:50:51'),
(93, 48, 'Kế toán viên', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng Kế toán tài chính', 'Hanoi, Việt Nam', 0, 0, 2010, 0, 2017, 'Y', '2017-01-07 23:13:57', '2017-01-07 23:13:57'),
(94, 35, 'Nhân viên sự kiện', 'VTC INTECOM', '', 'Hanoi, Việt Nam', 0, 0, 1981, 0, 1986, 'Y', '2017-01-07 23:18:04', '2017-01-07 23:18:04'),
(95, 79, 'Air Pollution Compliance Inspector', 'Công ty Cổ phần Dịch vụ Hàng hóa Nội Bài (NCTS)', 'IT', 'Kolkata, West Bengal, India', 0, 12, 2013, 1, 2016, 'Y', '2017-01-11 18:53:26', '2017-01-13 19:05:21'),
(96, 80, 'General Manager', 'Techno Exponent', 'IT', 'Kolkata, West Bengal, India', 0, 2, 1984, 1, 2017, 'Y', '2017-01-11 21:26:05', '2017-01-11 21:27:46'),
(97, 82, 'HR Clerk', 'Công ty Cổ phần FPT (FPT Corp)', '', 'Hanoi, Vietnam', 0, 0, 2012, 0, 2017, 'Y', '2017-01-14 22:38:39', '2017-01-14 22:38:39'),
(98, 90, '', '', '', '', 0, 0, 0, 0, 2017, 'Y', '2017-01-16 17:01:01', '2017-01-16 17:01:01'),
(99, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 0, 0, 2017, 'Y', '2017-01-16 17:01:43', '2017-01-16 17:01:43'),
(100, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:51', '2017-01-16 17:01:51'),
(101, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:51', '2017-01-16 17:01:51'),
(102, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:51', '2017-01-16 17:01:51'),
(103, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:52', '2017-01-16 17:01:52'),
(104, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:52', '2017-01-16 17:01:52'),
(105, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:52', '2017-01-16 17:01:52'),
(106, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:52', '2017-01-16 17:01:52'),
(107, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:52', '2017-01-16 17:01:52'),
(108, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:52', '2017-01-16 17:01:52'),
(109, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:52', '2017-01-16 17:01:52'),
(110, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:52', '2017-01-16 17:01:52'),
(111, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:52', '2017-01-16 17:01:52'),
(112, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:52', '2017-01-16 17:01:52'),
(113, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:52', '2017-01-16 17:01:52'),
(114, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:53', '2017-01-16 17:01:53'),
(115, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:53', '2017-01-16 17:01:53'),
(116, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:53', '2017-01-16 17:01:53'),
(117, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:53', '2017-01-16 17:01:53'),
(118, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:53', '2017-01-16 17:01:53'),
(119, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:53', '2017-01-16 17:01:53'),
(120, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:53', '2017-01-16 17:01:53'),
(121, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:53', '2017-01-16 17:01:53'),
(122, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:53', '2017-01-16 17:01:53'),
(123, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:53', '2017-01-16 17:01:53'),
(124, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:53', '2017-01-16 17:01:53'),
(125, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:53', '2017-01-16 17:01:53'),
(126, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:53', '2017-01-16 17:01:53'),
(127, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:53', '2017-01-16 17:01:53'),
(128, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:54', '2017-01-16 17:01:54'),
(129, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:54', '2017-01-16 17:01:54'),
(130, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:54', '2017-01-16 17:01:54'),
(131, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:54', '2017-01-16 17:01:54'),
(132, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:54', '2017-01-16 17:01:54'),
(133, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:54', '2017-01-16 17:01:54'),
(134, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:54', '2017-01-16 17:01:54'),
(135, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:54', '2017-01-16 17:01:54'),
(136, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:54', '2017-01-16 17:01:54'),
(137, 90, 'General Manager', 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '', 'Hanoi, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-16 17:01:54', '2017-01-16 17:01:54'),
(138, 89, 'Department Store General Manager', 'Techno exponent', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 2, 1975, 0, 2017, 'Y', '2017-01-16 17:11:30', '2017-01-16 17:11:30'),
(139, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:24', '2017-01-16 17:14:24'),
(140, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:25', '2017-01-16 17:14:25'),
(141, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:25', '2017-01-16 17:14:25'),
(142, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:25', '2017-01-16 17:14:25'),
(143, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:25', '2017-01-16 17:14:25'),
(144, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:25', '2017-01-16 17:14:25'),
(145, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:25', '2017-01-16 17:14:25'),
(146, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:25', '2017-01-16 17:14:25'),
(147, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:26', '2017-01-16 17:14:26'),
(148, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:26', '2017-01-16 17:14:26'),
(149, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:26', '2017-01-16 17:14:26'),
(150, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:26', '2017-01-16 17:14:26'),
(151, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:26', '2017-01-16 17:14:26'),
(152, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:26', '2017-01-16 17:14:26'),
(153, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:26', '2017-01-16 17:14:26'),
(154, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:27', '2017-01-16 17:14:27'),
(155, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:27', '2017-01-16 17:14:27'),
(156, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:27', '2017-01-16 17:14:27'),
(157, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:27', '2017-01-16 17:14:27'),
(158, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:27', '2017-01-16 17:14:27'),
(159, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:27', '2017-01-16 17:14:27'),
(160, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:27', '2017-01-16 17:14:27'),
(161, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:27', '2017-01-16 17:14:27'),
(162, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:27', '2017-01-16 17:14:27'),
(163, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:27', '2017-01-16 17:14:27'),
(164, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:28', '2017-01-16 17:14:28'),
(165, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:28', '2017-01-16 17:14:28'),
(166, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:28', '2017-01-16 17:14:28'),
(167, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:28', '2017-01-16 17:14:28'),
(168, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:28', '2017-01-16 17:14:28'),
(169, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:28', '2017-01-16 17:14:28'),
(170, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:28', '2017-01-16 17:14:28'),
(171, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:28', '2017-01-16 17:14:28'),
(172, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:29', '2017-01-16 17:14:29'),
(173, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:29', '2017-01-16 17:14:29'),
(174, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:29', '2017-01-16 17:14:29'),
(175, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:29', '2017-01-16 17:14:29'),
(176, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:29', '2017-01-16 17:14:29'),
(177, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:29', '2017-01-16 17:14:29'),
(178, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:29', '2017-01-16 17:14:29'),
(179, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:29', '2017-01-16 17:14:29'),
(180, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:29', '2017-01-16 17:14:29'),
(181, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:30', '2017-01-16 17:14:30'),
(182, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:30', '2017-01-16 17:14:30'),
(183, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:30', '2017-01-16 17:14:30'),
(184, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:30', '2017-01-16 17:14:30'),
(185, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:30', '2017-01-16 17:14:30'),
(186, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:30', '2017-01-16 17:14:30'),
(187, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:30', '2017-01-16 17:14:30'),
(188, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:30', '2017-01-16 17:14:30'),
(189, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:30', '2017-01-16 17:14:30'),
(190, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:30', '2017-01-16 17:14:30'),
(191, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:31', '2017-01-16 17:14:31'),
(192, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:31', '2017-01-16 17:14:31'),
(193, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:31', '2017-01-16 17:14:31'),
(194, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:31', '2017-01-16 17:14:31'),
(195, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:31', '2017-01-16 17:14:31'),
(196, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:32', '2017-01-16 17:14:32'),
(197, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:32', '2017-01-16 17:14:32'),
(198, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:32', '2017-01-16 17:14:32'),
(199, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:32', '2017-01-16 17:14:32'),
(200, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:32', '2017-01-16 17:14:32'),
(201, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:32', '2017-01-16 17:14:32'),
(202, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:32', '2017-01-16 17:14:32'),
(203, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:32', '2017-01-16 17:14:32'),
(204, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:33', '2017-01-16 17:14:33'),
(205, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:33', '2017-01-16 17:14:33'),
(206, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:33', '2017-01-16 17:14:33'),
(207, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:33', '2017-01-16 17:14:33'),
(208, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:33', '2017-01-16 17:14:33'),
(209, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:33', '2017-01-16 17:14:33'),
(210, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:33', '2017-01-16 17:14:33'),
(211, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:33', '2017-01-16 17:14:33'),
(212, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:34', '2017-01-16 17:14:34'),
(213, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:34', '2017-01-16 17:14:34'),
(214, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:34', '2017-01-16 17:14:34'),
(215, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:34', '2017-01-16 17:14:34'),
(216, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:35', '2017-01-16 17:14:35'),
(217, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:35', '2017-01-16 17:14:35'),
(218, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:35', '2017-01-16 17:14:35'),
(219, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:35', '2017-01-16 17:14:35'),
(220, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:35', '2017-01-16 17:14:35'),
(221, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:35', '2017-01-16 17:14:35'),
(222, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:35', '2017-01-16 17:14:35'),
(223, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:35', '2017-01-16 17:14:35'),
(224, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:36', '2017-01-16 17:14:36'),
(225, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:36', '2017-01-16 17:14:36'),
(226, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:36', '2017-01-16 17:14:36'),
(227, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:36', '2017-01-16 17:14:36'),
(228, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:36', '2017-01-16 17:14:36'),
(229, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:36', '2017-01-16 17:14:36'),
(230, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:36', '2017-01-16 17:14:36'),
(231, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:36', '2017-01-16 17:14:36'),
(232, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:37', '2017-01-16 17:14:37'),
(233, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:37', '2017-01-16 17:14:37'),
(234, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:37', '2017-01-16 17:14:37'),
(235, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:37', '2017-01-16 17:14:37'),
(236, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:37', '2017-01-16 17:14:37'),
(237, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:37', '2017-01-16 17:14:37'),
(238, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:37', '2017-01-16 17:14:37'),
(239, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:37', '2017-01-16 17:14:37'),
(240, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:38', '2017-01-16 17:14:38'),
(241, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:38', '2017-01-16 17:14:38'),
(242, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:38', '2017-01-16 17:14:38'),
(243, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:38', '2017-01-16 17:14:38'),
(244, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:39', '2017-01-16 17:14:39'),
(245, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:39', '2017-01-16 17:14:39'),
(246, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:39', '2017-01-16 17:14:39'),
(247, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:39', '2017-01-16 17:14:39'),
(248, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:39', '2017-01-16 17:14:39'),
(249, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:39', '2017-01-16 17:14:39'),
(250, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:39', '2017-01-16 17:14:39'),
(251, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:39', '2017-01-16 17:14:39'),
(252, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:40', '2017-01-16 17:14:40'),
(253, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:40', '2017-01-16 17:14:40'),
(254, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:40', '2017-01-16 17:14:40'),
(255, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:40', '2017-01-16 17:14:40'),
(256, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:40', '2017-01-16 17:14:40'),
(257, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:40', '2017-01-16 17:14:40'),
(258, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:40', '2017-01-16 17:14:40'),
(259, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:40', '2017-01-16 17:14:40'),
(260, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:41', '2017-01-16 17:14:41'),
(261, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:41', '2017-01-16 17:14:41'),
(262, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:41', '2017-01-16 17:14:41'),
(263, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:41', '2017-01-16 17:14:41'),
(264, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:42', '2017-01-16 17:14:42'),
(265, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:42', '2017-01-16 17:14:42'),
(266, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:42', '2017-01-16 17:14:42'),
(267, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:42', '2017-01-16 17:14:42'),
(268, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:42', '2017-01-16 17:14:42'),
(269, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:42', '2017-01-16 17:14:42'),
(270, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:42', '2017-01-16 17:14:42'),
(271, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:42', '2017-01-16 17:14:42'),
(272, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:43', '2017-01-16 17:14:43'),
(273, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:43', '2017-01-16 17:14:43'),
(274, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:43', '2017-01-16 17:14:43'),
(275, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:43', '2017-01-16 17:14:43'),
(276, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:43', '2017-01-16 17:14:43'),
(277, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:43', '2017-01-16 17:14:43'),
(278, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:43', '2017-01-16 17:14:43'),
(279, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:43', '2017-01-16 17:14:43'),
(280, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:44', '2017-01-16 17:14:44'),
(281, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:44', '2017-01-16 17:14:44'),
(282, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:44', '2017-01-16 17:14:44'),
(283, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:44', '2017-01-16 17:14:44'),
(284, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:45', '2017-01-16 17:14:45'),
(285, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:45', '2017-01-16 17:14:45'),
(286, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:45', '2017-01-16 17:14:45'),
(287, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:45', '2017-01-16 17:14:45'),
(288, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:45', '2017-01-16 17:14:45'),
(289, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:45', '2017-01-16 17:14:45'),
(290, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:45', '2017-01-16 17:14:45'),
(291, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:45', '2017-01-16 17:14:45'),
(292, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:46', '2017-01-16 17:14:46'),
(293, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:46', '2017-01-16 17:14:46'),
(294, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:46', '2017-01-16 17:14:46'),
(295, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:46', '2017-01-16 17:14:46'),
(296, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:46', '2017-01-16 17:14:46'),
(297, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:46', '2017-01-16 17:14:46'),
(298, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:46', '2017-01-16 17:14:46'),
(299, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:47', '2017-01-16 17:14:47');
INSERT INTO `work_experiences` (`id`, `user_id`, `title`, `company`, `department`, `city`, `country`, `from_month`, `from_year`, `to_month`, `to_year`, `status`, `created_at`, `updated_at`) VALUES
(300, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:47', '2017-01-16 17:14:47'),
(301, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:47', '2017-01-16 17:14:47'),
(302, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:47', '2017-01-16 17:14:47'),
(303, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:48', '2017-01-16 17:14:48'),
(304, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:48', '2017-01-16 17:14:48'),
(305, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:48', '2017-01-16 17:14:48'),
(306, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:48', '2017-01-16 17:14:48'),
(307, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:49', '2017-01-16 17:14:49'),
(308, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:49', '2017-01-16 17:14:49'),
(309, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:49', '2017-01-16 17:14:49'),
(310, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:49', '2017-01-16 17:14:49'),
(311, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:50', '2017-01-16 17:14:50'),
(312, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:50', '2017-01-16 17:14:50'),
(313, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:50', '2017-01-16 17:14:50'),
(314, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:50', '2017-01-16 17:14:50'),
(315, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:50', '2017-01-16 17:14:50'),
(316, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:50', '2017-01-16 17:14:50'),
(317, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:50', '2017-01-16 17:14:50'),
(318, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:50', '2017-01-16 17:14:50'),
(319, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:51', '2017-01-16 17:14:51'),
(320, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:51', '2017-01-16 17:14:51'),
(321, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:51', '2017-01-16 17:14:51'),
(322, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:51', '2017-01-16 17:14:51'),
(323, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:51', '2017-01-16 17:14:51'),
(324, 91, 'Commissioner of Internal Revenue', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 11, 2010, 0, 2017, 'Y', '2017-01-16 17:14:51', '2017-01-16 17:14:51'),
(337, 1, 'Chief Operating Officer', 'Công ty Cổ phần Basa (BASACO)', '', 'California, United States', 0, 0, 1989, 0, 2017, 'Y', '2017-01-16 20:44:28', '2017-01-16 20:44:28'),
(338, 1, 'City Alderman', 'Công ty Cổ phần Bao bì Biên Hòa (SOVI)', 'aaaa', 'State of São Paulo, Brazil', 0, 0, 1985, 0, 2017, 'Y', '2017-01-16 20:47:26', '2017-01-16 20:47:26'),
(339, 93, 'Chuyên viên thanh toán', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng PTSP&Marketing - Khối KHDN', 'Hanoi, Vietnam', 0, 0, 2012, 0, 2017, 'Y', '2017-01-17 22:15:08', '2017-01-17 22:15:08'),
(340, 97, 'Manager', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'IT', 'Kolkata, West Bengal, India', 0, 9, 2015, 8, 2016, 'Y', '2017-01-20 18:06:08', '2017-01-20 18:06:08'),
(341, 98, 'manager', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata Zoo, Kolkata, West Bengal, India', 0, 12, 2015, 0, 2015, 'Y', '2017-01-20 18:19:34', '2017-01-20 18:19:34'),
(342, 100, 'Mnger', 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', 'It', 'Kolkata, West Bengal, India', 0, 9, 2008, 12, 2016, 'Y', '2017-01-20 19:56:02', '2017-01-20 19:56:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `all_tropics`
--
ALTER TABLE `all_tropics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ask_questions`
--
ALTER TABLE `ask_questions`
  ADD PRIMARY KEY (`id`), ADD KEY `ask_questions_user_id_index` (`user_id`), ADD KEY `ask_questions_post_type_index` (`post_type`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_ifnos`
--
ALTER TABLE `contact_ifnos`
  ADD PRIMARY KEY (`id`), ADD KEY `contact_ifnos_user_id_index` (`user_id`);

--
-- Indexes for table `contact_messages`
--
ALTER TABLE `contact_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD UNIQUE KEY `idCountry` (`idCountry`);

--
-- Indexes for table `degrees`
--
ALTER TABLE `degrees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `educations`
--
ALTER TABLE `educations`
  ADD PRIMARY KEY (`id`), ADD KEY `educations_user_id_index` (`user_id`), ADD KEY `educations_country_index` (`country`), ADD KEY `educations_from_month_index` (`from_month`), ADD KEY `educations_from_year_index` (`from_year`), ADD KEY `educations_to_month_index` (`to_month`), ADD KEY `educations_to_year_index` (`to_year`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorite_tropics`
--
ALTER TABLE `favorite_tropics`
  ADD PRIMARY KEY (`id`), ADD KEY `favorite_tropics_tropic_id_index` (`tropic_id`), ADD KEY `favorite_tropics_user_id_index` (`user_id`);

--
-- Indexes for table `filter_order_sets`
--
ALTER TABLE `filter_order_sets`
  ADD PRIMARY KEY (`id`), ADD KEY `filter_order_sets_user_id_index` (`user_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`), ADD KEY `languages_locale_index` (`locale`);

--
-- Indexes for table `notification_settings`
--
ALTER TABLE `notification_settings`
  ADD PRIMARY KEY (`id`), ADD KEY `notification_settings_user_id_index` (`user_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_translations`
--
ALTER TABLE `page_translations`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `page_translations_page_id_locale_unique` (`page_id`,`locale`), ADD KEY `page_translations_locale_index` (`locale`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `privacy_settings`
--
ALTER TABLE `privacy_settings`
  ADD PRIMARY KEY (`id`), ADD KEY `privacy_settings_user_id_index` (`user_id`), ADD KEY `privacy_settings_max_request_index` (`max_request`);

--
-- Indexes for table `school_majors`
--
ALTER TABLE `school_majors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timezones`
--
ALTER TABLE `timezones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tropics`
--
ALTER TABLE `tropics`
  ADD PRIMARY KEY (`id`), ADD KEY `tropics_user_id_index` (`user_id`), ADD KEY `tropics_type_index` (`type`);

--
-- Indexes for table `tropic_groups`
--
ALTER TABLE `tropic_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `universities`
--
ALTER TABLE `universities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`), ADD UNIQUE KEY `users_user_name_unique` (`user_name`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`id`), ADD KEY `user_address_user_id_index` (`user_id`), ADD KEY `user_address_type_index` (`type`);

--
-- Indexes for table `work_experiences`
--
ALTER TABLE `work_experiences`
  ADD PRIMARY KEY (`id`), ADD KEY `work_experiences_user_id_index` (`user_id`), ADD KEY `work_experiences_country_index` (`country`), ADD KEY `work_experiences_from_month_index` (`from_month`), ADD KEY `work_experiences_from_year_index` (`from_year`), ADD KEY `work_experiences_to_month_index` (`to_month`), ADD KEY `work_experiences_to_year_index` (`to_year`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `all_tropics`
--
ALTER TABLE `all_tropics`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `ask_questions`
--
ALTER TABLE `ask_questions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=354;
--
-- AUTO_INCREMENT for table `contact_ifnos`
--
ALTER TABLE `contact_ifnos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `contact_messages`
--
ALTER TABLE `contact_messages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `degrees`
--
ALTER TABLE `degrees`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `educations`
--
ALTER TABLE `educations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `favorite_tropics`
--
ALTER TABLE `favorite_tropics`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `filter_order_sets`
--
ALTER TABLE `filter_order_sets`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `notification_settings`
--
ALTER TABLE `notification_settings`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `page_translations`
--
ALTER TABLE `page_translations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `privacy_settings`
--
ALTER TABLE `privacy_settings`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `school_majors`
--
ALTER TABLE `school_majors`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=406;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `timezones`
--
ALTER TABLE `timezones`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=549;
--
-- AUTO_INCREMENT for table `tropics`
--
ALTER TABLE `tropics`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=588;
--
-- AUTO_INCREMENT for table `tropic_groups`
--
ALTER TABLE `tropic_groups`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `universities`
--
ALTER TABLE `universities`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=191;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=311;
--
-- AUTO_INCREMENT for table `work_experiences`
--
ALTER TABLE `work_experiences`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=343;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ask_questions`
--
ALTER TABLE `ask_questions`
ADD CONSTRAINT `ask_questions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `contact_ifnos`
--
ALTER TABLE `contact_ifnos`
ADD CONSTRAINT `contact_ifnos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `educations`
--
ALTER TABLE `educations`
ADD CONSTRAINT `educations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `favorite_tropics`
--
ALTER TABLE `favorite_tropics`
ADD CONSTRAINT `favorite_tropics_tropic_id_foreign` FOREIGN KEY (`tropic_id`) REFERENCES `all_tropics` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `favorite_tropics_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `filter_order_sets`
--
ALTER TABLE `filter_order_sets`
ADD CONSTRAINT `filter_order_sets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notification_settings`
--
ALTER TABLE `notification_settings`
ADD CONSTRAINT `notification_settings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `page_translations`
--
ALTER TABLE `page_translations`
ADD CONSTRAINT `page_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `privacy_settings`
--
ALTER TABLE `privacy_settings`
ADD CONSTRAINT `privacy_settings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tropics`
--
ALTER TABLE `tropics`
ADD CONSTRAINT `tropics_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_address`
--
ALTER TABLE `user_address`
ADD CONSTRAINT `user_address_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `work_experiences`
--
ALTER TABLE `work_experiences`
ADD CONSTRAINT `work_experiences_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
